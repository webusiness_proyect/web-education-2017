-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:3306
-- Tiempo de generación: 07-09-2016 a las 12:08:13
-- Versión del servidor: 5.6.28-76.1-log
-- Versión de PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `wbinnova_education`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `actualizar_area`(IN `nombre` VARCHAR(60), IN `id` INT UNSIGNED)
    NO SQL
    COMMENT 'actualiza el nombre de un registro en la tabla AREAS'
IF NOT EXISTS(SELECT* FROM AREAS WHERE AREAS.nombre_area = nombre AND AREAS.id_area <> id)THEN
UPDATE AREAS SET AREAS.nombre_area = nombre WHERE AREAS.id_area = id;
SELECT 'Se actualizo el nombre del curso exitosamente!!!' AS msg;
ELSE
SELECT 'No se pudo actualizar el nombre del curso porque ya existe en otro registro!!!' AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `actualizar_carrera`(IN `id` INT, IN `nombre` VARCHAR(70), IN `nombreCorto` VARCHAR(15), IN `descripcion` VARCHAR(500))
    NO SQL
    COMMENT 'actualiza el nombre de un registro en la tabla CARRERAS'
IF NOT EXISTS(SELECT* FROM CARRERAS WHERE CARRERAS.nombre_carrera = nombre AND CARRERAS.id_carrera <> id)THEN
UPDATE CARRERAS SET CARRERAS.nombre_carrera = nombre, CARRERAS.nombre_corto_carrera = nombreCorto, CARRERAS.descripcion_carrera = descripcion
WHERE CARRERAS.id_carrera = id;
SELECT 'Se actualizaron los datos de la carrera exitosamente!!!' AS msg;
ELSE
SELECT 'No se pudieron actualizar los datos de la carrera porque el nombre: "', nombre, '", ya existe en otro registro de la base de datos!!!' AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `actualizar_jornada`(IN `id` INT, IN `nombre` VARCHAR(25))
    NO SQL
    COMMENT 'actualiza el nombre de un registro en la tabla JORNADAS'
IF NOT EXISTS (SELECT* FROM JORNADAS WHERE JORNADAS.nombre_jornada = nombre and JORNADAS.id_jornada <> id)THEN
UPDATE JORNADAS SET JORNADAS.nombre_jornada = nombre WHERE JORNADAS.id_jornada = id;
SELECT 'Los datos se actualizaron exitosamente!!!' AS msg;
ELSE
SELECT concat('No se pudieron actualizar los datos del plan, porque el nombre de jornada: ', nombre, ', ya existe en la base de datos') AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `actualizar_nivel`(IN `id` INT, IN `nombre` VARCHAR(30))
    NO SQL
    COMMENT 'actualiza los datos de un registro en la tabla NIVELES'
IF NOT EXISTS (SELECT* FROM NIVELES WHERE NIVELES.nombre_nivel = nombre and NIVELES.id_nivel <> id) THEN
UPDATE NIVELES SET NIVELES.nombre_nivel = nombre WHERE NIVELES.id_nivel = id;
SELECT 'Los datos del nivel han sido actualizados exitosamente!!!' AS msg;
ELSE
SELECT concat('No se pudieron actualizar los datos del nivel, porque el nombre: ', nombre, ', ya existe en otro registro de la base de datos!!!') AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `actualizar_nivel_grado`(IN `id` INT, IN `npj` INT, IN `grado` INT, IN `carrera` INT, IN `seccion` INT, IN `inscripcion` DECIMAL(5,2), IN `mensualidad` DECIMAL(5,2))
    NO SQL
    COMMENT 'actualiza el nombre de un registro en la tabla NIVELES_GRADOS'
IF NOT EXISTS(SELECT* FROM NIVELES_GRADOS WHERE NIVELES_GRADOS.id_grado = grado AND NIVELES_GRADOS.id_seccion = seccion AND NIVELES_GRADOS.id_nivel_plan_jornada = npj AND NIVELES_GRADOS.id_carrera = carrera AND NIVELES_GRADOS.id_nivel_grado <> id)THEN
UPDATE NIVELES_GRADOS SET NIVELES_GRADOS.id_grado = grado, NIVELES_GRADOS.id_seccion = seccion, NIVELES_GRADOS.id_nivel_plan_jornada = npj, NIVELES_GRADOS.id_carrera = carrera, NIVELES_GRADOS.cuota_inscripcion = inscripcion, NIVELES_GRADOS.cuota_mensualidad = mensualidad WHERE NIVELES_GRADOS.id_nivel_grado = id;
SELECT 'Se actualizaron los datos de la asignación de grado al nivel exitosamente!!!' AS msg;
ELSE
SELECT 'No se pudo actulaizar la asignación de grado al nivel, porque ya existe los mismos datos en otro registro!!!' AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `actualizar_persona`(IN `id` INT, IN `nombres` VARCHAR(50), IN `apellidos` VARCHAR(50), IN `cui` VARCHAR(13), IN `direccion` VARCHAR(60), IN `nacimiento` DATE, IN `telefono` VARCHAR(10), IN `telefonob` VARCHAR(10), IN `puesto` INT, IN `correo` VARCHAR(50))
    NO SQL
    COMMENT 'Cambia el estado de un registro de la tabla PERSONAS'
BEGIN
	if not exists(select* from PERSONAS where PERSONAS.cui_persona = cui and PERSONAS.id_persona <> id)then
		update PERSONAS set PERSONAS.nombres_persona = nombres, PERSONAS.apellidos_persona = apellidos, PERSONAS.cui_persona = cui,
		PERSONAS.direccion_persona = direccion, PERSONAS.fecha_nacimiento_persona = nacimiento,
        PERSONAS.telefono_persona = telefono, PERSONAS.telefono_auxiliar_persona = telefonob, PERSONAS.id_puesto = puesto, PERSONAS.correo_persona = correo
        where PERSONAS.id_persona = id;
        select 'Se actulizaron los datos de la persona exitosamente!!!' as msg;
	else
		select concat('No se pudieron actualizar los datos de la persona porque el No de DPI: ', cui, ', ya existe en otro registro!!!') as msg;
	end if;
END$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `actualizar_plan`(IN `id` INT, IN `nombre` VARCHAR(25))
    NO SQL
    COMMENT 'Actuliza el nombre de un registro de la tabla PLANES'
IF NOT EXISTS(SELECT* FROM PLANES WHERE PLANES.nombre_plan = nombre AND PLANES.id_plan <> id)THEN
UPDATE PLANES set PLANES.nombre_plan = nombre WHERE PLANES.id_plan = id;
SELECT 'Los datos fueron actualizados existosamente!!!' AS msg;
ELSE
SELECT concat('No se pudieron actualizar los datos, porque el nombre de plan: ',nombre, ', ya existe en la base de datos!!!') AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `actualizar_plan_nivel`(IN `id` INT, IN `nivel` INT, IN `plan` INT, IN `jornada` INT)
    NO SQL
    COMMENT 'actualiza datos de un registro en tabla NIVELES_PLANES_JORNADAS'
IF NOT EXISTS(SELECT* FROM NIVELES_PLANES_JORNADAS WHERE NIVELES_PLANES_JORNADAS.id_nivel = nivel AND NIVELES_PLANES_JORNADAS.id_jornada = jornada AND NIVELES_PLANES_JORNADAS.id_plan = plan AND NIVELES_PLANES_JORNADAS.id_nivel_plan_jornada <> id) THEN
UPDATE NIVELES_PLANES_JORNADAS SET NIVELES_PLANES_JORNADAS.id_nivel = nivel, NIVELES_PLANES_JORNADAS.id_jornada = jornada, NIVELES_PLANES_JORNADAS.id_plan = plan WHERE NIVELES_PLANES_JORNADAS.id_nivel_plan_jornada = id;
SELECT 'Se actualizaron los datos de asignación del nivel en el plan y jornada exitosamente!!!' AS msg;
ELSE
SELECT 'No se pudieron actualizar los datos de asignación del nivel en el plan y jornada, porque ya existe un mismo registro con la misma asignación!!!' AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `actualizar_puesto`(IN `id` INT, IN `nombre` VARCHAR(30))
    NO SQL
    COMMENT 'actualiza el nombre de un registro en la tabla PUESTOS'
IF NOT EXISTS(SELECT* FROM PUESTOS WHERE PUESTOS.nombre_puesto = nombre AND PUESTOS.id_puesto <> id)THEN
UPDATE PUESTOS SET PUESTOS.nombre_puesto = nombre WHERE PUESTOS.id_puesto = id;
SELECT 'Los datos del puesto han sido actualizados!!!' AS msg;
ELSE
SELECT concat('No se pudieron actulizar los datos del puesto, porque el nombre del puesto: ', nombre, ', ya existe en otro registro de la base de datos!!!') AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `actualizar_salon`(IN `id` INT, IN `nombre` VARCHAR(20))
    NO SQL
    COMMENT 'actualiza el nombre de un registro en la tabla SALONES'
IF NOT EXISTS(SELECT* FROM SALONES WHERE SALONES.nombre_salon = nombre AND SALONES.id_salon <> id)THEN
UPDATE SALONES SET SALONES.nombre_salon = nombre WHERE SALONES.id_salon = id;
SELECT 'Se actualizo el nombre del salón exitosamente!!!' AS msg;
ELSE
SELECT concat('No se pudo actualizar el nombre de salón porque l nombre: "',nombre,'" ya existe en otro registro!!!') as msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `actualizar_seccion`(IN `id` INT, IN `nombre` VARCHAR(10))
    NO SQL
    COMMENT 'actualiza el nombre de un registro en la tabla SECCIONES'
IF NOT EXISTS(SELECT* FROM SECCIONES WHERE SECCIONES.nombre_seccion = nombre AND SECCIONES.id_seccion <> id)THEN
UPDATE SECCIONES SET SECCIONES.nombre_seccion = nombre WHERE SECCIONES.id_seccion = id;
SELECT concat('Se a actualizado los datos de la seccion!!!') AS msg;
ELSE
SELECT concat('No se pudo actualizar el nombre de la sección, porque el nombre: ', nombre, ', ya existe en la base de datos!!!') AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `estado_area`(IN `id` INT, IN `estado` BOOLEAN)
    NO SQL
    COMMENT 'Cambia el estado de un registro de la tabla AREAS'
UPDATE AREAS SET AREAS.estado_area = estado WHERE AREAS.id_area = id$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `estado_asignacion_area`(IN `id` INT, IN `estado` BOOLEAN)
    NO SQL
    COMMENT 'Cambia el estado de un registro de la tabla ASIGNACION_AREAS'
UPDATE ASIGNACION_AREAS SET ASIGNACION_AREAS.estado_asignacion_area = estado WHERE ASIGNACION_AREAS.id_asignacion_area = id$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `estado_carrera`(IN `id` INT, IN `estado` BOOLEAN)
    NO SQL
    COMMENT 'Cambia el estado de un registro de la tabla CARRERAS'
UPDATE CARRERAS SET CARRERAS.estado_carrera = estado WHERE CARRERAS.id_carrera = id$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `estado_grado`(IN `id` INT, IN `estado` BOOLEAN)
    NO SQL
    COMMENT 'Cambia el estado de un registro de la tabla GRADOS'
UPDATE GRADOS SET GRADOS.estado_grado = estado WHERE GRADOS.id_grado = id$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `estado_jornada`(IN `id` INT, IN `estado` BOOLEAN)
    NO SQL
    COMMENT 'Cambia el estado de un registro de la tabla JORNADAS'
UPDATE JORNADAS SET JORNADAS.estado_jornada = estado WHERE
JORNADAS.id_jornada = id$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `estado_nivel`(IN `id` INT, IN `estado` BOOLEAN)
    NO SQL
    COMMENT 'Cambia el estado de un registro de la tabla NIVELES'
UPDATE NIVELES SET NIVELES.estado_nivel = estado WHERE NIVELES.id_nivel = id$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `estado_nivel_grado`(IN `id` INT, IN `estado` BOOLEAN)
    NO SQL
    COMMENT 'Cambia el estado de un registro de la tabla NIVELES_GRADOS'
UPDATE NIVELES_GRADOS SET NIVELES_GRADOS.estado_nivel_grado = estado WHERE NIVELES_GRADOS.id_nivel_grado = id$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `estado_permiso_rol`(IN `id` INT, IN `estado` BOOLEAN)
    NO SQL
    COMMENT 'Cambia el estado de un registro de la tabla PERMISOS_ROLES'
BEGIN
	update permission_role SET permission_role.state_permission_role = estado where
    permission_role.id_permission_role = id;
END$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `estado_persona`(IN `id` INT, IN `estado` BOOLEAN)
    NO SQL
    COMMENT 'Cambia el estado de un registro de la tabla PERSONAS'
BEGIN
	update PERSONAS set PERSONAS.estado_persona = estado where  PERSONAS.id_persona = id;
END$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `estado_plan`(IN `id` INT, IN `estado` BOOLEAN)
    NO SQL
    COMMENT 'Cambia el estado de un registro de la tabla PLANES'
UPDATE PLANES SET PLANES.estado_plan = estado WHERE
PLANES.id_plan = id$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `estado_puesto`(IN `id` INT, IN `estado` BOOLEAN)
    NO SQL
    COMMENT 'Cambia el estado de un registro de la tabla PUESTOS'
UPDATE PUESTOS SET PUESTOS.estado_puesto = estado WHERE PUESTOS.id_puesto = id$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `estado_rol`(IN `id` INT, IN `estado` BOOLEAN)
    NO SQL
    COMMENT 'Cambia el estado de un registro de la tabla ROLES'
BEGIN
	update roles set roles.state_rol = estado where roles.id = id;
END$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `estado_salon`(IN `id` INT, IN `estado` BOOLEAN)
    NO SQL
    COMMENT 'Cambia el estado de un registro de la tabla SALONES'
UPDATE SALONES SET SALONES.estado_salon = estado WHERE SALONES.id_salon = id$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nueva_area`(IN `nombre` VARCHAR(60), IN `fecha` DATE)
    NO SQL
    COMMENT 'Agrega un nuevo registro a la tabla AREAS'
IF NOT EXISTS (SELECT* FROM AREAS where AREAS.nombre_area = nombre) THEN
INSERT INTO AREAS(AREAS.nombre_area, AREAS.fecha_registro_area, AREAS.estado_area) VALUES(nombre, fecha, TRUE);
SELECT concat('Se registro el nuevo curso: "', nombre,'", exitosamente!!!') AS msg;
ELSE
SELECT concat('No se pudo registrar el curso: ', nombre, ', porque ya existe en la base de datos!!!') AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nueva_asignacion_area`(IN `grado` INT, IN `salon` INT, IN `curso` INT, IN `fecha` DATETIME)
    NO SQL
    COMMENT 'Agrega un nuevo registro a la tabla ASIGNACION_AREAS'
IF NOT EXISTS(SELECT* FROM ASIGNACION_AREAS WHERE ASIGNACION_AREAS.id_nivel_grado = grado AND ASIGNACION_AREAS.id_area = curso) THEN
INSERT INTO ASIGNACION_AREAS(ASIGNACION_AREAS.id_nivel_grado, ASIGNACION_AREAS.id_area, ASIGNACION_AREAS.id_salon, ASIGNACION_AREAS.fecha_asignacion_area, ASIGNACION_AREAS.estado_asignacion_area)
VALUES(grado, curso, salon, fecha, TRUE);
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nueva_asignacion_docente`(IN `asignacionarea` INT, IN `persona` INT)
    NO SQL
    COMMENT 'Agrega un nuevo registro a la tabla ASIGNACION_DOCENTE'
BEGIN
	if not exists(select* from ASIGNACION_DOCENTE where ASIGNACION_DOCENTE.id_asignacion_area = asignacionarea and ASIGNACION_DOCENTE.id_persona = persona)then
		insert into ASIGNACION_DOCENTE(ASIGNACION_DOCENTE.id_asignacion_area, ASIGNACION_DOCENTE.id_persona, ASIGNACION_DOCENTE.estado_asignacion_docente)
        values(asignacionarea, persona, TRUE);
	end if;
END$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nueva_carrera`(IN `nombre` VARCHAR(70), IN `nombreCorto` VARCHAR(15), IN `descripcion` VARCHAR(500), IN `fecha` TIMESTAMP)
    NO SQL
    COMMENT 'Agrega un nuevo registro a la tabla CARRERAS'
IF NOT EXISTS(SELECT* FROM CARRERAS WHERE CARRERAS.nombre_carrera = nombre) THEN
INSERT INTO CARRERAS(CARRERAS.nombre_carrera, CARRERAS.nombre_corto_carrera, CARRERAS.descripcion_carrera, CARRERAS.fecha_creacion_carrera, CARRERAS.estado_carrera)
VALUES(nombre, nombreCorto, descripcion, fecha, TRUE);
SELECT concat('Se ha registrado la nueva carrera: "', nombre, '", exitosamente!!!') AS msg;
ELSE
SELECT concat('No se ha podido registrar la carrera: ', nombre, ', porque ya existe en la base de datos!!!') AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nueva_jornada`(IN `nombre` VARCHAR(25))
    NO SQL
    COMMENT 'Agrega un nuevo registro a la tabla JORNADAS'
IF NOT EXISTS (SELECT* FROM JORNADAS WHERE JORNADAS.nombre_jornada = nombre) THEN
INSERT INTO JORNADAS(JORNADAS.nombre_jornada, JORNADAS.estado_jornada)
VALUES(nombre, TRUE);
SELECT concat('Se ha registrado la nueva jornada: ', nombre, ', exitosamente!!!') AS msg;
ELSE
SELECT concat('No se pudo registrar la nueva jornada, porque el nombre de la jornada: ', nombre, ', ya existe en la base de datos') AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nueva_persona`(IN `nombres` VARCHAR(50), IN `apellidos` VARCHAR(50), IN `cui` VARCHAR(13), IN `direccion` VARCHAR(60), IN `nacimiento` DATE, IN `telefono` VARCHAR(10), IN `telefonob` VARCHAR(10), IN `puesto` INT, IN `correo` VARCHAR(50))
    NO SQL
    COMMENT '''Agrega un nuevo registro a la tabla PERSONAS'
BEGIN
	if not exists(select* from PERSONAS where PERSONAS.cui_persona = cui)then
		insert into PERSONAS(PERSONAS.nombres_persona, PERSONAS.apellidos_persona,
        PERSONAS.cui_persona, PERSONAS.direccion_persona, PERSONAS.fecha_nacimiento_persona,
        PERSONAS.telefono_persona, PERSONAS.telefono_auxiliar_persona, PERSONAS.id_puesto, PERSONAS.estado_persona, PERSONAS.correo_persona)
        values(nombres, apellidos, cui, direccion, nacimiento, telefono, telefonob, puesto, TRUE, correo);
        select concat('Se ha registrado a la nueva persona: "', nombres, ' ', apellidos, '", exitosamente!!!') as msg;
	else
		select concat('No se pudo registrar a la nueva persona, porque el No de DPI: ', cui, ', ya esta registrado!!!') as msg;
	end if;
END$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nueva_seccion`(IN `nombre` VARCHAR(10))
    NO SQL
    COMMENT 'Agrega un nuevo registro a la tabla SECCIONES'
IF NOT EXISTS(SELECT* FROM SECCIONES WHERE SECCIONES.nombre_seccion = nombre)THEN
INSERT INTO SECCIONES(SECCIONES.nombre_seccion)
VALUES(nombre);
SELECT concat('Se a registrado la nueva sección: ', nombre, ', exitosamente!!!') AS msg;
ELSE
SELECT concat('No se pudo registrar la nueva sección, porque el nombre de sección: ', nombre, ', ya existe en la base de datos!!!');
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nuevo_grado`(IN `nombre` VARCHAR(40), IN `fecha` DATE)
    NO SQL
    COMMENT 'Agrega un nuevo registro a la tabla GRADOS'
IF NOT EXISTS (SELECT* FROM GRADOS WHERE GRADOS.nombre_grado = nombre) THEN
INSERT INTO GRADOS(GRADOS.nombre_grado, GRADOS.fecha_registro_grado) VALUES(nombre, fecha);
SELECT concat('Se a registrado el nuevo grado: ', nombre, ', exitosamente!!!') AS msg;
ELSE
SELECT concat('No se pudo registrar el nuevo grado: ', nombre, ', porque ya existe en la base de datos!!!') AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nuevo_nivel`(IN `nombre` VARCHAR(30), IN `fecha` DATE)
    NO SQL
    COMMENT 'Agrega un nuevo registro a la tabla NIVELES'
IF NOT EXISTS (SELECT* FROM NIVELES WHERE NIVELES.nombre_nivel = nombre)THEN
INSERT INTO NIVELES(NIVELES.nombre_nivel, NIVELES.fecha_registro_nivel)
VALUES(nombre, fecha);
SELECT concat('Se ha registrado el nuevo nivel: ', nombre, ', exitosamente!!!') AS msg;
ELSE
SELECT concat('No se pudo registrar el nivel porque el nombre de nivel: ', nombre, ', ya existe en la base de datos!!!') AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nuevo_nivel_grado`(IN `npj` INT, IN `grado` INT, IN `carrera` INT, IN `seccion` INT, IN `inscripcion` DECIMAL(5,2), IN `mensualidad` DECIMAL(5,2))
    NO SQL
    COMMENT 'Agrega un nuevo registro a la tabla NIVELES_GRADOS'
IF NOT EXISTS(SELECT* FROM NIVELES_GRADOS WHERE NIVELES_GRADOS.id_grado = grado AND NIVELES_GRADOS.id_seccion = seccion AND NIVELES_GRADOS.id_nivel_plan_jornada = npj AND NIVELES_GRADOS.id_carrera = carrera)THEN
INSERT INTO NIVELES_GRADOS(NIVELES_GRADOS.id_grado, NIVELES_GRADOS.id_seccion, NIVELES_GRADOS.id_nivel_plan_jornada, NIVELES_GRADOS.id_carrera,NIVELES_GRADOS.estado_nivel_grado, NIVELES_GRADOS.cuota_inscripcion, NIVELES_GRADOS.cuota_mensualidad)
VALUES(grado, seccion, npj, carrera, TRUE, inscripcion, mensualidad);
SELECT 'Se registrado la nueva asignación del grado al nivel' AS msg;
ELSE
SELECT 'No se pudo registrar la asignación de grado al nivel porque ya existe en la base de datos!!!' AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nuevo_permiso_rol`(IN `rol` INT, IN `permiso` INT)
    NO SQL
    COMMENT 'Agrega un nuevo registro a la tabla PERMISOS_ROLES'
IF NOT EXISTS(SELECT* FROM permission_role WHERE permission_role.role_id = rol and permission_role.permission_id = permiso)THEN
INSERT INTO permission_role(permission_role.role_id, permission_role.permission_id, permission_role.state_permission_role)
VALUES(rol, permiso, TRUE);
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nuevo_plan`(IN `nombre` VARCHAR(25))
    NO SQL
    COMMENT 'crea un nuevo registro en la tabla PLANES'
IF NOT EXISTS(SELECT* FROM PLANES WHERE PLANES.nombre_plan = nombre) THEN
INSERT INTO PLANES(PLANES.nombre_plan, PLANES.estado_plan)
VALUES(nombre, TRUE);
SELECT concat('Se ha registrado exitosamente el nuevo plan: ', nombre, '!!!') AS msg;
ELSE
SELECT concat('No se pudo registrar el plan: ', nombre, ', porque ya existe en la base de datos!!!') AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nuevo_plan_nivel`(IN `nivel` INT, IN `plan` INT, IN `jornada` INT)
    NO SQL
    COMMENT 'Agrega un nuevo registro a la tabla NIVELES_PLANES_JORNADAS'
IF NOT EXISTS (SELECT* FROM NIVELES_PLANES_JORNADAS WHERE NIVELES_PLANES_JORNADAS.id_nivel = nivel AND NIVELES_PLANES_JORNADAS.id_plan = plan AND NIVELES_PLANES_JORNADAS.id_jornada = jornada) THEN
INSERT INTO NIVELES_PLANES_JORNADAS(NIVELES_PLANES_JORNADAS.id_nivel, NIVELES_PLANES_JORNADAS.id_plan, NIVELES_PLANES_JORNADAS.id_jornada)
VALUES(nivel, plan, jornada);
SELECT 'Se a registrado la asignación del nivel a un plan y una jornada' AS msg;
ELSE
SELECT 'No se pudo registrar la asignación porque ya existe ya existe un registro del mismo nivel en el mismo plan y jornada registrado!!!' AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nuevo_puesto`(IN `nombre` VARCHAR(30))
    NO SQL
    COMMENT 'Ingresa un nuevo puesto'
IF NOT EXISTS(SELECT* FROM PUESTOS WHERE PUESTOS.nombre_puesto = nombre) THEN
INSERT INTO PUESTOS(PUESTOS.nombre_puesto, PUESTOS.estado_puesto)
VALUES(nombre, TRUE);
SELECT concat('Se ha registrado el nuevo puesto: ', nombre,' exitosamente!!!') AS msg;
ELSE
SELECT concat('No se pudo registrar el  nuevo puesto, porque el nombre: ', nombre, 'ya existe en la base de datos!!!') AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nuevo_rol`(IN `nombre` VARCHAR(40), IN `descripcion` VARCHAR(100), IN `fecha` DATETIME)
    NO SQL
    COMMENT 'Agrega un nuevo registro a la tabla ROLES'
IF NOT EXISTS(SELECT* FROM roles WHERE roles.name = nombre)THEN
INSERT INTO roles(roles.name, roles.display_name, roles.description, roles.state_rol, roles.created_at)
VALUES(nombre, nombre, descripcion, TRUE, fecha);
SELECT roles.id FROM roles ORDER BY roles.id DESC LIMIT 1;
ELSE
SELECT 0 AS id;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nuevo_rol_usuario`(IN `usuario` INT, IN `rol` INT)
    NO SQL
    COMMENT 'crea nuevo registro en la table rol usuario'
BEGIN

    insert into role_user(role_user.user_id, role_user.role_id)
    values(usuario, rol);

END$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nuevo_salon`(IN `nombre` VARCHAR(20))
    NO SQL
    COMMENT 'Agrega un nuevo registro a la tabla SALONES'
IF NOT EXISTS(SELECT* FROM SALONES WHERE SALONES.nombre_salon = nombre) THEN
INSERT INTO SALONES(SALONES.nombre_salon, SALONES.estado_salon)
VALUES(nombre, TRUE);
SELECT concat('Se a registrado el nuevo salón: ', nombre, ' exitosamente!!!') AS msg;
ELSE
SELECT concat('No se pudo registrar el nuevo salon: ', nombre, ', porque ya existe en la base de datos!!!!') AS msg;
END IF$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nuevo_usuario`(IN `usuario` VARCHAR(70), IN `contrasena` VARCHAR(70), IN `fecha` DATE, IN `correo` VARCHAR(50), IN `rol` INT)
    NO SQL
    COMMENT 'agrega un nuevo registro en la tabla USUARIOS'
BEGIN
	if not exists(select* from users where users.name = usuario and users.email = correo)then
		insert into users(users.name, users.password, users.created_at, users.email, users.estado_usuario)
        values(usuario, contrasena, fecha, correo, TRUE);
		select users.id as id from users order by users.id desc limit 1;
	else
		select 0 as id;
	end if;
END$$

CREATE DEFINER=`wbinnova`@`localhost` PROCEDURE `nuevo_usuario_persona`(IN `usuario` INT, IN `persona` INT)
    NO SQL
    COMMENT 'agrega un nuevo regostro a la tabla USUARIO_PERSONA'
BEGIN
	insert into USUARIO_PERSONA(USUARIO_PERSONA.user_id, USUARIO_PERSONA.id_persona)
    values(usuario, persona);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ACTIVIDADES`
--

CREATE TABLE IF NOT EXISTS `ACTIVIDADES` (
  `id_actividad` bigint(20) NOT NULL,
  `id_asignacion_area` int(11) NOT NULL,
  `id_unidad` tinyint(4) NOT NULL,
  `id_tipo_actividad` tinyint(4) NOT NULL,
  `nombre_actividad` varchar(30) NOT NULL,
  `descripcion_actividad` varchar(200) NOT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_entrega` datetime NOT NULL,
  `nota_total` decimal(2,0) NOT NULL,
  `nombre_recurso` varchar(30) DEFAULT NULL,
  `url_recursos` varchar(50) DEFAULT NULL,
  `ciclo_academico` date NOT NULL,
  PRIMARY KEY (`id_actividad`),
  KEY `tipo_actividad_tareas_fk` (`id_tipo_actividad`),
  KEY `bloques_tareas_fk` (`id_unidad`),
  KEY `asignacion_areas_tareas_fk` (`id_asignacion_area`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los registros de una nueva actividad para que realice el estudiante';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `AREAS`
--

CREATE TABLE IF NOT EXISTS `AREAS` (
  `id_area` smallint(6) NOT NULL AUTO_INCREMENT,
  `nombre_area` varchar(60) NOT NULL,
  `fecha_registro_area` date NOT NULL,
  `estado_area` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_area`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los nombres de las áreas(cursos) que se imparten en el establecimiento' AUTO_INCREMENT=45 ;

--
-- Volcado de datos para la tabla `AREAS`
--

INSERT INTO `AREAS` (`id_area`, `nombre_area`, `fecha_registro_area`, `estado_area`) VALUES
(1, 'geografia', '2016-06-27', 1),
(2, 'finanzas', '2016-06-27', 1),
(3, 'computacion', '2016-06-27', 1),
(4, 'progrentis', '2016-06-27', 1),
(5, 'mecanografia', '2016-06-27', 1),
(6, 'contabilidad', '2016-06-27', 1),
(7, 'legislación', '2016-06-27', 1),
(8, 'legilación', '2016-06-27', 1),
(9, 'ingles', '2016-06-27', 1),
(10, 'lenguaje', '2016-06-27', 1),
(11, 'archivo', '2016-06-27', 1),
(12, 'cálculo', '2016-06-27', 1),
(13, 'prac-progra', '2016-06-27', 1),
(14, 'admon', '2016-06-27', 1),
(15, 'derecho/soci', '2016-06-27', 1),
(16, 'economia', '2016-06-27', 1),
(17, 'tecni. redac', '2016-06-27', 1),
(18, 'ed. física', '2016-06-27', 1),
(19, 'matematicas', '2016-06-27', 1),
(20, 'derecho', '2016-06-27', 1),
(21, 'orga. guber.', '2016-06-27', 1),
(22, 'practica sup', '2016-06-27', 1),
(23, 'merca', '2016-06-27', 1),
(24, 'seminario', '2016-06-27', 1),
(25, 'etica', '2016-06-27', 1),
(26, 'progra com.', '2016-06-27', 1),
(27, 'cc. sociales', '2016-06-27', 1),
(28, 'estadistica', '2016-06-27', 1),
(29, 'prob. socio.', '2016-06-27', 1),
(30, 'lógica', '2016-06-27', 1),
(31, 'algoritmos', '2016-06-27', 1),
(32, 'social/filo', '2016-06-27', 1),
(33, 'física', '2016-06-27', 1),
(34, 'ind/plasti', '2016-06-27', 1),
(35, 'hogar', '2016-06-27', 1),
(36, 'biología', '2016-06-27', 1),
(37, 'medio natural', '2016-06-27', 1),
(38, 'especia IV', '2016-06-27', 1),
(39, 'especia III', '2016-06-27', 1),
(40, 'Especia II', '2016-06-27', 1),
(41, 'especialidad', '2016-06-27', 1),
(42, 'química', '2016-06-27', 1),
(43, 'especial II', '2016-06-27', 1),
(44, 'lectura', '2016-08-19', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ASIGNACION_AREAS`
--

CREATE TABLE IF NOT EXISTS `ASIGNACION_AREAS` (
  `id_asignacion_area` int(11) NOT NULL AUTO_INCREMENT,
  `id_nivel_grado` int(11) NOT NULL,
  `id_area` smallint(6) NOT NULL,
  `id_salon` bigint(20) NOT NULL,
  `fecha_asignacion_area` datetime NOT NULL,
  `estado_asignacion_area` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_asignacion_area`),
  KEY `areas_asignacion_areas_fk` (`id_area`),
  KEY `salones_asignacion_areas_fk` (`id_salon`),
  KEY `niveles_grados_asignacion_areas_fk` (`id_nivel_grado`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar las asignaciones de áreas a los diferentes grados' AUTO_INCREMENT=20 ;

--
-- Volcado de datos para la tabla `ASIGNACION_AREAS`
--

INSERT INTO `ASIGNACION_AREAS` (`id_asignacion_area`, `id_nivel_grado`, `id_area`, `id_salon`, `fecha_asignacion_area`, `estado_asignacion_area`) VALUES
(1, 4, 3, 3, '2016-06-27 14:27:39', 1),
(2, 4, 9, 1, '2016-06-27 14:27:39', 1),
(3, 4, 4, 1, '2016-06-27 14:27:39', 1),
(4, 4, 43, 1, '2016-06-27 14:27:39', 1),
(5, 4, 41, 1, '2016-06-27 14:27:39', 1),
(6, 4, 38, 1, '2016-06-27 14:27:39', 1),
(7, 4, 39, 1, '2016-06-27 14:27:39', 1),
(8, 4, 32, 1, '2016-06-27 14:27:39', 1),
(9, 4, 10, 1, '2016-06-27 14:27:39', 1),
(10, 4, 18, 1, '2016-06-27 14:27:39', 1),
(11, 4, 33, 1, '2016-06-27 14:27:39', 1),
(12, 4, 19, 1, '2016-06-27 14:27:39', 1),
(13, 1, 3, 3, '2016-06-29 11:21:38', 1),
(14, 1, 9, 3, '2016-06-29 11:21:38', 1),
(15, 1, 34, 3, '2016-06-29 11:21:38', 1),
(16, 1, 19, 1, '2016-07-06 10:40:40', 1),
(17, 4, 5, 2, '2016-08-12 11:45:52', 1),
(18, 4, 6, 3, '2016-08-12 11:45:52', 1),
(19, 1, 44, 1, '2016-08-19 15:18:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ASIGNACION_DOCENTE`
--

CREATE TABLE IF NOT EXISTS `ASIGNACION_DOCENTE` (
  `id_asignacion_docente` bigint(20) NOT NULL AUTO_INCREMENT,
  `estado_asignacion_docente` tinyint(1) NOT NULL DEFAULT '1',
  `id_asignacion_area` int(11) NOT NULL,
  `id_persona` int(11) NOT NULL,
  PRIMARY KEY (`id_asignacion_docente`),
  KEY `personas_asignacion_docente_fk` (`id_persona`),
  KEY `asignacion_areas_asignacion_docente_fk` (`id_asignacion_area`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para gurdar las asignaciones de los docentes, de las áreas que imparten en cada grado' AUTO_INCREMENT=31 ;

--
-- Volcado de datos para la tabla `ASIGNACION_DOCENTE`
--

INSERT INTO `ASIGNACION_DOCENTE` (`id_asignacion_docente`, `estado_asignacion_docente`, `id_asignacion_area`, `id_persona`) VALUES
(1, 1, 13, 3),
(2, 1, 14, 3),
(3, 1, 15, 3),
(4, 1, 10, 3),
(5, 1, 9, 3),
(6, 1, 8, 3),
(7, 1, 1, 3),
(8, 1, 3, 3),
(9, 1, 4, 3),
(10, 1, 12, 3),
(11, 1, 6, 3),
(12, 1, 13, 2),
(13, 1, 14, 2),
(14, 1, 15, 2),
(15, 1, 16, 2),
(16, 1, 1, 2),
(17, 1, 2, 2),
(18, 1, 3, 2),
(19, 1, 13, 4),
(20, 1, 14, 4),
(21, 1, 15, 4),
(22, 1, 16, 4),
(23, 1, 14, 7),
(24, 1, 1, 7),
(25, 1, 12, 7),
(26, 1, 3, 7),
(27, 1, 13, 9),
(28, 1, 16, 9),
(29, 1, 1, 9),
(30, 1, 12, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `BITACORA_ACTIVIDADES`
--

CREATE TABLE IF NOT EXISTS `BITACORA_ACTIVIDADES` (
  `id_bitacora_actividad` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_actividad` varchar(200) NOT NULL,
  `fecha_actividad` datetime NOT NULL,
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id_bitacora_actividad`),
  KEY `usuarios_vitacora_actividades_fk` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar el registro de actividades de todos los usuarios que ingresen al sistema' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `CARRERAS`
--

CREATE TABLE IF NOT EXISTS `CARRERAS` (
  `id_carrera` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_carrera` varchar(70) NOT NULL,
  `nombre_corto_carrera` varchar(15) NOT NULL,
  `descripcion_carrera` varchar(500) NOT NULL,
  `fecha_creacion_carrera` datetime NOT NULL,
  `estado_carrera` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_carrera`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los datos de las carreras que se imparten en una institución' AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `CARRERAS`
--

INSERT INTO `CARRERAS` (`id_carrera`, `nombre_carrera`, `nombre_corto_carrera`, `descripcion_carrera`, `fecha_creacion_carrera`, `estado_carrera`) VALUES
(1, 'no aplica', 'na', 'Para los grados en los que no aplique una carrera', '2016-06-27 09:53:05', 1),
(2, 'bachillerato en computación', 'bc', 'Carrera de bachillerato en computación', '2016-06-27 09:53:59', 1),
(3, 'bachilerato en chef', 'bch', 'Carrera de bachillerato con orientación en chef', '2016-06-27 10:02:12', 1),
(4, 'bachillertato en medicina', 'bm', 'Carrera de bachillerato con especialidad en medicina', '2016-06-27 10:21:14', 1),
(5, 'perito contador', 'pc', 'Carrera de perito contador', '2016-06-27 10:22:11', 1),
(6, 'perito en administración de empresas', 'pae', 'Carrera de perito con especialidad en administración de empresas con una duración de 3 años.', '2016-06-27 10:27:05', 1),
(7, 'bachillerato por madurez', 'bm', 'Carrera para el bachillerato por madurez', '2016-06-27 11:12:37', 1),
(8, 'Bachillerato en Diseño grafico', 'DG', 'Carrera de diseño grafico', '2016-09-07 16:53:55', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `DEPARTAMENTO`
--

CREATE TABLE IF NOT EXISTS `DEPARTAMENTO` (
  `id_departamento` smallint(6) NOT NULL COMMENT 'Campo para almacenar el identificador unico de cada registro',
  `nombre_departamento` varchar(50) DEFAULT NULL COMMENT 'campo para almacenar el nombre del departamento',
  PRIMARY KEY (`id_departamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar todo los datos de los departamentos de guatemala';

--
-- Volcado de datos para la tabla `DEPARTAMENTO`
--

INSERT INTO `DEPARTAMENTO` (`id_departamento`, `nombre_departamento`) VALUES
(1, 'alta verapaz'),
(2, 'baja verapaz'),
(3, 'chimaltenango'),
(4, 'chiquimula'),
(5, 'el progreso'),
(6, 'escuintla'),
(7, 'guatemala'),
(8, 'huehuetenango'),
(9, 'izabal'),
(10, 'jalapa'),
(11, 'jutiapa'),
(12, 'petén'),
(13, 'quetzaltenango'),
(14, 'quiché'),
(15, 'retalhuleu'),
(16, 'sacatepéquez'),
(17, 'san marcos'),
(18, 'santa rosa'),
(19, 'sololá'),
(20, 'suchitepéquez'),
(21, 'totonicapán'),
(22, 'zacapa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `DESCRIPCION`
--

CREATE TABLE IF NOT EXISTS `DESCRIPCION` (
  `id_descripcion` tinyint(4) NOT NULL AUTO_INCREMENT,
  `mision` varchar(1000) NOT NULL,
  `vision` varchar(1000) NOT NULL,
  `valores` varchar(1000) NOT NULL,
  `historia` varchar(2000) NOT NULL,
  `mas` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id_descripcion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar descripciones de la institución como: visio, mision, valores, entre otros.' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `DOCENTES_GUIAS`
--

CREATE TABLE IF NOT EXISTS `DOCENTES_GUIAS` (
  `id_docente_guia` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_docente_guia` date NOT NULL,
  `estado_docente_guia` tinyint(1) NOT NULL,
  `id_persona` int(11) NOT NULL,
  `id_nivel_grado` int(11) NOT NULL,
  PRIMARY KEY (`id_docente_guia`),
  KEY `personas_docentes_guias_fk` (`id_persona`),
  KEY `niveles_grados_docentes_guias_fk` (`id_nivel_grado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar a los docentes quías de cada grado' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ESTUDIANTES`
--

CREATE TABLE IF NOT EXISTS `ESTUDIANTES` (
  `id_estudiante` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` bigint(20) NOT NULL,
  `id_nivel_grado` int(11) NOT NULL,
  `id_municipio` int(11) NOT NULL,
  `nombre_estudiante` varchar(60) NOT NULL,
  `apellidos_estudiante` varchar(60) NOT NULL,
  `codigo_personal_estudiante` varchar(15) DEFAULT NULL,
  `fecha_nacimiento_estudiante` date NOT NULL,
  `genero_estudiante` varchar(2) NOT NULL,
  `direccion_estudiante` varchar(100) DEFAULT NULL COMMENT 'Campo para almacenar los datos de la dirección de residencia del estudiante',
  `colonia_estudiante` varchar(100) DEFAULT NULL COMMENT 'Campo para almacenar el nombre de la colonia donde vive el estudiante',
  `zona_estudiante` varchar(30) DEFAULT NULL COMMENT 'almacena la zona donde reside el estudiante',
  `telefono_casa` varchar(15) DEFAULT NULL COMMENT 'campo para registrar el numero de telefono de casa del estudiante, puede cer un numero de celular',
  `empresa_telefonica` varchar(15) DEFAULT NULL COMMENT 'Campo para almacenar el nombre de la empresa telefonica de sus número de su telefono',
  `correo_estudiante` varchar(70) DEFAULT NULL COMMENT 'Campo para almacenar la dirección de correo electronico personal del estudiante',
  `enfermedad_padecida_estudiante` varchar(1000) DEFAULT NULL COMMENT 'Campo para registrar los padecimientos de enfermedades que tenga el estudiante',
  `medicamento_recomendado_estudiante` varchar(1000) DEFAULT NULL COMMENT 'Campo para almacenar los medicamentos recomendados del estudiante',
  `alergico_estudiante` varchar(500) DEFAULT NULL COMMENT 'campo para almacenar los comportamientos alergicos que tenga el estudiante',
  `hospital_estudiante` varchar(100) DEFAULT NULL COMMENT 'Campo para almacenar el hospital sugerido para llevar al estudiante en caso de emergencias',
  `tipo_sangre_estudiante` varchar(30) DEFAULT NULL COMMENT 'Campo para almacenar el tipo de sangre del estudiante',
  `observaciones_estudiante` varchar(1000) DEFAULT NULL COMMENT 'almacena algunas observaciones extras a la hora de inscribir a un estudiante',
  `fecha_registro_estudiante` date NOT NULL,
  `estado_estudiante` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_estudiante`),
  KEY `municipio_estudiantes_fk` (`id_municipio`),
  KEY `usuarios_estudiantes_fk` (`id`),
  KEY `asignacion_areas_estudiantes_fk_idx` (`id_nivel_grado`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los datos de los estudiantes' AUTO_INCREMENT=38 ;

--
-- Volcado de datos para la tabla `ESTUDIANTES`
--

INSERT INTO `ESTUDIANTES` (`id_estudiante`, `id`, `id_nivel_grado`, `id_municipio`, `nombre_estudiante`, `apellidos_estudiante`, `codigo_personal_estudiante`, `fecha_nacimiento_estudiante`, `genero_estudiante`, `direccion_estudiante`, `colonia_estudiante`, `zona_estudiante`, `telefono_casa`, `empresa_telefonica`, `correo_estudiante`, `enfermedad_padecida_estudiante`, `medicamento_recomendado_estudiante`, `alergico_estudiante`, `hospital_estudiante`, `tipo_sangre_estudiante`, `observaciones_estudiante`, `fecha_registro_estudiante`, `estado_estudiante`) VALUES
(6, 17, 1, 87, 'José Fernando', 'Marino López', '', '1991-11-14', 'M', 'zona 3 Guatemala', 'mi colonia', 'zona 3', '45935678', 'tigo', 'jose-marino@prueba.com', 'Ninguno', 'Ninguno', 'Ninguno', 'Igss', 'A+', 'Ninguna', '2016-08-01', 1),
(8, 21, 4, 87, 'Juan Jorge', 'Paredes Sosa', '', '2000-07-14', 'M', 'zona 5 Guatemala', 'mi colonia', 'zona 5', '12345678', 'claro', 'juan-paredes1@prueba.com', 'Ninguno', 'Ninguno', 'Ninguno', 'Hospital nacional', 'A+', 'Ninguna', '2016-08-04', 1),
(9, 24, 1, 185, 'Herbert', 'Bungeroth', '', '2004-02-04', 'M', '6ta calle 0-65 zona 2', 'colonia san bernadro', 'zona 2', '99889907', 'tigo', 'herbert@gmail.com', 'ninguno', 'ninguno', 'ninguno', 'Ninguno', 'A-', 'ninguna', '2016-08-12', 1),
(10, 27, 3, 165, 'pedro', 'flores', '', '2016-08-19', 'M', 'flores', 'san isidro', 'zona 4', '78787878', 'tigo', 'flores@gmail.com', 'ninguno', 'ninguno', 'ninguna', 'Roosvel', 'NO SABE', 'ningua', '2016-08-19', 1),
(11, 30, 15, 147, 'Luis', 'Gomez', '', '1992-07-15', 'M', '5ta. calle a 6-69', 'Callejon Castillo', 'zona 3', '45127845', 'claro', 'micorreo@gmail.com', 'hola', 'hola', 'ninguna', 'bienvenidos', 'A-', 'ninguna', '2016-08-24', 1),
(12, 32, 16, 159, 'Perla', 'Donado', '', '2001-06-13', 'F', 'calle la castañaza 9-69 ', 'la almendra', 'zona 3', '45562354', 'tigo', 'donado@perla.com', 'ninguna', 'ninguna', 'ningua', 'paso al infierno', 'B+', 'ninguna', '2016-08-24', 1),
(13, 34, 16, 170, 'Pedro', 'Arrivillaga', '', '2001-06-12', 'M', 'Benito Camela', 'la ñola', 'zona 8', '45127845', 'claro', 'benito@gmail.com', 'ninguno', 'niguno', 'ninguno', 'Paso al Infierno', 'B-', 'Hola', '2016-08-25', 1),
(14, 36, 16, 225, 'alguien', 'alguno', '01010', '2016-08-25', 'M', 'Ninguna', 'ninguna', 'zona 1', '30154416', 'movistar', 'ninguno@gmail.com', 'ninguno', 'ninguno', 'ninguno', 'ninguno', 'A-', 'ninguna', '2016-08-25', 1),
(15, 38, 9, 222, 'hola', 'mundo', 'hola', '2001-02-07', 'M', 'hola', 'hola', 'zona 9', '45125623', 'claro', 'nignuna@gmail.com', 'nignuna', 'nignuna', 'nignuna', 'nignuna', 'B+', 'nignuna', '2016-08-25', 1),
(16, 40, 16, 189, 'ninguno', 'ninguno', '', '2001-06-14', 'M', 'ninguno', 'ninguno', 'zona 0', '45124578', 'claro', 'ninguno@gmail.com', 'ninguno', 'ninguno', 'ninguno', 'ninguno', 'A+', 'ninguno', '2016-08-25', 1),
(23, 53, 16, 242, 'constancia', 'constancia', '', '2011-01-04', 'M', 'constancia', 'constancia', 'zona 9', '45455454', 'claro', 'constancia@gmail.com', 'constancia', 'constancia', 'constancia', 'constancia', 'A+', 'constancia', '2016-08-25', 1),
(37, 73, 16, 242, 'pruebaaa', 'pruebaaa', 'pruebaaa', '2016-06-23', 'M', 'pruebaaa', 'pruebaaa', 'zona 10', '45124578', 'claro', 'pruebaaa@gmail.com', 'pruebaaa', 'pruebaaa', 'pruebaaa', 'pruebaaa', 'B+', 'pruebaaa', '2016-08-25', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `FOROS`
--

CREATE TABLE IF NOT EXISTS `FOROS` (
  `id_foro` int(11) NOT NULL AUTO_INCREMENT,
  `id_asignacion_area` int(11) NOT NULL,
  `titulo_foro` varchar(40) NOT NULL,
  `mensaje_foro` varchar(500) NOT NULL,
  `fecha_foro` datetime NOT NULL,
  `id_usuario` bigint(20) NOT NULL,
  PRIMARY KEY (`id_foro`),
  UNIQUE KEY `id_usuario_foros_fk` (`id_usuario`),
  KEY `asignacion_areas_foros_fk` (`id_asignacion_area`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los foros creados para un curso' AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `FOROS`
--

INSERT INTO `FOROS` (`id_foro`, `id_asignacion_area`, `titulo_foro`, `mensaje_foro`, `fecha_foro`, `id_usuario`) VALUES
(1, 13, 'Sobre el curso', 'Hola, que tal a todos, este curso se trata sobre el manejo de algunas herramientas ofimáticas...', '2016-08-10 17:08:26', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `GRADOS`
--

CREATE TABLE IF NOT EXISTS `GRADOS` (
  `id_grado` tinyint(4) NOT NULL AUTO_INCREMENT,
  `nombre_grado` varchar(40) NOT NULL,
  `fecha_registro_grado` datetime NOT NULL,
  `estado_grado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_grado`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los datos de los grados disponibles en el establecimiento' AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `GRADOS`
--

INSERT INTO `GRADOS` (`id_grado`, `nombre_grado`, `fecha_registro_grado`, `estado_grado`) VALUES
(1, 'párvulos', '2016-06-27 00:00:00', 1),
(2, 'preparatoria', '2016-06-27 00:00:00', 1),
(3, 'primero', '2016-06-27 00:00:00', 1),
(4, 'segundo', '2016-06-27 00:00:00', 1),
(5, 'tercero', '2016-06-27 00:00:00', 1),
(6, 'cuarto', '2016-06-27 00:00:00', 1),
(7, 'quinto', '2016-06-27 00:00:00', 1),
(8, 'sexto', '2016-06-27 00:00:00', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `INTITUCION`
--

CREATE TABLE IF NOT EXISTS `INTITUCION` (
  `id_institucion` tinyint(4) NOT NULL AUTO_INCREMENT,
  `nombre_institucion` varchar(100) NOT NULL,
  `direccion_institucion` varchar(100) NOT NULL,
  `logo_institucion` varchar(100) NOT NULL,
  `telefono_uno_institucion` varchar(10) NOT NULL,
  `telefono_dos_institucion` varchar(10) DEFAULT NULL,
  `web_institucion` varchar(50) DEFAULT NULL,
  `correo_institucion` varchar(60) DEFAULT NULL,
  `facebook_institucion` varchar(50) DEFAULT NULL,
  `twiter_institucion` varchar(50) DEFAULT NULL,
  `instagram_institucion` varchar(50) DEFAULT NULL,
  `id_descripcion` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_institucion`),
  KEY `descripcion_intitucion_fk` (`id_descripcion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los datos de la institución educativa' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `JORNADAS`
--

CREATE TABLE IF NOT EXISTS `JORNADAS` (
  `id_jornada` tinyint(4) NOT NULL AUTO_INCREMENT,
  `nombre_jornada` varchar(30) NOT NULL,
  `estado_jornada` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_jornada`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar las jornadas disponibles en el establecimiento ej(vespertino, matutino)' AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `JORNADAS`
--

INSERT INTO `JORNADAS` (`id_jornada`, `nombre_jornada`, `estado_jornada`) VALUES
(1, 'matutina', 1),
(2, 'vespertina', 1),
(3, 'nocturna', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `MUNICIPIO`
--

CREATE TABLE IF NOT EXISTS `MUNICIPIO` (
  `id_municipio` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_municipio` varchar(70) DEFAULT NULL COMMENT 'Campo para alamacenar el nombre del municipio',
  `id_departamento` smallint(6) DEFAULT NULL COMMENT 'Campo para almacenar el identificador unico de cada registro',
  PRIMARY KEY (`id_municipio`),
  KEY `departamento_municipio_fk` (`id_departamento`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar todos los nombres de los municipios del país de guatemala.' AUTO_INCREMENT=350 ;

--
-- Volcado de datos para la tabla `MUNICIPIO`
--

INSERT INTO `MUNICIPIO` (`id_municipio`, `nombre_municipio`, `id_departamento`) VALUES
(15, 'cobán', 1),
(16, 'chisec', 1),
(17, 'chahal', 1),
(18, 'fray bartolomé de las casas', 1),
(19, 'lanquín', 1),
(20, 'panzós', 1),
(21, 'tamahú', 1),
(22, 'tucurú', 1),
(23, 'tactic', 1),
(24, 'santa maria cahabón', 1),
(25, 'senahú', 1),
(26, 'san cristóbal verapaz', 1),
(27, 'san juan chamelco', 1),
(28, 'san pedro carchá', 1),
(29, 'santa cruz verapaz', 1),
(30, 'santa catalina la tinta', 1),
(31, 'raxruhá', 1),
(32, 'cubulco', 2),
(33, 'santa cruz el chol', 2),
(34, 'granados', 2),
(35, 'purulhá', 2),
(36, 'rabinal', 2),
(37, 'salamá', 2),
(38, 'san miguel chicaj', 2),
(39, 'san jerónimo', 1),
(40, 'chimaltenango', 3),
(41, 'san jose poaquil', 3),
(42, 'san martin jilotepeque', 3),
(43, 'san juan comalapa', 3),
(44, 'santa apolonia', 3),
(45, 'sanata cruz balanyá', 3),
(46, 'tecpán', 3),
(47, 'patzún', 3),
(48, 'pochuta', 3),
(49, 'patzicía', 3),
(50, 'el tejar', 3),
(51, 'parramos', 3),
(52, 'acatenango', 3),
(53, 'yepocapa', 3),
(54, 'san andres itzapa', 3),
(55, 'zaragoza', 3),
(56, 'chiquimula', 4),
(57, 'camotan', 4),
(58, 'concepción las minas', 4),
(59, 'esquipulas', 4),
(60, 'ipala', 4),
(61, 'jocotan', 4),
(62, 'olopa', 4),
(63, 'quetzaltepeque', 4),
(64, 'san josé la arada', 4),
(65, 'san juan ermita', 4),
(66, 'san jacinto', 4),
(67, 'guastatoya', 5),
(68, 'morazán', 5),
(69, 'el jicaro', 5),
(70, 'san agustín acasaguastlán', 5),
(71, 'san antonio la paz', 5),
(72, 'sanarate', 5),
(73, 'sansare', 5),
(74, 'escuintla', 6),
(75, 'guanagazapa', 6),
(76, 'iztapa', 6),
(77, 'la democracia', 6),
(78, 'la gomera', 6),
(79, 'masagua', 6),
(80, 'nueva concepción', 6),
(81, 'palín', 6),
(82, 'san jośe', 6),
(83, 'san vicente pacaya', 6),
(84, 'santa lucia cotzumalguapa', 6),
(85, 'siquinalá', 6),
(86, 'tiquisate', 6),
(87, 'guatemala', 7),
(88, 'santa catarina pinula', 7),
(89, 'san josé pinula', 7),
(90, 'palencia', 7),
(91, 'chinautla', 7),
(92, 'san pedro ayampuc', 7),
(93, 'mixco', 7),
(94, 'san pedro sacatepéquez', 7),
(95, 'san juan sacatepéquez', 7),
(96, 'chuarrancho', 7),
(97, 'villa nueva', 7),
(98, 'villa canales', 7),
(99, 'amatitlán', 7),
(100, 'fraijanes', 7),
(101, 'san miguel petapa', 7),
(102, 'san raymundo', 7),
(103, 'aguacatán', 8),
(104, 'chiantla', 8),
(105, 'colotenango', 8),
(106, 'concepción huista', 8),
(107, 'cuilco', 8),
(108, 'huehuetenango', 8),
(109, 'jacaltenango', 8),
(110, 'la democracia', 8),
(111, 'la libertad', 8),
(112, 'malacatancito', 8),
(113, 'nentón', 8),
(114, 'san antonio huista', 8),
(115, 'san gaspar ixchil', 8),
(116, 'san ildefonso ixtahuacán', 8),
(117, 'san juan atitán', 8),
(118, 'san juan ixcoy', 8),
(119, 'san mateo ixtatán', 8),
(120, 'san miguel acatán', 8),
(121, 'san pedro necta', 8),
(122, 'san pedro soloma', 8),
(123, 'san rafael la independencia', 8),
(124, 'san rafael petzal', 8),
(125, 'san sebastián coatán', 8),
(126, 'san sebastián huehhuetenango', 8),
(127, 'santa ana huista', 8),
(128, 'santa bárbara', 8),
(129, 'santa cruz barillas', 8),
(130, 'santa eulalia', 8),
(131, 'santiago chimaltenango', 8),
(132, 'tectitán', 8),
(133, 'todos santos cuchumatán', 8),
(134, 'union cantinil', 8),
(135, 'puerto barrios', 9),
(136, 'el estor', 9),
(137, 'livingston', 9),
(138, 'los amates', 9),
(139, 'morales', 9),
(140, 'jalapa', 10),
(141, 'mataquescuintla', 10),
(142, 'monjas', 10),
(143, 'san pedro pinula', 10),
(144, 'san luis jilotepeque', 10),
(145, 'san manuel chaparrón', 10),
(146, 'san carlos alzatate', 10),
(147, 'jutiapa', 11),
(148, 'agua blanca', 11),
(149, 'asunción mita', 11),
(150, 'atescatempa', 11),
(151, 'comapa', 11),
(152, 'conguaco', 11),
(153, 'el adelantado', 11),
(154, 'el progeso', 11),
(155, 'jalpatagua', 11),
(156, 'jerez', 11),
(157, 'moyuta', 11),
(158, 'pasaco', 11),
(159, 'quesada', 11),
(160, 'san jóse acatempa', 11),
(161, 'santa catarina mita', 11),
(162, 'yupiltepeque', 11),
(163, 'zapotitlan', 11),
(164, 'dolores', 12),
(165, 'flores', 12),
(166, 'la libertad', 12),
(167, 'melchor de mencos', 12),
(168, 'poptún', 12),
(169, 'san andres', 12),
(170, 'san benito', 12),
(171, 'san francisco', 12),
(172, 'san josé', 12),
(173, 'san luis', 12),
(174, 'santa ana', 12),
(175, 'sayaxché', 12),
(176, 'quetzaltenango', 13),
(177, 'almolonga', 13),
(178, 'cabricán', 13),
(179, 'cajolá', 13),
(180, 'cantel', 13),
(181, 'coatepeque', 13),
(182, 'colomba', 13),
(183, 'concepción chiquirichapa', 13),
(184, 'el palmar', 13),
(185, 'flores costa cuca', 13),
(186, 'génova', 13),
(187, 'huitán', 13),
(188, 'la esperanza', 13),
(189, 'olintepeque', 13),
(190, 'san juan ostuncalco', 13),
(191, 'palestina del los altos', 13),
(192, 'salcajá', 13),
(193, 'san carlos sija', 13),
(194, 'san francisco la unión', 13),
(195, 'san martín sacatepéquez', 13),
(196, 'san mateo', 13),
(197, 'san miguel sigüilá', 13),
(198, 'sibilia', 13),
(199, 'zunil', 13),
(200, 'santa cruz del quiché', 14),
(201, 'canillá', 14),
(202, 'chajul', 14),
(203, 'chicamán', 14),
(204, 'chiché', 14),
(205, 'chichicastenango', 14),
(206, 'chinique', 14),
(207, 'cunén', 14),
(208, 'ixcán', 14),
(209, 'joyabaj', 14),
(210, 'nebaj', 14),
(211, 'pachalum', 14),
(212, 'patzité', 14),
(213, 'sacapulas', 14),
(214, 'san andrés sajcabajá', 14),
(215, 'san antonio ilotenango', 14),
(216, 'san bartolomé jocotenango', 14),
(217, 'san juan cotzal', 14),
(218, 'san pedro jocopilas', 14),
(219, 'uspantán', 14),
(220, 'zacualpa', 14),
(221, 'retalhuleu', 15),
(222, 'champerico', 15),
(223, 'el asintal', 15),
(224, 'nuevo san carlos', 15),
(225, 'san andrés villa seca', 15),
(226, 'san martín zapotitlan', 15),
(227, 'san felipe', 15),
(228, 'san sebastián', 15),
(229, 'santa cruz muluá', 15),
(230, 'antigua guatemala', 16),
(231, 'alotenango', 16),
(232, 'ciudad vieja', 16),
(233, 'jocotenango', 16),
(234, 'magdalena milpas altas', 16),
(235, 'pastores', 16),
(236, 'san antonio aguas calientes', 16),
(237, 'san bartolomé milpas altas', 16),
(238, 'san lucas sacatepéquez', 16),
(239, 'san miguel dueñas', 16),
(240, 'santa catarina barahona', 16),
(241, 'santa lucia milpas latas', 16),
(242, 'santa maria de jesús', 16),
(243, 'santiago sacatepéquez', 16),
(244, 'santo domingo xenacoj', 16),
(245, 'sumpango', 16),
(246, 'san marcos', 17),
(247, 'ayutla', 17),
(248, 'catarina', 17),
(249, 'comitancillo', 17),
(250, 'concepción tutuapa', 17),
(251, 'el quetzal', 17),
(252, 'el rodeo', 17),
(253, 'el tumbador', 17),
(254, 'esquipulas palo gordo', 17),
(255, 'ixchiguán', 17),
(256, 'la reforma', 17),
(257, 'malacatán', 17),
(258, 'nuevo progreso', 17),
(259, 'ocós', 17),
(260, 'pajapita', 17),
(261, 'río blanco', 17),
(262, 'san antonio sacatepéquez', 17),
(263, 'san cristóbal cucho', 17),
(264, 'san josé ojetenam', 17),
(265, 'lorenzo', 17),
(266, 'san miguel ixtahuacán', 17),
(267, 'san pablo', 17),
(268, 'san pedro sacatépequez', 17),
(269, 'san rafaél pie de la cuesta', 17),
(270, 'sibinal', 17),
(271, 'sipacapa', 17),
(272, 'tacaná', 17),
(273, 'tajumulco', 17),
(274, 'tejutla', 17),
(275, 'la blanca', 17),
(276, 'cuilapa', 18),
(277, 'casillas', 18),
(278, 'chiquimulilla', 18),
(279, 'guazacapán', 18),
(280, 'nueva santa rosa', 18),
(281, 'oratorio', 18),
(282, 'pueblo nuevo viñas', 18),
(283, 'san juan tecuaco', 18),
(284, 'san rafaél las flores', 18),
(285, 'santa cruz naranjo', 18),
(286, 'santa maría ixhuatán', 18),
(287, 'santa rosa de lima', 18),
(288, 'taxisco', 18),
(289, 'barberena', 18),
(290, 'sololá', 19),
(291, 'concepción', 19),
(292, 'nahualá', 19),
(293, 'panajachel', 19),
(294, 'san andres semetabaj', 19),
(295, 'san antonio palopó', 19),
(296, 'san josé chacayá', 19),
(297, 'san juan la laguna', 19),
(298, 'san lucas tolimán', 19),
(299, 'san marcos la laguna', 19),
(300, 'san pablo la laguna', 19),
(301, 'san pedro la laguna', 19),
(302, 'santa catarina ixtahuacan', 19),
(303, 'santa catarina palopó', 19),
(304, 'santa catarina palopó', 19),
(305, 'santa clara la laguna', 19),
(306, 'santa cruz la laguna', 19),
(307, 'santa lucía utatlán', 19),
(308, 'santa maría visitación', 19),
(309, 'santiago atitlán', 19),
(310, 'mazatenango', 20),
(311, 'chicacao', 20),
(312, 'cuyotenengo', 20),
(313, 'patulul', 20),
(314, 'pueblo nuevo', 20),
(315, 'río bravo', 20),
(316, 'samayac', 20),
(317, 'san antonio suchitepéquez', 20),
(318, 'san bernardino', 20),
(319, 'san josé el ídolo', 20),
(320, 'san francisco zapotitlán', 20),
(321, 'san gabriel', 20),
(322, 'san juan bautista', 20),
(323, 'san lorenzo', 20),
(324, 'san miguel panán', 20),
(325, 'san pablo jocopilas', 20),
(326, 'santa bárbara', 20),
(327, 'santo domingo suchitepéquez', 20),
(328, 'santo tomas la unión', 20),
(329, 'zunilito', 20),
(330, 'san jose la maquina', 20),
(331, 'totonicapán', 21),
(332, 'momostenango', 21),
(333, 'san andrés xecul', 21),
(334, 'san bartolo', 21),
(335, 'san cristobal totonicapán', 21),
(336, 'san francisco el alto', 21),
(337, 'santa lucía la reforma', 21),
(338, 'santa maría cuiquimula', 21),
(339, 'cabañas', 22),
(340, 'estanzuela', 22),
(341, 'gualán', 22),
(342, 'huité', 22),
(343, 'la unión', 22),
(344, 'río hondo', 22),
(345, 'san diego', 22),
(346, 'san jorge', 22),
(347, 'teculután', 22),
(348, 'usumatlán', 22),
(349, 'zacapa', 22);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `NIVELES`
--

CREATE TABLE IF NOT EXISTS `NIVELES` (
  `id_nivel` tinyint(4) NOT NULL AUTO_INCREMENT,
  `nombre_nivel` varchar(30) NOT NULL,
  `fecha_registro_nivel` date NOT NULL,
  `estado_nivel` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_nivel`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los niveles disponibles en el establecimiento educativo' AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `NIVELES`
--

INSERT INTO `NIVELES` (`id_nivel`, `nombre_nivel`, `fecha_registro_nivel`, `estado_nivel`) VALUES
(1, 'pre-primaria', '2016-06-27', 1),
(2, 'primaria', '2016-06-27', 1),
(3, 'básico', '2016-06-27', 1),
(4, 'diversificado', '2016-06-27', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `NIVELES_GRADOS`
--

CREATE TABLE IF NOT EXISTS `NIVELES_GRADOS` (
  `id_nivel_grado` int(11) NOT NULL AUTO_INCREMENT,
  `id_grado` tinyint(4) NOT NULL,
  `id_seccion` tinyint(4) NOT NULL,
  `id_nivel_plan_jornada` int(11) NOT NULL,
  `id_carrera` int(11) NOT NULL,
  `cuota_inscripcion` decimal(5,2) DEFAULT NULL COMMENT 'almacena el registro de la cuota de inscripción de grado',
  `cuota_mensualidad` decimal(5,2) DEFAULT NULL COMMENT 'almacena la cuota de mensualidad del grado',
  `estado_nivel_grado` tinyint(1) NOT NULL,
  `pensum_nivel_grado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_nivel_grado`),
  KEY `carreras_niveles_grados_fk` (`id_carrera`),
  KEY `secciones_niveles_grados_fk` (`id_seccion`),
  KEY `grados_niveles_grados_fk` (`id_grado`),
  KEY `niveles_planes_jornadas_niveles_grados_fk` (`id_nivel_plan_jornada`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar las relaciones de niveles con grados' AUTO_INCREMENT=17 ;

--
-- Volcado de datos para la tabla `NIVELES_GRADOS`
--

INSERT INTO `NIVELES_GRADOS` (`id_nivel_grado`, `id_grado`, `id_seccion`, `id_nivel_plan_jornada`, `id_carrera`, `cuota_inscripcion`, `cuota_mensualidad`, `estado_nivel_grado`, `pensum_nivel_grado`) VALUES
(1, 3, 1, 1, 1, '375.00', '295.00', 1, 0),
(2, 4, 1, 1, 1, '375.00', '325.00', 1, 0),
(3, 5, 1, 1, 1, NULL, NULL, 1, 0),
(4, 6, 1, 3, 2, NULL, NULL, 1, 0),
(5, 7, 1, 3, 2, NULL, NULL, 1, 0),
(6, 6, 1, 3, 3, NULL, NULL, 1, 0),
(7, 7, 1, 3, 3, NULL, NULL, 1, 0),
(8, 6, 1, 3, 4, NULL, NULL, 1, 0),
(9, 7, 1, 4, 4, NULL, NULL, 1, 0),
(10, 6, 1, 3, 5, NULL, NULL, 1, 0),
(11, 7, 1, 3, 5, NULL, NULL, 1, 0),
(12, 6, 1, 3, 6, NULL, NULL, 1, 0),
(13, 7, 1, 3, 6, NULL, NULL, 1, 0),
(14, 8, 1, 3, 6, NULL, NULL, 1, 0),
(15, 3, 2, 1, 1, '999.99', '100.00', 1, 0),
(16, 4, 2, 1, 1, '500.00', '500.00', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `NIVELES_PLANES_JORNADAS`
--

CREATE TABLE IF NOT EXISTS `NIVELES_PLANES_JORNADAS` (
  `id_nivel_plan_jornada` int(11) NOT NULL AUTO_INCREMENT,
  `id_nivel` tinyint(4) NOT NULL,
  `id_jornada` tinyint(4) NOT NULL,
  `id_plan` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_nivel_plan_jornada`),
  KEY `jornadas_niveles_planes_jornadas_fk` (`id_jornada`),
  KEY `planes_niveles_planes_jornadas_fk` (`id_plan`),
  KEY `niveles_niveles_planes_jornadas_fk` (`id_nivel`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar las relaciones ntre niveles, jornadas y planes.' AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `NIVELES_PLANES_JORNADAS`
--

INSERT INTO `NIVELES_PLANES_JORNADAS` (`id_nivel_plan_jornada`, `id_nivel`, `id_jornada`, `id_plan`) VALUES
(1, 3, 1, 1),
(2, 3, 1, 2),
(3, 4, 1, 1),
(4, 4, 1, 2),
(5, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `OPCIONES`
--

CREATE TABLE IF NOT EXISTS `OPCIONES` (
  `id_opcion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_opcion` varchar(50) NOT NULL,
  `contenedor_principal` smallint(6) NOT NULL,
  `ruta_opcion` varchar(50) NOT NULL,
  `estado_opcion` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_opcion`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los nombres de las opciones del sistema' AUTO_INCREMENT=22 ;

--
-- Volcado de datos para la tabla `OPCIONES`
--

INSERT INTO `OPCIONES` (`id_opcion`, `nombre_opcion`, `contenedor_principal`, `ruta_opcion`, `estado_opcion`) VALUES
(1, 'carreras', 1, 'carreras', 1),
(2, 'niveles', 1, 'niveles', 1),
(3, 'grados', 1, 'grados', 1),
(4, 'planes niveles', 1, 'asignarniveles', 1),
(5, 'grados niveles', 1, 'gradoniveles', 1),
(6, 'usuario', 2, 'usuarios', 1),
(7, 'roles', 2, 'roles', 1),
(8, 'empleados', 3, 'personas', 1),
(9, 'puestos', 3, 'puestos', 1),
(10, 'secciones', 1, 'secciones', 1),
(11, 'jornadas', 1, 'jornadas', 1),
(12, 'salones', 1, 'salones', 1),
(13, 'planes', 1, 'planes', 1),
(14, 'cursos', 1, 'areas', 1),
(15, 'pensum grados', 1, 'pensum', 1),
(16, 'asignacion docente', 4, 'asignaciondocente', 1),
(17, 'inscripcion estudiantes', 5, 'inscripcionestudiantes', 1),
(18, 'unidades', 1, 'unidades', 1),
(19, 'Mis Cursos', 4, 'docente', 1),
(20, 'Buscar Estudiantes', 5, 'verestudiantes', 1),
(21, 'Buscar Empleados', 3, 'verusuario', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `id_opcion` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `opciones_permissions_fk` (`id_opcion`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los permisos disponibles del sistema.' AUTO_INCREMENT=64 ;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`, `id_opcion`) VALUES
(15, 'crear-carrera', 'crear carrera', 'Registra una nuevo carrera en el sistema.', '2016-06-21 15:00:12', NULL, 1),
(16, 'editar-carrera', 'editar carrera', 'Edita los datos de una carrera.', '2016-06-21 15:00:12', NULL, 1),
(17, 'estado-carrera', 'estado carrera', 'Cambia el estado de una carrera habilitado/deshabilitado', '2016-06-21 15:00:12', NULL, 1),
(18, 'crear-nivel', 'crear nivel', 'Registra un nuevo nivel en el sistema.', '2016-06-21 15:00:12', NULL, 2),
(19, 'editar-nivel', 'editar nivel', 'Cambia los datos referentes a un nivel.', '2016-06-21 15:00:12', NULL, 2),
(20, 'estado-nivel', 'estado nivel', 'Cambia el estado de un nivel de habilitado a deshabilitado', '2016-06-21 15:00:12', NULL, 2),
(21, 'crear-grado', 'crear grado', 'Permiso para registrar un grado en el sistema', '2016-06-21 15:00:12', NULL, 3),
(22, 'editar-grado', 'editar grado', 'Permiso para editar los datos de un grado registrado en el sistema.', '2016-06-21 15:00:12', NULL, 3),
(23, 'estado-grado', 'estado grado', 'Permiso para cambiar el estado de un grado habilitado/deshabiitado', '2016-06-21 15:00:12', NULL, 3),
(24, 'crear-plannivel', 'crear plan nivel', 'Permiso para asignar un nivel a un plan.', '2016-06-21 15:00:12', NULL, 4),
(25, 'editar-plannivel', 'editar plan nivel', 'Permiso para editar la aisgnación de un nivel a un plan.', '2016-06-21 15:00:12', NULL, 4),
(26, 'crear-gradonivel', 'crear grado nivel', 'Permiso para asignar un grado a un nivel', '2016-06-21 15:00:12', NULL, 5),
(27, 'editar-gradonivel', 'editar grado nivel', 'Permisos para editar la sisgnación de un grado a un nivel', '2016-06-21 15:00:12', NULL, 5),
(28, 'estado-gradonivel', 'estado grado nivel', 'Permiso para habilitar/deshabilitar una asignacion de grado a un nivel', '2016-06-21 15:00:12', NULL, 5),
(29, 'crear-seccion', 'crear sección', 'Permiso para registrar una nueva sección en el sistema.', '2016-06-21 15:00:12', NULL, 10),
(30, 'editar-seccion', 'editar sección', 'Permiso para editar los datos de una sección.', '2016-06-21 15:00:12', NULL, 10),
(31, 'crear-jornada', 'crear jornada', 'Permiso para registrar una nueva jornada en el sistema.', '2016-06-21 15:00:12', NULL, 11),
(32, 'editar-jornada', 'editar jornada', 'Permiso para editar los datos de una jornada', '2016-06-21 15:00:12', NULL, 11),
(33, 'estado-jornada', 'estado jornada', 'Permiso para cambiar el estado de una jornada habilitado/deshabilitado.', '2016-06-21 15:00:12', NULL, 11),
(34, 'crear-salon', 'crear salón', 'Permiso para registrar un nuevo salón en el sistema.', '2016-06-21 15:00:12', NULL, 12),
(35, 'editar-salon', 'editar salón', 'Permiso para editar los datos de un salón', '2016-06-21 15:00:12', NULL, 12),
(36, 'estado-salon', 'estado salón', 'Permiso para cambiar el estado de un salón habilitado/deshabilitado.', '2016-06-21 15:00:12', NULL, 12),
(37, 'crear-plan', 'crear plan', 'Permiso para registrar un nuevo plan en el sistema.', '2016-06-21 15:00:12', NULL, 13),
(38, 'editar-plan', 'editar plan', 'Permiso para editar los datos de un plan.', '2016-06-21 15:00:12', NULL, 13),
(39, 'estado-plan', 'estado plan', 'Permiso para cambiar el estado de un plan habilitado/deshabilitado.', '2016-06-21 15:00:12', NULL, 13),
(40, 'crear-curso', 'crear curso', 'Permiso para registrar un nuevo curso en el sistema', '2016-06-21 15:00:12', NULL, 14),
(41, 'editar-curso', 'editar curso', 'Permiso para editar los datos de un curso', '2016-06-21 15:00:12', NULL, 14),
(42, 'estado-curso', 'estado curso', 'Permiso para cambiar el estado de un cruso habilitado/deshabilitado.', '2016-06-21 15:00:12', NULL, 14),
(43, 'crear-pensum', 'crear pensum', 'Permiso para crear un nuevo pensum en el sistema', '2016-06-21 15:00:12', NULL, 15),
(44, 'editar-pensum', 'editar pensum', 'Permiso para agregar nuevos cursos o dehabilitar/habilitar a un pensum existente.', '2016-06-21 15:00:12', NULL, 15),
(45, 'ver-pensum', 'ver pensum', 'Permiso para ver todos los cursos asignados a un pensum.', '2016-06-21 15:00:12', NULL, 15),
(46, 'crear-usuario', 'crear usuario', 'Permiso para registrar un nuevo usuario en el sistema', '2016-06-21 15:00:12', NULL, 6),
(47, 'editar-usuario', 'editar usuario', 'Permiso para cambiar el rol de un usuario.', '2016-06-21 15:00:12', NULL, 6),
(48, 'crear-rol', 'crear rol', 'Permiso para registrar un nuevo rol y agregarle los permisos', '2016-06-21 15:00:12', NULL, 7),
(49, 'editar-rol', 'editar rol', 'Permiso para agregar más permisos a un rol o deshabilitar un rol asignado.', '2016-06-21 15:00:12', NULL, 7),
(50, 'estado-rol', 'estado rol', 'Permiso para cambiar el estado de un rol habilitado/deshabilitado-', '2016-06-21 15:00:12', NULL, 7),
(51, 'crear-empleado', 'crear empleado', 'Permiso para registrar un nuevo empleado dentro del sistema.', '2016-06-21 15:00:12', NULL, 8),
(52, 'editar-empleado', 'editar impleado', 'Permiso para editar los datos de un empleado', '2016-06-21 15:00:12', NULL, 8),
(53, 'estado-empleado', 'estado empleado', 'Permiso para cambiar el estado de un empleado habilitado/deshabilitado', '2016-06-21 15:00:12', NULL, 8),
(54, 'ver-persona', 'ver datos person', 'Permiso para ver todos los datos de un empleado', '2016-06-21 15:00:12', NULL, 8),
(55, 'crear-puesto', 'crear puesto', 'Permiso para crear un nuevo puesto.', '2016-06-21 15:00:12', NULL, 9),
(56, 'editar-puesto', 'editar puesto', 'Permiso para editar los datos de un puesto', '2016-06-21 15:00:12', NULL, 9),
(57, 'estado-puesto', 'estado puesto', 'Permiso para cambiar el estado de un puesto habilitado/deshabilitado', '2016-06-21 15:00:12', NULL, 9),
(58, 'crear-asignacion', 'crear asignación', 'Crea una nueva asignación de cursos a un docente', '2016-06-21 15:00:12', NULL, 16),
(59, 'crear-inscripcion', 'crear  inscripción', 'Crear una nueva inscripción de un estudiante en un grado de un determinado nivel , jornada y plan', '2016-06-21 15:00:12', NULL, 17),
(60, 'crear-unidad', 'crear unidad', 'Permiso para crear unidad', '2016-06-21 15:00:12', NULL, 18),
(61, 'editar-unidad', 'editar unidad', 'Permiso para editar los datos de una unidad', '2016-06-21 15:00:12', NULL, 18),
(62, 'crear-zona', 'crear zona', 'Permiso para el docente, de que pueda crear y asignar puntos para la zona', '2016-06-21 15:00:12', NULL, 19),
(63, 'ver-inscripcion', 'ver inscripcion', 'Permiso para ver todos los alumnos inscritos', '2016-08-19 09:00:12', NULL, 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `id_permission_role` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `state_permission_role` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_permission_role`),
  KEY `roles_permission_role_fk` (`role_id`),
  KEY `permissions_permission_role_fk` (`permission_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para relacionar los permisos con los roles' AUTO_INCREMENT=119 ;

--
-- Volcado de datos para la tabla `permission_role`
--

INSERT INTO `permission_role` (`id_permission_role`, `role_id`, `permission_id`, `state_permission_role`) VALUES
(1, 1, 15, 1),
(2, 1, 16, 1),
(3, 1, 17, 1),
(4, 1, 18, 1),
(5, 1, 19, 1),
(6, 1, 20, 1),
(7, 1, 21, 1),
(8, 1, 22, 1),
(9, 1, 23, 1),
(10, 1, 24, 1),
(11, 1, 25, 1),
(12, 1, 26, 1),
(13, 1, 27, 1),
(14, 1, 28, 1),
(15, 1, 29, 1),
(16, 1, 30, 1),
(17, 1, 31, 1),
(18, 1, 32, 1),
(19, 1, 33, 1),
(20, 1, 34, 1),
(21, 1, 35, 1),
(22, 1, 36, 1),
(23, 1, 37, 1),
(24, 1, 38, 1),
(25, 1, 39, 1),
(26, 1, 40, 1),
(27, 1, 41, 1),
(28, 1, 42, 1),
(29, 1, 43, 1),
(30, 1, 44, 1),
(31, 1, 45, 1),
(32, 1, 46, 1),
(33, 1, 47, 1),
(34, 1, 48, 1),
(35, 1, 49, 1),
(36, 1, 50, 1),
(37, 1, 51, 1),
(38, 1, 52, 1),
(39, 1, 53, 1),
(40, 1, 54, 1),
(41, 1, 55, 1),
(42, 1, 56, 1),
(43, 1, 57, 1),
(44, 1, 58, 1),
(45, 2, 15, 0),
(46, 2, 16, 0),
(47, 2, 17, 0),
(48, 1, 59, 1),
(49, 3, 18, 1),
(50, 3, 52, 1),
(51, 4, 35, 1),
(52, 1, 60, 1),
(53, 1, 61, 1),
(54, 2, 62, 1),
(55, 5, 59, 1),
(56, 5, 60, 1),
(57, 5, 15, 1),
(58, 5, 16, 1),
(59, 5, 17, 1),
(60, 5, 18, 1),
(61, 5, 19, 1),
(62, 5, 20, 1),
(63, 5, 21, 1),
(64, 5, 22, 1),
(65, 5, 23, 1),
(66, 5, 24, 1),
(67, 5, 25, 1),
(68, 5, 26, 1),
(69, 5, 27, 1),
(70, 5, 28, 1),
(71, 5, 46, 1),
(72, 5, 47, 1),
(73, 5, 48, 1),
(74, 5, 49, 1),
(75, 5, 50, 1),
(76, 5, 51, 1),
(77, 5, 52, 1),
(78, 5, 53, 1),
(79, 5, 54, 1),
(80, 5, 55, 1),
(81, 5, 56, 1),
(82, 5, 57, 1),
(83, 5, 29, 1),
(84, 5, 30, 1),
(85, 5, 31, 1),
(86, 5, 32, 1),
(87, 5, 33, 1),
(88, 5, 34, 1),
(89, 5, 35, 1),
(90, 5, 36, 1),
(91, 5, 37, 1),
(92, 5, 38, 1),
(93, 5, 39, 1),
(94, 5, 40, 1),
(95, 5, 41, 1),
(96, 5, 42, 1),
(97, 5, 43, 1),
(98, 5, 44, 1),
(99, 5, 45, 1),
(100, 5, 58, 1),
(101, 5, 61, 1),
(102, 5, 62, 1),
(103, 6, 51, 1),
(104, 6, 53, 1),
(105, 6, 29, 1),
(106, 6, 30, 1),
(107, 6, 43, 1),
(108, 6, 45, 1),
(109, 6, 62, 1),
(110, 6, 18, 1),
(111, 6, 19, 1),
(112, 7, 59, 1),
(113, 7, 63, 1),
(114, 1, 63, 1),
(115, 2, 63, 1),
(116, 8, 40, 1),
(117, 8, 41, 1),
(118, 8, 42, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PERSONAS`
--

CREATE TABLE IF NOT EXISTS `PERSONAS` (
  `id_persona` int(11) NOT NULL AUTO_INCREMENT,
  `nombres_persona` varchar(50) NOT NULL,
  `apellidos_persona` varchar(50) NOT NULL,
  `cui_persona` varchar(25) NOT NULL,
  `direccion_persona` varchar(60) NOT NULL,
  `fecha_nacimiento_persona` date NOT NULL,
  `telefono_persona` varchar(15) NOT NULL,
  `telefono_auxiliar_persona` varchar(15) DEFAULT NULL,
  `correo_persona` varchar(50) NOT NULL,
  `estado_persona` tinyint(1) NOT NULL DEFAULT '1',
  `id_puesto` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_persona`),
  KEY `puestos_personas_fk` (`id_puesto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar a las personas que trabajan en el establecimiento como: docentes, conserjes, secretarias y todo su personal administrativo' AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `PERSONAS`
--

INSERT INTO `PERSONAS` (`id_persona`, `nombres_persona`, `apellidos_persona`, `cui_persona`, `direccion_persona`, `fecha_nacimiento_persona`, `telefono_persona`, `telefono_auxiliar_persona`, `correo_persona`, `estado_persona`, `id_puesto`) VALUES
(1, 'Byron Ismael', 'Castro Gutiérrez', '3456109345628', 'Guatemala', '2016-06-06', '45672145', NULL, 'bcastro@prueba.com', 1, 1),
(2, 'Andrea Elizabet', 'Fernandez López', '6734592345893', 'Zona 9 Guatemala', '1991-07-26', '3459-3456', NULL, 'andrea321@prueba.com', 1, 2),
(3, 'Mario Eduardo', 'López Bargas', '7345910345893', 'Zona 7 Guatemala', '1990-05-11', '4239-0567', NULL, 'mariol12@prueba.com', 1, 2),
(4, 'Estefania Rosario', 'Morales Hernandez', '3567302456789', 'Guatemala zona 9', '2001-01-02', '53956789', NULL, 'estefanimorales2@prueba.com', 1, 2),
(5, 'Cruz José', 'Florián Nájera', '2369177222201', 'Guatemala', '1990-05-03', '30154416', NULL, 'jflorian@webusiness.co', 1, 4),
(6, 'ADMIN', 'ADMIN', '4512457845124', 'GUATEMAL', '2014-12-09', '30154416', NULL, 'admin@gmail.com', 1, 4),
(7, 'Julio Pablo', 'Gomez Ruiz', '2333322222332', 'Guatemala', '1972-02-02', '77889900', NULL, 'julio@gmail.com', 1, 2),
(8, 'Nancy', 'Marroquin Contreras', '1122334455667', 'guatemala', '2016-08-19', '45454545', NULL, 'nancy@gmail.com', 1, 2),
(9, 'pedro', 'domingez', '8989898989878', 'Guatemala', '2001-01-09', '89898876', NULL, 'pedro@gmail.com', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PLANES`
--

CREATE TABLE IF NOT EXISTS `PLANES` (
  `id_plan` tinyint(4) NOT NULL AUTO_INCREMENT,
  `nombre_plan` varchar(25) NOT NULL,
  `estado_plan` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_plan`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los planes disponibles del establecimiento ej(diario, sabatino)' AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `PLANES`
--

INSERT INTO `PLANES` (`id_plan`, `nombre_plan`, `estado_plan`) VALUES
(1, 'diario', 1),
(2, 'fin de semana', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PUESTOS`
--

CREATE TABLE IF NOT EXISTS `PUESTOS` (
  `id_puesto` tinyint(4) NOT NULL AUTO_INCREMENT,
  `nombre_puesto` varchar(30) NOT NULL,
  `estado_puesto` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_puesto`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para alamcenar los puestos diponibles en elestablecimiento, como: docent, director, secretario, etc.' AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `PUESTOS`
--

INSERT INTO `PUESTOS` (`id_puesto`, `nombre_puesto`, `estado_puesto`) VALUES
(1, 'director', 1),
(2, 'docente', 1),
(3, 'secretaria', 1),
(4, 'Usuario', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PUNTOS`
--

CREATE TABLE IF NOT EXISTS `PUNTOS` (
  `id_puntos` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_la` varchar(30) DEFAULT NULL,
  `puntos_la` decimal(2,0) DEFAULT NULL,
  `nombre_lb` varchar(30) DEFAULT NULL,
  `puntos_lb` decimal(2,0) DEFAULT NULL,
  `nombre_lc` varchar(30) DEFAULT NULL,
  `puntos_lc` decimal(2,0) DEFAULT NULL,
  `nombre_ld` varchar(30) DEFAULT NULL,
  `puntos_ld` decimal(2,0) DEFAULT NULL,
  `nombre_le` varchar(30) DEFAULT NULL,
  `puntos_le` decimal(2,0) DEFAULT NULL,
  `nombre_lf` varchar(30) DEFAULT NULL,
  `puntos_lf` decimal(2,0) DEFAULT NULL,
  `nota_evaluacion` decimal(2,0) DEFAULT NULL,
  `habitos` varchar(2) DEFAULT NULL,
  `puntualidad` varchar(2) DEFAULT NULL,
  `ciclo_academico` date NOT NULL,
  PRIMARY KEY (`id_puntos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los puntos de evaluaciones de y de zon para un estudiante' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `PUNTOS_ESTUDIANTES`
--

CREATE TABLE IF NOT EXISTS `PUNTOS_ESTUDIANTES` (
  `id_puntos_estudiante` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_estudiante` bigint(20) NOT NULL,
  `id_actividad` bigint(20) NOT NULL,
  `fecha_calificado` datetime NOT NULL,
  `nombre_archivo` varchar(40) DEFAULT NULL,
  `modificado` tinyint(1) NOT NULL,
  `fecha_modificado` datetime NOT NULL,
  `url_archivo` varchar(50) DEFAULT NULL,
  `comentario_tarea` varchar(500) DEFAULT NULL,
  `nota_final` decimal(2,0) DEFAULT NULL,
  `fecha_subida` date DEFAULT NULL,
  `estado_entrega` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_puntos_estudiante`),
  KEY `tareas_tarea_estudiante_fk` (`id_actividad`),
  KEY `estudiantes_tarea_estudiante_fk` (`id_estudiante`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla para relacionar una actividad con un estudiante' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `REFERENCIAS`
--

CREATE TABLE IF NOT EXISTS `REFERENCIAS` (
  `id_referencia` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'Identificador unico de cada registro',
  `nombre_referencia` varchar(150) NOT NULL COMMENT 'almacena el nombre de la referencia del estudiante',
  `parentesco_referencia` varchar(50) NOT NULL COMMENT 'campo para almacenar el parentesco que tiene la referencia del estudiante',
  `telefono_referencia` varchar(15) NOT NULL COMMENT 'almacenar el numero de telefono',
  `id_estudiante` bigint(20) NOT NULL,
  PRIMARY KEY (`id_referencia`),
  KEY `estudiantes_referencias_fk` (`id_estudiante`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar las referencias personales del estudiante' AUTO_INCREMENT=36 ;

--
-- Volcado de datos para la tabla `REFERENCIAS`
--

INSERT INTO `REFERENCIAS` (`id_referencia`, `nombre_referencia`, `parentesco_referencia`, `telefono_referencia`, `id_estudiante`) VALUES
(1, 'Melani Hernandez', 'Hermana', '12345678', 6),
(2, 'Jose Fernando Gonzales Ramos', 'hermano', '12345678', 6),
(5, 'Melani Hernandez', 'Hermana', '12345678', 8),
(6, 'Jose Fernando Gonzales Ramos', 'hermano', '12345678', 8),
(7, 'Mariela Fernananda López Maldonado', 'Prima', '123455678', 9),
(8, 'Juan Perez', 'tio', '12345678', 10),
(9, 'Carlangas', 'Padre', '12457845', 11),
(10, 'Paco Lorenzana', 'Amigo', '124578989', 12),
(11, 'Mariela Fernananda López Maldonado', 'tia', '78451245', 13),
(12, 'ninguna', 'ninguna', '4512452114', 14),
(13, 'nignuna', 'nignuna', '451245878', 15),
(14, 'ninguno', 'ninguno', '45124512', 16),
(21, 'constancia', 'constancia', '45127845', 23),
(35, 'pruebaaa', 'pruebaaa', '88552266', 37);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `RESPUESTAS_FOROS`
--

CREATE TABLE IF NOT EXISTS `RESPUESTAS_FOROS` (
  `id_respuesta_foro` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_foro` int(11) NOT NULL,
  `mensaje_respuesta` varchar(500) NOT NULL,
  `fecha_respuesta` datetime NOT NULL,
  `id_usuario` bigint(20) NOT NULL,
  PRIMARY KEY (`id_respuesta_foro`),
  UNIQUE KEY `id_usuario_respuestas_foros_fk` (`id_usuario`),
  KEY `foros_respuestas_foros_fk` (`id_foro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar las respuestas hacia un foro creado' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `state_rol` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los roles disponibles en la aplicación' AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`, `state_rol`) VALUES
(1, 'administrador', 'administrador', 'Rol para el usuario administrador', '2016-06-21 12:06:46', NULL, 1),
(2, 'profesor', 'profesor', 'Rol para los profesores', '2016-06-29 10:57:27', NULL, 1),
(3, 'padres', 'padres', 'rol para el padre de familia', '2016-07-06 10:51:18', NULL, 1),
(4, 'estudiantes', 'estudiantes', 'Permiso para los estudiantes', '2016-07-28 11:05:23', NULL, 1),
(5, 'Usuario', 'Usuario', 'Usuario de Prueba', '2016-08-09 08:56:16', NULL, 1),
(6, 'NuevoRol', 'NuevoRol', 'este es un nuevo rol', '2016-08-09 11:56:26', NULL, 1),
(7, 'NUEVOROL2', 'NUEVOROL2', 'este es un nuevo rol', '2016-08-19 09:03:25', NULL, 1),
(8, 'newrol3', 'newrol3', 'es un rol', '2016-08-24 12:07:19', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `id_role_user` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id_role_user`),
  KEY `roles_role_user_fk` (`role_id`),
  KEY `users_role_user_fk` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar las relaciones entre usuarios y roles' AUTO_INCREMENT=45 ;

--
-- Volcado de datos para la tabla `role_user`
--

INSERT INTO `role_user` (`id_role_user`, `role_id`, `user_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 3),
(9, 4, 17),
(11, 4, 21),
(12, 5, 22),
(13, 5, 23),
(14, 4, 24),
(15, 2, 26),
(16, 4, 27),
(17, 2, 29),
(18, 4, 30),
(19, 4, 32),
(20, 4, 34),
(21, 4, 36),
(22, 4, 38),
(23, 4, 40),
(30, 4, 53),
(44, 4, 73);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SALONES`
--

CREATE TABLE IF NOT EXISTS `SALONES` (
  `id_salon` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_salon` varchar(20) NOT NULL,
  `estado_salon` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_salon`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar el nombre de las clases (lugares fisicos donde se imparten las docencias)' AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `SALONES`
--

INSERT INTO `SALONES` (`id_salon`, `nombre_salon`, `estado_salon`) VALUES
(1, 'salón a', 1),
(2, 'salón b', 1),
(3, 'lab. computación', 1),
(4, 'taller dibujo', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SECCIONES`
--

CREATE TABLE IF NOT EXISTS `SECCIONES` (
  `id_seccion` tinyint(4) NOT NULL AUTO_INCREMENT,
  `nombre_seccion` varchar(10) NOT NULL,
  PRIMARY KEY (`id_seccion`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los datos de las secciones disponibles en el establecimiento' AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `SECCIONES`
--

INSERT INTO `SECCIONES` (`id_seccion`, `nombre_seccion`) VALUES
(1, 'a'),
(2, 'b'),
(3, 'c'),
(4, 'd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TIPO_ACTIVIDAD`
--

CREATE TABLE IF NOT EXISTS `TIPO_ACTIVIDAD` (
  `id_tipo_actividad` tinyint(4) NOT NULL AUTO_INCREMENT,
  `nombre_tipo_actividad` varchar(40) NOT NULL,
  `descripcion_tipo_actividad` varchar(300) NOT NULL,
  `estado_tipo_actividad` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_tipo_actividad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los tipos de actividades disponibles (evaluacion. laboratorio, tarea, etc).' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TUTOR`
--

CREATE TABLE IF NOT EXISTS `TUTOR` (
  `id_tutor` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre_tutor` varchar(60) NOT NULL,
  `apellidos_tutor` varchar(60) NOT NULL,
  `direccion_tutor` varchar(70) NOT NULL,
  `telefono_primario_tutor` varchar(15) NOT NULL,
  `empresa_telefono_tutor` varchar(30) DEFAULT NULL COMMENT 'almacena el nombre de la empresa telefonica del numero de telefono personal del tutor',
  `correo_electronico_tutor` varchar(50) NOT NULL,
  `cui_tutor` varchar(25) NOT NULL,
  `lugar_trabajo_tutor` varchar(100) DEFAULT NULL COMMENT 'campo para almacenar el nombre del lugar de trabajo del tutor',
  `direccion_trabajo_tutor` varchar(100) DEFAULT NULL COMMENT 'almacena la dirección de trabajo del tutor',
  `telefono_trabajo_tutor` varchar(15) DEFAULT NULL COMMENT 'almacena el telefono del trabajo del tutor',
  `empresa_telefono_trabajo` varchar(30) DEFAULT NULL COMMENT 'Nombre de la empresa telefonica del numero del telefono de trabajo del tutor',
  `cargo_tutor` varchar(50) DEFAULT NULL COMMENT 'almacenar el nombre del cargo dentro de la empresa donde trabaja el tutor',
  `parentesco_tutor` varchar(50) DEFAULT NULL COMMENT 'tabla para almacenar el nombre del parentesco del tutor con el estudiante',
  `estado_tutor` tinyint(1) NOT NULL DEFAULT '1',
  `id` bigint(20) NOT NULL,
  PRIMARY KEY (`id_tutor`),
  KEY `usuarios_tutor_fk` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los datos de los tutores de los estudiantes' AUTO_INCREMENT=30 ;

--
-- Volcado de datos para la tabla `TUTOR`
--

INSERT INTO `TUTOR` (`id_tutor`, `nombre_tutor`, `apellidos_tutor`, `direccion_tutor`, `telefono_primario_tutor`, `empresa_telefono_tutor`, `correo_electronico_tutor`, `cui_tutor`, `lugar_trabajo_tutor`, `direccion_trabajo_tutor`, `telefono_trabajo_tutor`, `empresa_telefono_trabajo`, `cargo_tutor`, `parentesco_tutor`, `estado_tutor`, `id`) VALUES
(9, 'Maria Feliza', 'Fernandez Santizo', 'Guatemala', '12345678', 'claro', 'mgonzalez@prueba.com', '1234567899087', 'Banco Industrial', '7a. Avenida 6ta calle zona 9', '12345678', 'claro', 'Cajera', 'Madre de Familia', 1, 18),
(10, 'Manuel Alejandro', 'Hernadez Gonzales', 'Zona 3 Guatemala', '12345678', 'claro', 'manuel@prueba.com', '1920487895067', 'Web Business', '1a. Avenida 8a calle zona 9', '12345678', 'movistar', 'Programador', 'Padre de Familia', 1, 19),
(11, 'Juan', 'Pedro', 'guatemala', '78787878', 'claro', 'juan@gmail.com', '8989898989899', 'guatemala', 'guatemala', '7887878878', 'claro', 'ninguno', 'padre', 1, 25),
(12, 'juana', 'flores', 'guatemala', '89898989', 'claro', 'juana@gmail.com', '7878789898789', 'ninguno', 'ninguna', '89898989', 'tigo', 'gerente', 'padre', 1, 28),
(13, 'Elver', 'Galarga', 'Guatemala', '12457845', 'claro', 'elver@galarga.com', '4512457845124', 'Ninguno', 'Ninguno', '1245786589', 'tigo', 'Ninguno', 'tio', 1, 31),
(14, 'Perla', 'Paconila', 'Quezada', '45451212', 'claro', 'perla@gmail.com', '4512457854512', 'Su casa', 'Su casa', '451232554', 'claro', 'ama de casa', 'mama', 1, 33),
(15, 'Pedro', 'Gomez', 'Guatemala', '12457845', 'claro', 'pedro@gmail.com', '7845126345124', 'Su casa', 'Ninguna', '45124578', 'claro', 'Gerente', 'Papa', 1, 35),
(16, 'hola', 'hoal', 'hola', '45124578', 'claro', 'ninguno@gmail.com', '4512457845121', 'Ninguno', 'El Caso', '1245784512', 'claro', 'ningkuno', 'ninguno', 1, 37),
(17, 'nignuna', 'nignuna', 'nignuna', '45124514', 'claro', 'nignuna@gmail.com', '4512451245124', 'nignuna', 'nignuna', '45124578', 'tigo', 'nignuna', 'nignuna', 1, 39),
(24, 'constancia', 'constancia', 'constancia', '454545412', 'tigo', 'constancia@gmail.com', '4545454545454', 'constancia', 'constancia', '45127845', 'claro', 'constancia', 'constancia', 1, 54);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `TUTOR_ESTUDIANTE`
--

CREATE TABLE IF NOT EXISTS `TUTOR_ESTUDIANTE` (
  `id_tutor_estudiante` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_estudiante` bigint(20) NOT NULL,
  `id_tutor` bigint(20) NOT NULL,
  PRIMARY KEY (`id_tutor_estudiante`),
  KEY `estudiantes_tutor_estudiante_fk` (`id_estudiante`),
  KEY `tutor_tutor_estudiante_fk` (`id_tutor`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar las asignaciones de tutores de los estudiantes' AUTO_INCREMENT=44 ;

--
-- Volcado de datos para la tabla `TUTOR_ESTUDIANTE`
--

INSERT INTO `TUTOR_ESTUDIANTE` (`id_tutor_estudiante`, `id_estudiante`, `id_tutor`) VALUES
(9, 6, 9),
(10, 6, 10),
(13, 8, 9),
(14, 8, 10),
(15, 9, 11),
(16, 10, 12),
(17, 11, 13),
(18, 12, 14),
(19, 13, 15),
(20, 14, 16),
(21, 15, 17),
(22, 16, 17),
(29, 23, 24),
(43, 37, 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `UNIDADES`
--

CREATE TABLE IF NOT EXISTS `UNIDADES` (
  `id_unidad` tinyint(4) NOT NULL AUTO_INCREMENT,
  `nombre_unidad` varchar(20) NOT NULL,
  `estado_unidad` tinyint(1) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_final` date NOT NULL,
  PRIMARY KEY (`id_unidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar las unidades en las que se divide el ciclo academico.' AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `UNIDADES`
--

INSERT INTO `UNIDADES` (`id_unidad`, `nombre_unidad`, `estado_unidad`, `fecha_inicio`, `fecha_final`) VALUES
(1, 'i unidad', 1, '2016-01-11', '2016-02-29'),
(2, 'ii unidad', 1, '2016-03-01', '2016-04-30'),
(3, 'iii unidad', 1, '2016-05-01', '2016-06-30'),
(4, 'iv unidad', 1, '2016-07-01', '2016-08-31'),
(5, 'v unidad', 1, '2016-09-01', '2016-10-28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(70) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `estado_usuario` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para almacenar los usuarios y contraseñas de los usuarios' AUTO_INCREMENT=74 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `estado_usuario`) VALUES
(1, 'Byron Castro', 'bcastro20110105@webusiness.co', '$2y$10$Ml98O8o5Yd1DzYE7vl2I4.uRCDs.LRiGDUSo5ak9aCAN06F4Gzuqy', 'l1Ch12yYeVtYEoQWWmdMwVkXBhsIk4SwJgnn2slijVfs0vIkrXDOss0E2jL6', '2011-01-05 00:00:00', '2016-08-24 15:22:00', 1),
(2, 'mario lópez', 'mlópez19900511@webusiness.co', '$2y$10$IpKZFB2q9yTkUlK07PRdz.PJRUXvequn6wzTWCD8E/qY7sgiv054y', NULL, '1990-05-11 00:00:00', NULL, 1),
(3, 'andrea fernandez', 'afernandez19910726@webusiness.co', '$2y$10$PlOCtIvfqzW6o2oSpb9.Hu2QsoCbgPj6r0Y5sG6ofobUSl1cK1NTa', 'YcImoaojZajgIqHCZ9N3VTJ7UxvludSSbtH0kTgRcDg72zGwmouAxrFxf8kU', '1991-07-26 00:00:00', '2016-08-12 14:08:24', 1),
(17, 'josé marino', 'jmarino19911114@webusiness.co', '$2y$10$EoAEfVbNIVmOdceW5.hhgeQVA9OItwyyV3rOGqXPOEV21XdjT5SFy', 'D9aewzdNUCvV44sbJKTlMY7Lt94sGOz7BUrftQmTbBHLnavKlVRCkiYpRCc9', '2016-08-01 00:00:00', '2016-08-19 15:36:30', 1),
(18, 'josé marino', 'jmarino20160801@webusiness.co', '$2y$10$Qzm0R3XNnqBodVeeDraZz.J.qc85DPAC0n3.DtnXp.7neM9ZhrX5G', NULL, '2016-08-01 00:00:00', NULL, 1),
(19, 'josé marino', 'jmarino20160801@webusiness.co', '$2y$10$jNpCVBfapIvo6IdKXtaAEOYPQ4ZiXofrVV5wVm64hVUkwbcYPUmm6', NULL, '2016-08-01 00:00:00', NULL, 1),
(21, 'juan paredes', 'jparedes20000714@webusiness.co', '$2y$10$e2zkwHNF9hz9rdPiUS73f.hYMubO.gVj3USknQbA2QDCCZ6.wXyHa', 'KDa7W0Snrc53I1fGbs3loe5xC3x54T4MwkMs76CkYBUzosL1gzX5F4o06H1a', '2016-08-04 00:00:00', '2016-08-12 13:55:13', 1),
(22, 'cruz florián', 'cflorián19900503@webusiness.co', '$2y$10$OZkR47JvbP1D/ZnXhIM5c.lofGcL92TfsDh7PR8qXvuSMRMAIP2lW', NULL, '1990-05-03 00:00:00', NULL, 1),
(23, 'admin admin', 'aadmin20141209@webusiness.co', '$2y$10$s6T4fFbImW.ryPVtlhEnlOfhz7bbB3HfPeGmmMjTNsC18L2hO0WzG', 'S3BzHFBhpVJxmAwePonewI8MxhOfs26GSPi2mAJuKkSDpbyUtsRT1rsJIOAo', '2014-12-09 00:00:00', '2016-08-22 11:24:55', 1),
(24, 'herbert bungeroth', 'hbungeroth20040204@webusiness.co', '$2y$10$Pa4UCoZbhG4njq0W2xiRG.TjAsWKB.mA9k6Z5wPQ/PsI0ThpSM/te', NULL, '2016-08-12 00:00:00', NULL, 1),
(25, 'herbert bungeroth', 'hbungeroth20160812@webusiness.co', '$2y$10$Xe9EMO/0/HYYwFGAXUCdveBMKJuVJuxffOqUi2iUkvVzyyNhr03wO', NULL, '2016-08-12 00:00:00', NULL, 1),
(26, 'nancy marroquin', 'nmarroquin20160819@webusiness.co', '$2y$10$9vEHAB2mbm/4pdZJz1SR.OCtCVNyRiYYtscJ82wpi3p62KRIEotbm', '6PgcwvdthZlABKELwNKRaLTD457h69tEVsEOZi9Ceb3hsv43XwWRP8kLKiOW', '2016-08-19 00:00:00', '2016-08-19 14:17:09', 1),
(27, 'pedro flores', 'pflores20160819@webusiness.co', '$2y$10$4AKBGwpGEqDbIMZEMkfyC.bPqVqJgQfR9CK8Q/1EcKpZhMgNJ18CO', NULL, '2016-08-19 00:00:00', NULL, 1),
(28, 'pedro flores', 'pflores20160819@webusiness.co', '$2y$10$tXF2gXqITd0zuyka9s4yGOCBlulh.JsFg2LqsfvvElsYzrKWWyUzW', NULL, '2016-08-19 00:00:00', NULL, 1),
(29, 'pedro domingez', 'pdomingez20010109@webusiness.co', '$2y$10$whZRzndgeW4Ri/IOqZQBh.Gub2rgUTwAyMioAo9TkLl8kYizm1JTe', 'bUaYLf6yFTkuagFp2PP7azNGIsUOGZewvsWhzN26S0EYEvwjgVIknHDmjlRD', '2001-01-09 00:00:00', '2016-08-19 15:48:10', 1),
(30, 'luis gomez', 'lgomez19920715@webusiness.co', '$2y$10$59W2kmukiGI1bUve8cEG4.fnPZShnad7W6E8m5qOj2lc.I9sjYUBO', NULL, '2016-08-24 00:00:00', NULL, 1),
(31, 'luis gomez', 'lgomez20160824@webusiness.co', '$2y$10$eY0cQhHvtVJvrJVvw3/3dOOFc8Y186zFdAxLQHNfrZmO1OiHoTFKS', NULL, '2016-08-24 00:00:00', NULL, 1),
(32, 'perla donado', 'pdonado20010613@webusiness.co', '$2y$10$LzG3ETIh2BEsBrs0mAmuQ.Siea.oB1jCHMA2nnHLNV5dbDc5JV9CW', NULL, '2016-08-24 00:00:00', NULL, 1),
(33, 'perla donado', 'pdonado20160824@webusiness.co', '$2y$10$Nyls8AlcR2q3k29dEFXPXOWPvynyuN8/l9LLUQMvrrqyCVdZa3Lsm', NULL, '2016-08-24 00:00:00', NULL, 1),
(34, 'pedro arrivillaga', 'parrivillaga20010612@webusiness.co', '$2y$10$j73E32HsWFbsYeP0YoHzOOszqUP.264tFdrLG52P6.AUapq5oInkC', NULL, '2016-08-25 00:00:00', NULL, 1),
(35, 'pedro arrivillaga', 'parrivillaga20160825@webusiness.co', '$2y$10$/rVP9rVOev6SxPcLwaLW/ORC.D0ENs.wvhdS9khvhqyWOBz2.Nix.', NULL, '2016-08-25 00:00:00', NULL, 1),
(36, 'alguien alguno', 'aalguno20160825@webusiness.co', '$2y$10$8LmM1j5Dal3aGOw15oTF2O4o0ocyovsje58lmP5RCo7Z0I7XN0jji', NULL, '2016-08-25 00:00:00', NULL, 1),
(37, 'alguien alguno', 'aalguno20160825@webusiness.co', '$2y$10$5JGSjvlLjFCDIRyEO2Et7O6ZelV9dE07xcEYOP0qeOZaj93ofoGTe', NULL, '2016-08-25 00:00:00', NULL, 1),
(38, 'hola mundo', 'hmundo20010207@webusiness.co', '$2y$10$dWWEOQN/haZwxuTdhLpsweEEqt92ZfPZcQLplEvMWiaJCzE4wSzja', NULL, '2016-08-25 00:00:00', NULL, 1),
(39, 'hola mundo', 'hmundo20160825@webusiness.co', '$2y$10$y7H3pBBocPKKNWHIRNoMEO8zoYc6Bf3hGJUtQzvANcZKVdjfnf.Um', NULL, '2016-08-25 00:00:00', NULL, 1),
(40, 'ninguno ninguno', 'nninguno20010614@webusiness.co', '$2y$10$FJzTb.hzeLR6qj0GTzJDIeMfWsHPEcsBYyuvZGI.OelWck1FS9JyS', NULL, '2016-08-25 00:00:00', NULL, 1),
(53, 'constancia constancia', 'cconstancia20110104@webusiness.co', '$2y$10$4kdPBhg7ha94r9Pg0lCY7OUJPjZLL54BdKmfAzUYsVkQOMFg.xTVi', NULL, '2016-08-25 00:00:00', NULL, 1),
(54, 'constancia constancia', 'cconstancia20160825@webusiness.co', '$2y$10$Hsln1L40MJTD8ErXg4qcvunpmrvBMVmbrw2jeiG1ES6Hv67drujGu', NULL, '2016-08-25 00:00:00', NULL, 1),
(73, 'pruebaaa pruebaaa', 'ppruebaaa20160623@webusiness.co', '$2y$10$9IgAoL4KrAcxaepb0g9odegH38m7Krk49x9oIt/bKbwsyFyPDDHDq', NULL, '2016-08-25 00:00:00', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `USUARIO_PERSONA`
--

CREATE TABLE IF NOT EXISTS `USUARIO_PERSONA` (
  `id_usuario_persona` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `id_persona` int(11) NOT NULL,
  PRIMARY KEY (`id_usuario_persona`),
  KEY `personas_usuario_persona_fk` (`id_persona`),
  KEY `usuarios_usuario_persona_fk` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Tabla para relacionar un usuario con una persona' AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `USUARIO_PERSONA`
--

INSERT INTO `USUARIO_PERSONA` (`id_usuario_persona`, `user_id`, `id_persona`) VALUES
(1, 2, 3),
(2, 3, 2),
(3, 22, 5),
(4, 23, 6),
(5, 26, 8),
(6, 29, 9);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `ACTIVIDADES`
--
ALTER TABLE `ACTIVIDADES`
  ADD CONSTRAINT `asignacion_areas_tareas_fk` FOREIGN KEY (`id_asignacion_area`) REFERENCES `ASIGNACION_AREAS` (`id_asignacion_area`) ON UPDATE CASCADE,
  ADD CONSTRAINT `bloques_tareas_fk` FOREIGN KEY (`id_unidad`) REFERENCES `UNIDADES` (`id_unidad`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tipo_actividad_tareas_fk` FOREIGN KEY (`id_tipo_actividad`) REFERENCES `TIPO_ACTIVIDAD` (`id_tipo_actividad`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `ASIGNACION_AREAS`
--
ALTER TABLE `ASIGNACION_AREAS`
  ADD CONSTRAINT `areas_asignacion_areas_fk` FOREIGN KEY (`id_area`) REFERENCES `AREAS` (`id_area`) ON UPDATE CASCADE,
  ADD CONSTRAINT `niveles_grados_asignacion_areas_fk` FOREIGN KEY (`id_nivel_grado`) REFERENCES `NIVELES_GRADOS` (`id_nivel_grado`) ON UPDATE CASCADE,
  ADD CONSTRAINT `salones_asignacion_areas_fk` FOREIGN KEY (`id_salon`) REFERENCES `SALONES` (`id_salon`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `ASIGNACION_DOCENTE`
--
ALTER TABLE `ASIGNACION_DOCENTE`
  ADD CONSTRAINT `asignacion_areas_asignacion_docente_fk` FOREIGN KEY (`id_asignacion_area`) REFERENCES `ASIGNACION_AREAS` (`id_asignacion_area`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `personas_asignacion_docente_fk` FOREIGN KEY (`id_persona`) REFERENCES `PERSONAS` (`id_persona`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `BITACORA_ACTIVIDADES`
--
ALTER TABLE `BITACORA_ACTIVIDADES`
  ADD CONSTRAINT `usuarios_vitacora_actividades_fk` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `DOCENTES_GUIAS`
--
ALTER TABLE `DOCENTES_GUIAS`
  ADD CONSTRAINT `niveles_grados_docentes_guias_fk` FOREIGN KEY (`id_nivel_grado`) REFERENCES `NIVELES_GRADOS` (`id_nivel_grado`) ON UPDATE CASCADE,
  ADD CONSTRAINT `personas_docentes_guias_fk` FOREIGN KEY (`id_persona`) REFERENCES `PERSONAS` (`id_persona`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `ESTUDIANTES`
--
ALTER TABLE `ESTUDIANTES`
  ADD CONSTRAINT `asignacion_areas_estudiantes_fk` FOREIGN KEY (`id_nivel_grado`) REFERENCES `NIVELES_GRADOS` (`id_nivel_grado`) ON UPDATE CASCADE,
  ADD CONSTRAINT `municipio_estudiantes_fk` FOREIGN KEY (`id_municipio`) REFERENCES `MUNICIPIO` (`id_municipio`) ON UPDATE CASCADE,
  ADD CONSTRAINT `usuarios_estudiantes_fk` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `FOROS`
--
ALTER TABLE `FOROS`
  ADD CONSTRAINT `asignacion_areas_foros_fk` FOREIGN KEY (`id_asignacion_area`) REFERENCES `ASIGNACION_AREAS` (`id_asignacion_area`) ON UPDATE CASCADE,
  ADD CONSTRAINT `id_usuarios_foros_fk` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `INTITUCION`
--
ALTER TABLE `INTITUCION`
  ADD CONSTRAINT `descripcion_intitucion_fk` FOREIGN KEY (`id_descripcion`) REFERENCES `DESCRIPCION` (`id_descripcion`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `MUNICIPIO`
--
ALTER TABLE `MUNICIPIO`
  ADD CONSTRAINT `departamento_municipio_fk` FOREIGN KEY (`id_departamento`) REFERENCES `DEPARTAMENTO` (`id_departamento`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `NIVELES_GRADOS`
--
ALTER TABLE `NIVELES_GRADOS`
  ADD CONSTRAINT `carreras_niveles_grados_fk` FOREIGN KEY (`id_carrera`) REFERENCES `CARRERAS` (`id_carrera`) ON UPDATE CASCADE,
  ADD CONSTRAINT `grados_niveles_grados_fk` FOREIGN KEY (`id_grado`) REFERENCES `GRADOS` (`id_grado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `niveles_planes_jornadas_niveles_grados_fk` FOREIGN KEY (`id_nivel_plan_jornada`) REFERENCES `NIVELES_PLANES_JORNADAS` (`id_nivel_plan_jornada`) ON UPDATE CASCADE,
  ADD CONSTRAINT `secciones_niveles_grados_fk` FOREIGN KEY (`id_seccion`) REFERENCES `SECCIONES` (`id_seccion`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `NIVELES_PLANES_JORNADAS`
--
ALTER TABLE `NIVELES_PLANES_JORNADAS`
  ADD CONSTRAINT `jornadas_niveles_planes_jornadas_fk` FOREIGN KEY (`id_jornada`) REFERENCES `JORNADAS` (`id_jornada`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `niveles_niveles_planes_jornadas_fk` FOREIGN KEY (`id_nivel`) REFERENCES `NIVELES` (`id_nivel`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `planes_niveles_planes_jornadas_fk` FOREIGN KEY (`id_plan`) REFERENCES `PLANES` (`id_plan`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `opciones_permissions_fk` FOREIGN KEY (`id_opcion`) REFERENCES `OPCIONES` (`id_opcion`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permissions_permission_role_fk` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `roles_permission_role_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `PERSONAS`
--
ALTER TABLE `PERSONAS`
  ADD CONSTRAINT `puestos_personas_fk` FOREIGN KEY (`id_puesto`) REFERENCES `PUESTOS` (`id_puesto`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `PUNTOS_ESTUDIANTES`
--
ALTER TABLE `PUNTOS_ESTUDIANTES`
  ADD CONSTRAINT `estudiantes_tarea_estudiante_fk` FOREIGN KEY (`id_estudiante`) REFERENCES `ESTUDIANTES` (`id_estudiante`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tareas_tarea_estudiante_fk` FOREIGN KEY (`id_actividad`) REFERENCES `ACTIVIDADES` (`id_actividad`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `REFERENCIAS`
--
ALTER TABLE `REFERENCIAS`
  ADD CONSTRAINT `estudiantes_referencias_fk` FOREIGN KEY (`id_estudiante`) REFERENCES `ESTUDIANTES` (`id_estudiante`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `RESPUESTAS_FOROS`
--
ALTER TABLE `RESPUESTAS_FOROS`
  ADD CONSTRAINT `foros_respuestas_foros_fk` FOREIGN KEY (`id_foro`) REFERENCES `FOROS` (`id_foro`) ON UPDATE CASCADE,
  ADD CONSTRAINT `id_usuario_respuestas_foros_fk` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `roles_role_user_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `users_role_user_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `TUTOR`
--
ALTER TABLE `TUTOR`
  ADD CONSTRAINT `usuarios_tutor_fk` FOREIGN KEY (`id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `TUTOR_ESTUDIANTE`
--
ALTER TABLE `TUTOR_ESTUDIANTE`
  ADD CONSTRAINT `estudiantes_tutor_estudiante_fk` FOREIGN KEY (`id_estudiante`) REFERENCES `ESTUDIANTES` (`id_estudiante`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tutor_tutor_estudiante_fk` FOREIGN KEY (`id_tutor`) REFERENCES `TUTOR` (`id_tutor`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `USUARIO_PERSONA`
--
ALTER TABLE `USUARIO_PERSONA`
  ADD CONSTRAINT `personas_usuario_persona_fk` FOREIGN KEY (`id_persona`) REFERENCES `PERSONAS` (`id_persona`) ON UPDATE CASCADE,
  ADD CONSTRAINT `usuarios_usuario_persona_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
