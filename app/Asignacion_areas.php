<?php



namespace education;



use Illuminate\Database\Eloquent\Model;

use DB;

class Asignacion_areas extends Model

{

    protected $table = 'ASIGNACION_AREAS';



    /*

      Metodo para buscar los grados que estan disponibles para crearles un pensum

      de acuerdo al plan y jornada indicados

    */

    public static function getGrado($plan, $jornada)

    {

      return DB::table('NIVELES_PLANES_JORNADAS AS npj')

               ->join('NIVELES AS n', 'npj.id_nivel', '=', 'n.id_nivel')

               ->join('NIVELES_GRADOS AS ng', 'npj.id_nivel_plan_jornada', '=', 'ng.id_nivel_plan_jornada')

               ->join('GRADOS AS g', 'ng.id_grado', '=', 'g.id_grado')

               ->join('SECCIONES AS s', 'ng.id_seccion', '=', 's.id_seccion')

               ->join('CARRERAS AS c', 'ng.id_carrera', '=', 'c.id_carrera')

               ->where([['npj.id_plan', $plan], ['npj.id_jornada', $jornada], ['ng.pensum_nivel_grado', FALSE]])

               ->select('ng.id_nivel_grado', 'g.nombre_grado', 'n.nombre_nivel', 'c.nombre_carrera', 's.nombre_seccion')

               ->get();

    }



    /*

    Metodo para agregar un nuevo registro a la base de datos (pensum de los grados)

    */

    public static function setAsignacionArea($grado, $salon, $curso)

    {

      return DB::insert('CALL nueva_asignacion_area(?, ?, ?, ?)', array($grado, $salon, $curso, date('Y-m-d H:i:s')));

    }



    /*

      Metodo para buscar los grados donde se han asignados cursos de su pensum

    */

    public static function getGradosPensum()

    {

      return ASIGNACION_AREAS::join('NIVELES_GRADOS AS ng', 'ASIGNACION_AREAS.id_nivel_grado', '=', 'ng.id_nivel_grado')

                             ->join('NIVELES_PLANES_JORNADAS AS npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')

                             ->join('NIVELES AS n', 'npj.id_nivel', '=', 'n.id_nivel')

                             ->join('GRADOS AS g', 'ng.id_grado', '=', 'g.id_grado')

                             ->join('CARRERAS AS c', 'ng.id_carrera', '=', 'c.id_carrera')

                             ->select('g.nombre_grado', 'n.nombre_nivel', 'c.nombre_carrera', 'ng.id_nivel_grado')

                             ->groupBy('ng.id_nivel_grado')

                             ->get();

    }



    /*

      Metodo pára buscar el pensum de un grado por su id

    */

    public static function getPensumGrado($id)

    {

      return ASIGNACION_AREAS::join('AREAS as a', 'ASIGNACION_AREAS.id_area', '=', 'a.id_area')

                             ->join('SALONES as s', 'ASIGNACION_AREAS.id_salon', '=', 's.id_salon')

                             ->join('NIVELES_GRADOS as ng', 'ASIGNACION_AREAS.id_nivel_grado', '=', 'ng.id_nivel_grado')

                             ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')

                             ->join('CARRERAS as c', 'ng.id_carrera', '=', 'c.id_carrera')

                             ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')

                             ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')

                             ->where('ng.id_nivel_grado', $id)

                             ->select('a.nombre_area', 'g.nombre_grado', 'c.nombre_carrera', 'n.nombre_nivel', 's.nombre_salon', 's.id_salon', 'ASIGNACION_AREAS.id_asignacion_area', 'ASIGNACION_AREAS.estado_asignacion_area', 'ASIGNACION_AREAS.id_nivel_grado')

                             ->get();

    }

    /*

      Metodo para cambiar el estado de un area del pensum asignado

    */

    public static function stateAsignacionArea($id, $estado)

    {

      return DB::select('CALL estado_asignacion_area(?, ?)', array($id, $estado));

    }



    /*

      Metodo para buscar los cursos del pensum de un grado

    */

    public static function getPensumGradoWhere($grado)

    {

      return ASIGNACION_AREAS::join('AREAS as a', 'ASIGNACION_AREAS.id_area', '=', 'a.id_area')

                             ->where('ASIGNACION_AREAS.estado_asignacion_area', TRUE)

                             ->where('ASIGNACION_AREAS.id_nivel_grado', $grado)

                             ->select('ASIGNACION_AREAS.id_asignacion_area', 'a.nombre_area')

                             ->get();

    }


    

    /*FUNCION PARA TRAER TODAS LAS AREAS ASIGNADAS A UN GRADO*/


    public static function getAreaWhere($nivel)

    {

      return ASIGNACION_AREAS::join('AREAS as a', 'ASIGNACION_AREAS.id_area', '=', 'a.id_area')

                                    ->where('ASIGNACION_AREAS.id_nivel_grado',$nivel)
                                    ->where('a.estado_area',1)

                                    ->select('a.nombre_area', 'a.id_area')

                                    ->get();

                                    /*
                                    select * from asignacion_areas as aa join areas as a on aa.id_area=a.id_area
                                    where aa.id_nivel_grado=4
                                    */

    }


}

