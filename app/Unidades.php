<?php



namespace education;



use Illuminate\Database\Eloquent\Model;



class Unidades extends Model

{

    protected $table = 'UNIDADES';

    public $timestamps = false;

     public static function getUnidades()

    {

      $unidades = array();

      foreach (UNIDADES::where('estado_unidad', TRUE)->get() as $key => $u) {

        $unidades[$u->id_unidad] = mb_strtoupper($u->nombre_unidad);

      }

      return $unidades;

    }

}

