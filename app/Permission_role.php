<?php

namespace education;

use Illuminate\Database\Eloquent\Model;
use DB;
class Permission_role extends Model
{
  protected $table = 'permission_role';

  /*
    Agrega un nuevo registro a al base de datos
  */
  public static function setPermisoRol($rol, $permiso)
  {
    return DB::insert('CALL nuevo_permiso_rol(?, ?)', array($rol, $permiso));
  }

  /* Metodo para buscar los permisos asignados a un rol por el id del rol
  */
  public static function findPermisos($rol)
  {
    return permission_role::join('permissions AS so', 'permission_role.permission_id', '=', 'so.id')
                         ->where('role_id', $rol)
                         ->select('so.name', 'permission_role.id_permission_role', 'permission_role.state_permission_role')
                         ->get();
  }

  /*
    Metodo para cambiar el estado de un permiso asignado a un rol
  */
  public static function statePermisoRol($id, $estado)
  {
    return DB::select('CALL estado_permiso_rol(?, ?)', array($id, $estado));
  }
}
