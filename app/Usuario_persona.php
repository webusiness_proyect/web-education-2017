<?php

namespace education;

use Illuminate\Database\Eloquent\Model;
use DB;
class Usuario_persona extends Model
{
    protected $table = 'USUARIO_PERSONA';

    /*
      Agrega un nuevo registro a la base de datos
    */
    public static function setUsuarioPersona($usuario, $persona)
    {
      return DB::insert('CALL nuevo_usuario_persona(?, ?)', array($usuario, $persona));
    }


}
