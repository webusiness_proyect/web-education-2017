<?php

namespace education;

use Illuminate\Database\Eloquent\Model;

class MensajeCorreo extends Model
{
    protected $table = 'mensajes_correo';
}
