<?php

namespace education;

use Illuminate\Database\Eloquent\Model;
use DB;
use education\Usuarios;
class Personas extends Model
{
    protected $table = 'PERSONAS';
    protected $table2 = 'users';

    

    /*
      Metodo para agregar un nuevo registro a la base de datos
    */
    public static function setPersona($nombres, $apellidos, $cui, $direccion, $nacimiento, $telefono, $telefono2, $puesto, $correo)
    {
      return DB::select('CALL nueva_persona(?, ?, ?, ?, ?, ?, ?, ?, ?)', array($nombres, $apellidos, $cui, $direccion, $nacimiento, $telefono, $telefono2, $puesto, $correo));
    }

    /*
      Metodo para obtener los datos de una persona por su id
    */
    public static function findPersona($id)
    {
      return PERSONAS::join('PUESTOS AS p', 'PERSONAS.id_puesto', '=', 'p.id_puesto')
      ->where('id_persona', $id)->first();
    }

        public static function findUltimaPersona()
    {
      return PERSONAS::orderBy('id_persona', 'DESC')->first();

      /*select id_persona from PERSONAS order by id_persona desc limit 1*/
    }

    /*
      Metodo para actualizar los datos de una persona
    */
    public static function updatePersona($id, $nombres, $apellidos, $cui, $direccion, $nacimiento, $telefono, $telefono2, $puesto, $correo)
    {
      return DB::select('CALL actualizar_persona(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', array($id, $nombres, $apellidos, $cui, $direccion, $nacimiento, $telefono, $telefono2, $puesto, $correo));
    }

    /*
      Metodo para camibar el estado de un registro de la tabla PERSONAS
    */
    public static function statePersona($id, $estado)
    {
      return DB::select('CALL estado_persona(?, ?)', array($id, $estado));
    }

    /*
      Metodo para buscar a las personas que tengan una coincidencia en el nombre o apellido
    */
    public static function getPersonasLike($q)
    {
       /*return PERSONAS::join('puestos AS p', 'PERSONAS.id_puesto', '=', 'p.id_puesto')

                     ->where('PERSONAS.apellidos_persona', 'like', '%'.$coincidencia.'%')

                     ->orWhere('PERSONAS.nombres_persona', 'like', '%'.$coincidencia.'%')

                     ->orWhere('PERSONAS.correo_persona', 'like', '%'.$coincidencia.'%')

                     ->orWhere('PERSONAS.nombre_puesto', 'like', '%'.$coincidencia.'%')

                     ->select('PERSONAS.nombres_persona', 'PERSONAS.apellidos_persona', 'PERSONAS.id_persona','p.nombre_puesto as puesto')

                     ->get();*/

                     /*return permission_role::join('permissions AS so', 'permission_role.permission_id', '=', 'so.id')
                         ->where('role_id', $rol)
                         ->select('so.name', 'permission_role.id_permission_role', 'permission_role.state_permission_role')
                         ->get();*/

                          return PERSONAS::join('PUESTOS as p', 'PERSONAS.id_puesto', '=', 'p.id_puesto')
                     
                     ->where(function ($query) use ($q) {
                         $query->where('apellidos_persona', 'like', '%'.$q.'%')
                               ->orWhere('nombres_persona', 'like', '%'.$q.'%');
                     })
                     ->where ('PERSONAS.estado_persona','=',1)
                     ->select('nombres_persona', 'apellidos_persona', 'id_persona', 'p.nombre_puesto')
                     ->get();
    }
 

//funcion que Buscar persona por nombre, apellido, telefono, correo o puesto cj
   



    /*Metodo para buscar a las personas que coincidan con el paramtreo de busqueda y que tenga el puesto de docente*/
    public static function getPersonasLikeDocente($q)
    {
      /*return PERSONAS::join('PUESTOS as p', 'PERSONAS.id_puesto', '=', 'p.id_puesto')
                     ->where('nombre_puesto', '=', 'docente')
                     //->orWhere('nombre_persona', 'like', '%'.$q.'%')
                     //->where([['p.nombre_puesto', '=', 'docente'], ['apellidos_persona', 'like', '%'.$q.'%'], ['nombre_persona', 'like', '%'.$q.'%']])
                    //->where([['apellidos_persona', 'like', '%'.$q.'%'], 'OR',['nombres_persona', 'like', '%'.$q.'%']])
                     //->orWhere('nombres_persona', 'like', '%'.$q.'%')
                     ->select('nombres_persona', 'apellidos_persona', 'id_persona')
                     ->get();*/
      return PERSONAS::join('PUESTOS as p', 'PERSONAS.id_puesto', '=', 'p.id_puesto')
                     ->where('nombre_puesto', '=', 'docente')
                     ->where(function ($query) use ($q) {
                         $query->where('apellidos_persona', 'like', '%'.$q.'%')
                               ->orWhere('nombres_persona', 'like', '%'.$q.'%');
                     })
                     ->select('nombres_persona', 'apellidos_persona', 'id_persona')
                     ->get();
    }

    public static function getBuscaUsuario($q)
    {
      /*r return Usuarios::join('role_user as ru', 'Usuarios.id', '=', 'ru.user_id')
      ->join('roles as r', 'ru.role_id', '=', 'r.id')
      ->where(function ($query) use ($q) {
                         $query->where('Usuarios.name', 'like', '%'.$q.'%')
                               ->orWhere('Usuarios.email', 'like', '%'.$q.'%');
                     })
                     ->select('Usuarios.name as name', 'Usuarios.email as email', 'Usuarios.id as id')
                     ->get();*/
      return Usuarios::where(function ($query) use ($q) {
                         $query->where('name', 'like', '%'.$q.'%')
                               ->orWhere('email', 'like', '%'.$q.'%');
                     })

                     ->select('name', 'email', 'id')
                     ->distinct('id')
                     ->get();


        /*$usuarios=DB::table('users as u')->join('role_user AS ru', 'u.id', '=', 'ru.user_id')
          ->join('roles as r', 'ru.role_id', '=', 'r.id')
        
          ->select('u.name as nombre', 'u.email as correo', 'r.name as rol', 'u.id as id')


          ->where('u.name','LIKE', '%'.$q.'%')
          ->orWhere('u.email','LIKE', '%'.$q.'%')
          ->orWhere('r.name','LIKE', '%'.$q.'%')
          ->where('u.estado_usuario','=','1')
          ->orderBy('u.id', 'asc')
          ->paginate(10);*/


                      
    }



    /*Model::where(function ($query) {
    $query->where('a', '=', 1)
          ->orWhere('b', '=', 1);
})->where(function ($query) {
    $query->where('c', '=', 1)
          ->orWhere('d', '=', 1);
});*/
}
