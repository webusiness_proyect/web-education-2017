<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;

use education\Http\Requests\ActividadesRequest;
use education\Http\Controllers\menuController;
use education\Repositories\RespondeActividadesRepository;
use education\Repositories\actividadesRepository;
use education\Repositories\unidadesRepository;
use education\Repositories\userRepository;
use education\actividad;
use education\User;
use Session;
use Auth;
use DB;

class misActividadesController extends Controller
{
  protected $menu;
  protected $actividades;
  protected $RespondeActividades;
  protected $unidades;
  protected $users;
public function __construct(actividadesRepository $actividad, RespondeActividadesRepository $respondeactividad,unidadesRepository $unidad, userRepository $User )
  {
    $m = new menuController();
    $this->menu = $m->index();
    $this->actividades = $actividad;
    $this->RespondeActividades = $respondeactividad;
    $this->unidades = $unidad;
    $this->users = $User;
  }

   public function index(Request $request)
    {

        $this->actividades->id_area = $request['a'];
        $this->actividades->id_asignacion_area = $request['a'];
        $this->actividades->id_actividad = $request['c'];
        $this->RespondeActividades->id = $request['c'];
        $this->actividades->id_usuario =(Auth::user()->id);
        $this->unidades->fecha_inicio = date('Y-m-d');//fecha inicial de la unidad
        $this->unidades->fecha_final = date('Y-m-d');//fecha final de la unidad
        $unidad = $this->unidades->getUnidadActual();//obtenemos la unidad disponible de acuerdo a la fecha
        $this->actividades->id_unidad=$unidad['id_unidad'];
        $items = $this->actividades->getMiActividadCursounidad();//tareas entregadas
        $items2 = $this->actividades->getMiActividadCursoSinEntregar();//tareas sin entregar
        $items3 = $this->actividades->getMiActividadCursoAnteriores();//tareas anteriores entregadas
        $items4 = $this->actividades->getMiActividadCursoAnterioresSinEntregar();//tareas anteriores sin entrega
            $area=DB::table('AREAS')
          ->select('AREAS.nombre_area')
          ->where('id_area','=',$request['a'])
          ->first();
        


        return view('misactividades.index', [ 'items'=>$this->menu, 'actividades'=>$items,'ractividades'=>$items2, 'anteriores'=>$items3, 'anterioressinentrega'=>$items4, 'area'=>$area, 'curso'=>$request['a'],'curso'=>$request['b'], 'curso'=>$request['c']]);
    }

}
