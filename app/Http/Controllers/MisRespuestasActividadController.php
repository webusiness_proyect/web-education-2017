<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;


use education\Http\Requests;
use education\Http\Requests\MisRespuestasActividadRequest;
use education\Http\Controllers\menuController;
use education\Repositories\RespondeActividadesRepository;
use education\Repositories\actividadesRepository;
use education\Repositories\unidadesRepository;
use education\RespondeActividad;
use education\actividad;
use education\User;
use education\file;
use Session;
use Auth;
use DB;

class MisRespuestasActividadController extends Controller
{
   	protected $menu;
    protected $actividades;
  	protected $ractividades;
  	protected $unidades;
  	public function __construct(RespondeActividadesRepository $ractividad, unidadesRepository $unidad, actividadesRepository $actividad)
  {
    $m = new menuController();
    $this->menu = $m->index();
    $this->ractividades = $ractividad; 
    $this->unidades = $unidad;
    $this->actividades = $actividad;
  }

   public function index(Request $request)
    {
        
        $this->ractividades->id_actividad = $request['a'];
        $this->actividades->id_area = $request['a'];
        $items = $this->ractividades->getUsuariosRespuestActividadesFiles();
        $items1 = $this->ractividades->valorMaximoNota();
        $items2 = $this->actividades->getTareasSinEntregar();
        $area=DB::table('AREAS as a')
            ->join('ACTIVIDADES as ac','a.id_area','=','ac.id_area')
             ->where('ac.id_actividad','=',$request['a'])
             ->select('a.nombre_area')
             ->get();//traingo informacion de unidades
        return view('misrespuestasactividades.index', ['items'=>$this->menu, 'ractividades'=>$items, 'notas'=>$items1, 'sinentrega'=>$items2, 'curso'=>$request['a'], 'area'=>$area]);
        /*
        $this->ractividades->id_actividad = $request['a'];
        $items = $this->ractividades->valorMaximoNota();
        return view('misrespuestasactividades.index', ['items'=>$this->menu, 'ractividades'=>$items, 'curso'=>$request['a']]);
        */
    }

    
      public function create(Request $request)
    {
        return view('misrespuestasactividades.nractividad', ['items'=>$this->menu, 'curso'=>$request['a']]);

            
    }



      public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$this->authorize('owner', $foros);
        $this->ractividades->id = $id;
        

        
        


        $items1 = $this->ractividades->valorMaximoNota();
        //$ractividad = $this->ractividades->getRespuestaActividadesById1();
        $ractividad = $this->ractividades->getRespuestaActividadesById1File();
        //$valor_maximo_nota = $this->ractividades->valorMaximoNota();
        return view('misrespuestasactividades.eractividad', ['items'=>$this->menu, 'ractividad'=>$ractividad, 'notas'=>$items1]);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MisRespuestasActividadRequest $request, $id)


    {


        
        
          //return back();
       /*

       $this->ractividades->id = $id;//asignar el id de la tarea actual
        $ractividades = $this->ractividades->getRespuestaActividadesById1();//
       
        $this->ractividades->estado_entrega_actividad = $request['estado_entrega_actividad'];//cambio estado a calificado
        $this->ractividades->observaciones_maestro = $request['observaciones_maestro'];


       
        $nota=$this->ractividades->calificacion = $request['calificacion'];
     

        $respuesta=$this->ractividades->getIdRespuestaActividadFile();


        $id_respuesta=$this->ractividades->id_respuesta_actividad=$respuesta['id_respuesta_actividad'];
       
       $validacion=$this->ractividades->getValidarNotaIngresada($id_respuesta, $nota);


        
       
        if(count($validacion)==0)
        {

        $this->ractividades->updateRespuestaActividades();
        
        Session::flash('mensaje', 'Se ha calificado exitosamente la tarea!!!');
        return redirect('/misrespuestasactividades');
       

        
        }

        else
        {

           Session::flash('mensaje', 'NO SE PUEDE AGREGAR NOTA, calificacion mayor a la que se puede dar!!!');
        return redirect('/misrespuestasactividades');               
        //return back()->withInput();             

        


        }
                  

*/

        

        $this->ractividades->id = $id;//asignar el id de la tarea actual
        $ractividades = $this->ractividades->getRespuestaActividadesById1();//
        $id_actividad=$this->ractividades->id_actividad;
       
        $this->ractividades->estado_entrega_actividad = $request['estado_entrega_actividad'];
        $this->ractividades->calificacion = $request['calificacion'];
        $this->ractividades->observaciones_maestro = $request['observaciones_maestro'];
       // $this->ractividades->fecha_calificacion = 
        $this->ractividades->updateRespuestaActividadesFile();
        Session::flash('mensaje', 'Se ha calificado la ACTIVIDAD exitosamente!!!');
        return redirect()->route('misactividadesdocente.index', [$id_actividad]);
      
       


                

            }



             
            

       // }
     //}

        

        

     

    



         


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
