<?php


namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Http\Requests\RespondeActividadesRequest;
use education\Http\Controllers\menuController;
use education\Repositories\RespondeActividadesRepository;
use education\Repositories\actividadesRepository;
use education\Repositories\unidadesRepository;
use education\RespondeActividad;
use education\User;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;

use education\file;


class RespondeActividadController extends Controller
{
    protected $menu;
    protected $actividades;
    protected $ractividades;
    protected $unidades;
    public function __construct(RespondeActividadesRepository $ractividad, unidadesRepository $unidad, actividadesRepository $actividad)
  {
    $m = new menuController();
    $this->menu = $m->index();
    $this->ractividades = $ractividad; 
    $this->unidades = $unidad;
    $this->actividades = $actividad;
  }

  public function index(Request $request)
    {
        
        $this->ractividades->id_actividad = $request['a'];
        $items = $this->actividades->getActividadCurso();
        return view('respuestaactividades.index', ['items'=>$this->menu, 'actividades'=>$items, 'actividad'=>$request['a'],'curso'=>$request['b']]);
    }

      public function create(Request $request)
    {
         
        return view('respuestaactividades.nractividad', ['items'=>$this->menu, 'curso'=>$request['actividad']]);

            
    }
    
     public function store(RespondeActividadesRequest $request)
    {

      

        $user = new file;
        $user->title= Input::get('comentarios');
        $nombre_servidor=request()->server->get('SERVER_NAME');
        $user->url=$nombre_servidor;
        if(Input::hasFile('ruta'))
        {
            
            //UNIDAD
            $this->unidades->fecha_inicio = date('Y-m-d');//fecha inicio
            $this->unidades->fecha_final = date('Y-m-d');// fecha fin
            $unidad=$this->unidades->getUnidadActual();//traigo la unidad entre fecha actual
            $unidades=$user->unidad=$this->ractividades->unidad=$unidad['id_unidad'];//traigo id unidad
            $id_usuario=$user->id_usuario=$this->ractividades->id_usuario = Auth::user()->id;// user
            $id_actividad=$user->id_actividad=$request['curso'];//agrego curso url
            $this->ractividades->id_actividad=$id_actividad;
           $areas=$this->ractividades->getIdActividad();
           $area=$user->area=$this->ractividades->area=$areas['id_area'];
           $val_max=$user->valor_max=$areas['nota_total'];      
           $nota_max = $this->ractividades->existRespuestaActividadesFiles($id_actividad,$id_usuario,$unidades,$area);//traigo todos los datos de las notas

           /*DATOS DE LOS ARCHIVOS A SUBIR*/
            $destinationPath = request()->server->get('SERVER_NAME').'/img'; // upload path
            $file=Input::file('ruta');
            $urlimg=$file->getClientOriginalName();

            $file->move(public_path().'/img/',$id_usuario.$urlimg); // subir archivo a una url
            
            $archivo=$user->name='/img/'.$id_usuario.$urlimg;
            $size=$user->size=$file->getClientsize();
            $type=$user->type=$file->getClientMimeType();


        

           if(count($nota_max)==0)//si no se subio la tarea
            {

            if($type=='image/png'&&$size<='100000')
            {

                $user->save();

                 Session::flash('mensaje', 'Haz Entregado la tarea exitosamente!!!');
             
                 return redirect('todasmisactividades')->with('success-message', 'imagen png subido exitosamente');
                

            }


            elseif ($type=='image/jpeg'&&$size<='100000')

                 {

                $user->save();

                Session::flash('mensaje', 'Haz Entregado la tarea exitosamente!!!');
             
                 
                 return redirect('todasmisactividades')->with('success-message', 'imagen jpg subido exitosamente');
                

            }

             elseif ($type=='image/jpg'&&$size<='100000')

                 {

                $user->save();
                Session::flash('mensaje', 'Haz Entregado la tarea exitosamente!!!');
             
                 
                 return redirect('todasmisactividades')->with('success-message', 'imagen jpg subido exitosamente');
                

            }

            elseif($type=='application/pdf'&&$size<='100000')
            {
                $user->save();

                 Session::flash('mensaje', 'Haz Entregado la tarea exitosamente!!!');
             
                 
                 return redirect('todasmisactividades')->with('success-message', 'pdf subido exitosamente');
            }

            elseif($type=='application/vnd.openxmlformats'&&$size<='100000')
            {
                $user->save();

                  Session::flash('mensaje', 'Haz Entregado la tarea exitosamente!!!');
             
                 
                 return redirect('todasmisactividades')->with('success-message', 'pdf subido exitosamente');
            }


            elseif($type=='application/vnd.openxmlformats-officedocument.wordprocessingml.document'&&$size<='100000')
            {
                $user->save();

                  Session::flash('mensaje', 'Haz Entregado la tarea exitosamente!!!');
             
                 
                 return redirect('todasmisactividades')->with('success-message', 'documento subido exitosamente');
            }

            elseif($type=='application/msword'&&$size<='100000')
            {
                $user->save();

                  Session::flash('mensaje', 'Haz Entregado la tarea exitosamente!!!');
             
                 
                 return redirect('todasmisactividades')->with('success-message', 'documento subido exitosamente');
            }

            elseif($type=='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'&&$size<='100000')
            {
                $user->save();

                  Session::flash('mensaje', 'Haz Entregado la tarea exitosamente!!!');
             
                 
                 return redirect('todasmisactividades')->with('success-message', 'excel subido exitosamente');
            }


            elseif($type=='application/vnd.openxmlformats-officedocument.presentationml.presentation'&&$size<='100000')
            {
                $user->save();

                  Session::flash('mensaje', 'Haz Entregado la tarea exitosamente!!!');
             
                 
                 return redirect('todasmisactividades')->with('success-message', 'excel subido exitosamente');
            }

            else
            {
                 Session::flash('mensaje', 'ESTE TIPO DE ARHVIOS NO ES PERMITIDO O ES MUY GRANDE PARA PODER SER ENTREGADO');
             
                 
                 return redirect('respuestaactividades')->with('error-message', 'no se permite este tipo de archivos'); 

            }
        }
        else //de lo contrario ya se entrego la tarea y no se puede subir 2 veces la misma
         {
           Session::flash('mensaje', 'YA ENTREGASTE ESTA TAREA, No puedes entregar dos veces la misma tarea preguntale a tu profesor!!!');
          
          return redirect('/respuestaactividades');
         }
        

        
      } 

  }    
}