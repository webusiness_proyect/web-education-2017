<?php



namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;

use education\Http\Controllers\menuController;

use education\Sub_opciones;

use education\User;

use education\Roles;

use education\Estudiantes;

use education\log_notificaciones;

use education\Repositories\HorarioRepository;

use education\Repositories\NotificacionesRepository;

use Auth;

use DB;

use education\Repositories\unidadesRepository;

use Carbon\Carbon;

use Session;



class principalController extends Controller

{

      protected $menu;

      protected $unidades;

       protected $horarios;

       protected $notificaciones;

      public function __construct(menuController $m, unidadesRepository $unidad, HorarioRepository $horario, NotificacionesRepository $notificacion)

      {

        //$m = new menuController();

        $this->menu = $m->index();

        $this->unidades = $unidad;

        $this->horarios = $horario;

        $this->notificaciones = $notificacion;

      }

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(Request $request)

    {
/*
         $items = $this->menu;

               return view('bienvenida.index', compact('items'));
               */
               
      $items = $this->menu;

      $this->unidades->fecha_inicio = date('Y-m-d');//fecha inicial de la unidad


      $this->unidades->fecha_final = date('Y-m-d');//fecha final de la unidad


      $unidad = $this->unidades->getUnidadActual();//obtenemos la unidad disponible de acuerdo a la fecha

      $permisos = User::getPermisosRol(Auth::user()->id);
      if($unidad!=null)
      {

      foreach ($permisos as $key => $permiso) {

        switch ($permiso->id) {

          case '1':

          return view('bienvenida.index', compact('items','unidad','query'));
         
            break;

          case '2'://docente

               $user=$this->horarios->user = Auth::user()->id;//traigo el id 

               //actividades entregadas pendientes de calificar
               /*select count(*) from files as f join users as u on u.id=f.id_usuario
                  join ESTUDIANTES AS e on e.id=u.id
                  join ACTIVIDADES as ac on f.id_actividad=ac.id_actividad
                  where ac.id_usuario=3
                  and ac.id_unidad=7
                  and f.estado_entrega_actividad="ENTREGADO"
                  */
            


                  $pendientesentrega=DB::table('files as f')
                  ->join('users as u','u.id','=','f.id_usuario')
                  ->join('ESTUDIANTES AS e','e.id','=','u.id')
                  ->join('ACTIVIDADES as ac','f.id_actividad','=','ac.id_actividad')
                  ->select(DB::raw('COUNT(*) as newpendientes'))
                  ->where('ac.id_usuario','=', Auth::user()->id)
                  ->where('ac.id_unidad','=', $unidad->id_unidad)
                  ->where('f.estado_entrega_actividad','=', 'ENTREGADO')
                  ->first();// traigo los niveles asignados por el docente

               $niveles=DB::table('USUARIO_PERSONA as up')
                  ->join('ASIGNACION_DOCENTE as ad','up.id_persona','=','ad.id_persona')
                  ->join('ASIGNACION_AREAS as aa','ad.id_asignacion_area','=','aa.id_asignacion_area')
                  ->select(DB::raw('DISTINCT aa.id_nivel_grado as nivel'))
                  ->where('up.user_id','=', Auth::user()->id)
                  ->first();// traigo los niveles asignados por el docente
               $minivel=$niveles->nivel;
               $visitado=DB::table('log_notificaciones')
                ->select(DB::raw('COUNT(*) as visita'))
                ->where('log_notificaciones.usuario','=', Auth::user()->id)
                ->first();// verifico si ha visto notificaciones
               $visita=$visitado->visita;

          if($visita==0)// si no ha visitado
          {
          

              $notificacionesnuevas=DB::table('NOTIFICACIONES as n')
              ->select(DB::raw('COUNT(*) as newnot'))
              ->where('n.id_rol','=', 2)
              ->where('n.id_nivel_grado','=', $minivel)
              ->where('n.fecha_create','>=','2017-01-16 10:06:14')
              ->get();
              //tomo todas las actividades de este a;o

          }
          else
          {
                $ultimafechavisita=DB::table('log_notificaciones')
                ->select('log_notificaciones.fecha_revision_notificacion')
                ->where('log_notificaciones.usuario','=', Auth::user()->id)
                ->orderBy('log_notificaciones.fecha_revision_notificacion', 'desc')
                ->first();// traigo el ultimo ingreso a notificaciones de este usuario
                $ultfisit=$ultimafechavisita->fecha_revision_notificacion;
    
              $notificacionesnuevas=DB::table('NOTIFICACIONES as n')
              ->select(DB::raw('COUNT(*) as newnot'))
              ->where('n.id_rol','=', 2)
              ->where('n.id_nivel_grado','=', $minivel)
              ->where('n.fecha_create','>=',$ultimafechavisita->fecha_revision_notificacion)
              ->get();

          }

              $dat = Carbon::now();
              $date = $dat->format('l jS \\of F Y');
              $today=$dat->format('l');
              $docente=$this->horarios->getDocentes();
              $id=$this->horarios->id_area=$docente['id_persona'];

              if($today=='Monday')
          {
            $hoy='1';
            $dia='Lunes';
           $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
          ->join('ASIGNACION_AREAS AS AA', 'AA.id_area', '=', 'ar.id_area')
          ->join('ASIGNACION_DOCENTE AS AD', 'AD.id_asignacion_area', '=', 'AA.id_asignacion_area')
          ->join('PERSONAS AS P', 'AD.id_persona', '=', 'P.id_persona')
          ->join('users AS U', 'U.id', '=', 'P.id_persona')
      
        
          ->select(DB::raw('DISTINCT(ar.nombre_area)'), 'c.dia','c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')


           ->where('c.estado_actividad_calendario','=','1')
          ->where('P.id_persona','=',$id)
          ->where('c.dia','=','1')
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexprofesor', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas, 'pendientesentrega'=>$pendientesentrega]);
        }

        elseif($today=='Tuesday')
        {
            $hoy='2';
            $dia='Martes';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
          ->join('ASIGNACION_AREAS AS AA', 'AA.id_area', '=', 'ar.id_area')
          ->join('ASIGNACION_DOCENTE AS AD', 'AD.id_asignacion_area', '=', 'AA.id_asignacion_area')
          ->join('PERSONAS AS P', 'AD.id_persona', '=', 'P.id_persona')
          ->join('users AS U', 'U.id', '=', 'P.id_persona')
      
        
          ->select(DB::raw('DISTINCT(ar.nombre_area)'), 'c.dia','c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')


           ->where('c.estado_actividad_calendario','=','1')
          ->where('P.id_persona','=',$id)
          ->where('c.dia','=','2')
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexprofesor', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas, 'pendientesentrega'=>$pendientesentrega]);
        }
        elseif($today=='Wednesday')
        {
            $hoy='3';
            $dia='Miercoles';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
          ->join('ASIGNACION_AREAS AS AA', 'AA.id_area', '=', 'ar.id_area')
          ->join('ASIGNACION_DOCENTE AS AD', 'AD.id_asignacion_area', '=', 'AA.id_asignacion_area')
          ->join('PERSONAS AS P', 'AD.id_persona', '=', 'P.id_persona')
          ->join('users AS U', 'U.id', '=', 'P.id_persona')
      
        
          ->select(DB::raw('DISTINCT(ar.nombre_area)'), 'c.dia','c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')


           ->where('c.estado_actividad_calendario','=','1')
          ->where('P.id_persona','=',$id)
          ->where('c.dia','=','3')
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexprofesor', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas, 'pendientesentrega'=>$pendientesentrega]);
        }

        elseif($today=='Thursday')
        {
            $hoy='4';
            $dia='Jueves';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
          ->join('ASIGNACION_AREAS AS AA', 'AA.id_area', '=', 'ar.id_area')
          ->join('ASIGNACION_DOCENTE AS AD', 'AD.id_asignacion_area', '=', 'AA.id_asignacion_area')
          ->join('PERSONAS AS P', 'AD.id_persona', '=', 'P.id_persona')
          ->join('users AS U', 'U.id', '=', 'P.id_persona')
      
        
          ->select(DB::raw('DISTINCT(ar.nombre_area)'), 'c.dia','c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')


           ->where('c.estado_actividad_calendario','=','1')
          ->where('P.id_persona','=',$id)
          ->where('c.dia','=','4')
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexprofesor', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas, 'pendientesentrega'=>$pendientesentrega]);
        }

        elseif($today=='Friday')
        {
            $hoy='5';
            $dia='Viernes';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
          ->join('ASIGNACION_AREAS AS AA', 'AA.id_area', '=', 'ar.id_area')
          ->join('ASIGNACION_DOCENTE AS AD', 'AD.id_asignacion_area', '=', 'AA.id_asignacion_area')
          ->join('PERSONAS AS P', 'AD.id_persona', '=', 'P.id_persona')
          ->join('users AS U', 'U.id', '=', 'P.id_persona')
      
        
          ->select(DB::raw('DISTINCT(ar.nombre_area)'), 'c.dia','c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')


           ->where('c.estado_actividad_calendario','=','1')
          ->where('P.id_persona','=',$id)
          ->where('c.dia','=','5')
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexprofesor', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas, 'pendientesentrega'=>$pendientesentrega]);
        }

        elseif($today=='Saturday')
        {
            $hoy='6';
            $dia='Sabado';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
          ->join('ASIGNACION_AREAS AS AA', 'AA.id_area', '=', 'ar.id_area')
          ->join('ASIGNACION_DOCENTE AS AD', 'AD.id_asignacion_area', '=', 'AA.id_asignacion_area')
          ->join('PERSONAS AS P', 'AD.id_persona', '=', 'P.id_persona')
          ->join('users AS U', 'U.id', '=', 'P.id_persona')
      
        
          ->select(DB::raw('DISTINCT(ar.nombre_area)'), 'c.dia','c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')


           ->where('c.estado_actividad_calendario','=','1')
          ->where('P.id_persona','=',$id)
          ->where('c.dia','=','6')
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexprofesor', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas, 'pendientesentrega'=>$pendientesentrega]);
        }
         elseif($today=='Sunday')
        {
            $hoy='7';
            $dia='Domingo';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
          ->join('ASIGNACION_AREAS AS AA', 'AA.id_area', '=', 'ar.id_area')
          ->join('ASIGNACION_DOCENTE AS AD', 'AD.id_asignacion_area', '=', 'AA.id_asignacion_area')
          ->join('PERSONAS AS P', 'AD.id_persona', '=', 'P.id_persona')
          ->join('users AS U', 'U.id', '=', 'P.id_persona')
      
        
          ->select(DB::raw('DISTINCT(ar.nombre_area)'), 'c.dia','c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')


           ->where('c.estado_actividad_calendario','=','1')
          ->where('P.id_persona','=',$id)
          ->where('c.dia','=','7')
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexprofesor', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas, 'pendientesentrega'=>$pendientesentrega]);
        }
            
            break;
          case '3':  // padres


        $this->unidades->fecha_inicio = date('Y-m-d');//fecha inicial de la unidad
        $this->unidades->fecha_final = date('Y-m-d');//fecha final de la unidad
        $unidad = $this->unidades->getUnidadActual();//obtenemos la unidad disponible de acuerdo a la fecha
        $thisunidad=$unidad->id_unidad;

        

          //query para traer todos las actividades a mi cargo lochoa
          /*
              SELECT ac.nombre_actividad 
              FROM roles as r join role_user as ru on r.id=ru.role_id 
              join users as u on ru.user_id=u.id 
              join TUTOR as t on u.id=t.id 
              join TUTOR_ESTUDIANTE as te on t.id_tutor=te.id_tutor
              join ESTUDIANTES as e on e.id_estudiante=te.id_estudiante
              join ACTIVIDADES AS ac on ac.id_asignacion_area=e.id_nivel_grado
          where t.id=417
          */
                     $datos=DB::table('roles as r')
                  ->join('role_user as ru','r.id','=','ru.role_id')
                  ->join('users as u','ru.user_id','=','u.id')
                  ->join('TUTOR as t','u.id','=','t.id')
                  ->join('TUTOR_ESTUDIANTE as te','t.id_tutor','=','te.id_tutor')
                  ->join('ESTUDIANTES as e','e.id_estudiante','=','te.id_estudiante')
                  ->join('ACTIVIDADES AS ac','ac.id_asignacion_area','=','e.id_nivel_grado')
                  ->select('ac.nombre_actividad', 'e.id', 'e.nombre_estudiante','e.id_nivel_grado')
                  ->where('t.id','=', Auth::user()->id)
                  
                  ->get();// traigo los niveles asignados por el docente

                  $datos2=Roles::join('role_user as ru','roles.id','=','ru.role_id')
                  ->join('users as u','ru.user_id','=','u.id')
                  ->join('TUTOR as t','u.id','=','t.id')
                  ->join('TUTOR_ESTUDIANTE as te','t.id_tutor','=','te.id_tutor')
                  ->join('ESTUDIANTES as e','e.id_estudiante','=','te.id_estudiante')
                  ->join('ACTIVIDADES AS ac','ac.id_asignacion_area','=','e.id_nivel_grado')
                  ->select('ac.nombre_actividad', 'e.id', 'e.nombre_estudiante','e.id_nivel_grado')
                  ->where('t.id','=', Auth::user()->id)
                  
                  ->first();

                  $hijo=$datos2->id;
                  $nivel=$datos2->id_nivel_grado;

                       /* NUEVAS ACTIVIDADES SIN ENTREGAR DE SUS HIJOS */
          /*  Select count(*) from ACTIVIDADES as a  
                  where not exists 
                  (select * from files as f 
                  where f.id_actividad = a.id_actividad 
                  and f.Id_usuario=17)
                  and a.id_unidad=7
          and a.id_asignacion_area=4*/         

             $pendientesentrega= DB::table('ACTIVIDADES as a')
                  ->whereNotExists(function($query) use ($hijo)
                    {
                        $query->from('files as f')
                              ->whereRaw('f.id_actividad = a.id_actividad')         
                              ->where('f.id_usuario', '=', $hijo);
                    })  
                     
                    ->where('a.id_asignacion_area',intval($nivel))
                    ->where('a.id_unidad',$thisunidad)
                    ->select(DB::raw('COUNT(*) as newpendientes'))
                    ->first();

            
              
               $visitado=DB::table('log_notificaciones')
                ->select(DB::raw('COUNT(*) as visita'))
                ->where('log_notificaciones.usuario','=', Auth::user()->id)
                ->first();// verifico si ha visto notificaciones
               $visita=$visitado->visita;

          if($visita==0)// si no ha visitado
          {
          

              $notificacionesnuevas=DB::table('NOTIFICACIONES as n')
              ->select(DB::raw('COUNT(*) as newnot'))
              ->where('n.id_rol','=', 3)
              ->where('n.id_nivel_grado','=', $nivel)
              ->where('n.fecha_create','>=','2017-01-16 10:06:14')
              ->get();
              //tomo todas las actividades de este a;o

          }
          else
          {
                $ultimafechavisita=DB::table('log_notificaciones')
                ->select('log_notificaciones.fecha_revision_notificacion')
                ->where('log_notificaciones.usuario','=', Auth::user()->id)
                ->orderBy('log_notificaciones.fecha_revision_notificacion', 'desc')
                ->first();// traigo el ultimo ingreso a notificaciones de este usuario
                $ultfisit=$ultimafechavisita->fecha_revision_notificacion;
    
              $notificacionesnuevas=DB::table('NOTIFICACIONES as n')
              ->select(DB::raw('COUNT(*) as newnot'))
              ->where('n.id_rol','=', 3)
              ->where('n.id_nivel_grado','=', $nivel)
              ->where('n.fecha_create','>=',$ultimafechavisita->fecha_revision_notificacion)
              ->get();

          }



              $dat = Carbon::now();
              $date = $dat->format('l jS \\of F Y');
              $today=$dat->format('l');
              $this->horarios->user=Auth::user()->id;
              $nivelalumnos=$this->horarios->getHijosTutores();

              

              //$id=$nivelalumno['id_nivel_grado']; 

                 $id=$nivelalumnos['id_nivel_grado'];

              /*CALENDARIO HIJOS DE PADRE
              select distinct * from CALENDARIO as c join DIAS as d on c.dia=d.id_dia
          join NIVELES_GRADOS as ng on ng.id_nivel_grado=c.id_nivel_grado
          join GRADOS as g on ng.id_grado=g.id_grado
          join CARRERAS as ca on ng.id_carrera = ca.id_carrera 
          join NIVELES_PLANES_JORNADAS as npj on ng.id_nivel_plan_jornada=npj.id_nivel_plan_jornada
          join NIVELES as n on npj.id_nivel=n.id_nivel
          join JORNADAS as j on npj.id_jornada=j.id_jornada
          join PLANES as pl on npj.id_plan=pl.id_plan
          join AREAS as ar on ar.id_area=c.id_area
          
      
      
           where c.id_nivel_grado=1
           and c.estado_actividad_calendario=1
           and c.dia=1
          order By c.hora_inicia asc*/


               if($today=='Monday')
        {
            $hoy='1';
            $dia='Lunes';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.id_nivel_grado','=', $id)
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=',$hoy)
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexpadre', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas,'pendientesentrega'=>$pendientesentrega]);
        }
        elseif($today=='Tuesday')
        {
            $hoy='2';
            $dia='Martes';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.id_nivel_grado','=', $id)
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=',$hoy)
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexpadre', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas,'pendientesentrega'=>$pendientesentrega]);
        }

        elseif($today=='Wednesday')
        {
            $hoy='3';
            $dia='Miercoles';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.id_nivel_grado','=', $id)
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=',$hoy)
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexpadre', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas,'pendientesentrega'=>$pendientesentrega]);
        }

        elseif($today=='Thursday')
        {
            $hoy='4';
            $dia='Jueves';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.id_nivel_grado','=', $id)
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=',$hoy)
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexpadre', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas,'pendientesentrega'=>$pendientesentrega]);
        }

        elseif($today=='Friday')
        {
            $hoy='5';
            $dia='Viernes';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.id_nivel_grado','=', $id)
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=',$hoy)
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexpadre', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas,'pendientesentrega'=>$pendientesentrega]);
        }

        elseif($today=='Saturday')
        {
            $hoy='6';
            $dia='Sabado';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.id_nivel_grado','=', $id)
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=',$hoy)
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexpadre', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas,'pendientesentrega'=>$pendientesentrega]);
        }

         elseif($today=='Sunday')
        {
            $hoy='7';
            $dia='Domingo';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.id_nivel_grado','=', $id)
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=',$hoy)
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexpadre', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas,'pendientesentrega'=>$pendientesentrega]);
        }
  

                ;

            break;

          case '4':// alumnos
         

              $this->horarios->user = Auth::user()->id;//agrego mi codigo
              $niveles=$this->horarios->getNivelEstudiantes();//traigo todos mis datos
              $id=$this->horarios->id_area=$niveles['id_nivel_grado'];//traigo mi nivel
              $dat = Carbon::now();//fecha actual
              $date = $dat->format('l jS \\of F Y');//formato de fecha
              $today=$dat->format('l');// solo dia



        /* nuevas notificaciones */
         /* query que muestra todas las notificaciones que no se han visto desde
          la ultima visita
          SELECT count(*) FROM `notificaciones` as n WHERE id_rol=2 and id_nivel_grado=20 and n.fecha_create>'2017-01-27 10:29:24'
          */

             $nivel=DB::table('ESTUDIANTES')
              ->select('ESTUDIANTES.id','ESTUDIANTES.id_nivel_grado')
              ->where('ESTUDIANTES.id','=', Auth::user()->id)
              ->where('estado_estudiante','=','1')
              ->first();// traigo el nivel del estudiante

               $datos2=Estudiantes::where('id','=', Auth::user()->id)
              ->where('estado_estudiante','=','1')
              ->select('ESTUDIANTES.id','ESTUDIANTES.id_nivel_grado')
              ->first();

                  
              $minivel=$datos2->id_nivel_grado;

              
          
                  
              $ultimafechavisita=DB::table('log_notificaciones')
              ->select('log_notificaciones.fecha_revision_notificacion')
              ->where('log_notificaciones.usuario','=', Auth::user()->id)
              ->orderBy('log_notificaciones.fecha_revision_notificacion', 'desc')
              ->first();// traigo mi ultima fecha de visita notificaciones

                $visitado=DB::table('log_notificaciones')
                ->select(DB::raw('COUNT(*) as visita'))
                ->where('log_notificaciones.usuario','=', Auth::user()->id)
                ->first();// verifico si ha visto notificaciones
               $visita=$visitado->visita;

          if($visita==0)// si no ha visitado
          {

             $notificacionesnuevas=DB::table('NOTIFICACIONES as n')
          ->select(DB::raw('COUNT(*) as newnot'))
          ->where('n.id_rol','=', 4)
          ->where('n.id_nivel_grado','=', $minivel)
          ->where('n.fecha_create','>=','2017-01-16 10:06:14')
          ->get();
          }

          else
          {
               $notificacionesnuevas=DB::table('NOTIFICACIONES as n')
          ->select(DB::raw('COUNT(*) as newnot'))
          ->where('n.id_rol','=', 4)
          ->where('n.id_nivel_grado','=', $minivel)
          ->where('n.fecha_create','>=',$ultimafechavisita->fecha_revision_notificacion)
          ->get();

          }

        
        $this->unidades->fecha_inicio = date('Y-m-d');//fecha inicial de la unidad
        $this->unidades->fecha_final = date('Y-m-d');//fecha final de la unidad
        $unidad = $this->unidades->getUnidadActual();//obtenemos la unidad disponible de acuerdo a la fecha
        $thisunidad=$unidad->id_unidad;

        
        $nivel=$this->horarios->getNivelEstudiantes();
        $miarea=$nivel->id_asignacion_area;

        $actividades= DB::table('ACTIVIDADES as a')
                  ->whereNotExists(function($query) 
                    {
                        $query->from('files as f')
                              ->whereRaw('f.id_actividad = a.id_actividad')         
                              ->where('f.id_usuario', '=', Auth::user()->id);
                    })  
                     
                    ->where('a.id_asignacion_area',intval($minivel))
                    ->where('a.id_unidad',$thisunidad)
                    ->select(DB::raw('COUNT(*) as newact'))
                    ->first();
          /* NUEVAS ACTIVIDADES SIN ENTREGAR */
          /*  Select count(*) from ACTIVIDADES as a  
                  where not exists 
                  (select * from files as f 
                  where f.id_actividad = a.id_actividad 
                  and f.Id_usuario=17)
                  and a.id_unidad=7
          and a.id_asignacion_area=4*/              


                  


        if($today=='Monday')
        {
            $hoy='1';
            $dia='Lunes';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.id_nivel_grado','=', $id)
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=',$hoy)
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexalumno', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas,'actividades'=>$actividades]);
        }
        elseif($today=='Tuesday')
        {
            $hoy='2';
            $dia='Martes';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.id_nivel_grado','=', $id)
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=',$hoy)
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexalumno', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas,'actividades'=>$actividades]);
        }

        elseif($today=='Wednesday')
        {
            $hoy='3';
            $dia='Miercoles';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.id_nivel_grado','=', $id)
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=',$hoy)
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexalumno', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas,'actividades'=>$actividades]);
        }

        elseif($today=='Thursday')
        {
            $hoy='4';
            $dia='Jueves';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.id_nivel_grado','=', $id)
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=',$hoy)
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexalumno', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas,'actividades'=>$actividades]);
        }

        elseif($today=='Friday')
        {
            $hoy='5';
            $dia='Viernes';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.id_nivel_grado','=', $id)
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=',$hoy)
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexalumno', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas,'actividades'=>$actividades]);
        }

        elseif($today=='Saturday')
        {
            $hoy='6';
            $dia='Sabado';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.id_nivel_grado','=', $id)
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=',$hoy)
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexalumno', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas,'actividades'=>$actividades]);
        }

         elseif($today=='Sunday')
        {
            $hoy='7';
            $dia='Domingo';
            $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.id_nivel_grado','=', $id)
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=',$hoy)
          ->orderBy('c.hora_inicia', 'asc')
          ->get();
              return view('bienvenida.indexalumno', ['items'=>$this->menu, 'horarios'=>$items, 'date'=>$date,'today'=>$dia, 'notificacionesnuevas'=>$notificacionesnuevas,'actividades'=>$actividades]);
        }



            break;

          case '5':

          return view('bienvenida.indexusuario', compact('items','unidad','query'));
                   

            break;

             case '6':

          return view('bienvenida.indexdirector', compact('items','unidad','query'));
                   

            break;


        }

        


      }
      }

          else
          {
                  Session::flash('mensaje', 'NO EXISTE UNA UNIDAD DISPONIBLE PARA ESTA FECHA, COMUNQUESE AL CORREO education@webusiness.co O AL TELEFONO +502 30154416');
                  return view('auth.login');
          }


       

      //return view('menu-test', compact('items'));

        

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        //

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        //

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        //

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        //

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        //

    }

}

