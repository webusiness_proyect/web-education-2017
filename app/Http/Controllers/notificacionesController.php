<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;


use education\Http\Requests;


use education\Http\Controllers\menuController;


use education\notificaciones;

use education\Repositories\NotificacionesRepository;


use education\Planes;


use education\Jornadas;


use education\Salones;


use education\Roles;


use \Validator;


use Session;


use education\User;


use Auth;

use DB;


class notificacionesController extends Controller
{
     protected $menu;

     protected $notificaciones;


    public function __construct(NotificacionesRepository $notificacion)


    {


      $m = new menuController();


      $this->menu = $m->index();

      $this->notificaciones = $notificacion;


    }

    
    protected function paginate($items, $perPage = 12)

    {

        //Get current page form url e.g. &page=1

        $currentPage = LengthAwarePaginator::resolveCurrentPage();



        //Create a new Laravel collection from the array data

        $collection = new Collection($items);



        //Slice the collection to get the items to display in current page

        $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);



        //Create our paginator and pass it to the view

        return new LengthAwarePaginator($currentPageItems, count($collection), $perPage);

    }



    /**


     * Display a listing of the resource.


     *


     * @return \Illuminate\Http\Response


     */


    public function index(Request $request)


    {


        $user = User::where('id', Auth::user()->id)->first();
         $usuario= (Auth::user()->id);


        if ($user->can(['crear-notificacion', 'editar-notificacion'])) {

        	


          //$pensum = Asignacion_areas::getGradosPensum();

          $query=trim($request->get('searchText'));

          $notificaciones=DB::table('NOTIFICACIONES AS n')
         
          
                    
          ->where('n.user','=',$usuario)
        
          ->select('n.id_notificacion','n.titulo_notificacion','n.informacion_notificacion','n.fecha_create', 'n.estado_mensaje')  
          
          ->groupBy('n.fecha_create')
          ->orderBy('n.fecha_create', 'asc')
          ->paginate(10);

          /*QUERY QUE MUESTRA TODOS LOS DATOS DE LA NOTIFICACION

          SELECT * FROM NOTIFICACIONES as n join PLANES as p on n.id_plan=p.id_plan 
			join JORNADAS as j on n.id_jornada=j.id_jornada
			join NIVELES as niv on n.id_nivel=niv.id_nivel
			join CARRERAS as c on n.id_carreras=c.id_carrera
			join GRADOS as g on n.id_grado=g.id_grado
			join roles as r on n.id_rol=r.id
			where user=1
                             */



          return view('notificaciones.index', ['notificaciones'=>$notificaciones, 'items'=>$this->menu, 'searchText'=>$query]);


        }


        return abort(403);


    }


    public function create()


    {


      $user = User::where('id', Auth::user()->id)->first();


      if ($user->can('crear-notificacion')) {


        $jornadas = Jornadas::All();


        $planes = Planes::All();


        $roles = Roles::All();


        foreach ($jornadas as $key => $row) {


          $j[$row->id_jornada] = mb_strtoupper($row->nombre_jornada);


        }


        foreach ($planes as $key => $row) {


          $p[$row->id_plan] = mb_strtoupper($row->nombre_plan);


        }


        foreach ($roles as $key => $row) {


          $r[$row->id] = mb_strtoupper($row->name);


        }


        return view('notificaciones.nnotificacion', ['jornadas'=>$j, 'planes'=>$p, 'roles'=>$r, 'items'=>$this->menu]);


      }


        return abort(403);


    }





    /**


     * Store a newly created resource in storage.


     *


     * @param  \Illuminate\Http\Request  $request


     * @return \Illuminate\Http\Response


     */


    public function store(Request $request)


    {


      //crear el arreglo de los mensajes de validacion


          $mensajes = array(


          'grado.required' => 'Hey! El grado es requerido!!!.',


          'id_rol.required' => 'Hey! El rol es requerido!!!.',

          'titulo.required' => 'Hey! El titulo del mensaje es requerido!!!.',

          'descripcion.required' => 'Hey! La descripcion del mensaje es requerido!!!.',



          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',


          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',


          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',


          );





          $v = Validator::make(


          $request->all(),


           [


                'grado' => 'required',


                'id_rol' => 'required',

                'titulo' => 'required',

                'descripcion' => 'required'




            ],


            $mensajes);





        if ($v->fails())


        {


            return redirect()->back()->withInput()->withErrors($v->errors());


        }

        $user = Auth::user()->id;

        $grado = $request['grado'];

        $id_rol = $request['id_rol'];

        $titulo = $request['titulo'];

        $descripcion = $request['descripcion'];

        $rand= rand(1,1000000);
          /*$notificacion= notificaciones::groupBy('id_notificacion')
          ->first();
            */

        $existe=notificaciones::findnotificacion($rand);

           if($existe==0)
           {


           
        foreach ($grado as $key => $g) {


          for ($i=0; $i < count($id_rol); $i++) {


            notificaciones::setEnvioNotificacion($rand, $g, $id_rol[$i],$titulo,$descripcion,$user);


          }


        }


      Session::flash('mensaje', 'Se enviado una notificacion exitosamente');


      return redirect('/notificaciones');

      }

      else
      {

        Session::flash('mensaje', 'NO SE PUEDE ENVIAR MENSAJE INTENTE NUEVAENTE');


         return redirect('/notificaciones');
      }


    }

    public function show($id)


    {


       $user = User::where('id', Auth::user()->id)->first();


        if ($user->can('ver-notificaciones')) {



           $notificaciones=DB::table('NOTIFICACIONES AS n') 
           ->join('NIVELES_GRADOS AS ng', 'n.id_nivel_grado', '=', 'ng.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')    
          ->join('NIVELES AS niv', 'npj.id_nivel', '=', 'niv.id_nivel')    
          ->join('JORNADAS AS j', 'npj.id_jornada', '=', 'j.id_jornada')    
          ->join('PLANES AS pl', 'npj.id_plan', '=', 'pl.id_plan') 
          ->join('roles AS r', 'n.id_rol', '=', 'r.id')                  
          ->where('n.id_notificacion','=',$id)     
          ->select('n.titulo_notificacion','n.informacion_notificacion','n.informacion_notificacion','n.fecha_create','g.nombre_grado as grado','ca.nombre_carrera as carrera','niv.nombre_nivel as nivel','j.nombre_jornada as jornada', 'pl.nombre_plan as plan', 'r.name')

        
          ->orderBy('n.fecha_create', 'desc')  
          
          ->paginate(10);


          return view('notificaciones.mnotificacion', ['notificaciones'=>$notificaciones, 'items'=>$this->menu]);


        }


        return abort(403);
      /*
     SELECT g.nombre_grado as grado, ca.nombre_carrera as carrera, niv.nombre_nivel as nivel,  j.nombre_jornada as jornada, pl.nombre_plan as plan, r.name  FROM `NOTIFICACIONES` as n join NIVELES_GRADOS AS ng on n.id_nivel_grado=ng.id_nivel_grado
      join GRADOS as g on ng.id_grado=g.id_grado
      join CARRERAS as ca on ng.id_carrera=ca.id_carrera
      join NIVELES_PLANES_JORNADAS as npj on ng.id_nivel_plan_jornada=npj.id_nivel_plan_jornada
      join NIVELES AS niv on npj.id_nivel=niv.id_nivel
      join JORNADAS AS j on npj.id_jornada=j.id_jornada
      join PLANES AS pl ON npj.id_plan=pl.id_plan
      join ROLES AS r on n.id_rol=r.id
      WHERE n.id_notificacion = 77454
    */

    }

    public function edit($id)
    {
        $this->notificaciones->id_notificacion = $id;

        $dato=$this->notificaciones->findmyNotificacionById($id);
       
        $jornadas = Jornadas::All();


        $planes = Planes::All();


        $roles = Roles::All();


        foreach ($jornadas as $key => $row) {


          $j[$row->id_jornada] = mb_strtoupper($row->nombre_jornada);


        }


        foreach ($planes as $key => $row) {


          $p[$row->id_plan] = mb_strtoupper($row->nombre_plan);


        }


        foreach ($roles as $key => $row) {


          $r[$row->id] = mb_strtoupper($row->name);


        }
      
        return view('notificaciones.enotificacion', ['items' => $this->menu, 'jornadas'=>$j, 'planes'=>$p, 'roles'=>$r, 'dato'=>$dato]);
        
     }


    public function update(Request $request, $id)

    {

      /*
      UPDATE `educativo02`.`estudiantes` SET `codigo_personal_estudiante` = '00123', `estado_estudiante` = '1' WHERE `estudiantes`.`id_estudiante` = 6;
      */



        
        $this->notificaciones->id_notificacion = $id;//asignar el id de la tarea actual
        $id_notificacion=$this->notificaciones->findmyNotificacionById($id);//
       
        $this->notificaciones->titulo_notificacion = $request['titulo_notificacion'];
        $this->notificaciones->informacion_notificacion = $request['informacion_notificacion'];
     
        $this->notificaciones->updatenotificacion();
        Session::flash('mensaje', 'Se ha modificado los datos de esta notificacion');
        return redirect('/notificaciones');



    }




    public function notificaciones(Request $request)

    {

      $coincidencia = $request['query'];

     $a = array();

    $datos = Roles::getRolesLike($coincidencia);

      foreach ($datos as $key => $row) {

        $aux = array('value'=>ucfirst($row->name), 'data'=>$row->id);

        array_push($a, $aux);

      }

      return response()->json(array(

                            "query"=>"Unit",

                            "suggestions"=>$a));//devolviendo el json para la respuesta

    }





}
