<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;


use Illuminate\Pagination\LengthAwarePaginator;

use Illuminate\Support\Collection;


use education\Http\Controllers\menuController;

use education\Repositories\docentesRepository;

use education\Repositories\estudiantesRepository;

use education\Repositories\padresRepository;

use education\Repositories\unidadesRepository;

use Auth;

use Session;

use DB;

class padresdocenteController extends Controller
{
     protected $menu;

   protected $padres;

   protected $unidades;

   protected $docentes;

  public function __construct(docentesRepository $docente, padresRepository $padre, unidadesRepository $unidad)

  {

    $m = new menuController();

    $this->menu = $m->index();

    $this->padres = $padre;

    $this->unidades = $unidad;

    $this->docentes = $docente;

  }


    public function index(Request $request)

    {




       
        $this->padres->id_usuario = Auth::user()->id;//obtenemos el id de la persona logueada

        $mdocentes = $this->padres->getDocentes();//obtenemos los docentes

        //$cursos = $this->paginate($aux, 10);//paginar el resultado


           

        return view('misdocnetes.index', ['items'=>$this->menu, 'docentes'=>$mdocentes]);


      

    }


    public function show(Request $request)

    {

          $docente=$this->docentes->id_usuario = $request['a'];


        $this->unidades->fecha_inicio = date('Y-m-d');//fecha inicial de la unidad


        $this->unidades->fecha_final = date('Y-m-d');//fecha final de la unidad


        $unidad = $this->unidades->getUnidadActual();//obtenemos la unidad disponible de acuerdo a la fecha




        $aux = $this->docentes->getCursos();//obtenemos los cursos asignados al docente

        //$cursos = $this->paginate($aux, 10);//paginar el resultado


          $query=trim($request->get('searchText'));

          $cursos=DB::table('PERSONAS as p')
                 ->join('ASIGNACION_DOCENTE as ad', 'p.id_persona', '=', 'ad.id_persona')
                 ->join('ASIGNACION_AREAS as aa', 'ad.id_asignacion_area', '=', 'aa.id_asignacion_area')
                 ->join('AREAS as a', 'aa.id_area', '=', 'a.id_area')
                 ->join('NIVELES_GRADOS as ng', 'aa.id_nivel_grado', '=', 'ng.id_nivel_grado')
                 ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
                 ->join('SECCIONES as s', 'ng.id_seccion', '=', 's.id_seccion')
                 ->join('CARRERAS as c', 'ng.id_carrera', '=', 'c.id_carrera')
                 ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
                 ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
                 ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
                 ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
                 ->join('USUARIO_PERSONA as up', 'p.id_persona', '=', 'up.id_persona')
                 ->select('a.nombre_area', 'aa.id_asignacion_area', 'ad.id_asignacion_docente', 'g.nombre_grado', 's.nombre_seccion', 'c.nombre_carrera', 'n.nombre_nivel', 'pl.nombre_plan', 'j.nombre_jornada', 'up.user_id', 'a.id_area')
                    
                    
                    
                    ->where('ad.estado_asignacion_docente', TRUE)
                    ->where('up.user_id',$docente)

                    ->paginate(10);

        return view('docentes.index', ['items'=>$this->menu, 'unidad'=>$unidad, 'cursos'=>$cursos, 'searchText'=>$query]);

        /*
            select a.nombre_area, aa.id_asignacion_area, ad.id_asignacion_docente, g.nombre_grado, s.nombre_seccion, c.nombre_carrera, n.nombre_nivel, pl.nombre_plan, j.nombre_jornada, up.user_id, a.id_area from PERSONAS as p join ASIGNACION_DOCENTE as ad on p.id_persona=ad.id_persona

                 join ASIGNACION_AREAS as aa on ad.id_asignacion_area=aa.id_asignacion_area

                 join AREAS as a on aa.id_area=a.id_area

                 join NIVELES_GRADOS as ng on aa.id_nivel_grado=ng.id_nivel_grado

                 join GRADOS as g on ng.id_grado=g.id_grado

                 join SECCIONES as s on ng.id_seccion=s.id_seccion

                 join CARRERAS as c on ng.id_carrera=c.id_carrera

                 join NIVELES_PLANES_JORNADAS as npj on  ng.id_nivel_plan_jornada=npj.id_nivel_plan_jornada

                 join NIVELES as n on npj.id_nivel=n.id_nivel

                 join PLANES as pl on npj.id_plan=pl.id_plan

                 join JORNADAS as j on npj.id_jornada=j.id_jornada

                 join USUARIO_PERSONA as up on p.id_persona=up.id_persona

                 where ad.estado_asignacion_docente=TRUE and

                 up.user_id=3
                 */

    }



}
