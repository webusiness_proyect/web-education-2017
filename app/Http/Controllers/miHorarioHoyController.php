<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;

use education\Http\Requests\horariosRequest ;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use education\Http\Requests;
use education\Http\Controllers\menuController;
use education\Asignacion_areas;
use education\Repositories\HorarioRepository;
use education\horario;
use education\Areas;
use \Validator;
use Session;
use education\User;
use Auth;
use DB;

class miHorarioHoyController extends Controller
{
    protected $menu;

  protected $horarios;

  public function __construct(HorarioRepository $horario)

  {

    $m = new menuController();

    $this->menu = $m->index();

    $this->horarios = $horario;

  }

   public function index(Request $request)

    {

        

        //$items = $this->horarios->getAllCalendarios();


    		$this->horarios->user = Auth::user()->id;
    		$niveles=$this->horarios->getNivelEstudiantes();
    		$id=$this->horarios->id_area=$niveles['id_nivel_grado'];

    		$fecha=strtotime(DateTime());

    		$hoy=(date('w', $fecha));


          $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
      
        
          ->select(DB::raw('DISTINCT(c.dia)'), 'c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera','ar.nombre_area', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.id_nivel_grado','=', $id)
          ->where('c.estado_actividad_calendario','=','1')
          ->where('c.dia','=',$hoy)
          ->orderBy('c.hora_inicia', 'asc')
         
          ->paginate(50);
          /*
          QUERY QUE MUESTRA TODAS LAS ACTIVIDADES DEL CALENDARIO POR DIA
          select distinct * from calendario as c join DIAS as d on c.dia=d.id_dia
          join niveles_grados as ng on ng.id_nivel_grado=c.id_nivel_grado
          join grados as g on ng.id_grado=g.id_grado
          join carreras as ca on ng.id_carrera=ca.id_carrera
          join niveles_planes_jornadas as npj on ng.id_nivel_plan_jornada=npj.id_nivel_plan_jornada
          join niveles as n on npj.id_nivel=n.id_nivel
          join jornadas as j on npj.id_jornada=j.id_jornada
          join planes as pl on npj.id_plan=pl.id_plan
          join areas as ar on ar.id_area=c.id_area

          where c.dia=1
          and g.nombre_grado='SEXTO'
          and ca.nombre_carrera='perito contador'
          and n.nombre_nivel='Diversificado'
          order by c.hora_inicia asc
          */

          


        return view('bienvenida.indexalumno', ['items'=>$this->menu, 'horarios'=>$items]);

    }
}
