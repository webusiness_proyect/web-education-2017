<?php





namespace education\Http\Controllers;





use Illuminate\Http\Request;





use education\Http\Requests;


use education\Http\Controllers\menuController;


use education\Niveles_planes_jornadas;


use education\Jornadas;


use education\Planes;


use education\Niveles;


use \Validator;


use Session;


use education\User;


use Auth;

use DB;


class planesnivelesController extends Controller


{


    protected $menu;


    public function __construct()


    {


      $m = new menuController();


      $this->menu = $m->index();


    }


    /**


     * Display a listing of the resource.


     *


     * @return \Illuminate\Http\Response


     */


    public function index(Request $request)


    {


      $user = User::where('id', Auth::user()->id)->first();


      if ($user->can(['crear-plannivel', 'editar-plannivel'])) {


        //$datos = Niveles_planes_jornadas::getPlanesNiveles();

        $query=trim($request->get('searchText'));

          $datos=DB::table('NIVELES_PLANES_JORNADAS')
          ->join('NIVELES', 'NIVELES_PLANES_JORNADAS.id_nivel', '=', 'NIVELES.id_nivel')


          ->join('PLANES', 'NIVELES_PLANES_JORNADAS.id_plan', '=', 'PLANES.id_plan')


          ->join('JORNADAS', 'NIVELES_PLANES_JORNADAS.id_jornada', '=', 'JORNADAS.id_jornada')


           ->select('NIVELES_PLANES_JORNADAS.id_nivel_plan_jornada', 'NIVELES.nombre_nivel', 'PLANES.nombre_plan', 'JORNADAS.nombre_jornada','NIVELES_PLANES_JORNADAS.estado_niveles_planes_jornadas')

          ->where('NIVELES.nombre_nivel','LIKE', '%'.$query.'%')
          ->orWhere('PLANES.nombre_plan','LIKE', '%'.$query.'%')
          ->orWhere('JORNADAS.nombre_jornada','LIKE', '%'.$query.'%')
          ->orderBy('NIVELES_PLANES_JORNADAS.id_nivel_plan_jornada', 'asc')
          ->paginate(10);


        return view('nivelesplanesjornadas.index', ['datos'=>$datos, 'items'=>$this->menu, 'searchText'=>$query]);


      }


      return abort(403);


    }






    /**


     * Show the form for creating a new resource.


     *


     * @return \Illuminate\Http\Response


     */


    public function create()


    {


        $user = User::where('id', Auth::user()->id)->first();


        if ($user->can('crear-plannivel')) {


          $niveles = Niveles::All();


          $jornadas = Jornadas::All();


          $planes = Planes::All();


          foreach ($niveles as $key => $row) {


            $n[$row->id_nivel] = mb_strtoupper($row->nombre_nivel);


          }


          foreach ($jornadas as $key => $row) {


            $j[$row->id_jornada] = mb_strtoupper($row->nombre_jornada);


          }


          foreach ($planes as $key => $row) {


            $p[$row->id_plan] = mb_strtoupper($row->nombre_plan);


          }


          return view('nivelesplanesjornadas.nnivelplanjornada', ['niveles'=>$n, 'jornadas'=>$j, 'planes'=>$p, 'items'=>$this->menu]);


        }


        return abort(403);


    }





    /**


     * Store a newly created resource in storage.


     *


     * @param  \Illuminate\Http\Request  $request


     * @return \Illuminate\Http\Response


     */


    public function store(Request $request)


    {


      //crear el arreglo de los mensajes de validacion


          $mensajes = array(


          'required' => 'Hey! EL campo :attribute es requerido!!!.',


          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',


          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',


          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',


          );





          $v = Validator::make(


          $request->all(),


           [


                'id_nivel' => 'required',


                'id_plan' => 'required',


                'id_jornada' => 'required'


            ],


            $mensajes);





        if ($v->fails())


        {


            return redirect()->back()->withInput()->withErrors($v->errors());


        }


      $nivel = $request['id_nivel'];


      $plan = $request['id_plan'];


      $jornada = $request['id_jornada'];


      $result = Niveles_planes_jornadas::setPlanNivel($nivel, $plan, $jornada);


      Session::flash('mensaje', $result[0]->msg);


      return redirect('/asignarniveles');


    }





    /**


     * Display the specified resource.


     *


     * @param  int  $id


     * @return \Illuminate\Http\Response


     */


    public function show($id)


    {


        //


    }





    /**


     * Show the form for editing the specified resource.


     *


     * @param  int  $id


     * @return \Illuminate\Http\Response


     */


    public function edit($id)


    {


      $user = User::where('id', Auth::user()->id)->first();


      if ($user->can('editar-plannivel')) {


        $niveles = Niveles::All();


        $jornadas = Jornadas::All();


        $planes = Planes::All();


        foreach ($niveles as $key => $row) {


          $n[$row->id_nivel] = mb_strtoupper($row->nombre_nivel);


        }


        foreach ($jornadas as $key => $row) {


          $j[$row->id_jornada] = mb_strtoupper($row->nombre_jornada);


        }


        foreach ($planes as $key => $row) {


          $p[$row->id_plan] = mb_strtoupper($row->nombre_plan);


        }


        $dato = Niveles_planes_jornadas::findPlanNivel($id);


        //return $dato->id_nivel_plan_jornada;


        return view('nivelesplanesjornadas.enivelplanjornada', ['dato'=>$dato, 'niveles'=>$n, 'jornadas'=>$j, 'planes'=>$p, 'items'=>$this->menu]);


      }


      return abort(403);


    }





    /**


     * Update the specified resource in storage.


     *


     * @param  \Illuminate\Http\Request  $request


     * @param  int  $id


     * @return \Illuminate\Http\Response


     */


    public function update(Request $request, $id)


    {


      //crear el arreglo de los mensajes de validacion


          $mensajes = array(


          'required' => 'Hey! EL campo :attribute es requerido!!!.',


          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',


          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',


          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',


          );





          $v = Validator::make(


          $request->all(),


           [


                'id_nivel' => 'required',


                'id_plan' => 'required',


                'id_jornada' => 'required'


            ],


            $mensajes);





        if ($v->fails())


        {


            return redirect()->back()->withInput()->withErrors($v->errors());


        }


      $nivel = $request['id_nivel'];


      $plan = $request['id_plan'];


      $jornada = $request['id_jornada'];


      $result = Niveles_planes_jornadas::updatePlanNivel($id, $nivel, $plan, $jornada);


      Session::flash('mensaje', $result[0]->msg);


      return redirect('/asignarniveles');


    }





    /**


     * Remove the specified resource from storage.


     *


     * @param  int  $id


     * @return \Illuminate\Http\Response


     */


    public function destroy($id, Request $request)


 {

        $estado = $request['estado'];

        if ($estado == 1) {

          Niveles_planes_jornadas::stateNivelesPlanesJornadas($id, FALSE);

        } else {

          Niveles_planes_jornadas::stateNivelesPlanesJornadas($id, TRUE);

        }

        $grados =DB::table('NIVELES_PLANES_JORNADAS')
          ->join('NIVELES', 'NIVELES_PLANES_JORNADAS.id_nivel', '=', 'NIVELES.id_nivel')
          ->join('PLANES', 'NIVELES_PLANES_JORNADAS.id_plan', '=', 'PLANES.id_plan')
          ->join('JORNADAS', 'NIVELES_PLANES_JORNADAS.id_jornada', '=', 'JORNADAS.id_jornada')
           ->select('NIVELES_PLANES_JORNADAS.id_nivel_plan_jornada', 'NIVELES.nombre_nivel', 'PLANES.nombre_plan', 'JORNADAS.nombre_jornada','NIVELES_PLANES_JORNADAS.estado_niveles_planes_jornadas')

          ->orderBy('NIVELES_PLANES_JORNADAS.id_nivel_plan_jornada', 'asc')
          ->paginate(10);

        return response()->json($grados);

    }





    public function bscajx(Request $request)


    {


      $jornada = $request['jornada'];


      $plan = $request['plan'];


      $niveles = Niveles_planes_jornadas::getNivelWhere($plan, $jornada);


      return response()->json($niveles);


    }


}


