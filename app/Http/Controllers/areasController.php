<?php



namespace education\Http\Controllers;



use Illuminate\Http\Request;



use education\Http\Requests;

use education\Http\Controllers\menuController;

use education\Areas;

use \Validator;

use Session;

use education\User;

use Auth;

use Illuminate\Support\Facades\Redirect;

use DB;

class areasController extends Controller

{

    protected $menu;

    public function __construct()

    {

      $m = new menuController();

      $this->menu = $m->index();

    }

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(Request $request)

    {

        $user = User::where('id', Auth::user()->id)->first();

        if ($user->can(['crear-curso', 'editar-curso', 'estado-curso'])) {

          $query=trim($request->get('searchText'));

          $areas=DB::table('AREAS')->where('nombre_area','LIKE', '%'.$query.'%')
          
          ->orderBy('id_area', 'asc')
          ->paginate(10);

          //$areas = Areas::paginate(10);

          return view('areas.index', ['areas'=>$areas, 'items'=>$this->menu, 'searchText'=>$query]);

        }

        return abort(403);

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        $user = User::where('id', Auth::user()->id)->first();

        if ($user->can('crear-curso')) {

          return view('areas.narea',['items'=>$this->menu]);

        }

        return abort(403);

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

      //crear el arreglo de los mensajes de validacion

          $mensajes = array(

          'required' => 'Hey! EL campo :attribute es requerido!!!.',

          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',

          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',

          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',

          );



          $v = Validator::make(

          $request->all(),

           [

                'nombre_area' => 'required|min:4|max:100|unique:AREAS,nombre_area',

            ],

            $mensajes);



        if ($v->fails())

        {

            return redirect()->back()->withInput()->withErrors($v->errors());

        }

      $nombre = $request['nombre_area'];

      $result = Areas::setArea($nombre);

      Session::flash('mensaje', $result[0]->msg);

      return redirect('/areas');

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        $user = User::where('id', Auth::user()->id)->first();

        if ($user->can('editar-curso')) {

          $area = Areas::findArea($id);

          return view('areas.earea', ['area'=>$area, 'items'=>$this->menu]);

        }

      return abort(403);

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

      //crear el arreglo de los mensajes de validacion

          $mensajes = array(

          'required' => 'Hey! EL campo :attribute es requerido!!!.',

          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',

          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',

          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',

          );



          $v = Validator::make(

          $request->all(),

           [

                'nombre_area' => 'required|min:4|max:100|unique:AREAS,nombre_area,'.$id.',id_area',

            ],

            $mensajes);



        if ($v->fails())

        {

            return redirect()->back()->withInput()->withErrors($v->errors());

        }

      $nombre = $request['nombre_area'];

      $result = Areas::updateArea($id, $nombre);

      Session::flash('mensaje', $result[0]->msg);

      return redirect('/areas');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id, Request $request)

    {

        $estado = $request['estado'];

        if ($estado == 1) {

          Areas::stateArea($id, FALSE);

        } else {

          Areas::stateArea($id, TRUE);

        }

        $areas = Areas::All();

        return response()->json($areas);

    }



    /*

      Metodo que devuelve el nombre del curso, por medio de la peticion ajax

    */

    public function areas(Request $request)

    {

      $coincidencia = $request['query'];

     $a = array();

    $datos = Areas::getAreasLike($coincidencia);

      foreach ($datos as $key => $row) {

        $aux = array('value'=>ucfirst($row->nombre_area), 'data'=>$row->id_area);

        array_push($a, $aux);

      }

      return response()->json(array(

                            "query"=>"Unit",

                            "suggestions"=>$a));//devolviendo el json para la respuesta

    }

}

