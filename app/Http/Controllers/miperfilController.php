<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;

use education\Http\Requests\MiPerfilRequest;
use education\Http\Controllers\menuController;
use education\Repositories\userRepository;
use education\User;
use Session;
use Auth;
use Illuminate\Support\Facades\Input;
use Validator;
use Redirect;
use DB;
use education\Http\Requests\UserRequest;
use Hash;
use education\Components\FlashMessages;
use Alert;


class miperfilController extends Controller
{
    protected $menu;
    protected $users;
  
  	public function __construct(userRepository $user)
  {
    $m = new menuController();
    $this->menu = $m->index();
    $this->users = $user; 

  }

   public function index(Request $request)
    {

      

        $items=DB::table('users as u')
             ->select('u.id','u.name','u.email','u.password','u.url','u.size','u.type')
             ->where('id','=', Auth::user()->id)->first();
      
        return view('miperfil.index', ['items'=>$this->menu, 'usuarios'=>$items]);
    }

    public function edit($id)
    {
        //$this->authorize('owner', $foros);


        Auth::user()->id=$id;
        $usuario=User::where('id', Auth::user()->id)->first();
        /*
        $usuario=DB::table('users as u')
             ->select('u.id','u.name','u.email','u.password','u.url','u.size','u.type')
             ->where('id','=', Auth::user()->id)->first();
             */
        return view('miperfil.eperfil', ['items'=>$this->menu, 'usuario'=>$usuario]);
        


    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)


    {

        
        
           
       



        $password= Hash::make($request['password']);
        $password2= $request['password2'];

        if($password===$password2)
        {
             DB::table('users')->where('id', $id)


                      ->update([

                                  

                                  'password'=>$password,



                              ]);

                       Session::flash('mensaje', 'Se han actualizado la contraseña!!!');

                return redirect('/miperfil');
        }

        else
        {
               Session::flash('mensaje', 'las contraseñas no coinciden');
                
                //Alert::message('Message', 'Optional Title');
                return redirect('/miperfil');
        }

        


        /*
        $user = User::find($id);
        if(Input::hasFile('url'))
        {
            $file=Input::file('url');//creo variable con el archivo de ingreso
            $urlimg=$file->getClientOriginalName();//selecciono nombre del archivo
            $file->move(public_path().'/img/usuarios/'.$urlimg); // subir archivo a una url       
            $user->url='/img/usuarios/'.$urlimg;
            $size=$user->size=$file->getClientsize();
            $type=$user->type=$file->getClientMimeType();
            $password=$user->password=Hash::make($password);
            if($type=='image/png'&&$size<='100000')
            {
                $user->save();
                 Session::flash('mensaje', 'La imagen ser a subido exitosamente!!!');
                 return redirect('miperfil')->with('success-message', 'imagen png subido exitosamente');
            }
            elseif ($type=='image/jpeg'&&$size<='100000')
                 {
                    $user->save();
                    Session::flash('mensaje', 'La imagen ser a subido exitosamente!!!');
                    return redirect('miperfil')->with('success-message', 'imagen jpg subido exitosamente');
                }

             elseif ($type=='image/jpg'&&$size<='100000')

                 {
                $user->save();
                Session::flash('mensaje', 'La imagen ser a subido exitosamente!!!');
                 return redirect('miperfil')->with('success-message', 'imagen jpg subido exitosamente');
                

            }

            else
            {
                 Session::flash('mensaje', 'ESTE TIPO DE ARHVIOS NO ES PERMITIDO O ES MUY GRANDE PARA PODER SER ENTREGADO');
             
                 
                 return redirect('miperfil')->with('error-message', 'no se permite este tipo de archivos'); 

            }
        }
        */
        


        

    }
            

    


        

  }
            

            




