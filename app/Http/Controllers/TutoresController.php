<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;

use education\Repositories\padresRepository;

use education\Repositories\userRepository;

use education\Tutor;

use education\Tutor_estudiante;

use education\User;

use Session;

use Auth;

use DB;

class TutoresController extends Controller
{

  protected $menu;

  protected $tutores;

  protected $users;

  public function __construct(padresRepository $tutor, userRepository $user)

  {

    $m = new menuController();

    $this->menu = $m->index();

    $this->tutores = $tutor;

    $this->users = $user;

  }


   public function index(Request $request)

    {

       $user = User::where('id', Auth::user()->id)->first();

    if ($user->can(['ver-todos-tutores', 'editar-tutores'])) {

          //$grados = Grados::All();

           $query=trim($request->get('searchText'));

          $tutores=DB::table('TUTOR as t')
         
          ->join('TUTOR_ESTUDIANTE as tu', 't.id_tutor', '=', 'tu.id_tutor')
          ->join('ESTUDIANTES as e', 'tu.id_estudiante', '=', 'e.id_estudiante')

          ->where('t.nombre_tutor','LIKE', '%'.$query.'%')
          ->where('t.apellidos_tutor','LIKE', '%'.$query.'%')
         
          
          ->orderBy('t.apellidos_tutor', 'asc')
         ->select('t.nombre_tutor','t.apellidos_tutor','t.correo_electronico_tutor','t.cui_tutor','t.telefono_primario_tutor','t.lugar_trabajo_tutor','t.telefono_trabajo_tutor','t.id_tutor','t.estado_tutor','e.nombre_estudiante','e.apellidos_estudiante')
          ->paginate(10);

          return view('tutores.index', ['tutores'=>$tutores, 'items'=>$this->menu, 'searchText'=>$query]);

        }

        /*
        QUERY QUE MUESTRA TODOS LOS TUTORES CON SUS HIJOS ASIGNADOS
        SELECT * FROM TUTOR AS t JOIN TUTOR_ESTUDIANTE AS tu on t.id_tutor=tu.id_tutor
		join ESTUDIANTES as e on tu.id_estudiante=e.id_estudiante
		*/

        return abort(403);
    }


    public function show(Request $request)

    {

     


       

        $this->tutores->id_estudiante = $request['a'];

         $tutor = $this->tutores->findTutorEstudiante();//obtenemos el tutor del hijo
        


        return view('tutoreshijos.index', ['items'=>$this->menu, 'tutor'=>$tutor]);

    }

      public function edit($id)
    {
        $this->tutores->id_tutor = $id;
      
        $dato = Tutor::findTutorById($id);

     
        $telefonicas = array('tigo'=>'Tigo', 'claro'=>'Claro', 'movistar'=>'Movistar');
        return view('tutores.etutor', ['items' => $this->menu, 'telefonicas'=>$telefonicas,'dato'=>$dato]);
        
     }

     public function update(Request $request, $id)

    {

        
        $this->tutores->id_tutor = $id;//asignar el id de la tarea actual
        $id = $this->tutores->findTutorById();//
       
        $this->tutores->nombre_tutor = $request['nombre_tutor'];
        $this->tutores->apellidos_tutor = $request['apellidos_tutor'];
        $this->tutores->direccion_tutor = $request['direccion_tutor'];
        $this->tutores->telefono_primario_tutor = $request['telefono_primario_tutor'];
        $this->tutores->empresa_telefono_tutor = $request['empresa_telefono_tutor'];
        $this->tutores->correo_electronico_tutor = $request['correo_electronico_tutor'];
        $this->tutores->cui_tutor = $request['cui_tutor'];
        $this->tutores->lugar_trabajo_tutor = $request['lugar_trabajo_tutor'];
        $this->tutores->direccion_trabajo_tutor = $request['direccion_trabajo_tutor'];
        $this->tutores->telefono_trabajo_tutor = $request['telefono_trabajo_tutor'];
        $this->tutores->empresa_telefono_trabajo = $request['empresa_telefono_trabajo'];
        $this->tutores->cargo_tutor = $request['cargo_tutor'];
        $this->tutores->parentesco_tutor = $request['parentesco_tutor'];
       


        $this->tutores->updateTutores();
        Session::flash('mensaje', 'Se han cambiado los datos para el tutor!!!');
        return redirect('/tutores');

    }




    

}
