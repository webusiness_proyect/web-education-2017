<?php





namespace education\Http\Controllers;





use Illuminate\Http\Request;

use Illuminate\Pagination\LengthAwarePaginator;

use Illuminate\Support\Collection;






use education\Http\Requests;


use education\Http\Controllers\menuController;


use education\Asignacion_areas;


use education\Planes;


use education\Jornadas;


use education\Salones;


use \Validator;


use Session;


use education\User;


use Auth;

use DB;


class asignacionareasController extends Controller


{


    protected $menu;


    public function __construct()


    {


      $m = new menuController();


      $this->menu = $m->index();


    }

    
    protected function paginate($items, $perPage = 12)

    {

        //Get current page form url e.g. &page=1

        $currentPage = LengthAwarePaginator::resolveCurrentPage();



        //Create a new Laravel collection from the array data

        $collection = new Collection($items);



        //Slice the collection to get the items to display in current page

        $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);



        //Create our paginator and pass it to the view

        return new LengthAwarePaginator($currentPageItems, count($collection), $perPage);

    }



    /**


     * Display a listing of the resource.


     *


     * @return \Illuminate\Http\Response


     */


    public function index(Request $request)


    {


        $user = User::where('id', Auth::user()->id)->first();


        if ($user->can(['crear-pensum', 'editar-pensum', 'ver-pensum'])) {


          //$pensum = Asignacion_areas::getGradosPensum();

          $query=trim($request->get('searchText'));

          $pensum=DB::table('ASIGNACION_AREAS')
          ->join('NIVELES_GRADOS AS ng', 'ASIGNACION_AREAS.id_nivel_grado', '=', 'ng.id_nivel_grado')
          ->join('NIVELES_PLANES_JORNADAS AS npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES AS n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('GRADOS AS g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS AS c', 'ng.id_carrera', '=', 'c.id_carrera')
                    
          ->where('g.nombre_grado','LIKE', '%'.$query.'%')
          ->orWhere('n.nombre_nivel','LIKE', '%'.$query.'%')
          ->orWhere('c.nombre_carrera','LIKE', '%'.$query.'%')
          //->where('ASIGNACION_AREAS.estado_asignacion_area','=','1')
          ->select('ASIGNACION_AREAS.id_asignacion_area as id','ASIGNACION_AREAS.estado_asignacion_area','g.nombre_grado', 'n.nombre_nivel', 'c.nombre_carrera', 'ng.id_nivel_grado')  
          
          ->groupBy('ng.id_nivel_grado')
          ->orderBy('ASIGNACION_AREAS.id_asignacion_area', 'asc')
          ->paginate(10);

          /*ASIGNACION_AREAS::join('NIVELES_GRADOS AS ng', 'ASIGNACION_AREAS.id_nivel_grado', '=', 'ng.id_nivel_grado')


                             ->join('NIVELES_PLANES_JORNADAS AS npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')


                             ->join('NIVELES AS n', 'npj.id_nivel', '=', 'n.id_nivel')


                             ->join('GRADOS AS g', 'ng.id_grado', '=', 'g.id_grado')


                             ->join('CARRERAS AS c', 'ng.id_carrera', '=', 'c.id_carrera')


                             ->select('g.nombre_grado', 'n.nombre_nivel', 'c.nombre_carrera', 'ng.id_nivel_grado')


                             ->groupBy('ng.id_nivel_grado')


                             ->get();
                             */



          return view('asignacionareas.index', ['pensum'=>$pensum, 'items'=>$this->menu, 'searchText'=>$query]);


        }


        return abort(403);


    }





    /**


     * Show the form for creating a new resource.


     *


     * @return \Illuminate\Http\Response


     */


    public function create()


    {


      $user = User::where('id', Auth::user()->id)->first();


      if ($user->can('crear-pensum')) {


        $jornadas = Jornadas::All();


        $planes = Planes::All();


        $salones = Salones::All();


        foreach ($jornadas as $key => $row) {


          $j[$row->id_jornada] = mb_strtoupper($row->nombre_jornada);


        }


        foreach ($planes as $key => $row) {


          $p[$row->id_plan] = mb_strtoupper($row->nombre_plan);


        }


        foreach ($salones as $key => $row) {


          $s[$row->id_salon] = mb_strtoupper($row->nombre_salon);


        }


        return view('asignacionareas.nasignacionarea', ['jornadas'=>$j, 'planes'=>$p, 'salones'=>$s, 'items'=>$this->menu]);


      }


        return abort(403);


    }





    /**


     * Store a newly created resource in storage.


     *


     * @param  \Illuminate\Http\Request  $request


     * @return \Illuminate\Http\Response


     */


    public function store(Request $request)


    {


      //crear el arreglo de los mensajes de validacion


          $mensajes = array(


          'grado.required' => 'Hey! El grado es requerido!!!.',


          'cursos.required' => 'Hey! El curso es requerido!!!.',


          'salones.required' => 'Hey! Los salones son requeridos!!!.',


          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',


          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',


          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',


          );





          $v = Validator::make(


          $request->all(),


           [


                'grado' => 'required',


                'cursos' => 'required',


                'salones' => 'required'


            ],


            $mensajes);





        if ($v->fails())


        {


            return redirect()->back()->withInput()->withErrors($v->errors());


        }


        $grado = $request['grado'];


        $cursos = $request['cursos'];


        $salones = $request['salones'];





        foreach ($grado as $key => $g) {


          for ($i=0; $i < count($cursos); $i++) {


            Asignacion_areas::setAsignacionArea($g, $salones[$i], $cursos[$i]);


          }


        }


      Session::flash('mensaje', 'Se a realizado la asignación exitosamente');


      return redirect('/pensum');


    }





    /**


     * Display the specified resource.


     *


     * @param  int  $id


     * @return \Illuminate\Http\Response


     */


    public function show($id)


    {


        $user = User::where('id', Auth::user()->id)->first();


        if ($user->can('ver-pensum')) {


          $pensum = Asignacion_areas::getPensumGrado($id);


          return view('asignacionareas.masignacionarea', ['pensum'=>$pensum, 'items'=>$this->menu]);


        }


        return abort(403);


    }





    /**


     * Show the form for editing the specified resource.


     *


     * @param  int  $id


     * @return \Illuminate\Http\Response


     */


    public function edit($id)


    {


        $user = User::where('id', Auth::user()->id)->first();


        if ($user->can('editar-pensum')) {


          $salones = Salones::All();


          $pensum = Asignacion_areas::getPensumGrado($id);


          foreach ($salones as $key => $row) {


            $s[$row->id_salon] = mb_strtoupper($row->nombre_salon);


          }


          return view('asignacionareas.easignacionarea', ['pensum'=>$pensum, 'salones'=>$s, 'items'=>$this->menu]);


        }


        return abort(403);


    }





    /**


     * Update the specified resource in storage.


     *


     * @param  \Illuminate\Http\Request  $request


     * @param  int  $id


     * @return \Illuminate\Http\Response


     */


    public function update(Request $request, $id)


    {


        $curso = json_decode($request['curso']);


        $salon = json_decode($request['salon']);


        for ($i=0; $i < count($curso); $i++) {


          //echo $salon[$i]->salon;


          //echo $id;


          Asignacion_areas::setAsignacionArea($id, $salon[$i]->salon, $curso[$i]->curso);


        }


        $pensum = Asignacion_areas::getPensumGrado($id);


        return response()->json($pensum);


    }





    /**


     * Remove the specified resource from storage.


     *


     * @param  int  $id


     * @return \Illuminate\Http\Response


     */


    public function destroy(Request $request, $id)


    {


        $estado = $request['estado'];


        if ($estado == 1) {


          Asignacion_areas::stateAsignacionArea($id, FALSE);


        } else {


          Asignacion_areas::stateAsignacionArea($id, TRUE);


        }

         $pensum=DB::table('ASIGNACION_AREAS')
          ->join('NIVELES_GRADOS AS ng', 'ASIGNACION_AREAS.id_nivel_grado', '=', 'ng.id_nivel_grado')
          ->join('NIVELES_PLANES_JORNADAS AS npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES AS n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('GRADOS AS g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS AS c', 'ng.id_carrera', '=', 'c.id_carrera')
          ->select('ASIGNACION_AREAS.id_asignacion_area','ASIGNACION_AREAS.estado_asignacion_area','g.nombre_grado', 'n.nombre_nivel', 'c.nombre_carrera', 'ng.id_nivel_grado')            

          //->where('ASIGNACION_AREAS.estado_asignacion_area','=','1')
          ->groupBy('ng.id_nivel_grado')
          ->orderBy('ASIGNACION_AREAS.id_asignacion_area', 'asc')
          ->paginate(10);



        return response()->json($pensum);


    }





    /*


      Metodo para buscar los grados para asignarles un pensum


    */


    public function grados(Request $request)


    {


      $plan = $request['plan'];


      $jornada = $request['jornada'];


      $grados = Asignacion_areas::getGrado($plan, $jornada);


      return response()->json($grados);


    }


    /*FUNCION PARA TRAER TODAS LAS AREAS ASIGNADAS A UN GRADO*/


    public static function getAreaWhere($nivel)

    {

      return ASIGNACION_AREAS::join('AREAS as a', 'ASIGNACION_AREAS.id_area', '=', 'a.id_area')

                                    ->where('ASIGNACION_AREAS.id_nivel_grado',$nivel)
                                    ->where('ASIGNACION_AREAS.estado_area',1)

                                    ->select('ASIGNACION_AREAS.nombre_area', 'ASIGNACION_AREAS.id_area')

                                    ->get();

                                    /*
                                    select * from asignacion_areas as aa join areas as a on aa.id_area=a.id_area
                                    where aa.id_nivel_grado=4
                                    */

    }





    /*


      Metodo para buscar todos los cursos del pensum de un grado


    */


    public function pensumGrado(Request $request)


    {


      $grado = $request['grado'];


      $pensum = Asignacion_areas::getPensumGradoWhere($grado);


      return response()->json($pensum);


    }


}


