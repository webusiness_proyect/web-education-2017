<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Http\Controllers\menuController;
use education\Jornadas;
use \Validator;
use Session;
use education\User;
use Auth;
class jornadasController extends Controller
{
    protected $menu;
    public function __construct()
    {
      $m = new menuController();
      $this->menu = $m->index();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can(['crear-jornad', 'editar-jornada', 'estado-jornada'])) {
          $jornadas = Jornadas::All();
          return view('jornadas.index', ['jornadas'=>$jornadas, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('crear-jornada')) {
          return view('jornadas.njornada', ['items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'required' => 'Hey! EL campo :attribute es requerido!!!.',
          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
          );

          $v = Validator::make(
          $request->all(),
           [
                'nombre_jornada' => 'required|min:4|max:25|unique:JORNADAS,nombre_jornada',
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
      $nombre = $request['nombre_jornada'];
      $result = Jornadas::setJornadas($nombre);
      Session::flash('mensaje', $result[0]->msg);
      return redirect('/jornadas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('editar-jornada')) {
          $jornada =Jornadas::findJornada($id);
          return view('jornadas.ejornada', ['jornada'=>$jornada, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'required' => 'Hey! EL campo :attribute es requerido!!!.',
          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
          );

          $v = Validator::make(
          $request->all(),
           [
                'nombre_jornada' => 'required|min:4|max:25|unique:JORNADAS,nombre_jornada,'.$id.',id_jornada',
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
      $nombre = $request['nombre_jornada'];
      $result = Jornadas::updateJornada($id, $nombre);
      Session::flash('mensaje', $result[0]->msg);
      return redirect('/jornadas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $estado = $request['estado'];
        if ($estado == true) {
          Jornadas::stateJornada($id, FALSE);//cambiar a estado false
        } else {
          Jornadas::stateJornada($id, TRUE);//cambiar a estado true
        }
        $jornadas = Jornadas::All();
        return response()->json($jornadas);
    }
}
