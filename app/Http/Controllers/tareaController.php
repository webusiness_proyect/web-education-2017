<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;


use education\file;

use Illuminate\Support\Facades\Input;

use Session;

use Validator;

use Redirect;

use education\Http\Requests\ActividadesRequest;

use education\Http\Controllers\menuController;

use education\Repositories\actividadesRepository;

use education\Repositories\unidadesRepository;

use education\Repositories\userRepository;

use education\actividad;

use education\tipo_actividad;

use education\User;


use Auth;

class tareaController extends Controller
{

      protected $menu;

  protected $actividades;

  protected $unidades;

  protected $users;

  public function __construct(actividadesRepository $actividad, unidadesRepository $unidad, userRepository $User)

  {

    $m = new menuController();

    $this->menu = $m->index();

    $this->actividades = $actividad;

    $this->unidades = $unidad;

    $this->users = $User;

  }
    public function index()
    {
        return view('file/file');
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new file;

        $user->title= Input::get('name');
        
      
         $nombre_servidor=request()->server->get('SERVER_NAME');

         $user->url=$nombre_servidor;

        if(Input::hasFile('image')){

                $destinationPath = request()->server->get('SERVER_NAME').'/img'; // upload path
               
                
                

            $file=Input::file('image');
            
            $file->move(public_path().'/img/',$file->getClientOriginalName()); // uploading file to given path

            $urlimg=$file->getClientOriginalName();

             //UNIDAD
              $this->unidades->fecha_inicio = date('Y-m-d');
              $this->unidades->fecha_final = date('Y-m-d');
        
              $user->unidad = $this->unidades->getUnidadActual();//traigo la unidad actual
            
           

           

            $archivo=$user->name='/img/'.$urlimg;
            $size=$user->size=$file->getClientsize();
            $type=$user->type=$file->getClientMimeType();

        }

        if($type=='image/png'&&$size<='100000')
        {

            $user->save();



             
             return redirect('file/show')->with('success-message', 'imagen png subido exitosamente');
            

        }

        elseif ($type=='image/jpeg'&&$size>='100000')

             {

            $user->save();

             
             return redirect('file/show')->with('success-message', 'imagen jpg subido exitosamente');
            

        }

        elseif($type=='application/pdf'&&$size>='100000')
        {
            $user->save();

             
             
            return redirect('file/show')->with('success-message', 'pdf subido exitosamente');
        }

        elseif($type=='application/vnd.openxmlformats'&&$size>='100000')
        {
            $user->save();

             
             return redirect('file/show')->with('success-message', 'pdf subido exitosamente');
        }


        else
        {
           
             return redirect("file/")->with('error-message', 'no se permite este tipo de archivos'); 

        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function showall()
    {
        $user=file::all();

        return view('file/show', compact('user'));

         //UNIDAD
              $this->unidades->fecha_inicio = date('Y-m-d');
              $this->unidades->fecha_final = date('Y-m-d');
        
              $user->unidad = $this->unidades->getUnidadActual();//traigo la unidad actual

        return view('file/show', ['items'=>$this->menu]);

    }


    public function show($id)
    {
        $user=file::findorfail($id);
        return view('file/showall',compact('user'));
    }

    public function getDownload($id)
    {

    //PDF file is stored under project/public/download/info.pdf
        $user=file::findorfail($id); 
        
        $file= public_path().$user;
          

        return response()->download($file);
    }


}
