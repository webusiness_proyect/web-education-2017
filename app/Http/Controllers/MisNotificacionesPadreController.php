<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;


use education\Http\Requests\ActividadesRequest;
use education\Http\Controllers\menuController;
use education\Repositories\NotificacionesRepository;
use Session;
use Auth;
use DB;

class MisNotificacionesPadreController extends Controller
{
    //

    protected $menu;
  protected $notificaciones;

public function __construct(NotificacionesRepository $notificacion)
  {
    $m = new menuController();
    $this->menu = $m->index();
    $this->notificaciones = $notificacion;
    
  }

   public function index(Request $request)
    {


        		$user=$this->notificaciones->usuario =(Auth::user()->id);
        
        

                 $notificaciones= DB::table('roles as r')
                 ->join('role_user as ru', 'r.id', '=', 'ru.role_id')
                 ->join('users as u', 'ru.user_id', '=', 'u.id')
                 ->join('TUTOR as t', 'u.id', '=', 't.id')
                 ->join('TUTOR_ESTUDIANTE as te', 't.id_tutor', '=', 'te.id_tutor')
                 ->join('ESTUDIANTES as e', 'e.id_estudiante', '=', 'te.id_estudiante')

                 ->join('NOTIFICACIONES as n', 'e.id_nivel_grado', '=', 'n.id_nivel_grado')
                 ->join('users as us', 'n.user', '=', 'us.id')
                 ->where('ru.user_id',$user)
                 ->where('n.id_rol','3')
                
     ->select(DB::raw('DISTINCT n.id_notificacion'),'n.titulo_notificacion','n.informacion_notificacion','n.id_nivel_grado','n.fecha_create','us.name')
                 ->get();


         /*Query que muestra todas las notificaciones como padre
    SELECT DISTINCT(n.id_notificacion), n.titulo_notificacion, n.informacion_notificacion, us.name 
    FROM roles as r join role_user as ru on r.id=ru.role_id 
    join users as u on ru.user_id=u.id 
    join TUTOR as t on u.id=t.id 
    join TUTOR_ESTUDIANTE as te on t.id_tutor=te.id_tutor
    join ESTUDIANTES as e on e.id_estudiante=te.id_estudiante
    join NOTIFICACIONES as n on e.id_nivel_grado=n.id_nivel_grado 
    join users as us on n.user=us.id 
    where ru.user_id=417 and n.id_rol=3
    */
      


        return view('notificacionespadre.index', [ 'items'=>$this->menu, 'notificaciones'=>$notificaciones]);
    }

     public function show(Request $request)

    {

        $id=$request['a'];
        $user = Auth::user()->id;
        $insert=DB::table('log_notificaciones')->insertGetId(
                      ['usuario'=>$user]);
        
        $notificaciones=DB::table('NOTIFICACIONES as n')
                ->join('users as u', 'n.user', '=', 'u.id')
                 ->where('n.id_notificacion', $id)
                 ->select('n.id_notificacion','n.titulo_notificacion','n.informacion_notificacion','n.id_nivel_grado','n.fecha_create','u.name')                 
                 ->get();

        //$notificaciones= notificaciones::where('id_notificacion', $request['a'])->first();

        return view('notificaciones.snotificacion', ['items'=>$this->menu, 'notificaciones'=>$notificaciones]);

    }



   
}
