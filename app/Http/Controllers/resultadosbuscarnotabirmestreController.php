<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;

use education\Unidades;

use education\actividad;

use education\Repositories\actividadesRepository;

use education\Repositories\RespondeActividadesRepository;

use DB;

use Session;

use Auth;


use education\Http\Requests\MisRespuestasActividadRequest;

class resultadosbuscarnotabirmestreController extends Controller
{
    protected $menu;

   protected $actividades;

   protected $ractividades;


    public function __construct(actividadesRepository $actividad, RespondeActividadesRepository $ractividad)


    {


      $m = new menuController();


      $this->menu = $m->index();

      $this->actividades = $actividad;

      $this->ractividades = $ractividad; 


    }
     public function index(Request $request)


    {


  
        $students=DB::table('AREAS as a')
             ->join('ASIGNACION_AREAS as aa','a.id_area','=','aa.id_area')
             ->join('ESTUDIANTES as e','e.id_nivel_grado','=','aa.id_nivel_grado')
             ->where('e.id','=',$request['user'])
             ->select('a.nombre_area','a.id_area', 'e.nombre_estudiante', 'e.apellidos_estudiante', 'e.id', 'e.id_nivel_grado')
             ->get();//traigo datos del estudiante seleccionado


    	$act=DB::table('ACTIVIDADES as a')
    		->join('files as f','a.id_actividad','=','f.id_actividad')
    		->join('AREAS as ar', 'f.area', '=', 'ar.id_area')
    		->join('users as u','a.id_usuario','=','u.id')
    		->join('role_user as ru', 'ru.user_id', '=', 'u.id')
    		->join('roles as r', 'r.id', '=', 'ru.role_id')
    		->join('TIPO_ACTIVIDAD as ta', 'ta.id_tipo_actividad', '=', 'a.id_tipo_actividad')
    		->select('f.id','f.area','f.id_usuario', 'f.calificacion','ar.nombre_area','a.id_actividad', 'a.nombre_actividad', 'a.descripcion_actividad')
    		->where('a.id_area','=',$request['area'])
    		->where('a.id_unidad','=',$request['unidad'])
    		->where('f.id_usuario','=',$request['user'])
    		->where('a.fecha_inicio','LIKE', '%'.$request['year'].'%')
    		->get();//traigo actividadesentregadas


    	$unidad=DB::table('UNIDADES')
             ->where('id_unidad','=',$request['unidad'])
             ->select('nombre_unidad','fecha_inicio', 'fecha_final')
             ->get();//traingo informacion de unidades
         
        $user=$request['user']; //creo variable con datos del alumno seleccionado
    	$sinnota= DB::table('ACTIVIDADES as a')
                    ->whereNotExists(function($query) use ($user) 
                    {
                        $query->from('files as f')
                              ->whereRaw('f.id_actividad =  a.id_actividad')
                              ->where('f.id_usuario','=',$user);
                    })
                    ->where('a.id_area',$request['area'])
                    ->where('a.id_unidad',$request['unidad'])
                    ->where('a.fecha_inicio','LIKE', '%'.$request['year'].'%')
                    ->select('a.id_actividad', 'a.nombre_actividad','a.descripcion_actividad','a.fecha_inicio', 'a.nota_total')
                    ->orderBy('a.id_actividad','asc')
                    ->get();//traigo tareas no entregadas de esta unidad

                 /* QUERY muestra los alumnos que no han entregado  una determinada tarea   de un determinado curso
             		SELECT * FROM  ACTIVIDADES as a
                      
                   where not exists (select * from files as f where f.id_actividad =  a.id_actividad and f.id_usuario=17) AND  a.id_area=3 and a.id_unidad=5 and a.fecha_inicio like '%2016%'

                    order By a.id_actividad

 
                */

   


      $year=$request['year'];

            return view('administra_notas_niveles.notas_unidad', ['items' => $this->menu,'actividades'=>$act,'students'=>$students,'year'=>$year,'unidad'=>$unidad,'sinnota'=>$sinnota]);
  

   	}


   	public function edit($id)
    {
        $this->ractividades->id = $id;
        $items1 = $this->ractividades->valorMaximoNota();
        //$ractividad = $this->ractividades->getRespuestaActividadesById1();
        $ractividad = $this->ractividades->getRespuestaActividadesById1File();
        //$valor_maximo_nota = $this->ractividades->valorMaximoNota();
        return view('administra_notas_niveles.eractividad', ['items'=>$this->menu, 'ractividad'=>$ractividad, 'notas'=>$items1]);
    }


    public function update(MisRespuestasActividadRequest $request, $id)


    {


        
        
        
    	 $areas=DB::table('ACTIVIDADES as a')
             ->where('a.id_actividad','=',$id)
             ->select('a.nota_total')
             ->get();//traingo informacion de unidades

        $actividad=actividad::where('id_actividad', $id)->select('nota_total')->get();

        



        $this->ractividades->id = $id;//asignar el id de la tarea actual
        $ractividades = $this->ractividades->getRespuestaActividadesById1();//
        $id_actividad=$this->ractividades->id_actividad;
       
        $this->ractividades->estado_entrega_actividad = $request['estado_entrega_actividad'];
        $calificacion=$this->ractividades->calificacion = $request['calificacion'];
        $this->ractividades->observaciones_maestro = $request['observaciones_maestro'];
       // $this->ractividades->fecha_calificacion = 

         if($calificacion<=$actividad)
         {
         	$this->ractividades->updateRespuestaActividadesFile();
         	Session::flash('mensaje', 'Se ha calificado la ACTIVIDAD exitosamente!!!');
        	return redirect()->route('administra_notas_niveles.index', [$id_actividad]);
         }
         else
         {
         		Session::flash('mensaje', 'NOTA ASIGNADA MAYOR A LA NOTA PERMITIDA!!!');
        	return redirect()->route('administra_notas_niveles.index', [$id_actividad]);
         }
        
      
      
       


                

            }

}
