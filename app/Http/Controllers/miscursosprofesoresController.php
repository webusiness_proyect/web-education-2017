<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;



use education\Http\Controllers\menuController;

use education\Repositories\docentesRepository;

use education\Repositories\estudiantesRepository;

use education\Repositories\unidadesRepository;



use Auth;

use Session;

use DB;


class miscursosprofesoresController extends Controller
{
    
  protected $menu;

  protected $docentes;

  protected $estudiantes;

  protected $unidades;

  public function __construct(docentesRepository $docente, estudiantesRepository $estudiante, unidadesRepository $unidad)

  {

    $m = new menuController();

    $this->menu = $m->index();

    $this->docentes = $docente;

    $this->estudiantes = $estudiante;

    $this->unidades = $unidad;

  }



  /**

     * Create a length aware custom paginator instance.

     *

     * @param  Collection  $items

     * @param  int  $perPage

     * @return \Illuminate\Pagination\LengthAwarePaginator

     */
    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function show(Request $request)

    {


        $this->unidades->fecha_inicio = date('Y-m-d');//fecha inicial de la unidad


        $this->unidades->fecha_final = date('Y-m-d');//fecha final de la unidad


        $unidad = $this->unidades->getUnidadActual();//obtenemos la unidad disponible de acuerdo a la fecha



        $this->docentes->id_usuario = $request['a'];//obtenemos el id de la persona logueada

        $aux = $this->docentes->getCursos();//obtenemos los cursos asignados al docente
        $usuario=$request['a'];

        //$cursos = $this->paginate($aux, 10);//paginar el resultado


            $query=trim($request->get('searchText'));

          $cursos=DB::table('AREAS AS a')
                 ->join('ASIGNACION_AREAS AS aa', 'a.id_area', '=', 'aa.id_area')
                 ->join('ASIGNACION_DOCENTE AS ad', 'aa.id_asignacion_area', '=', 'ad.id_asignacion_area')
                 
                 ->select('a.nombre_area', 'aa.id_asignacion_area', 'ad.id_asignacion_docente')
                    
                    
                    
                    ->where('ad.id_persona', $usuario)
                    
                   
                    ->get();

        return view('miscursosprofesores.index', ['items'=>$this->menu, 'unidad'=>$unidad, 'cursos'=>$cursos, 'searchText'=>$query]);

        /*
          select * from AREAS AS a JOIN ASIGNACION_AREAS AS aa ON a.id_area=aa.id_area
join ASIGNACION_DOCENTE AS ad on aa.id_asignacion_area=ad.id_asignacion_area
where ad.id_persona=38
                 */

    }

}
