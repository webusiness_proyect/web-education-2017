<?php

namespace education\Http\Controllers;


use Illuminate\Http\Request;
use education\Http\Requests;
use education\Http\Requests\inscripcionRequest;
use education\Http\Controllers\menuController;
use education\Repositories\estudiantesRepository;
use education\Repositories\tutorestudianteRepository;
use education\Repositories\referenciasRepository;
use education\Repositories\userRepository;
use education\Repositories\Usuario_estudianteRepository;
use education\Repositories\roleuserRepository;
use education\Repositories\rolesRepository;
use education\Departamento;
use education\Planes;
use education\Usuarios;
use education\Estudiantes;
use education\Jornadas;
use education\Tutor;
use education\Usuario_persona;
use \Validator;
use Session;
use education\User;
use education\Niveles;
use DB;
use PDF;
use Auth;
use Illuminate\Support\Facades\Route;
use Mail;
use Redirect;

class inscripcionController extends Controller
{
      protected $menu;
      protected $estudiantes;
      protected $tutores;
      protected $referencias;
      protected $users;
      protected $roleusers;
      protected $roles;
      protected $usuarioestudiantes;

      public function __construct(estudiantesRepository $estudiante, tutorestudianteRepository $tutor, referenciasRepository $referencia, userRepository $user, roleuserRepository $roleuser, rolesRepository $rol, Usuario_estudianteRepository $usuarioestudiante)
      {
        $m = new menuController();
        $this->menu = $m->index();
        $this->estudiantes = $estudiante;
        $this->tutores = $tutor;
        $this->referencias = $referencia;
        $this->users = $user;
        $this->roleusers = $roleuser;
        $this->roles = $rol;
        $this->usuarioestudiantes = $usuarioestudiante;
      }

    /**
     * Display a listing of the resource.
     *
   * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
     $user = User::where('id', Auth::user()->id)->first();
        if ($user->can(['crear-inscripcion', 'ver-inscripcion'])) {
           //$students = Estudiantes::paginate(5);
            $query=trim($request->get('searchText'));
            $students=DB::table('ESTUDIANTES')->join('NIVELES_GRADOS as ng', 'ESTUDIANTES.id_nivel_grado', '=', 'ng.id_nivel_grado')
                    ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
                    ->join('CARRERAS as c', 'ng.id_carrera', '=', 'c.id_carrera')
                    ->join('MUNICIPIO as m', 'ESTUDIANTES.id_municipio', '=', 'm.id_municipio')
                    ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
                    ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
                    ->leftjoin('REFERENCIAS as r', 'r.id_estudiante', '=', 'ESTUDIANTES.id_estudiante')
                    ->leftjoin('TUTOR_ESTUDIANTE as te', 'te.id_estudiante', '=', 'ESTUDIANTES.id_estudiante')
                    ->leftjoin('TUTOR as t', 'te.id_tutor', '=', 't.id_tutor')
                    ->join('users as u', 'ESTUDIANTES.id', '=', 'u.id')

          ->where('ESTUDIANTES.nombre_estudiante','LIKE', '%'.$query.'%')
          ->orWhere('ESTUDIANTES.apellidos_estudiante','LIKE', '%'.$query.'%')
          ->orWhere('c.nombre_carrera','LIKE', '%'.$query.'%')
          ->orWhere('g.nombre_grado','LIKE', '%'.$query.'%')
          ->orWhere('u.email','LIKE', '%'.$query.'%')
          ->where('estado_estudiante','=','1')
          ->orderBy('id_estudiante', 'asc')
          ->select('ESTUDIANTES.nombre_estudiante','ESTUDIANTES.apellidos_estudiante','ESTUDIANTES.telefono_casa','ESTUDIANTES.correo_estudiante','ESTUDIANTES.id_estudiante','ESTUDIANTES.estado_estudiante','ESTUDIANTES.id','g.estado_grado','g.id_grado','m.nombre_municipio', 'c.nombre_carrera', 'g.nombre_grado', 'n.nombre_nivel','r.nombre_referencia as nombre_ref','r.parentesco_referencia','r.parentesco_referencia','r.telefono_referencia','r.telefono_referencia','t.nombre_tutor','t.apellidos_tutor','t.cui_tutor','t.direccion_tutor','t.telefono_primario_tutor','t.telefono_trabajo_tutor','t.direccion_tutor','t.telefono_trabajo_tutor','t.cargo_tutor','u.email', 'u.password')
          ->paginate(10);

        return view('inscripciones.index', ['items'=> $this->menu,  'cursos'=>$students, 'searchText'=>$query]);
        }
        return abort(403);
    }
    /**
     * Show the form for creating a new resource.
    *
     * @return \Illuminate\Http\Response
     */
    public function create()
   {
        $deptos = Departamento::getDepartamentos();
        $planes = Planes::getPlanes();
        $jornadas = Jornadas::getJornadas();
        $niveles = Niveles::getNiveles();
        $telefonicas = array('tigo'=>'Tigo', 'claro'=>'Claro', 'movistar'=>'Movistar');

        $parentesco = array('padre'=>'padre',  'madre'=>'madre', 'tío(a)'=>'tío(a)','abuelo(a)'=>'abuelo(a)', 'familiar'=>'familiar');
        return view('inscripciones.create', ['items' => $this->menu, 'deptos'=>$deptos, 'planes'=>$planes, 'jornadas'=>$jornadas, 'niveles'=>$niveles, 'telefonicas'=>$telefonicas, 'parentesco'=>$parentesco]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(inscripcionRequest $request)
    {
      //capturando los datos del estudiante
      $this->estudiantes->id_nivel_grado = $request['id_grado'];
      $this->estudiantes->id_municipio = $request['id_municipio'];
      $this->estudiantes->nombre = $request['nombre_estudiante'];
      $this->estudiantes->apellido = $request['apellidos_estudiante'];
      $this->estudiantes->codigo_personal = $request['codigo_personal_estudiante'];
      $this->estudiantes->fecha_nacimiento = $request['fecha_nacimiento_estudiante'];
      $this->estudiantes->genero = $request['genero_estudiante'];
      $this->estudiantes->direccion = $request['direccion_estudiante'];
      $this->estudiantes->colonia = $request['colonia_estudiante'];
      $this->estudiantes->zona = $request['zona_estudiante'];
      $this->estudiantes->telefono_casa = $request['telefono_casa'];
      $this->estudiantes->empresa_telefonica = $request['empresa_telefonica'];
      $this->estudiantes->correo = $request['correo_estudiante'];
      $this->estudiantes->enfermedad_padecida = $request['enfermedad_padecida_estudiante'];
      $this->estudiantes->medicamento_recomendado = $request['medicamento_recomendado_estudiante'];
      $this->estudiantes->alergico = $request['alergico_estudiante'];
      $this->estudiantes->hospital = $request['hospital_estudiante'];
      $this->estudiantes->tipo_sangre = $request['tipo_sangre_estudiante'];
      $this->estudiantes->observaciones = $request['observaciones_estudiante'];

      //fin de la captura de datos de los estudiantes

        $b = false;
        DB::beginTransaction();//inicio de la transaccion
        try {
          //registrar al estudiante
          $result = $this->registrar_estudiante();
          if ($result == 0) {//si ya existe el estudiante entonces el metodo anterior devuelve un 0, indicando que ya esta registrado
            $msg = 'No se pudo regisrar al(la) estudiante porque ya esta registrado en el sistema!!!';
          } else {//de lo contrario le asignamos los tutores

             $user2 = Usuarios::join('ESTUDIANTES as e', 'users.id', '=', 'e.id')
            ->select('users.name as nombre', 'users.email as correo', 'e.nombre_estudiante', 'e.apellidos_estudiante', 'e.correo_estudiante','users.id as id')
             ->orderBy('users.id', 'desc')->first();
            /*Mail::send('emails.welcome', ['user' => $user2], function ($m) use ($user2) {
            $m->to($user2->correo_estudiante,  $user2->nombre_estudiante)->subject($user2->correo);
              });// envio correo con usuario y contraseña a estudiante*/

              Mail::send('emails.welcome', ['user' => $user2], function ($m) use ($user2) {
            $m->to($user2->correo_persona,  $user2->nombres_persona)->subject($user2->correo)->setBody('<h1><strong>BIENVENIDO A EDUCATION '. $user2->nombres_persona.' </h1></strong>  <h4> Su Usuario: '.$user2->correo.'</h4>  <h4> Su Contraseña:<strong> administrador123 </strong></h4><br/> <h5><center> para iniciar sesion ingresa <a href="http://education.wbinnovacionreal.com/"> <STRONG> AQUI </STRONG></a></center></h5><center> <h5>Powered by <a href="http://webusiness.co/"> Web Business S.A. </a>   <a href="tel:+5022327806">  <i class="fa fa-phone"></i>Tel. +50223278063</a> <i class="fa fa-at"></i>correo: education@webusiness.co <br/><br/></center>');
        });

            $this->registrar_tutor($request, $result);
            $this->registrar_referencia($request, $result);
            $this->invoice();
            $msg = 'Se ha registrado al nuevo estudiante, exitosamente!!!';
          }
          DB::commit();//confirmamos los cambios en la base de datos
          $b = true;
        } catch (Exception $e) {
            DB::rollBack();//regresamos al estado en que estaba la base de datos
            $msg = 'No se pudo completar la transacción, debido a un error!!!';
        }
        Session::flash('mensaje', $msg);
        if ($b == true) {
          return redirect('report');
        }
/*
         $user3 = Usuarios::join('TUTOR as t', 'users.id', '=', 't.id')
            ->select('users.name as nombre', 'users.email as correo', 't.nombre_tutor', 't.apellidos_tutor', 't.correo_electronico_tutor','users.id as id')
             ->orderBy('users.id', 'desc')->first();
            Mail::send('emails.padres', ['user' => $user3], function ($m) use ($user3) {
            $m->to($user3->correo_electronico_tutor,  $user3->nombre_tutor)->subject($user3->correo);
              });// envio correo con usuario y contraseña a estudiante
              */
     return redirect('/report');
        
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   


    /**

     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */









    //metodo para registrar un nuevo estudiante



    private function registrar_estudiante()

    {
        $existe = $this->estudiantes->findEstudianteWhere();
        if (count($existe) == 0) {
            //quitamos los guiones a la fecha de nacimiento
            $fecha = '';
            $aux = $this->estudiantes->fecha_nacimiento;
            for ($i=0; $i < strlen($aux); $i++) {
                if ($aux[$i] != '-') {
                  $fecha .= $aux[$i];//obtenemos la nueva fecha sin guiones
                }//fin del if
            }//fin del for

            //separar los nombres y apellidos
            list($pri_apellido) = explode(" ",$this->estudiantes->apellido);
            list($pri_nombre) = explode(" ",$this->estudiantes->nombre);
            //crear el nombre que aparece de la persona loqueada en el sistema
            $usuario = mb_strtolower($pri_nombre.' '.$pri_apellido);
            $pass = 'estudiante123';//crear el nombre del usuario
            //crear el nuevo correo del estudiante
            $nombre_servidor=request()->server->get('SERVER_NAME');
            //$correo = mb_strtolower($this->estudiantes->nombre[0].$pri_apellido.$fecha).'@webusiness.co';
            $correo2 = mb_strtolower($this->estudiantes->nombre[0].$pri_apellido.$fecha);
            //asignar los datos de nombre, correo y contraseña
            $this->users->name = $usuario;
            $this->users->email = $correo2;
            $this->users->password = bcrypt($pass);
            $idUsuario = $this->users->setUsuario();//registra el nuevo usuario
            //buscar al nuevo usuario registrado
            //$nUsuario = $this->users->findUsuarioWhere();
            //buscar el rol con nombre estudiante
            $this->roles->name = 'estudiantes';
            $rol = $this->roles->findRolWhere();

            //asignar el id del usuario
            $this->roleusers->user_id = $idUsuario;
            //asignar el id del rol
            $this->roleusers->role_id = $rol->id;
            //asignar el rol al usuario del estudiante
            $this->roleusers->setRoleUser();
            $this->estudiantes->id_usuario = $idUsuario;
            $idEstudiante = $this->estudiantes->setEstudiante();
            $this->usuarioestudiantes->id_usuario = $idUsuario;
            $this->usuarioestudiantes->id_alumno = $idEstudiante;
            $this->usuarioestudiantes->setUsuarioEstudiante();
            return $idEstudiante;
        } else {
          return 0;
        }//fin del if else
    }//fin del metodo para registrar un estudiante







    private function registrar_estudiante_usuario()

    {



      $existe = $this->estudiantes->findEstudianteWhere();



        if (count($existe) == 0) {



            //quitamos los guiones a la fecha de nacimiento



            



            $aux = $this->estudiantes->fecha_nacimiento;



            for ($i=0; $i < strlen($aux); $i++) {



                if ($aux[$i] != '-') {



                  $fecha .= $aux[$i];//obtenemos la nueva fecha sin guiones



                }//fin del if



            }//fin del for







            //separar los nombres y apellidos



            list($pri_apellido) = explode(" ",$this->estudiantes->apellido);



            list($pri_nombre) = explode(" ",$this->estudiantes->nombre);



            //crear el nombre que aparece de la persona loqueada en el sistema



            $usuario = mb_strtolower($pri_nombre.' '.$pri_apellido);



            $pass = 'estudiante123';//crear el nombre del usuario



            //crear el nuevo correo del estudiante



            $correo = mb_strtolower($this->estudiantes->nombre[0].$pri_apellido.$fecha);







            //asignar los datos de nombre, correo y contraseña



            $this->users->name = $usuario;



            $this->users->email = $correo;



            $this->users->password = bcrypt($pass);



            $idUsuario = $this->users->setUsuario();//registra el nuevo usuario







            //buscar al nuevo usuario registrado



            //$nUsuario = $this->users->findUsuarioWhere();



            //buscar el rol con nombre estudiante



            $this->roles->name = 'estudiantes';



            $rol = $this->roles->findRolWhere();







            //asignar el id del usuario



            $this->roleusers->user_id = $idUsuario;



            //asignar el id del rol



            $this->roleusers->role_id = $rol->id;



            //asignar el rol al usuario del estudiante



            $this->roleusers->setRoleUser();







            $this->estudiantes->id_usuario = $idUsuario;



            $idEstudiante = $this->estudiantes->setEstudiante();







            return $idEstudiante;



        } else {



          return 0;



        }//fin del if else





    }







    //metodo para registrar un tutor o varios tutores



    public function registrar_tutor($request, $idEstudiante)



    {



      //captura de datos de los tutores
      

      $nombre = $request['tutor.*.nombre'];



      $apellido = $request['tutor.*.apellidos'];



      $direccion = $request['tutor.*.direccion'];



      $telefono_primario = $request['tutor.*.telefono_primario'];



      $empresa_telefono = $request['tutor.*.empresa_telefono'];



      $correo_electronico = $request['tutor.*.correo_electronico'];



      $cui = $request['tutor.*.cui'];



      $lugar_trabajo = $request['tutor.*.lugar_trabajo'];



      $direccion_trabajo = $request['tutor.*.direccion_trabajo'];



      $telefono_trabajo = $request['tutor.*.telefono_trabajo'];



      $empresa_telefono_trabajo = $request['tutor.*.empresa_telefono_trabajo'];



      $cargo = $request['tutor.*.cargo'];



      $parentesco = $request['tutor.*.parentesco'];



      //fin de la captura de datos de los tutores



        for ($i=0; $i < count($nombre); $i++) {



            //asignacion de datos del tutor



              $this->tutores->nombre = $nombre[$i];



              $this->tutores->apellido = $apellido[$i];



              $this->tutores->direccion = $direccion[$i];



              $this->tutores->telefono_primario = $telefono_primario[$i];



              $this->tutores->empresa_telefono = $empresa_telefono[$i];



              $this->tutores->correo_electronico = $correo_electronico[$i];



              $this->tutores->cui = $cui[$i];



              $this->tutores->lugar_trabajo = $lugar_trabajo[$i];



              $this->tutores->direccion_trabajo = $direccion_trabajo[$i];



              $this->tutores->telefono_trabajo = $telefono_trabajo[$i];



              $this->tutores->empresa_telefono_trabajo = $empresa_telefono_trabajo[$i];



              $this->tutores->cargo = $cargo[$i];



              $this->tutores->parentesco = $parentesco[$i];



            $existe2 = $this->tutores->findTutorByDPI();//buscamos si existe tutor
            if (count($existe2) == 0) {//si no existe
               $fecha2 = '';
               $aux2 = $this->estudiantes->fecha_nacimiento;
            for ($i=0; $i < strlen($aux2); $i++) 
            {
                if ($aux2[$i] != '-') 
                {
                  $fecha2 .= $aux2[$i];//obtenemos la nueva fecha sin guiones
                }//fin del if
            }//fin del for

              //separar los nombres y apellidos

              list($pri_apellido2) = explode(" ",$this->tutores->apellido);
              list($pri_nombre2) = explode(" ",$this->tutores->nombre);

              //crear el nombre que aparece de la persona loqueada en el sistema

              $usuario3 = mb_strtolower($pri_nombre2.' '.$pri_apellido2);

              $pass3 = 'padres123';//crear el nombre del usuario

               $nombre_servidor=request()->server->get('SERVER_NAME');
              //crear el nuevo correo del estudiante

              $correo3 = mb_strtolower($this->tutores->nombre[0].$pri_apellido2.$fecha2);

              //asignar los datos de nombre, correo y contraseña

              $this->users->name = $usuario3;
              $this->users->email = $correo3;
              $this->users->password = bcrypt($pass3);

              //registra el nuevo usuario

              $idUsuario3 = $this->users->setUsuario();//registra el nuevo usuario PADRE2

              $this->roles->name = 'padres';

              $rol = $this->roles->findRolWhere();


            //asignar el id del usuario


            $this->roleusers->user_id = $idUsuario3;


            //asignar el id del rol

            $this->roleusers->role_id = $rol->id;


            //asignar el rol al usuario del estudiante


            $this->roleusers->setRoleUser();


              //$this->tutores->id_usuario = $this->users->setUsuario();//PADRE1

            $this->tutores->id_usuario =$idUsuario3;

              //return 'No existe ningun tutor con el dpi indicado';

              //registro usuario cj


              $idTutor = $this->tutores->setTutor();


              $this->tutores->setTutorEstudiante($idTutor, $idEstudiante);
           
            return $idTutor;


              //return 'No existe ningun tutor con el dpi indicado';



            }else {



          return 0;


            }//fin del if else









        }//fin dle for









    }//fin del metodo para registrar un tutor para el estudiante







    //metodo para registrar a la o las referencias de los estudiantes



    public function registrar_referencia($request, $idEstudiante)



    {



      //captura de datos de las referencias del estudiante



        $nombre = $request['referencia.*.nombre'];



        $parentesco = $request['referencia.*.parentesco'];



        $telefono = $request['referencia.*.telefono'];



        //fin de la captura de datos de las referencias del estudiante



        for ($i=0; $i < count($nombre); $i++) {



          //asignacion de datos de la referencia



          $this->referencias->nombre = $nombre[$i];



          $this->referencias->parentesco = $parentesco[$i];



          $this->referencias->telefono = $telefono[$i];



          $this->referencias->id_estudiante = $idEstudiante;



          //registra a la nueva referencia



          $this->referencias->setReferencia();



        }//fin del for



    }//fin del metodo para registrar una nueva referencia







    //metodo que muestra el formulario de constancia de un estudiante, para poder generar el pdf







    /*public function constancia()



    {



      return view('inscripciones.constancia', ['items'=> $this->menu]);



    }*/







    //metodo para generar la constancia de inscripcion del estudiante



   







  public function invoice() 

     {



        

       

        $data = $this->getData();

        $data2 = $this->getData2();

        $date = date('d-m-Y');

        

        $view =  \View::make('report.invoice', compact('data', 'data2', 'date'))->render();

        $pdf = \App::make('dompdf.wrapper');

        $pdf->loadHTML($view);

        return $pdf->stream('invoice');

    }

 

    public function getData() 

    {





  

    



        $estudiantedata = $this->estudiantes->getUltimoEstudianteInscrito();//datos de estudiante



        /*$datos = array();

        foreach ($estudiantedata as $key => $row) 

        {

          

            $datos['nombre'] = ($row->nombre_estudiante);

            return $datos;

           

          

        }

        */




  $data =[
          'id'=>$estudiantedata->id,
          'nombre' => $estudiantedata->nombre,
          'apellidos' => $estudiantedata->apellido,
          'fecha' => $estudiantedata->fecha,
          'direccion' => $estudiantedata->direccion,
          'colonia' => $estudiantedata->colonia,
          'zona' => $estudiantedata->zona,
          'telefono' => $estudiantedata->telefono,
          'referencia' => $estudiantedata->nombre_ref,
          'correo_estudiante' => $estudiantedata->correo_estudiante,
          'fecha_registro_estudiante' => $estudiantedata->fecha_registro_estudiante,
          'enfermedad_padecida_estudiante' => $estudiantedata->enfermedad_padecida_estudiante,
          'medicamento_recomendado_estudiante' => $estudiantedata->medicamento_recomendado_estudiante,
          'alergico_estudiante' => $estudiantedata->alergico_estudiante,
          'hospital_estudiante' => $estudiantedata->hospital_estudiante,
          'tipo_sangre_estudiante' => $estudiantedata->tipo_sangre_estudiante,
          'observaciones_estudiante' => $estudiantedata->observaciones_estudiante,
          'empresa_telefonica' => $estudiantedata->empresa_telefonica,
          'nombre_carrera' => $estudiantedata->nombre_carrera,
          'nombre_grado' => $estudiantedata->nombre_grado,
          'nombre_nivel' => $estudiantedata->nombre_nivel,
          'parentesco_referencia' => $estudiantedata->parentesco_referencia,
          'telefono_referencia' => $estudiantedata->telefono_referencia,
          'apellidos_tutor' => $estudiantedata->apellidos_tutor,
          'correo_electronico_tutor'=> $estudiantedata->correo_electronico_tutor,
          'nombre_tutor' => $estudiantedata->nombre_tutor,
          'cui_tutor' => $estudiantedata->cui_tutor,
          'direccion_tutor' => $estudiantedata->direccion_tutor,
          'telefono_primario_tutor' => $estudiantedata->telefono_primario_tutor,
          'telefono_trabajo_tutor' => $estudiantedata->telefono_trabajo_tutor,
          'direccion_tutor' => $estudiantedata->direccion_tutor,
          'telefono_trabajo_tutor' => $estudiantedata->telefono_trabajo_tutor,
          'cargo_tutor' => $estudiantedata->cargo_tutor,
          'email' => $estudiantedata->email,
          'nombre_municipio' => $estudiantedata->nombre_municipio,
          'url' => request()->server->get('SERVER_NAME')

            


        ];



        return $data;

       



/*

        $estudiante = $this->estudiantes->getUltimoEstudianteInscrito();



        $data =  [

            'quantity'      =>  $estudiante->apellidos_estudiante,

            'description'   => 'some ramdom text',

            'price'   => '500',

            'total'     => '500'

        ];

        */

        }









  public function reports() 

     {



        

       

        $data = $this->getData2();

        $date = date('d-m-Y');

        

        $view =  \View::make('report.invoice', compact('data', 'date'))->render();

        $pdf = \App::make('dompdf.wrapper');

        $pdf->loadHTML($view);

        return $pdf->stream('reports');

    }

 

    public function getData2() 

    {




  $userdata = $this->estudiantes->getusuarios();
        /*$datos = array();
        foreach ($estudiantedata as $key => $row) 
        {
          
            $datos['nombre'] = ($row->nombre_estudiante);
            return $datos;
           
          
        }
        */

       


        $data2 =[
              'email'=>$userdata->email
         
            
          

        ];
         return $data2;

       


       



/*

        $estudiante = $this->estudiantes->getUltimoEstudianteInscrito();



        $data =  [

            'quantity'      =>  $estudiante->apellidos_estudiante,

            'description'   => 'some ramdom text',

            'price'   => '500',

            'total'     => '500'

        ];

        */

        }


          public function destroy($id, Request $request)
    {
        $estado = $request['estado'];
        if ($estado == 1) {
          Estudiantes::stateEstudiante($id, FALSE);
          Estudiantes::stateUser($id, FALSE);
        } else {
          Estudiantes::stateEstudiante($id, TRUE);
          Estudiantes::stateUser($id, TRUE);
        }
        $students = DB::table('ESTUDIANTES')->join('NIVELES_GRADOS as ng', 'ESTUDIANTES.id_nivel_grado', '=', 'ng.id_nivel_grado')
         ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
                    ->join('CARRERAS as c', 'ng.id_carrera', '=', 'c.id_carrera')
                    ->join('MUNICIPIO as m', 'ESTUDIANTES.id_municipio', '=', 'm.id_municipio')
                    ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
                    ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
                    ->leftjoin('REFERENCIAS as r', 'r.id_estudiante', '=', 'ESTUDIANTES.id_estudiante')
                    ->leftjoin('TUTOR_ESTUDIANTE as te', 'te.id_estudiante', '=', 'ESTUDIANTES.id_estudiante')
                    ->leftjoin('TUTOR as t', 'te.id_tutor', '=', 't.id_tutor')
                    ->join('users as u', 'ESTUDIANTES.id', '=', 'u.id')

          ->orderBy('id_estudiante', 'asc')
          ->select('ESTUDIANTES.nombre_estudiante','ESTUDIANTES.apellidos_estudiante','ESTUDIANTES.telefono_casa','ESTUDIANTES.correo_estudiante','ESTUDIANTES.id_estudiante','g.estado_grado','g.id_grado','m.nombre_municipio', 'c.nombre_carrera', 'g.nombre_grado', 'n.nombre_nivel','r.nombre_referencia as nombre_ref','r.parentesco_referencia','r.parentesco_referencia','r.telefono_referencia','r.telefono_referencia','t.nombre_tutor','t.apellidos_tutor','t.cui_tutor','t.direccion_tutor','t.telefono_primario_tutor','t.telefono_trabajo_tutor','t.direccion_tutor','t.telefono_trabajo_tutor','t.cargo_tutor','u.email', 'u.password')

          ->paginate(10);

        return response()->json($students);
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->estudiantes->id_estudiante = $id;
        $deptos = Departamento::getDepartamentos();
        $planes = Planes::getPlanes();
        $jornadas = Jornadas::getJornadas();
        $niveles = Niveles::getNiveles();
        $dato = Estudiantes::findEstudiante($id);

     
        $telefonicas = array('tigo'=>'Tigo', 'claro'=>'Claro', 'movistar'=>'Movistar');
        return view('inscripciones.einscripcion', ['items' => $this->menu, 'deptos'=>$deptos, 'planes'=>$planes, 'jornadas'=>$jornadas, 'niveles'=>$niveles, 'telefonicas'=>$telefonicas,'dato'=>$dato]);
        
     }

 public function show($id)
    {
       //
      $this->tutores->id_estudiante = $id;
        $deptos = Departamento::getDepartamentos();
        $planes = Planes::getPlanes();
        $jornadas = Jornadas::getJornadas();
        $niveles = Niveles::getNiveles();
        $dato = Tutor::findTutorEstudiante($id);
     
        $telefonicas = array('tigo'=>'Tigo', 'claro'=>'Claro', 'movistar'=>'Movistar');
        return view('inscripciones.pinscripcion', ['items' => $this->menu, 'deptos'=>$deptos, 'planes'=>$planes, 'jornadas'=>$jornadas, 'niveles'=>$niveles, 'telefonicas'=>$telefonicas,'dato'=>$dato]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)

    {

      /*
      UPDATE `educativo02`.`estudiantes` SET `codigo_personal_estudiante` = '00123', `estado_estudiante` = '1' WHERE `estudiantes`.`id_estudiante` = 6;
      */



        
        $this->estudiantes->id_estudiante = $id;//asignar el id de la tarea actual
        $id_estudiante=$this->estudiantes->findEstudiante($id);//
       
       $this->estudiantes->id_nivel_grado = $request['id_grado'];
      $this->estudiantes->id_municipio = $request['id_municipio'];
      $this->estudiantes->nombre = $request['nombre_estudiante'];
      $this->estudiantes->apellido = $request['apellidos_estudiante'];
      $this->estudiantes->codigo_personal = $request['codigo_personal_estudiante'];
      $this->estudiantes->fecha_nacimiento = $request['fecha_nacimiento_estudiante'];
      $this->estudiantes->genero = $request['genero_estudiante'];
      $this->estudiantes->direccion = $request['direccion_estudiante'];
      $this->estudiantes->colonia = $request['colonia_estudiante'];
      $this->estudiantes->zona = $request['zona_estudiante'];
      $this->estudiantes->telefono_casa = $request['telefono_casa'];
      $this->estudiantes->empresa_telefonica = $request['empresa_telefonica'];
      $this->estudiantes->correo = $request['correo_estudiante'];
      $this->estudiantes->enfermedad_padecida = $request['enfermedad_padecida_estudiante'];
      $this->estudiantes->medicamento_recomendado = $request['medicamento_recomendado_estudiante'];
      $this->estudiantes->alergico = $request['alergico_estudiante'];
      $this->estudiantes->hospital = $request['hospital_estudiante'];
      $this->estudiantes->tipo_sangre = $request['tipo_sangre_estudiante'];
      $this->estudiantes->observaciones = $request['observaciones_estudiante'];
       // $this->ractividades->fecha_calificacion = 
        $this->estudiantes->updateEstudiante();
        Session::flash('mensaje', 'Se ha modificado los datos de este estudiante');
        return redirect('/inscripcionestudiantes');



    }











}



