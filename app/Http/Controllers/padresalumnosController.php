<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;

use Illuminate\Pagination\LengthAwarePaginator;

use Illuminate\Support\Collection;


use education\Http\Controllers\menuController;

use education\Repositories\docentesRepository;

use education\Repositories\estudiantesRepository;

use education\Repositories\padresRepository;

use education\Repositories\unidadesRepository;

use Auth;

use Session;

use DB;

class padresalumnosController extends Controller
{
   protected $menu;

   protected $padres;

   protected $unidades;

   protected $estudiantes;



  public function __construct(estudiantesRepository $estudiante, padresRepository $padre, unidadesRepository $unidad)

  {

    $m = new menuController();

    $this->menu = $m->index();

    $this->padres = $padre;

    $this->unidades = $unidad;

    $this->estudiantes = $estudiante;

  }


    public function index(Request $request)

    {




        $this->padres->id_usuario = Auth::user()->id;//obtenemos el id de la persona logueada

        $alumnos = $this->padres->getAlumnos();//obtenemos los cursos asignados al docente

        //$cursos = $this->paginate($aux, 10);//paginar el resultado


           

        return view('misalumnos.index', ['items'=>$this->menu, 'alumnos'=>$alumnos]);

      

    }

    public function show(Request $request)
    {


          $this->estudiantes->id_usuario = $request['a'];
          $this->estudiantes->id = $request['a'];
          $this->estudiantes->id_estudiante = $request['b'];
          $this->unidades->fecha_inicio = date('Y-m-d');
          $this->unidades->fecha_final = date('Y-m-d');
          $unidad = $this->unidades->getUnidadActual();//traigo la unidad actual
          $this->estudiantes->unidad=$unidad['id_unidad'];
          

          
          $items = $this->estudiantes->getTodasMisNotasFile();

          $items2 = $this->estudiantes->getMitotalNotaFile();
            


        return view('MisNotasPorUnidad.index', [ 'items'=>$this->menu, 'unidad'=>$unidad, 'notas'=>$items]);


    }

     public function alumnos(Request $request)

    {




        $this->padres->id_usuario = Auth::user()->id;//obtenemos el id de la persona logueada

        $alumnos = $this->padres->getAlumnos();//obtenemos los cursos asignados al docente

        //$cursos = $this->paginate($aux, 10);//paginar el resultado


           

        return view('misalumnos.alumnos', ['items'=>$this->menu, 'alumnos'=>$alumnos]);

      

    }


}
