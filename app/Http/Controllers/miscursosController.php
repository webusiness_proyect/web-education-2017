<?php





namespace education\Http\Controllers;





use Illuminate\Http\Request;





use education\Http\Requests;


use education\Http\Controllers\menuController;


use education\Repositories\unidadesRepository;


use education\Repositories\estudiantesRepository;


use education\Repositories\userRepository;


use education\User;


use Auth;

use DB;





class miscursosController extends Controller


{


  protected $menu;


  protected $unidades;


  protected $estudiantes;


  protected $usuarios;


  public function __construct(menuController $m, unidadesRepository $unidad, estudiantesRepository $estudiante, userRepository $usuario)


  {


    $this->menu = $m->index();


    $this->unidades = $unidad;


    $this->estudiantes = $estudiante;


    $this->usuarios=$usuario;


  }


    /**


     * Display a listing of the resource.


     *


     * @return \Illuminate\Http\Response


     */


    public function index(Request $request)


    {





        $this->unidades->fecha_inicio = date('Y-m-d');//fecha inicial de la unidad


        $this->unidades->fecha_final = date('Y-m-d');//fecha final de la unidad


        $unidad = $this->unidades->getUnidadActual();//obtenemos la unidad disponible de acuerdo a la fecha

        /*$unidades=DB::table('UNIDADES')
          
          ->where('fecha_inicio','<=', date('Y-m-d'))
          ->where('fecha_final','>=', date('Y-m-d'))
          ->select('nombre_unidad')
          ->first();

          $unidad=$unidades['nombre_unidad'];
          */

       
        $this->estudiantes->id_usuario = Auth::user()->id;

          


        //$cursos = $this->estudiantes->getCursosEstudiante();

        /*
        return Estudiantes::join('NIVELES_GRADOS as ng', 'ESTUDIANTES.id_nivel_grado', '=', 'ng.id_nivel_grado')

                    ->join('ASIGNACION_AREAS as aa', 'ng.id_nivel_grado', '=', 'aa.id_nivel_grado')

                    ->join('AREAS as a', 'aa.id_area', '=', 'a.id_area')

                    ->where('ESTUDIANTES.id', $this->id_usuario)

                    ->select('ESTUDIANTES.id','aa.id_asignacion_area', 'a.nombre_area', 'a.id_area')

                    ->get();
                    */
         $query=trim($request->get('searchText'));

          $cursos=DB::table('ESTUDIANTES')
          ->join('NIVELES_GRADOS as ng', 'ESTUDIANTES.id_nivel_grado', '=', 'ng.id_nivel_grado')
          ->join('ASIGNACION_AREAS as aa', 'ng.id_nivel_grado', '=', 'aa.id_nivel_grado')
          ->join('AREAS as a', 'aa.id_area', '=', 'a.id_area')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'npj.id_nivel_plan_jornada', '=', 'ng.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('GRADOS as g', 'g.id_grado', '=', 'ng.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as p', 'npj.id_plan', '=', 'p.id_plan')
          ->join('SALONES as sal', 'aa.id_salon', '=', 'sal.id_salon')

          ->select('ESTUDIANTES.id','aa.id_asignacion_area', 'a.nombre_area', 'a.id_area','g.nombre_grado','n.nombre_nivel','ca.nombre_carrera','j.nombre_jornada','p.nombre_plan', 'sal.nombre_salon')

          
          ->where('a.nombre_area','LIKE', '%'.$query.'%')
          ->where('ESTUDIANTES.id','=', Auth::user()->id)
          ->where('estado_estudiante','=','1')
          ->paginate(10);

          /*
         SELECT distinct(nombre_area) FROM ESTUDIANTES
          join NIVELES_GRADOS as ng ON ESTUDIANTES.id_nivel_grado=ng.id_nivel_grado
          join ASIGNACION_AREAS as aa on ng.id_nivel_grado = aa.id_nivel_grado
          join AREAS as a on aa.id_area = a.id_area 
          join NIVELES_PLANES_JORNADAS as npj on npj.id_nivel_plan_jornada = ng.id_nivel_plan_jornada 
          join NIVELES as n on npj.id_nivel = n.id_nivel 
          join GRADOS as g on g.id_grado = ng.id_grado 
          join CARRERAS as ca on ng.id_carrera=ca.id_carrera
          join JORNADAS as j on npj.id_jornada=j.id_jornada
          join PLANES as p on npj.id_plan=p.id_plan
          join SALONES as sal on aa.id_salon=sal.id_salon


          where ESTUDIANTES.id=17
          and estado_estudiante=1
          */







        return view('estudiantes.index', ['items'=>$this->menu, 'unidad'=>$unidad,  'cursos'=>$cursos,  'searchText'=>$query]);


    }





    /**


     * Show the form for creating a new resource.


     *


     * @return \Illuminate\Http\Response


     */


    public function create()


    {


        //


    }





    /**


     * Store a newly created resource in storage.


     *


     * @param  \Illuminate\Http\Request  $request


     * @return \Illuminate\Http\Response


     */


    public function store(Request $request)


    {


        //


    }





    /**


     * Display the specified resource.


     *


     * @param  int  $id


     * @return \Illuminate\Http\Response


     */


    public function show($id)


    {


        //


    }





    /**


     * Show the form for editing the specified resource.


     *


     * @param  int  $id


     * @return \Illuminate\Http\Response


     */


    public function edit($id)


    {


        //


    }





    /**


     * Update the specified resource in storage.


     *


     * @param  \Illuminate\Http\Request  $request


     * @param  int  $id


     * @return \Illuminate\Http\Response


     */


    public function update(Request $request, $id)


    {


        //


    }





    /**


     * Remove the specified resource from storage.


     *


     * @param  int  $id


     * @return \Illuminate\Http\Response


     */


    public function destroy($id)


    {


        //


    }


}


