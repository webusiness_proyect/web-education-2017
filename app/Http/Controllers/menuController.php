<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Sub_opciones;
use education\User;
use Auth;
class menuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //obtemos lo permisos asignados al usuario que se a autenticado en el sistema
      $permisos = User::getPermisosRol(Auth::user()->id);
      //declarcion de variables tipo array para ordenar las opciones que tiene disponible
      $Institucional = array();
      $Usuario = array();
      $Persona = array();
      $Docente = array();
      $Estudiante = array();
      $Tutores = array();
      $Directores = array();
      $i = false;
      $u = false;
      $p = false;
      $d = false;
      $e = false;
      $t = false;
      $dir = false;
      foreach ($permisos as $key => $permiso) {
        switch ($permiso->contenedor_principal) {
          case '1':
              $Institucional[$permiso->nombre_opcion] = array('url'=>$permiso->ruta_opcion);
              $i = true;
            break;
          case '2':
              $Usuario[$permiso->nombre_opcion] = array('url'=>$permiso->ruta_opcion);
              $u = true;
            break;
          case '3':
              $Persona[$permiso->nombre_opcion] = array('url'=>$permiso->ruta_opcion);
              $p = true;
            break;
          case '4':
            $Docente[$permiso->nombre_opcion] = array('url'=>$permiso->ruta_opcion);
            $d = true;
            break;
          case '5':
            $Estudiante[$permiso->nombre_opcion] = array('url'=>$permiso->ruta_opcion);
            $e = true;
            break;

          case '6':
            $Tutores[$permiso->nombre_opcion] = array('url'=>$permiso->ruta_opcion);
            $t = true;
            break;

              case '7':
            $Directores[$permiso->nombre_opcion] = array('url'=>$permiso->ruta_opcion);
            $dir = true;
            break;
        }

      }
      $items = array();
       if ($i == true) {
        $items['Institucional'] = ['submenu' => $Institucional];
      }

       if ($p == true) {
        $items['Persona'] = ['submenu' => $Persona];
      }

      if ($u == true) {
        $items['Usuario'] = ['submenu' => $Usuario];
      }
     

     


      if ($d == true) {
        $items['Docente'] = ['submenu' => $Docente];
      }
      if ($e == true) {
        $items['Estudiante'] = ['submenu' => $Estudiante];
      }
       if ($t == true) {
        $items['Tutores'] = ['submenu' => $Tutores];
      }

      if ($dir == true) {
        $items['Directores'] = ['submenu' => $Directores];
      }
      /*$items = [
        'Institucional'       => ['submenu' => $Institucional

                            ],
          'Institucional'       => ['submenu' => [
                                  'Carreras'     => ['url'=>'carreras'],
                                  'Niveles'   => ['url'=>'niveles'],
                                  'Grados' => ['url'=>'grados'],
                                  'Planes Niveles' => ['url'=>'asignarniveles'],
                                  'Grados Niveles' => ['url'=>'gradoniveles'],
                                  'Secciones' => ['url'=>'secciones'],
                                  'Jornadas' => ['url'=>'jornadas'],
                                  'Salones' => ['url'=>'salones'],
                                  'Planes' => ['url'=>'planes'],
                                  'Cursos' => ['url'=>'areas'],
                                  'Pensum Grados' => ['url'=>'pensum'],
                                  ]
                              ],
            'Usuario'       => ['submenu' => [
                                    'usuarios'     => ['url' => 'usuarios'],
                                    'roles'   => ['url' => 'roles']
                                    ]
                                ],
            'Persona'       => ['submenu' => [
                                    'empleados'     => ['url'=>'personas'],
                                    'puestos'   => ['url'=>'puestos'],
                                    'permisos' => ['url'=>'permisos']
                                    ]
                                ],


        ];*/

        return $items;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
