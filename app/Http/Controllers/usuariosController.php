<?php
namespace education\Http\Controllers;


use Illuminate\Http\Request;



use education\Http\Controllers\menuController;

use education\User;

use education\Usuarios;

use education\Roles;

use education\Personas;


use education\Repositories\userRepository;

use education\Usuario_persona;

use education\Estudiantes;

use \Validator;

use Session;

use Mail;

use PDF;

use DB;

use Hash;

use education\Role;

use Auth;

use Illuminate\Support\Facades\Route;

use Redirect;

class usuariosController extends Controller

{

    protected $menu;

    protected $usuarios;

    public function __construct(userRepository $usuario)

    {

      $m = new menuController();

      $this->menu = $m->index();

      $this->usuarios = $usuario;

    }

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(Request $request)

    {

        $user = User::where('id', Auth::user()->id)->first();

        if ($user->can('crear-usuario')) {

         //$usuarios = User::paginate(5);

          

            $query=trim($request->get('searchText'));

          $usuarios=DB::table('users as u')->join('role_user AS ru', 'u.id', '=', 'ru.user_id')
          ->join('roles as r', 'ru.role_id', '=', 'r.id')
        
          ->select('u.name as nombre', 'u.email as correo', 'r.name as rol', 'u.id as id', 'u.estado_usuario')


          ->where('u.name','LIKE', '%'.$query.'%')
          ->orWhere('u.email','LIKE', '%'.$query.'%')
          ->orWhere('r.name','LIKE', '%'.$query.'%')
          
          ->orderBy('u.id', 'asc')
          ->paginate(10);
          

          /*$usuarios =  DB::table('users as u')

                 ->join('role_user as ru', 'u.id', '=', 'ru.user_id')

                 ->join('roles as r', 'ru.role_id', '=', 'r.id')

                 ->select('u.name as nombre', 'u.email as correo', 'r.name as rol')



            ->get();*/

          return view('usuarios.index', ['usuarios'=>$usuarios, 'items'=>$this->menu, 'searchText'=>$query]);

        }

        return abort(403);

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        $user = User::where('id', Auth::user()->id)->first();

        if ($user->can('crear-usuario')) {

          $roles = Role::All();

          foreach ($roles as $key => $row) {

            $r[$row->id] = mb_strtoupper($row->name);

          }

          return view('usuarios.nusuario', ['roles'=>$r, 'items'=>$this->menu]);

        }

        return abort(403);

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

      //crear el arreglo de los mensajes de validacion

          $dominio='@webusiness.co';//YA NO SE UTILIZARA 28/03/2017

          $messages = array(

          'id_persona.required' => 'Hey! EL campo persona es requerido!!!.',

          'id_rol.required' => 'Hey! EL campo rol es requerido!!!.',

          );



          $v = Validator::make(

          $request->all(),

           [

                'id_persona' => 'required',

                'id_rol' => 'required',

            ],

            $messages);



        if ($v->fails())

        {

            return redirect()->back()->withInput()->withErrors($v->errors());

        }

      $id = $request['id_persona'];

      $rol = $request['id_rol'];

      $persona = Personas::findPersona($id);
      /*
         public static function findLastUsers()
    {
      return users::join('USUARIO_PERSONA', 'USUARIO_PERSONA.user_id', '=', 'users.id')
          ->join('PERSONAS as p', 'USUARIO_PERSONA.id_persona', '=', 'p.id_persona')
        
          ->select('users.name as nombre', 'users.email as correo', 'p.nombres_persona', 'p.apellidos_persona', 'p.correo_persona','users.id as id')

          ->orderBy('users.id', 'desc')->first();
              //funcion que muestra el ultimo usuario ingresado
    /*SELECT * FROM users as u join  `USUARIO_PERSONA` on USUARIO_PERSONA.user_id=u.id
      join PERSONAS as p on USUARIO_PERSONA.id_persona=p.id_persona
      order by u.id desc limit 1
      
    }
    */

    

      // Separamos los Apellidos

      if($rol==1)//si el puesto es ADMINISTRADOR
      {

      $aux = $persona->fecha_nacimiento_persona;

      $fecha = '';

      for ($i=0; $i < strlen($aux); $i++) {//quitamos los guiones de la fecha de nacimiento

        if ($aux[$i] != '-') {

          $fecha .= $aux[$i];

        }

      }



      list($pri_apellido) = explode(" ",$persona->apellidos_persona);

      list($pri_nombre) = explode(" ",$persona->nombres_persona);

      $usuario = mb_strtolower($pri_nombre.' '.$pri_apellido);

      //$pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);

      $pass = 'administrador123';

      $correo = mb_strtolower($persona->nombres_persona[0].$pri_apellido.$fecha);



       $password = Hash::make($pass);//password cifrado

      DB::beginTransaction();

      try {

        $result = User::setUsuario($usuario, $password, $fecha, $correo, $rol);

        //return $result[0];

        //var_dump($result);

        //return $result['id'];

        //echo $result[0]->id;

        $msg = 'No se pudo registrar al usuario, porque ya existe en el base de datos!!!';

        if ($result[0]->id != 0) {

          //echo $result[0]->id;

          Usuario_persona::setUsuarioPersona($result[0]->id, $id);

          User::setRolUsuario($result[0]->id, $rol);

          $correo=strval($persona->coreo_persona);

            
            $user1 = Usuarios::join('USUARIO_PERSONA', 'USUARIO_PERSONA.user_id', '=', 'users.id')
          ->join('PERSONAS as p', 'USUARIO_PERSONA.id_persona', '=', 'p.id_persona')
        
          ->select('users.name as nombre', 'users.email as correo', 'p.nombres_persona', 'p.apellidos_persona', 'p.correo_persona','users.id as id')

          ->orderBy('users.id', 'desc')->first();
            /*
           $user1=DB::table('users as u')->join('USUARIO_PERSONA AS up', 'up.user_id', '=', 'u.id')
          ->join('PERSONAS as p', 'up.id_persona', '=', 'p.id_persona')
        
          ->select('u.name as nombre', 'u.email as correo', 'p.nombres_persona', 'p.apellidos_persona', 'p.correo_persona', 'u.id')
          ->orderBy('u.id', 'desc')->first();
          */

          //return view('emails.administrador', ['user'=>$user1]);


            Mail::send('emails.administrador', ['user' => $user1], function ($m) use ($user1) {
            $m->to($user1->correo_persona,  $user1->nombres_persona)->subject($user1->correo)->setBody('<h1><strong>BIENVENIDO A EDUCATION '. $user1->nombres_persona.' </h1></strong>  <h4> Su Usuario: '.$user1->correo.'</h4>  <h4> Su Contraseña:<strong> administrador123 </strong></h4><br/> <h5><center> para iniciar sesion ingresa <a href="http://education.wbinnovacionreal.com/"> <STRONG> AQUI </STRONG></a></center></h5><center> <h5>Powered by <a href="http://webusiness.co/"> Web Business S.A. </a>   <a href="tel:+5022327806">  <i class="fa fa-phone"></i>Tel. +50223278063</a> <i class="fa fa-at"></i>correo: education@webusiness.co <br/><br/></center>');
        });

          $msg = 'Se a registrado al nuevo usuario exitosamente!!!';

        }

        $html = '<strong>usuario: </strong>'.$usuario.'<br />';

        $html .= '<strong>contraseña:</strong> '.$pass.'<br />';

        $html .= '<strong>Correo Institucional:</strong> '.$correo;





        //PDF::SetTitle('Registro Usuario');

        //PDF::AddPage();

        //PDF::Write(0, $html);

        // output the HTML content

        //PDF::writeHTML($html, true, false, true, false, '');

        //PDF::Output('registro.pdf', 'D');

        DB::commit();

        

      } catch (Exception $e) {

        DB::rollBack();

      }

      Session::flash('mensaje', $msg);

      return redirect('/usuarios');
      }//FIN PUESTO DIRECTOR

      else if($rol==2)//si el puesto es DOCENTE
      {
        $aux = $persona->fecha_nacimiento_persona;

      $fecha = '';

      for ($i=0; $i < strlen($aux); $i++) {//quitamos los guiones de la fecha de nacimiento

        if ($aux[$i] != '-') {

          $fecha .= $aux[$i];

        }

      }



      list($pri_apellido) = explode(" ",$persona->apellidos_persona);

      list($pri_nombre) = explode(" ",$persona->nombres_persona);

      $usuario = mb_strtolower($pri_nombre.' '.$pri_apellido);

      //$pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);

      $pass = 'docente123';

      $correo = mb_strtolower($persona->nombres_persona[0].$pri_apellido.$fecha);



       $password = Hash::make($pass);//password cifrado

      DB::beginTransaction();

      try {

        $result = User::setUsuario($usuario, $password, $fecha, $correo, $rol);

        //return $result[0];

        //var_dump($result);

        //return $result['id'];

        //echo $result[0]->id;

        $msg = 'No se pudo registrar al usuario, porque ya existe en el base de datos!!!';

        if ($result[0]->id != 0) {

          //echo $result[0]->id;

          Usuario_persona::setUsuarioPersona($result[0]->id, $id);

          User::setRolUsuario($result[0]->id, $rol);

          $correo=strval($persona->coreo_persona);

          $user2 = Usuarios::join('USUARIO_PERSONA', 'USUARIO_PERSONA.user_id', '=', 'users.id')
          ->join('PERSONAS as p', 'USUARIO_PERSONA.id_persona', '=', 'p.id_persona')
        
          ->select('users.name as nombre', 'users.email as correo', 'p.nombres_persona', 'p.apellidos_persona', 'p.correo_persona','users.id as id')

          ->orderBy('users.id', 'desc')->first();

            $this->datosusuarios();

           Mail::send('emails.docente', ['user' => $user2], function ($m) use ($user2) {
                $m->to($user2->correo_persona,  $user2->nombres_persona)->subject($user2->correo)->setBody('<h1><strong>BIENVENIDO A EDUCATION '. $user1->nombres_persona.' </h1></strong>  <h4> Su Usuario: '.$user1->correo.'</h4>  <h4> Su Contraseña:<strong> docente123 </strong></h4><br/> <h5><center> para iniciar sesion ingresa <a href="http://education.wbinnovacionreal.com/"> <STRONG> AQUI </STRONG></a></center></h5><center> <h5>Powered by <a href="http://webusiness.co/"> Web Business S.A. </a>   <a href="tel:+5022327806">  <i class="fa fa-phone"></i>Tel. +50223278063</a> <i class="fa fa-at"></i>correo: education@webusiness.co <br/><br/></center>');
        });

          $msg = 'Se a registrado al nuevo usuario exitosamente!!!';

        }

        $html = '<strong>usuario: </strong>'.$usuario.'<br />';

        $html .= '<strong>contraseña:</strong> '.$pass.'<br />';

        $html .= '<strong>Correo Institucional:</strong> '.$correo;





        //PDF::SetTitle('Registro Usuario');

        //PDF::AddPage();

        //PDF::Write(0, $html);

        // output the HTML content

        //PDF::writeHTML($html, true, false, true, false, '');

        //PDF::Output('registro.pdf', 'D');

        DB::commit();

        

      } catch (Exception $e) {

        DB::rollBack();

      }

      Session::flash('mensaje', $msg);

      return redirect('/usuarios');

      }// fin docente


      else //caso contrario
      {
        $aux = $persona->fecha_nacimiento_persona;

      $fecha = '';

      for ($i=0; $i < strlen($aux); $i++) {//quitamos los guiones de la fecha de nacimiento

        if ($aux[$i] != '-') {

          $fecha .= $aux[$i];

        }

      }



      list($pri_apellido) = explode(" ",$persona->apellidos_persona);

      list($pri_nombre) = explode(" ",$persona->nombres_persona);

      $usuario = mb_strtolower($pri_nombre.' '.$pri_apellido);

      //$pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);

      $pass = 'usuario123';

      $correo = mb_strtolower($persona->nombres_persona[0].$pri_apellido.$fecha);



       $password = Hash::make($pass);//password cifrado

      DB::beginTransaction();

      try {

        $result = User::setUsuario($usuario, $password, $fecha, $correo, $rol);

        //return $result[0];

        //var_dump($result);

        //return $result['id'];

        //echo $result[0]->id;

        $msg = 'No se pudo registrar al usuario, porque ya existe en el base de datos!!!';

        if ($result[0]->id != 0) {

          //echo $result[0]->id;

          Usuario_persona::setUsuarioPersona($result[0]->id, $id);

          User::setRolUsuario($result[0]->id, $rol);

          $correo=strval($persona->coreo_persona);

          $user3 = Usuarios::join('USUARIO_PERSONA', 'USUARIO_PERSONA.user_id', '=', 'users.id')
          ->join('PERSONAS as p', 'USUARIO_PERSONA.id_persona', '=', 'p.id_persona')
        
          ->select('users.name as nombre', 'users.email as correo', 'p.nombres_persona', 'p.apellidos_persona', 'p.correo_persona','users.id as id')

          ->orderBy('users.id', 'desc')->first();

            $this->datosusuarios();

            Mail::send('emails.usuario', ['user' => $user3], function ($m) use ($user3) {
                $m->to($user3->correo_persona,  $user3->nombres_persona)->subject($user3->correo)->setBody('<h1><strong>BIENVENIDO A EDUCATION '. $user3->nombres_persona.' </h1></strong>  <h4> Su Usuario: '.$user3->correo.'</h4>  <h4> Su Contraseña:<strong> usuario123 </strong></h4><br/> <h5><center> para iniciar sesion ingresa <a href="http://education.wbinnovacionreal.com/"> <STRONG> AQUI </STRONG></a></center></h5><center> <h5>Powered by <a href="http://webusiness.co/"> Web Business S.A. </a>   <a href="tel:+5022327806">  <i class="fa fa-phone"></i>Tel. +50223278063</a> <i class="fa fa-at"></i>correo: education@webusiness.co <br/><br/></center>');
        });
          $msg = 'Se a registrado al nuevo usuario exitosamente!!!';

        }

        $html = '<strong>usuario: </strong>'.$usuario.'<br />';

        $html .= '<strong>contraseña:</strong> '.$pass.'<br />';

        $html .= '<strong>Correo Institucional:</strong> '.$correo;





        //PDF::SetTitle('Registro Usuario');

        //PDF::AddPage();

        //PDF::Write(0, $html);

        // output the HTML content

        //PDF::writeHTML($html, true, false, true, false, '');

        //PDF::Output('registro.pdf', 'D');

        DB::commit();

        

      } catch (Exception $e) {

        DB::rollBack();

      }

      Session::flash('mensaje', $msg);

      return redirect('/usuarios');

      }

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        $user = User::where('id', Auth::user()->id)->first();

        if ($user->can('editar-usuario')) 
        {

            $roles = Role::All();
            $usuario = User::findUser($id);

          foreach ($roles as $key => $row) {

            $r[$row->id] = mb_strtoupper($row->name);

          }

          return view('usuarios.eusuario', ['roles'=>$r, 'usuario'=>$usuario, 'items'=>$this->menu]);

        }

        return abort(403);


       


    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        
        $this->usuarios->id = $id;//asignar el id de la tarea actual
        $id = $this->usuarios->getUsuariosByid();//
       
        $this->usuarios->rol = $request['id_rol'];

        $this->usuarios->updateRoleUsers();
        Session::flash('mensaje', 'Se ha cambiado el tipo de rol para este usuario!!!');
        return redirect('/usuarios');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy(Request $request, $id)
    {
        $estado = $request['estado'];
        if ($estado == 1) {
          Usuarios::stateUser($id, FALSE);
        } else {
          Usuarios::stateUser($id, TRUE);
        }
        $usuarios =DB::table('users as u')->join('role_user AS ru', 'u.id', '=', 'ru.user_id')
          ->join('roles as r', 'ru.role_id', '=', 'r.id')
          ->select('u.name as no
            mbre', 'u.email as correo', 'r.name as rol', 'u.id as id', 'u.estado_usuario')
          ->orderBy('u.id', 'asc')
          ->paginate(10);

        //return response()->json($usuarios);

          return response()->json('Listo!!');
    }

     public function autocompleteusuario(Request $request)
    {
      $q = $request['query'];
      $d = array();
      $datos = User::getUsuario($q);
      foreach ($datos as $key => $row) {
        $aux = array('value'=>ucwords($row->name.' '.$row->email), 'data'=>$row->id);
        array_push($d, $aux);
      }
      return response()->json(array(
                            "query"=>"Unit",
                            "suggestions"=>$d));//devolviendo el json para la respuesta
    }


       public function autocomplete2(Request $request)
    {
    $q = $request['query'];
     $d = array();
    $datos = Personas::getPersonasLike($q);
      foreach ($datos as $key => $row) {
        $aux = array('value'=>ucwords($row->nombres_persona.' '.$row->apellidos_persona.'- - ->  PUESTO:'.$row->nombre_puesto),'data'=>$row->id_persona);
        array_push($d, $aux);
      }
      return response()->json(array(
                            "query"=>"Unit",
                            "suggestions"=>$d));//devolviendo el json para la respuesta
    }



  public function datosusuarios() 

     {



        

       

        $data = $this->getData();

        $date = date('d-m-Y');

        

        $view =  \View::make('emails.welcome', compact('data',  'date'))->render();

        $pdf = \App::make('dompdf.wrapper');

        $pdf->loadHTML($view);

        return ('emails.welcome');

    }


    public function getData() 

    {


        $userdedata = Usuarios::orderBy('users.id', 'DESC')->first();//datos de estudiante

          $data =[
          'name'=>$userdedata->name,
          'email'=>$userdedata->email,
        ];

        return $data;

        }


}

