<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;

use education\Http\Controllers\menuController;

use education\Http\Requests\helpRequest;

use education\MensajeCorreo;

use education\User;

use Session;

use Auth;

use DB;

use Mail;

use Hash;

use Illuminate\Support\Facades\Route;

use Redirect;


class helpdeskController extends Controller
{
	protected $menu;

    public function __construct()



  {

    $m = new menuController();

    $this->menu = $m->index();

  }

   public function index(Request $request)

    {

    	/* QUERY PARA BUSCAR TODOS LOS MENSAJES DE SOPORTE TECNICO*/
  		/*
  		SELECT mc.id_mensaje, mc.asunto, mc.mensaje, em.estado, tm.tipo_mensaje, u.name, mc.crated_at 
  		FROM mensajes_correo as mc join estado_mesaje as em on mc.estado=em.id_estado_mensaje join tipo_mensaje as tm on mc.tipo_mensaje=tm.id_tipo_mensaje join users as u on mc.usuario_receptor=u.id where usuario_emisor=23 and tm.id_tipo_mensaje=2
		*/
        $id = Auth::user()->id;

        $mensajes=DB::table('mensajes_correo as mc')
        	 ->join('estado_mesaje as em o','mc.estado','=','em.id_estado_mensaje')
        	 ->join('tipo_mensaje as tm','mc.tipo_mensaje','=','tm.id_tipo_mensaje')
        	 ->join('users as u','mc.usuario_receptor','=','u.id')
             ->where('mc.usuario_emisor','=',$id)
             ->where('tm.id_tipo_mensaje','=',2)
             ->select('mc.id_mensaje', 'mc.asunto', 'mc.mensaje', 'em.estado', 'tm.tipo_mensaje', 'u.name', 'mc.crated_at')
              ->orderby('mc.id_mensaje','DESC')
             ->paginate(10);//traigo mensajes de usuarios
       

        return view('helpdesk.index', ['items'=>$this->menu, 'mensajes'=>$mensajes]);

    }

    public function create(Request $request)

    {

        return view('helpdesk.nhelp', ['items'=>$this->menu]);

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(helpRequest $request)

    {

      try {

      	$usuario_emisor = Auth::user()->id;

      	$tipo_mensaje = 2;

      	$usuario_receptor = 433;//usuario de soporte tecnico stecnico20170404@webusiness.co education@webusiness.co

      	$estado=1;

        $asunto = $request['asunto'];

        $descripcion = $request['descripcionhelp'];



        DB::table('mensajes_correo')->insert([
                                  'usuario_emisor'=>Auth::user()->id,
                                  'tipo_mensaje'=>$tipo_mensaje,
                                  'usuario_receptor'=>$usuario_receptor,
                                  'estado'=>$estado,
                                  'asunto'=>$request['asunto'],
                                  'mensaje'=>$request['descripcionhelp'],

                                ]);
        /*QUERY PARA MOSTRAR EL ULTIMO MENSAJE ENVIADO
       SELECT mc.id_mensaje, mc.asunto, mc.mensaje, em.estado, tm.tipo_mensaje, u.name, mc.crated_at, p.correo_persona, ps.correo_persona, ps.telefono_persona, us.name as de FROM mensajes_correo as mc join estado_mesaje as em on mc.estado=em.id_estado_mensaje join tipo_mensaje as tm on mc.tipo_mensaje=tm.id_tipo_mensaje join users as u on mc.usuario_receptor=u.id
join USUARIO_PERSONA AS up on u.id=up.user_id 
join PERSONAS as p on up.id_persona=p.id_persona
join users as us on mc.usuario_emisor=us.id
join USUARIO_PERSONA AS ups on us.id=ups.user_id 
join PERSONAS as ps on ups.id_persona=ps.id_persona
where usuario_emisor=23 and tm.id_tipo_mensaje=2 order by mc.id_mensaje desc limit 1
        */

         $help = DB::table('mensajes_correo as mc')
        	 ->join('estado_mesaje as em o','mc.estado','=','em.id_estado_mensaje')
        	 ->join('tipo_mensaje as tm','mc.tipo_mensaje','=','tm.id_tipo_mensaje')
        	 ->join('users as u','mc.usuario_receptor','=','u.id')
        	 ->join('USUARIO_PERSONA AS up','u.id','=','up.user_id')
        	 ->join('PERSONAS as p','up.id_persona','=','p.id_persona')
        	 ->join('users as us','mc.usuario_emisor','=','us.id')
        	 ->join('USUARIO_PERSONA AS ups','us.id','=','ups.user_id')
        	 ->join('PERSONAS as ps','ups.id_persona','=','ps.id_persona')
             ->where('mc.usuario_emisor','=',Auth::user()->id)
             ->where('tm.id_tipo_mensaje','=',2)
             ->select('p.correo_persona as to', 'mc.id_mensaje', 'mc.asunto', 'mc.mensaje', 'em.estado', 'tm.tipo_mensaje', 'u.name', 'mc.crated_at','ps.correo_persona as from','ps.telefono_persona','us.name')
             ->orderby('mc.id_mensaje','DESC')
             ->first();//traigo correo del destinatario del ultimo mensaje enviado
         
         Mail::send('emails.administrador', ['user' => $help], function ($m) use ($help) {
            $m->to($help->to)->subject($help->from)->setBody('<h1><strong>SOPORTE TECNICO </h1></strong>  <h2> ASUNTO:'.$help->asunto .'  DESCRIPCION:' .$help->mensaje.' <br/>Correo personal: '.$help->from.'  telefono: <a href="tel:+'.$help->telefono_persona.'"> '.$help->telefono_persona.'</a></h2>  <h5><center> para iniciar sesion ingresa <a href="http://education.wbinnovacionreal.com/"> <STRONG> AQUI </STRONG></a></center></h5><center> <h5>Powered by <a href="http://webusiness.co/"> Web Business S.A. </a>   <a href="tel:+5022327806">  <i class="fa fa-phone"></i>Tel. +50223278063</a> <i class="fa fa-at"></i>correo: education@webusiness.co <br/><br/></center>');
        });

          $msg = 'Se a registrado al nuevo usuario exitosamente!!!';


      } catch (Exception $e) {

          return abort(500);

      }

        Session::flash('mensaje', 'La ayuda ha sido solicitada exitosamente!!!');

        return redirect('/helpdesk');

    }



}
