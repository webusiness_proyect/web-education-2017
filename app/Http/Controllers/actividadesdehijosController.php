<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;


use Illuminate\Pagination\LengthAwarePaginator;

use Illuminate\Support\Collection;


use education\Http\Controllers\menuController;

use education\Repositories\docentesRepository;

use education\Repositories\estudiantesRepository;

use education\Repositories\padresRepository;

use education\Repositories\unidadesRepository;

use Auth;

use Session;

use DB;

class actividadesdehijosController extends Controller
{
    protected $menu;

   protected $padres;

   protected $unidades;

   protected $estudiantes;



  public function __construct(estudiantesRepository $estudiante, padresRepository $padre, unidadesRepository $unidad)

  {

    $m = new menuController();

    $this->menu = $m->index();

    $this->padres = $padre;

    $this->unidades = $unidad;

    $this->estudiantes = $estudiante;

  }


    public function index(Request $request)

    {




        $this->padres->id_usuario = Auth::user()->id;//obtenemos el id de la persona logueada

        $alumnos = $this->padres->getAlumnos();//obtenemos los cursos asignados al docente

        //$cursos = $this->paginate($aux, 10);//paginar el resultado


           

        return view('misactividadesalumnos.index', ['items'=>$this->menu, 'alumnos'=>$alumnos]);

      

    }

    public function show(Request $request)
    {


        $this->unidades->fecha_inicio = date('Y-m-d');//fecha inicial de la unidad

        $this->unidades->fecha_final = date('Y-m-d');//fecha final de la unidad

        $unidad = $this->unidades->getUnidadActual();//obtenemos la unidad disponible 

        $this->estudiantes->id_usuario = $request['a'];

        

           $cursos=DB::table('ESTUDIANTES')
          ->join('NIVELES_GRADOS as ng', 'ESTUDIANTES.id_nivel_grado', '=', 'ng.id_nivel_grado')
          ->join('ASIGNACION_AREAS as aa', 'ng.id_nivel_grado', '=', 'aa.id_nivel_grado')
          ->join('AREAS as a', 'aa.id_area', '=', 'a.id_area')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'npj.id_nivel_plan_jornada', '=', 'ng.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('GRADOS as g', 'g.id_grado', '=', 'ng.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as p', 'npj.id_plan', '=', 'p.id_plan')
          ->join('SALONES as sal', 'aa.id_salon', '=', 'sal.id_salon')

          ->select('ESTUDIANTES.id','aa.id_asignacion_area', 'a.nombre_area', 'a.id_area','g.nombre_grado','n.nombre_nivel','ca.nombre_carrera','j.nombre_jornada','p.nombre_plan', 'sal.nombre_salon')

          ->where('ESTUDIANTES.id','=', $request['a'])
          ->where('estado_estudiante','=','1')
          ->paginate(10);






        return view('misactividadesalumnos.alumnos', ['items'=>$this->menu, 'unidad'=>$unidad,  'cursos'=>$cursos]);


    }
}
