<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;



use education\Http\Controllers\menuController;


use education\Repositories\unidadesRepository;


use education\Repositories\estudiantesRepository;


use education\Repositories\userRepository;


use education\User;


use Auth;

use DB;


class misforosController extends Controller
{
   
  protected $menu;


  protected $unidades;


  protected $estudiantes;


  protected $usuarios;


  public function __construct(menuController $m, unidadesRepository $unidad, estudiantesRepository $estudiante, userRepository $usuario)


  {


    $this->menu = $m->index();


    $this->unidades = $unidad;


    $this->estudiantes = $estudiante;


    $this->usuarios=$usuario;


  }


    /**


     * Display a listing of the resource.


     *


     * @return \Illuminate\Http\Response


     */


    public function index(Request $request)


    {





        $this->unidades->fecha_inicio = date('Y-m-d');//fecha inicial de la unidad


        $this->unidades->fecha_final = date('Y-m-d');//fecha final de la unidad


        $unidad = $this->unidades->getUnidadActual();//obtenemos la unidad disponible de acuerdo a la fecha


        


        $this->estudiantes->id_usuario = Auth::user()->id;


        


        


        //$cursos = $this->estudiantes->getCursosEstudiante();

        /*
        return Estudiantes::join('NIVELES_GRADOS as ng', 'ESTUDIANTES.id_nivel_grado', '=', 'ng.id_nivel_grado')

                    ->join('ASIGNACION_AREAS as aa', 'ng.id_nivel_grado', '=', 'aa.id_nivel_grado')

                    ->join('AREAS as a', 'aa.id_area', '=', 'a.id_area')

                    ->where('ESTUDIANTES.id', $this->id_usuario)

                    ->select('ESTUDIANTES.id','aa.id_asignacion_area', 'a.nombre_area', 'a.id_area')

                    ->get();
                    */
         $query=trim($request->get('searchText'));

           $cursos=DB::table('ESTUDIANTES')
          ->join('NIVELES_GRADOS as ng', 'ESTUDIANTES.id_nivel_grado', '=', 'ng.id_nivel_grado')
          ->join('ASIGNACION_AREAS as aa', 'ng.id_nivel_grado', '=', 'aa.id_nivel_grado')
          ->join('AREAS as a', 'aa.id_area', '=', 'a.id_area')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'npj.id_nivel_plan_jornada', '=', 'ng.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('GRADOS as g', 'g.id_grado', '=', 'ng.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as p', 'npj.id_plan', '=', 'p.id_plan')
          ->join('SALONES as sal', 'aa.id_salon', '=', 'sal.id_salon')

          ->select('ESTUDIANTES.id','aa.id_asignacion_area', 'a.nombre_area', 'a.id_area','g.nombre_grado','n.nombre_nivel','ca.nombre_carrera','j.nombre_jornada','p.nombre_plan', 'sal.nombre_salon')

          
          ->where('a.nombre_area','LIKE', '%'.$query.'%')
          ->where('ESTUDIANTES.id','=', Auth::user()->id)
          ->where('estado_estudiante','=','1')
          ->paginate(10);







        return view('misforos.index', ['items'=>$this->menu, 'unidad'=>$unidad,  'cursos'=>$cursos,  'searchText'=>$query]);


    }


}
