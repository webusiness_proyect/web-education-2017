<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Http\Controllers\menuController;
use education\Personas;
use Redirect;
use education\Usuarios;
use education\Puestos;
use \Validator;
use Session;
use education\User;
use Auth;
use DB;
use Illuminate\Support\Facades\Route;
use Hash;
use education\Role;
use Mail;
use education\Roles;
use education\Repositories\userRepository;
use education\Usuario_persona;
use education\Estudiantes;




class personasController extends Controller
{
    protected $menu;
    public function __construct()
    {
      $m = new menuController();
      $this->menu = $m->index();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can(['crear-empleado', 'editar-empleado', 'estado-empleado', 'ver-empleado'])) {
          //$personas = Personas::paginate(5);;



            $query=trim($request->get('searchText'));

          $personas=DB::table('PERSONAS as p')->join('PUESTOS as pu', 'p.id_puesto', '=', 'pu.id_puesto')
          
        
          ->select('p.nombres_persona', 'p.apellidos_persona', 'p.cui_persona', 'p.direccion_persona','p.fecha_nacimiento_persona','p.telefono_persona','p.correo_persona','pu.nombre_puesto','p.id_persona','p.estado_persona')

          /*PERSONAS::join('PUESTOS as p', 'PERSONAS.id_puesto', '=', 'p.id_puesto')*/


          ->where('p.nombres_persona','LIKE', '%'.$query.'%')
          ->orWhere('p.apellidos_persona','LIKE', '%'.$query.'%')
          ->orWhere('p.cui_persona','LIKE', '%'.$query.'%')
          ->orWhere('p.direccion_persona','LIKE', '%'.$query.'%')
          ->orWhere('p.fecha_nacimiento_persona','LIKE', '%'.$query.'%')
          ->orWhere('p.telefono_persona','LIKE', '%'.$query.'%')
          ->orWhere('p.correo_persona','LIKE', '%'.$query.'%')
          ->orWhere('pu.nombre_puesto','LIKE', '%'.$query.'%')
          ->where('p.estado_persona','=','1')
          ->orderBy('p.id_persona', 'asc')
          ->paginate(10);

           /*$personas = DB::table('PERSONAS as p')
                 ->join('puestos as pu', 'p.id_puesto', '=', 'pu.id_puesto')

            ->get();*/

          return view('personas.index', ['personas'=>$personas, 'items'=>$this->menu, 'searchText'=>$query]);

          //$personas_roles=Personas::join('PUESTOS as p', 'PERSONAS.id_puesto', '=', 'p.id_puesto');
          //return view('personas.index', ['personas_roles'=>$personas_roles, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('crear-empleado')) {
          $puestos = Puestos::All();
          foreach ($puestos as $key => $row) {
            $p[$row->id_puesto] = mb_strtoupper($row->nombre_puesto);
          }
          return view('personas.npersona', ['puestos'=>$p, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'required' => 'Hey! EL campo :attribute es requerido!!!.',
          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
          'date_format' => 'Hey! El valor de campo :attribute tiene que ser una fecha valida con el formato año-mes-día',
          );

          $v = Validator::make(
          $request->all(),
           [
                'nombres_persona' => 'required|min:2|max:50',
                'apellidos_persona' => 'required|min:5|max:50',
                'cui_persona' => 'required|min:13|max:13|unique:PERSONAS,cui_persona',
                'direccion_persona' => 'required|min:5|max:60',
                'fecha_nacimiento_persona' => 'required|min:10|max:10|date_format:"Y-m-d"',
                'telefono_persona' => 'required|min:8|max:10',
                'telefono_auxilia_persona' => 'min:8|max:10',
                'id_puesto' => 'required',
                'correo_persona'=>'required'
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
        $result = Personas::setPersona(
        $request['nombres_persona'], $request['apellidos_persona'], $request['cui_persona'],
        $request['direccion_persona'], $request['fecha_nacimiento_persona'], $request['telefono_persona'],
        $request['telefono_auxilia_persona'], $request['id_puesto'], $request['correo_persona']
        );
        /* CREAR USUARIO PARA DOCENTE */
        $dominio='@webusiness.co';
        if( $request['id_puesto']==2)
        {
          $aux =$request['fecha_nacimiento_persona'];
          $fecha = '';
          for ($i=0; $i < strlen($aux); $i++) 
          {//quitamos los guiones de la fecha de nacimiento
            if ($aux[$i] != '-') 
            {
              $fecha .= $aux[$i];
            }
          }

          list($pri_apellido) = explode(" ",$request['apellidos_persona']);
          list($pri_nombre) = explode(" ",$request['nombres_persona']);
          $usuario = mb_strtolower($pri_nombre.' '.$pri_apellido);
          $pass = 'docente123';

          $correo = mb_strtolower($pri_nombre.$pri_apellido.$fecha).$dominio;
          $password = Hash::make($pass);//password cifrado

          DB::beginTransaction();//INICIO DE TRANSACCION
            try {
              $rol = 2;
              $result2 = User::setUsuario($usuario, $password, $fecha, $correo, $rol);
              $msg = 'No se pudo registrar al usuario, porque ya existe en el base de datos!!!';
              if ($result2[0]->id != 0)//si los resultados del array en la pocicion 0 
                {
                  $persona = Personas::findUltimaPersona();
                  Usuario_persona::setUsuarioPersona($result2[0]->id, $persona->id_persona);
                  User::setRolUsuario($result2[0]->id, $rol);
                  $correo=strval($persona->coreo_persona);
                  $user2 = Usuarios::join('USUARIO_PERSONA', 'USUARIO_PERSONA.user_id', '=', 'users.id')
                    ->join('PERSONAS as p', 'USUARIO_PERSONA.id_persona', '=', 'p.id_persona')
                    ->select('users.name as nombre', 'users.email as correo', 'p.nombres_persona', 'p.apellidos_persona', 'p.correo_persona','users.id as id')
                    ->orderBy('users.id', 'desc')->first();

                 

                  Mail::send('emails.docente', ['user' => $user2], function ($m) use ($user2) 
                  {
                  $m->to($user2->correo_persona, $user2->nombres_persona)->subject($user2->correo);
                  });

                    $msg = 'Se a registrado al nuevo usuario exitosamente!!!';
                }

                $html = '<strong>usuario: </strong>'.$usuario.'<br />';
                $html .= '<strong>contraseña:</strong> '.$pass.'<br />';
                $html .= '<strong>Correo Institucional:</strong> '.$correo;
                DB::commit();    
                } 
                catch (Exception $e) 
                {
                  DB::rollBack();
                }

        }

        
      
        

        Session::flash('mensaje', $result[0]->msg);
        return redirect('/personas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('ver-empleado')) {
          $persona = Personas::findPersona($id);
          return view('personas.mpersona', ['persona'=>$persona, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('editar-empleado')) {
          $persona = Personas::findPersona($id);
          $puestos = Puestos::All();
          foreach ($puestos as $key => $row) {
            $p[$row->id_puesto] = mb_strtoupper($row->nombre_puesto);
          }
          return view('personas.epersona', ['persona'=>$persona, 'puestos'=>$p, 'items'=>$this->menu]);
        }
        return abort(403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      //crear el arreglo de los mensajes de validacion
          $mensajes = array(
          'required' => 'Hey! EL campo :attribute es requerido!!!.',
          'min' => 'Hey! El campo :attribute debe tener como minimo :min caracteres!!!',
          'max' => 'Hey! El campo :attribute no puede tener mas de :max caracteres!!!',
          'unique' => 'Hey! El valor del campo :attribute ya existe en la base de datos, tiene que ser unico!!!',
          'date_format' => 'Hey! El valor de campo :attribute tiene que ser una fecha valida con el formato año-mes-día',
          );

          $v = Validator::make(
          $request->all(),
           [
                'nombres_persona' => 'required|min:5|max:50',
                'apellidos_persona' => 'required|min:5|max:50',
                'cui_persona' => 'required|min:13|max:13|unique:PERSONAS,cui_persona,'.$id.',id_persona',
                'direccion_persona' => 'required|min:5|max:60',
                'fecha_nacimiento_persona' => 'required|min:10|max:10|date_format:"Y-m-d"',
                'telefono_persona' => 'required|min:8|max:10',
                'telefono_auxilia_persona' => 'min:8|max:10',
                'id_puesto' => 'required',
                'correo_persona'=>'required'
            ],
            $mensajes);

        if ($v->fails())
        {
            return redirect()->back()->withInput()->withErrors($v->errors());
        }
        $result = Personas::updatePersona(
        $id,
        $request['nombres_persona'], $request['apellidos_persona'], $request['cui_persona'],
        $request['direccion_persona'], $request['fecha_nacimiento_persona'], $request['telefono_persona'],
        $request['telefono_auxilia_persona'], $request['id_puesto'], $request['correo_persona']
        );

        Session::flash('mensaje', $result[0]->msg);
        return redirect('/personas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $estado = $request['estado'];
        if ($estado == 1) {
          Personas::statePersona($id, FALSE);
        } else {
          Personas::statePersona($id, TRUE);
        }
        return response()->json('Listo!!!');
    }

    /*
      Metodo para buscar los nombres completos de las personas para el
      Autocompletado
    */
    public function bscautomplete(Request $request)
    {
    $q = $request['query'];
     $d = array();
    $datos = Personas::getPersonasLike($q);
      foreach ($datos as $key => $row) {
        $aux = array('value'=>ucwords($row->nombres_persona.' '.$row->apellidos_persona.'- - ->  PUESTO:'.$row->nombre_puesto),'data'=>$row->id_persona);
        array_push($d, $aux);
      }
      return response()->json(array(
                            "query"=>"Unit",
                            "suggestions"=>$d));//devolviendo el json para la respuesta
    }

    /*Busca a una persona por el parametro de busqueda y que tenga el puesto docente*/
    public function buscadocente(Request $request)
    {
      $q = $request['query'];
      $d = array();
      $datos = Personas::getPersonasLikeDocente($q);
      foreach ($datos as $key => $row) {
        $aux = array('value'=>ucwords($row->nombres_persona.' '.$row->apellidos_persona), 'data'=>$row->id_persona);
        array_push($d, $aux);
      }
      return response()->json(array(
                            "query"=>"Unit",
                            "suggestions"=>$d));//devolviendo el json para la respuesta
    }

     public function buscausuario(Request $request)
    {
      $q = $request['query'];
      $d = array();
      $datos = Personas::getBuscaUsuario($q);
      foreach ($datos as $key => $row) {
        $aux = array('value'=>ucwords($row->name), 'data'=>$row->name);
        array_push($d, $aux);
      }
      return response()->json(array(
                            "query"=>"Unit",
                            "suggestions"=>$d));//devolviendo el json para la respuesta
    }





}