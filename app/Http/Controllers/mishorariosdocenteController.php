<?php


namespace education\Http\Controllers;



use Illuminate\Http\Request;
use education\Http\Requests\horariosRequest ;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use education\Http\Requests;
use education\Http\Controllers\menuController;
use education\Asignacion_areas;
use education\Repositories\HorarioRepository;
use education\horario;
use education\Areas;
use \Validator;
use Session;
use education\User;
use Auth;
use DB;

class mishorariosdocenteController extends Controller
{
    
	protected $menu;

  protected $horarios;

  public function __construct(HorarioRepository $horario)

  {

    $m = new menuController();

    $this->menu = $m->index();

    $this->horarios = $horario;

  }

   public function index(Request $request)

    {

        

        //$items = $this->horarios->getAllCalendarios();

            $this->horarios->user = Auth::user()->id;
            $docente=$this->horarios->getDocentes();
            $id=$this->horarios->id_area=$docente['id_persona'];

            $query=trim($request->get('searchText'));
            $query2=trim($request->get('searchText2'));
            $query3=trim($request->get('searchText3'));

          $items=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
          ->join('ASIGNACION_AREAS AS AA', 'AA.id_area', '=', 'ar.id_area')
          ->join('ASIGNACION_DOCENTE AS AD', 'AD.id_asignacion_area', '=', 'AA.id_asignacion_area')
          ->join('PERSONAS AS P', 'AD.id_persona', '=', 'P.id_persona')
          ->join('users AS U', 'U.id', '=', 'P.id_persona')
      
        
          ->select(DB::raw('DISTINCT(ar.nombre_area)'), 'c.dia','c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

         
          ->where('c.estado_actividad_calendario','=','1')
          ->where('P.id_persona','=',$id)
          ->where('c.dia','=','1')
          ->orderBy('c.hora_inicia', 'asc')
         
          ->paginate(50);
          /*QUERY QUE MUESTRA EL HORARIO DE TODOS LOS CURSOS ASIGNADOS A MI COMO PROFESOR

          select distinct ar.nombre_area, C.hora_inicia, C.hora_fin 
          from CALENDARIO AS C JOIN DIAS as d on C.dia=d.id_dia
          join NIVELES_GRADOS as ng on ng.id_nivel_grado=C.id_nivel_grado
          join GRADOS as g on ng.id_grado=g.id_grado
          join CARRERAS as ca on ng.id_carrera=ca.id_carrera
          join NIVELES_PLANES_JORNADAS as npj on ng.id_nivel_plan_jornada=npj.id_nivel_plan_jornada
          join NIVELES as n on npj.id_nivel=n.id_nivel
          join JORNADAS as j on npj.id_jornada=j.id_jornada
          join PLANES as pl on npj.id_plan=pl.id_plan
          join AREAS as ar on ar.id_area=C.id_area
          JOIN ASIGNACION_AREAS AS AA ON AA.id_area=ar.id_area
          JOIN ASIGNACION_DOCENTE AS AD ON AD.id_asignacion_area=AA.id_asignacion_area
          JOIN PERSONAS AS P ON AD.id_persona=P.id_persona
          JOIN users AS U ON U.id=P.id_persona
          where P.id_persona=2
          and C.dia=1
          */

           $martes=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
          ->join('ASIGNACION_AREAS AS AA', 'AA.id_area', '=', 'ar.id_area')
          ->join('ASIGNACION_DOCENTE AS AD', 'AD.id_asignacion_area', '=', 'AA.id_asignacion_area')
          ->join('PERSONAS AS P', 'AD.id_persona', '=', 'P.id_persona')
          ->join('users AS U', 'U.id', '=', 'P.id_persona')
      
        
          ->select(DB::raw('DISTINCT(ar.nombre_area)'), 'c.dia','c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

         
          ->where('c.estado_actividad_calendario','=','1')
          ->where('P.id_persona','=',$id)
          ->where('c.dia','=','2')
          ->orderBy('c.hora_inicia', 'asc')
         
          ->paginate(50);

          //MIERCOLES

          $miercoles=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
          ->join('ASIGNACION_AREAS AS AA', 'AA.id_area', '=', 'ar.id_area')
          ->join('ASIGNACION_DOCENTE AS AD', 'AD.id_asignacion_area', '=', 'AA.id_asignacion_area')
          ->join('PERSONAS AS P', 'AD.id_persona', '=', 'P.id_persona')
          ->join('users AS U', 'U.id', '=', 'P.id_persona')
      
        
          ->select(DB::raw('DISTINCT(ar.nombre_area)'), 'c.dia','c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

         
          ->where('c.estado_actividad_calendario','=','1')
          ->where('P.id_persona','=',$id)
          
          ->where('c.dia','=','3')
          ->orderBy('c.hora_inicia', 'asc')
         
          ->paginate(50);

          //jueves

           $jueves=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
          ->join('ASIGNACION_AREAS AS AA', 'AA.id_area', '=', 'ar.id_area')
          ->join('ASIGNACION_DOCENTE AS AD', 'AD.id_asignacion_area', '=', 'AA.id_asignacion_area')
          ->join('PERSONAS AS P', 'AD.id_persona', '=', 'P.id_persona')
          ->join('users AS U', 'U.id', '=', 'P.id_persona')
      
        
          ->select(DB::raw('DISTINCT(ar.nombre_area)'), 'c.dia','c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.estado_actividad_calendario','=','1')
          ->where('P.id_persona','=',$id)
          
          ->where('c.dia','=','4')
          ->orderBy('c.hora_inicia', 'asc')
         
          ->paginate(50);

          //viernes

           $viernes=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
          ->join('ASIGNACION_AREAS AS AA', 'AA.id_area', '=', 'ar.id_area')
          ->join('ASIGNACION_DOCENTE AS AD', 'AD.id_asignacion_area', '=', 'AA.id_asignacion_area')
          ->join('PERSONAS AS P', 'AD.id_persona', '=', 'P.id_persona')
          ->join('users AS U', 'U.id', '=', 'P.id_persona')
      
          ->select(DB::raw('DISTINCT(ar.nombre_area)'), 'c.dia','c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

         
          ->where('c.estado_actividad_calendario','=','1')
          ->where('P.id_persona','=',$id)
          ->where('c.dia','=','5')
          ->orderBy('c.hora_inicia', 'asc')
         
          ->paginate(50);

          //sabado

           $sabado=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
          ->join('ASIGNACION_AREAS AS AA', 'AA.id_area', '=', 'ar.id_area')
          ->join('ASIGNACION_DOCENTE AS AD', 'AD.id_asignacion_area', '=', 'AA.id_asignacion_area')
          ->join('PERSONAS AS P', 'AD.id_persona', '=', 'P.id_persona')
          ->join('users AS U', 'U.id', '=', 'P.id_persona')
      
        
          ->select(DB::raw('DISTINCT(ar.nombre_area)'), 'c.dia','c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

         
          ->where('c.estado_actividad_calendario','=','1')
          ->where('P.id_persona','=',$id)
          ->where('c.dia','=','6')
          ->orderBy('c.hora_inicia', 'asc')
         
          ->paginate(50);
         
          

          //domingo

           $domingo=DB::table('CALENDARIO as c')->join('DIAS as d', 'c.dia', '=', 'd.id_dia')
          ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'c.id_nivel_grado')
          ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
          ->join('CARRERAS as ca', 'ng.id_carrera', '=', 'ca.id_carrera')
          ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
          ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
          ->join('PLANES as pl', 'npj.id_plan', '=', 'pl.id_plan')
          ->join('AREAS as ar', 'ar.id_area', '=', 'c.id_area')
          ->join('ASIGNACION_AREAS AS AA', 'AA.id_area', '=', 'ar.id_area')
          ->join('ASIGNACION_DOCENTE AS AD', 'AD.id_asignacion_area', '=', 'AA.id_asignacion_area')
          ->join('PERSONAS AS P', 'AD.id_persona', '=', 'P.id_persona')
          ->join('users AS U', 'U.id', '=', 'P.id_persona')
      
        
          ->select(DB::raw('DISTINCT(ar.nombre_area)'), 'c.dia','c.hora_inicia as inicio', 'c.hora_fin as fin', 'c.idcalendario', 'd.dia','c.estado_actividad_calendario as estado','g.nombre_grado','ca.nombre_carrera', 'j.nombre_jornada', 'pl.nombre_plan', 'n.nombre_nivel')

          ->where('c.estado_actividad_calendario','=','1')
          ->where('P.id_persona','=',$id)
          ->where('c.dia','=','7')
          ->orderBy('c.hora_inicia', 'asc')
         
          ->paginate(50);
          
          /*

				select
				     c.dia, c.hora_inicia, c.hora_fin,c.asignacion_area, aa.id_salon, aa.id_area, a.nombre_area, d.dia 
				   
				  from calendario as c
				  join asignacion_areas as aa on c.asignacion_area=aa.id_asignacion_area
				  join areas as a on aa.id_area=a.id_area
				  left join DIAS as d on c.dia=d.id_dia
				  
				 
				    
				    order by c.dia
    		*/


        $carreras =  DB::table('GRADOS as g')
          ->join('NIVELES_GRADOS as ng', 'g.id_grado', '=', 'ng.id_grado')
          ->join('CARRERAS as ca', 'ca.id_carrera', '=', 'ng.id_carrera') 
          ->join('NIVELES_PLANES_JORNADAS as npj', 'npj.id_nivel_plan_jornada', '=', 'ng.id_nivel_plan_jornada')
          ->join('NIVELES as n', 'n.id_nivel', '=', 'npj.id_nivel')
          ->join('JORNADAS as j', 'j.id_jornada', '=', 'npj.id_jornada')
          ->join('PLANES as pl', 'pl.id_plan', '=', 'npj.id_plan')   
         ->select(DB::raw('DISTINCT(g.nombre_grado)'), 'ca.nombre_carrera', 'ca.id_carrera', 'ng.id_nivel_grado', 'ca.nombre_corto_carrera','n.nombre_nivel','j.nombre_jornada','pl.nombre_plan')
          ->where('g.estado_grado','=','1')
          ->paginate(50);

          foreach ($carreras as $key => $row) {
            $tc[$row->id_nivel_grado] = mb_strtoupper($row->nombre_grado). '  '. mb_strtoupper($row->nombre_carrera). '   '. mb_strtoupper($row->nombre_nivel). ' - JORNADA: '. mb_strtoupper($row->nombre_jornada);
          }


        return view('mishorariosdocente.index', ['items'=>$this->menu, 'horarios'=>$items, 'martes'=>$martes, 'miercoles'=>$miercoles, 'jueves'=>$jueves, 'viernes'=>$viernes, 'sabado'=>$sabado, 'domingo'=>$domingo,  'searchText'=>$query, 'searchText2'=>$query2, 'searchText3'=>$query3,'carreras'=>$tc]);

    }

}
