<?php



namespace education\Http\Controllers;



use Illuminate\Http\Request;



use education\Http\Requests;

use education\Http\Requests\ActividadesRequest;

use education\Http\Controllers\menuController;

use education\Repositories\actividadesRepository;

use education\Repositories\unidadesRepository;

use education\Repositories\userRepository;

use education\Asignacion_areas;

use education\actividad;

use education\tipo_actividad;

use education\User;

use Session;

use Auth;

use DB;

//use Carbon\Carbon;



class ActividadesController extends Controller

{

  protected $menu;

  protected $actividades;

  protected $unidades;

  protected $users;

  public function __construct(actividadesRepository $actividad, unidadesRepository $unidad, userRepository $User)

  {

    $m = new menuController();

    $this->menu = $m->index();

    $this->actividades = $actividad;

    $this->unidades = $unidad;

    $this->users = $User;

  }

   public function index(Request $request)

    {

        $this->unidades->fecha_inicio = date('Y-m-d');//fecha inicial de la unidad
        $this->unidades->fecha_final = date('Y-m-d');//fecha final de la unidad
        $unidad = $this->unidades->getUnidadActual();//obtenemos la unidad disponible de acuerdo a la fecha
        $this->actividades->id_area = $request['a'];
        $this->actividades->id_unidad =$unidad['id_unidad'];
        $area = $this->actividades->getArea();//obtenemos la unidad disponible de acuerdo a la fecha
        $area=DB::table('AREAS')
             ->where('id_area','=',$request['a'])
             ->select('nombre_area')
             ->get();//traingo informacion de unidades
        $items = $this->actividades->getActividadesThisunidad();//traigo actividades de este curso
        $items2 = $this->actividades->getTotalActividadCurso();//traigo total actividades esta unidad
        $items3 = $this->actividades->getActividadCursoUnidadAnterior();//traigo cursos de unidades pasadas

        return view('Actividad.index', ['items'=>$this->menu, 'unidad'=>$unidad, 'area'=>$area, 'actividades'=>$items, 'total'=>$items2, 'anteriores'=>$items3, 'curso'=>$request['a'],'area'=>$area]);

    }



      public function create(Request $request)

    {

     

        $items = $this->actividades->getActividadCurso();
        $tipo_actividad = tipo_actividad::All();

          foreach ($tipo_actividad as $key => $row) {

            $ta[$row->id_tipo_actividad] = mb_strtoupper($row->nombre_actividad);

          }

        return view('Actividad.nactividad', ['tipo_actividad'=>$ta,'items'=>$this->menu, 'curso'=>$request['a']]);



            

    }





    

     public function store(ActividadesRequest $request)

    {

      try {

      	//$date = Carbon::now();



        $this->unidades->fecha_inicio = date('Y-m-d');
        $this->unidades->fecha_final = date('Y-m-d');
        $unidad = $this->unidades->getUnidadActual();
        $areas= Asignacion_areas::where('id_asignacion_area', $request['curso'])->first();

        $this->actividades->id_usuario = Auth::user()->id;

        $this->actividades->id_asignacion_area = $areas['id_nivel_grado'];

        $this->actividades->id_unidad = $unidad['id_unidad'];

        $this->actividades->id_area = $request['curso'];

        $this->actividades->id_tipo_actividad = $request['id_tipo_actividad'];

        $this->actividades->nombre_actividad = $request['nombre_actividad'];

        $this->actividades->descripcion_actividad = $request['descripcion_actividad'];

        $this->actividades->nota_total = $request['nota_total'];

        $this->actividades->fecha_entrega = $request['fecha_entrega'];

        

        $this->actividades->setActividad();

      } catch (Exception $e) {

          return abort(500);

      }

        Session::flash('mensaje', 'la actividad ha sido registrada exitosamente!!!');

        return redirect('/misactividadesdocente');

    }




    public function edit($id)

    {

       
      
        $this->actividades->id_actividad = $id;

          $dato = $this->actividades->getActividadById();

           $tipo_actividad = tipo_actividad::All();

          foreach ($tipo_actividad as $key => $row) {

            $ta[$row->id_tipo_actividad] = mb_strtoupper($row->nombre_tipo_actividad);

          }

             //$dato = horario::findGradoNivel($id);

        return view('Actividad.eactividad', ['items'=>$this->menu, 'tipo_actividad'=>$ta, 'dato'=>$dato]);

        

    }


     public function update(Request $request, $id)

    {

        ////crear el arreglo de los mensajes de validacion

          

     

       

        $this->actividades->id_actividad = $id;

        $this->actividades->nombre_actividad = $request['nombre_actividad'];

        $this->actividades->descripcion_actividad = $request['descripcion_actividad'];

        $this->actividades->nota_total = $request['nota_total'];

        $this->actividades->fecha_entrega = $request['fecha_entrega'];
        
        $this->actividades->updateActividad();

        Session::flash('mensaje', 'Se han actualizado los datos la tarea!!!');

        return redirect('/misactividadesdocente');

    }


    



}



