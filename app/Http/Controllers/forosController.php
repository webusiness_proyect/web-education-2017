<?php



namespace education\Http\Controllers;



use Illuminate\Http\Request;



use education\Http\Requests;

use education\Http\Requests\forosRequest;

use education\Http\Controllers\menuController;

use education\Repositories\forosRepository;

use education\Foros;

use education\User;

use Session;

use Auth;



class forosController extends Controller

{

  protected $menu;

  protected $foros;

  public function __construct(forosRepository $foro)

  {

    $m = new menuController();

    $this->menu = $m->index();

    $this->foros = $foro;

  }

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(Request $request)

    {

        $this->foros->id_asignacion_area = $request['a'];

        $items = $this->foros->getForosCurso();

        return view('foros.index', ['items'=>$this->menu, 'foros'=>$items, 'curso'=>$request['a']]);

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create(Request $request)

    {

        return view('foros.nforo', ['items'=>$this->menu, 'curso'=>$request['a']]);

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(forosRequest $request)

    {

      try {

        $this->foros->titulo = $request['titulo_foro'];

        $this->foros->mensaje = $request['mensaje_foro'];

        $this->foros->id_usuario = Auth::user()->id;

        $this->foros->id_asignacion_area = $request['curso'];

        $this->foros->setForo();

      } catch (Exception $e) {

          return abort(500);

      }

        Session::flash('mensaje', 'El foro ha sido registrado exitosamente!!!');

        return redirect('/foros');

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        //$this->authorize('owner', $foros);

        $this->foros->id_foro = $id;

        $foro = $this->foros->getForoById();

        return view('foros.eforo', ['items'=>$this->menu, 'foro'=>$foro]);

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(forosRequest $request, $id)

    {

        $this->foros->id_foro = $id;//asignar el id del foro actual

        $this->authorize('owner', $foros);//verificamos si esta autorizado para actulizar los datos de este foro



        $this->foros->titulo = $request['titulo_foro'];

        $this->foros->mensaje = $request['mensaje_foro'];

        $this->foros->updateForo();

        Session::flash('mensaje', 'Se han actualizado los datos del foro exitosamente!!!');

        return redirect('/foros');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        //

    }

}

