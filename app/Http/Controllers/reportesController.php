<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Repositories\estudiantesRepository;
use education\Repositories\referenciasRepository;
use education\Repositories\tutorestudianteRepository;
use PDF;
class reportesController extends Controller
{
    protected $estudiantes;
    protected $referencias;
    protected $tutores;
    public function __construct(estudiantesRepository $estudiante, referenciasRepository $referencia, tutorestudianteRepository $tutor)
    {
      $this->estudiantes = $estudiante;
      $this->referencias = $referencia;
      $this->tutores = $tutor;
    }

    public function constancia_inscripcion()
    {
      $this->estudiantes->id_estudiante = 6;
      $this->referencias->id_estudiante = 6;
      $this->tutores->id_estudiante = 6;
      $estudiante = $this->estudiantes->getEstudianteInscripcion();

        //Encabezado de la página
        PDF::setHeaderCallback(function($pdf){
          // Logo
          $image_file = public_path().'/img/'.'LOGO EDUCATIVO-19.jpg';
          $pdf->Image($image_file, 5, 0, 65, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
          // Set font
          $pdf->SetFont('helvetica', 'B', 20);
          // Title
          $pdf->Ln(10);
          $pdf->SetXY(70,15);
          $pdf->MultiCell(0, 5, 'FICHA DE INSCRIPCIÓN '.date('Y'), 0, 'L', 0, 0, '', '', true);
        });

        // set margins
      PDF::SetMargins(10, 30, 10);
      PDF::SetTitle('Constancia de Inscripción');
      PDF::AddPage('P',array(355.6, 215.9));
      // set color for background
      PDF::SetFillColor(133, 193, 233);
      // MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)
      PDF::MultiCell(45, 60, 'Información Personal del alumno', 1, 'C', 1, 1, '', '', true);
      //posicionar el cursos el las coordenadas indicadas
      PDF::SetXY(55,30);
      // set font
      PDF::SetFont('helvetica', '', 9);
      // set color for background
      PDF::SetFillColor(255, 255, 255);
      PDF::MultiCell(45, 5, 'Apellidos:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, ucwords($estudiante->apellidos_estudiante), 1, 'L', 1, 1, '', '', true);
      PDF::SetXY(55,35);
      PDF::MultiCell(45, 5, 'Nombres:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, ucwords($estudiante->nombre_estudiante), 1, 'L', 1, 1, '', '', true);
      PDF::SetXY(55,40);
      PDF::MultiCell(45, 5, 'Fecha de nacimiento:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, $estudiante->fecha_nacimiento_estudiante.'                    Edad: '.(date('Y-m-d') - $estudiante->fecha_nacimiento_estudiante).' Años', 1, 'L', 1, 1, '', '', true);
      //PDF::MultiCell(45, 5, 'Edad:', 1, 'R', 1, 0, '', '', true);
      //PDF::MultiCell(0, 5, (date('Y-m-d') - $estudiante->fecha_nacimiento_estudiante), 1, 'L', 1, 1, '', '', true);
      PDF::SetXY(55,45);
      PDF::MultiCell(45, 5, 'Carrera/ciclo:', 1, 'R', 1, 0, '', '', true);
      if ($estudiante->nombre_carrera == 'no aplica') {
        PDF::MultiCell(0, 5, ucwords($estudiante->nombre_nivel), 1, 'L', 1, 1, '', '', true);
      } else {
        PDF::MultiCell(0, 5, ucwords($estudiante->nombre_carrera), 1, 'L', 1, 1, '', '', true);
      }

      PDF::SetXY(55,50);
      PDF::MultiCell(45, 5, 'Grado:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, ucwords($estudiante->nombre_grado), 1, 'L', 1, 1, '', '', true);
      PDF::SetXY(55,55);
      PDF::MultiCell(45, 5, 'Dirección:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, $estudiante->direccion_estudiante, 1, 'L', 1, 1, '', '', true);
      PDF::SetXY(55,60);
      PDF::MultiCell(45, 5, 'Colonia:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, $estudiante->colonia_estudiante, 1, 'L', 1, 1, '', '', true);
      PDF::SetXY(55,65);
      PDF::MultiCell(45, 5, 'Zona:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, $estudiante->zona_estudiante, 1, 'L', 1, 1, '', '', true);
      PDF::SetXY(55,70);
      PDF::MultiCell(45, 5, 'Municipio:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, ucwords($estudiante->nombre_municipio), 1, 'L', 1, 1, '', '', true);
      PDF::SetXY(55,75);
      PDF::MultiCell(45, 5, 'Telefono de casa/Celular:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, $estudiante->telefono_casa.'   '.$estudiante->empresa_telefonica, 1, 'L', 1, 1, '', '', true);
      PDF::SetXY(55,80);
      PDF::MultiCell(45, 5, 'Correo electrónico:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, $estudiante->correo_estudiante, 1, 'L', 1, 1, '', '', true);
      PDF::SetXY(55,85);
      PDF::MultiCell(45, 5, 'Fecha de inscripción:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, $estudiante->fecha_registro_estudiante.'                    Ciclo Escolar: '.date('Y'), 1, 'L', 1, 1, '', '', true);

      //obtener las referencias del estudiante

      $referencia = $this->referencias->getReferenciaEstudiante();
      $numReferencias = count($referencia);
      PDF::SetFillColor(133, 193, 233);
      PDF::SetFont('helvetica', '', 12);
      PDF::MultiCell(45, (($numReferencias+1) * 5), 'Referencias', 1, 'C', 1, 1, '', '', true);
      //posicionar el cursos el las coordenadas indicadas
      PDF::SetXY(55,90);
      // set font
      PDF::SetFont('helvetica', '', 9);
      // set color for background
      PDF::SetFillColor(255, 255, 255);
      PDF::MultiCell(45, 5, 'Parentesco', 1, 'C', 1, 0, '', '', true);
      PDF::MultiCell(66, 5, 'Nombre', 1, 'C', 1, 0, '', '', true);
      PDF::MultiCell(40, 5, 'Teléfono', 1, 'C', 1, 1, '', '', true);
      $y = 90;
      foreach ($referencia as $key => $row) {
        $y = $y+5;
        //posicionar el cursos el las coordenadas indicadas
        PDF::SetXY(55,$y);
        PDF::MultiCell(45, 5, ucwords($row->parentesco_referencia), 1, 'C', 1, 0, '', '', true);
        PDF::MultiCell(66, 5, ucwords($row->nombre_referencia), 1, 'C', 1, 0, '', '', true);
        PDF::MultiCell(40, 5, $row->telefono_referencia, 1, 'C', 1, 1, '', '', true);
      }
      //PDF::MultiCell(0, 5, $estudiante->correo_estudiante, 1, 'L', 1, 1, '', '', true);
      // set color for background
      PDF::SetFillColor(133, 193, 233);
      //set font
      PDF::SetFont('helvetica', '', 12);
      $tutor = $this->tutores->getTutoresEstudiante();
      for ($i=0; $i < count($tutor); $i++) {
        //$total = count($tutor);
        PDF::MultiCell(45, 50, 'Información del encargado', 1, 'C', 1, 1, '', '', true);
      }
      // set font
      PDF::SetFont('helvetica', '', 9);
      // set color for background
      PDF::SetFillColor(255, 255, 255);
      //posicionar el cursos el las coordenadas indicadas
      $y = 100;
      foreach ($tutor as $key => $row) {
      $y = $y+5;
      PDF::SetXY(55,$y);
      PDF::MultiCell(45, 5, 'Apellidos:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, ucwords($row->apellidos_tutor), 1, 'L', 1, 0, '', '', true);
      $y = $y+5;
      PDF::SetXY(55,$y);
      PDF::MultiCell(45, 5, 'Nombres:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, ucwords($row->nombre_tutor), 1, 'L', 1, 0, '', '', true);
      $y = $y+5;
      PDF::SetXY(55,$y);
      PDF::MultiCell(45, 5, 'DPI:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, $row->cui_tutor.'                    Parentesco: '.ucwords($row->parentesco_tutor), 1, 'L', 1, 0, '', '', true);
      $y = $y+5;
      PDF::SetXY(55,$y);
      PDF::MultiCell(45, 5, 'Dirección:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, ucwords($row->direccion_tutor), 1, 'L', 1, 0, '', '', true);
      $y = $y+5;
      PDF::SetXY(55,$y);
      PDF::MultiCell(45, 5, 'Teléfono/Celular:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, $row->telefono_primario_tutor.'            '.$row->empresa_telefono_tutor, 1, 'L', 1, 0, '', '', true);
      $y = $y+5;
      PDF::SetXY(55,$y);
      PDF::MultiCell(45, 5, 'Correo electrónico:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, $row->correo_electronico_tutor, 1, 'L', 1, 0, '', '', true);
      $y = $y+5;
      PDF::SetXY(55,$y);
      PDF::MultiCell(45, 5, 'Lugar de Trabajo:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, ucwords($row->lugar_trabajo_tutor), 1, 'L', 1, 0, '', '', true);
      $y = $y+5;
      PDF::SetXY(55,$y);
      PDF::MultiCell(45, 5, 'Dirección de Trabajo', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, ucwords($row->direccion_trabajo_tutor), 1, 'L', 1, 0, '', '', true);
      $y = $y+5;
      PDF::SetXY(55,$y);
      PDF::MultiCell(45, 5, 'Teléfono de Trabajo', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, $row->telefono_trabajo_tutor.'            '.$row->empresa_telefono_trabajo, 1, 'L', 1, 0, '', '', true);
      $y = $y+5;
      PDF::SetXY(55,$y);
      PDF::MultiCell(45, 5, 'Cargo:', 1, 'R', 1, 0, '', '', true);
      PDF::MultiCell(0, 5, ucwords($row->cargo_tutor), 1, 'L', 1, 0, '', '', true);
    }
    PDF::Ln(5);
        // set color for background
    PDF::SetFillColor(133, 193, 233);
    //set font
    PDF::SetFont('helvetica', '', 12);
    PDF::MultiCell(45, 25, 'Información Médica', 1, 'C', 1, 0, '', '', true);
    PDF::SetFont('helvetica', '', 9);
    // set color for background
    PDF::SetFillColor(255, 255, 255);
    //get y
    $y = PDF::GetY();
    PDF::SetXY(55,$y);
    PDF::MultiCell(45, 5, 'Enfermedades que padece:', 1, 'R', 1, 0, '', '', true);
    PDF::MultiCell(0, 5, ucwords($estudiante->enfermedad_padecida_estudiante), 1, 'L', 1, 0, '', '', true);
    $y = $y+5;
    PDF::SetXY(55,$y);
    PDF::MultiCell(45, 5, 'Medicamentos recomendados', 1, 'R', 1, 0, '', '', true);
    PDF::MultiCell(0, 5, ucwords($estudiante->medicamento_recomendado_estudiante), 1, 'L', 1, 0, '', '', true);
    $y = $y+5;
    PDF::SetXY(55,$y);
    PDF::MultiCell(45, 5, 'Es alérgico a:', 1, 'R', 1, 0, '', '', true);
    PDF::MultiCell(0, 5, ucwords($estudiante->alergico_estudiante), 1, 'L', 1, 0, '', '', true);
    $y = $y+5;
    PDF::SetXY(55,$y);
    PDF::MultiCell(45, 5, 'Emergencia llevar a hospital:', 1, 'R', 1, 0, '', '', true);
    PDF::MultiCell(0, 5, ucwords($estudiante->hospital_estudiante), 1, 'L', 1, 0, '', '', true);
    $y = $y+5;
    PDF::SetXY(55,$y);
    PDF::MultiCell(45, 5, 'Tipo de sangre:', 1, 'R', 1, 0, '', '', true);
    PDF::MultiCell(0, 5, ucwords($estudiante->tipo_sangre_estudiante), 1, 'L', 1, 0, '', '', true);

    PDF::SetFillColor(133, 193, 233);
    //set font
    PDF::SetFont('helvetica', '', 12);
    PDF::Ln(5);
    PDF::MultiCell(45, 25, 'Cuotas Autorizadas', 1, 'C', 1, 0, '', '', true);

    PDF::SetFont('helvetica', '', 9);
    // set color for background
    PDF::SetFillColor(255, 255, 255);
    $y = $y+5;
    PDF::SetXY(55,$y);
    PDF::MultiCell(45, 5, 'Inscripción:', 1, 'R', 1, 0, '', '', true);
    PDF::MultiCell(45, 5, '1 cuota de:', 1, 'L', 1, 0, '', '', true);
    PDF::MultiCell(61, 5, 'Q.', 1, 'L', 1, 0, '', '', true);
    $y = $y+5;
    PDF::SetXY(55,$y);
    PDF::MultiCell(45, 5, 'Colegiaturas:', 1, 'R', 1, 0, '', '', true);
    PDF::MultiCell(45, 5, '10 cuotas de:', 1, 'L', 1, 0, '', '', true);
    PDF::MultiCell(61, 5, 'Q.', 1, 'L', 1, 0, '', '', true);
    $y = $y+5;
    PDF::SetXY(55,$y);
    PDF::MultiCell(45, 5, 'Derecho de examen:', 1, 'R', 1, 0, '', '', true);
    PDF::MultiCell(45, 5, '2 cuotas de:', 1, 'L', 1, 0, '', '', true);
    PDF::MultiCell(61, 5, 'Q.', 1, 'L', 1, 0, '', '', true);
    $y = $y+5;
    PDF::SetXY(55,$y);
    PDF::MultiCell(45, 5, 'Graduación:', 1, 'R', 1, 0, '', '', true);
    PDF::MultiCell(45, 5, '3 cuotas de:', 1, 'L', 1, 0, '', '', true);
    PDF::MultiCell(61, 5, 'Q.', 1, 'L', 1, 0, '', '', true);
    $y = $y+5;
    PDF::SetXY(55,$y);
    PDF::MultiCell(45, 5, 'Carné:', 1, 'R', 1, 0, '', '', true);
    PDF::MultiCell(45, 5, '1 cuota de:', 1, 'L', 1, 0, '', '', true);
    PDF::MultiCell(61, 5, 'Q.', 1, 'L', 1, 0, '', '', true);

    PDF::SetFillColor(133, 193, 233);
    //set font
    PDF::SetFont('helvetica', '', 12);
    PDF::Ln(5);
    PDF::MultiCell(45, 15, 'Observaciones:', 1, 'C', 1, 0, '', '', true);
    PDF::SetFont('helvetica', '', 9);
    // set color for background
    PDF::SetFillColor(255, 255, 255);
    PDF::MultiCell(0, 5, $estudiante->observaciones_estudiante, 1, 'L', 1, 0, '', '', true);

    //pie de pagina
    PDF::setFooterCallback(function($pdf){
      $pdf->SetXY(10,-35);
      $pdf->SetFillColor(255, 255, 255);
      $pdf->MultiCell(98, 5, 'f____________________', 0, 'C', 1, 0, '', '', true);
      $pdf->MultiCell(98, 5, 'f____________________', 0, 'C', 1, 0, '', '', true);
      $pdf->SetXY(10,-30);
      $pdf->MultiCell(98, 5, 'Firma del encargado', 0, 'C', 1, 0, '', '', true);
      $pdf->MultiCell(98, 5, 'Firma del alumno', 0, 'C', 1, 0, '', '', true);
    });
    //fin del pie de pagina
    $directorio = public_path().'/constancia_inscripcion';
      PDF::Output($directorio.'/constancia.pdf', 'F');
    }
}
