<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;

use education\Roles;
use education\Usuario_persona;
use education\Asignacion_docente;


use education\Http\Requests\ActividadesRequest;
use education\Http\Controllers\menuController;
use education\Repositories\NotificacionesRepository;
use Session;
use Auth;
use DB;

class MisNotificacionesDocenteController extends Controller
{
  protected $menu;
  protected $notificaciones;

public function __construct(NotificacionesRepository $notificacion)
  {
    $m = new menuController();
    $this->menu = $m->index();
    $this->notificaciones = $notificacion;
    
  }

   public function index(Request $request)
    {


        $user=$this->notificaciones->usuario =(Auth::user()->id);
       
        $rol=Roles::join('role_user as ru', 'roles.id', '=', 'ru.role_id')
                 ->where('ru.user_id',$user) 
                 ->select('roles.id')            
                 ->first();
        $persona=Usuario_persona::where('USUARIO_PERSONA.user_id',$user)
                 ->select('USUARIO_PERSONA.id_persona as id')
                 ->first();
        $asignacion=Asignacion_docente::join('ASIGNACION_AREAS as aa', 'ASIGNACION_DOCENTE.id_asignacion_area', '=', 'aa.id_asignacion_area')
                 ->where('ASIGNACION_DOCENTE.id_persona',$persona)
                 ->groupBy('aa.id_nivel_grado')
                 ->first();
 
                 

                 $notificaciones= DB::table('roles as r')
                 ->join('role_user as ru', 'r.id', '=', 'ru.role_id')
                 ->join('users as u', 'ru.user_id', '=', 'u.id')
                 ->join('USUARIO_PERSONA as up', 'u.id', '=', 'up.user_id')
                 ->join('PERSONAS as p', 'up.id_persona', '=', 'p.id_persona')
                 ->join('ASIGNACION_DOCENTE as ad', 'p.id_persona', '=', 'ad.id_persona')
                 ->join('ASIGNACION_AREAS as aa', 'ad.id_asignacion_area', '=', 'aa.id_asignacion_area')
                 ->join('NOTIFICACIONES as n', 'aa.id_nivel_grado', '=', 'n.id_nivel_grado')
                 ->join('users as us', 'n.user', '=', 'us.id')
                 ->where('ru.user_id',$user)
                 ->where('n.id_rol',2)
                
     ->select(DB::raw('DISTINCT n.id_notificacion'),'n.titulo_notificacion','n.informacion_notificacion','n.id_nivel_grado','n.fecha_create','us.name')

     ->orderBy('n.fecha_create', 'desc')
                 ->paginate(10);


       

        /* QUERY FUMADO
        SELECT DISTINCT(n.id_notificacion), n.titulo_notificacion, n.informacion_notificacion FROM roles as r join role_user as ru on r.id=ru.role_id
            join  users as u on ru.user_id=u.id
            join USUARIO_PERSONA as up on u.id=up.user_id
            join PERSONAS as p on up.id_persona=p.id_persona
            join ASIGNACION_DOCENTE as ad on p.id_persona=ad.id_persona
            join ASIGNACION_AREAS as aa on ad.id_asignacion_area=aa.id_asignacion_area
            join NOTIFICACIONES as n on aa.id_nivel_grado=n.id_nivel_grado
            join users as us on n.user=us.id
            where ru.user_id=3 and n.id_rol=2
*/
       

      



        return view('notificacionesdocente.index', [ 'items'=>$this->menu, 'notificaciones'=>$notificaciones,'rol'=>$rol]);
    }

      public function show(Request $request)

    {

        $id=$request['a'];
        $user = Auth::user()->id;
        $insert=DB::table('log_notificaciones')->insertGetId(
                      ['usuario'=>$user]);
        
        $notificaciones=DB::table('NOTIFICACIONES as n')
                ->join('users as u', 'n.user', '=', 'u.id')
                 ->where('n.id_notificacion', $id)
                 ->select('n.id_notificacion','n.titulo_notificacion','n.informacion_notificacion','n.id_nivel_grado','n.fecha_create','u.name')                 
                 ->get();

        //$notificaciones= notificaciones::where('id_notificacion', $request['a'])->first();

        return view('notificaciones.snotificacion', ['items'=>$this->menu, 'notificaciones'=>$notificaciones]);

    }
}
