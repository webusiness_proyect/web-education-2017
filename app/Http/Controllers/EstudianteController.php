<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

use education\Http\Requests;
use education\Http\Controllers\menuController;
use education\Estudiantes;
use \Validator;
use Session;
use education\User;
use Auth;

class EstudianteController extends Controller
{
     protected $menu;
    protected $users;
    public function __construct()
    {
      $m = new menuController();
      $this->menu = $m->index();
       $this->users->email=$request['c'];

        $items2 = $this->users->findUsuarioWhere();
    }
    /**
     * Display a listing of the resource.
     *   
     * @return \Illuminate\Http\Response
     */
	public function index()
    {
        $user = User::where('id', Auth::user()->id)->first();
        if ($user->can(['crear-inscripcion', 'ver-inscripcion'])) {
          $estudiantes = Estudiantes::All();

          return view('estudiantesinscritos.index', ['estudiantes'=>$estudiantes, 'items'=>$this->menu]);

          //$personas_roles=Personas::join('PUESTOS as p', 'PERSONAS.id_puesto', '=', 'p.id_puesto');
          //return view('personas.index', ['personas_roles'=>$personas_roles, 'items'=>$this->menu]);
        }
        return abort(403);
    }
      public function show($id)
    {
        //
       $user = User::where('id', Auth::user()->id)->first();
        if ($user->can('ver-inscripcion')) {
          $estudiante = ESTUDIANTES::findEstudiante($id);
          return view('estudiantesinscritos.minscripcion', ['items'=>$estudiante, 'items'=>$this->menu]);
        }
        return abort(403);
    }

}
