<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;

use DB;

class BuscarNotasController extends Controller
{

	 protected $menu;


    public function __construct()


    {


      $m = new menuController();


      $this->menu = $m->index();


    }
     public function index(Request $request)


    {

      /*
      select * from ESTUDIANTES where id_nivel_grado=3 and fecha_update like '%2016%'
      */

  	   $students=DB::table('ESTUDIANTES as E')

          ->where('E.id_nivel_grado','=',  $request['id_nivel'])
          ->where('E.fecha_update','LIKE', '%'.$request['año'].'%')
          ->select('E.nombre_estudiante', 'E.apellidos_estudiante', 'E.id')
          ->orderby('E.apellidos_estudiante', 'asc')
                   
          ->paginate(10);

          $year=$request['año'];


          if(($students['id'])==0)
          {
             $students2=DB::table('ESTUDIANTES as E')

          ->where('E.id_nivel_grado','=',  $request['id_nivel'])
          ->where('E.fecha_registro_estudiante','LIKE', '%'.$request['año'].'%')
          ->select('E.nombre_estudiante', 'E.apellidos_estudiante', 'E.id')
                   
          ->paginate(10);

          return view('administra_notas_niveles.search', ['items' => $this->menu, 'students'=>$students2, 'year'=>$year]);
          }

          else
          {
            return view('administra_notas_niveles.search', ['items' => $this->menu, 'students'=>$students, 'year'=>$year]);
          }
     
       
        
	}

	}
