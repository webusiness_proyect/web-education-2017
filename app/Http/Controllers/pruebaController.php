<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;
use education\Role;
use education\User;
use education\Permission;
class pruebaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      /**
       * Creamos un Rol
       */
      //$admin = new \App\Models\Role();
      $admin = new Role();
      $admin->name = 'admin';
      $admin->display_name = 'Administrador de usuarios'; // opcional
      $admin->description = 'Se permite al usuario gestionar y editar otros usuarios'; // opcional
      $admin->save();

      /**
       * Creamos un Usuario
       */
      //$user = new \App\User();
      $user = new User();
      $user->name = 'Byron Castro';
      $user->email = 'bicguti@gmail.com';
      $user->password = bcrypt('12345');
      $user->save();

      /**
      * Asignamos el Rol admin al usuario
      * Usando el alias del paquete
      */
      //$user->attachRole($admin);// podemos enviar el Rol o el id del Rol

      /**
      * O usamos Eloquent
      */
      $user->roles()->attach($admin->id); //usamos solamente el id del Rol

      /**
      * Creamos permisos
      */
      //$carrera = new \App\Models\Permission();
      $carrera = new Permission();
      $carrera->name = 'create-carrera';
      $carrera->display_name = 'Crear Carreras'; // opcional
      // Allow a user to...
      $carrera->description = 'crear nuevas carreras'; // opcional
      $carrera->save();

      //$editNivel = new \App\Models\Permission();
      $editNivel = new Permission();
      $editNivel->name = 'edit-nivel';
      $editNivel->display_name = 'Editar Niveles'; // opcional
      // Allow a user to...
      $editNivel->description = 'editar los niveles existentes'; // opcional
      $editNivel->save();

      /**
      * Al Rol admin le asignamos los permisos
      */
      //$admin->attachPermission($carrera); //esto es equivalente a $admin->perms()->sync(array($createPost->id));
      $admin->attachPermissions([$carrera, $editNivel]); //esto es equivalente a $admin->perms()->sync(array($createPost->id, $editUser->id));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
