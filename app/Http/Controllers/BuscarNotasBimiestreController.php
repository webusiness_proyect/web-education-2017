<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;

use education\Unidades;

use education\Repositories\actividadesRepository;

use DB;

class BuscarNotasBimiestreController extends Controller
{
   protected $menu;

   protected $actividades;


    public function __construct(actividadesRepository $actividad)


    {


      $m = new menuController();


      $this->menu = $m->index();

      $this->actividades = $actividad;


    }
     public function index(Request $request)


    {

      /*
      SELECT * FROM AREAS as a join ASIGNACION_AREAS as aa on a.id_area=aa.id_area join ESTUDIANTES as E on e.id_nivel_grado=aa.id_nivel_grado where e.id=17
      */

       //$unidades = Unidades::getUnidades();

       $unidades=DB::table('UNIDADES as u')
       ->select('u.id_unidad','u.nombre_unidad')      
       ->get();

       $year=$request['b'];

  	   

             $students=DB::table('AREAS as a')
             ->join('ASIGNACION_AREAS as aa','a.id_area','=','aa.id_area')
             ->join('ESTUDIANTES as e','e.id_nivel_grado','=','aa.id_nivel_grado')
             ->where('e.id','=',  $request['a'])
             ->select('a.nombre_area','a.id_area', 'e.nombre_estudiante', 'e.apellidos_estudiante', 'e.id', 'e.id_nivel_grado')
             ->get();

             /*
             QUERY QUE MUESTRA TODOS LOS CURSOS CON SU NOTA FINAL
             SELECT a.nombre_area, sum(f.calificacion) FROM AREAS as a join ASIGNACION_AREAS AS aa on a.id_area=aa.id_area LEFT OUTER JOIN files as f on aa.id_area=f.area where aa.id_nivel_grado=4 group by a.nombre_area asc
             */



             /*
             query con pivote
             SELECT a.nombre_area, (CASE WHEN f.unidad=1 THEN sum(f.calificacion) END) AS 'unidad 1',
             (CASE WHEN f.unidad=2 THEN sum(f.calificacion) END) AS 'unidad 2',
             (CASE WHEN f.unidad=3 THEN sum(f.calificacion) END) AS 'unidad 3',
             (CASE WHEN f.unidad=4 THEN sum(f.calificacion) END) AS 'unidad 4',
             (CASE WHEN f.unidad=5 THEN sum(f.calificacion) END) AS 'unidad 5',
             (CASE WHEN f.unidad=6 THEN sum(f.calificacion) END) AS 'unidad 6' FROM `AREAS` as a LEFT OUTER JOIN files as f on a.id_area=f.area
             LEFT OUTER JOIN UNIDADES as u on f.unidad=u.id_unidad
             where f.id_usuario=24
             group by  a.nombre_area, f.unidad


*/
               /* QUERY QUE MUESTRA TODAS LAS NOTAS POR CURSO POR UNIDAD
SELECT a.nombre_area, sum(f.calificacion) FROM AREAS as a join ASIGNACION_AREAS AS aa on a.id_area=aa.id_area LEFT OUTER JOIN files as f on aa.id_area=f.area where f.id_usuario=24 and f.unidad=6 group by a.nombre_area asc 
UNION ALL 
SELECT a.nombre_area, 0 as nota from AREAS as a join ASIGNACION_AREAS AS aa on a.id_area=aa.id_area where not exists (select * from files as f where f.area = a.id_area and f.unidad=6 and f.id_usuario=24) and aa.id_nivel_grado=1 group by a.nombre_area
             */
//PRIMERA UNIDAD
             $notasok1=DB::table('AREAS as a')
             ->join('ASIGNACION_AREAS AS aa','a.id_area','=','aa.id_area')
             ->leftjoin('files as f','aa.id_area','=','f.area')
             ->where('f.id_usuario','=',  $request['a'])
             ->where('f.unidad','=', '6')
             ->select(DB::raw('sum(f.calificacion) as nota1', 'a.id_area', 'a.nombre_area'))
             ->groupBy('a.nombre_area')
             ->get();

            $notasno1=DB::table('AREAS as a')
                ->join('ASIGNACION_AREAS AS aa','a.id_area','=','aa.id_area')
                    ->whereNotExists(function($query) 
                    {
                        $query->from('files as f')
                              ->whereRaw('f.area = a.id_area')
                              ->where('f.unidad', '=', '6');     
                    })
                    ->select(DB::raw('SUM(a.id_area - a.id_area) as nota1', 'a.id_area', 'a.nombre_area'))
                    ->where('aa.id_nivel_grado',$students[0]->id_nivel_grado)
                    ->groupBy('a.nombre_area')
                    ->get();

                   
              /* QUERY QUE MUESTRA TODAS LAS NOTAS POR CURSO POR UNIDAD
SELECT a.nombre_area, sum(f.calificacion) FROM AREAS as a join ASIGNACION_AREAS AS aa on a.id_area=aa.id_area LEFT OUTER JOIN files as f on aa.id_area=f.area where f.id_usuario=24 and f.unidad=6 group by a.nombre_area asc 
UNION ALL 
SELECT a.nombre_area, 0 as nota from AREAS as a join ASIGNACION_AREAS AS aa on a.id_area=aa.id_area where not exists (select * from files as f where f.area = a.id_area and f.unidad=6 and f.id_usuario=24) and aa.id_nivel_grado=1 group by a.nombre_area
             */



                 


         
            return view('administra_notas_niveles.buscarnotabimestre', ['items' => $this->menu, 'students'=>$students, 'unidades'=>$unidades,'notasok1'=>$notasok1,'year'=>$year,'notasno1'=>$notasno1]);
     
     
       
        
	}

  public function show(Request $request)
  {


      $this->actividades->id_area=$request['area'];

      $this->actividades->id_unidad=$request['unidad'];

      $this->actividades->id_usuario=$request['user'];

      //$actividades = $this->actividades->ActividadesConEntregaPorunidad();//traigo actividades de esta unidad
      $students=$this->actividades->infoalumno();


    $actividades=DB::table('ACTIVIDADES as a')
    ->join('files as f','a.id_actividad','=','f.id_actividad')
    ->join('users as u','a.id_usuario','=','u.id')
    ->join('role_user as ru', 'ru.user_id', '=', 'u.id')
    ->join('roles as r', 'r.id', '=', 'ru.role_id')
    ->join('TIPO_ACTIVIDAD as ta', 'ta.id_tipo_actividad', '=', 'a.id_tipo_actividad')
    ->join('AREAS as ar', 'f.area', '=', 'ar.id_area')

    ->where('a.id_area','=',$request['area'])
    ->where('a.id_unidad','=',$request['unidad'])
    ->where('f.id_usuario','=',$request['user'])
        ->select('f.id_usuario', 'f.calificacion','ar.nombre_area','a.id_actividad')
    ->get();


      $year=$request['year'];

            return view('administra_notas_niveles.notas_unidad', ['items'=>$this->menu,'actividades'=>$this->actividades,'students'=>$students,'year'=>$year]);
  }
}
