<?php

namespace education\Http\Controllers;

use Illuminate\Http\Request;

use education\Http\Requests;

use education\Http\Requests\DirectorRequest;

use education\Departamento;

use education\Planes;

use education\Usuarios;

use education\Unidades;

use education\Estudiantes;

use education\Jornadas;

use education\Niveles;

use \Validator;

use Session;

use education\User;

use Auth;

use DB;



class DirectorController extends Controller
{


    protected $menu;


    public function __construct()


    {


      $m = new menuController();


      $this->menu = $m->index();


    }


    public function index(Request $request)


    {

  	$user = User::where('id', Auth::user()->id)->first();
  	

     if ($user->can(['ver-notas-nivel', 'modificar-notas-nivel'])) 

     {

        $deptos = Departamento::getDepartamentos();
        $planes = Planes::getPlanes();
        $jornadas = Jornadas::getJornadas();
        $niveles = Niveles::getNiveles();
        $unidades = Unidades::getUnidades();
            
        return view('administra_notas_niveles.index', ['items' => $this->menu, 'deptos'=>$deptos, 'planes'=>$planes, 'jornadas'=>$jornadas, 'niveles'=>$niveles, 'unidades'=>$unidades]);
      }


        return abort(403);

      }

      public function search(DirectorRequest $request)
    {
       //
        /* buscar todos los alumnos que entregaron una tarea
        select * from files as f where f.area=3 and f.unidad=6 and fecha_entrega like '%2017%'
        */


            $students=DB::table('files as f')

          ->where('f.area','=',  $request['id_nivel'])
          ->where('f.unidad','=', $request['id_unidad'])
          ->where('f.fecha_entrega','LIKE', '%'.$request['año'].'%')
          ->select('f.id','f.id_usuario', 'f.estado_entrega_actividad','f.calificacion')
          
          ->paginate(10);
     
       
        return view('administra_notas_niveles.search', ['items' => $this->menu, 'students'=>$students]);
    }


      

}
