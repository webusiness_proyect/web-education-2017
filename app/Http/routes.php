<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Route::get('logueo', 'autenticacionController@index');
Route::group(['middleware' => 'auth'], function () {

Route::get('/', 'principalController@index');
Route::resource('roles', 'rolesController');
Route::get('niveles/ajxniveles', 'nivelesController@listado');
Route::resource('niveles', 'nivelesController');
Route::resource('planes', 'planesController');
Route::resource('jornadas', 'jornadasController');
Route::resource('puestos', 'puestosController');
Route::resource('secciones', 'seccionesController');
Route::resource('grados', 'gradosController');
Route::get('asignarniveles/obtenerajx', 'planesnivelesController@bscajx');
Route::resource('asignarniveles', 'planesnivelesController');
Route::resource('salones', 'salonesController');
Route::resource('areas/autocomplete', 'areasController@areas');
Route::resource('areas', 'areasController');
Route::get('personas/autocomplete', 'personasController@bscautomplete');
Route::get('usuarios/autocomplete2', 'personasController@buscausuario');//busqueda de usuarios
Route::get('personas/autocomplete/docente/', 'personasController@buscadocente');
Route::resource('personas', 'personasController');
Route::resource('carreras', 'carrerasController');
Route::resource('gradoniveles', 'gradosnivelesController');
Route::resource('pensum/pensumgrado', 'asignacionareasController@pensumGrado');
Route::resource('pensum/grados', 'asignacionareasController@grados');
Route::resource('pensum', 'asignacionareasController');
Route::resource('subopciones', 'subopcionesController');
Route::resource('permisosroles', 'permisosrolesController');
Route::resource('usuarios', 'usuariosController');
Route::resource('prueba', 'pruebaController');
Route::resource('asignaciondocente/grados', 'asignaciondocenteController@getGrados');
Route::resource('asignaciondocente', 'asignaciondocenteController');
Route::get('inscripcionestudiantes/constancia', 'inscripcionController@constancia');
Route::resource('inscripcionestudiantes', 'inscripcionController');
Route::resource('municipios', 'municipioController');
Route::resource('unidades', 'unidadesController');
Route::resource('foros', 'forosController');
Route::resource('respuestasforos', 'respuestasforosController');

//cambios hechos por jose florian
Route::resource('verestudiantes', 'EstudianteController');//cj
Route::resource('verusuario', 'UsuariosRolesController');//cj
//actividades y tareas
Route::resource('Actividad', 'ActividadesController');//cj
Route::resource('misactividades',  'misActividadesController');//cj
Route::resource('respuestaactividades', 'RespondeActividadController');//cj
Route::resource('misrespuestasactividades', 'MisRespuestasActividadController');//cj
Route::resource('Nota', 'NotaController');//cj
Route::resource('MisNotas', 'MisNotasController');//cj
Route::resource('MisNotasPorUnidad', 'MisNotasPorUnidadController');//cj
Route::resource('horarios', 'horarioController');//cj
//inscripcion pdf 
Route::get('report', 'PdfController@invoice');
Route::get('personas/autocomplete/usuario/', 'personasController@buscausuario');
Route::get('asignarareas/obtenerajx', 'horarioController@bscajx');//cj horarios
//opciones de estudiantes
Route::resource('estudiantes', 'miscursosController');
Route::resource('misforos', 'misforosController');
Route::resource('todasmisactividades', 'todasMisActividadesController');
Route::resource('MisNotasTotales', 'MisNotasTotalesController');
Route::resource('mishorarios', 'mishorariosController');
// subir archivos
Route::get('file','tareaController@index');
Route::post('file/store','tareaController@store');
Route::get('file/show','tareaController@showall');
Route::get('file/show/{id}','tareaController@show');
Route::get('file/user/download', 'tareaController@download');


//opciones de profesores
Route::resource('docente', 'docenteController');
Route::resource('misforosdocente', 'misforosdocenteController');
Route::resource('misactividadesdocente', 'misactividadesdocenteController');
Route::resource('mishorariosdocente', 'mishorariosdocenteController');
Route::resource('misnotashijos', 'misnotashijosController');
Route::resource('misnotascursohijos', 'misnotascursohijosController');


//opciones de padres de familia
Route::resource('misalumnos', 'padresalumnosController');
Route::resource('misdocnetes', 'padresdocenteController');
Route::resource('tutores', 'TutoresController');
Route::resource('tutoreshijos', 'TutoresController');
Route::resource('misnotashijos', 'misnotashijosController');
Route::resource('miscursosprofesores', 'miscursosprofesoresController');
Route::resource('misactividadeshijos', 'actividadesdehijosController');


//mis opciones de configuracion
Route::resource('miperfil', 'miperfilController');//personaliza imagen y datos de usuario
Route::resource('misconfiguraciones', 'misconfiguracionesController');//personaliza imagen y nombres de institucion
Route::resource('helpdesk', 'helpdeskController');//panel de ayuda y soporte documentacion y foros de ayuda
Route::resource('enviomensajes', 'enviomensajesController');//formulario de contacto y ayuda

//NOTIFICACIONES
Route::resource('notificaciones', 'notificacionesController');//formulario para envio de notificaciones
Route::resource('notificaciones/autocomplete/', 'notificacionesController@notificaciones');

Route::resource('plantillas.menu', 'menunotificacionesController');//esta babosada no sirve por las rutas verificar si se puede hacer con js

Route::resource('notificacionesdocente', 'MisNotificacionesDocenteController');
Route::resource('notificacionesalumno', 'MisNotificacionesAlumnoController');
Route::resource('notificacionespadre', 'MisNotificacionesPadreController');
Route::resource('notificacionesuser', 'MisNotificacionesPadreController');//pendiente

//calendario de hoy

Route::resource('indexalumno', 'miHorarioHoyController');

//director

Route::resource('administra_notas_niveles', 'DirectorController');
Route::resource('buscarnotas', 'BuscarNotasController');
Route::resource('buscarnotasbimestre', 'BuscarNotasBimiestreController');
Route::resource('resultadosbuscarnotasbimestre', 'resultadosbuscarnotabirmestreController');










//Route::get('constancia', 'reportesController@constancia_inscripcion');
//Route::get('pagos', 'reportesController@constancia_inscripcion');
});//fin del rout group
/*Route::get('/', function () {
    return view('welcome');
});*/

Route::auth();

//Route::get('/home', 'HomeController@index');
