<?php



namespace education\Http\Requests;



use education\Http\Requests\Request;



class inscripcionRequest extends Request

{

    /**

     * Determine if the user is authorized to make this request.

     *

     * @return bool

     */

    public function authorize()

    {

        return true;

    }



    /**

     * Get the validation rules that apply to the request.

     *

     * @return array

     */

    public function rules()

    {

        return  [

            'id_grado' => 'required',

            'id_municipio' => 'required',

            'nombre_estudiante' => 'required|max:60',

            'apellidos_estudiante' => 'required|max:60',

            'fecha_nacimiento_estudiante' => 'required|date_format:"Y-m-d"',

            'genero_estudiante' => 'required',

            'direccion_estudiante' => 'required|max:100',

            //'colonia_estudiante' => 'required|max:100',

            //'zona_estudiante' => 'required|max:30',

            //'telefono_casa' => 'required|digits_between:8,15',

            //'telefono_casa' => 'required|numeric|max:15',

            'empresa_telefonica' => 'required|max:15',

            'correo_estudiante' => 'required|email',

            //'enfermedad_padecida_estudiante' => 'required|max:1000',

            //'medicamento_recomendado_estudiante' => 'required|max:1000',

            //'alergico_estudiante' =>    'required|max:500',

            //'hospital_estudiante' => 'required|max:100',

            //'tipo_sangre_estudiante' => 'required|max:30',

            //'observaciones_estudiante' => 'required|max:1000',

            //validacion de los campos de los datos del tutor o tutores

            'tutor.*.nombre' => 'required|max:60',

            'tutor.*.apellidos' => 'required|max:60',

            //'tutor.*.direccion' => 'required|max:70',

            //'tutor.*.telefono_primario' => 'required|digits_between:8,15',

            //'tutor.*.empresa_telefono' => 'required|max:30',

            'tutor.*.correo_electronico' => 'required|email',

            'tutor.*.cui' => 'required|numeric',

            //'tutor.*.lugar_trabajo' => 'required|max:100',

            //'tutor.*.direccion_trabajo' => 'required|max:100',

            //'tutor.*.telefono_trabajo' => 'required|digits_between:8,15',

            //'tutor.*.empresa_telefono_trabajo' => 'required|max:30',

            //'tutor.*.cargo' => 'required|max:50',

            //'tutor.*.parentesco' => 'required|max:50',

            //validacion de los campos de los datos de la referencia o referencias

            'referencia.*.nombre' => 'required|max:150',

            //'referencia.*.parentesco' => 'required|max:50',

            //'referencia.*.telefono' => 'required|max:50',

        ];



    }

    public function messages()

    {

      return [

        'referencia.*.telefono.required' =>'El número de telefono de la referencia es requerido!!!',

        //'nombre_puesto.max' =>'El nombre del puesto no puede tener más de 50 caracteres!!!',

        //'nombre_puesto.min' => 'El nombre del puesto debe tener como minimo 2 caracteres!!!',

        //'nombre_puesto.unique' => 'El nombre del puesto debe ser unico, este nombre de puesto ya esta registrado!!!'

      ];

    }

}

