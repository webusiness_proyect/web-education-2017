<?php

namespace education\Http\Requests;

use education\Http\Requests\Request;

class horariosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'id_nivel_grado' => 'required',

            'id_area' => 'required',

            'id_dia' => 'required',

            'hora_inicia' => 'required',

            'hora_fin' => 'required',



        ];
    }
}
