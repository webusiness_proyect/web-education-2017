<?php

namespace education\Http\Requests;

use education\Http\Requests\Request;

class forosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $id = $this->route('foros');
      if ($this->method() == 'PUT') {
        $titulo_foro = 'required|max:40|unique:FOROS,titulo_foro,'.$id.',id_foro';
      }else {
        $titulo_foro = 'required|max:40|unique:FOROS,titulo_foro';
      }
        return [
            'titulo_foro'=>$titulo_foro,
            'mensaje_foro'=>'required|max:5000',
            'curso'=>'required'
        ];
    }
}
