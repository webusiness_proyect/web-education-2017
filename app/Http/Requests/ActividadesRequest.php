<?php

namespace education\Http\Requests;

use education\Http\Requests\Request;

class ActividadesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('actividades');
        if ($this->method() == 'PUT') {
        $titulo_foro = 'required|max:40|unique:ACTIVIDADES,id_actividad,'.$id.',id_actividad';
      }else {
        $titulo_foro = 'required|max:40|unique:ACTIVIDADES,id_actividad     ';
      }
        return [
            'id_tipo_actividad' => 'required',
            'nombre_actividad' => 'required|max:60',
            'descripcion_actividad' => 'required|max:60',
            'fecha_entrega' => 'required|date_format:"Y-m-d"',
            'nota_total' => 'required'
            

           
        ];
    }
}




