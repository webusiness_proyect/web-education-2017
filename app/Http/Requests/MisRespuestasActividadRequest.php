<?php

namespace education\Http\Requests;

use education\Http\Requests\Request;
use education\Repositories\RespondeActividadesRepository;
use DB;
use education\actividad;

class MisRespuestasActividadRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


         
         $id = $this->route('misrespuestasactividades');
         
      
        return [

        

            'estado_entrega_actividad'=>'required',
            'calificacion'=>'required|numeric|min:0|max:100',
            'observaciones_maestro'=>'required|max:500',



          

            
            
        ];
    }

    
}
