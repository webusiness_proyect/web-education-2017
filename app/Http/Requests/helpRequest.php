<?php



namespace education\Http\Requests;



use education\Http\Requests\Request;



class helpRequest extends Request

{

    /**

     * Determine if the user is authorized to make this request.

     *

     * @return bool

     */

    public function authorize()

    {

        return true;

    }



    /**

     * Get the validation rules that apply to the request.

     *

     * @return array

     */

    public function rules()

    {

      

        return [

            'asunto'=>'required',

            'descripcionhelp'=>'required|max:100'

        ];

    }

}

