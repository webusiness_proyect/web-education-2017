<?php







namespace education;







use Illuminate\Foundation\Auth\User as Authenticatable;



use Zizaco\Entrust\Traits\EntrustUserTrait;



use DB;



class User extends Authenticatable



{



    use EntrustUserTrait; //hacemos uso del trait en la clase User para hacer uso de sus métodos



    /**



     * The attributes that are mass assignable.



     *



     * @var array



     */



     protected $table = 'users';



    protected $fillable = [



        'name', 'email', 'password',



    ];







    /**



     * The attributes that should be hidden for arrays.



     *



     * @var array



     */



    protected $hidden = [



        'password', 'remember_token',



    ];







    //establecemos las relaciones con el modelo Role, ya que un usuario puede tener varios roles



    //y un rol lo pueden tener varios usuarios



    public function roles(){



        return $this->belongsToMany('education\Role');



    }







    /*



      Metodo para agregar un nuevo registro a la base de datos



    */



    public static function setUsuario($usuario, $pass, $fecha, $correo, $rol)



    {



      return DB::select('CALL nuevo_usuario(?, ?, ?, ?, ?)', array($usuario, $pass, $fecha, $correo, $rol));



    }







    /*



      Metodo para agregar un nuevo rol a un usuario



    */



    public static function setRolUsuario($usuario, $rol)



    {



      return DB::insert('CALL nuevo_rol_usuario(?, ?)', array($usuario, $rol));



    }



    /*



      Metodo para buscar los permisos asignados a un usuario por medio de su rol



    */



    public static function getPermisosRol($id)



    {



        return DB::table('OPCIONES as o')



                 ->join('permissions as p', 'o.id_opcion', '=', 'p.id_opcion')



                 ->join('permission_role as pr', 'p.id', '=', 'pr.permission_id')



                 ->join('roles as r', 'pr.role_id', '=', 'r.id')



                 ->join('role_user as ru', 'r.id', '=', 'ru.role_id')



                 ->where('ru.user_id', $id)

                 ->where('pr.state_permission_role', 1)



                 ->select('o.nombre_opcion', 'p.display_name', 'o.contenedor_principal', 'o.ruta_opcion','r.id')



                 ->get();



    }







    //funcion agregada por cj para agregar tareas



    public function actividad()



    {



        return $this->belongsToMany('education\actividad','idUsuario','id');



    }





    public static function getUsuario($q)

    {

      /*return PERSONAS::join('PUESTOS as p', 'PERSONAS.id_puesto', '=', 'p.id_puesto')

                     ->where('nombre_puesto', '=', 'docente')

                     //->orWhere('nombre_persona', 'like', '%'.$q.'%')

                     //->where([['p.nombre_puesto', '=', 'docente'], ['apellidos_persona', 'like', '%'.$q.'%'], ['nombre_persona', 'like', '%'.$q.'%']])

                    //->where([['apellidos_persona', 'like', '%'.$q.'%'], 'OR',['nombres_persona', 'like', '%'.$q.'%']])

                     //->orWhere('nombres_persona', 'like', '%'.$q.'%')

                     ->select('nombres_persona', 'apellidos_persona', 'id_persona')

                     ->get();*/

      return users::where(function ($query) use ($q) {

                         $query->where('name', 'like', '%'.$q.'%')

                               ->orWhere('email', 'like', '%'.$q.'%');

                     })

                     ->select('name', 'email', 'id')

                     ->get();



              

                      

    }


    public static function findUser($id)
    {
      return DB::table('users')
      ->join('role_user AS ru', 'users.id', '=', 'ru.user_id')
      ->join('roles AS r', 'ru.role_id', '=', 'r.id')
      ->select('users.id', 'users.name', 'r.name as rol', 'ru.user_id', 'ru.role_id')
      ->where('users.id', $id)->first();

      /*
      select u.name,r.name from users as u join role_user as ru on u.id=ru.user_id 
join     on ru.role_id=r.id where u.id=1
*/

    }

     public static function findRolUser($id)
    {
      return DB::table('roles as r')
      ->join('role_user as ru', 'r.id', '=', 'ru.role_id')
      ->select('r.id', 'r.name')
      ->where('ru.user_id', $id)->first();

      /*
      select * from roles as r join role_user as ru on r.id=ru.role_id where ru.user_id=1
*/

    }














}



