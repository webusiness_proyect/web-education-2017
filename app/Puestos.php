<?php

namespace education;

use Illuminate\Database\Eloquent\Model;
use DB;
class Puestos extends Model
{
    protected $table = 'PUESTOS';

    /*
      Metodo para agregar un nuevo registro a la base de datos
    */
    public static function setPuesto($nombre)
    {
      return DB::select('CALL nuevo_puesto(?)', array($nombre));
    }

    /*
      Metodo para buscar los datos de un puesto por su id
    */
    public static function findPuesto($id)
    {
      return Puestos::where('id_puesto', $id)->first();
    }

    /*
      Metodo para actualizar los datos de un puesto
    */
    public static function updatePuesto($id, $nombre)
    {
      return DB::select('CALL actualizar_puesto(?, ?)', array($id, $nombre));
    }

    /*
      metodo para cambiar el estado de un puesto
    */
    public static function statePuesto($id, $estado)
    {
      return DB::select('CALL estado_puesto(?, ?)', array($id, $estado));
    }
}
