<?php



namespace education;




use Illuminate\Database\Eloquent\Model;

use DB;

class Estudiantes extends Model

{

    protected $table = 'ESTUDIANTES';



     /*

      Metodo para obtener los datos de un alumno por su id

    */

    public static function findEstudiante($id)

    {

      //return ESTUDIANTES::where('id_estudiante', $id)->first();

      return ESTUDIANTES::join('TUTOR_ESTUDIANTE as te','ESTUDIANTES.id_estudiante','=','te.id_estudiante')
      		->join('TUTOR AS T','T.id_tutor','=','te.id_tutor')
    	 	->where('ESTUDIANTES.id_estudiante', $id)->first();

    	 /*select * from ESTUDIANTES join TUTOR_ESTUDIANTE as te on ESTUDIANTES.id_estudiante=te.id_estudiante
		JOIN TUTOR AS T ON T.id_tutor=te.id_tutor where ESTUDIANTES.id_estudiante=17
		*/

    }


    /*

      Metodo para cambiar el estado de un estudiante

    */

    public static function stateEstudiante($id, $estado)

    {

      return DB::select('CALL estado_estudiante(?, ?)', array($id, $estado));

    }

    public static function stateUser($id, $estado)

    {

      return DB::select('CALL estado_usuarios(?, ?)', array($id, $estado));

    }

    




}

