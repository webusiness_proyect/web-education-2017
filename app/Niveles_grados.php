<?php

namespace education;

use Illuminate\Database\Eloquent\Model;
use DB;
class Niveles_grados extends Model
{
    protected $table = 'NIVELES_GRADOS';

    /*
      Metodo par abuscar el nombre de un grado, nivel, seccion.
    */
    public static function getNivelesGrados()
    {
      return NIVELES_GRADOS::join('NIVELES_PLANES_JORNADAS AS npj', 'NIVELES_GRADOS.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
                           ->join('NIVELES AS n', 'npj.id_nivel', '=', 'n.id_nivel')
                           ->join('GRADOS AS g', 'NIVELES_GRADOS.id_grado', '=', 'g.id_grado')
                           ->join('SECCIONES AS s', 'NIVELES_GRADOS.id_seccion', '=', 's.id_seccion')
                           ->join('CARRERAS AS c', 'NIVELES_GRADOS.id_carrera', '=', 'c.id_carrera')
                           ->select('NIVELES_GRADOS.estado_nivel_grado', 'NIVELES_GRADOS.id_nivel_grado', 'g.nombre_grado', 's.nombre_seccion', 'c.nombre_carrera', 'n.nombre_nivel')
                           
                           ->get();
    }

    /*
      Metodo para agregar un nuevo registro a la base de datos
    */
    public static function setGradoNivel($npj, $grado, $carrera, $seccion, $inscripcion, $mensualidad)
    {
      return DB::select('CALL nuevo_nivel_grado(?, ?, ?, ?, ?, ?)', array($npj, $grado, $carrera, $seccion, $inscripcion, $mensualidad));
    }

    /*
      Metodo para buscar los datos de un registro de la tabla NIVELES_GRADOS
    */
    public static function findGradoNivel($id)
    {
      return NIVELES_GRADOS::where('id_nivel_grado', $id)->first();
    }

    /*
      Metodo para obtener el nombre de nivel, plan y jornada asignado a un NIVEL_GRADO
    */
    public static function findNivelPJ($npj)
    {
      return NIVELES_GRADOS::join('NIVELES_PLANES_JORNADAS AS npj', 'NIVELES_GRADOS.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
                           ->join('NIVELES AS n ', 'npj.id_nivel', '=', 'n.id_nivel')
                           ->where('npj.id_nivel_plan_jornada', $npj)
                           ->distinct('n.nombre_nivel')
                           ->select('n.nombre_nivel', 'npj.id_jornada', 'npj.id_plan', 'npj.id_nivel_plan_jornada')
                           ->get();
    }

    /*
      Metodo para actualizar la asignación de un grado a un nivel
    */
    public static function updateGradoNivel($id, $npj, $grado, $carrera, $seccion, $inscripcion, $mensualidad)
    {
      return DB::select('CALL actualizar_nivel_grado(?, ?, ?, ?, ?, ?, ?)', array($id, $npj, $grado, $carrera, $seccion, $inscripcion, $mensualidad));
    }

    /*
      Metodo para cambiar el estado de un registro de la tabla NIVELES_GRADOS
    */
    public static function stateGradoNivel($id, $estado)
    {
      return DB::select('CALL estado_nivel_grado(?, ?)', array($id, $estado));
    }

    /*
      Obtiene los grados de acuerod a un nivel, plan y jornada
    */
    public static function getGrados($id)
    {
      return NIVELES_GRADOS::join('GRADOS as g', 'NIVELES_GRADOS.id_grado', '=', 'g.id_grado')
                           ->join('SECCIONES as s', 'NIVELES_GRADOS.id_seccion', '=', 's.id_seccion')
                           ->join('CARRERAS as c', 'NIVELES_GRADOS.id_carrera', '=', 'c.id_carrera')
                           ->where('NIVELES_GRADOS.id_nivel_plan_jornada', $id)
                           ->select('g.nombre_grado', 'c.nombre_carrera', 's.nombre_seccion', 'NIVELES_GRADOS.id_nivel_grado')
                           ->get();
    }
    /*
    select g.nombre_grado, c.nombre_carrera, s.nombre_seccion, ng.id_nivel_grado
from NIVELES_GRADOS ng inner join GRADOS g
on ng.id_grado = g.id_grado inner join SECCIONES s
on ng.id_seccion = s.id_seccion inner join CARRERAS c
on ng.id_carrera = c.id_carrera
where ng.id_nivel_plan_jornada = 1
    */

}
