<?php

namespace education;

use Illuminate\Database\Eloquent\Model;


use DB;

class notificaciones extends Model
{
       protected $table =  'NOTIFICACIONES';


        public static function setEnvioNotificacion($notificacion, $grado, $id_rol, $titulo, $descripcion, $user)

    {

      return DB::insert('CALL nueva_notificacion(?, ?, ?, ?, ?, ?)', array($notificacion, $grado, $id_rol, $titulo, $descripcion, $user));

    }


     public static function findnotificacion($rand)

    {

      return notificaciones::where('id_notificacion', $rand)->first();

    }
     public static function findnotificacionbyid($id)

    {

      return notificaciones::where('id_notificacion', $id)->first();

    }




}
