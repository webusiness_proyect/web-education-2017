<?php



namespace education;



use Illuminate\Database\Eloquent\Model;

use DB;



class Tutor extends Model

{

    protected $table = 'TUTOR';

     public static function findTutor($id)

    {

    	 return TUTOR::join('TUTOR_ESTUDIANTE as te','te.id_tutor','=','TUTOR.id_tutor')
    	 ->where('te.id_estudiante', $id)->first();
    	 /*

      return  DB::table('TUTOR_ESTUDIANTE as te')

    

     ->join('TUTOR as t','te.id_tutor','=','t.id_tutor')


     ->select('te.id_tutor_estudiante','te.id_tutor','t.id_tutor','t.nombre_tutor','t.apellidos_tutor','t.direccion_tutor','t.telefono_primario_tutor','t.empresa_telefono_tutor','t.correo_electronico_tutor','t.cui_tutor','t.lugar_trabajo_tutor','t.direccion_trabajo_tutor','t.telefono_trabajo_tutor','t.empresa_telefono_trabajo','t.cargo_tutor','t.parentesco_tutor')


    ->where('te.id_estudiante',$id)

    ->get();
    */

      /*SELECT * FROM   tutor as t join `tutor_estudiante` as te on te.id_tutor=t.id_tutor where te.id_estudiante=17*/

    }

    public static function findTutorEstudiante($id)

    {

         return TUTOR::join('TUTOR_ESTUDIANTE as TU','TUTOR.id_tutor','=','TU.id_tutor')
         ->where('TU.id_estudiante', $id)->first();
         /*

      QUERY QUE MUESTRA TODOS LOS PADRES DE UN HIJO

SELECT * 
FROM TUTOR AS T
JOIN TUTOR_ESTUDIANTE AS TU ON T.id_tutor = TU.id_tutor
WHERE TU.id_estudiante =6
*/

    }

    public static function findTutorById($id)

    {

         return TUTOR::where('TUTOR.id_tutor', $id)->first();
         /*

      QUERY QUE MUESTRA TODOS LOS PADRES DE UN HIJO

SELECT * 
FROM TUTOR AS T
JOIN TUTOR_ESTUDIANTE AS TU ON T.id_tutor = TU.id_tutor
WHERE TU.id_estudiante =6
*/

    }


}

