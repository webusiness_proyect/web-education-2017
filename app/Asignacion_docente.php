<?php

namespace education;

use Illuminate\Database\Eloquent\Model;
use DB;
class Asignacion_docente extends Model
{
    protected $table = 'ASIGNACION_DOCENTE';

    /*
      Metodo para agregar un nuevo registro a la base de datos
    */
    public static function setAsignacionDocente($area, $persona)
    {
      return DB::insert('CALL nueva_asignacion_docente(?, ?)', array($area, $persona));
    }

    /*
      Obtiene los nombres de los docentes que ya tienen asignado cursos
    */
    public static function getNombreDocente()
    {
      return ASIGNACION_DOCENTE::join('PERSONAS as p', 'ASIGNACION_DOCENTE.id_persona', '=', 'p.id_persona')
                               ->select('p.id_persona', 'p.nombres_persona', 'p.apellidos_persona')
                               ->distinct()
                               ->get();
    }

    /*
      Metodo para obtener los cursos asignados de todos los niveles, jornadas, planes y grados
    */
    public static function getCursosAsignados($docente)
    {
      return ASIGNACION_DOCENTE::join('ASIGNACION_AREAS as aa', 'ASIGNACION_DOCENTE.id_asignacion_area', '=', 'aa.id_asignacion_area')
                              ->join('NIVELES_GRADOS as ng', 'aa.id_nivel_grado', '=', 'ng.id_nivel_grado')
                              ->join('AREAS as a', 'aa.id_area', '=', 'a.id_area')
                              ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')
                              ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')
                              ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')
                              ->join('CARRERAS as c', 'ng.id_carrera', '=', 'c.id_carrera')
                              ->join('PLANES as p', 'npj.id_plan', '=', 'p.id_plan')
                              ->join('JORNADAS as j', 'npj.id_jornada', '=', 'j.id_jornada')
                              ->where('ASIGNACION_DOCENTE.id_persona', $docente)
                              ->select('a.nombre_area', 'g.nombre_grado', 'c.nombre_carrera', 'n.nombre_nivel', 'p.nombre_plan', 'j.nombre_jornada', 'ASIGNACION_DOCENTE.id_asignacion_docente', 'ASIGNACION_DOCENTE.estado_asignacion_docente')
                              ->get();
    }

    /*
     Metodo para cambiar el estado de una asignacion de un cursoa al docente Habilitado/deshabilitado
    */
    public static function stateAsignacionDocente($estado, $id)
    {
      return DB::update('UPDATE ASIGNACION_DOCENTE set estado_asignacion_docente = ? where id_asignacion_docente = ?', array($estado, $id));
    }
}
