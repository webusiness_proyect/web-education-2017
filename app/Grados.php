<?php

namespace education;

use Illuminate\Database\Eloquent\Model;
use DB;
class Grados extends Model
{
    protected $table = 'GRADOS';

    /*
      Metodo para agregar un nuevo registro a la base de datos
    */
    public static function setGrado($nombre)
    {
      return DB::select('CALL nuevo_grado(?, ?)', array($nombre, date('Y-m-d')));
    }

    /*
      Metodo para buscar los datos de un grado por su id
    */
    public static function findGrado($id)
    {
      return Grados::where('id_grado', $id)->first();
    }

    /*
      Metodo para actualizar los datos de un grado
    */
    public static function updateGrado($id, $nombre)
    {
      return DB::select('CALL actualizar_grado(?, ?)', array($id, $nombre));
    }

    /*
      Metodo para cambiar el estado de un grado
    */
    public static function stateGrado($id, $estado)
    {
      return DB::select('CALL estado_grado(?, ?)', array($id, $estado));
    }
}
