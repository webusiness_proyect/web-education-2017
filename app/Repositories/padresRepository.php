<?php



namespace education\Repositories;







use DB;
use education\Tutor;



/**



 * Clase repositorio para el tratamiento de datos para el docente



 */



class padresRepository



{



    public $id_persona;

    public $id_estudiante;

    public $id_usuario;

    public $id_tutor;

    public $nombre_tutor;

    public $apellidos_tutor;

    public $direccion_tutor;

    public $telefono_primario_tutor;

    public $empresa_telefono_tutor;

    public $correo_electronico_tutor;

    public $cui_tutor;

    public $lugar_trabajo_tutor;

    public $direccion_trabajo_tutor;

    public $telefono_trabajo_tutor;

    public $cargo_tutor;

    public $parentesco_tutor;






    //metodo para obtener todo los cursos que tiene asignado un docente



    public function getAlumnos()



    {



        return DB::table('ESTUDIANTES AS E')



                 ->join('TUTOR_ESTUDIANTE AS TE', 'E.id_estudiante', '=', 'TE.id_estudiante')



                 ->join('TUTOR AS T', 'TE.id_tutor', '=', 'T.id_tutor')



                 ->join('users as U', 'T.id', '=', 'U.id')



                 ->join('NIVELES_GRADOS AS NG', 'E.id_nivel_grado', '=', 'NG.id_nivel_grado')



                 ->join('GRADOS AS G', 'NG.id_grado', '=', 'G.id_grado')



                 ->join('NIVELES_PLANES_JORNADAS AS NPJ', 'NG.id_nivel_plan_jornada', '=', 'NPJ.id_nivel_plan_jornada')





                 ->join('NIVELES AS N', 'NPJ.id_nivel', '=', 'N.id_nivel')



                 ->join('JORNADAS AS J', 'NPJ.id_jornada', '=', 'J.id_jornada')



                 ->join('PLANES AS PL', 'NPJ.id_plan', '=', 'PL.id_plan')



                 ->join('SECCIONES AS S', 'NG.id_seccion', '=', 'S.id_seccion')



                 ->join('CARRERAS AS CA', 'NG.id_carrera', '=', 'CA.id_carrera')



                 ->where('U.id',$this->id_usuario)



                 ->select('E.nombre_estudiante', 'E.apellidos_estudiante', 'NG.cuota_inscripcion', 'NG.cuota_mensualidad', 'G.nombre_grado', 'N.nombre_nivel', 'J.nombre_jornada', 'PL.nombre_plan', 'CA.nombre_carrera', 'S.nombre_seccion', 'E.id_estudiante','E.id')



                 ->get();

                 /* QUERY QUE SELECCIONA LOS DATOS DE TOSO LOS ALUMNOS AL CARGO DEL TUTOR

SELECT * FROM ESTUDIANTES AS E JOIN TUTOR_ESTUDIANTE AS TE ON E.id_estudiante=TE.id_estudiante

JOIN TUTOR AS T ON TE.id_tutor=T.id_tutor

JOIN users as U ON T.id=u.id

JOIN NIVELES_GRADOS AS NG ON E.id_nivel_grado=NG.id_nivel_grado

JOIN GRADOS AS G ON NG.id_grado=G.id_grado

JOIN NIVELES_PLANES_JORNADAS AS NPJ ON NG.id_nivel_plan_jornada=NPJ.id_nivel_plan_jornada

JOIN NIVELES AS N ON NPJ.id_nivel=N.id_nivel

JOIN JORNADAS AS J ON NPJ.id_jornada=J.id_jornada

JOIN PLANES AS PL ON NPJ.id_plan=PL.id_plan

JOIN SECCIONES AS S ON NG.id_seccion=S.id_seccion

JOIN CARRERAS AS CA ON NG.id_carrera=CA.id_carrera

WHERE U.id=394

*/



    }



     public function getDocentes()



    {



        return DB::table('PERSONAS AS P')



                 ->join('ASIGNACION_DOCENTE AS AD', 'P.id_persona', '=', 'AD.id_persona')



                 ->join('ASIGNACION_AREAS AS AA', 'AD.id_asignacion_area', '=', 'AA.id_asignacion_area')



                 ->join('NIVELES_GRADOS AS NG', 'AA.id_nivel_grado', '=', 'NG.id_nivel_grado')



                 ->join('ESTUDIANTES AS E', 'E.id_nivel_grado', '=', 'NG.id_nivel_grado')



                 ->join('TUTOR_ESTUDIANTE AS TE', 'E.id_estudiante', '=', 'TE.id_estudiante')



                 ->join('TUTOR AS TU', 'TE.id_tutor', '=', 'TU.id_tutor')



                 ->join('users AS U', 'TU.id', '=', 'U.id')






                 ->where('U.id',$this->id_usuario)



                 ->select(DB::raw('DISTINCT P.nombres_persona'), 'P.apellidos_persona', 'P.id_persona', 'U.id')



                 ->get();

                

/* QUERY QUE MUESTRA TODOS LOS PROFESORES QUE TIENE ASIGNADOS MI HIJO



SELECT distinct(P.nombres_persona), P.apellidos_persona FROM PERSONAS AS P JOIN ASIGNACION_DOCENTE AS AD ON P.id_persona=AD.id_persona

JOIN ASIGNACION_AREAS AS AA ON AD.id_asignacion_area=AA.id_asignacion_area

JOIN NIVELES_GRADOS AS NG ON AA.id_nivel_grado=NG.id_nivel_grado

JOIN ESTUDIANTES AS E ON E.id_nivel_grado=NG.id_nivel_grado

JOIN TUTOR_ESTUDIANTE AS TE ON E.id_estudiante=TE.id_estudiante

JOIN TUTOR AS TU ON TE.id_tutor=TU.id_tutor

JOIN users AS U ON TU.id=U.id

WHERE U.id=335

*/
}



    public function findTutorEstudiante()

    {

         return Tutor::join('TUTOR_ESTUDIANTE as TU','TUTOR.id_tutor','=','TU.id_tutor')
          ->select('TUTOR.nombre_tutor','TUTOR.apellidos_tutor','TUTOR.correo_electronico_tutor','TUTOR.cui_tutor','TUTOR.telefono_primario_tutor','TUTOR.lugar_trabajo_tutor','TUTOR.telefono_trabajo_tutor','TUTOR.id_tutor','TUTOR.estado_tutor')
         ->where('TU.id_estudiante', $this->id_estudiante)
          
         ->get();
         /*
          return DB::table('TUTOR as t')
         ->join('TUTOR_ESTUDIANTE as TU','t.id_tutor','=','TU.id_tutor')
         ->where('TU.id_estudiante', $this->id_estudiante)


        ->select('t.nombre_tutor','t.apellidos_tutor','t.correo_electronico_tutor','t.cui_tutor','t.telefono_primario_tutor','t.lugar_trabajo_tutor','t.telefono_trabajo_tutor','t.id_tutor','t.estado_tutor')

         ->first();
         /*

      QUERY QUE MUESTRA TODOS LOS PADRES DE UN HIJO

SELECT * 
FROM TUTOR AS T
JOIN TUTOR_ESTUDIANTE AS TU ON T.id_tutor = TU.id_tutor
WHERE TU.id_estudiante =6
*

    



      QUERY QUE MUESTRA TODOS LOS PADRES DE UN HIJO

SELECT * 
FROM TUTOR AS T
JOIN TUTOR_ESTUDIANTE AS TU ON T.id_tutor = TU.id_tutor
WHERE TU.id_estudiante =6
*/

    




    }



  public function updateTutores()

  {

    /*
    UPDATE  TUTOR SET  `nombre_tutor` =  'Maria Meliza',
`apellidos_tutor` =  'Fernandez Santillana',
`direccion_tutor` =  'Guatemala, Guatemala',
`telefono_primario_tutor` =  '30154416',
`empresa_telefono_tutor` =  'movistar',
`correo_electronico_tutor` =  'mgonzalez@gmail.com',
`cui_tutor` =  '4512457845124',
`lugar_trabajo_tutor` =  'Banco Gyt',
`direccion_trabajo_tutor` =  '6a. Avenida 6ta calle zona 9',
`telefono_trabajo_tutor` =  '45127845',
`empresa_telefono_trabajo` =  'movistar',
`cargo_tutor` =  'Gerente',
`parentesco_tutor` =  'Madre' WHERE  `TUTOR`.`id_tutor` =9;

*/

   DB::table('TUTOR')->where('id_tutor', $this->id_tutor)

            ->update([

                   'apellidos_tutor'=>$this->apellidos_tutor,

                  'nombre_tutor'=>$this->nombre_tutor,

                  'direccion_tutor'=>$this->direccion_tutor,

                  'telefono_primario_tutor'=>$this->telefono_primario_tutor,

                  'empresa_telefono_tutor'=>$this->empresa_telefono_tutor,

                  'correo_electronico_tutor'=>$this->correo_electronico_tutor,

                  'cui_tutor'=>$this->cui_tutor,

                  'lugar_trabajo_tutor'=>$this->lugar_trabajo_tutor,

                  'direccion_trabajo_tutor'=>$this->direccion_trabajo_tutor,

                  'telefono_trabajo_tutor'=>$this->telefono_trabajo_tutor,

                  'empresa_telefono_trabajo'=>$this->empresa_telefono_trabajo,

                  'cargo_tutor'=>$this->cargo_tutor,

                  'parentesco_tutor'=>$this->parentesco_tutor

            ]);

  }


    public  function findTutorById()

    {

         return Tutor::where('id_tutor', $this->id_tutor)->first();
         
         /*

      QUERY QUE MUESTRA TODOS LOS PADRES DE UN HIJO

SELECT * 
FROM TUTOR AS T
JOIN TUTOR_ESTUDIANTE AS TU ON T.id_tutor = TU.id_tutor
WHERE TU.id_estudiante =6
*/

    }







}



