<?php

namespace education\Repositories;

use education\Foros;
use DB;
/**
 * Clase repositorio para el manejo de datos de la tabla Foros
 */
class forosRepository
{
  public $id_foro;
  public $id_asignacion_area;
  public $titulo;
  public $mensaje;
  public $fecha;
  public $id_usuario;

    //metodo para buscar todos los foros disponibles en la plataforma
  public function getForos($value='')
  {
    # code...
  }

  //metodo para buscar los foros de acuerdo a un curso
  public function getForosCurso()
  {
    //return Foros::where('id_asignacion_area', $this->id_asignacion_area)->get();

     return DB::table('FOROS as f')

    

     ->join('ASIGNACION_AREAS as aa','aa.id_asignacion_area','=','f.id_asignacion_area')

     ->join('AREAS as a','a.id_area','=','aa.id_area')

     ->join('users as u','f.id_usuario','=','u.id')

    ->join('role_user as ru', 'ru.user_id', '=', 'u.id')

     ->join('roles as r', 'r.id', '=', 'ru.role_id')

     ->select('f.id_foro','f.titulo_foro','f.mensaje_foro','f.fecha_foro','f.id_usuario','a.nombre_area','u.name as nombre','r.name as rol')


    ->where('f.id_asignacion_area',$this->id_asignacion_area)

    ->get();
  }

  /*
      Metodo para registrar un nuevo foro en el sistema
  */
  public function setForo()
  {
    DB::table('FOROS')->insert([
                                  'id_usuario'=>$this->id_usuario,
                                  'titulo_foro'=>$this->titulo,
                                  'mensaje_foro'=>$this->mensaje,
                                  'fecha_foro'=>$this->fecha = date('Y-m-d H:i:s'),
                                  'id_asignacion_area'=>$this->id_asignacion_area
                                ]);
  }

  /*
      Metodo para obtener los datos de un foro por medio de su id
  */
  public function getForoById()
  {
    //return Foros::where('id_foro', $this->id_foro)->first();

     return DB::table('FOROS as f')

     ->join('users as u', 'f.id_usuario', '=', 'u.id')


     ->join('role_user as ru', 'ru.user_id', '=', 'u.id')

     ->join('roles as r', 'r.id', '=', 'ru.role_id')

     ->join('ASIGNACION_AREAS as aa','aa.id_asignacion_area','=','f.id_asignacion_area')

     ->join('AREAS as a','a.id_area','=','aa.id_area')

     ->select('f.id_foro','f.titulo_foro','f.mensaje_foro','f.fecha_foro','f.id_usuario','u.name as nombre','r.name as rol', 'a.nombre_area')


    ->where('id_foro',$this->id_foro)

    ->first();
  }

  //metodo para actualizar los datos de un foro
  public function updateForo()
  {
    DB::table('FOROS')->where('id_foro', $this->id_foro)
                      ->update([
                                'titulo_foro'=>$this->titulo,
                                'mensaje_foro'=>$this->mensaje,
                              ]);
  }

}
