<?php

namespace education\Repositories;



use education\RespondeActividad;

use education\actividad;

use education\Estudiantes;

use education\file;





use DB;

/**

 * Clase repositorio para tratar los datos de la tabla respuetas de tareas

 */

class RespondeActividadesRepository

{

  public $id;

  public $id_respuesta_actividad;

  public $id_actividad;

  public $id_usuario;

  public $estado_entrega_actividad;

  public $calificacion;

  public $fecha_entrega;

  public $comentarios;

  public $observaciones_maestro;

  public $ruta_actividad;

  public $area;

  public $unidad;

  public $valor_max;

  public $title;

  public $name;

  public $size;

  public $type;

  public $url;

  public $nota;



  /*

      Metodo para registrar una nueva unidad

  */

  public function setRespondeActividad($val_max,$area)

  {

    DB::table('RESPUESTA_ACTIVIDADES')->insert([
                        

                                   'id_actividad'=>$this->id_actividad,

                                  'id_usuario'=>$this->id_usuario,

                                  'comentarios'=>$this->comentarios,

                                  'ruta_actividad'=>$this->ruta_actividad,

                                  'unidad'=>$this->unidad,

                                  'area'=>$area,

                                  'valor_max'=>$val_max,




                                    ]);

  }

  public function setTareaFile($val_max,$area,$fsize,$ftype,$urlimg)

  {

    DB::table('files')->insert([

                                 

                                   'id_actividad'=>$this->id_actividad,

                                  'id_usuario'=>$this->id_usuario,

                                  //'comentarios'=>$this->comentarios,

                                  //'ruta_actividad'=>$this->ruta_actividad,

                                  'unidad'=>$this->unidad,

                                  'area'=>$this->area,

                                  'valor_max'=>$val_max,

                                  'title'=>$this->$comentarios,

                                  'name'=>$urlimg,

                                  'size'=>$fsize,

                                  'type'=>$ftype,




                                    ]);

  }



   



  /*

      Metodo para ver si exite una respuesta de actividad con el nombre especificado

  */

  public function existRespuestaActividades($id_actividad,$id_usuario,$unidades,$areas)

  {


    return RespondeActividad::where('id_actividad',$id_actividad)
              ->where('id_usuario',$id_usuario)
              ->where('unidad', $unidades)
              ->where('area',$areas)
              ->first();

      

              //SELECT count(*) FROM `respuesta_actividades` WHERE id_actividad=1 and id_usuario=17 and unidad=5 and area=3

  }

  public function existRespuestaActividadesFiles($id_actividad,$id_usuario,$unidades,$areas)

  {


    return file::where('id_actividad',$id_actividad)
              ->where('id_usuario',$id_usuario)
              ->where('unidad', $unidades)
              ->where('area',$areas)
              ->first();

      

              //SELECT count(*) FROM `respuesta_actividades` WHERE id_actividad=1 and id_usuario=17 and unidad=5 and area=3

  }

  public function valorMaximoNota()
  {
/*
      return DB::table('actividades')

              ->where('id_actividad',$id)
              ->select('nota_total')


              ->get();*/

              return actividad::where('id_actividad', $this->id_actividad)->select('nota_total')->get();

    //SELECT * FROM `actividades` where id_actividad=3
  }

  public function valorMaximoNota2($id_actividad)
  {
/*
      return DB::table('actividades')

              ->where('id_actividad',$id)
              ->select('nota_total')


              ->get();*/

              return actividad::where('id_actividad', $id_actividad)->select('nota_total')->first();;

    //SELECT * FROM `actividades` where id_actividad=3
  }


  /*

      Metodo para buscar todas las respuesta de actividad  regitradas

  */

  public function getRespuestaActividades()

  {

    return RESPUESTA_ACTIVIDADES::All();

  }



  public function getUsuariosRespuestActividades()

  {

    return RespondeActividad::join('users as u', 'u.id', '=', 'RESPUESTA_ACTIVIDADES.id_usuario')

             ->where('RESPUESTA_ACTIVIDADES.id_actividad', $this->id_actividad)

             ->select('RESPUESTA_ACTIVIDADES.id_respuesta_actividad', 'RESPUESTA_ACTIVIDADES.estado_entrega_actividad','RESPUESTA_ACTIVIDADES.calificacion','RESPUESTA_ACTIVIDADES.fecha_entrega','RESPUESTA_ACTIVIDADES.comentarios','u.name')

             ->get();

  }



  public function getUsuariosRespuestActividadesFiles()

  {

    return file::join('users as u', 'u.id', '=', 'files.id_usuario')

            ->join('ESTUDIANTES as e','e.id','=','u.id')

             ->where('files.id_actividad', $this->id_actividad)

             ->select('e.nombre_estudiante','e.apellidos_estudiante','files.id as id_respuesta_actividad', 'files.estado_entrega_actividad','files.calificacion','files.fecha_entrega','files.title as comentarios', 'files.name as url','u.name')

             ->get();

             /*
             //seleccionar todos los usuarios que tienen asignada una tarea de una actividad y de un grado en comun

                SELECT * FROM ESTUDIANTES AS E JOIN NIVELES_GRADOS AS NG ON E.id_nivel_grado=NG.id_nivel_grado
                JOIN GRADOS AS G ON NG.id_grado=G.id_grado
                JOIN ASIGNACION_AREAS AS AA ON AA.id_nivel_grado=NG.id_nivel_grado
                JOIN ACTIVIDADES AS ACT ON ACT.id_area=AA.id_area
                JOIN files as F ON ACT.id_actividad=F.id_actividad
                WHERE E.id_nivel_grado=1 and ACT.id_actividad=13 
                */

  }




  /*

      Metodo para obtener los datos de una respuesta de actividad por su id de actividad

  */

  public function getRespuestaActividadesById()

  {

    return DB::table('RESPUESTA_ACTIVIDADES') 

    ->where('id_actividad', $this->id_actividad)

    ->get();

  }

   public function getNotaMaxRespuestaActividadesById($id_act)

  {

    return DB::table('RESPUESTA_ACTIVIDADES') 

    ->where('id_actividad', $id_act)

    ->first();

  }




  /*



 public function getRespuestaActividadesById()

  {

    return DB::table('RESPUESTA_ACTIVIDADES') 

    ->where('id_actividad', $this->id_actividad)

    ->get();

  }

*/



  //funcion para traer todas las respuetas para modificar

   public function getRespuestaActividadesById1()

  {

   return RespondeActividad::where('id_respuesta_actividad', $this->id_respuesta_actividad)->first();

   }

    public function getRespuestaActividadesById1File()

  {

   return file::where('id', $this->id)->first();

   }

   public function getRespuestasbyUser()
   {
    return file::where('id', $this->id);
   }


  /*

      Metodo para actualizar los datos de una unidad

  */

  public function updateRespuestaActividades()

  {

   DB::table('RESPUESTA_ACTIVIDADES')->where('id_respuesta_actividad', $this->id_respuesta_actividad)

            ->update([

                   'estado_entrega_actividad'=>$this->estado_entrega_actividad,

                   //'fecha_calificacion'=>$this->fecha_calificacion,

                  'calificacion'=>$this->calificacion,

                  'observaciones_maestro'=>$this->observaciones_maestro,

            ]);

  }


  public function updateRespuestaActividadesFile()

  {

   DB::table('files')->where('id', $this->id)

            ->update([

                   'estado_entrega_actividad'=>$this->estado_entrega_actividad,

                   //'fecha_calificacion'=>$this->fecha_calificacion,

                  'calificacion'=>$this->calificacion,

                  'observaciones_maestro'=>$this->observaciones_maestro,

            ]);

  }

  public function getIdActividad()
  {

 
             
             return actividad::where('id_actividad', '=', $this->id_actividad)

             

                   ->first();




  }

 public function getIdRespuestaActividad()
  {

 
             
             return RespondeActividad::where('id_actividad', '=', $this->id_respuesta_actividad)

             

                   ->first();




  }

  public function getIdRespuestaActividadFile()
  {

 
             
             return file::where('id_actividad', '=', $this->id)

             

                   ->first();




  }

  /*

  update respuesta_actividades set estado_entrega_actividad="CALIFICADO", fecha_calificacion="20-3-2016", calificacion=10, observaciones_maestro="EXCELENTE TRABAJO" where id_respuesta_actividad=1

  */



 //traigo un valor si la nota que estoy asignando es menor a la nota total asignada

  public function getValidarNotaIngresada($id_respuesta,$nota)
  {

     return RespondeActividad::where('id_respuesta_actividad', $id_respuesta)
     ->where('valor_max','>=',$nota)
     ->first();
     





    //-- devuelve un valor nulo SELECT * FROM `respuesta_actividades` WHERE id_respuesta_actividad=14 and valor_max>21
  }


//funcion que muestra  en detalle el punteo de todas las actividades hechar por un usuario en un area
public function getDetalleNota()
{




                return DB::table('ESTUDIANTES as e')

                  ->join('RESPUESTA_ACTIVIDADES as ra', 'e.id','=','ra.id_usuario')

                 ->where('ra.id_usuario', $this->id_usuario)

                 ->where('ra.unidad', $this->unidad)

                 ->where('ra.area', $this->area)

                 ->select( 'e.nombre_estudiante', 'e.apellidos_estudiante', 'ra.calificacion')

                 
                 ->get();
  /*
  select e.nombre_estudiante, e.apellidos_estudiante, sum(ra.calificacion) as nota from estudiantes as e 
  join respuesta_actividades as ra on e.id=ra.id_usuario where ra.id_usuario=17 and ra.unidad=5 and ra.area=3
  */
}

//funcion que muestra  en detalle el punteo de todas las actividades hechar por un usuario en un area





  

}

