<?php
namespace education\Repositories;

use education\Unidades;
use DB;
/**
 * Clase repositorio para tratar los datos de la tabla Unidades
 */
class unidadesRepository
{
  public $id;
  public $nombre;
  public $fecha_inicio;
  public $fecha_final;
  public $estado;

  /*
      Metodo para registrar una nueva unidad
  */
  public function setUnidad()
  {
    DB::table('UNIDADES')->insertGetId([
                                  'nombre_unidad'=>$this->nombre,
                                  'fecha_inicio'=>$this->fecha_inicio,
                                  'fecha_final'=>$this->fecha_final,
                                  'estado_unidad'=>$this->estado = true
                                    ]);
  }

  /*
      Metodo para ver si exite una unidad con el nombre especificado
  */
  public function existUnidad()
  {
    return DB::table('UNIDADES')
             ->where('nombre_unidad',$this->nombre)
             ->count();
  }

  /*
      Metodo para buscar todas las unidades regitradas
  */
  public function getUnidades()
  {
    return Unidades::All();
  }

  /*
      Metodo para obtener los datos de una unidad por su id
  */
  public function getUnidadById()
  {
    return Unidades::where('id_unidad', $this->id)->first();
  }

  /*
      Metodo para actualizar los datos de una unidad
  */
  public function updateUnidad()
  {
    Unidades::where('id_unidad', $this->id)
            ->update([
                  'nombre_unidad'=>$this->nombre,
                  'fecha_inicio'=>$this->fecha_inicio,
                  'fecha_final'=>$this->fecha_final
            ]);
  }

  /*
      Metodo para buscar las unidad actual
  */
  public function getUnidadActual()
  {
    return Unidades::where('fecha_inicio', '<=', $this->fecha_inicio)
                   ->where('fecha_final', '>=', $this->fecha_final)
                   ->first();
  }
}
