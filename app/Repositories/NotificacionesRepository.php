<?php

namespace education\Repositories;



use education\Roles;
use education\Usuario_persona;
use education\Asignacion_docente;
use DB;

/**

 * Clase repositorio para el tratamiento de datos de la tabla roles

 */

class NotificacionesRepository

{

  

    public $usuario;

    public $roles;

    public $persona;

    public $nivelgrado;

    public $titulo_notificacion ;

    public $informacion_notificacion ;

    public $id_notificacion;




    public function findmyRolWhere()

    {

      return Roles::join('role_user as ru', 'roles.id', '=', 'ru.role_id')


                 ->where('ru.user_id',$this->usuario)

                 
                 ->get();
                 /*

                 return DB::table('roles as r')

                ->join('role_user as ru', 'r.id', '=', 'ru.role_id')

                 ->where('ru.user_id', '=', $this->usuario)

                 ->select('r.id')
                 
                 ->first();
                 */

      /* query que trae todos los roles por usuario
      select * from roles as r join role_user as ru on r.id=ru.role_id
        where ru.user_id=22
        */


    }

     public function findmyPersonaWhere()

    {
      
      return Usuario_persona::where('USUARIO_PERSONA.user_id',$this->usuario)

                  ->select('USUARIO_PERSONA.user_id as id')

     
                 ->get();/*

                 return DB::table('USUARIO_PERSONA')

                 ->where('user_id', '=', $this->usuario)

                 ->select('id_persona')

                                
                 ->first();*/

      /* query que trae mi id de persona
      SELECT * FROM `usuario_persona` WHERE user_id=22
        */


    }


     public function findmyNivelGradoWhere()

    {
      /*

      return Asignacion_docente::join('ASIGNACION_AREAS as aa', 'ASIGNACION_DOCENTE.id_asignacion_area', '=', 'aa.id_asignacion_area')

                 ->where('ASIGNACION_DOCENTE.id_persona',$this->persona)

                   ->select('aa.id_nivel_grado as nivel')

                 ->groupBy('aa.id_nivel_grado')

               

                 
                 ->get();*/

                 
                 return DB::table('ASIGNACION_DOCENTE as ad')

                ->join('ASIGNACION_AREAS as aa', 'ad.id_asignacion_area', '=', 'aa.id_asignacion_area')

                 ->where('ad.id_persona', '=', $this->persona)

                

                 ->select('aa.id_nivel_grado')
                 ->groupBy('aa.id_nivel_grado')
                 
                 ->get();
                 


      /* query que trae el id_nivel grado asignadas por docente
      SELECT * FROM asignacion_docente as ad join asignacion_areas as aa on ad.id_asignacion_area=aa.id_asignacion_area WHERE ad.id_persona=2
    group by aa.id_nivel_grado
        */


    }




  //metodo para actualizar los datos del mensaje enviado


  public function updatenotificacion()


  {


    DB::table('NOTIFICACIONES')->where('id_notificacion', $this->id_notificacion)


                      ->update([

                                  

                                  'titulo_notificacion'=>$this->titulo_notificacion,

                                  'informacion_notificacion'=>$this->informacion_notificacion,



                              ]);

                      /* query que modifica los datos del mensaje enviado
                      UPDATE NOTIFICACIONES SET titulo_notificacion =  'mi titulo',
                      informacion_notificacion =  "nueva informacion" WHERE id_notificacion =292162
                        */

  }

    public function findmyNotificacionWhere()

    {

      return DB::table('NOTIFICACIONES as n')

                ->join('users as u', 'n.user', '=', 'u.id')

                 ->where('n.id_nivel_grado', $this->nivelgrado)

                 ->where('n.id_rol',$this->roles)


                 ->select('n.id_notificacion','n.titulo_notificacion','n.informacion_notificacion','n.id_nivel_grado','n.fecha_create','u.name')

                 
                 ->get();

      /* query que trae el id_nivel grado asignadas por docente
      SELECT * FROM NOTIFICACIONES as n join users as u on n.user=u.id WHERE id_nivel_grado=16 and id_rol=2
        */


    }

    public function findmyNotificacionById($id)

    {

      return DB::table('NOTIFICACIONES as n')

               

                 ->where('n.id_notificacion', $id)

                

                 ->select('n.id_notificacion','n.titulo_notificacion','n.informacion_notificacion','n.id_nivel_grado','n.fecha_create')

                 
                 ->first();

      /* query que trae el id_nivel grado asignadas por docente
      SELECT * FROM NOTIFICACIONES as n join users as u on n.user=u.id WHERE id_nivel_grado=16 and id_rol=2
        */


    }




}

