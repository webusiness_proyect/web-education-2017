<?php





namespace education\Repositories;





use education\actividad;


use DB;


/**


 * Clase repositorio para el manejo de datos de la tabla Foros


 */


class actividadesRepository


{


  public $id_actividad;


  public $id_usuario;


  public $id_asignacion_area;


  public $id_unidad;


  public $id_area;


  public $id_tipo_actividad;


  public $nombre_actividad;


  public $descripcion_actividad;  


  public $fecha_entrega;


  public $nota_total;


  





    //metodo para buscar todos los foros disponibles en la plataforma


  public function getActividad($value='')


  {


    # code...


  }






  public function getActividadesThisunidad()


  {


    return DB::table('ACTIVIDADES as a')

     ->join('UNIDADES as un','a.id_unidad','=','un.id_unidad')


     ->join('users as u','a.id_usuario','=','u.id')

    ->join('role_user as ru', 'ru.user_id', '=', 'u.id')

     ->join('roles as r', 'r.id', '=', 'ru.role_id')

     ->join('TIPO_ACTIVIDAD as ta', 'ta.id_tipo_actividad', '=', 'a.id_tipo_actividad')

    
     ->select('un.nombre_unidad','a.id_actividad','a.id_usuario','a.id_asignacion_area','a.id_area','a.id_unidad','a.id_tipo_actividad','a.nombre_actividad','a.descripcion_actividad','a.fecha_inicio','a.fecha_entrega','a.nota_total','a.nombre_recurso','a.url_recursos','u.name as nombre','r.name as rol','ta.nombre_actividad')


    ->where('a.id_area',$this->id_area)

     ->where('a.id_unidad',$this->id_unidad)


    ->get();



    /* QUERY QUE MUESTRA TODAS LAS ACTIVIDADES POR CALIFICAR
    SELECT a.id_actividad,a.id_usuario,a.id_asignacion_area,a.id_area,a.id_unidad,a.id_tipo_actividad,a.nombre_actividad,a.descripcion_actividad,a.fecha_inicio,a.fecha_entrega, a.nota_total,a.nombre_recurso,a.url_recursos,u.name as nombre,r.name as rol,ta.nombre_actividad
 FROM ACTIVIDADES as a

     join users as u ON a.id_usuario=u.id

    join role_user as ru ON ru.user_id=u.id

     join roles as r ON r.id=ru.role_id

     join TIPO_ACTIVIDAD as ta ON ta.id_tipo_actividad=a.id_tipo_actividad

     

    where a.id_area=13
    */


  }


  //metodo para buscar todas las actividades creadas en esta area


  public function getActividadCurso()


  {




    return DB::table('ACTIVIDADES as a')

    ->join('UNIDADES as un','a.id_unidad','=','un.id_unidad')

     ->join('users as u','a.id_usuario','=','u.id')

    ->join('role_user as ru', 'ru.user_id', '=', 'u.id')

     ->join('roles as r', 'r.id', '=', 'ru.role_id')

     ->join('TIPO_ACTIVIDAD as ta', 'ta.id_tipo_actividad', '=', 'a.id_tipo_actividad')

     ->join('files as f', 'f.id_actividad', '=', 'a.id_actividad')

     ->select('un.nombre_unidad','a.id_actividad','a.id_usuario','a.id_asignacion_area','a.id_area','a.id_unidad','a.id_tipo_actividad','a.nombre_actividad','a.descripcion_actividad','a.fecha_inicio','a.fecha_entrega','a.nota_total','a.nombre_recurso','a.url_recursos','u.name as nombre','r.name as rol','ta.nombre_actividad','f.estado_entrega_actividad','f.calificacion')

 

    ->where('a.id_area',$this->id_area)



   



    ->get();

    /* QUERY QUE MUESTRA TODAS LAS ACTIVIDADES POR CALIFICAR
    SELECT a.id_actividad,a.id_usuario,a.id_asignacion_area,a.id_area,a.id_unidad,a.id_tipo_actividad,a.nombre_actividad,a.descripcion_actividad,a.fecha_inicio,a.fecha_entrega, a.nota_total,a.nombre_recurso,a.url_recursos,u.name as nombre,r.name as rol,ta.nombre_actividad
 FROM ACTIVIDADES as a

     join users as u ON a.id_usuario=u.id

    join role_user as ru ON ru.user_id=u.id

     join roles as r ON r.id=ru.role_id

     join TIPO_ACTIVIDAD as ta ON ta.id_tipo_actividad=a.id_tipo_actividad

     

    where a.id_area=13
    */


  }
//actividades de unidades anteriores


  public function getActividadCursoUnidadAnterior()


  {


    return DB::table('ACTIVIDADES as a')

    ->join('UNIDADES as un','a.id_unidad','=','un.id_unidad')

     ->join('users as u','a.id_usuario','=','u.id')

    ->join('role_user as ru', 'ru.user_id', '=', 'u.id')

     ->join('roles as r', 'r.id', '=', 'ru.role_id')

     ->join('TIPO_ACTIVIDAD as ta', 'ta.id_tipo_actividad', '=', 'a.id_tipo_actividad')

     ->join('files as f', 'f.id_actividad', '=', 'a.id_actividad')

     ->select(DB::raw('DISTINCT a.id_actividad'),'un.nombre_unidad','a.id_usuario','a.id_asignacion_area','a.id_area','a.id_unidad','a.id_tipo_actividad','a.nombre_actividad','a.descripcion_actividad','a.fecha_inicio','a.fecha_entrega','a.nota_total','a.nombre_recurso','a.url_recursos','u.name as nombre','r.name as rol','ta.nombre_actividad','f.estado_entrega_actividad','f.calificacion')

 

    ->where('a.id_area',$this->id_area)

     ->whereNotIn('a.id_unidad', [$this->id_unidad])
    

   


    ->orderBy('a.id_actividad', 'desc')




    ->get();
    

    /* QUERY QUE MUESTRA TODAS LAS ACTIVIDADES POR CALIFICAR
    SELECT a.id_actividad,a.id_usuario,a.id_asignacion_area,a.id_area,a.id_unidad,a.id_tipo_actividad,a.nombre_actividad,a.descripcion_actividad,a.fecha_inicio,a.fecha_entrega, a.nota_total,a.nombre_recurso,a.url_recursos,u.name as nombre,r.name as rol,ta.nombre_actividad
 FROM ACTIVIDADES as a

     join users as u ON a.id_usuario=u.id

    join role_user as ru ON ru.user_id=u.id

     join roles as r ON r.id=ru.role_id

     join TIPO_ACTIVIDAD as ta ON ta.id_tipo_actividad=a.id_tipo_actividad

     

    where a.id_area=13
    */


  }


  public function getTotalActividadCurso()


  {


    return DB::table('ACTIVIDADES as a')

     ->join('users as u','a.id_usuario','=','u.id')

    ->join('role_user as ru', 'ru.user_id', '=', 'u.id')

     ->join('roles as r', 'r.id', '=', 'ru.role_id')

     ->join('TIPO_ACTIVIDAD as ta', 'ta.id_tipo_actividad', '=', 'a.id_tipo_actividad')

     ->select('a.id_actividad','a.id_usuario','a.id_asignacion_area','a.id_area','a.id_unidad','a.id_tipo_actividad','a.nombre_actividad','a.descripcion_actividad','a.fecha_inicio','a.fecha_entrega', DB::raw('sum(a.nota_total) as total'),'a.nombre_recurso','a.url_recursos','u.name as nombre','r.name as rol','ta.nombre_actividad')


    ->where('a.id_area',$this->id_area)

     ->where('a.id_unidad',$this->id_unidad)


    ->get();


  }



  public function getActividades()


  {


    return DB::table('ACTIVIDADES as a')

    ->join('files as f', 'a.id_actividad', '=', 'f.id_actividad')

     ->select('a.id_actividad','a.nombre_actividad','a.descripcion_actividad','a.fecha_inicio', 'a.fecha_entrega','a.nota_total','f.estado_entrega_actividad','f.calificacion')


    ->where('a.id_area',$this->id_area)




    ->get();


  }

  //metodo para buscar los foros de acuerdo a un curso


  public function getArea()


  {


    return DB::table('AREAS as a')

    

     ->select('a.nombre_area')


    ->where('a.id_area',$this->id_area)


    ->get();


  }





  public function getMiActividadCurso()


  {

return DB::table('ACTIVIDADES as a')


                 ->join('ASIGNACION_AREAS as aa', 'a.id_asignacion_area', '=', 'aa.id_asignacion_area')


                 ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'aa.id_nivel_grado')

                  ->leftjoin('files as f', 'a.id_actividad', '=', 'f.id_actividad')

                  ->select('a.id_actividad','a.nombre_actividad','a.descripcion_actividad','a.fecha_inicio', 'a.fecha_entrega','a.nota_total','f.estado_entrega_actividad','f.calificacion')


                ->where('a.id_unidad',  $this->id_unidad)                 

                 ->where('f.id_usuario',  $this->id_usuario)

                 ->orderBy('a.nombre_actividad','asc')





                 ->get();
                 /* QUERY QUE MUESTRA TODAS LAS TAREAS ASIGNADAS Y SU ESTADO
                    SELECT * 
                  FROM ACTIVIDADES AS a
                  JOIN ASIGNACION_AREAS AS aa ON a.id_asignacion_area = aa.id_asignacion_area
                  JOIN NIVELES_GRADOS AS ng ON ng.id_nivel_grado = aa.id_nivel_grado
                  LEFT OUTER JOIN files as f on a.id_actividad=f.id_actividad
                  WHERE a.id_area =3 AND f.id_usuario=13
                */





  }



  public function getMiActividadCursoAnteriores()


  {

return DB::table('ACTIVIDADES as a')


                 ->join('ASIGNACION_AREAS as aa', 'a.id_asignacion_area', '=', 'aa.id_asignacion_area')


                 ->join('NIVELES_GRADOS as ng', 'ng.id_nivel_grado', '=', 'aa.id_nivel_grado')

                  ->leftjoin('files as f', 'a.id_actividad', '=', 'f.id_actividad')

                  ->select('a.id_actividad','a.nombre_actividad','a.descripcion_actividad','a.fecha_inicio', 'a.fecha_entrega','a.nota_total','f.estado_entrega_actividad','f.calificacion')


                ->whereNotIn('a.id_unidad', [$this->id_unidad])              

                 ->where('f.id_usuario',  $this->id_usuario)

                 ->where('f.area',  $this->id_area)

                 ->orderBy('a.id_actividad','asc')





                 ->get();
                 /* QUERY QUE MUESTRA TODAS LAS TAREAS ASIGNADAS Y SU ESTADO
                    SELECT * 
                  FROM ACTIVIDADES AS a
                  JOIN ASIGNACION_AREAS AS aa ON a.id_asignacion_area = aa.id_asignacion_area
                  JOIN NIVELES_GRADOS AS ng ON ng.id_nivel_grado = aa.id_nivel_grado
                  LEFT OUTER JOIN files as f on a.id_actividad=f.id_actividad
                  WHERE a.id_area =3 AND f.id_usuario=13
                */





  }


  public function getMiActividadCursounidad()//entregadas


  {

return DB::table('ACTIVIDADES as a')


                  ->join('files as f', 'a.id_actividad', '=', 'f.id_actividad')

                  ->join('UNIDADES as u', 'u.id_unidad', '=', 'a.id_unidad')

                  ->select('u.nombre_unidad','a.id_actividad','a.nombre_actividad','a.descripcion_actividad','a.fecha_inicio', 'a.fecha_entrega','a.nota_total','f.estado_entrega_actividad','f.calificacion')


                 

                                  ->where('f.id_usuario',  $this->id_usuario)

                                  ->where('f.unidad',  $this->id_unidad)
           

                 ->orderBy('a.id_actividad','asc')





                 ->get();
                 /* QUERY QUE MUESTRA TODAS LAS TAREAS ASIGNADAS Y SU ESTADO
                    SELECT * 
                  FROM ACTIVIDADES AS a
                  JOIN files as f on a.id_actividad=f.id_actividad where f.id_usuario=17 and f.unidad=6
                */





  }

//funcion modulo de tareas de alumnos sin entregar por ellos mismos


  public function getMiActividadCursoSinEntregar()


  {


    

return DB::table('ACTIVIDADES as a')

                ->join('UNIDADES as u','a.id_unidad','=','u.id_unidad')

                    ->whereNotExists(function($query) 
                    {
                        $query->from('files as f')
                              ->whereRaw('f.id_actividad = a.id_actividad')
                              
                              ->where('f.id_usuario', '=', $this->id_usuario);
                    })
                    
                    ->select('u.nombre_unidad','a.id_actividad','a.nombre_actividad','a.descripcion_actividad','a.fecha_inicio', 'a.fecha_entrega','a.nota_total')

                    ->where('a.id_area',$this->id_area)

                    ->where('a.id_unidad',$this->id_unidad)

                    ->orderBy('a.id_actividad', 'desc')
                    

                    ->get();

                  
                 


                 /* QUERY QUE selecciona todas las actividades que no
                  han sido entregadas por el alumno en ese curso

                  Select * from ACTIVIDADES as a  
                  where not exists (select * from files as f where f.id_actividad = a.id_actividad and f.Id_usuario=17)  and a.id_area=3 
                */

  }



//funcion modulo de tareas de alumnos sin entregar por ellos mismos


  public function getMiActividadCursoAnterioresSinEntregar()


  {


    

return DB::table('ACTIVIDADES as a')

                ->join('UNIDADES as u','a.id_unidad','=','u.id_unidad')

                    ->whereNotExists(function($query) 
                    {
                        $query->from('files as f')
                              ->whereRaw('f.id_actividad = a.id_actividad')
                              
                              ->where('f.id_usuario', '=', $this->id_usuario);
                              
                    })
                    
                    ->select('u.nombre_unidad','a.id_actividad','a.nombre_actividad','a.descripcion_actividad','a.fecha_inicio', 'a.fecha_entrega','a.nota_total')

                    
                    ->where('a.id_actividad',$this->id_area)

                    ->whereNotIn('a.id_unidad', [$this->id_unidad])   

                    ->orderBy('a.id_actividad', 'desc')
                    

                    ->get();

                  
                 


                 /* QUERY QUE selecciona todas las actividades que no
                  han sido entregadas por el alumno en ese curso

                  Select * from ACTIVIDADES as a  
                  where not exists (select * from files as f where f.id_actividad = a.id_actividad and f.Id_usuario=17)  and a.id_area=3 
                */

  }


//funcion modulo de  listado de alumnos que no han entregado  una determinada tarea de un determinado curso

  public function getTareasSinEntregar()


  {

return DB::table('ESTUDIANTES')

                    ->join('NIVELES_GRADOS as ng', 'ESTUDIANTES.id_nivel_grado', '=', 'ng.id_nivel_grado')

                    ->join('ASIGNACION_AREAS as aa', 'ng.id_nivel_grado', '=', 'aa.id_nivel_grado')

                    


                    ->whereNotExists(function($query) 
                    {
                        $query->from('files as f')
                              ->whereRaw('f.id_usuario =  ESTUDIANTES.id');
                              
                             
                    })
                    
                   
                    ->where('aa.id_asignacion_area',$this->id_area)



                    ->orderBy('ESTUDIANTES.apellidos_estudiante','asc')
                    

                    ->get();

                  
                 


                 /* QUERY muestra los alumnos que no han entregado  una determinada tarea 
                 de un determinado curso

                   

                SELECT * FROM  ESTUDIANTES 
                 
                        join  NIVELES_GRADOS as ng on ESTUDIANTES.id_nivel_grado=ng.id_nivel_grado
                        join ASIGNACION_AREAS as aa on ng.id_nivel_grado=aa.id_nivel_grado

                   where not exists (select * from files as f where f.id_usuario =  ESTUDIANTES.id) AND  aa.id_asignacion_area=13

                    order By ESTUDIANTES.apellidos_estudiante asc

 
                */





  }












  /*


      Metodo para registrar un nuevo foro en el sistema


  */


  public function setActividad()


  {


    DB::table('ACTIVIDADES')->insert([


                                  'id_usuario'=>$this->id_usuario,


                                  'id_asignacion_area'=>$this->id_asignacion_area,


                                  'id_unidad'=>$this->id_unidad,


                                  'id_area'=>$this->id_area,


                                  'id_tipo_actividad'=>$this->id_tipo_actividad,


                                  'nombre_actividad'=>$this->nombre_actividad,


                                  'descripcion_actividad'=>$this->descripcion_actividad,


                                  'nota_total'=>$this->nota_total,


                                  'fecha_entrega'=>$this->fecha_entrega = date('Y-m-d H:i:s')


                                  


                                ]);


  }





  /*


      Metodo para obtener los datos de un foro por medio de su id


  */


  public function getActividadById()


  {


    return actividad::where('id_actividad', $this->id_actividad)->first();


  }





  //metodo para actualizar los datos de un foro


  public function updateActividad()


  {


    DB::table('ACTIVIDADES')->where('id_actividad', $this->id_actividad)


                      ->update([

                                  

                                  'nombre_actividad'=>$this->nombre_actividad,

                                  'descripcion_actividad'=>$this->descripcion_actividad,

                                  'nota_total'=>$this->nota_total,

                                  'fecha_entrega'=>$this->fecha_entrega,


                              ]);


  }

  public function ActividadesConEntregaPorunidad()
  {
    /*query 
    SELECT a.id_actividad,f.id_usuario, f.calificacion,ar.nombre_area,a.id_asignacion_area,a.id_area,a.id_unidad,a.id_tipo_actividad,a.nombre_actividad,a.descripcion_actividad,a.fecha_inicio,a.fecha_entrega, a.nota_total,a.nombre_recurso,a.url_recursos,u.name as nombre,r.name as rol,ta.nombre_actividad
 FROM ACTIVIDADES as a
  join files as f on a.id_actividad=f.id_actividad
    join users as u ON a.id_usuario=u.id
    join role_user as ru ON ru.user_id=u.id
    join roles as r ON r.id=ru.role_id
    join TIPO_ACTIVIDAD as ta ON ta.id_tipo_actividad=a.id_tipo_actividad
    join AREAS as ar on f.area=ar.id_area
    where a.id_area=3
    and a.id_unidad=6
    and f.id_usuario=24
    group by a.id_actividad
    */

    return DB::table('ACTIVIDADES as a')
    ->join('files as f','a.id_actividad','=','f.id_actividad')
    ->join('users as u','a.id_usuario','=','u.id')
    ->join('role_user as ru', 'ru.user_id', '=', 'u.id')
    ->join('roles as r', 'r.id', '=', 'ru.role_id')
    ->join('TIPO_ACTIVIDAD as ta', 'ta.id_tipo_actividad', '=', 'a.id_tipo_actividad')
    ->join('AREAS as ar', 'f.area', '=', 'ar.id_area')
    
    ->where('a.id_area',$this->id_area)
    ->where('a.id_unidad',$this->id_unidad)
    ->where('f.id_usuario',$this->id_usuario)
    ->select('f.id_usuario', 'f.calificacion','ar.nombre_area','a.id_actividad','a.id_usuario','a.id_asignacion_area','a.id_area','a.id_unidad','a.id_tipo_actividad','a.nombre_actividad','a.descripcion_actividad','a.fecha_inicio','a.fecha_entrega','a.nota_total','a.nombre_recurso','a.url_recursos','u.name as nombre','r.name as rol','ta.nombre_actividad')
    ->get();


  }

   public function infoalumno()
  {
    /*query 
    
    */

    return DB::table('AREAS as a')
             ->join('ASIGNACION_AREAS as aa','a.id_area','=','aa.id_area')
             ->join('ESTUDIANTES as e','e.id_nivel_grado','=','aa.id_nivel_grado')
             ->where('e.id','=',  $this->id_usuario)
             ->select('a.nombre_area','a.id_area', 'e.nombre_estudiante', 'e.apellidos_estudiante', 'e.id', 'e.id_nivel_grado')
             ->get();


  }


  





}


