<?php



namespace education\Repositories;



use education\User;

use education\Usuario_estudiante;

use DB;

/**

 * Clase repositorio para los usuarios

 */

class userRepository

{

   public $id;

  public $name;

  public $email;

  public $password;

  public $created_at;

  public $estado_usuario;

  public $rol;

  public $url;

  /*public function forUser(User $user)

  {

      return $user->tasks()

                  ->orderBy('created_at', 'asc')

                  ->get();

  }*/



  public function getUsers()

  {

    return User::select('*')->get();

  }



  //Metodo para registrar a un nuevo usuario

  public function setUsuario()

  {

    return DB::table('users')->insertGetId([

                                          'name'=>$this->name,

                                          'email'=>$this->email,

                                          'password'=>$this->password,

                                          'created_at'=>$this->created_at = date('Y-m-d'),

                                          'estado_usuario'=>$this->estado_usuario = true,

                                          ]);

    //DB::insert('INSERT INTO users(name, email, password, created_at, estado_usuario) VALUES(?, ?, ?, ?, ?)', array($this->name, $this->email, $this->password, $this->created_at = date('Y-m-d H:i:s'), $this->estado_usuario = true));

  }



  //metodo para buscar a un usuario por su correo

  public function findUsuarioWhere()

  {

    return User::where('email', $this->email)->first();

  }

  public function getUsuariosByid()

  {

   return User::where('id', $this->id)->first();

   }

   public function updateRoleUsers()

  {

    //UPDATE `role_user` SET `role_id` = '1' WHERE `role_user`.`id_role_user` = 13;

   DB::table('role_user')->where('user_id', $this->id)

            ->update(['role_id'=>$this->rol]);

  }


   public function updateImgUsers()

  {

    //UPDATE `role_user` SET `role_id` = '1' WHERE `role_user`.`id_role_user` = 13;

   DB::table('users')->where('id', $this->id)

            ->update(['url'=>$this->url]);

  }




}

