<?php



namespace education\Repositories;



use education\Estudiantes;
use education\actividad;
use education\Tutor;
use education\Referencias;
use education\Unidades;
use education\Areas;
use education\file;

use DB;

//use DB;

/**

 * Clase repositorio para los estudiantes

 */

class estudiantesRepository

{

  public $id;

  public $id_estudiante;

  public $id_usuario;

  public $id_nivel_grado;

  public $id_municipio;

  public $nombre;

  public $apellido;

  public $codigo_personal;

  public $fecha_nacimiento;

  public $genero;

  public $direccion;

  public $colonia;

  public $zona;

  public $telefono_casa;

  public $empresa_telefonica;

  public $correo;

  public $enfermedad_padecida;

  public $medicamento_recomendado;

  public $alergico;

  public $hospital;

  public $tipo_sangre;

  public $observaciones;

  public $curso;

  public $nombre_tutor;

  public $unidad;

  public $area;



  /*

      Metodo para obtener todos los estudiantes

  */

  public function getEstudiantes()

  {

    return Estudiantes::select('*')->get();

  }



  /*

      Metodo para buscar a un estudiante por su id

  */

  public function findEstudiante($id)

  {

    return Estudiantes::where('id_estudiante', $id)->first();

  }

  public function findEstudianteById()

  {

    return Estudiantes::where('id_estudiante', $this->$id_estudiante)->first();

  }


  public function findEstudianteByIdUser()

  {

    return Estudiantes::where('id', $this->$id)->select('ESTUDIANTES.estado_estudiante')->first();

  }



  /*

      Metodo para buscar a aun estudiante por sus nombres y apellidos

  */

  public function findEstudianteWhere()

  {

    return Estudiantes::where('nombre_estudiante', $this->nombre)

                      ->where('apellidos_estudiante', $this->apellido)

                      ->get();

  }



  public function setEstudiante()

  {

    return DB::table('ESTUDIANTES')->insertGetId(

                      ['id'=>$this->id_usuario,

                       'id_nivel_grado'=>$this->id_nivel_grado,

                       'id_municipio'=>$this->id_municipio,

                       'nombre_estudiante'=>$this->nombre,

                       'apellidos_estudiante'=>$this->apellido,

                       'codigo_personal_estudiante'=>$this->codigo_personal,

                       'fecha_nacimiento_estudiante'=>$this->fecha_nacimiento,

                       'genero_estudiante'=>$this->genero,

                       'direccion_estudiante'=>$this->direccion,

                       'colonia_estudiante'=>$this->colonia,

                       'zona_estudiante'=>$this->zona,

                       'telefono_casa'=>$this->telefono_casa,

                       'empresa_telefonica'=>$this->empresa_telefonica,

                       'correo_estudiante'=>$this->correo,

                       'enfermedad_padecida_estudiante'=>$this->enfermedad_padecida,

                       'medicamento_recomendado_estudiante'=>$this->medicamento_recomendado,

                       'alergico_estudiante'=>$this->alergico,

                       'hospital_estudiante'=>$this->hospital,

                       'tipo_sangre_estudiante'=>$this->tipo_sangre,

                       'observaciones_estudiante'=>$this->observaciones,

                       'fecha_registro_estudiante'=>date('Y-m-d H:i:s'),

                       'estado_estudiante'=>TRUE], 'id_estudiante');

  }


  public function updateEstudiante()

  {
          /* jose florian
      UPDATE ESTUDIANTES SET `id_nivel_grado` = '3', `id_municipio` = '88', `nombre_estudiante` = 'Juanes Jorge', `apellidos_estudiante` = 'Pared Sosa', `codigo_personal_estudiante` = '123', `fecha_nacimiento_estudiante` = '2001-07-14', `genero_estudiante` = 'F', `direccion_estudiante` = 'zona 4 Guatemala', `colonia_estudiante` = 'mi colonia 2', `zona_estudiante` = 'zona 4', `telefono_casa` = '12345677', `empresa_telefonica` = 'tigo', `correo_estudiante` = 'juan-paredes2@prueba.com', `enfermedad_padecida_estudiante` = 'Ninguna', `medicamento_recomendado_estudiante` = 'Ninguna', `alergico_estudiante` = 'Ninguna', `hospital_estudiante` = 'Hospitalito nacional', `tipo_sangre_estudiante` = 'A-', `observaciones_estudiante` = 'Ningun', `fecha_registro_estudiante` = '2016-12-23', `estado_estudiante` = '1' WHERE `estudiantes`.`id_estudiante` = 8;
      */

   DB::table('ESTUDIANTES')->where('id_estudiante', $this->id_estudiante)

            ->update([

                   'id_nivel_grado'=>$this->id_nivel_grado,

                  'id_municipio'=>$this->id_municipio,

                  'apellidos_estudiante'=>$this->apellido,

                  'codigo_personal_estudiante'=>$this->codigo_personal,

                  'fecha_nacimiento_estudiante'=>$this->fecha_nacimiento,

                  'genero_estudiante'=>$this->genero,

                  'direccion_estudiante'=>$this->direccion,

                  'colonia_estudiante'=>$this->colonia,

                  'zona_estudiante'=>$this->zona,

                  'telefono_casa'=>$this->telefono_casa,

                  'empresa_telefonica'=>$this->empresa_telefonica,

                  'correo_estudiante'=>$this->correo,

                  'enfermedad_padecida_estudiante'=>$this->enfermedad_padecida,

                  'alergico_estudiante'=>$this->alergico,

                  'medicamento_recomendado_estudiante'=>$this->medicamento_recomendado,

                  'hospital_estudiante'=>$this->hospital,

                  'tipo_sangre_estudiante'=>$this->tipo_sangre,

                  'observaciones_estudiante'=>$this->observaciones


            ]);

  }



//metodo para buscar a un estudiante por su id para generar el reporte de inscripción

public function getEstudianteInscripcion()

{

  return Estudiantes::join('NIVELES_GRADOS as ng', 'ESTUDIANTES.id_nivel_grado', '=', 'ng.id_nivel_grado')

                    ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')

                    ->join('CARRERAS as c', 'ng.id_carrera', '=', 'c.id_carrera')

                    ->join('MUNICIPIO as m', 'ESTUDIANTES.id_municipio', '=', 'm.id_municipio')

                    ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')

                    ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')

                    ->where('ESTUDIANTES.id_estudiante', $this->id_estudiante)

                    ->select('ESTUDIANTES.nombre_estudiante', 'ESTUDIANTES.apellidos_estudiante', 'ESTUDIANTES.fecha_nacimiento_estudiante', 'ESTUDIANTES.direccion_estudiante', 'ESTUDIANTES.colonia_estudiante', 'ESTUDIANTES.zona_estudiante', 'ESTUDIANTES.telefono_casa', 'ESTUDIANTES.correo_estudiante', 'ESTUDIANTES.fecha_registro_estudiante', 'ESTUDIANTES.enfermedad_padecida_estudiante', 'ESTUDIANTES.medicamento_recomendado_estudiante', 'ESTUDIANTES.alergico_estudiante', 'ESTUDIANTES.hospital_estudiante', 'ESTUDIANTES.tipo_sangre_estudiante', 'ESTUDIANTES.observaciones_estudiante', 'ESTUDIANTES.empresa_telefonica', 'm.nombre_municipio', 'c.nombre_carrera', 'g.nombre_grado', 'n.nombre_nivel')

                    ->first();

}

/*

-- select de todos los estudiantes inscritos
Select ESTUDIANTES.nombre_estudiante, ESTUDIANTES.apellidos_estudiante, ESTUDIANTES.fecha_nacimiento_estudiante, ESTUDIANTES.direccion_estudiante, ESTUDIANTES.colonia_estudiante, ESTUDIANTES.zona_estudiante, ESTUDIANTES.telefono_casa, ESTUDIANTES.correo_estudiante, ESTUDIANTES.fecha_registro_estudiante, ESTUDIANTES.enfermedad_padecida_estudiante, ESTUDIANTES.medicamento_recomendado_estudiante, ESTUDIANTES.alergico_estudiante, ESTUDIANTES.hospital_estudiante, ESTUDIANTES.tipo_sangre_estudiante, ESTUDIANTES.observaciones_estudiante, ESTUDIANTES.empresa_telefonica, m.nombre_municipio, c.nombre_carrera, g.nombre_grado, n.nombre_nivel
from Estudiantes join NIVELES_GRADOS as ng on ESTUDIANTES.id_nivel_grado=ng.id_nivel_grado

                    join GRADOS as g on ng.id_grado=g.id_grado
                    join CARRERAS as c on ng.id_carrera=c.id_carrera
                    join MUNICIPIO as m on ESTUDIANTES.id_municipio=m.id_municipio
                    join NIVELES_PLANES_JORNADAS as npj on ng.id_nivel_plan_jornada=npj.id_nivel_plan_jornada
                    join NIVELES as n on npj.id_nivel=n.id_nivel
                    */



//metodo para buscar la nomina de estudiantes de acuerdo a un curs


public function getUsuariosNotafinalUnidad()
{

   return DB::table('ESTUDIANTES as e')

                 ->join('NIVELES_GRADOS as ng', 'e.id_nivel_grado', '=', 'ng.id_nivel_grado')

                  ->join('ASIGNACION_AREAS as aa', 'ng.id_nivel_grado', '=', 'aa.id_nivel_grado')

                  ->join('RESPUESTA_ACTIVIDADES as ra', 'ra.id_usuario','=','e.id')

                 ->where('aa.id_asignacion_area', $this->curso)

                 ->select('e.id_estudiante', 'e.nombre_estudiante', 'e.apellidos_estudiante','aa.id_asignacion_area', DB::raw('sum(ra.calificacion) AS calificacion'))

                 
                 ->get();
/*
    return actividad::join('RESPUESTA_ACTIVIDADES as ra', 'ACTIVIDADES.id_actividad', '=', 'ra.id_actividad')

                    ->join('TIPO_ACTIVIDAD as ta', 'ACTIVIDADES.id_tipo_actividad', '=', 'ta.id_tipo_actividad')

                    ->join('TIPO_ACTIVIDAD_ZONA as taz', 'ta.tipo_zona_actividad', '=', 'taz.id_tipo_actividad_zona')

                    ->join('USUARIO_ESTUDIANTE as ue', 'ra.id_usuario', '=', 'ue.id_usuario')

                    ->join('ESTUDIANTES as e', 'e.id_estudiante', '=', 'ue.id_alumno')

                    ->where('ACTIVIDADES.id_area', $this->curso)


                    ->orderBy('e.apellidos_estudiante', 'asc')

                    ->select('e.id_estudiante', 'e.nombre_estudiante', 'e.apellidos_estudiante', 'ra.calificacion')

                    ->get();
                    */

  /*
  return Estudiantes::join('NIVELES_GRADOS as ng', 'ESTUDIANTES.id_nivel_grado', '=', 'ng.id_nivel_grado')

                    ->join('ASIGNACION_AREAS as aa', 'ng.id_nivel_grado', '=', 'aa.id_nivel_grado')

                    ->join('RESPUESTA_ACTIVIDADES as ra', 'ra.id_usuario','=','ESTUDIANTES.id')                 

                    ->where('aa.id_asignacion_area', $this->curso)

                    ->orderBy('ESTUDIANTES.apellidos_estudiante', 'asc')

                    ->select('ESTUDIANTES.id_estudiante', 'ESTUDIANTES.nombre_estudiante', 'ESTUDIANTES.apellidos_estudiante', 'aa.id_asignacion_area','ra.calificacion')



                    ->get();

                    */



}


public function getEstudiantesCurso()

{

  

  return Estudiantes::join('NIVELES_GRADOS as ng', 'ESTUDIANTES.id_nivel_grado', '=', 'ng.id_nivel_grado')

                    ->join('ASIGNACION_AREAS as aa', 'ng.id_nivel_grado', '=', 'aa.id_nivel_grado')

                    ->where('aa.id_asignacion_area', $this->curso)

                    ->orderBy('ESTUDIANTES.apellidos_estudiante', 'asc')

                    ->select('ESTUDIANTES.id','ESTUDIANTES.id_estudiante', 'ESTUDIANTES.nombre_estudiante', 'ESTUDIANTES.apellidos_estudiante', 'aa.id_asignacion_area', 'aa.id_area')

                    ->get();
                    

                    /*
select ESTUDIANTES.id_estudiante,ESTUDIANTES.nombre_estudiante, ESTUDIANTES.apellidos_estudiante, aa.id_asignacion_area, sum(ra.calificacion) as total from  Estudiantes             join NIVELES_GRADOS as ng on ESTUDIANTES.id_nivel_grado=ng.id_nivel_grado

                    join ASIGNACION_AREAS as aa on ng.id_nivel_grado = aa.id_nivel_grado
                    
                    left join respuesta_actividades ra on ra.id_usuario=estudiantes.id

                    where aa.id_asignacion_area = 13

                    order By ESTUDIANTES.apellidos_estudiante ASC
                    */

                    
/*
                      return actividad::join('RESPUESTA_ACTIVIDADES as ra', 'ACTIVIDADES.id_actividad', '=', 'ra.id_actividad')

                    ->join('TIPO_ACTIVIDAD as ta', 'ACTIVIDADES.id_tipo_actividad', '=', 'ta.id_tipo_actividad')

                    ->join('TIPO_ACTIVIDAD_ZONA as taz', 'ta.tipo_zona_actividad', '=', 'taz.id_tipo_actividad_zona')

                    ->join('USUARIO_ESTUDIANTE as ue', 'ra.id_usuario', '=', 'ue.id_usuario')

                    ->join('ESTUDIANTES as e', 'e.id_estudiante', '=', 'ue.id_alumno')

                    ->where('ACTIVIDADES.id_area', $this->curso)

                    ->where('ACTIVIDADES.id_usuario', $this->id_usuario)

                    ->where('ACTIVIDADES.id_unidad', $unidad)

                    ->orderBy('e.apellidos_estudiante', 'asc')

                    ->select('e.id_estudiante', 'e.nombre_estudiante', 'e.apellidos_estudiante', 'ra.calificacion')

                    ->get();
                    

                    /*

                    Select  e.nombre_estudiante, e.apellidos_estudiante, sum(ra.calificacion) as total from actividades a 
                    join respuesta_actividades ra on a.id_actividad=ra.id_actividad 
                      join tipo_actividad as ta on a.id_tipo_actividad=ta.id_tipo_actividad
                      join tipo_actividad_zona as taz on ta.tipo_zona_actividad=taz.id_tipo_actividad_zona
                      join usuario_estudiante ue on ra.id_usuario=ue.id_usuario
                      join estudiantes e on e.id_estudiante=ue.id_alumno
                      
    
    where a.id_area=9 and a.id_usuario=3 and id_unidad=5
    */

}





//metodo para obtener todos los cursos asignados a un estudiante

/*
Select Estudiantes.id_estudiante, Estudiantes.nombre_estudiante, Estudiantes.apellidos_estudiante, aa.id_asignacion_area from Estudiantes join NIVELES_GRADOS as ng on Estudiantes.id_nivel_grado=ng.id_nivel_grado 

                    join ASIGNACION_AREAS as aa on ng.id_nivel_grado=aa.id_nivel_grado

                    where aa.id_asignacion_area=13

                    order by Estudiantes.apellidos_estudiante asc

                    */

public function getCursosEstudiante()

{

    return Estudiantes::join('NIVELES_GRADOS as ng', 'ESTUDIANTES.id_nivel_grado', '=', 'ng.id_nivel_grado')

                    ->join('ASIGNACION_AREAS as aa', 'ng.id_nivel_grado', '=', 'aa.id_nivel_grado')

                    ->join('AREAS as a', 'aa.id_area', '=', 'a.id_area')

                    ->where('ESTUDIANTES.id', $this->id_usuario)

                    ->select('ESTUDIANTES.id','aa.id_asignacion_area', 'a.nombre_area', 'a.id_area')

                    ->get();
}

//funcion para mostrar el ultimo estudiante inscrito cjfn

public function getUltimoEstudianteInscrito()
{

  

   return Estudiantes::join('users as u', 'ESTUDIANTES.id', '=', 'u.id')

                    ->leftjoin('TUTOR_ESTUDIANTE as te', 'te.id_estudiante', '=', 'ESTUDIANTES.id_estudiante')

                    ->leftjoin('TUTOR as t', 't.id_tutor', '=', 'te.id_tutor')

                     ->leftjoin('REFERENCIAS as r', 'r.id_estudiante', '=', 'ESTUDIANTES.id_estudiante')

                    ->join('NIVELES_GRADOS as ng', 'ESTUDIANTES.id_nivel_grado', '=', 'ng.id_nivel_grado')

                    ->join('GRADOS as g', 'ng.id_grado', '=', 'g.id_grado')

                    ->join('CARRERAS as c', 'ng.id_carrera', '=', 'c.id_carrera')

                    ->join('MUNICIPIO as m', 'ESTUDIANTES.id_municipio', '=', 'm.id_municipio')

                    ->join('NIVELES_PLANES_JORNADAS as npj', 'ng.id_nivel_plan_jornada', '=', 'npj.id_nivel_plan_jornada')

                    ->join('NIVELES as n', 'npj.id_nivel', '=', 'n.id_nivel')

                   
              

                   

                    ->orderBy('u.id', 'DESC')

                    ->select('ESTUDIANTES.id_estudiante as id','ESTUDIANTES.nombre_estudiante as nombre', 'ESTUDIANTES.apellidos_estudiante as apellido', 'ESTUDIANTES.fecha_nacimiento_estudiante as fecha', 'ESTUDIANTES.direccion_estudiante as direccion', 'ESTUDIANTES.colonia_estudiante as colonia', 'ESTUDIANTES.zona_estudiante as zona', 'ESTUDIANTES.telefono_casa as telefono', 'ESTUDIANTES.correo_estudiante', 'ESTUDIANTES.fecha_registro_estudiante', 'ESTUDIANTES.enfermedad_padecida_estudiante', 'ESTUDIANTES.medicamento_recomendado_estudiante', 'ESTUDIANTES.alergico_estudiante', 'ESTUDIANTES.hospital_estudiante', 'ESTUDIANTES.tipo_sangre_estudiante', 'ESTUDIANTES.observaciones_estudiante', 'ESTUDIANTES.empresa_telefonica', 'm.nombre_municipio', 'c.nombre_carrera', 'g.nombre_grado', 'n.nombre_nivel','r.nombre_referencia as nombre_ref','r.parentesco_referencia','r.parentesco_referencia','r.telefono_referencia','r.telefono_referencia','t.nombre_tutor','t.apellidos_tutor','t.correo_electronico_tutor','t.cui_tutor','t.direccion_tutor','t.telefono_primario_tutor','t.telefono_trabajo_tutor','t.direccion_tutor','t.telefono_trabajo_tutor','t.cargo_tutor','u.email', 'u.password')



                    ->first();
                

                     
  /*

  --- query que muestra todos los datos del ultimo  estudiante inscrito
 Select ESTUDIANTES.nombre_estudiante, ESTUDIANTES.apellidos_estudiante, ESTUDIANTES.fecha_nacimiento_estudiante, ESTUDIANTES.direccion_estudiante, ESTUDIANTES.colonia_estudiante, ESTUDIANTES.zona_estudiante, ESTUDIANTES.telefono_casa, ESTUDIANTES.correo_estudiante, ESTUDIANTES.fecha_registro_estudiante, ESTUDIANTES.enfermedad_padecida_estudiante, ESTUDIANTES.medicamento_recomendado_estudiante, ESTUDIANTES.alergico_estudiante, ESTUDIANTES.hospital_estudiante, ESTUDIANTES.tipo_sangre_estudiante, ESTUDIANTES.observaciones_estudiante, ESTUDIANTES.empresa_telefonica, m.nombre_municipio, c.nombre_carrera, g.nombre_grado, n.nombre_nivel, r.nombre_referencia, r.parentesco_referencia, r.telefono_referencia, t.nombre_tutor, t.apellidos_tutor, t.cui_tutor, t.direccion_tutor, t.telefono_primario_tutor, t.telefono_trabajo_tutor, t.correo_electronico_tutor, t.lugar_trabajo_tutor, t.direccion_tutor, t.telefono_trabajo_tutor, t.cargo_tutor, u.email, u.password
from ESTUDIANTES 
          join users u on ESTUDIANTES.id=u.id
          left join TUTOR_ESTUDIANTE as te on te.id_estudiante=ESTUDIANTES.id_estudiante
          left join TUTOR t on t.id_tutor=te.id_tutor
          left join REFERENCIAS r on r.id_estudiante=ESTUDIANTES.id_estudiante
                    join NIVELES_GRADOS as ng on ESTUDIANTES.id_nivel_grado=ng.id_nivel_grado
                    join GRADOS as g on ng.id_grado=g.id_grado
                    join CARRERAS as c on ng.id_carrera=c.id_carrera
                    join MUNICIPIO as m on ESTUDIANTES.id_municipio=m.id_municipio
                    join NIVELES_PLANES_JORNADAS as npj on ng.id_nivel_plan_jornada=npj.id_nivel_plan_jornada
                    join NIVELES as n on npj.id_nivel=n.id_nivel
                    
                    order by  u.id desc limit 1
                    */


}



//funcion FINAL que muestra  en detalle el punteo de todas las actividades hechar por un usuario en un area

public function getDetalleNota2()

{

  

  return Estudiantes::join('RESPUESTA_ACTIVIDADES as ra', 'ESTUDIANTES.id', '=', 'ra.id_usuario')
                  ->join('ACTIVIDADES as a', 'ra.id_actividad', '=', 'a.id_actividad')

                    
                    ->where('ESTUDIANTES.id', $this->curso)

                    ->where('ra.unidad', $this->unidad)

                    ->where('ra.area', $this->area)

                    

                    ->orderBy('ESTUDIANTES.apellidos_estudiante', 'asc')

                    ->select('ESTUDIANTES.id', 'ESTUDIANTES.nombre_estudiante','ESTUDIANTES.apellidos_estudiante','ra.calificacion','a.nombre_actividad as actividad', 'a.nota_total','ra.estado_entrega_actividad', 'ra.valor_max', 'ra.fecha_entrega')

                    ->get();
                    

                   /*
  select e.nombre_estudiante, e.apellidos_estudiante, ra.calificacion from estudiantes as e 
  join respuesta_actividades as ra on e.id=ra.id_usuario where ra.id_usuario=17 and ra.unidad=5 and ra.area=3
  */

}


public function getDetalleNota2File()

{

  

  return Estudiantes::join('files as f', 'ESTUDIANTES.id', '=', 'f.id_usuario')
                  ->join('ACTIVIDADES as a', 'f.id_actividad', '=', 'a.id_actividad')
                  ->join('UNIDADES as u', 'f.unidad', '=', 'u.id_unidad')

                    
                    ->where('ESTUDIANTES.id', $this->curso)

                    ->where('f.unidad', $this->unidad)

                    ->where('a.id_area', $this->area)

                    

                    ->orderBy('ESTUDIANTES.apellidos_estudiante', 'asc')

                    ->select('u.nombre_unidad','ESTUDIANTES.id', 'ESTUDIANTES.nombre_estudiante','ESTUDIANTES.apellidos_estudiante','f.calificacion','a.nombre_actividad as actividad', 'a.nota_total','f.estado_entrega_actividad', 'a.nota_total as valor_max', 'f.fecha_entrega')

                    ->get();
                    

                   /*
  select e.nombre_estudiante, e.apellidos_estudiante, ra.calificacion from estudiantes as e 
  join respuesta_actividades as ra on e.id=ra.id_usuario where ra.id_usuario=17 and ra.unidad=5 and ra.area=3
  */

}




public function getDetalleNotaAnterior()

{

  

  return Estudiantes::join('files as f', 'ESTUDIANTES.id', '=', 'f.id_usuario')
                  ->join('ACTIVIDADES as a', 'f.id_actividad', '=', 'a.id_actividad')
                   ->join('UNIDADES as u', 'f.unidad', '=', 'u.id_unidad')

                    

                    
                    ->where('ESTUDIANTES.id', $this->curso)

                    

                    ->where('a.id_area', $this->area)

                    ->whereNotIn('a.id_unidad', [$this->unidad])

                    

                    ->orderBy('ESTUDIANTES.apellidos_estudiante', 'asc')

                    
                    ->select('u.nombre_unidad','ESTUDIANTES.id', 'ESTUDIANTES.nombre_estudiante','ESTUDIANTES.apellidos_estudiante','f.calificacion','a.nombre_actividad as actividad', 'a.nota_total','f.estado_entrega_actividad', 'a.nota_total as valor_max', 'f.fecha_entrega')

                    ->get();
                    

                   /*
  select e.nombre_estudiante, e.apellidos_estudiante, ra.calificacion from estudiantes as e 
  join respuesta_actividades as ra on e.id=ra.id_usuario where ra.id_usuario=17 and ra.unidad=5 and ra.area=3
  */

}


//funcion FINAL que muestra  en detalle el punteo de todas las actividades hechar por un usuario en un area

public function gettotalNota()

{

  

    return Estudiantes::join('RESPUESTA_ACTIVIDADES as ra', 'ESTUDIANTES.id', '=', 'ra.id_usuario')
                    ->join('ACTIVIDADES as a', 'ra.id_actividad', '=', 'a.id_actividad')

                    ->join('AREAS as ar', 'a.id_area', '=', 'ar.id_area')

                      
                      ->where('ESTUDIANTES.id', $this->curso)

                      ->where('ra.unidad', $this->unidad)

                      ->where('ra.area', $this->area)

                      ->orderBy('ESTUDIANTES.apellidos_estudiante', 'asc')

                      ->select( DB::raw('sum(ra.calificacion) AS calificacion'), 'ESTUDIANTES.id', 'ESTUDIANTES.nombre_estudiante','ESTUDIANTES.apellidos_estudiante','ar.nombre_area')

                    ->get();
                    

                   /*
  select e.nombre_estudiante, e.apellidos_estudiante, sum(ra.calificacion) as nota from estudiantes as e 
  join respuesta_actividades as ra on e.id=ra.id_usuario where ra.id_usuario=17 and ra.unidad=5 and ra.area=3
  */

}


public function gettotalNotaFile()

{

  

    return Estudiantes::join('files as f', 'ESTUDIANTES.id', '=', 'f.id_usuario')
                    ->join('ACTIVIDADES as a', 'f.id_actividad', '=', 'a.id_actividad')

                    ->join('AREAS as ar', 'a.id_area', '=', 'ar.id_area')

                      
                      ->where('ESTUDIANTES.id', $this->curso)

                      ->where('f.unidad', $this->unidad)

                      ->where('a.id_area', $this->area)

                      ->orderBy('ESTUDIANTES.apellidos_estudiante', 'asc')

                      ->select( DB::raw('sum(f.calificacion) AS calificacion'), 'ESTUDIANTES.id', 'ESTUDIANTES.nombre_estudiante','ESTUDIANTES.apellidos_estudiante','ar.nombre_area')

                    ->get();
                    

                   /*
  select e.nombre_estudiante, e.apellidos_estudiante, sum(ra.calificacion) as nota from estudiantes as e 
  join respuesta_actividades as ra on e.id=ra.id_usuario where ra.id_usuario=17 and ra.unidad=5 and ra.area=3
  */

}


public function getMisNotas()

{

  

  return Estudiantes::join('RESPUESTA_ACTIVIDADES as ra', 'ESTUDIANTES.id', '=', 'ra.id_usuario')
                  ->join('ACTIVIDADES as a', 'ra.id_actividad', '=', 'a.id_actividad')

                  ->join('AREAS as ar', 'a.id_area', '=', 'ar.id_area')

                    
                    ->where('ESTUDIANTES.id', $this->id_usuario)

                    ->where('ra.unidad', $this->unidad)

                    ->where('ra.area', $this->area)


                    ->orderBy('ESTUDIANTES.apellidos_estudiante', 'asc')

                    ->select('ra.calificacion', 'a.nombre_actividad as actividad', 'a.nota_total','ra.estado_entrega_actividad', 'ra.valor_max', 'ra.fecha_entrega')

                    ->get();
                    

                   /*
  sselect a.nombre_actividad, e.apellidos_estudiante, ra.calificacion from estudiantes as e 
  join respuesta_actividades as ra on e.id=ra.id_usuario
  join actividades as a on a.id_actividad=ra.id_actividad
  where ra.id_usuario=17 and ra.unidad=5 and ra.area=3
  */

}

//NOTAS DEL ALUMNO
public function getMisNotasFile()

{

  

  return Estudiantes::join('files as f', 'ESTUDIANTES.id', '=', 'f.id_usuario')
                  ->join('ACTIVIDADES as a', 'f.id_actividad', '=', 'a.id_actividad')

                  ->join('UNIDADES as u', 'f.unidad', '=', 'u.id_unidad')


                  ->join('AREAS as ar', 'a.id_area', '=', 'ar.id_area')

                    
                    ->where('ESTUDIANTES.id', $this->id_usuario)

                    ->where('f.unidad', $this->unidad)

                    ->where('a.id_area', $this->area)


                    ->orderBy('a.id_actividad', 'asc')

                    ->select('u.nombre_unidad','f.calificacion', 'a.nombre_actividad as actividad', 'a.nota_total','f.estado_entrega_actividad', 'a.nota_total as valor_max', 'f.fecha_entrega')

                    ->get();
                    

                   /*
  sselect a.nombre_actividad, e.apellidos_estudiante, ra.calificacion from estudiantes as e 
  join respuesta_actividades as ra on e.id=ra.id_usuario
  join actividades as a on a.id_actividad=ra.id_actividad
  where ra.id_usuario=17 and ra.unidad=5 and ra.area=3
  */

}

//NOTAS UNIDADES ANTERIORRES POR ALUMNO
public function getMisNotasFileAnteriores()

{

  

  return Estudiantes::join('files as f', 'ESTUDIANTES.id', '=', 'f.id_usuario')
                  ->join('ACTIVIDADES as a', 'f.id_actividad', '=', 'a.id_actividad')

                  ->join('UNIDADES as u', 'f.unidad', '=', 'u.id_unidad')


                  ->join('AREAS as ar', 'a.id_area', '=', 'ar.id_area')

                    
                    ->where('ESTUDIANTES.id', $this->id_usuario)

                    ->whereNotIn('a.id_unidad', [$this->unidad])

                    ->where('a.id_area', $this->area)


                    ->orderBy('a.id_actividad', 'desc')

                    ->select('u.nombre_unidad','f.calificacion', 'a.nombre_actividad as actividad', 'a.nota_total','f.estado_entrega_actividad', 'a.nota_total as valor_max', 'f.fecha_entrega')

                    ->get();
                    

                   /*
  sselect a.nombre_actividad, e.apellidos_estudiante, ra.calificacion from estudiantes as e 
  join respuesta_actividades as ra on e.id=ra.id_usuario
  join actividades as a on a.id_actividad=ra.id_actividad
  where ra.id_usuario=17 and ra.unidad=5 and ra.area=3
  */

}


public function getMitotalNota()

{

  

    return Estudiantes::join('RESPUESTA_ACTIVIDADES as ra', 'ESTUDIANTES.id', '=', 'ra.id_usuario')
                    ->join('ACTIVIDADES as a', 'ra.id_actividad', '=', 'a.id_actividad')

                    ->join('AREAS as ar', 'a.id_area', '=', 'ar.id_area')

                      
                       ->where('ESTUDIANTES.id', $this->id_usuario)

                        ->where('ra.unidad', $this->unidad)

                        ->where('ra.area', $this->area)

                      ->orderBy('ESTUDIANTES.apellidos_estudiante', 'asc')

                      ->select( DB::raw('sum(ra.calificacion) AS calificacion'), 'ESTUDIANTES.id', 'ESTUDIANTES.nombre_estudiante','ESTUDIANTES.apellidos_estudiante','ar.nombre_area')

                    ->get();
                    

                   /*
  select e.nombre_estudiante, e.apellidos_estudiante, sum(ra.calificacion) as nota from estudiantes as e 
  join respuesta_actividades as ra on e.id=ra.id_usuario where ra.id_usuario=17 and ra.unidad=5 and ra.area=3
  */

}


public function getMitotalNotaFile()

{

  

    return Estudiantes::join('files as f', 'ESTUDIANTES.id', '=', 'f.id_usuario')
                    ->join('ACTIVIDADES as a', 'f.id_actividad', '=', 'a.id_actividad')

                    ->join('AREAS as ar', 'a.id_area', '=', 'ar.id_area')

                      
                       ->where('ESTUDIANTES.id', $this->id_usuario)

                        ->where('f.unidad', $this->unidad)

                        ->where('a.id_area', $this->area)

                      ->orderBy('ESTUDIANTES.apellidos_estudiante', 'asc')

                      ->select( DB::raw('sum(f.calificacion) AS calificacion'), 'ESTUDIANTES.id', 'ESTUDIANTES.nombre_estudiante','ESTUDIANTES.apellidos_estudiante','ar.nombre_area')

                    ->get();
                    

                   /*
  select e.nombre_estudiante, e.apellidos_estudiante, sum(ra.calificacion) as nota from estudiantes as e 
  join respuesta_actividades as ra on e.id=ra.id_usuario where ra.id_usuario=17 and ra.unidad=5 and ra.area=3
  */

}


// seleccionar el total de notas por area de un determinado usuario y una determinada unidad

public function getTodasMisNotas()

{

  

    return Areas::join('ASIGNACION_AREAS as aa', 'AREAS.id_area', '=', 'aa.id_area')
                    ->join('GRADOS as g', 'aa.id_nivel_grado', '=', 'g.id_grado')

                    ->join('ESTUDIANTES as e', 'e.id_nivel_grado', '=', 'g.id_grado')

                    ->join('ACTIVIDADES as act', 'act.id_area', '=', 'AREAS.id_area')

                    ->join('RESPUESTA_ACTIVIDADES as ra', 'ra.id_actividad', '=', 'act.id_actividad')

                      
                       ->where('e.id', $this->id_usuario)

                        ->where('ra.id_usuario', $this->id_usuario)

                       

                      ->groupBy('AREAS.nombre_area')

                      ->select( 'AREAS.nombre_area',DB::raw('sum(ra.calificacion) AS total','e.nombre_estudiante as nombre', 'e.apellidos_estudiante as apellidos'))

                    ->get();
                    

                   /*
  select a.nombre_area, sum(ra.calificacion) as total  from areas as a join asignacion_areas as aa on a.id_area=aa.id_area
join grados as g on aa.id_nivel_grado=g.id_grado
join estudiantes as e on e.id_nivel_grado=g.id_grado
join actividades as act on act.id_area=a.id_area
join respuesta_actividades as ra on ra.id_actividad=act.id_actividad
where e.id=17 and ra.id_usuario=17 and ra.unidad=5
group by a.nombre_area
  */

}


public function getTodasMisNotasFile()

{

  

    return file::join('AREAS as a', 'files.area', '=', 'a.id_area')
                    ->join('UNIDADES AS u', 'files.unidad', '=', 'u.id_unidad')
                     ->join('ESTUDIANTES AS E', 'files.id_usuario', '=', 'E.id')


                      
                       ->where('files.unidad', '!=', $this->unidad)

                        ->where('files.id_usuario', $this->id_usuario)

                         ->where('E.estado_estudiante', 1)




                      ->groupBy('files.area')

                      ->select( 'u.nombre_unidad','a.nombre_area',DB::raw('sum(files.calificacion) AS total'))

                    ->get();
                   /*query que muestra todas las notas de cursos anteriores
                    SELECT u.nombre_unidad, a.nombre_area, SUM( f.calificacion ) 
                    FROM files AS f
                    JOIN AREAS AS a ON f.area = a.id_area
                    JOIN UNIDADES AS u ON f.unidad = u.id_unidad
          JOIN ESTUDIANTES AS E on f.id_usuario=E.id

                    WHERE f.id_usuario =17
                    AND f.unidad !=6
          AND E.estado_estudiante=1
                    GROUP BY f.area
                    */



                    

}




public function getTodasMisNotasFileSinNota()

{

  

    return Areas::join('ASIGNACION_AREAS as aa', 'AREAS.id_area', '=', 'aa.id_area')
                    ->join('GRADOS as g', 'aa.id_nivel_grado', '=', 'g.id_grado')

                    ->join('ESTUDIANTES as e', 'e.id_nivel_grado', '=', 'g.id_grado')

                    ->join('ACTIVIDADES as act', 'act.id_area', '=', 'AREAS.id_area')

                    ->join('files as f', 'f.id_actividad', '=', 'act.id_actividad')

                      
                       ->where('e.id', $this->id_usuario)

                        ->where('f.id_usuario', $this->id_usuario)

                         

                      ->groupBy('f.unidad')

                      ->select( 'AREAS.nombre_area',DB::raw('sum(f.calificacion) AS total','e.nombre_estudiante as nombre', 'e.apellidos_estudiante as apellidos'))

                    ->get();

                    /*query que muestra todas las notas de cursos anteriores
                    SELECT u.nombre_unidad, a.nombre_area, SUM( f.calificacion ) 
                    FROM files AS f
                    JOIN AREAS AS a ON f.area = a.id_area
                    JOIN UNIDADES AS u ON f.unidad = u.id_unidad

                    WHERE f.id_usuario =17
                    AND f.unidad !=6
                    GROUP BY f.area*/



}



public function getusuarios()
{

   return DB::table('users as u')

                 
                ->orderBy('u.id', 'desc')
                 
                 ->first();






}
}

