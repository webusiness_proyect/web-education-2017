<?php



namespace education\Repositories;







use education\horario;



use education\Estudiantes;

use education\Personas;

use education\Usuario_persona;




use DB;



/**



 * Clase repositorio para el tratamiento de datos para el docente



 */



class HorarioRepository



{



    public $idcalendario;



    public $dia;



    public $hora_inicia;



    public $hora_fin;



    public $id_nivel_grado;



    public $id_area;



    public $fecha_creacion;



    public $fecha_modificacion;



    public $user;



    public $estado_actividad_calendario;



    //metodo para obtener todo los cursos que tiene asignado un docente



    public function setCalendario()



    {



      DB::table('CALENDARIO')->insert([



                                  'user'=>$this->user,



                                  'dia'=>$this->dia,



                                  'hora_inicia'=>$this->hora_inicia,



                                  'hora_fin'=>$this->hora_fin,



                                  'id_nivel_grado'=>$this->id_nivel_grado,



                                  'id_area'=>$this->id_area,





                                  'estado_actividad_calendario'=>$this->estado_actividad_calendario

                                  



                                ]);



    }



      public function getCalendarioById()



  {



     return horario::where('idcalendario', $this->idcalendario)->first();



  }



     public function getNivelEstudiantes()



  {



     return Estudiantes::where('id', $this->user)->first();



  }



  public function getnivelGradoTutores()
  {

     return Usuario_persona::join('ASIGNACION_DOCENTE as ad','USUARIO_PERSONA.id_persona','=','ad.id_persona')
      ->join('ASIGNACION_AREAS as aa','ad.id_asignacion_area','=','aa.id_asignacion_area')
      ->select(DB::raw('DISTINCT aa.id_nivel_grado'))
          ->where('USUARIO_PERSONA.user_id','=', $this->user)
      ->get();

      /* return $nivel=DB::table('USUARIO_PERSONA as up')
          ->join('ASIGNACION_DOCENTE as ad','up.id_persona','=','ad.id_persona')
          ->join('ASIGNACION_AREAS as aa','ad.id_asignacion_area','=','aa.id_asignacion_area')
          ->select(DB::raw('DISTINCT aa.id_nivel_grado'))
          ->where('up.id_persona','=', $this->user)
          ->get();// traigo el nivel del estudiante*/

    /*SELECT distinct(aa.id_nivel_grado) FROM `USUARIO_PERSONA` as up join  ASIGNACION_DOCENTE as ad on up.id_persona=ad.id_persona
  join ASIGNACION_AREAS as aa on ad.id_asignacion_area=aa.id_asignacion_area
  where up.id_persona=3
  */


  }

  public function getDocentes()



  {



     return Personas::join('USUARIO_PERSONA AS UP','PERSONAS.id_persona','=','UP.id_persona')
     ->where('UP.user_id', $this->user)->first();

     /* QUERY QUE TRAE TODOS LOS DATOS DE LA PERSONA POR ID
     select * from PERSONAS join USUARIO_PERSONA AS UP on PERSONAS.id_persona=UP.id_persona
      where UP.user_id=3
      */



  }

  public function getHijosTutores()



  {



     return Estudiantes::join('TUTOR_ESTUDIANTE AS TE','ESTUDIANTES.id_estudiante','=','TE.id_estudiante')
     ->join('TUTOR AS T','TE.id_tutor','=','T.id_tutor')
     ->join('users AS U','T.id','=','U.id')
     ->join('NIVELES_GRADOS AS NG','ESTUDIANTES.id_nivel_grado','=','NG.id_nivel_grado')
     ->join('GRADOS AS G','NG.id_grado','=','G.id_grado')
     ->join('NIVELES_PLANES_JORNADAS AS NPJ','NG.id_nivel_plan_jornada','=','NPJ.id_nivel_plan_jornada')
     ->join('NIVELES AS N','NPJ.id_nivel','=','N.id_nivel')
     ->join('JORNADAS AS J','NPJ.id_jornada','=','J.id_jornada')
     ->join('PLANES AS PL','NPJ.id_plan','=','PL.id_plan')
     ->join('SECCIONES AS S','NG.id_seccion','=','S.id_seccion')
     ->join('CARRERAS AS CA','NG.id_carrera','=','CA.id_carrera')
     ->select('ESTUDIANTES.id_nivel_grado')
     ->where('U.id', $this->user)->first(5);

     /* QUERY QUE TRAE LA ASIGNACION DE UN HIJO A SU PADRE
     SELECT * 
FROM ESTUDIANTES AS E
JOIN TUTOR_ESTUDIANTE AS TE ON E.id_estudiante = TE.id_estudiante
JOIN TUTOR AS T ON TE.id_tutor = T.id_tutor
JOIN users AS U ON T.id = U.id
JOIN NIVELES_GRADOS AS NG ON E.id_nivel_grado = NG.id_nivel_grado
JOIN GRADOS AS G ON NG.id_grado = G.id_grado
JOIN NIVELES_PLANES_JORNADAS AS NPJ ON NG.id_nivel_plan_jornada = NPJ.id_nivel_plan_jornada
JOIN NIVELES AS N ON NPJ.id_nivel = N.id_nivel
JOIN JORNADAS AS J ON NPJ.id_jornada = J.id_jornada
JOIN PLANES AS PL ON NPJ.id_plan = PL.id_plan
JOIN SECCIONES AS S ON NG.id_seccion = S.id_seccion
JOIN CARRERAS AS CA ON NG.id_carrera = CA.id_carrera
WHERE U.id =394
LIMIT 0 , 30
      */



  }












     public function updateCalendario()



  {



    DB::table('CALENDARIO')->where('idcalendario', $this->idcalendario)



                      ->update([



                                  'dia'=>$this->dia,



                                  'hora_inicia'=>$this->hora_inicia,



                                  'hora_fin'=>$this->hora_fin,



                                  'id_nivel_grado'=>$this->id_nivel_grado,



                                  'id_area'=>$this->id_area,





                              ]);



  }















    /*







    QUERY PARA SELECCIONAR TODOS LOS horarios en el calendario





select

     c.dia, c.hora_inicia, c.hora_fin,c.asignacion_area, aa.id_salon, aa.id_area, a.nombre_area, d.dia 

   

  from calendario as c

  join asignacion_areas as aa on c.asignacion_area=aa.id_asignacion_area

  join areas as a on aa.id_area=a.id_area

  left join dias as d on c.dia=d.id_dia

  

 

    

    order by c.dia

    */





}



