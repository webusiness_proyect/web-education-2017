<?php
namespace education\Repositories;

use education\Role;
use DB;
/**
 * Clase repositorio para el tratamiento de datos de la tabla roles
 */
class rolesRepository
{
    public $name;
    public $display_name;
    public $description;
    public $create_at;
    public $state_rol;

    //metodo para registrar un nuevo rol
    public function setRol()
    {
        //DB::insert('INSERT INTO roles(name, display_name, description, create_at, )');
    }

    //metodo para obtener a todos los roles disponibles
    public function getRoles()
    {
      # code...
    }

    //metodo para obtener los datos de un rol por su nombre
    public function findRolWhere()
    {
      return Role::where('name', $this->name)->first();
    }

}
