<?php



namespace education;



use Illuminate\Database\Eloquent\Model;

use DB;

class Usuarios extends Model

{

   
   protected $table = 'users';


    /*

      Metodo para agregar un nuevo registro a la base de datos

    */

    public static function setUsuario($usuario, $pass, $fecha, $correo, $rol)

    {

      return DB::select('CALL nuevo_usuario(?, ?, ?, ?, ?)', array($usuario, $pass, $fecha, $correo, $rol));

    }


    public static function getUsuario1($q)
    {
      /*return PERSONAS::join('PUESTOS as p', 'PERSONAS.id_puesto', '=', 'p.id_puesto')
                     ->where('nombre_puesto', '=', 'docente')
                     //->orWhere('nombre_persona', 'like', '%'.$q.'%')
                     //->where([['p.nombre_puesto', '=', 'docente'], ['apellidos_persona', 'like', '%'.$q.'%'], ['nombre_persona', 'like', '%'.$q.'%']])
                    //->where([['apellidos_persona', 'like', '%'.$q.'%'], 'OR',['nombres_persona', 'like', '%'.$q.'%']])
                     //->orWhere('nombres_persona', 'like', '%'.$q.'%')
                     ->select('nombres_persona', 'apellidos_persona', 'id_persona')
                     ->get();*/
                     /*
      return users::where(function ($query) use ($q) {
                         $query->where('users.name', 'like', '%'.$q.'%')
                               ->orWhere('users.email', 'like', '%'.$q.'%');
                     })
                     ->select('users.name as name', 'users.email as email', 'users.id as id')
                     ->get();
	*/


        return DB::table('users as u')->join('role_user AS ru', 'u.id', '=', 'ru.user_id')
          ->join('roles as r', 'ru.role_id', '=', 'r.id')
        
          ->select('u.name as nombre', 'u.email as correo', 'r.name as rol', 'u.id as id')


          ->where('u.name','LIKE', '%'.$q.'%')
          ->orWhere('u.email','LIKE', '%'.$q.'%')
          ->orWhere('r.name','LIKE', '%'.$q.'%')
          ->orderBy('u.id', 'asc');


                      
    }

    public static function getBuscaUsuario($q)
    {
      /*return PERSONAS::join('PUESTOS as p', 'PERSONAS.id_puesto', '=', 'p.id_puesto')
                     ->where('nombre_puesto', '=', 'docente')
                     //->orWhere('nombre_persona', 'like', '%'.$q.'%')
                     //->where([['p.nombre_puesto', '=', 'docente'], ['apellidos_persona', 'like', '%'.$q.'%'], ['nombre_persona', 'like', '%'.$q.'%']])
                    //->where([['apellidos_persona', 'like', '%'.$q.'%'], 'OR',['nombres_persona', 'like', '%'.$q.'%']])
                     //->orWhere('nombres_persona', 'like', '%'.$q.'%')
                     ->select('nombres_persona', 'apellidos_persona', 'id_persona')
                     ->get();*/
      return users::where(function ($query) use ($q) {
                         $query->where('name', 'like', '%'.$q.'%');
                               
                     })
                     ->select('name', 'email', 'id')
                     ->get();
                   
    }

  public static function stateUser($id, $estado)

    {

      return DB::select('CALL estado_usuarios(?, ?)', array($id, $estado));

    }



}

