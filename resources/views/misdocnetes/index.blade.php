@extends('principal')



@section('titulo')

  <title>Mis alumnos</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

      <di <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-bookmark" aria-hidden="true"></div> Docentes de mis hijos </h1>
      
        @include('mensajes.msg')

        

           <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
       
      </center>

        </div>

       
        <div class="col-sm-12 panel panel-default">

          <br/>



        <div class="table-responsive">

          <table class="table table-hover table-bordered">

            <thead>

              <tr>

               <th>
                <CENTER>
                  <h4><strong>ENVIA MENSAJE</strong></h4>
                </CENTER>
                </th>


                <th>
                  <CENTER>
                 <h4><strong> DOCENTE </strong></h4>
               </CENTER>
                </th>

                <th>
                  <CENTER>
                 <h4><strong> OPCIONES </strong></h4>
               </CENTER>
                </th>


               

              </tr>

            </thead>

            <tbody>

              @foreach($docentes as $key => $docente)

                <tr>

                  
                   <td>

                    <center>
                    <a href="{{ url('/enviomensajes') }}"><i class="fa fa-envelope-o fa-2x"></i></a>
                  </center>

                  </td>

                  
                  <td>
                  <center>
                    <h4>{{ mb_strtoupper($docente->nombres_persona) }}    {{ mb_strtoupper($docente->apellidos_persona) }}</h4>
                  </center>
                  </td>

                  
                    <td>

                    <center>
                       <H4><a href="/miscursosprofesores/show?a={{ $docente->id_persona }}"  class="btn btn-success"><div class="fa fa-eye fa-2x" enabled></div> CURSOS</a><br/><H4>
                       
                      </center>

                  </td>

                 
                 

                </tr>

              @endforeach

            </tbody>

          </table>

        </div>

      </div>

      <div class="col-sm-12 text-center">

     
      </div>

    </div>

  </div>

@endsection

