@extends('principal')

@section('titulo')
  <title>Inscripciones Estudiantes</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">



<div class="panel-body ">
        <h1 class="page-header text-center"> <div class="fa fa-users" aria-hidden="true"></div>INSCRIPCION DE ESTUDIANTES</h1>
        <div class="panel panel-default">
        <center><a href="#demo"  data-toggle="collapse"><div class="fa fa-question-circle fa-2x "></div><H4><STRONG>INFORMACION</STRONG></H4></a></center>
          <div id="demo" class="collapse">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra estudiantes, asigna un tutor y una referencia, el sistema creara un usuario y una contraseña para cada usuario los cuales llegaran a los correos registrados
            </h4>

          
           
          </center>
        
        <div class="alert alert-warning alert-dismissable">
          <center>

            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-info btn btn-default" aria-hidden="true"></i><STRONG>LLENA TODOS LOS CAMPOS EN EL ORDEN INDICADO  TODOS LOS * NO DEBEN DEJARSE EN BLANCO</a> </STRONG><br/>
           

            
          </CENTER>
        </div>
        </div>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
         @permission('crear-alumno')

        {!!link_to_route('inscripcionestudiantes.create', $title = 'Nueva Inscripción', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus'])!!}
        @include('inscripciones.search')
         @endpermission



         @if(count($cursos) == 0)
          <p class="text-info">
            No se han registrado estudiantes aun...
          </p>

        </center>




        
      @else
      <div class="table-responsive panel panel-default">

        <table class="table table-hover">
            <thead>
              <tr>
             
                <th>
                  ALUMNO
                </th>
                <th>
                  TELEFONO 
                </th>
                <th>
                  CORREO 
                </th>
                <th>
                  GRADO 
                </th>
                <th>
                  CARRERA 
                </th>
                <th>
                  NIVEL 
                </th>
                @permission('editar-alumno')
                <th>
                  OPCIONES
                </th>
                @endpermission
                @permission('estado-alumno')
                <th>
                  ESTADO
                </th>
                @endpermission
              </tr>
            </thead>
            <tbody id="datosEstudiantes">
              @foreach($cursos as $key => $grado)
                <tr>
                 
                  <td>
                    {{ mb_strtoupper($grado->apellidos_estudiante) }}

                    {{ mb_strtoupper($grado->nombre_estudiante) }}
                  </td>
                   <td>
                    {{ mb_strtoupper($grado->telefono_casa) }}
                  </td>
                   <td>
                    {{ mb_strtoupper($grado->correo_estudiante) }}
                  </td>
                   <td>
                    {{ mb_strtoupper($grado->nombre_grado) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($grado->nombre_carrera) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($grado->nombre_nivel) }}
                  </td>
                  @permission('editar-alumno')
                  <td>
                  <center>
                    {!!link_to_route('inscripcionestudiantes.edit', $title = 'Editar', $parameters = $grado->id_estudiante, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}
                    <a href="/tutoreshijos/show?a={{$grado->id_estudiante}}" class="btn btn-default"><div class="fa fa-eye"></div> Padres </a>
                  </center>

                   
                  </td>
                 
                  <td>
  
                     @if($grado->estado_estudiante == TRUE)
                              <input type="checkbox" name="estado" checked value="{{ $grado->id }}" class="toggleEstado">
                            @else
                              <input type="checkbox" name="estado" value="{{ $grado->id }}" class="toggleEstado">
                            @endif
                  </td>
                  @endpermission
                </tr>
              @endforeach
            </tbody>


          </table>
          </div>
           <div class="text-center">
            {!! $cursos->links() !!}
          </div>
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
          @include('mensajes.carga')
          @endif
      
      </div>
    </div>
  </div>
@endsection