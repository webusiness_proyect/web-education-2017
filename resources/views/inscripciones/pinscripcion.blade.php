@extends('principal')



@section('titulo')

  <title>TUTORES</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">



      

        <h1 class="page-header text-center"> <div class="fa fa-eye" aria-hidden="true"></div>INSCRIPCION DE ESTUDIANTES</h1>

        

      </div>

        

        <p>

          NOTA: Todos los campos con (*) son requeridos.

        </p>

        @include('mensajes.errores')

      </div>

      <div class="col-sm-12">

       {!!Form::model($dato,  ['route'=>['inscripcionestudiantes.update', $dato->id_tutor], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'tutor'])!!}


        <center>

       

        </center>



        <div class="tab-content panel panel-default">

        <div id="menu1" class="tab-pane fade in active">

     

          <h3>Datos Tutores</h3>

          @include('inscripciones.campos.tutores') 

        </div>


        <div id="menu4" class="tab-pane fade">

          <h3>Registrar Estudiante</h3>

          <button type="submit" name="registrar" class="btn btn-success btn-lg"><i class="fa fa-save"></i> Registrar</button>


        </div>
        <br/>
       

        


           <ul class="nav nav-pills">
           

        <li class="active">{!! link_to(URL::previous(), 'CANCELAR', ['class' => 'btn btn-info fa fa-times']) !!} <i class="fa"></i></li>

        <li><a class="active" data-toggle="tab" href="#menu1"> Tutores <i class="fa fa-eye"></i></a></li>

        <li><a data-toggle="tab" href="#menu4"> Registrar <i class="fa fa-save"></i></a></li>

        </ul>
        </center>
        </div>

        </div>






        {!!Form::close()!!}

      </div>

    </div>

  </div>

@endsection

