<div class="panel panel-primary">

  <!-- Default panel contents -->

  <div class="panel-heading">Datos Estudiantes</div>

  <div class="panel-body">



<div class="form-group">

  {!!Form::label('nombres', 'Nombres*', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

      {!!Form::text('nombre_estudiante', null, ['class'=>'form-control', 'placeholder'=>'Nombre del estudiante...'])!!}

  </div>

</div>

<div class="form-group">

  {!!Form::label('apellidos', 'Apellidos*', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

      {!!Form::text('apellidos_estudiante', null, ['class'=>'form-control', 'placeholder'=>'Apellidos del estudiante...'])!!}

  </div>

</div>

<div class="form-group">

  {!!Form::label('codigo', 'Código Personal', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

      {!!Form::text('codigo_personal_estudiante', null, ['class'=>'form-control', 'placeholder'=>'Código personal del estudiante...'])!!}

  </div>

</div>

<div class="form-group">

  {!!Form::label('fecha', 'Fecha Nacimiento', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

      <div class="input-group calendario">

          {!!Form::text('fecha_nacimiento_estudiante', null, ['class'=>'form-control', 'placeholder'=>'Fecha de nacimiento del estudiante...'])!!}

          <span class="input-group-addon">

            <i class="fa fa-calendar"></i>

          </span>

      </div>

  </div>

</div>

<div class="form-group">

  {!!Form::label('genero', 'Genero*', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::radio('genero_estudiante', 'M')!!} Masculino

    {!!Form::radio('genero_estudiante', 'F')!!} Femenino

  </div>

</div>

<div class="form-group">

  {!!Form::label('departamento', 'Departamento*', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::select('id_departamento', $deptos, null,['class'=>'form-control', 'placeholder'=>'Seleccione departamento...'])!!}

  </div>

</div>

<div class="form-group">

  {!!Form::label('municipio', 'Municipio*', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::select('id_municipio', array(), null,['class'=>'form-control', 'placeholder'=>'Seleccione municipio...'])!!}

  </div>

</div>

<div class="form-group">

  {!!Form::label('direccion', 'Dirección*', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::text('direccion_estudiante', null, ['class'=>'form-control', 'placeholder'=>'Dirección del estudiante'])!!}

  </div>

</div>

<div class="form-group">

  {!!Form::label('colonia', 'Colonia ', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::text('colonia_estudiante', null, ['class'=>'form-control', 'placeholder'=>'Colonia del estudiante'])!!}

  </div>

</div>

<div class="form-group">

  {!!Form::label('zona', 'Zona Estudiante', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::select('zona_estudiante', ['zona 0'=>'ZONA 0', 'zona 1'=>'ZONA 1', 'zona 2'=>'ZONA 2', 'zona 3'=>'ZONA 3', 'zona 4'=>'ZONA 4', 'zona 5'=>'ZONA 5', 'zona 6'=>'ZONA 6', 'zona 7'=>'ZONA 7', 'zona 8'=>'ZONA 8', 'zona 9'=>'ZONA 9', 'zona 10'=>'ZONA 10', 'zona 11'=>'ZONA 11', 'zona 12'=>'ZONA 12', 'zona 13'=>'ZONA 13', 'zona 14'=>'ZONA 14', 'zona 15'=>'ZONA 15', 'zona 16'=>'ZONA 16', 'zona 17'=>'ZONA 17', 'zona 18'=>'ZONA 18', 'zona 19'=>'ZONA 19', 'zona 20'=>'ZONA 20', 'zona 21'=>'ZONA 21', 'zona 22'=>'ZONA 22', 'zona 23'=>'ZONA 23', 'Otra'=>'OTRA'], null, ['class'=>'form-control', 'placeholder'=>'Zona de residencia del estudiante'])!!}

  </div>

</div>

<div class="form-group">

  {!!Form::label('telefono', 'Telefono Casa', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::text('telefono_casa', null, ['class'=>'form-control', 'placeholder'=>'Telefono casa del estudiante'])!!}

  </div>

</div>

<div class="form-group">

  {!!Form::label('empresa', 'Empresa Telefonica', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::radio('empresa_telefonica', 'tigo')!!} Tigo

    {!!Form::radio('empresa_telefonica', 'claro')!!} Claro

    {!!Form::radio('empresa_telefonica', 'movistar')!!} Movistar

  </div>

</div>

<div class="form-group">

  {!!Form::label('correo', 'Correo*', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::text('correo_estudiante', null, ['class'=>'form-control', 'placeholder'=>'Dirección de correo personal del estudiante'])!!}

  </div>

</div>

<div class="form-group">

  {!!Form::label('padicimientos', 'Padecimientos', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::textarea('enfermedad_padecida_estudiante', null, ['class'=>'form-control', 'placeholder'=>'Enfermedades padecidas por el estudiante'])!!}

  </div>

</div>

<div class="form-group">

  {!!Form::label('medicamentos', 'Medicamentos', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::textarea('medicamento_recomendado_estudiante', null, ['class'=>'form-control', 'placeholder'=>'Medicamentos recomendados para el estudiante'])!!}

  </div>

</div>

<div class="form-group">

  {!!Form::label('alergias', 'Alergias', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::textarea('alergico_estudiante', null, ['class'=>'form-control', 'placeholder'=>'Medicamentos o sustancias a las que es alergico el estudiante'])!!}

  </div>

</div>

<div class="form-group">

  {!!Form::label('hospital', 'Hospital', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::text('hospital_estudiante', null, ['class'=>'form-control', 'placeholder'=>'En caso de emergencia a que hospital trasladar al estudiante'])!!}

  </div>

</div>

<div class="form-group">

  {!!Form::label('sangre', 'Sangre', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::select('tipo_sangre_estudiante', ['A+'=>'A+', 'A-'=>'A-', 'B+'=>'B+', 'B-'=>'B-', 'AB+'=>'AB+', 'AB-'=>'AB-', 'O+'=>'O+', 'O-'=>'O-', 'O+'=>'O+', 'NO SABE'=>'NO SABE'], null, ['class'=>'form-control', 'placeholder'=>'Tipo de sangre del estudiante...'])!!}

  </div>

</div>

<div class="form-group">

  {!!Form::label('observaciones', 'Observaciones', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::textarea('observaciones_estudiante', null, ['class'=>'form-control', 'placeholder'=>'Observaciones sobre el estudiante...'])!!}

  </div>

</div>



</div>

</div>

