@extends('layouts.app')
@section('content')

<style>
body {
  /* Location of the image */
  background-image: url(/img/INICIO.jpg);
  
  /* Background image is centered vertically and horizontally at all times */
  background-position: center center;
  
  /* Background image doesn't tile */
  background-repeat: no-repeat;
  
  /* Background image is fixed in the viewport so that it doesn't move when 
     the content's height is greater than the image's height */
  background-attachment: fixed;
  
  /* This is what makes the background image rescale based
     on the container's size */
  background-size: cover;

}
</style>
<div class="container" >
    <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel">
               	<div class="panel-heading">
               	<h2>Acceso</h2>
               	</div>
               	<div class="panel-body-success">
                      	<form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}" id="login">
                      	  {{ csrf_field() }}                      
                      	  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">                        <!--  <label for="email" class="col-md-4 control-label">E-Mail Address</label>-->
                      	  <div class="col-md-12">      
                      	  <div class="input-group"> 
                      	         <span class="input-group-addon"><i class="fa fa-user"></i></span>                            {!!Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'Correo institucional...'])!!}                          <!--  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">-->  
                                 </div>
                                 @if ($errors->has('email'))                                  
                                 <span class="help-block">
                                   <strong>{{ $errors->first('email') }}
                                   </strong>                                  
                                 </span>                              @endif                          
                                 </div>           
                                    </div>                      
                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">                          
                                    <div class="col-md-12">                            
                                    <div class="input-group">   
                                      <span class="input-group-addon">
                                      <i class="fa fa-key"></i>
                                      </span>                              {!!Form::password('password', ['class'=>'form-control', 'placeholder'=>'Contrase���a...'])!!}                              <!--<input id="password" type="password" class="form-control" name="password">-->                            
                                      </div>                              
                                      @if ($errors->has('password'))                                  
                                      <span class="help-block">       
                                        <strong>{{ $errors->first('password') }}</strong>
                                      </span>                              @endif                          
                                      </div>                      
                                      </div>                                                 
                                      <div class="col-md-6">                              
                                      <div class="checkbox">                                  
                                      <label>                                      
                                      <input type="checkbox" name="remember"> Recordarme                                  
                                      </label>                              
                                      </div>     
                                      </div>                              
                                      <div class="col-md-6">                                  
                                      <a href="">Olvido clave o usuario</a>
                                      </div>
<button type="submit" class="btn btn-lg btn-principal btn-block">
                                      <span class="fa fa-btn fa-sign-in"></span> Ingresar                              
                                      </button>                  
                                      </form>                
                </div> 
                <BR/>
                 @include('mensajes.msg')           
                </div>        
            </div>    
      </div>
  </div>
  @endsection