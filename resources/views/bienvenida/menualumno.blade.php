<!-- Navigation -->

<nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ url('/') }}"></a><img src="{{ url('/img/LOGO EDUCATIVO-20.png') }}">
    </div>
    <!-- /.navbar-header -->
   
    <ul class="nav navbar-top-links navbar-right">
     <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
    $( function() {
      $( document ).tooltip();
       $(".draggable").draggable();
     
    } );
    </script>

    <H5> 

        @foreach($notificacionesnuevas as $key => $not)
        @if($not->newnot==0)
        <a href="{{ url('notificacionesalumno') }}"><div class="fa fa-envelope-o fa-2x" id="notificaciones" title="No tiene ninguna notificacion nueva, click para ver todas"></div> <span class="label label-danger  pull-center" title="No tiene ninguna notificacion nueva"></a></span>
        @else

          <a href="{{ url('notificacionesalumno') }}"><div class="fa fa-envelope-o fa-2x" id="notificaciones" title="Mis Notificaciones y Mensajes"></div> <span class="label label-danger  pull-center" title="Nuevos Mensajes y notificaciones">{{ mb_strtoupper($not->newnot) }}</a></span>
        @endif
         @endforeach


        @if($actividades->newact==0)
        <a href="{{ url('todasmisactividades') }}"><div class="fa fa-pencil-square fa-2x" id="notificaciones" title="No tiene ninguna tarea nueva, click para ver todas"></div> <span class="label label-danger  pull-center" title="No tiene ninguna notificacion nueva"></a></span>
        @else

          <a href="{{ url('todasmisactividades') }}"><div class="fa fa-pencil-square fa-2x" id="notificaciones" title="Tareas pendientes de entregar"></div> <span class="label label-danger  pull-center" title="Nuevas tareas y actividades">{{ucwords($actividades->newact)}}</a></span>
        @endif
        

         
   
        <li class="dropdown">   
            <a class="dropdown-toggle" id="nombre" data-toggle="dropdown" href="#"> 
                 @if((Auth::user()->url)==null)
                    <div class="fa fa-user fa-2x"></div>
                    @else
                    <img src="{{ ucwords(Auth::user()->url) }}" width="40" height="30">
                    @endif  
                     {{ ucwords(Auth::user()->name) }}
                  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="{{ url('miperfil') }}"><i class="fa fa-user fa-fw"></i> Perfil de Usuario</a>
                </li>

                <li><a href="{{ url('helpdesk') }}"><i class="fa fa-exclamation-circle"></i> Ayuda y Soporte</a>
                </li>  
                <li class="divider"></li>
                <li><a href="{{ url('/logout') }}"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    </H5>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse" id="menus">
          <!-- Crea el html para el menu lateral izquierdo -->
          {!! Menu::make($items, 'nav') !!}
          <!-- Fin de la creación del menu lateral izquierdo -->

        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>


<!-- /#Navigation -->
