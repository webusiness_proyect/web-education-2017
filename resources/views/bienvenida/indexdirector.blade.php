<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sistema Educativo - WB</title>
    <!-- Arhivos css para estilos-->
    @include('plantillas.estilos')
    <!-- /#fin del los archivos css -->


 
    
    <script>
    $( function() {
      $( document ).tooltip();
       $(".draggable").draggable();
     
    } );
    </script>
  </head>

@include('plantillas.menu')
         

  <body>
    <div class="wraper">
      <div id="page-wrapper">
      <div class="row">
          <div class="col-lg-12">
            <div>
                   <H3> <center> <div class="fa fa-user" aria-hidden="true"></div> BIENVENIDOS {{ ucwords(Auth::user()->name) }} </center></H3>
                </div>
               <div class="panel panel-primary">
               
                <div class="panel-footer btn-warning" title="click aqui para mas informacion" >  
                <H4> 
                  <center>
                  <a href="#demo2"  data-toggle="collapse">
                  <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i><strong>INFORMACION</strong></a>
                  <center>
                  <div id="demo2" class="collapse">
                    
                  <strong>En esta plataforma tu podras ADMINISTRAR, BUSCAR, MODIFICAR todas las notas asignadas por los profesores por grado, carrera, nivel, año, etc asi como imprimir reportes de notas</H4>


                  </div>

               </div>

                <div >
                   <H3> <center>ACCESO RAPIDO DEL DIRECTOR</center></H3>
                </div>


               
                   <div  class="table-responsive">
                    <table class="">

                      <thead>

                        <tr>
                          <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                          <link rel="stylesheet" href="/resources/demos/style.css">
                          <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                          <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                          <script>
                            $( function() {
                              $( document ).tooltip();
                            } );
                            </script>
                         <th>
                            <a href="#" class="btn btn-success btn-lg selected" title="Graficas y estadisticas en tiempo real"><i class="fa fa-pie-chart  fa-2x" aria-hidden="true"></i><br/>Graficas</a>
 

 
                          </th>

                           <th>   

                            <a href="{{ url('/administra_notas_niveles') }}" class="btn btn-default btn-lg" title="Crea actividades o tareas para que tus alumnos puedan resolverlas en clase o en casa y despues adjuntar la tarea en un documento digital"><i class="fa fa-book fa-2x" aria-hidden="true"></i><br/>Administra notas </a>

                          </th>

                          <th>   

                            <a href="{{ url('/horarios') }}" class="btn btn-default btn-lg" title="Mantengase informado de los cursos que se dan en su institucion, la hora exacta y mucho mas"><i class="fa fa-calendar fa-2x" aria-hidden="true"></i><br/>Horarios de su institucion</a>

                          </th>

                           <th>   

                            <a href="{{ url('/docente') }}" class="btn btn-default btn-lg" title="Busque Alumnos en su institucion"><i class="fa fa-pencil fa-2x" aria-hidden="true"></i><i class="fa fa-user fa-2x" aria-hidden="true"></i><br/>Buscar alumnos</a>

                          </th>

                             <th>   

                            <a href="{{ url('/misforosdocente') }}" class="btn btn-default btn-lg" title="Busque docentes de su institucion"><i class="fa fa-comments fa-2x" aria-hidden="true"></i><br/>Buscar Docentes</a>

                          </th>

                         

                            
                        </thead>

                      </table>
                    </div>
              



          </div>

          <!-- /.col-lg-12 -->

      </div>
      <br/>

        <br/>
        <div class="row ">
        <div class="col-sm-2 panel panel-default" >
          <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                <center>
                  <a href="{{ url('/mishorariosdocente') }}" title="Ver todos los horarios"><div class="fa fa-calendar fa-2x"></div>
                  </a>
                  </center>
                </th>
              </tr>
            </thead>
            <tbody>
             
             

             
            
            </tbody>
          </table>
        </div>


          <div class="col-lg-6 panel panel-default">

           
                  


               <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                  <script type="text/javascript">
                   google.charts.load('current', {'packages':['corechart']});
                   google.charts.setOnLoadCallback(drawChart);
                   function drawChart() {

                      var data = google.visualization.arrayToDataTable([
                       ['Task', 'Hours per Day'],
                       ['CALIFICADAS',     11],
                      
                       ['PENDIENTES DE CALIFICAR',    7]
                      ]);

                     var options = {
                        title: 'TAREAS PENDIENTE DE CALIFICAR'
                      };

                      var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                      chart.draw(data, options);
                    }
                  </script>


              <div id="piechart"></div>

             <br/>
             
                 



          </div>

          <div class="col-lg-4">
              <center>
              <img src="{{ url('/img/docente.png') }}"  width="250" height="450">
              </center>
          </div>

          

          <!-- /.col-lg-12 -->

      </div>



  </div>

  <!-- /#page-wrapper -->

  <!-- /#page-wrapper -->
 @include('plantillas.footer')



  <!-- /#fin de los archivos javascript -->

  </body>

</html>

