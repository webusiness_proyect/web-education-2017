<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sistema Educativo - WB</title>
    <!-- Arhivos css para estilos-->
    @include('plantillas.estilos')
    <!-- /#fin del los archivos css -->


 
    
    <script>
    $( function() {
      $( document ).tooltip();
       $(".draggable").draggable();
     
    } );
    </script>
  </head>

@include('bienvenida.menudocente')
         

  <body>
    <div class="wraper">
      <div id="page-wrapper">
      <div class="row">
          <div class="col-lg-12">
            <div>
                   <H3> <center> <div class="fa fa-user" aria-hidden="true"></div> BIENVENIDOS {{ ucwords(Auth::user()->name) }} </center></H3>
                </div>
               <div class="panel panel-primary">
               
                <div class="panel-footer" title="click aqui para mas informacion" >  
                <H4> 
                  <center>
                  <a href="#demo2"  data-toggle="collapse">
                  <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i><strong>MAS INFORMACION</strong></a>
                  <center>
                  <div id="demo2" class="collapse">
                    
                  En esta plataforma tu podras <strong>CREAR, ADMINISTRAR </strong> todos los cursos asignados por tu institución.
                  <br/>
                  <strong>¿QUE PUEDO HACER?</strong>
                   <li>Crear actividades como conversaciones o foros de temas importantes de tu clase</li>
                  </li>Asignar tareas o actividades para que sus alumnos resuelvan y las adjunten en un documento de word, pdf, imagen y pueda ser calificada por ti</li>
                  <li> Ver los horarios asginados de tus cursos en tiempo real </li>
                  <li> Recibe notificaciones de tus superiores con reuniones, información y mucho mas </li>
                  <li>Asigna notas por las actividades recibidad y muchas cosas más</li>
                  </H4>


                  </div>

               </div>

                <div>
                   <H3> <center>ACCESO RAPIDO DEL DOCENTE</center></H3>
                </div>


               
                   
                      
                          <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                          <link rel="stylesheet" href="/resources/demos/style.css">
                          <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                          <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                          <script>
                            $( function() {
                              $( document ).tooltip();
                            } );
                            </script>
                         
                            <a href="#" class="btn btn-success btn-lg selected" title="Graficas y estadisticas en tiempo real"><i class="fa fa-pie-chart  fa-2x" aria-hidden="true"></i><br/>Estadísticas</a>
 

   

                            <a href="{{ url('/misactividadesdocente') }}" class="btn btn-default btn-lg" title="Crea actividades o tareas para que tus alumnos puedan resolverlas en clase o en casa y despues adjuntar la tarea en un documento digital"><i class="fa fa-check fa-2x" aria-hidden="true"></i><br/>Nuevas Actividades y tareas (+{{ucwords($pendientesentrega->newpendientes)}})</a>

                            

                            <a href="{{ url('/mishorariosdocente') }}" class="btn btn-default btn-lg" title="Administre su tiempo de manera efectiva sabiendo que curso esta en este horario"><i class="fa fa-calendar fa-2x" aria-hidden="true"></i><br/>Mi horarios</a>

                           

                            <a href="{{ url('/docente') }}" class="btn btn-default btn-lg" title="Ve todos tus cursos y notas asignadas a tus cursos"><i class="fa fa-pencil fa-2x" aria-hidden="true"></i><i class="fa fa-user fa-2x" aria-hidden="true"></i><br/>Mis cursos y alumnos </a>

                        

                            <a href="{{ url('/misforosdocente') }}" class="btn btn-default btn-lg" title="cree foros y conversaciones en los que participes tu y tus alumnos sobre un tema en especial de tu curso "><i class="fa fa-comments fa-2x" aria-hidden="true"></i><br/>Mis foros</a>

                    



          </div>

          <!-- /.col-lg-12 -->

      </div>
      <br/>

        <br/>
        <div class="row ">
        <div class="col-sm-2 panel panel-default" >
          <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                <center>
                  <a href="{{ url('/mishorariosdocente') }}" title="Ver todos los horarios"><div class="fa fa-calendar fa-2x"></div>{{ ucwords($today)}} 
                  </a>
                  </center>
                </th>
              </tr>
            </thead>
            <tbody>
             
              @foreach($horarios as $key => $horario)
                <tr>
                
                  
                   

                  <td>
                      <h6>
                     <b>INICIA:</b>{{ mb_strtoupper($horario->inicio) }}
                     <b>FIN:</b>{{ mb_strtoupper($horario->fin) }}<br/>
                     <b>CURSO:{{ mb_strtoupper($horario->nombre_area) }}</b>
                     
                  
                     
                    </h6>
                    
                     


                  </td>
                </tr>
                @endforeach


             
            
            </tbody>
          </table>
        </div>


          <div class="col-lg-6 panel panel-default">

           <br/>
                  


               <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                  <script type="text/javascript">
                   google.charts.load('current', {'packages':['corechart']});
                   google.charts.setOnLoadCallback(drawChart);
                   function drawChart() {

                      var data = google.visualization.arrayToDataTable([
                       ['Task', 'Hours per Day'],
                       ['CALIFICADAS',     11],
                      
                       ['PENDIENTES DE CALIFICAR',    7]
                      ]);

                     var options = {
                        title: 'TAREAS PENDIENTE DE CALIFICAR'
                      };

                      var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                      chart.draw(data, options);
                    }
                  </script>


              <div id="piechart"></div>

             <br/>
             
                 



          </div>

          <div class="col-lg-4">
              <center>
              <img src="{{ url('/img/docente.png') }}"  width="200" height="350">
              </center>
          </div>

          

          <!-- /.col-lg-12 -->

      </div>



  </div>

  <!-- /#page-wrapper -->

  <!-- /#page-wrapper -->
 @include('plantillas.footer')



  <!-- /#fin de los archivos javascript -->

  </body>

</html>

