

<!DOCTYPE html>

<html lang="en">

  <head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>FORMULARIO DE INSCRIPCION</title>

   

  </head>

  <body>

 

    <main>

      <div id="details" class="clearfix">

        <div id="invoice">
          
            
          <center>

          <h3><img src="{{ url('/img/LOGO EDUCATIVO-19.png') }}">FICHA DE INSCRIPCION NO {{ $data['id'] }}</h3>
        
          </center>

        </div>

      </div>
      <center>
        <div align="center">

      <table border="1" align="center" WIDTH="100%"cellspacing="0" cellpadding="0">

        


            <tr>
             <td rowspan="12" bgcolor= "#A9BCF5">
             <center>
                Información Personal</br> del alumno
             </center>
             </td>
             <td>Apellidos: </td>
              <td>{{ $data['apellidos'] }}</td>
            </tr>

           <tr>
            <td>Nombres: </td>
              <td>{{ $data['nombre'] }}</td>
              
            </tr>

            <tr>
              <td>Fecha de nacimiento:  </td>
              <td>{{ $data['fecha'] }}</td>
            </tr>

            <tr>
              <td>Carrera/ciclo:  </td>
              <td>{{ $data['nombre_carrera'] }}</td>
            </tr>

             <tr>
              <td>Grado:  </td>
              <td>{{ $data['nombre_grado'] }}</td>
            </tr>

            <tr>
              <td>Direccion:  </td>
               <td>{{ $data['direccion'] }}</td>
            </tr>

            <tr>
              <td>Colonia: </td>
              <td>{{ $data['colonia'] }}</td>
            </tr>

            <tr>
              <td>Zona: </td>
              <td>{{ $data['zona'] }}</td>
            </tr>
            
            <tr>
              <td>Municipio: </td>
              <td>{{ $data['nombre_municipio'] }}</td>
            </tr>

            <tr>
              <td>Telefono de casa/Celular:  </td>
              <td>{{ $data['telefono'] }}</td>
            </tr>

            <tr>
              <td>Fecha de inscripción:  </td>
              <td>{{ $data['fecha_registro_estudiante'] }}</td>
            </tr>
             <tr>
              <td>Ciclo Escolar:  </td>
              <td>{{ $data['telefono'] }}</td>
            </tr>

            <tr>
            <td rowspan="3" bgcolor= "#A9BCF5">
             <center>
                Referencias 
             </center>
             </td>

              <td>Parentesco:   </td>
              <td>{{ $data['parentesco_referencia'] }}</td>
            </tr>

            <tr>
              <td>Nombre:  </td>
              <td>{{ $data['referencia'] }}</td>
            </tr>

            <tr>
              <td>Telefono:  </td>
              <td>{{ $data['telefono_referencia'] }}</td>
            </tr>
            <tr>
             <td rowspan="11" bgcolor= "#A9BCF5">
             <center>
                Información del encargado
             </center>
            
              <td>Apellidos:  </td>
              <td>{{ $data['apellidos_tutor'] }}</td>
            </tr>


            <tr>
              <td>Nombres:   </td>
              <td>{{ $data['nombre_tutor'] }}</td>
            </tr>

            <tr>
              <td>DPI:  </td>
              <td>{{ $data['cui_tutor'] }}</td>
            </tr>

            <tr>
              <td>Dirección:  </td>
              <td>{{ $data['direccion_tutor'] }}</td>
            </tr>

            <tr>
              <td>Teléfono/Celular: </td>
              <td>{{ $data['telefono_primario_tutor'] }}</td>
            </tr>

            <tr>
              <td>Correo electrónico:  </td>
              <td>{{ $data['correo_electronico_tutor'] }}</td>
            </tr>

            <tr>
              <td>Lugar de Trabajo:   </td>
              <td>{{ $data['telefono'] }}</td>
            </tr>

            <tr>
              <td>Dirección de Trabajo:  </td>
              <td>{{ $data['direccion_tutor'] }}</td>
            </tr>

            <tr>
              <td>Linea Telefonica:  </td>
              <td>{{ $data['telefono_trabajo_tutor'] }}</td>
            </tr>

            <tr>
              <td>Cargo:  </td>
              <td>{{ $data['cargo_tutor'] }}</td>
            </tr>

            <tr>
              <td>Telefono de casa/Celular:  </td>
              <td>{{ $data['telefono'] }}</td>
            </tr>

            <tr>
             <td rowspan="5" bgcolor= "#A9BCF5">
             <center>
                Información Médica 
             </center>
            
              <td>Enfermedades que padece:  </td>
              <td>{{ $data['enfermedad_padecida_estudiante'] }}</td>
            </tr>

            <tr>
              <td>Medicamentos recomendados:  </td>
              <td>{{ $data['medicamento_recomendado_estudiante'] }}</td>
            </tr>
            <tr>
              <td>Es alérgico a:  </td>
              <td>{{ $data['alergico_estudiante'] }}</td>
            </tr>
            <tr>
              <td>Emergencia llevar a hospital:  </td>
              <td>{{ $data['hospital_estudiante'] }}</td>
            </tr>
            <tr>
              <td>Tipo de sangre:  </td>
              <td>{{ $data['tipo_sangre_estudiante'] }}</td>
            </tr>

             <tr>
             <td rowspan="5" bgcolor= "#A9BCF5">
             <center>
                Cuotras Autorizadas
             </center>
            
              <td>Inscripción:  </td>
              <td></td>
            </tr>

            <tr>
              <td>Colegiaturas:  </td>
              <td></td>
            </tr>
            <tr>
              <td>Derecho de examen:  </td>
              <td></td>
            </tr>
            <tr>
              <td>Graduación:  </td>
              <td></td>
            </tr>
            <tr>
              <td>Carné: </td>
              <td></td>
            </tr>

            <tr>

              <td bgcolor= "#A9BCF5"><center>observaciones </center> </td>
              <td colspan="2" >{{ $data['observaciones_estudiante'] }} </td>
            
              
            </tr>


           
      </table>
        acceso como alumno: <b>{{ $data['email'] }} </b>
        contraseña: <b> estudiante123</b><br/>
         acceso como padre: <b>{{ $data2['email'] }} </b>
         contraseña: <b> padres123</b>
    </center>
  </div>

  </body>

</html>