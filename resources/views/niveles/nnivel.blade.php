@extends('principal')

@section('titulo')
  <title>Nuevo Nivel</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nuevo Nivel</h1>
        <p>
          Nota: Todos los cmapos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::open(['route'=>'niveles.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'nnivel'])!!}
          @include('niveles.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
