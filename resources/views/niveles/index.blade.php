@extends('principal')

@section('titulo')
  <title>Niveles</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel-body ">
        <h1 class="page-header text-center"> <div class="fa fa-sliders" aria-hidden="true"></div>Niveles</h1>
        <div class="panel panel-default">
           <div class="row">
            <div class="col-sm-10">
           
            <center><a href="#demo"  data-toggle="collapse"><div class="fa fa-question-circle fa-2x "></div><H4><STRONG>INFORMACION</STRONG></H4></a></center>
            <div id="demo" class="collapse">
             
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra niveles educativos para tu institución disponibles en tu region ejemplo:<strong>PRIMARIA</strong>
            </h4>
          </center>
        </div>
      </div>
      </div>
      </div>
      </div>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12 panel panel-default">
        <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
        @permission('crear-nivel')
        {!!link_to_route('niveles.create', $title = 'Nuevo Nivel', $parameters =  null, $attributes =['class'=>'btn btn-primary fa fa-plus-square'])!!}
        <br>
        <br>
        @endpermission
        @if(count($niveles) == 0)
          <p class="text-info">
            No se han registrado niveles aun...
          </p>
        </center>
        @else
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NOMBRE NIVEL
                </th>
                @permission('editar-nivel')
                <th>
                  ACTUALIZAR
                </th>
                @endpermission
                @permission('estado-nivel')
                <th>
                  ESTADO
                </th>
                @endpermission
              </tr>
            </thead>
            <tbody id="datosNiveles">
              @foreach($niveles as $key => $nivel)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($nivel->nombre_nivel) }}
                  </td>
                  @permission('editar-nivel')
                  <td>
                    {!!link_to_route('niveles.edit', $title = 'Editar', $parameters = $nivel->id_nivel, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}
                  </td>
                  @endpermission
                  @permission('estado-nivel')
                  <td>



                 

                     @if($nivel->estado_nivel == TRUE)
                              <input type="checkbox" name="estado" checked value="{{ $nivel->id_nivel }}" class="toggleEstado">
                            @else
                              <input type="checkbox" name="estado" value="{{ $nivel->id_nivel }}" class="toggleEstado">
                            @endif
                  </td>
                  @endpermission
                </tr>
              @endforeach
            </tbody>
          </table>
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        @endif
      </div>
    </div>
    @include('mensajes.carga')
  </div>
@endsection
