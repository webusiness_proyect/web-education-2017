@extends('principal')

@section('titulo')
  <title>Editar Nivel</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="col-sm-12">
      <h1 class="page-header text-center">Editar Nivel</h1>
      <p>
        Nota: Todos los campos con (*) son obligatorios.
      </p>
      @include('mensajes.errores')
    </div>
    <div class="col-sm-12">
      {!!Form::model($nivel, ['route'=>['niveles.update', $nivel->id_nivel], 'method'=>'PUT', 'class'=>'form-horizontal'])!!}
        @include('niveles.form.campos')
      {!!Form::close()!!}
    </div>
  </div>
@endsection
