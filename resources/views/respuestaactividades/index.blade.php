@extends('principal')



@section('titulo')

  <title>Actividades</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center">ACTIVIDADES</h1>

        @include('mensajes.msg')

      </div>

      <div class="col-sm-12">

        <p>

       

        </p>

        @if (count($actividades) == 0)

          <p class="text-info">

            No se han creado actividades en este curso...

          </p>

        @else

          <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>

                  <th>

                    NO

                  </th>

                  <th>

                    NOMBRE ACTIVIDAD

                  </th>

                  <th>

                    DESCRIPCION ACTIVIDAD

                  </th>

                  <th>

                    FECHA DE INICIO

                  </th>

                  <th>

                    FECHA ENTREGA

                  </th>

                  <th>

                    NOTA TOTAL

                  </th>

                  <th>

                    EDITAR

                  </th>

                  <th>

                    VER ENTREGADOS

                  </th>

                </tr>

              </thead>

              <tbody>

                @foreach ($actividades as $key=>$actividad)

                  <tr>

                    <td>

                        {{ $key+1 }}

                    </td>

                    

                    <td>

                    {{ mb_strtoupper($actividad->nombre_actividad) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($actividad->descripcion_actividad) }}

                  </td>

                  <td>

                   {{ mb_strtoupper($actividad->fecha_inicio) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($actividad->fecha_entrega) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($actividad->nota_total) }}

                  </td>

                    <td>

                      @can('owner', $actividad)

                      {!!link_to_route('actividades.edit', $title = ' Editar', $parameters = $actividad->id_actividad, $attributes = ['class'=>'btn btn-primary fa fa-edit'])!!}

                      @endcan

                    </td>

                    <td>

                      {!!link_to_route('respuestasforos.create', $title = 'Ver Entregas', $parameters = ['actividad'=>$actividad->id_actividad], $attributes = ['class'=>'btn btn-info '])!!}

                    </td>

                  </tr>

                @endforeach

              </tbody>

            </table>

             <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

          </div>

        @endif

      </div>

    </div>

  </div>

@endsection

