@extends('principal')

@section('title')
  <title>Respuesta del Foro</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center"> <div class="fa fa-comments"></div>Foros del curso de {!! $foro->nombre_area !!}</h1>
      </div>
      <div class="col-sm-12">
      <center>   {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}</center>
      <br/>
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h2 class="panel-title"><H1> <div class="fa fa-comment-o"></div> TEMA PRINCIPAL: {{ $foro->titulo_foro }}</H1>  {!! $foro->nombre !!} - {!! $foro->rol !!} </h2> 
          </div>
          <div class="panel-body">
            {!! $foro->mensaje_foro !!}
            {!!link_to_route('respuestasforos.create', $title = ' Responder', $parameters = ['foro'=>$foro->id_foro], $attributes = ['class'=>'btn btn-info fa fa-comments'])!!}
          </div>
          <div class="panel-body">
            Fecha de creacion: {!! $foro->fecha_foro !!}

          </div>
        </div>
      </div>
      @foreach ($respuestas as $respuesta)

      @if($respuesta['rol']=='Profesor')

        <div class="panel panel-primary">
          <div class="panel-heading">
            <h2 class="panel-title"><div class="fa fa-comment-o"></div>{{ ucwords($respuesta['nombre'].' - '.$respuesta['rol']) }} </h2> 
          </div>
          <div class="panel-body">
            {!! $respuesta['respuesta'] !!}
          </div>
          <div class="panel-body">
            Fecha de respuesta: {{ $respuesta['fecha'] }}

          </div>
        </div>

        @else




      <div class="col-sm-offset-2 col-sm-10">
        <div class="panel panel-success">
          <div class="panel-heading">
            <h3 class="panel-title"><div class="fa fa-comments-o"></div>{{ ucwords($respuesta['nombre'].' - '.$respuesta['rol']) }}</h3>
          </div>
          <div class="panel-body">
            {!! $respuesta['respuesta'] !!}
          </div>
          <div class="panel-footer text-right">
             {!!link_to_route('respuestasforos.create', $title = ' Responder', $parameters = ['foro'=>$foro->id_foro], $attributes = ['class'=>'btn btn-info fa fa-comments'])!!} Fecha de respuesta: {{ $respuesta['fecha'] }}
          </div>
        </div>


      </div>

      @endif
    @endforeach



    </div>
  </div>
@endsection
