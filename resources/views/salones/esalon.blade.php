@extends('principal')

@section('titulo')
  <title>Editar Salón</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Edita Salón</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::model($salon, ['route'=>['salones.update', $salon->id_salon], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'nsalon'])!!}
          @include('salones.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
