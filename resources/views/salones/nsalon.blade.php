@extends('principal')

@section('titulo')
  <title>Nuevo Salón</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nuevo Salón</h1>
        <p>
          Nota: Todos los capos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::open(['route'=>'salones.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'nsalon'])!!}
          @include('salones.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
