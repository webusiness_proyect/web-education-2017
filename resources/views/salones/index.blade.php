@extends('principal')

@section('titulo')
  <title>Salones</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
         <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-home " aria-hidden="true"></div> Salones de clases disponibles </h1>
         <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong>y administra salones de clases disponibles en tu institucion ejemplo:<strong> SALON:1</strong>
            </h4>
           
          </center>
        </div>
    
         

        @include('mensajes.msg')
        </div>
      </div>
      <div class="col-sm-12 panel panel-default">

        <center>
        <br/>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
        @permission('crear-salon')
        {!!link_to_route('salones.create', $title = 'Nuevo Salón', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square'])!!}


        @endpermission
        @include('salones.search')
        @if(count($salones) == 0)
          <p class="text-info">
            No se han registrado salones aun.
          </p>
        </center>
        @else
        <div class="col-sm-12 panel panel-default">
          <table class="table table-hover table-bordered">
            <thead>
              <th>
                NO
              </th>
              <th>
                NOMBRE
              </th>
              @permission('editar-salon')
              <th>
                ACTUALIZAR
              </th>
              @endpermission
              @permission('estado-salon')
              <th>
                ESTADO
              </th>
              @endpermission
            </thead>
            <tbody id="datosSalones">
              @foreach($salones as $key => $salon)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($salon->nombre_salon) }}
                  </td>
                  @permission('editar-salon')
                  <td>
                    {!!link_to_route('salones.edit', $title = 'Editar', $parameters = $salon->id_salon, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}
                  </td>
                  @endpermission
                  @permission('estado-salon')
                  <td>

                  @if($salon->estado_salon == TRUE)
                              <input type="checkbox" name="estado" checked value="{{ $salon->id_salon }}" class="toggleEstado">
                            @else
                              <input type="checkbox" name="estado" value="{{ $salon->id_salon }}" class="toggleEstado">
                            @endif



                  </td>
                  @endpermission
                </tr>
              @endforeach
            </tbody>
          </table>
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
          @include('mensajes.carga')
           <div class="text-center">

            {!! $salones->links() !!}

          </div>
        @endif

      </div>
      </div>
    </div>
  </div>
@endsection
