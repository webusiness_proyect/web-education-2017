@extends('principal')





@section('titulo')


  <title>Pensum Grados</title>


@endsection





@section('cuerpo')


  <div id="page-wrapper">


    <div class="row">


      <div class="col-sm-12">


      <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-bookmark" aria-hidden="true"></div>Pensum de grados</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CONFIGURA </strong> tu pensum, selecciona un grado, asignales cursos y asignales un salon ejemplo:<strong>*PRIMERO PRIMARIA, CURSO DE MATEMATICAS, INGLES, ETC EN EL SALON A</strong>
            </h4>
           
          </center>
        </div>
          <h6>
         <CENTER>
          * DEBERAS DE CONFIGURAR EL <a href="{{ url('/asignarniveles') }}">PLAN NIVELES</a> PREVIAMENTE

            
          </CENTER>
          </h6>

        @include('mensajes.msg')

        </div>


      </div>


 
       <div class="col-sm-12 panel panel-default">

          <br/>
           <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}


        @permission('crear-pensum')


        {!!link_to_route('pensum.create', $title = 'Nuevo Pensum', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square'])!!}

        @include('asignacionareas.search')


        <br>


        <br>


        @endpermission


        @if(count($pensum) == 0)


          <p class="text-info">


            No se ha registrado ningun pensum aun.


          </p>
          </center>


        @else


          <div class="col-sm-12 panel panel-default table-responsive">


            <table class="table table-hover">


              <thead>


                <tr>


                  <th>


                    NO


                  </th>


                  <th>


                    GRADO


                  </th>


                  <th>


                    NIVEL


                  </th>


                  <th>


                    CARRERA


                  </th>


                  @permission('editar-pensum')


                  <th>


                    ACTUALIZAR


                  </th>


                  @endpermission


                  @permission('ver-pensum')


                  <th>


                    VER


                  </th>


                  @endpermission

                  




                </tr>


              </thead>


              <tbody id="datosPensum">


                @foreach($pensum as $key => $p)


                  <tr>


                    <td>


                      {{ $key+1 }}


                    </td>


                    <td>


                      {{ mb_strtoupper($p->nombre_grado) }}


                    </td>


                    <td>


                      {{ mb_strtoupper($p->nombre_nivel) }}


                    </td>


                    <td>


                      {{ mb_strtoupper($p->nombre_carrera) }}


                    </td>


                    @permission('editar-pensum')


                    <td>


                      {!!link_to_route('pensum.edit', $title = 'Editar', $parameters = $p->id_nivel_grado, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}


                    </td>


                    @endpermission


                    @permission('ver-pensum')


                    <td>


                      {!!link_to_route('pensum.show', $title = 'Ver Pensum', $parameters = $p->id_nivel_grado, $attributes = ['class'=>'btn btn-default fa fa-eye'])!!}


                    </td>


                    @endpermission






                  </tr>


                @endforeach


              </tbody>


            </table>

            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

              @include('mensajes.carga')

            <div class="text-center">

            {!! $pensum->links() !!}

          </div>


          </div>


        @endif


      </div>


    </div>


  </div>


@endsection


