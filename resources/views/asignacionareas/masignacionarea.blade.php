@extends('principal')

@section('titulo')
  <title>Pensum del Grado</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Pensum del Grado</h1>
        <h3 class="text-center">{{ ucwords($pensum[0]->nombre_grado.' '.$pensum[0]->nombre_nivel.' '.$pensum[0]->nombre_carrera) }}</h3>
      </div>
      <div class="col-sm-12">
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <th>
                NO
              </th>
              <th>
                CURSO
              </th>
              <th>
                SALON
              </th>
            </thead>
            <tbody>
              @foreach($pensum as $key => $p)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($p->nombre_area) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($p->nombre_salon) }}
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
