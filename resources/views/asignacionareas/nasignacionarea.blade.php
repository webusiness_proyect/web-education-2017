@extends('principal')





@section('titulo')


  <title>Nuevo Pensum</title>


@endsection





@section('cuerpo')


  <div id="page-wrapper">


    <div class="row">


      <div class="col-sm-12">

      <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-plus" aria-hidden="true"></div><div class="fa fa-bookmark" aria-hidden="true"></div> - Nuevo Pensum de grados</h1>

         <p>


          Nota: Todos los campos con (*) son obligatorios.


        </p>


        @include('mensajes.errores')

        </div>


      </div>


      
       


      </div>


      <div class="col-sm-12">


        <ul class="nav nav-tabs">


          <li class="active"><a data-toggle="tab" href="#bgrados">Paso 1 - Agregar grados a jornada</a></li>


          <li><a data-toggle="tab" href="#acurso">Paso 2 - Agregar salon a cursos </a></li>


          <li><a data-toggle="tab" href="#gcambios">Paso 3 - Registrar Pensum</a></li>


        </ul>





            {!!Form::open(['route'=>'pensum.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'pensum'])!!}


              @include('asignacionareas.form.campos')





            {!!Form::close()!!}


          @include('mensajes.carga')


      </div>


    </div>


  </div>


@endsection


