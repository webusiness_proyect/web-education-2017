@extends('principaldocente')



@section('titulo')

  <title>Mis Cursos Asignados</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

      <di <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-book" aria-hidden="true"></div> - - Mis Actividades y tareas </h1>
        <h4 class="text-center"><div class="glyphicon glyphicon-cog"></div> {{mb_strtoupper($unidad->nombre_unidad) }}</h4>
        <div class="panel panel-default">
          <center>
            <h4>
              <a href="#demo2" class="btn btn-warning" data-toggle="collapse">
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" title="click aqui para mas informacion" aria-hidden="true"></i></a>  CREA TAREAS</strong> y <STRONG>CALIFICA TAREAS </STRONG> en tus cursos asignados, para que tus alumnos puedan realizarlas en horario normal de clases o en sus casas
            </h4>

          
           
          </center>
          <div id="demo2" class="collapse">
          <div class="alert alert-warning alert-dismissable">
          <center>

            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-info btn btn-default" aria-hidden="true"></i><STRONG> ESTAS TAREAS PUEDEN SER ENTREGADAS EN UN ARCHIVO DIGITAL EN DOCUMENTO DE WORD, PDF, HOJA DE EXCEL, DIAPOSITIVA DE POWER POINT O IMAGEN PNG O JPG</a> </STRONG><br/>
           

            
          </CENTER>
    
        </div>
        </div>
        </div>

        @include('mensajes.msg')

        </div>
        

           <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
       
      </center>
         
        <div class="col-sm-12 panel panel-default">

          <br/>



        <div class="table-responsive">

          <table class="table table-hover table-bordered">

            <thead>

              <tr>

               
               <th>

                 <h4><STRONG><CENTER> ACTIVIDADES Y TAREAS DE MIS CURSOS</CENTER></h4></STRONG>

                </th>

              

                <th>

                 <h4><STRONG><CENTER> PENSUM </CENTER></h4></STRONG>

                </th>

                

                <th>

                  <h4><STRONG><CENTER>CARRERA</CENTER></h4></STRONG>

                </th>

                <th>

                  <h4><STRONG><CENTER>SECCION</CENTER></h4></STRONG>

                </th>
                

              </tr>

            </thead>

            <tbody>

              @foreach($cursos as $key => $curso)

                <tr>

                   <td>

                 


                    <!-- Single button -->

                    <center>


        

                    <a href="/Actividad?a={{$curso->id_area}}&b={{ $curso->id_asignacion_docente }}&c={{ $curso->id_asignacion_area }}" class="btn btn-success" title="Click aqui para ver mis actividades y tareas calificadas y pendientes para calificar de este curso"><div class="fa fa-pencil-square"></div>{{ mb_strtoupper($curso->nombre_area) }}</a>

                    
                    </center>

            

             


                    {{-- {!!link_to_route('docente.create', $title = ' Zona', $parameters = ['a'=>$curso->id_asignacion_area,'b'=>$curso->id_asignacion_docente,'c'=>$curso->id_unidad], $attributes = ['class'=>'btn btn-primary fa fa-edit'])!!}

                    --}}

                  </td>

                  

                  <td>

                      {{ mb_strtoupper($curso->nombre_grado) }}   
                      {{ mb_strtoupper($curso->nombre_nivel) }}    
                      {{ mb_strtoupper($curso->nombre_plan) }}
                      {{ mb_strtoupper($curso->nombre_jornada) }}

                  </td>
                  <td>
                  {{ mb_strtoupper($curso->nombre_carrera) }}
                  </td>

                  <td>

                    {{ mb_strtoupper($curso->nombre_seccion) }}

                  </td>

                 

                </tr>

              @endforeach

            </tbody>

          </table>

        </div>

      </div>

      <div class="col-sm-12 text-center">

        {{ $cursos->setPath('misactividadesdocente')->links() }}

      </div>

    </div>

  </div>

@endsection

