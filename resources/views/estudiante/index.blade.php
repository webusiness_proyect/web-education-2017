@extends('principalalumno')



@section('titulo')

  <title>Mis Cursos</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center"><div class="glyphicon glyphicon-th"></div>  Mis Cursos</h1>

        <h3 class="text-center"><div class="glyphicon glyphicon-cog"></div>   {{ mb_strtoupper($unidad->nombre_unidad) }}</h3>

        @include('mensajes.msg')

      </div>

      <div class="col-sm-12">

        <div class="table-responsive">

          <table class="table table-hover table-bordered">

            <thead>

              <tr>

                <th>

                  NO

                </th>

                <th>

                  CURSO

                </th>

                <th>

                  ACCIONES

                </th>

              </tr>

            </thead>

            <tbody>

              @foreach ($cursos as $key => $curso)

                <tr>

                  <td>

                    {{ $key+1 }}

                  </td>

                  <td>

                    {{ mb_strtoupper($curso->nombre_area) }}

                  </td>

                  <td>

                    <!-- Single button -->

                    <div class="btn-group">

                      <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                        Seleccione... <span class="caret"></span>

                      </button>

                      <ul class="dropdown-menu">

                        <li><a href="/misactividades?a={{$curso->id_area}}">Mis Tareas de este curso</a></li>

                        <li><a href="/foros?a={{ $curso->id_asignacion_area }}">Mis Foros de este curso</a></li>

                        <li><a href="/MisNotas?a={{ $curso->id_area }}&b={{ $curso->id }}">Mis notas de este curso</a></li>

                      

                    </div>

                  </td>

                </tr>

              @endforeach

            </tbody>

          </table>

        </div>

      </div>

    </div>

  </div>

@endsection

