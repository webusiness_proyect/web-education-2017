@extends('principal')

@section('titulo')
  <title>Carreras</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center "> <div class="fa fa-graduation-cap"></div>Carreras</h1>
        <div class="panel panel-default">
            <div class="row">
            <div class="col-sm-10">
           
            <center><a href="#demo"  data-toggle="collapse"><div class="fa fa-question-circle fa-2x "></div><H4><STRONG>INFORMACION</STRONG></H4></a></center>
            <div id="demo" class="collapse">
             
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra las carreras impartidas en tu institucion, ejemplo: <STRONG>BACHILLERATO EN COMPUTACION</STRONG>
            </h4>
          </center>
        </div>
        </div>
        </div>
        </div>
        </div>
        @include('mensajes.msg')
        <div class="col-sm-12">
        <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
        @permission('crear-carrera')
          {!!link_to_route('carreras.create', $title = 'Nueva Carrera', $parameters = null, $attributes = ['class'=>'btn btn-primary  fa fa-plus'])!!}
        @endpermission
        </div>

        @include('carreras.search') 
        
      <div class="col-sm-12 panel panel-default">
         

        @if(count($carreras) == 0)
          <p class="text-info">
            No se han registrado carreras aun.
          </p>
          </div>

        </center>

      </div>

        <div class="col-sm-12 panel panel-default">
        @else
          <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  CARRERA
                </th>
                @permission('editar-carrera')
                <th>
                  ACTUALIZAR
                </th>
                @endpermission
                <th>
                  ESTADO
                </th>
              </tr>
            </thead>
            <tbody id="datosCarreras">
              @foreach($carreras as $key => $carrera)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($carrera->nombre_carrera) }}
                  </td>
                  @permission('editar-carrera')
                  <td>
                    {!!link_to_route('carreras.edit', $title = 'Editar', $parameters = $carrera->id_carrera, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}
                  </td>
                  @endpermission
                  <td>
                    
                     @if($carrera->estado_carrera == TRUE)
                              <input type="checkbox" name="estado" checked value="{{ $carrera->id_carrera }}" class="toggleEstado">
                            @else
                              <input type="checkbox" name="estado" value="{{ $carrera->id_carrera }}" class="toggleEstado">
                            @endif
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
          @include('mensajes.carga')
        @endif
      </div>

      <div class="col-sm-12 text-center">


        {{ $carreras->setPath('carreras')->links() }}


      </div>

    </div>
  </div>
@endsection
