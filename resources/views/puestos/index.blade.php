@extends('principal')





@section('titulo')


  <title>Puestos</title>


@endsection





@section('cuerpo')


  <div id="page-wrapper">


    <div class="row">


      <div class="col-sm-12">

      
        <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-users" aria-hidden="true"></div> <div class="fa fa-gears" aria-hidden="true"></div>Puestos de mi empresa</h1>

        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra, los puestos de empleados de tu empresa, para asignarlos a personas ejemplo:<strong>*DOCENTE</strong>
            </h4>
           
          </center>
        </div>
          <h6>

        @include('mensajes.msg')

        </div>


      </div>


      <div class="col-sm-12 panel panel-default">
      <br/>
      <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}

        @permission('crear-puesto')


        {!!link_to_route('puestos.create', $title = 'Nuevo Puesto', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus'])!!}


        <br>


        <br>


        @endpermission


        @if(count($puestos) == 0)


          <p class="text-info">


            No se han registrado puestos aun.


          </p>

        </center>


        @else


          <table class="table table-hover table-bordered">


            <thead>


              <tr>


                <th>


                  NO


                </th>


                <th>


                  NOMBRE


                </th>


                @permission('editar-puesto')


                <th>


                  ACTUALIZAR


                </th>


                @endpermission


                @permission('estado-puesto')


                <th>


                  ESTADO


                </th>


                @endpermission


              </tr>


            </thead>


            <tbody id="datosPuestos">


              @foreach($puestos as $key => $puesto)


                <tr>


                  <td>


                    {{ $key+1 }}


                  </td>


                  <th>


                    {{ mb_strtoupper($puesto->nombre_puesto) }}


                  </th>


                  @permission('editar-puesto')


                  <th>


                    {!!link_to_route('puestos.edit', $title = 'Editar', $parameters = $puesto->id_puesto, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}


                  </th>


                  @endpermission


                  @permission('estado-puesto')


                  <th>


                    @if($puesto->estado_puesto == true)


                      {!!link_to_route('puestos.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success fa fa-check eliminarPuesto', 'data-id'=>$puesto->id_puesto, 'data-estado'=>$puesto->estado_puesto])!!}


                    @else


                      {!!link_to_route('puestos.destroy', $title = 'Deshabilitado', $parameters = null, $attributes = ['class'=>'btn btn-danger fa fa-times eliminarPuesto', 'data-id'=>$puesto->id_puesto, 'data-estado'=>$puesto->estado_puesto])!!}


                    @endif


                  </th>


                  @endpermission


                </tr>


              @endforeach


            </tbody>


          </table>


          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">


        @endif


      </div>


      @include('mensajes.carga')


    </div>


  </div>


@endsection


