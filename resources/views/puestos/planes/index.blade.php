@extends('principal')

@section('titulo')
  <title>Planes</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Planes</h1>
          @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        @permission('crear-plan')
        {!!link_to_route('planes.create', $title ='Nuevo Plan', $parameters = null, $attributes = ['class'=>'btn btn-primary'])!!}
        <br>
        <br>
        @endpermission
        @if (count($planes) == 0)
          <p class="text-info">
            No se han registrado planes aun...
          </p>
        @else
          <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NOMBRE PLAN
                </th>
                @permission('editar-plan')
                <th>
                  ACTUALIZAR
                </th>
                @endpermission
                @permission('estado-plan')
                <th>
                  ESTADO
                </th>
                @endpermission
              </tr>
            </thead>
            <tbody id="datosPlanes">
              @foreach($planes as $key => $plan)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($plan->nombre_plan) }}
                  </td>
                  @permission('editar-plan')
                  <td>
                    {!!link_to_route('planes.edit', $title = 'Editar', $parameters = $plan->id_plan, $attributes = ['class'=>'btn btn-primary'])!!}
                  </td>
                  @endpermission
                  @permission('estado-plan')
                  <td>
                    @if($plan->estado_plan == true)
                      {!!link_to_route('planes.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarPlan', 'data-id'=>$plan->id_plan, 'data-estado'=>$plan->estado_plan])!!}
                    @else
                      {!!link_to_route('planes.destroy', $title = 'Deshabilitado', $parameters = null, $attributes = ['class'=>'btn btn-danger eliminarPlan', 'data-id'=>$plan->id_plan, 'data-estado'=>$plan->estado_plan])!!}
                    @endif
                  </td>
                  @endpermission
                </tr>
              @endforeach
            </tbody>
          </table>
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        @endif
      </div>
    </div>

    <!-- Mensaje de carga-->
    @include('mensajes.carga')

  </div>



@endsection
