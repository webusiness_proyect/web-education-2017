@extends('principal')





@section('titulo')


  <title>Nuevo Puesto</title>


@endsection





@section('cuerpo')


  <div id="page-wrapper">


    <div class="row">


      <div class="col-sm-12">
      <div class="panel-body ">

        <h1 class="page-header text-center"> 
        <div class="fa fa-plus" aria-hidden="true"></div> 
        
        <div class="fa fa-gears" aria-hidden="true"></div>Nuevo Puesto</h1>

        <p>


          Nota: Todos los campos con (*) son obligatorios.


        </p>


        @include('mensajes.errores')



        </div>


     



      </div>


      <div class="col-sm-12 panel panel-default">
      <center>
      <br/>

        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}

      </center>
      <br/>
        {!!Form::open(['route'=>'puestos.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'npuesto'])!!}


          @include('puestos.form.campos')


        {!!Form::close()!!}


      </div>


    </div>


  </div>


@endsection


