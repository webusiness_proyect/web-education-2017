@extends('principal')

@section('titulo')
  <title>Planes Niveles</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
         <div class="panel-body ">
        <h1 class="page-header text-center"> <div class="fa fa-pencil-square" aria-hidden="true"></div>Planes Niveles</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> ASIGNA </strong> a un nivel un plan y una jornada ejemplo:<strong>*NIVEL: PRIMARIA, PLAN: DIARIO, JORNADA: MATUTINA</strong>
            </h4>
            
          </center>
        </div>
        <h6>
         <CENTER>
            * ESTOS DEBERAN DE EXISTIR PARA PODER SER ADMINISTRADOS
          </CENTER>
          </h6>
        @include('mensajes.msg')
        </div>
         <div class="col-sm-12 panel panel-default">
          <br/>
           <center>
            {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
            @permission('crear-plannivel')
            {!!link_to_route('asignarniveles.create', $title = 'Nuevo Plan - Nivel', $parameters = null, $attributes =['class'=>'btn btn-primary fa fa-plus-square'])!!}
          <br>
          <br>
        @endpermission
        @include('nivelesplanesjornadas.search')
        @if(count($datos) == 0)
          <p class="text-info">
            No se han registrado asignacioónes de de niveles a planes y jornadas.
          </p>
          </center>
          </div>
        @else
          <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NIVEL
                </th>
                <th>
                  PLAN
                </th>
                <th>
                  JORNADA
                </th>
                @permission('editar-plannivel')
                <th>
                  ACTUALIZAR
                </th>
                @endpermission
                @permission('editar-plannivel')
                <th>
                  ESTADO 
                </th>
                @endpermission
              </tr>
            </thead>
            <tbody id="datosAsignacion">
              @foreach($datos as $key => $row)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($row->nombre_nivel) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($row->nombre_plan) }}
                  </td>
                  <td>
                    {{ mb_strtoupper($row->nombre_jornada) }}
                  </td>
                  @permission('editar-plannivel')
                  <td>
                    {!!link_to_route('asignarniveles.edit', $title = 'Editar', $parameters = $row->id_nivel_plan_jornada, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}
                  </td>
                  <td>
                   @if($row->estado_niveles_planes_jornadas == TRUE)
                              <input type="checkbox" name="estado" checked value="{{ $row->id_nivel_plan_jornada }}" class="toggleEstado">
                            @else
                              <input type="checkbox" name="estado" value="{{ $row->id_nivel_plan_jornada }}" class="toggleEstado">
                            @endif
                  @endpermission
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>

          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

          @include('mensajes.carga')

          <div class="text-center">

            {!! $datos->links() !!}

          </div>

        @endif
      </div>
    </div>
  </div>
@endsection
