<?php
  $msg = Session::get('mensaje');
  if ($msg != null) {
    $palabra = 'No';
    $buscar = strpos($msg, $palabra);
  }
?>
@if ($msg != null)

  @if($buscar === false)
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
       {{ $msg }}
    </div>
  @else
    <div class="alert alert-danger alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{ $msg }}
    </div>
  @endif

@endif
