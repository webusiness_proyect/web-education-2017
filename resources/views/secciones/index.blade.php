@extends('principal')

@section('titulo')
  <title>Secciones</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">

        <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-sort-alpha-asc" aria-hidden="true"></div> Secciones disponibles</h1>
          <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra secciones disponibles en tu institucion ejemplo:<strong>SECCION: A</strong>
            </h4>
            
          </center>
        </div>
        
        @include('mensajes.msg')

        </div>

      </div>
      <div class="col-sm-12 panel panel-default">

        <br/>
        <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
        @permission('crear-seccion')
        {!!link_to_route('secciones.create', $title = 'Nueva sección', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square'])!!}
        <br>
        <br>
        @endpermission
        @if(count($secciones) == 0)
          <p class="text-info">
            No se han registrado secciones aun.
          </p>
        </center>
        @else
          <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NOMBRE SECCION
                </th>
                @permission('editar-seccion')
                <th>
                  ACTUALIZAR
                </th>
                <th>
                  ESTADO
                </th>
                @endpermission
              </tr>
            </thead>
            <tbody id="datosSecciones">
              @foreach($secciones as $key => $seccion)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($seccion->nombre_seccion) }}
                  </td>
                  @permission('editar-seccion')
                  <td>
                    {!!link_to_route('secciones.edit', $title ='Editar', $parameters = $seccion->id_seccion, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}
                  </td>
                  <td>
                   @if($seccion->estado_secciones == TRUE)
                      <input type="checkbox" name="estado" checked value="{{ $seccion->id_seccion }}" class="toggleEstado">
                  @else
                      <input type="checkbox" name="estado" value="{{ $seccion->id_seccion }}" class="toggleEstado">
                  @endif

                  </td>
                  @endpermission
                </tr>
              @endforeach
            </tbody>
          </table>
        @endif
      </div>
    </div>
  </div>
@endsection
