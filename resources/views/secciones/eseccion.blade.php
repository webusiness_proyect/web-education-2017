@extends('principal')

@section('titulo')
  <title>Editar Sección</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Editar Sección</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::model($seccion, ['route'=>['secciones.update', $seccion->id_seccion], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'nseccion'])!!}
          @include('secciones.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
