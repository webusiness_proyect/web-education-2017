@extends('principal')



@section('titulo')

  <title>Nueva Actividad</title>

@endsection



@section('cuerpo')

<div id="page-wrapper">

  <div class="row">

    <div class="col-sm-12">

      <h1 class="page-header text-center">Nueva Actividad</h1>

      @include('mensajes.errores')

      <p>

        Nota: Todos los campos con (*) son obligatorios.

      </p>

    </div>

    <div class="col-sm-12">

      {!!Form::open(['route'=>'Actividad.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'actividades'])!!}

        <input type="hidden" name="curso" value="{{ $curso }}">

        @include('Actividad.form.actividades')

      {!!Form::close()!!}

    </div>

  </div>

</div>

@endsection

