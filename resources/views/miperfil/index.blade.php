@extends('principal')

@section('titulo')
  <title>Mi perfil</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center"> <div class="fa fa-user" aria-hidden="true"></div>Mi Perfil</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CONFIGURA  </strong> tu contraseña y tu imagen para mostrar
            </h4>
          </center>
        </div>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        <center>
          USUARIO: {{ ucwords(Auth::user()->name) }} 
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
       
        <br/>
       
      </center>
     
        <br/>
      </div>
      <div class="col-sm-12  panel panel-default">
       
          <div class="table-responsive">
            <table class="table table-hover">

                <tr>                
                  <th>
                    IMAGEN
                  </th>
                  
                    @if((Auth::user()->url)==null)
                    <td>
                      <div class="fa fa-user fa-4x">
                      </div>
                      
                    
                    </td>
                      @else
                    <td>
                    <center>
                    <img src="{{ ucwords(Auth::user()->url) }}" width="200" height="250">
                     {!!link_to_route('miperfil.edit', $title = ' Cambiar imagen', $parameters = Auth::user()->id, $attributes = ['class'=>'btn btn-warning fa fa-edit'])!!}
                    
                    </center>
                    </td>
                    @endif               
                </tr>    
                <tr>                
                  <th>
                    NOMBRE DE USUARIO
                  </th>
                  <th>
                    
                     {{ ucwords(Auth::user()->name) }} 
                  </th>               
                </tr>
                <tr>                
                  <th>
                    USUARIO DE ACCESO
                  </th>
                  <th>
                     {{ ucwords(Auth::user()->email) }} 
                  </th>               
                </tr>

                <tr>                
                  <th>
                    CONTRASEÑA 
                  </th>
                  <th>
                    ************ 
                  </th>               
                </tr>
                <tr>
                  <th>
                  </th>
                  <th>
                  {!!link_to_route('miperfil.edit', $title = 'Modificar datos', $parameters = Auth::user()->id, $attributes = ['class'=>'btn btn-warning fa fa-edit'])!!}
                  </th>
                <tr>

                
                

            </table>
          </div>
        
      </div>
    </div>
  </div>
@endsection
