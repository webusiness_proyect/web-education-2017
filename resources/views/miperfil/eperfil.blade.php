@extends('principal')







@section('titulo')



  <title>editar</title>



@endsection







@section('cuerpo')



<div id="page-wrapper">



  <div class="row">



    <div class="col-sm-12">



      <h1 class="page-header text-center"><div class="fa fa-edit"></div><div class="fa fa-user"></div>  Modifica la informacion de tu perfil </h1>



      @include('mensajes.errores')



  

      <div class="col-sm-12">

        <div class="alert alert-warning">


         
          <center>
        

            <div class="fa fa-edit"> 

                Modifica la informacion de tu perfil, *tu usuario de acceso no puede ser modificado

            </div>
            <br/>

          


          

       

      </div>



    </div>



    <div class="col-sm-12 panel panel-default">
    <br/>



      {!!Form::model($usuario, ['route'=>['miperfil.update', $usuario->id], 'method'=>'PUT',  'class'=>'form-horizontal', 'id'=>'nperfil'])!!}


 

        @include('miperfil.form.campos')



      {!!Form::close()!!}



    </div>



  </div>



</div>



@endsection



