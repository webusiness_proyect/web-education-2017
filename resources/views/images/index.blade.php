@extends('principal')

@section('titulo')
  <title>Grados</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Grados</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        @permission('crear-grado')
        {!!link_to_route('grados.create', $title = 'Nuevo Grado', $parameters = null, $attributes = ['class'=>'btn btn-primary'])!!}
        <br>
        <br>
        @endpermission
        @if(count($grados) == 0)
          <p class="text-info">
            No se han registrado grados aun.
          </p>
        @else
          <table class="table table-hover">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NOMBRE GRADO
                </th>
                @permission('editar-grado')
                <th>
                  ACTUALIZAR
                </th>
                @endpermission
                @permission('estado-grado')
                <th>
                  ESTADO
                </th>
                @endpermission
              </tr>
            </thead>
            <tbody id="datosGrados">
              @foreach($grados as $key => $grado)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($grado->nombre_grado) }}
                  </td>
                  @permission('editar-grado')
                  <td>
                    {!!link_to_route('grados.edit', $title = 'Editar', $parameters = $grado->id_grado, $attributes = ['class'=>'btn btn-primary'])!!}
                  </td>
                  @endpermission
                  @permission('estado-grado')
                  <td>
                    @if($grado->estado_grado == true)
                      {!!link_to_route('grados.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarGrado', 'data-id'=>$grado->id_grado, 'data-estado'=>$grado->estado_grado])!!}
                    @else
                      {!!link_to_route('grados.destroy', $title = 'Deshabilitado', $parameters = null, $attributes = ['class'=>'btn btn-danger eliminarGrado', 'data-id'=>$grado->id_grado, 'data-estado'=>$grado->estado_grado])!!}
                    @endif
                  </td>
                  @endpermission
                </tr>
              @endforeach
            </tbody>
          </table>
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
          @include('mensajes.carga')
        @endif
      </div>
    </div>
  </div>
@endsection
