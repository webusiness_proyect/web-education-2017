@extends('principal')

@section('titulo')
  <title>Foros</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center"><div class="fa fa-comments"></div>Foros de este curso</h1>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12 panel panel-default">
        <p>

        <center>   
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
           @permission('crear-foro')


          {!!link_to_route('foros.create', $title = ' Nuevo Foro', $parameters = ['a'=>$curso], $attributes = ['class'=>'btn btn-primary fa fa-plus'])!!}

          @endpermission
          </center>
        </p>
        @if (count($foros) == 0)
          <p class="text-info">
            No se han creado foros en este curso...
          </p>
        @else
          <div class="table-responsive">
            <table class="table table-hover table-bordered">
              <thead>
                <tr>
                 
                  <th>
                    MIS FOROS
                  </th>
                 

                </tr>
              </thead>
              <tbody>
                @foreach ($foros as $key=>$foro)
                  <tr>

                    <td>
                    <h2>
                     
                      {!!link_to_route('respuestasforos.show', $title = $foro->titulo_foro, $parameters = $foro->id_foro, $attributes= null)!!}</h2>{!!link_to_route('respuestasforos.show', $title = $foro->mensaje_foro, $parameters = $foro->id_foro, $attributes= null)!!}
                      {!!link_to_route('respuestasforos.create', $title = ' Responder', $parameters = ['foro'=>$foro->id_foro], $attributes = ['class'=>'btn btn-success fa fa-comments'])!!}
                      <h5>CREADO POR:{!!link_to_route('respuestasforos.show', $title = $foro->nombre, $parameters = $foro->id_foro, $attributes= null)!!}
                      
                      {!!link_to_route('respuestasforos.show', $title = $foro->rol, $parameters = $foro->id_foro, $attributes= null)!!}
                      {!!link_to_route('respuestasforos.show', $title = $foro->fecha_foro, $parameters = $foro->id_foro, $attributes= null)!!}

                       @permission('editar-foros')
                  
                      
                        {!!link_to_route('foros.edit', $title = ' Editar', $parameters = $foro->id_foro, $attributes = ['class'=>'btn btn-warning fa fa-edit'])!!}
                      
                   
                      @endpermission
                      </h5>
                    </td>

                    
                    

                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        @endif
      </div>
    </div>
  </div>
@endsection
