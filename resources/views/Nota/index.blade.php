@extends('principal')



@section('titulo')

  <title>Actividades</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center "> <div class="glyphicon glyphicon-book"></div> NOTAS </h1>

        @include('mensajes.msg')

      </div>
            <div class="col-sm-12">

        <div class="alert alert-info">


         
          <center>
          @foreach ($total as $key=>$actividad)

            <div class="fa fa-check-square"> Aqui apareceran todas las notas de las actividades entregadas por el alumno {{ $nota=mb_strtoupper($actividad->nombre_estudiante).' ' .mb_strtoupper($actividad->apellidos_estudiante).' del curso de:  ' .mb_strtoupper($actividad->nombre_area) }}</div>
          @endforeach
          </center>

          

        </div>

      </div>



      <div class="col-sm-12">



        <p>

       



         
        </p>

        @if (count($notas) == 0)

          <p class="text-info">

            No se han encontrado notas para este curso...

          </p>

        @else


          <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>

                 

                  <th>

                    NOMBRE ACTIVIDAD

                  </th>

                  <th>

                    DESCRIPCION ACTIVIDAD

                  </th>


                  <th>

                    ESTADO DE ENTREGA

                  </th>


                  <th>

                    PUNTEO

                  </th>
                  <th>

                    UNIDAD

                  </th>

                </tr>

              </thead>

              <tbody>

                @foreach ($notas as $key=>$actividad)
                

                  <tr>


                    

                    <td>

                    {{ mb_strtoupper($actividad->actividad) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($actividad->fecha_entrega) }}

                  </td>

                  <td>

                   {{ mb_strtoupper($actividad->estado_entrega_actividad) }}

                  </td>

                   <td>

                   {{ $nota=mb_strtoupper($actividad->calificacion) }}

                  </td>
                  <td>

                   {{ $nota=mb_strtoupper($actividad->nombre_unidad) }}

                  </td>

                 
                  
                  

                  </tr>

                  

                @endforeach



              </tbody>

            </table>
            @foreach ($total as $key=>$total1)
                
                @if ($total=mb_strtoupper($total1->calificacion)< 60)

                  <h1 class="text-danger blockquote-reverse">

                   NOTA TOTAL: {{ $total=mb_strtoupper($total1->calificacion) }} </br>
                   </h1>
                   <H4 class="text-danger blockquote-reverse">NOTA REPROBADA</H4>

                  

                @else

                 <h1 class="text-success blockquote-reverse">

                   NOTA TOTAL: {{ $total=mb_strtoupper($total1->calificacion) }} </br>
                   </h1>
                   <H4 class="text-success blockquote-reverse">NOTA APROBADA</H4>

                 
                 @endif
                @endforeach
@endif



<div class="container">
 
  <center><a href="#demo" class="btn btn-default" data-toggle="collapse"><div class="fa fa-plus"></div>... MAS ACTIVIDADES DE UNIDADES ANTERIORES ...</a></center>
  <div id="demo" class="collapse">
   
        

          <div class="table-responsive">




          <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>

                 

                  <th>

                    NOMBRE ACTIVIDAD

                  </th>

                  <th>

                    DESCRIPCION ACTIVIDAD

                  </th>


                  <th>

                    ESTADO DE ENTREGA

                  </th>


                  <th>

                    PUNTEO

                  </th>
                  <th>

                    UNIDAD

                  </th>

                </tr>

              </thead>

              <tbody>

                @foreach ($anterior as $key=>$actividad)
                

                  <tr>

                   

                    

                    <td>

                    {{ mb_strtoupper($actividad->actividad) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($actividad->fecha_entrega) }}

                  </td>

                  <td>

                   {{ mb_strtoupper($actividad->estado_entrega_actividad) }}

                  </td>

                   <td>

                   {{ $nota=mb_strtoupper($actividad->calificacion) }}

                  </td>
                  <td>

                   {{ $nota=mb_strtoupper($actividad->nombre_unidad) }}

                  </td>

                 
                  
                  

                  </tr>

                  

                @endforeach



              </tbody>

            </table>

          </div>
        </div>
             <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

          </div>

        

      </div>

    </div>

  </div>

@endsection

