@extends('principal')



@section('titulo')

  <title>Nuevo Ticket</title>

@endsection



@section('cuerpo')

<div id="page-wrapper">

  <div class="row">

    <div class="col-sm-12">

      <h1 class="page-header text-center">Nuevo Ticket</h1>

      @include('mensajes.errores')

      <p>

        Nota: Todos los campos con (*) son obligatorios.

      </p>

    </div>

    <div class="col-sm-12">

      {!!Form::open(['route'=>'helpdesk.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'help'])!!}

        

        @include('helpdesk.form.campos')

      {!!Form::close()!!}

    </div>

  </div>

</div>

@endsection

