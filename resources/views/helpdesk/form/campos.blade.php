<div class="form-group">

  {!!Form::label('asunto', 'Titulo*', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::text('asunto', null, ['class'=>'form-control', 'placeholder'=>'Asunto'])!!}

  </div>

</div>

<div class="form-group">

  {!!Form::label('descripcion', 'Descripción*', ['class'=>'col-sm-2 control-label'])!!}

  <div class="col-sm-10">

    {!!Form::textarea('descripcionhelp', null, ['class'=>'form-control', 'placeholder'=>'Descripción del problema...', 'id'=>'descripcionhelp'])!!}

  </div>

</div>

<div class="form-group">

  <div class="col-sm-offset-2 col-sm-10">

    <button type="submit" name="button" class="btn btn-success"><span class="fa fa-save"></span> Solicitar Soporte</button>

  </div>

</div>

