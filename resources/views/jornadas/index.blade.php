@extends('principal')

@section('titulo')
  <title>Jornadas</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-sun-o" aria-hidden="true"></div> Jornadas disponibles <div class="fa fa-moon-o" aria-hidden="true"></div></h1>
          <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong>y administra las jornadas disponibles en tu institucion ejemplo:<strong> JORNADA:MATUTINA</strong>
            </h4>
           
          </center>
        </div>
         

        @include('mensajes.msg')

        </div>
      </div>
      <div class="col-sm-12 panel panel-default">

        <br/>
        <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
        @permission('crear-jornada')
        {!!link_to_route('jornadas.create', $title = 'Nueva Jornada', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square'])!!}
        <br>
        <br>
        @endpermission
        @if(count($jornadas) == 0)
          <p class="text-info">
            No se han registrado jornadas aun.
          </p>
          </center>
        @else
          <div class="table-responsive">
            <table class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    JORNADA
                  </th>
                  @permission('editar-jornada')
                  <th>
                    ACTUALIZAR
                  </th>
                  @endpermission
                  @permission('estado-jornada')
                  <th>
                    ESTADO
                  </th>
                  @endpermission
                </tr>
              </thead>
              <tbody id="datosJornadas">
                @foreach($jornadas as $key => $jornada)
                  <tr>
                    <td>
                      {{ $key+1 }}
                    </td>
                    <td>
                      {{ mb_strtoupper($jornada->nombre_jornada) }}
                    </td>
                    @permission('editar-jornada')
                    <td>
                      {!!link_to_route('jornadas.edit', $title = 'Editar', $parameters = $jornada->id_jornada, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}
                    </td>
                    @endpermission
                    @permission('estado-jornada')
                    <td>
    

                      @if($jornada->estado_jornada == TRUE)
                      <input type="checkbox" name="estado" checked value="{{ $jornada->id_jornada }}" class="toggleEstado">
                    @else
                        <input type="checkbox" name="estado" value="{{ $jornada->id_jornada }}" class="toggleEstado">
                    @endif

                    </td>
                    @endpermission
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
        @endif
      </div>
    </div>
    <!-- Mensaje de carga-->
    @include('mensajes.carga')
  </div>
@endsection
