@extends('principal')



@section('titulo')

  <title>Grados Niveles</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

       <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-pencil-square" aria-hidden="true"></div>Grados con Niveles</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> ASIGNA </strong> a un plan una jornada, un nivel, carrera, grado, seccion, costos de inscripcion y mensualidad ejemplo:<strong>*PLAN: DIARIO, JORNADA:MATUTINA, NIVEL:PRIMARIO, CARRERA: BACHILLERATO EN COMPUTACION, SECCION:A, INSCRIPCION: $.500.00, MENSUALIDAD: $.400.00</strong>
            </h4>
           
          </center>
        </div>
          <h6>
         <CENTER>
            * ESTOS DEBERAN DE EXISTIR PARA PODER SER ADMINISTRADOS
          </CENTER>
          </h6>

        @include('mensajes.msg')

        </div>
        <div class="col-sm-12 panel panel-default">

          <br/>
           <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}

        @permission('crear-gradonivel')

        {!!link_to_route('gradoniveles.create', $title = 'Nuevo Grado-Nivel', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square'])!!}

        <br>

        <br>

        @endpermission

        @include('gradosniveles.search')

        @if(count($datos) == 0)

          <p class="text-info">

            No se han registrado grados en ningun nivel aun.

          </p>
        </center>
        @else

          <div class="table-responsive">

            <table class="table table-hover table-hover table- table-bordered">

              <thead>

                <tr>

                  <th>

                    NO

                  </th>

                  <th>

                    SECCION

                  </th>

                  <th>

                    GRADO

                  </th>

                  <th>

                    NIVEL

                  </th>

                  <th>

                    CARRERA

                  </th>

                  @permission('editar-gradonivel')

                  <th>

                    ACTUALIZAR

                  </th>

                  @endpermission

                  @permission('estado-gradonivel')

                  <th>

                    ESTADO

                  </th>

                  @endpermission

                </tr>

              </thead>

              <tbody id="datosGradosNiveles">

                @foreach($datos as $key => $dato)

                  <tr>

                    <td>

                      {{ $key+1 }}

                    </td>

                    <td>

                      {{ mb_strtoupper($dato->nombre_seccion) }}

                    </td>

                    <td>

                      {{ mb_strtoupper($dato->nombre_grado) }}

                    </td>

                    <td>

                      {{ mb_strtoupper($dato->nombre_nivel) }}

                    </td>

                    <td>

                      {{ mb_strtoupper($dato->nombre_carrera) }}

                    </td>

                    @permission('editar-gradonivel')

                    <td>

                      {!!link_to_route('gradoniveles.edit', $title = 'Editar', $parameters = $dato->id_nivel_grado, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}

                    </td>

                    @endpermission

                    @permission('estado-gradonivel')

                    <td>


                      @if($dato->estado_nivel_grado == TRUE)
                              <input type="checkbox" name="estado" checked value="{{ $dato->id_nivel_grado }}" class="toggleEstado">
                            @else
                              <input type="checkbox" name="estado" value="{{ $dato->id_nivel_grado }}" class="toggleEstado">
                            @endif
                      

                    </td>

                    @endpermission

                  </tr>

                @endforeach

              </tbody>

            </table>

            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

            @include('mensajes.carga')

          </div>

        @endif

      </div>

      <div class="col-sm-12 text-center">

        {{ $datos->setPath('gradoniveles')->links() }}

      </div>

    </div>

  </div>

@endsection

