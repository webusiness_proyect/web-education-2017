<!DOCTYPE html><html lang="en">
<head>    
<meta charset="utf-8">    
<meta http-equiv="X-UA-Compatible" content="IE=edge">    
<meta name="viewport" content="width=device-width, initial-scale=1"> 
   <title>Bienvenido</title>    
   <!-- Bootstrap Core CSS -->    <link href="{{ url('/') }}/css/bootstrap.min.css" rel="stylesheet">    
   <!-- Custom CSS -->    <link href="{{ url('/') }}/css/sb-admin-2.css" rel="stylesheet">    
   <!-- Custom Fonts -->    <link href="{{ url('/') }}/css/font-awesome.min.css" rel="stylesheet" type="text/css">    
   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->    <!--[if lt IE 9]>        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>    <![endif]-->
</head>
	<body id="log" background="{{ url('/') }}">
	    @yield('content')    
	    <!-- jQuery -->    <script src="{{ url('/') }}/js/jquery.min.js"></script>    <!-- Bootstrap Core JavaScript -->    <script src="{{ url('/') }}/js/formValidation.min.js"></script>    
	    <!-- Bootstrap Core JavaScript -->    <script src="{{ url('/') }}/js/bootstrap.min.js"></script>    
	    <!-- Bootstrat validation JS -->    <script src="{{ url('/') }}/js/bootstrap.js"></script>    <script type="text/javascript">    
	    $('#login').formValidation({        
	    message: 'This value is not valid',
	            icon: {            valid: 'glyphicon glyphicon-ok',            invalid: 'glyphicon glyphicon-remove',            validating: 'glyphicon glyphicon-refresh'        },        fields: {          email: {              message: 'El correo no es valido!!!',              validators: {                  notEmpty: {                      message: 'El correo es requerido, no puede estar vacío!!!'                  },                  regexp: {                      regexp: /^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$/,                      message: 'La dirección de correo tiene que tener un formato valido!!!!'                  }              }          },          password: {              message: 'La contraseña no es valido!!!',              validators: {                  notEmpty: {                      message: 'La contraseña es requerida, no puede estar vacío!!!'                  },              }          },        }      });//fin de la validación del formulario    </script>    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}</body></html>