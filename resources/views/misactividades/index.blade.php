@extends('principal')



@section('titulo')

  <title>Actividades</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center"> <div class="fa fa-pencil"></div>  MIS ACTIVIDADES </h1>

        @include('mensajes.msg')

      </div>

      @if(count($ractividades)==0)
      <center>
      <h3>
      No existen tareas pendientes por entregar...
      </h3>
      </center>

      @else

      


        <div class="col-sm-12 panel panel-danger">
        <div class="panel-body">

          <div class="container">
 
            <center><a href="#demo"  data-toggle="collapse">

               
              

              <H3> <DIV class="fa fa-exclamation-triangle"></DIV> TAREAS SIN ENTREGAR 
                
                </H3></a></center>
                <div id="demo" class="collapse in">
             

          <br/>
               
      
        <div class="col-sm-10 panel panel-danger">
        <div class="panel-body">
        

         <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>

                  <th>

                    ESTADO

                  </th>

                  <th>

                     ACTIVIDAD

                  </th>

                  <th>

                    DESCRIPCION ACTIVIDAD

                  </th>



                  <th>

                    FECHA DE ASIGNACION 

                  </th>

                  <th>

                    FECHA LIMITE DE ENTREGA 

                  </th>

                   <th>

                    UNIDAD

                  </th>

                 

                 

                  <th>

                    SUBIR TAREA

                  </th>

                </tr>

              </thead>

              <tbody>

               

                @foreach ($ractividades as $key=>$ractividad)

                

                 

                  <tr>

                   <td class="btn-danger">
                      <li class="fa fa-exclamation-triangle">SIN ENTREGAR</li>
                    

                  </td>
                 

                    

                    <td>

                    {{ mb_strtoupper($ractividad->nombre_actividad) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($ractividad->descripcion_actividad) }}

                  </td>

                  

                  <td>

                    {{ mb_strtoupper($ractividad->fecha_inicio) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($ractividad->fecha_entrega) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($ractividad->nombre_unidad) }}
                  </td>

                  <td>


                      @permission('entregar-tarea')
                      <a class="btn btn-info glyphicon glyphicon-share" href="/respuestaactividades/create?actividad={{$ractividad->id_actividad}}"> ENTREGAR</a>
                      @endpermission
                    </td>

                  </tr>
                
                @endforeach

              </tbody>

            </table>
            
           </div>
            </div>
          </div>
          </div>
        </div>
      </div>

      @endif

  

      
       <div class="container">
 
            <center><a href="#demo2"  data-toggle="collapse">

               
              

              <H3> <DIV class="fa fa-check-circle"></DIV> TAREAS ENTREGADAS
                
                </H3></a></center>
                <div id="demo2" class="collapse">
                
         @if(count($actividades)==0)
      <center>
      <h3>
      No existen tareas pendientes por entregar...
      </h3>
      </center>

      @else
               
      
        <div class="col-sm-10 panel panel-danger">
        <div class="panel-body">

          <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>

                  <th>

                    ESTADO

                  </th>

                  <th>

                     ACTIVIDAD

                  </th>

                  <th>

                    DESCRIPCION ACTIVIDAD

                  </th>



                  <th>

                    FECHA DE ASIGNACION 

                  </th>

                  <th>

                    FECHA LIMITE DE ENTREGA 

                  </th>

                  <th>

                    NOTA ASIGNADA

                  </th>

                 

                  <th>

                    SUBIR TAREA

                  </th>

                </tr>

              </thead>

              <tbody>

               

                @foreach ($actividades as $key=>$actividad)

                

                 

                  <tr>

                    @if($actividad->estado_entrega_actividad=="ENTREGADO")

                  <td class="btn-warning">
                     
                    <li class="fa fa-info-circle">{{ mb_strtoupper($actividad->estado_entrega_actividad) }} pendiente de revision </li>

                  </td >
                    @elseif($actividad->estado_entrega_actividad=="CALIFICADO")

                    <td class="btn-success">
                     
                    <li class="fa fa-check-circle">{{ mb_strtoupper($actividad->estado_entrega_actividad) }}</li>

                  </td>


                    @else
                   <td class="btn-danger">
                      <li class="fa fa-exclamation-triangle">SIN ENTREGAR</li>
                    

                  </td>
                  @endif

                    

                    <td>

                    {{ mb_strtoupper($actividad->nombre_actividad) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($actividad->descripcion_actividad) }}

                  </td>

                  

                  <td>

                    {{ mb_strtoupper($actividad->fecha_inicio) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($actividad->fecha_entrega) }}

                  </td>


                  @if($actividad->calificacion==NULL)

                  <td class="btn-danger">
                  TAREA NO ENTREGADA
                  </td>

                  @elseif($actividad->calificacion==0)
                     <td class="btn-warning">
                  TAREA PENDIENTE DE REVISION
                  </td>

                  @else


                  <td class="btn-success">
                      <center>


                    {{ mb_strtoupper($actividad->calificacion) }}
                    </center>

                  </td>
                    @endif
                    

                    <td>

                      <a disabled="disabled" class="btn btn-success fa fa-check-circle" href=#> TAREA ENTREGADA</a>

                      <!--{!!link_to_route('respuestaactividades.create', $title = 'Entregar tarea', $parameters = ['a'=>$actividad->id_actividad,'b'=>$actividad->id_actividad], $attributes = ['class'=>'btn btn-info '])!!}
-->
                    </td>

                  </tr>
                
                @endforeach

              </tbody>

            </table>

            
            </div>
            </div>
            </div>
            </div>
          </div>

          @endif


          

          <div class="container">
 
            <center><a href="#demo3"  data-toggle="collapse">

              <H3> 
              <DIV class="fa fa-book"></DIV> ...TAREAS DE UNIDADES ANTERIORES ENTREGADAS ...
              </H3></a></center>
          

              <div id="demo3" class="collapse">

              @if(count($anteriores)==0)
          <center>
          <h3>
          No existen tareas de unidades anteriores entregadas...
          </h3>
          </center>

          @else
          
               
      
        <div class="col-sm-10 panel panel-danger">

        <div class="panel-body">

          <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>

                  <th>

                    ESTADO

                  </th>

                  <th>

                     ACTIVIDAD

                  </th>

                  <th>

                    DESCRIPCION ACTIVIDAD

                  </th>

                  <th>

                    FECHA DE ASIGNACION 

                  </th>

                  <th>

                    FECHA LIMITE DE ENTREGA 

                  </th>

                  <th>

                    NOTA ASIGNADA

                  </th>

                  <th>

                    SUBIR TAREA

                  </th>

                </tr>

              </thead>

              <tbody>


                @foreach ($anteriores as $key=>$actividad)

                  <tr>

                    @if($actividad->estado_entrega_actividad=="ENTREGADO")

                  <td class="btn-warning">
                     
                    <li class="fa fa-info-circle">{{ mb_strtoupper($actividad->estado_entrega_actividad) }} pendiente de revision </li>

                  </td >
                    @elseif($actividad->estado_entrega_actividad=="CALIFICADO")

                    <td class="btn-success">
                     
                    <li class="fa fa-check-circle">{{ mb_strtoupper($actividad->estado_entrega_actividad) }}</li>

                  </td>


                    @else
                   <td class="btn-danger">
                      <li class="fa fa-exclamation-triangle">SIN ENTREGAR</li>
                    

                  </td>
                  @endif

                    

                    <td>

                    {{ mb_strtoupper($actividad->nombre_actividad) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($actividad->descripcion_actividad) }}

                  </td>

                  

                  <td>

                    {{ mb_strtoupper($actividad->fecha_inicio) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($actividad->fecha_entrega) }}

                  </td>


                  @if($actividad->calificacion==NULL)

                  <td class="btn-danger">
                  TAREA NO ENTREGADA
                  </td>

                  @elseif($actividad->calificacion==0)
                     <td class="btn-warning">
                  TAREA PENDIENTE DE REVISION
                  </td>

                  @else


                  <td class="btn-success">
                      <center>


                    {{ mb_strtoupper($actividad->calificacion) }}
                    </center>

                  </td>
                    @endif
                    

                    <td>

                      <a disabled="disabled" class="btn btn-success fa fa-check-circle" href=#> TAREA ENTREGADA</a>

                      <!--{!!link_to_route('respuestaactividades.create', $title = 'Entregar tarea', $parameters = ['a'=>$actividad->id_actividad,'b'=>$actividad->id_actividad], $attributes = ['class'=>'btn btn-info '])!!}
-->
                    </td>

                  </tr>
                
                @endforeach

              </tbody>

            </table>

            </div>
            </div>
            </div>
            </div>
          </div>
          @endif


       
          <div class="panel-body">

          <div class="container">
 
          <center><a href="#demo4"  data-toggle="collapse">

               
              

          <H3> <DIV class="fa fa-exclamation-triangle"></DIV>... TAREAS DE UNIDADES ANTERIORES SIN ENTREGAR ... 
                
          </H3></a></center>
          <div id="demo4" class="collapse">
             

              @if(count($anterioressinentrega)==0)
          <center>
          <h3>
          No existen tareas de unidades anteriores  pendientes por entregar...
          </h3>
          </center>

          @else
               
      
        <div class="col-sm-10 panel panel-danger">
        <div class="panel-body">
        

         <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>

                  <th>

                    ESTADO

                  </th>

                  <th>

                     ACTIVIDAD

                  </th>

                  <th>

                    DESCRIPCION ACTIVIDAD

                  </th>



                  <th>

                    FECHA DE ASIGNACION 

                  </th>

                  <th>

                    FECHA LIMITE DE ENTREGA 

                  </th>

                   <th>

                    UNIDAD

                  </th>

                 

                 

                  <th>

                    *YA NO PUEDES SUBIR ESTA TAREA

                  </th>

                </tr>

              </thead>

              <tbody>

               

                @foreach ($anterioressinentrega as $key=>$ractividad)

                

                 

                  <tr>

                   <td class="btn-danger">
                      <li class="fa fa-exclamation-triangle">SIN ENTREGAR</li>
                    

                  </td>
                 

                    

                    <td>

                    {{ mb_strtoupper($ractividad->nombre_actividad) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($ractividad->descripcion_actividad) }}

                  </td>

                  

                  <td>

                    {{ mb_strtoupper($ractividad->fecha_inicio) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($ractividad->fecha_entrega) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($ractividad->nombre_unidad) }}
                  </td>

                  <td>



                      <a class="btn btn-info glyphicon glyphicon-share" disabled="true" href="#"> ENTREGAR</a>

                    </td>

                  </tr>
                
                @endforeach

              </tbody>

            </table>
           </div>
            </div>
          </div>
          </div>
        </div>
      </div>

      
      @endif


          

             <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
             
        </div>

         

        


@endsection

