@extends('principal')



@section('titulo')

  <title>  Cursos</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">
        <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-pencil-square" aria-hidden="true"></div>Cursos</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong>y administra cursos disponibles en tu institucion ejemplo:<strong> CURSO:MATEMATICAS</strong>
            </h4>
           
          </center>
        </div>
    

        @include('mensajes.msg')

        </div>
        <div class="col-sm-12 panel panel-default">

          <br/>
           <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}

         @permission('crear-curso')

        {!!link_to_route('areas.create', $title = 'Nuevo Curso', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square'])!!}


         @include('areas.search')

        @endpermission

        @if(count($areas) == 0)


          <p class="text-info">

            No se han registrado cursos aun.

          </p>
        </center>



        @else

        
           

        <br/>

  
    
          <table class="table table-hover table-hover table- table-bordered">

            <thead>

              <tr>

                <th>

                  NO

                </th>

                <th>

                  NOMBRE

                </th>

                @permission('editar-curso')

                <th>

                  ACTUALIZAR

                </th>

                @endpermission

                @permission('estado-curso')

                <th>

                  ESTADO

                </th>

                @endpermission

              </tr>

            </thead>

            <tbody id="datosAreas">

              @foreach($areas as $key => $area)

                <tr>

                  <td>

                    {{ $area->id_area}}

                  </td>

                  <td>

                    {{ mb_strtoupper($area->nombre_area) }}

                  </td>

                  @permission('editar-curso')

                  <td>

                    {!!link_to_route('areas.edit', $title = 'Editar', $parameters = $area->id_area, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}

                  </td>

                  @endpermission

                  @permission('estado-curso')

                  <td>

                  
                    @if($area->estado_area == TRUE)
                      <input type="checkbox" name="estado" checked value="{{ $area->id_area }}" class="toggleEstado">
                  @else
                      <input type="checkbox" name="estado" value="{{ $area->id_area }}" class="toggleEstado">
                  @endif


                  </td>

                  @endpermission

                </tr>

              @endforeach

            </tbody>

          </table>

          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

          @include('mensajes.carga')

          <div class="text-center">

            {!! $areas->links() !!}

          </div>

        @endif

      </div>

    </div>

  </div>

@endsection

