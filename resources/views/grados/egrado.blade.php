@extends('principal')

@section('titulo')
  <title>Editar Grado</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Editar Grado</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::model($grado, ['route'=>['grados.update', $grado->id_grado], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'ngrado'])!!}
          @include('grados.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
