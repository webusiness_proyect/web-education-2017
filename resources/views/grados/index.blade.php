@extends('principal')

@section('titulo')
  <title>Grados</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel-body ">
        <h1 class="page-header text-center"> <div class="fa fa-bookmark-o" aria-hidden="true"></div>Grados</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra grados los cuales estaran disponibles en tu pensum ejemplo:<strong>SEGUNDO </strong>
            </h4>
          </center>
        </div>
        @include('mensajes.msg')
      </div>
      </div>
     <div class="col-sm-12 panel panel-default">
        <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
        @permission('crear-grado')
        {!!link_to_route('grados.create', $title = 'Nuevo Grado', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square'])!!}
        <br>
        <br>
        @endpermission
         @include('grados.search')  
        @if(count($grados) == 0)
          <p class="text-info">
            No se han registrado grados aun.
          </p>
          </center>
        @else
          <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NOMBRE GRADO
                </th>
                <th>
                  FECHA CREACION
                </th>
                @permission('editar-grado')
                <th>
                  ACTUALIZAR
                </th>
                @endpermission
                @permission('estado-grado')
                <th>
                  ESTADO
                </th>
                @endpermission
              </tr>
            </thead>
            <tbody id="datosGrados">
              @foreach($grados as $key => $grado)
                <tr>
                  <td>
                    {{ $key+1 }}
                  </td>
                  <td>
                    {{ mb_strtoupper($grado->nombre_grado) }}
                  </td>
                   <td>
                    {{ mb_strtoupper($grado->fecha_registro_grado) }}
                  </td>
                  @permission('editar-grado')
                  <td>
                    {!!link_to_route('grados.edit', $title = 'Editar', $parameters = $grado->id_grado, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}
                  </td>
                  @endpermission
                  @permission('estado-grado')
                  <td>
                 

                    @if($grado->estado_grado == TRUE)
                              <input type="checkbox" name="estado" checked value="{{ $grado->id_grado }}" class="toggleEstado">
                            @else
                              <input type="checkbox" name="estado" value="{{ $grado->id_grado }}" class="toggleEstado">
                            @endif
                  </td>
                  @endpermission
                </tr>
              @endforeach
            </tbody>
          </table>
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
          @include('mensajes.carga')
        @endif
      </div>
    </div>
  </div>
@endsection
