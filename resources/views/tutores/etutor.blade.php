@extends('principal')



@section('titulo')

  <title>Nueva Horario</title>

@endsection



@section('cuerpo')

<div id="page-wrapper">

  <div class="row">

    <div class="col-sm-12">

      <h1 class="page-header text-center"> {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}    Nuevo horario</h1>

      @include('mensajes.errores')

      <p>

        Nota: Todos los campos con (*) son obligatorios.

      </p>

    </div>



    <div class="col-sm-12">

      {!!Form::model($dato,  ['route'=>['tutores.update', $dato->id_tutor], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'modificacionnestudiante'])!!}



        <div class="tab-content panel panel-default">

        <div id="home" class="tab-pane fade in active">

          <h3>TUTORES</h3>

          @include('tutores.form.tutoresedit')

        </div>

       
       

        <div id="menu4" class="tab-pane fade">

          <h3>Modificar Tutor</h3>

          <button type="submit" name="registrar" class="btn btn-success btn-lg"><i class="fa fa-save"></i> Registrar</button>


        </div>
        <br/>
       

        


      <ul class="nav nav-pills">
           

        <li class="active">{!! link_to(URL::previous(), 'CANCELAR', ['class' => 'btn btn-info fa fa-times']) !!} <i class="fa"></i></li>

        <li class="active"><a data-toggle="tab" href="#home"> tutores <i class="fa fa-bookmark-o"></i></a></li>

      

        <li><a data-toggle="tab" href="#menu4"> Registrar <i class="fa fa-save"></i></a></li>

        </ul>
        </center>
        </div>

        </div>



      {!!Form::close()!!}

    </div>
       @include('mensajes.carga')
  </div>

</div>

@endsection

