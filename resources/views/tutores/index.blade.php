@extends('principal')

@section('titulo')
  <title>tutores</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">



<div class="panel-body ">
        <h1 class="page-header text-center"> <div class="fa fa-users" aria-hidden="true"></div>TUTORES O PADRES DE FAMILIA</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> ADMINISTRA </strong> y modifica datos de padres de estudiantes en su institucion
            </h4>

          
           
          </center>
        </div>
        
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
         @permission('crear-alumno')

        {!!link_to_route('inscripcionestudiantes.create', $title = 'Nueva Inscripción', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus'])!!}
        @include('tutores.search')
         @endpermission



         @if(count($tutores) == 0)
          <p class="text-info">
            No se han registrado tutores aun...
          </p>

        </center>




        
      @else
      <div class="table-responsive panel panel-default">

        <table class="table table-hover">
            <thead>
              <tr>
             
                <th>
                  PADRE
                </th>
                <th>
                  CORREO 
                </th>
                <th>
                  TELEFONO 
                </th>
                <th>
                  ENCARGADO DE 
                 
                </th>

                @permission('editar-tutores')
                <th>
                  OPCIONES
                </th>
              
                <th>
                  ESTADO
                </th>
                @endpermission
              </tr>
            </thead>
            <tbody id="datosEstudiantes">
              @foreach($tutores as $key => $tutor)
                <tr>
                 
                  <td>

                  {{ mb_strtoupper($tutor->apellidos_tutor) }}

                  {{ mb_strtoupper($tutor->nombre_tutor) }}

                    
                  </td>
                   <td>
                    {{ mb_strtoupper($tutor->correo_electronico_tutor) }}
                  </td>
                   <td>
                    {{ mb_strtoupper($tutor->telefono_primario_tutor) }}
                  </td>
                   <td>
                    {{ mb_strtoupper($tutor->nombre_estudiante) }}

                    {{ mb_strtoupper($tutor->apellidos_estudiante) }}
                  </td>
                  
                  @permission('editar-tutores')
                  <td>
                    {!!link_to_route('tutores.edit', $title = 'Editar ', $parameters = $tutor->id_tutor, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}
                    
                  </td>
                 
                  <td>
  
                     @if($tutor->estado_tutor == TRUE)
                              <input type="checkbox" name="estado" checked value="{{ $tutor->id_tutor }}" class="toggleEstado">
                            @else
                              <input type="checkbox" name="estado" value="{{ $tutor->id_tutor }}" class="toggleEstado">
                            @endif
                  </td>
                  @endpermission
                </tr>
              @endforeach
            </tbody>


          </table>
          </div>
           <div class="text-center">
            {!! $tutores->links() !!}
          </div>
          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
          @include('mensajes.carga')
          @endif
      
      </div>
    </div>
  </div>
@endsection