@extends('principal')

@section('titulo')
  <title>Nueva Asignación Docente</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nueva Asignación Docentes</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::open(['route'=>'asignaciondocente.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'nasignaciondocente'])!!}
          <div class="form-group">
            {!!Form::label('docente', 'Docente*', ['class'=>'col-sm-2 control-label'])!!}
            <div class="col-sm-10">
              {!!Form::text('docente', null, ['class'=>'form-control', 'placeholder'=>'Busqueda del docente...', 'id'=>'autocompleteDocente']);!!}
              {!!Form::hidden('id_persona', null, ['id'=>'idPersona'])!!}
            </div>
          </div>
          <div class="form-group">
            {!!Form::label('plan', 'Plan*', ['class'=>'col-sm-2 control-label'])!!}
            <div class="col-sm-10">
              {!!Form::select('id_plan', $planes, null, ['class'=>'form-control', 'placeholder'=>'Seleccione un plan...', 'id'=>'pAsignacionDocente'])!!}
            </div>
          </div>
          <div class="form-group">
            {!!Form::label('jornada', 'Jornada*', ['class'=>'col-sm-2 control-label'])!!}
            <div class="col-sm-10">
              {!!Form::select('id_jornada', $jornadas, null, ['class'=>'form-control', 'placeholder'=>'Seleccione una jornada...', 'id'=>'jAsignacionDocente'])!!}
            </div>
          </div>
          <div class="form-group">
            {!!Form::label('nivel', 'Nivel*', ['class'=>'col-sm-2 control-label'])!!}
            <div class="col-sm-10">
              {!!Form::select('id_nivel', $niveles, null, ['class'=>'form-control', 'placeholder'=>'Seleccione un nivel...', 'id'=>'nAsignacionDocente'])!!}
            </div>
          </div>
          <div class="form-group">
            {!!Form::label('grado', 'Grado*', ['class'=>'col-sm-2 control-label'])!!}
            <div class="col-sm-10">
              {{--{!!Form::select('animal', array('Cats' => array('leopard' => 'Leopard'), 'Dogs' => array('spaniel' => 'Spaniel'),), null, ['class'=>'form-control', 'placeholder'=>'Seleccione un grado...'])!!}--}}
              {!!Form::select('id_grado', [], null, ['class'=>'form-control', 'placeholder'=>'Seleccione un grado...', 'id'=>'gAsignacionDocente'])!!}
            </div>
          </div>
          <div class="form-group" >
            {!!Form::label('pensum', 'Pensum', ['class'=>'col-sm-2 control-label'])!!}
            <div class="col-sm-10" id="pensumGrado">

            </div>
          </div>
          <div class="form-group" >
            {!!Form::label('asignacion', 'Asignación*', ['class'=>'col-sm-2 control-label'])!!}
            <div class="col-sm-10">

            <div class="panel-group">
              <div class="panel panel-default">
               <div class="panel-heading">
                 <h4 class="panel-title">
                   <a data-toggle="collapse" href=".collapse1">Cursos Asignados <span class="badge" id="countCursos">0</span></a>
                 </h4>
               </div>
               <div id="asignacionDocente" class="panel-collapse collapse collapse1">
                 
               </div>
              </div>
            </div>

            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" name="button" class="btn btn-success"><span class="fa fa-save"></span> Registrar</button>
            </div>
          </div>
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
