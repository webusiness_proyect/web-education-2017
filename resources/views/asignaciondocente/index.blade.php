@extends('principal')



@section('titulo')

  <title>Asignación Docentes</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">
 <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-flag" aria-hidden="true"></div>Asignación de Cursos a  Docentes</h1>

        @include('mensajes.msg')

          </div>
        <div class="col-sm-12 panel panel-default">

          <br/>
           <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}

        @permission('crear-asignacion')


          {!!link_to_route('asignaciondocente.create', $title = 'Nueva Asignación', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus'])!!}


          <br>


          <br>


        @endpermission


         @include('asignaciondocente.search')


        @if (count($docentes) == 0)


          <p class="text-info">


            No se han registrado asignaciónes de docentes aun.


          </p>

          </center>
        @else

          <div class="table-responsive">

            <table class="table table-hover">

              <thead>

                <tr>

                  <th>

                    NO

                  </th>

                  <th>

                    DOCENTE

                  </th>

                  <th>

                    ACTUALIZAR

                  </th>

                  <th>

                    VER ASIGNACIÓN

                  </th>



                </tr>

              </thead>

              <tbody>

                @foreach($docentes as $key => $docente)

                  <tr>

                    <td>

                      {{ $key+1 }}

                    </td>

                    <td>

                      {{ mb_strtoupper($docente->nombres_persona.' '.$docente->apellidos_persona) }}

                    </td>

                    <td>

                      {!!link_to_route('asignaciondocente.edit', $title = 'Editar Asignación', $parameters = $docente->id_persona, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}

                    </td>

                    <td>

                      {!!link_to_route('asignaciondocente.show', $title = 'Ver Cursos', $parameters = $docente->id_persona, $attributes = ['class'=>'btn btn-default fa fa-eye'])!!}

                    </td>

            

                  </tr>

                @endforeach

              </tbody>

            </table>
             <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

          @include('mensajes.carga')

          <div class="text-center">

            {!! $docentes->links() !!}

          </div>


          </div>

        @endif

      </div>

    </div>

  </div>

@endsection

