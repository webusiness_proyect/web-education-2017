@extends('principal')







@section('titulo')



  <title>Planes</title>



@endsection







@section('cuerpo')



  <div id="page-wrapper">



    <div class="row">



      <div class="col-sm-12">

      <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="glyphicon glyphicon-time" aria-hidden="true"></div> Planes de estudios disponibles </h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong>y administra planes de clases disponibles en tu institucion ejemplo:<strong> PLAN:DIARIO</strong>
            </h4>
           
          </center>
        </div>
    
        @include('mensajes.msg')
        </div>
      </div>



      </div>



      <div class="col-sm-12 panel panel-default">

        <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}


        @permission('crear-plan')



        {!!link_to_route('planes.create', $title ='Nuevo Plan', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square'])!!}



        <br>



        <br>



        @endpermission



        @if (count($planes) == 0)



          <p class="text-info">



            No se han registrado planes aun...



          </p>

          </center>



        @else



          <table class="table table-hover table-bordered">



            <thead>



              <tr>



                <th>



                  NO



                </th>



                <th>



                  NOMBRE PLAN



                </th>



                @permission('editar-plan')



                <th>



                  ACTUALIZAR



                </th>



                @endpermission



                @permission('estado-plan')



                <th>



                  ESTADO



                </th>



                @endpermission



              </tr>



            </thead>



            <tbody id="datosPlanes">



              @foreach($planes as $key => $plan)



                <tr>



                  <td>



                    {{ $key+1 }}



                  </td>



                  <td>



                    {{ mb_strtoupper($plan->nombre_plan) }}



                  </td>



                  @permission('editar-plan')



                  <td>



                    {!!link_to_route('planes.edit', $title = 'Editar', $parameters = $plan->id_plan, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}



                  </td>



                  @endpermission



                  @permission('estado-plan')



                  <td>




                    @if($plan->estado_plan == TRUE)
                      <input type="checkbox" name="estado" checked value="{{ $plan->id_plan }}" class="toggleEstado">
                  @else
                      <input type="checkbox" name="estado" value="{{ $plan->id_plan }}" class="toggleEstado">
                  @endif




                  </td>



                  @endpermission



                </tr>



              @endforeach



            </tbody>



          </table>



          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">



        @endif



      </div>



    </div>







    <!-- Mensaje de carga-->



    @include('mensajes.carga')







  </div>















@endsection



