@extends('principal')

@section('titulo')
  <title>Unidades</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center"> <div class="fa fa-pencil-square" aria-hidden="true"></div>Unidades</h1>
        <div class="panel panel-default">

          <div class="container">
            <div class="row">
            <div class="col-sm-10">
           
            <center><a href="#demo"  data-toggle="collapse"><div class="fa fa-question-circle fa-2x "></div><H4><STRONG>INFORMACION</STRONG></H4></a></center>
            <div id="demo" class="collapse">
             
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra las unidades en bimestres, trimestres o semestres en las que estara dividido tu ciclo escolar actual
            </h4>
          </center>
        </div>
      </div>
    </div>
 </div>
    </div>
        @include('mensajes.msg')
      </div>
      <div class="col-sm-12">
        <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
        @permission('crear-curso')
        {!!link_to_route('unidades.create', $title = ' Nueva Unidad', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus'])!!}
        @endpermission
        <br/>
       
      </center>
     
        <br/>
      </div>
      <div class="col-sm-12  panel panel-default">
        @if(count($unidades) ==  0)
          <p class="text-info">
            No se han registrado unidades aun.
          </p>
        @else
          <div class="table-responsive">
            <table class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    NOMBRE
                  </th>
                  <th>
                    FECHA_INICIO
                  </th>
                  <th>
                    FECHA_FINAL
                  </th>
                  <th>
                    EDITAR
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach($unidades as $key => $unidad)
                  <tr>
                    <td>
                      {{ $key+1 }}
                    </td>
                    <td>
                      {{ ucwords($unidad->nombre_unidad) }}
                    </td>
                    <td>
                      {{ $unidad->fecha_inicio }}
                    </td>
                    <td>
                      {{ $unidad->fecha_final }}
                    </td>
                    <th>
                      {!!link_to_route('unidades.edit', $title = ' Editar', $parameters = $unidad->id_unidad, $attributes = ['class'=>'btn btn-warning fa fa-edit'])!!}
                    </th>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        @endif
      </div>
    </div>
  </div>
@endsection
