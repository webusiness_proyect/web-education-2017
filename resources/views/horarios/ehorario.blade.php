@extends('principal')



@section('titulo')

  <title>Nueva Horario</title>

@endsection



@section('cuerpo')

<div id="page-wrapper">

  <div class="row">

    <div class="col-sm-12">

      <h1 class="page-header text-center"> {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}    Nuevo horario</h1>

      @include('mensajes.errores')

      <p>

        Nota: Todos los campos con (*) son obligatorios.

      </p>

    </div>



    <div class="col-sm-12">

      {!!Form::model($dato, ['route'=>['horarios.update', $dato->idcalendario], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'nhorario'])!!}

        

        @include('horarios.form.horarios')

      {!!Form::close()!!}

    </div>
       @include('mensajes.carga')
  </div>

</div>

@endsection

