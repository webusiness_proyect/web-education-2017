@extends('principal')

@section('titulo')
  <title>Horarios</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel-body ">
        <h1 class="page-header text-center"> <div class="fa fa-calendar" aria-hidden="true"></div> - Todos los Horarios</h1>
        <div class="panel panel-default">
          <div class="row">
            <div class="col-sm-10">
           
            <center><a href="#demo"  data-toggle="collapse"><div class="fa fa-question-circle fa-2x "></div><H4><STRONG>INFORMACION</STRONG></H4></a></center>
            <div id="demo" class="collapse">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CONFIGURA </strong> tu HORARIO, selecciona un grado de tu pensum, selecciona el cursos y asignales horario ejemplo:<strong>*PRIMERO PRIMARIA, CURSO DE MATEMATICAS, horario de 8:00 a 10:00</strong>
            </h4>
           
          </center>
        </div>
        </div>
        </div>
        </div>
          <h6>
         <CENTER>
          * DEBERAS DE CONFIGURAR EL <a href="{{ url('/asignarniveles') }}">PLAN NIVELES</a> PREVIAMENTE

            
          </CENTER>
          </h6>
        @include('mensajes.msg')
    
        <div class="form-group">

            
         
         
       

        </div>
      </div>
      </div>
     <div class="col-sm-12 panel panel-default">
     <br/>
     <div class="col-sm-12 panel panel-default">
      @include('horarios.search')
      </div>
      <br/>
        <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
        @permission('crear-horario')
        {!!link_to_route('horarios.create', $title = 'Nuevo horario', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square'])!!}

        @endpermission
          
        @if(count($horarios) == 0)
          <p class="text-info">
            No se han registrado horarios aun.
          </p>

          </center>
        @else

        <div class="table-responsive">

        <div class="col-sm-2 panel panel-default" >
          <table class="table table-hover table-bordered col-sm-2 panel panel-default">
            <thead>
              <tr>
                

                <th>
                  LUNES
                </th>
              
              </tr>
            </thead>
            <tbody id="datosGrados">
             
              @foreach($horarios as $key => $horario)
                <tr>
                
                  
                   

                  <td>
                      <h6>
                     <b>INICIA:</b>{{ mb_strtoupper($horario->inicio) }}
                     <b>FIN:</b>{{ mb_strtoupper($horario->fin) }}<br/>
                     <b>CURSO:{{ mb_strtoupper($horario->nombre_area) }}</b>
                     
                     
                     <b>GRADO:</b>{{ mb_strtoupper($horario->nombre_grado) }}
                     {{ mb_strtoupper($horario->nombre_carrera) }}
                   
                       {{ mb_strtoupper($horario->nombre_jornada) }}
                     {{ mb_strtoupper($horario->nombre_plan) }}
                     {{ mb_strtoupper($horario->nombre_nivel) }}
                     
                    </h6>
                    
                     
                     @permission('crear-horario')
                     {!!link_to_route('horarios.edit', $title = 'Editar', $parameters = $horario->idcalendario, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}
                     @endpermission

                  </td>
                </tr>
                @endforeach
             
            
            </tbody>
          </table>
        </div>
        <div class="col-sm-2 panel panel-default" >

          <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                  MARTES
                </th>
              </tr>
            </thead>
            <tbody id="datosGrados">
               @foreach($martes as $key => $martes)
                <tr>
                  <td>
                    <h6>
                     <b>INICIA:</b>{{ mb_strtoupper($martes->inicio) }}
                     <b>FIN:</b>{{ mb_strtoupper($martes->fin) }}<br/>
                     <b>CURSO:{{ mb_strtoupper($martes->nombre_area) }}</b>
                     
                     
                     <b>GRADO:</b>{{ mb_strtoupper($martes->nombre_grado) }}
                     {{ mb_strtoupper($martes->nombre_carrera) }}
                   
                       {{ mb_strtoupper($martes->nombre_jornada) }}
                     {{ mb_strtoupper($martes->nombre_plan) }}
                     {{ mb_strtoupper($martes->nombre_nivel) }}
                     
                    </h6>
                    
                     
                     @permission('crear-horario')
                     {!!link_to_route('horarios.edit', $title = 'Editar', $parameters = $martes->idcalendario, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}
                     @endpermission

                  </td>
                 
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>
        

        <div class="col-sm-2 panel panel-default" >

        <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                  MIERCOLES
                </th>
              </tr>
            </thead>
            <tbody id="datosGrados">
               @foreach($miercoles as $key => $martes)
                <tr>
                  <td>
                      <h6>
                     <b>INICIA:</b>{{ mb_strtoupper($martes->inicio) }}
                     <b>FIN:</b>{{ mb_strtoupper($martes->fin) }}<br/>
                     <b>CURSO:{{ mb_strtoupper($martes->nombre_area) }}</b>
                     
                     
                     <b>GRADO:</b>{{ mb_strtoupper($martes->nombre_grado) }}
                     {{ mb_strtoupper($martes->nombre_carrera) }}
                   
                       {{ mb_strtoupper($martes->nombre_jornada) }}
                     {{ mb_strtoupper($martes->nombre_plan) }}
                     {{ mb_strtoupper($martes->nombre_nivel) }}
                     
                    </h6>
                    
                     
                     @permission('crear-horario')
                     {!!link_to_route('horarios.edit', $title = 'Editar', $parameters = $martes->idcalendario, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}
                     @endpermission

                  </td>
              </tr>
                   @endforeach

            </tbody>
          </table>
        </div>

        <div class="col-sm-2 panel panel-default">

                    <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                  JUEVES
                </th>
              </tr>
            </thead>
            <tbody id="datosGrados">
             
               @foreach($jueves as $key => $martes)
               <tr>
                  <td>
                      <h6>
                     <b>INICIA:</b>{{ mb_strtoupper($martes->inicio) }}
                     <b>FIN:</b>{{ mb_strtoupper($martes->fin) }}<br/>
                     <b>CURSO:{{ mb_strtoupper($martes->nombre_area) }}</b>
                     
                     
                     <b>GRADO:</b>{{ mb_strtoupper($martes->nombre_grado) }}
                     {{ mb_strtoupper($martes->nombre_carrera) }}
                   
                       {{ mb_strtoupper($martes->nombre_jornada) }}
                     {{ mb_strtoupper($martes->nombre_plan) }}
                     {{ mb_strtoupper($martes->nombre_nivel) }}
                     
                    </h6>
                    
                     
                     @permission('crear-horario')
                     {!!link_to_route('horarios.edit', $title = 'Editar', $parameters = $martes->idcalendario, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}
                     @endpermission

                  </td>
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>

        <div class="col-sm-2 panel panel-default">

        <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                  VIERNES
                </th>
              </tr>
            </thead>
            <tbody id="datosGrados">
               @foreach($viernes as $key => $martes)
               <tr>
                  <td>
                      <h6>
                     <b>INICIA:</b>{{ mb_strtoupper($martes->inicio) }}
                     <b>FIN:</b>{{ mb_strtoupper($martes->fin) }}<br/>
                     <b>CURSO:{{ mb_strtoupper($martes->nombre_area) }}</b>
                     <b>GRADO:</b>{{ mb_strtoupper($martes->nombre_grado) }}
                     {{ mb_strtoupper($martes->nombre_carrera) }}
                   
                       {{ mb_strtoupper($martes->nombre_jornada) }}
                     {{ mb_strtoupper($martes->nombre_plan) }}
                     {{ mb_strtoupper($martes->nombre_nivel) }}
                    </h6>
                     @permission('crear-horario')
                     {!!link_to_route('horarios.edit', $title = 'Editar', $parameters = $martes->idcalendario, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}
                     @endpermission
                  </td>
                </tr>
                @endforeach
            </tbody>
          </table>
        </div>
         <div class="col-sm-2 panel panel-default">

          <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                  SABADO
                </th>
              </tr>
            </thead>
            <tbody id="datosGrados">
               @foreach($sabado as $key => $martes)
               <tr>
                  <td>
                      <h6>
                     <b>INICIA:</b>{{ mb_strtoupper($martes->inicio) }}
                     <b>FIN:</b>{{ mb_strtoupper($martes->fin) }}<br/>
                     <b>CURSO:{{ mb_strtoupper($martes->nombre_area) }}</b>
                     
                     
                     <b>GRADO:</b>{{ mb_strtoupper($martes->nombre_grado) }}
                     {{ mb_strtoupper($martes->nombre_carrera) }}
                   
                       {{ mb_strtoupper($martes->nombre_jornada) }}
                     {{ mb_strtoupper($martes->nombre_plan) }}
                     {{ mb_strtoupper($martes->nombre_nivel) }} 
                    </h6>
                     @permission('crear-horario')
                     {!!link_to_route('horarios.edit', $title = 'Editar', $parameters = $martes->idcalendario, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}
                     @endpermission
                  </td>
              </tr>
              @endforeach
                
            </tbody>
          </table>
        </div>
        <div class="col-sm-2 panel panel-default">
          <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                  DOMINGO
                </th>
              </tr>
            </thead>
            <tbody id="datosGrados">
               @foreach($domingo as $key => $martes)
               <tr>
                  <td>
                      <h6>
                     <b>INICIA:</b>{{ mb_strtoupper($martes->inicio) }}
                     <b>FIN:</b>{{ mb_strtoupper($martes->fin) }}<br/>
                     <b>CURSO:{{ mb_strtoupper($martes->nombre_area) }}</b>
                     <b>GRADO:</b>{{ mb_strtoupper($martes->nombre_grado) }}
                     {{ mb_strtoupper($martes->nombre_carrera) }}
                    {{ mb_strtoupper($martes->nombre_jornada) }}
                     {{ mb_strtoupper($martes->nombre_plan) }}
                     {{ mb_strtoupper($martes->nombre_nivel) }}
                    </h6>
                     @permission('crear-horario')
                     {!!link_to_route('horarios.edit', $title = 'Editar', $parameters = $martes->idcalendario, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}
                     @endpermission
                  </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        </div>

          <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
          @include('mensajes.carga')
        @endif
      </div>
    </div>
  </div>
@endsection
