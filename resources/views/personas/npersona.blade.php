@extends('principal')

@section('titulo')
  <title>Nuevo Empleado</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nuevo Empleado</h1>
        <p>
          Nota: Todos los campos con (*) son olbigatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::open(['route'=>'personas.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'npersona'])!!}
          @include('personas.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
