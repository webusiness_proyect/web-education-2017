@extends('principal')

@section('titulo')
  <title>Empleados</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
       <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-users" aria-hidden="true"></div><div class="fa fa-gear" aria-hidden="true"></div>Empleados de mi Institución</h1>
        <div class="panel panel-default">
        <center><a href="#demo"  data-toggle="collapse"><div class="fa fa-question-circle fa-2x "></div><H4><STRONG>INFORMACION</STRONG></H4></a></center>
          <div id="demo" class="collapse">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra los empleados de tu institucion ejemplo:<strong>Juan Perez, *docente </strong>
            </h4>

          
           
          </center>
           <div class="alert alert-warning alert-dismissable">
          <center>

            <button type="button" class="close" data-dismiss="alert">&times;</button>
           <i class="fa fa-info btn btn-default" aria-hidden="true"></i> LOS ESTUDIANTES SERAN CREADOS EN EL MODULO <a href="{{ url('/inscripcionestudiantes') }}">INSCRIPCION ESTUDIANTES</a><br/>
           <i class="fa fa-info btn btn-default" aria-hidden="true"></i> LOS EMPLEADOS NO PODRAN ACCEDER A LA PLATAFORMA A MENOS DE QUE CREES UN  <a href="{{ url('/usuarios') }}">USUARIO</a> DESPUES DE REGISTRAR UNA PERSONA

            
          </CENTER>
    
        </div>
          <h6>
         <CENTER>
          * DEBERAS DE CREAR UN  <a href="{{ url('/puestos') }}">PUESTO</a> PREVIAMENTE <BR/>
          </div>
        </div>
       
          

        @include('mensajes.msg')

        </div>
      </div>
      <div class="col-sm-12 panel panel-default">

      <br/>
        <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
        @permission('crear-empleado')
        {!!link_to_route('personas.create', $title = 'Nuevo Empleado', $parameters = null, $attributes =['class'=>'btn btn-primary fa fa-plus'])!!}
        <br>
        <br>
        @endpermission
        @include('personas.search')
        @if(count($personas) == 0)
          <p class="text-info">
            No se han registrado empleados aun.
          </p>
          </center>
        @else
          <div class="table-responsive">
            <table class="table table-hover table- table-bordered">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                   DATOS DEL EMPLEADO
                  </th>
                  <th>
                    TELEFONO
                  </th>
                  <th>
                    DPI
                  </th>
                  <th>
                    CORREO
                  </th>
                  <th>
                    PUESTO
                  </th>
                  @permission('editar-empleado')
                  <th>
                    ACTUALIZAR
                  </th>
                  @endpermission
                  @permission('estado-empleado')
                  <th>
                    ESTADO
                  </th>
                  @endpermission
                  @permission('ver-empleado')
                  <th>
                    MAS DATOS
                  </th>
                  @endpermission
                </tr>
              </thead>
              <tbody id="datosPersonas">
                @foreach($personas as $key => $persona)
                  <tr>
                    <td>
                      {{ $persona->id_persona }}
                    </td>
                    <td>
                      {{ $persona->apellidos_persona }}
                   
                      {{ $persona->nombres_persona }}
                    </td>
                    <td>
                      {{ $persona->telefono_persona }}
                    </td>
                    <td>
                      {{ $persona->cui_persona }}
                    </td>
                     <td>
                      {{ $persona->correo_persona }}
                    </td>
                     <td>
                      {{ $persona->nombre_puesto }}
                    </td>
                    @permission('editar-empleado')
                    <td>
                      {!!link_to_route('personas.edit', $title = 'Editar', $parameters = $persona->id_persona, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}
                    </td>
                    @endpermission
                    @permission('estado-empleado')
                    <td>
                      @if($persona->estado_persona == true)
                        <input type="checkbox" name="estado" checked value="{{ $persona->id_persona }}" class="toggleEstado">
                      @else
                        <input type="checkbox" name="estado" value="{{ $persona->id_persona }}" class="toggleEstado">
                      @endif
                    </td>
                    @endpermission
                    @permission('ver-empleado')
                    <td>
                      {!!link_to_route('personas.show', $title = 'Ver Más', $parameters = $persona->id_persona, $attributes = ['class'=>'btn btn-info'])!!}
                    </td>
                    @endpermission
                  </tr>
                @endforeach
              </tbody>
            </table>
            {!!Form::hidden('_token', csrf_token(), ['id'=>'token'])!!}
            <div class="text-center">
            {!! $personas->links() !!}
          </div>
          </div>
        @endif
      </div>
    </div>
  </div>
@endsection
