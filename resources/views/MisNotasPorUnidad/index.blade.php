@extends('principalalumno')






@section('titulo')



  <title>Actividades</title>



@endsection







@section('cuerpo')



  <div id="page-wrapper">



    <div class="row">



      <div class="col-sm-12">



        <h1 class="page-header text-center "> <div class="glyphicon glyphicon-book"></div> NOTAS </h1>



        @include('mensajes.msg')



      </div>

            <div class="col-sm-12">



        <div class="alert alert-info">





         

          <center>

          



            <H4><div class="fa fa-check-square"> Aqui apareceran todas las notas de los <strong>cursos</strong> de las unidades anteriores

            </H4>

            </div>

          

          </center>



          



        </div>



      </div>


           <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
       
      </center>






      <div class="col-sm-12">







        <p>



       







         

        </p>



        @if (count($notas) == 0)



            <div class="alert alert-warning">





         

          <center>

          



            <H3><div class="fa  fa-info-circle">  SU USUARIO HA SIDO DESACTIVADO O NO SE HAN CREADO NOTAS, CONTACTESE CON SU COLEGIO </H3>



            </div>

          

          </center>
        @else





          <div class="table-responsive">



            <table class="table table-hover table-bordered">



              <thead>



                <tr>



                  <th>



                    UNIDAD



                  </th>



                  <th>



                    NOMBRE CURSO



                  </th>



                  <th>



                    TOTAL



                  </th>

                  <th>



                    ESTATUS DE LA NOTA



                  </th>





                 



                </tr>



              </thead>



              <tbody>



                @foreach ($notas as $key=>$actividad)

                



                  <tr>



                    <td >



                      
                    {{ mb_strtoupper($actividad->nombre_unidad) }}


                    </td>



                    



                    <td>



                    {{ mb_strtoupper($actividad->nombre_area) }}



                  </td>



                  



                  <td>



                    {{ ($actividad->total) }}



                  </td>



                  @if($actividad->total<60)



                  <td class="btn-danger"> <h4> <li class="fa fa-remove"> NO APROBADA </h4></li></td>

                      @else

                      <td class="btn-success"> <h4> <li class="fa fa-info-circle">  APROBADA </h4></li></td>

                  @endif







                  



                 

                  

                  



                  </tr>



                  



                @endforeach







              </tbody>



            </table>

            



             <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">



          </div>



          <div class="alert alert-warning">





         

          <center>

          



            <H3><div class="fa  fa-info-circle">  SI TU NOTA NO APARECE EN ESTA SECCION ES POR QUE NO HAS ENTREGADO NINGUNA TAREA DESDE TU CUENTA O EL PROFESOR NO HA DEJADO ACTIVIDADES </H3>



            </div>

          

          </center>



          



        </div>





        @endif



      </div>



    </div>



  </div>



@endsection



