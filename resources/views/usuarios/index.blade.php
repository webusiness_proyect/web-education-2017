@extends('principal')



@section('titulo')

  <title>Usuarios</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">
          <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-user" aria-hidden="true"></div>Usuarios</h1>
        <div class="panel panel-default">
        <center><a href="#demo"  data-toggle="collapse"><div class="fa fa-question-circle fa-2x "></div><H4><STRONG>INFORMACION</STRONG></H4></a></center>
          <div id="demo" class="collapse">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra usuarios de tu institucion, el sistema creara un usuario y una conseña los cuales llegara a el correo del usuario registrado
            </h4>

          
           
          </center>
       
        <div class="alert alert-warning alert-dismissable">
          <center>

            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-info btn btn-default" aria-hidden="true"></i>ANTES DE CREAR UN USUARIO DEBERAS DE CREAR UNA <a href="{{ url('/personas') }}">PERSONA </a> <br/>
           <i class="fa fa-info btn btn-default" aria-hidden="true"></i> LOS ESTUDIANTES SERAN CREADOS EN EL MODULO <a href="{{ url('/inscripcionestudiantes') }}">INSCRIPCION ESTUDIANTES</a><br/>
           

            
          </CENTER>
    
        </div>
        </div>
          


        @include('mensajes.msg')

        </div>
        <div class="col-sm-12 panel panel-default">

          <br/>
           <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
        @permission('crear-usuario')

        {!!link_to_route('usuarios.create', $title = 'Nuevo Usuario', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus'])!!}

        <br>

        <br>

        @endpermission
        @include('usuarios.search')

        @if(count($usuarios) == 0)

          <p class="text-info">

            No se han registrado usuarios aun.

          </p>



        </center>



        @else

          <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>

                  <th>

                    CODIGO

                  </th>

                  <th>

                    NOMBRE

                  </th>

                   <th>

                    CORREO

                  </th>

                  <th>

                    TIPO DE USUARIO

                  </th>
                  @permission('crear-usuario')

                  <th>

                    EDITAR

                  </th>

                 
                  @endpermission                  


                </tr>

              </thead>

              <tbody id="datosUsuarios">

                @foreach($usuarios as $key => $usuario)

                  <tr>

                    <td>

                      {{  mb_strtoupper($usuario->id) }}

                    </td>

                    <td>

                      {{ mb_strtoupper($usuario->nombre) }}

                    </td>

                    <td>

                      {{ $usuario->correo }}

                    </td>

                     <td>

                      {{ $usuario->rol }}

                    </td>
                    @permission('editar-usuario')

                    <td>

                    {!!link_to_route('usuarios.edit', $title = 'Editar', $parameters = $usuario->id, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}

                  </td>

                    
                  @endpermission

                  </tr>

                @endforeach

              </tbody>

            </table>

            <div class="text-center">

            {!! $usuarios->links() !!}

          </div>

          </div>

        @endif

      </div>

    </div>

  </div>

@endsection

