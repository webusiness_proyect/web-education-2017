@extends('principal')



@section('titulo')

  <title>Editar Unidad</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center">Editar Unidad</h1>

        <p>

          Nota: Todos los campos con (*) son obligatorios.

        </p>

      </div>

      <div class="col-sm-12">

        {!!Form::model($usuario,['route'=>['usuarios.update', $usuario->id], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'nusuarios'])!!}

          @include('usuarios.form.camposedit')

        {!!Form::close()!!}

      </div>

    </div>

  </div>

@endsection

