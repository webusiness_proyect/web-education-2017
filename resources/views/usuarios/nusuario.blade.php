@extends('principal')

@section('titulo')
  <title>Nuevo Usuarios</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center"><div class="fa fa-plus" aria-hidden="true"></div><div class="fa fa-user" aria-hidden="true"></div>Nuevo Usuario</h1>
        <p>
          {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
         
      </div>
      <div class="col-sm-12">


        {!!Form::open(['route'=>'usuarios.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'nusuario'])!!}
          @include('usuarios.form.campos')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
