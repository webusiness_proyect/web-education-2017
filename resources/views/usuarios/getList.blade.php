@extends('principal')



@section('titulo')

  <title>Usuarios</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center">  <li class="fa fa-user-o"></li>Usuarios</h1>

        @include('mensajes.msg')

      </div>

      <div class="col-sm-12">

        @permission('crear-usuario')

        {!!link_to_route('usuarios.create', $title = 'Nuevo Usuario', $parameters = null, $attributes = ['class'=>'fa fa-user-plus btn btn-primary'])!!}

        <br>

        <br>

        @endpermission

        @if(count($usuarios) == 0)

          <p class="text-info">

            No se han registrado usuarios aun.

          </p>

        @else

          <div class="table-responsive">

            <table class="table table-hover">

              <thead>

                <tr>

                  <th>

                    NO 

                  </th>

                  <th>

                    NOMBRE

                  </th>

                </tr>

              </thead>

              <tbody>

                @foreach($usuarios as $key => $usuario)

                  <tr>

                    <td>

                      <div class="fa fa-user-circle"></div>{{ $key+1 }}

                    </td>

                    <td>

                      {{ mb_strtoupper($usuario->name) }}

                    </td>

                    <td>

                      {{ $usuario->email }}

                    </td>

                  </tr>

                @endforeach

              </tbody>

            </table>

            <center>

            {!! $usuarios->links() !!}

            </center>

          </div>

        @endif

      </div>

    </div>

    <div class="resul">
    </div>

  </div>

@endsection

