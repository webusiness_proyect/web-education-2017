{!! Form::open(array('url'=>'usuarios','method'=>'GET','autocomplete'=>'off','role'=>'search')) !!}
<div class="form-group">
<br/>
  <div class="input-group">
    
    <input type="text" class="form-control" name="searchText" id="busquedaUsuario" placeholder="Buscar usuarios por nombre por correo o por tipo de usuario..." value="{{$searchText}}">



    {!!Form::hidden('id', null, ['id'=>'idUsuario'])!!}
    {!!Form::hidden('id_persona', null, ['id'=>'idPersona'])!!}

   

    <span class="input-group-btn"> <div class="" aria-hidden="true"></div>

    <button type="submit" class="btn btn-info"><div class="fa fa-search"></div>Buscar</button>
    </span>
  </div>
</div>

{{Form::close()}}