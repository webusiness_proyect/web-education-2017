<div class="form-group">
  {!!Form::label('persona', 'Persona*', ['class'=>'col-sm-2 control-label'])!!}
  <div class="col-sm-10">
    {!!Form::text('persona', null, ['class'=>'form-control', 'placeholder'=>'Buscar persona...', 'id'=>'busquedaPersona'])!!}
  </div>
</div>
{!!Form::hidden('id_persona', null, ['id'=>'idPersona'])!!}
<div class="form-group">
  {!!Form::label('rol', 'Rol*', ['class'=>'col-sm-2 control-label'])!!}
  <div class="col-sm-10">
    {!!Form::select('id_rol', $roles, null, ['class'=>'form-control', 'placeholder'=>'Seleccione rol...'])!!}
  </div>
</div>
<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" name="guardar" class="btn btn-success"><span class="fa fa-save"></span> Guardar</button>
  </div>
</div>
