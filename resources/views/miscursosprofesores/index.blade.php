@extends('principal')



@section('titulo')

  <title>Mis Cursos Asignados</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

      <di <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-pencil" aria-hidden="true"></div> - - Cursos de este profesor </h1>
        <h4 class="text-center"><div class="glyphicon glyphicon-cog"></div>   {{ mb_strtoupper($unidad->nombre_unidad) }}</h4>


        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> INFORMATE </strong> de los cursos que son impartidos por este profesor a tu hijo
            </h4>

          
           
          </center>

        </div>
        
        

           <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
       
      </center>

        @include('mensajes.msg')

        </div>


        <div class="col-sm-12 panel panel-default">

          <br/>



        <div class="table-responsive">

          <table class="table table-hover table-bordered">

            <thead>

              <tr>

               <th>

               

                </th>

                <th>

                 <h4><STRONG><CENTER> CURSO</CENTER></h4></STRONG>

                </th>

                
               

                

              </tr>

            </thead>

            <tbody>

              @foreach($cursos as $key => $curso)

                <tr>

                   <td>

                 


                    <!-- Single button -->


                  <td>

                    <b>{{ mb_strtoupper($curso->nombre_area) }}</b>

                  </td>

                

                 

                 

                </tr>

              @endforeach

            </tbody>

          </table>

        </div>

      </div>

      <div class="col-sm-12 text-center">

      

      </div>

    </div>

  </div>

@endsection

