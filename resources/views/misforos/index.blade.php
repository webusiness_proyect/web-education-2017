@extends('principalalumno')







@section('titulo')



  <title>Mis Cursos</title>



@endsection







@section('cuerpo')



  <div id="page-wrapper">



    <div class="row">



      <div class="col-sm-12">



        <h1 class="page-header text-center"><div class="fa fa-comments"></div>  Mis foros - {{mb_strtoupper($unidad->nombre_unidad) }}</h3></h1>



        
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> PARTICIPA </strong> en foros y conversaciones sobre temas de interes en  tus cursos creados por tu profesor
            </h4>
           
          </center>
        </div>
     



        @include('mensajes.msg')

        @include('estudiantes.search')
        

           <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
       
      </center>




      </div>



      <div class="col-sm-12 panel panel-default">



        <div class="table-responsive">



          <table class="table table-hover table-bordered">



            <thead>



              <tr>




               <th>

                 <CENTER>MIS FOROS/CONVERSACIONES DE CURSOS</CENTER>

                </th>



                  <th>



                  GRADO



                </th>


                 <th>



                  PENSUM



                </th>

                <th>



                  SALON



                </th>






              </tr>



            </thead>



            <tbody>



              @foreach ($cursos as $key => $curso)



                <tr>



                  <td>

                  <center>


                   


                        <a href="/foros?a={{ $curso->id_asignacion_area }}"  class="btn btn-default"><div class="fa fa-comments"></div> FOROS Y CONVERSACIONES DE   {{ mb_strtoupper($curso->nombre_area) }}</a><br/>



                        



                  </center>

                  </td>




                  <td>



                    {{ mb_strtoupper($curso->nombre_grado) }}



                  </td>

                    <td>



                    {{ mb_strtoupper($curso->nombre_nivel) }}
                     {{ mb_strtoupper($curso->nombre_plan) }}
                      {{ mb_strtoupper($curso->nombre_jornada) }}
                      





                  </td>
                  <td>


                    {{ mb_strtoupper($curso->nombre_salon) }}


                  </td>




              



                </tr>
                



              @endforeach



            </tbody>



          </table>

           <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

          @include('mensajes.carga')

          <div class="text-center">

            {!! $cursos->links() !!}

          </div>



        </div>



      </div>



    </div>



  </div>



@endsection



