
<html>
<head>

  <title> Clientes </title>

  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>





  <div id="page-wrapper">



    <div class="row">



      <div class="col-sm-12">



        <h1 class="page-header text-center"> <div class="fa fa-pencil-square" aria-hidden="true"></div>CLIENTES</h1>
        
         <div class="panel panel-default">
          <center>
        
              {!!link_to_route('clientes.create', $title = 'REGISTRARSE', $parameters = ['a'=>$clientes], $attributes = ['class'=>'btn btn-primary fa fa-plus'])!!}

          </center>
        </div>


        @include('mensajes.msg')



      </div>







      <div class="col-sm-12 panel panel-default">







        <p>

         <center>   
     


       







       



        </p>



        @if (count($clientes) == 0)





          



        @else
        </center>



          <div class="table-responsive">



            <table class="table table-hover table-bordered">



              <thead>



                <tr>



                  <th>



                    NOMBRE



                  </th>



                  <th>



                    APELLIDO



                  </th>



                  <th>



                   CORREO



                  </th>

                   <th>



                    INSTITUCION



                  </th>



                  <th>



                    PAIS



                  </th>



                  <th>



                    TIPO DE LICENCIA



                  </th>



                  <th>



                    EDITAR



                  </th>

                   




                  <th>



                    VER INFORMACION DE CLIENTE



                  </th>





                </tr>



              </thead>



              <tbody>



                @foreach ($clientes as $key=>$actividad)



                  <tr>



                    <td>



                        {{ mb_strtoupper($actividad->nombre_cliente) }}



                    </td>



                    



                    <td>



                    {{ mb_strtoupper($actividad->apellido_cliente) }}



                  </td>



                  <td>



                    {{ mb_strtoupper($actividad->correo_cliente) }}



                  </td>

                   <td>



                    {{ mb_strtoupper($actividad->nombre_institucion) }}



                  </td>



                  <td>



                   {{ mb_strtoupper($actividad->nombre) }}



                  </td>



                


                  <td>



                    {{ mb_strtoupper($actividad->nombre_tipo_cuenta) }}



                  </td>

                   



                    <td>



                       {!!link_to_route('Actividad.edit', $title = 'Editar Cliente', $parameters = ['actividad'=>$actividad->id_cliente], $attributes = ['class'=>'btn btn-warning fa fa-edit'])!!}



                    </td>



                    <td>



                      <a class="btn btn-success fa fa-eye" href="misrespuestasactividades/?a={{$actividad->id_cliente}}" title="click aqui para ver el listado de tareas entregadas">VER DETALLES</a>

                      

                    </td>



                  </tr>



                @endforeach



              </tbody>



            </table>

          




             <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">



          </div>





        @endif



      </div>
      



    </div>



  </div>

  </body>

  </html>


