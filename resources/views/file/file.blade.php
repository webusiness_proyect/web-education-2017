<!DOCTYPE html>
<html>
    <head>
        <title>File Upload</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        
    </head>
    <body>

    
                @if (Session::has('success-message'))
                    <div class="alert alert-success">{{ Session::get('success-message') }}</div>
                @endif
 
                @if (Session::has('error-message'))
                    <div class="alert alert-danger">{{ Session::get('error-message') }}</div>
                @endif
       <form action="file/store" method="post" enctype="multipart/form-data">
            <label for="name"> name </label>
            <input type="text" name="name"><br/>

            <label for="image"> image </label>
            <input type="file" name="image"><br/>

            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="submit" name="submit" value="Submit">

       </form>
    </body>
</html>
