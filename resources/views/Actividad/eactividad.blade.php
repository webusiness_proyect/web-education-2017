@extends('principal')

@section('titulo')
  <title>Editar Curso</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Editar Curso</h1>
        <p>
          Nota: Todos los campos con (*) son olbigatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
        {!!Form::model($dato, ['route'=>['Actividad.update', $dato->id_actividad], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'nactividad'])!!}
          @include('Actividad.form.actividades')
        {!!Form::close()!!}
      </div>
    </div>
  </div>
@endsection
