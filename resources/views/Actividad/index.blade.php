@extends('principal')







@section('titulo')



  <title> Actividades y Tareas </title>



@endsection







@section('cuerpo')



  <div id="page-wrapper">



    <div class="row">



      <div class="col-sm-12">



        <h1 class="page-header text-center"> <div class="fa fa-pencil-square" aria-hidden="true"></div>ACTIVIDADES Y TAREAS DE  {{mb_strtoupper($area[0]->nombre_area)}}</h1>
        <h3 class="text-center"><div class="glyphicon glyphicon-cog"></div>   {{ mb_strtoupper($unidad->nombre_unidad) }}</h3>
         <div class="panel panel-default">
          <center>
            <h4>
            <a href="#demo2" class="btn btn-warning" data-toggle="collapse">
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" title="click aqui para mas informacion" aria-hidden="true"></i></a> <strong>LISTADO</strong> de tareas creadas para el curso de  {{mb_strtoupper($area[0]->nombre_area)}}
            </h4>

          
           
          </center>
          <div id="demo2" class="collapse">


          <div class="alert alert-warning alert-dismissable">
          <center>

            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-info btn btn-default" aria-hidden="true"></i>En esta sección aparecera el listado de todas tus tareas creadas por usted. Para ver el listado de tareas entregadas y  no entregadas da click en <strong>Ver Tareas Entregadas</strong><br/>
            <li> Tambien podras <strong>crear</strong> nuevas actividades o tareas para que tus alumnos las resuelvan en clase o en su casa </li>
           
            
          </CENTER>
    
        </div>

        </div>



        @include('mensajes.msg')



      </div>







      <div class="col-sm-12 panel panel-default">







        <p>

         <center>   
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}



       







          {!!link_to_route('Actividad.create', $title = ' Nueva actividad', $parameters = ['a'=>$curso], $attributes = ['class'=>'btn btn-primary fa fa-plus'])!!}



        </p>



        @if (count($actividades) == 0)





          



        @else
        </center>



          <div class="table-responsive">



            <table class="table table-hover table-bordered">



              <thead>



                <tr>



                  <th>



                    NO



                  </th>



                  <th>



                    NOMBRE ACTIVIDAD



                  </th>



                  <th>



                    DESCRIPCION ACTIVIDAD



                  </th>

                   <th>



                    UNIDAD



                  </th>



                  <th>



                    FECHA DE INICIO



                  </th>



                  <th>



                    FECHA ENTREGA



                  </th>



                  <th>



                    NOTA MAXIMA TOTAL



                  </th>

                   




                  <th>



                    EDITAR



                  </th>



                  <th>



                    VER TAREAS ENTREGADAS



                  </th>



                </tr>



              </thead>



              <tbody>



                @foreach ($actividades as $key=>$actividad)



                  <tr>



                    <td>



                        {{ $actividad->id_actividad }}



                    </td>



                    



                    <td>



                    {{ mb_strtoupper($actividad->nombre_actividad) }}



                  </td>



                  <td>



                    {{ mb_strtoupper($actividad->descripcion_actividad) }}



                  </td>

                   <td>



                    {{ mb_strtoupper($actividad->nombre_unidad) }}



                  </td>



                  <td>



                   {{ mb_strtoupper($actividad->fecha_inicio) }}



                  </td>



                  <td>



                    {{ mb_strtoupper($actividad->fecha_entrega) }}



                  </td>



                  <td>



                    {{ mb_strtoupper($actividad->nota_total) }}



                  </td>

                   



                    <td>



                       {!!link_to_route('Actividad.edit', $title = 'Editar Actividad', $parameters = ['actividad'=>$actividad->id_actividad], $attributes = ['class'=>'btn btn-warning fa fa-edit'])!!}



                    </td>



                    <td>



                      <a class="btn btn-success fa fa-eye" href="misrespuestasactividades/?a={{$actividad->id_actividad}}" title="click aqui para ver el listado de tareas entregadas">Ver tareas entregadas</a>

                      

                      <!--

                      {!!link_to_route('misrespuestasactividades.index', $title = 'Ver Entregas', $parameters = ['a'=>$actividad->id_actividad], $attributes = ['class'=>'btn btn-info '])!!}

                      -->

                    </td>



                  </tr>



                @endforeach



              </tbody>



            </table>

             </center>

              @foreach ($total as $key=>$total1)

        @if ($total=mb_strtoupper($total1->total)> 100)

                  <h1 class="text-danger">
                  <center>

                   NOTA TOTAL: {{ $total=mb_strtoupper($total1->total) }} </br>
                     </h1>
                  </center>
                     @else

                 <h1 class="text-success">
                  <center>
                   NOTA TOTAL: {{ $total=mb_strtoupper($total1->total) }} </br>
                   </h1>
                  </center>

                 
                 @endif
                   



            @endforeach


<div class="container">
 
  <center><a href="#demo" class="btn btn-default" data-toggle="collapse"><div class="fa fa-plus"></div>... MAS ACTIVIDADES DE UNIDADES ANTERIORES ...</a></center>
  <div id="demo" class="collapse">
   
        

          <div class="table-responsive">



            <table class="table table-hover table-bordered">



              <thead>



                <tr>



                  <th>



                    NO



                  </th>



                  <th>



                    NOMBRE ACTIVIDAD



                  </th>



                  <th>



                    DESCRIPCION ACTIVIDAD



                  </th>

                   <th>



                    UNIDAD



                  </th>



                  <th>



                    FECHA DE INICIO



                  </th>



                  <th>



                    FECHA ENTREGA



                  </th>



                  <th>



                    NOTA MAXIMA TOTAL



                  </th>

                   




                  <th>



                    EDITAR



                  </th>



                  <th>



                    VER TAREAS ENTREGADAS



                  </th>



                </tr>



              </thead>



              <tbody>



                @foreach ($anteriores as $key=>$actividad)



                  <tr>



                    <td>



                        {{ $actividad->id_actividad }}



                    </td>



                    



                    <td>



                    {{ mb_strtoupper($actividad->nombre_actividad) }}



                  </td>



                  <td>



                    {{ mb_strtoupper($actividad->descripcion_actividad) }}



                  </td>

                   <td>



                    {{ mb_strtoupper($actividad->nombre_unidad) }}



                  </td>



                  <td>



                   {{ mb_strtoupper($actividad->fecha_inicio) }}



                  </td>



                  <td>



                    {{ mb_strtoupper($actividad->fecha_entrega) }}



                  </td>



                  <td>



                    {{ mb_strtoupper($actividad->nota_total) }}



                  </td>

                   



                    <td>



                       {!!link_to_route('Actividad.edit', $title = 'Editar Actividad', $parameters = ['actividad'=>$actividad->id_actividad], $attributes = ['class'=>'btn btn-warning fa fa-edit'])!!}



                    </td>



                    <td>



                      <a class="btn btn-success fa fa-eye" href="misrespuestasactividades/?a={{$actividad->id_actividad}}">Ver tareas entregadas</a>

                      

                      <!--

                      {!!link_to_route('misrespuestasactividades.index', $title = 'Ver Entregas', $parameters = ['a'=>$actividad->id_actividad], $attributes = ['class'=>'btn btn-info '])!!}

                      -->

                    </td>



                  </tr>



                @endforeach



              </tbody>



            </table>
</div>
</div>








             <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">



          </div>





        @endif



      </div>
      



    </div>



  </div>



@endsection



