@extends('principal')
@section('titulo')
  <title>Empleados</title>
@endsection
@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
       <div class="panel-body">
        <h1 class="page-header text-center"> <div class="fa fa-users" aria-hidden="true"></div><div class="fa fa-search" aria-hidden="true"></div>Alumnos con este pensum</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-search fa-2x btn btn-default" aria-hidden="true"></i> Resultados de busqueda </strong> 
            </h4>
          </center>
        </div>
        @include('mensajes.msg')
        </div>
      </div>
      <div class="col-sm-12 panel panel-default">
      <br/>
        <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
        @if(count($students) == 0)
          <p class="text-info">
            No hay alumnos registrados a este curso y este año.
          </p>
          </center>
        @else
        <br/>
          <div class="table-responsive">
            <table class="table table-hover table- table-bordered">
              <thead>
                <tr>
                  <th>
                    OPCIONES
                  </th>
                  <th>
                    DATOS
                  </th>
                 </tr>
              </thead>
              <tbody id="datosPersonas">
                @foreach($students as $key => $persona)
                  <tr>
                    <td>              
                       <a class="btn btn-default fa fa-eye" href="buscarnotasbimestre/?a={{$persona->id}}&b={{$year}}">Ver notas de cursos</a>
                    </td>
                    <td>
                      {{ $persona->apellidos_estudiante }}
                      {{ $persona->nombre_estudiante }}

                    </td>
                   
                   
                   
                   
                  </tr>
                @endforeach
              </tbody>
            </table>
            {!!Form::hidden('_token', csrf_token(), ['id'=>'token'])!!}
            <div class="text-center">
            {!! $students->links() !!}
          </div>
          </div>
        @endif
      </div>
    </div>
  </div>
@endsection
