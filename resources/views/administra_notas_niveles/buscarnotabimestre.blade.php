@extends('principal')
@section('titulo')
  <title>Empleados</title>
@endsection
@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
       <div class="panel-body ">
        <h2 class="page-header text-center"> <div class="fa fa-users" aria-hidden="true"></div><div class="fa fa-gear" aria-hidden="true"></div>Notas de {{ucwords($students[0]->nombre_estudiante.' '.$students[0]->apellidos_estudiante)}}</h2>
        <h4 class="text-center">Año: {{$year}}</h4>
        <br/>


          <center>
                          {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}  
          </center>
   

        @include('mensajes.msg')
        </div>
      </div>
      <div class="col-sm-12 panel panel-default">
      <br/>

        @if(count($students) == 0)
          <p class="text-info">
            No hay alumnos registrados a este curso y este año.
          </p>
          </center>
        @else
          <div class="table-responsive">
            <table class="table table-hover table- table-bordered">
              <thead>
                <tr>
                  <th>
                    <H3><STRONG>CURSOS</STRONG></H3>
                  </th>
                  <th>
                    <H3><STRONG> SELECCIONE UNIDAD </STRONG></H3>
                  </th>
                 
                  
                
                 
                </tr>
              </thead>
              <tbody>
                @foreach($students as $key => $persona)
                  <tr>

                   <td>
                    <H4><STRONG>{{ $persona->nombre_area }}</STRONG></H4>
                   </td>
                   
                    <td>
                    @foreach($unidades as $key=>$unidad)
                        

                         <a href="/resultadosbuscarnotasbimestre/?unidad={{$unidad->id_unidad}}&area={{ $persona->id_area }}&year={{$year}}&user={{$persona->id}}" title="Click aqui para ver mis notas y puntos asignados para esta unidad" class="btn btn-default"><div class="fa fa-trophy" ></div> {{$unidad->nombre_unidad}}</a>
                         
                    @endforeach

                      
                      
                    </td>
                   
                   
                   
                   
                  </tr>
                @endforeach
              </tbody>
            </table>

           {!!Form::hidden('_token', csrf_token(), ['id'=>'token'])!!}

          </div>
        @endif
      </div>
    </div>
  </div>
@endsection
