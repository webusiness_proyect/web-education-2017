@extends('principal')
@section('titulo')
  <title>Empleados</title>
@endsection
@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
       <div class="panel-body ">
        <h2 class="page-header text-center"> <div class="fa fa-users" aria-hidden="true"></div><div class="fa fa-gear" aria-hidden="true"></div>Notas de {{ucwords($students[0]->nombre_estudiante.' '.$students[0]->apellidos_estudiante)}}</h2>
        <h4 class="text-center">{{ucwords($unidad[0]->nombre_unidad)}} del {{ucwords($unidad[0]->fecha_inicio)}} al {{ucwords($unidad[0]->fecha_final)}} Año: {{$year}}</h4>
        <br/>


          <center>
                          {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}  
          </center>
   

        @include('mensajes.msg')
        </div>
      </div>
      <div class="col-sm-12 panel panel-default">
      <br/>

        @if(count($actividades) == 0)
          <p class="text-info">
            No hay actividades entregadas por este alumno.
          </p>
          </center>
        @else
          <div class="table-responsive">
            <table class="table table-hover table- table-bordered">
              <thead>
                <tr>
                  <th>
                    <H3><STRONG>NOMBRE</STRONG></H3>
                  </th>
                  <th>
                    <H3><STRONG> DESCRIPCION </STRONG></H3>
                  </th>
                  <th>
                    <H3><STRONG> PUNTEO </STRONG></H3>
                  </th>
                  <th>
                    <H3><STRONG> MODIFICAR NOTA </STRONG></H3>
                  </th>
                 
                  
                
                 
                </tr>
              </thead>
              <tbody>
                @foreach($actividades as $key => $act)
                  <tr>

                   
                   
                    <td>
                    <H4><STRONG>{{ ucwords($act->nombre_actividad) }} </STRONG></H4>
                   </td>
                    <td>
                    <H4><STRONG>{{ ucwords($act->descripcion_actividad) }} </STRONG></H4>
                    </td>
                   <td>
                    <H4><STRONG>{{ ucwords($act->calificacion) }} </STRONG></H4>
                   </td>
                   <td>
                       

                    <H5> {!!link_to_route('resultadosbuscarnotasbimestre.edit', $title = 'CALIFICAR',  $parameters = [$act->id], $attributes = ['class'=>'btn btn-default  fa fa-check-square'])!!}</H5>

                  
                  </td>
                   
                   
                   
                  </tr>
                @endforeach
              </tbody>
            </table>

          

          </div>
        @endif


      </div>
      <div class="col-sm-12 panel panel-default">
      <br/>

        @if(count($sinnota) == 0)
          <p class="text-info">
            No hay notas sin entregar.
          </p>
          </center>
        @else
          <div class="table-responsive">
            <table class="table table-hover table- table-bordered">
              <thead>
                <tr>
                  <th>
                    <H3><STRONG>NOMBRE</STRONG></H3>
                  </th>
                  <th>
                    <H3><STRONG> DESCRIPCION </STRONG></H3>
                  </th>
                  <th>
                    <H3><STRONG> PUNTEO </STRONG></H3>
                  </th>
                  <th>
                    <H3><STRONG> CREAR NOTA </STRONG></H3>
                  </th>
                 
                  
                
                 
                </tr>
              </thead>
              <tbody>
                @foreach($sinnota as $key => $act)
                  <tr>

                   
                   
                    <td>
                    <H4><STRONG>{{ ucwords($act->nombre_actividad) }} </STRONG></H4>
                   </td>

                   <td>
                    <H4><STRONG>{{ ucwords($act->descripcion_actividad) }} </STRONG></H4>
                   </td>
                   <td class="alert-danger">
                     <center><strong> 0</strong></center>
                   </td>
                   <td>
                       

                    {!!link_to_route('areas.edit', $title = 'Crear Nota', $parameters = $act->id_actividad, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}

                  
                  </td>
                   
                   
                   
                  </tr>
                @endforeach
              </tbody>
            </table>

           {!!Form::hidden('_token', csrf_token(), ['id'=>'token'])!!}

          </div>
        @endif
    </div>
  </div>
@endsection
