@extends('principal')







@section('titulo')



  <title>Calificar actividad</title>



@endsection







@section('cuerpo')



  <div id="page-wrapper">



    <div class="row">



      <div class="col-sm-12">



        <h1 class="page-header text-center">Calificar Actividad</h1>



        @include('mensajes.errores')

        @foreach ($notas as $key=>$maximanota)



        





        <p>



          Nota: Todos los campos con (*) son obligatorios.

          {{ $nota_total=mb_strtoupper($maximanota->nota_total) }}



        </p>

        @endforeach



      </div>



      <div class="col-sm-12">



        {!!Form::model($ractividad, ['route'=>['resultadosbuscarnotasbimestre.update',$ractividad->id], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'calificarrespuestas'])!!}



          <input type="hidden" name="curso" value="0">



          @include('administra_notas_niveles.form.campos')



        {!!Form::close()!!}



      </div>



    </div>



  </div>



@endsection



