@extends('principal')



@section('titulo')

  <title>Actividades</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center"><div class="fa fa-eye" aria-hidden="true"></div><div class="fa fa-pencil-square" aria-hidden="true"></div>CALIFICA  TAREAS  DE {{mb_strtoupper($area[0]->nombre_area)}} </h1>
        <div class="panel panel-default">
          <center>
            <h4>
            <a href="#demo2" class="btn btn-warning" data-toggle="collapse">
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" title="click aqui para mas informacion" aria-hidden="true"></i></a> <strong>TAREAS ENTREGADAS</strong> 
            </h4>

          
           
          </center>
          <div id="demo2" class="collapse">


          <div class="alert alert-warning alert-dismissable">
          <center>

            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-info btn btn-default" aria-hidden="true"></i>En esta sección apareceran todas las <strong>Tareas Entregadas</strong> por tus alumnos<br/>
            <li> Tambien podras <strong>ver y crear</strong> notas para los alumnos que no entregaron su tarea o la traen de manera presencial(fisica) </li>
           
            
          </CENTER>
    
        </div>

        @include('mensajes.msg')
      </div>
      </div>
      

      <div class="col-sm-12 panel panel-default">
        <br/>
         <center>   
           {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
        <p>
        </p>
        @if (count($ractividades) == 0)
          <p class="text-info">
            No se han encontrado actividades para este curso...
            </center>
          </p>
        @else
<div class="container">
<div class="col-sm-12">
  <center><a href="#demo2" class="btn btn-success" data-toggle="collapse"><div class="fa fa-minus"></div> <div class="fa fa-check-circle"></div>TAREAS DE ESTA UNIDAD ENTREGADAS</a></center>
  </div>
  <div id="demo2" class="collapse in">
    <div class="row">
      <div class="col-sm-9">
         <div class="table-responsive">
           <table class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th>
                    <H5><STRONG>ESTADO </STRONG></H5>
                  </th>
                  <th>
                   <H5><STRONG>ALUMNO </STRONG></H5>
                  </th>         
                  <th id="nota">
                   <H5><STRONG> CALIFICACION </STRONG></H5>
                  </th>
                  <th>
                   <H5><STRONG> FECHA ENTREGA </STRONG></H5>
                  </th>
                  <th>
                   <H5><STRONG> COMENTARIOS </STRONG></H5>
                  </th>
                   <th>
                    <H5><STRONG>ARCHIVO </STRONG></H5>
                  </th>
                  <th >
                    <H5><STRONG> OPCIONES </STRONG></H5>
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($ractividades as $key=>$actividad)
                @foreach ($notas as $key=>$maximanota)
                  <tr>
                      @if($actividad->estado_entrega_actividad=="CALIFICADO")
                      <td class="btn-success">  <li class="fa fa-check-circle"> CALIFICADO </li></td>
                      @else
                      <td class="btn-danger">  <li class="fa fa-info-circle"> NO CALIFICADA </li></td>
                      @endif
                  <td>
                    <H5> {{ mb_strtoupper($actividad->apellidos_estudiante) }}
                    {{ mb_strtoupper($actividad->nombre_estudiante) }} </H5>
                  </td>
                  <td>
                    <H5>{{ $nota=mb_strtoupper($actividad->calificacion) }}</H5>
                  <td>
                     <H5>{{ mb_strtoupper($actividad->fecha_entrega) }}</H5>
                  </td>
                  <td>
                     <H5>{{ mb_strtoupper($actividad->comentarios) }}</H5>
                  </td>
                  <td>
                     <H5>{!! Html::link($actividad->url, 'DESCARGAR ', $attributes = ['class'=>'btn btn-success  fa fa-download']) !!}</H5>
                  </td>
                  <td>
                      <H5> {!!link_to_route('misrespuestasactividades.edit', $title = 'CALIFICAR',  $parameters = ['b'=>$actividad->id_respuesta_actividad, 'a'=>$curso], $attributes = ['class'=>'btn btn-default  fa fa-check-square'])!!}</H5>
                  </td>
                    {{ Form::hidden('post_id', $nota_total=($maximanota->nota_total)) }}
                  </td>
                  @if($nota<=$nota_total)
                   @else
                  <td class="btn-warning">

                    NOTA MAXIMA<li class="fa fa-exclamation-triangle">{{ $nota_total=mb_strtoupper($maximanota->nota_total) }}</li>

                  </td>
                  @endif
                        @if ($nota>$nota_total)
                        <td class="btn-danger"> <li class=" fa fa-remove">NOTA MALA</li> </td>
                        @else                    
                        @endif
                  </tr>
                @endforeach
                @endforeach
              </tbody>
            </table>
          </div>
      </div>
    </div>
  </div>
 </div>

      
<div class="container">
 <div class="col-sm-12">
  <center><a href="#demo" class="btn btn-danger" data-toggle="collapse"><div class="fa fa-plus"></div>  <div class="fa fa-exclamation-triangle"> </div>TAREAS NO ENTREGADAS</a></center>
  </div>
  <div id="demo" class="collapse">
    <div class="row">
      <div class="col-sm-9">
          <div class="table-responsive">
            <table class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th>
                    <H5><STRONG>ESTADO </STRONG></H5>
                  </th>
                  <th>
                   <H5><STRONG>ALUMNO </STRONG></H5>
                  </th>
                   <th>
                   <H5><STRONG>OPCIONES</STRONG></H5>
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($sinentrega as $key=>$actividad)
                  <tr>
                      <center>
                      <td class="btn-danger"> <H5><strong>  <div class="fa fa-exclamation-triangle"> </div>NO ENTREGADA </strong></H5></td>
                      </center>
                  <td>
                    <H5>
                      {{ $nota=mb_strtoupper($actividad->apellidos_estudiante) }}
                      {{ mb_strtoupper($actividad->nombre_estudiante) }}
                    </H5>
                  <td>
                      <H5>{!!link_to_route('misrespuestasactividades.edit', $title = 'NOTIFICAR',  $parameters = ['a'=>$actividad->id], $attributes = ['class'=>'btn btn-danger  fa fa-info-circle'])!!} {!!link_to_route('misrespuestasactividades.create', $title = 'AGREGAR NOTA',  $parameters = ['a'=>$actividad->id], $attributes = ['class'=>'btn btn-default  fa fa-check-square'])!!}</H5>
                    </td>
               </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
       </div>
       </div>
    <br/>


<div class="container">
<div class="col-sm-12">
   <center><a href="#demo3" class="btn btn-default" data-toggle="collapse"><div class="fa fa-plus"></div> <div class="fa fa-check-circle"></div>... TAREAS DE UNIDADES ANTERIORES ...</a></center>
   </div>
  <div id="demo3" class="collapse">
    <div class="row">
      <div class="col-sm-9">
          <div class="table-responsive">
            <table class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th>
                    <H5><STRONG>ESTADO </STRONG></H5>
                  </th>
                  <th>
                   <H5><STRONG>ALUMNO </STRONG></H5>
                  </th>
                  <th id="nota">
                   <H5><STRONG> CALIFICACION </STRONG></H5>
                  </th>
                  <th>
                   <H5><STRONG> FECHA ENTREGA </STRONG></H5>
                  </th>
                  <th>
                   <H5><STRONG> COMENTARIOS </STRONG></H5>
                  </th>
                   <th>
                    <H5><STRONG>ARCHIVO </STRONG></H5>
                  </th>
                   <th>
                    <H5><STRONG> OPCIONES </STRONG></H5>
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($ractividades as $key=>$actividad)

                @foreach ($notas as $key=>$maximanota)

                  <tr>

                    

                        
                      @if($actividad->estado_entrega_actividad=="CALIFICADO")
                      <td class="btn-success">  <li class="fa fa-check-circle"> CALIFICADO </li></td>
                      @else
                      <td class="btn-danger">  <li class="fa fa-info-circle"> NO CALIFICADA </li></td>
                      @endif

                    

                  <td>
                    <H5> {{ mb_strtoupper($actividad->apellidos_estudiante) }}
                    {{ mb_strtoupper($actividad->nombre_estudiante) }} </H5>

                  </td>

                  


                   


                  <td>

                    <H5>{{ $nota=mb_strtoupper($actividad->calificacion) }}</H5>


                  

                  







                  <td>

                     <H5>{{ mb_strtoupper($actividad->fecha_entrega) }}</H5>

                  </td>

                  <td>

                     <H5>{{ mb_strtoupper($actividad->comentarios) }}</H5>

                  </td>

                  <td>

                    
                     <H5>{!! Html::link($actividad->url, 'DESCARGAR ', $attributes = ['class'=>'btn btn-success  fa fa-download']) !!}</H5>
                  </td>
                  

                    <td>



                      <H5> {!!link_to_route('misrespuestasactividades.edit', $title = 'CALIFICAR',  $parameters = ['b'=>$actividad->id_respuesta_actividad, 'a'=>$curso], $attributes = ['class'=>'btn btn-default  fa fa-check-square'])!!}</H5>

                    </td>

                    {{ Form::hidden('post_id', $nota_total=($maximanota->nota_total)) }}
                    

                  </td>

                  @if($nota<=$nota_total)

                   @else
                  <td class="btn-warning">

                    NOTA MAXIMA<li class="fa fa-exclamation-triangle">{{ $nota_total=mb_strtoupper($maximanota->nota_total) }}</li>

                  </td>
                  @endif

                    

                        @if ($nota>$nota_total)


                        <td class="btn-danger"> <li class=" fa fa-remove">NOTA MALA</li> </td>

                        @else

                        

                        @endif



                   
                  </tr>

                @endforeach

                @endforeach


              </tbody>

            </table>

               

           

          </div>

    
      
      </div>
      @endif 

    </div>

  </div>

   </div>
</br>

@endsection


