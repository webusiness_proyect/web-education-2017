<div class="form-group">
  {!!Form::label('nombre', 'Nombre*', ['class'=>'col-sm-2 control-label'])!!}
  <div class="col-sm-10">
    {!!Form::text('nombre_carrera', null, ['class'=>'form-control', 'placeholder'=>'Nombre de la carrera...'])!!}
  </div>
</div>
<div class="form-group">
  {!!Form::label('nombre', 'Nombre corto*', ['class'=>'col-sm-2 control-label'])!!}
  <div class="col-sm-10">
    {!!Form::text('nombre_corto_carrera', null, ['class'=>'form-control', 'placeholder'=>'Nombre corto de la carrera...'])!!}
  </div>
</div>
<div class="form-group">
  {!!Form::label('descripcion', 'Descripción*', ['class'=>'col-sm-2 control-label'])!!}
  <div class="col-sm-10">
    {!!Form::textarea('descripcion_carrera', null, ['class'=>'form-control', 'placeholder'=>'Descripción de la carrera...'])!!}
  </div>
</div>
<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" name="guardar" class="btn btn-success"><span class="fa fa-save"></span> Registrar</button>
  </div>
</div>
