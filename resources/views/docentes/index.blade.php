@extends('principaldocente')



@section('titulo')

  <title>Mis Cursos Asignados</title>

@endsection



@section('cuerpo')


  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

      <di <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-pencil" aria-hidden="true"></div> - - Mis Cursos Asignados</h1>

        <h4 class="text-center"><div class="glyphicon glyphicon-cog"></div> {{mb_strtoupper($unidad->nombre_unidad) }}</h4>
        
      

        <div class="panel panel-default">
          <center>
            <h4>
            <a href="#demo2" class="btn btn-warning" data-toggle="collapse">
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" title="click aqui para mas informacion" aria-hidden="true"></i></a> CREA </strong> y administra tus  <strong>notas</strong> de todos tus cursos y alumnos asignados por tu institucion
            </h4>

          
           
          </center>
          <div id="demo2" class="collapse">

          <div class="alert alert-warning alert-dismissable">
          <center>

            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-info btn btn-default" aria-hidden="true"></i><STRONG> MANTENTE AL DIA DE LAS NOTAS DE TUS ESTUDIANTES, REVISA ESTE MODULO REGULARMENTE</a> </STRONG><br/>
           
            
          </CENTER>
    
        </div>

        </div>
        </div>
        

        <center>
           
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
       
      </center>

        @include('mensajes.msg')

        </div>

        
        <div class="col-sm-12 panel panel-default">

         



        <div class="table-responsive">

          <table class="table table-hover table-bordered">

            <thead>

              <tr>

              
                <th>

                 <h4><STRONG><CENTER> NOTAS Y PUNTOS DE MIS CURSOS </CENTER></h4></STRONG>

                </th>

                <th>

                 <h4><STRONG><CENTER> PENSUM </CENTER></h4></STRONG>

                </th>

                

                <th>

                  <h4><STRONG><CENTER>CARRERA</CENTER></h4></STRONG>

                </th>

                <th>

                  <h4><STRONG><CENTER>SECCION</CENTER></h4></STRONG>

                </th>

               

                

              </tr>

            </thead>

            <tbody>

              @foreach($cursos as $key => $curso)

                <tr>

                   <td>

                 


                    <!-- Single button -->

                    <center>


                    <a href="/docente/create?a={{$curso->id_asignacion_area}}&b={{ $curso->id_asignacion_docente }}" title="Click aqui para ver mis notas y puntos asignados para este curso" class="btn btn-success"><div class="fa fa-trophy" ></div> {{ mb_strtoupper($curso->nombre_area) }} </a>

                   

                    
                    </center>

            

             


                    {{-- {!!link_to_route('docente.create', $title = ' Zona', $parameters = ['a'=>$curso->id_asignacion_area,'b'=>$curso->id_asignacion_docente], $attributes = ['class'=>'btn btn-primary fa fa-edit'])!!}

                    --}}

                  </td>

                 

                  <td>

                      {{ mb_strtoupper($curso->nombre_grado) }}   
                      {{ mb_strtoupper($curso->nombre_nivel) }}    
                      {{ mb_strtoupper($curso->nombre_plan) }}
                      {{ mb_strtoupper($curso->nombre_jornada) }}

                  </td>
                  <td>
                  {{ mb_strtoupper($curso->nombre_carrera) }}
                  </td>

                  <td>

                    {{ mb_strtoupper($curso->nombre_seccion) }}

                  </td>

                 

                 

                </tr>

              @endforeach

            </tbody>

          </table>

        </div>

      </div>

      <div class="col-sm-12 text-center">

        {{ $cursos->setPath('docente')->links() }}

      </div>

    </div>

  </div>

@endsection

