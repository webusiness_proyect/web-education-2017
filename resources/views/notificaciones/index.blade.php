@extends('principal')





@section('titulo')


  <title>Notificaciones Enviadas</title>


@endsection





@section('cuerpo')


  <div id="page-wrapper">


    <div class="row">


      <div class="col-sm-12">


      <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-envelope-o" aria-hidden="true"></div>Mis Notificaciones</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> ENVIA </strong> notificaciones y mensajes a los usuarios del sistema.
            </h4>
           
          </center>
        </div>
        

        @include('mensajes.msg')

        </div>


      </div>


 
       <div class="col-sm-12 panel panel-default">

          <br/>
           <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}


        @permission('crear-notificacion')


        {!!link_to_route('notificaciones.create', $title = 'Nueva Notificacion', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square'])!!}

        


        <br>


        <br>


        @endpermission


        @if(count($notificaciones) == 0)


          <p class="text-info">


            No se ha creado ninguna notificacion aun.


          </p>
          </center>


        @else


          <div class="col-sm-12 panel panel-default table-responsive">


            <table class="table table-hover">


              <thead>


                <tr>


                 <th>

                ESTADO

                 </th>


                  <th>


                    TITULO


                  </th>


                  <th>


                    DESCRIPCION


                  </th>


                  <th>


                    FECHA


                  </th>
                  
                  


                  @permission('editar-notificacion')


                  <th>


                    MODIFICAR


                  </th>


                  @endpermission


                  @permission('crear-notificacion')


                  <th>


                    DETALLES


                  </th>

                

                  @endpermission

                  




                </tr>


              </thead>


              <tbody id="datosNotificacion">


                @foreach($notificaciones as $key => $p)


                  <tr>


                    
                  <td>
                  @if($p->estado_mensaje==1)
                      <i class="fa fa-check" aria-hidden="true"></i><i class="fa fa-envelope-o" aria-hidden="true"></i>
                  @else
                       <i class="fa fa-times" aria-hidden="true"></i><i class="fa fa-envelope-o" aria-hidden="true"></i>
                  @endif
                  </td>

                    <td>


                      {{ mb_strtoupper($p->titulo_notificacion) }}


                    </td>
                     <td>


                      {{ mb_strtoupper($p->informacion_notificacion) }}


                    </td>



                    <td>


                      {{ mb_strtoupper($p->fecha_create) }}


                    </td>


                   


                    @permission('editar-notificacion')


                    <td>


                      {!!link_to_route('notificaciones.edit', $title = 'Editar mensaje', $parameters = $p->id_notificacion, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}



                    </td>





                    <td>


                      {!!link_to_route('notificaciones.show', $title = 'Ver Detalle', $parameters = $p->id_notificacion, $attributes = ['class'=>'btn btn-success fa fa-eye'])!!}


                    </td>

                 

                    @endpermission






                  </tr>


                @endforeach


              </tbody>


            </table>

            <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">

              @include('mensajes.carga')

            <div class="text-center">

            {!! $notificaciones->links() !!}

          </div>


          </div>


        @endif


      </div>


    </div>


  </div>


@endsection


