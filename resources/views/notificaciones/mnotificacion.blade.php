@extends('principal')



@section('titulo')

  <title>Notificaciones</title>

@endsection



@section('cuerpo')

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center">NOTIFICACIONES ENVIADAS</h1>

        <h3 class="text-center">{{ ucwords($notificaciones[0]->titulo_notificacion.' '.$notificaciones[0]->informacion_notificacion.' '.$notificaciones[0]->fecha_create) }}</h3>

      </div>

      <div class="col-sm-12">

        <div class="table-responsive">

          <table class="table table-hover">

            <thead>

              <th>

                NO

              </th>

              <th>

                GRADO

              </th>

              <th>

                CARRERA

              </th>

              <th>

                NIVEL

              </th>
              <th>

                JORNADA

              </th>  

              <th>

                PLAN

              </th> 
              <th>

                TIPO DE USUARIO

              </th> 
            </thead>

            <tbody>

              @foreach($notificaciones as $key => $p)

                <tr>

                  <td>

                    {{ $key+1 }}

                  </td>

                  <td>

                    {{ mb_strtoupper($p->grado) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($p->carrera) }}

                  </td>
                  <td>

                    {{ mb_strtoupper($p->nivel) }}

                  </td>
                  <td>

                    {{ mb_strtoupper($p->jornada) }}

                  </td>
                  <td>

                    {{ mb_strtoupper($p->plan) }}

                  </td>

                  <td>

                    {{ mb_strtoupper($p->name) }}

                  </td>                                                                        

                </tr>

              @endforeach

            </tbody>

          </table>

        </div>

      </div>

    </div>

  </div>

@endsection

