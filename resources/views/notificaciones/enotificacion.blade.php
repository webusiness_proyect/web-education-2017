@extends('principal')



@section('titulo')

  <title>Editar Notificacion</title>

@endsection



@section('cuerpo')

<div id="page-wrapper">

  <div class="row">

    <div class="col-sm-12">

      <h1 class="page-header text-center"> {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}    Atras</h1>

      @include('mensajes.errores')

      <p>

        Nota: Todos los campos con (*) son obligatorios.

      </p>

    </div>



    <div class="col-sm-12">

      {!!Form::model($dato,  ['route'=>['notificaciones.update', $dato->id_notificacion], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'modificacionnotificacion'])!!}



        <div class="tab-content panel panel-default">

        <div id="amensaje" class="tab-pane fade in active">

          <h3>Editar Mensaje</h3>

          @include('notificaciones.form.mensajeedit')

        </div>

        

       

        <div id="menu4" class="tab-pane fade">

          <h3>Registrar Estudiante</h3>

          <button type="submit" name="registrar" class="btn btn-success btn-lg"><i class="fa fa-save"></i> Registrar</button>


        </div>
        <br/>
       

        


      <ul class="nav nav-pills">
           

        <li class="active">{!! link_to(URL::previous(), 'CANCELAR', ['class' => 'btn btn-info fa fa-times']) !!} <i class="fa"></i></li>

        <li class="active"><a data-toggle="tab" href="#amensaje"> Cambiar mensaje  <i class="fa fa-bookmark-o"></i></a></li>

    
        <li><a data-toggle="tab" href="#menu4">5. Registrar <i class="fa fa-save"></i></a></li>

        </ul>
        </center>
        </div>

        </div>



      {!!Form::close()!!}

    </div>
       @include('mensajes.carga')
  </div>

</div>

@endsection
