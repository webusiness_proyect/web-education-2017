@extends('principal')





@section('titulo')


  <title>Nueva Notificacion</title>


@endsection





@section('cuerpo')


  <div id="page-wrapper">


    <div class="row">


      <div class="col-sm-12">

      <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-plus" aria-hidden="true"></div><div class="fa fa-paper-plane-o" aria-hidden="true"></div> - Nueva notificacion</h1>

         <p>


          Nota: Todos los campos con (*) son obligatorios.


        </p>


        @include('mensajes.errores')

        </div>


      </div>


      
       


      </div>


      <div class="col-sm-12">


        <ul class="nav nav-tabs">


          <li class="active"><a data-toggle="tab" href="#bgrados">Paso 1 - selecciona el nivel y el grado</a></li>


          <li><a data-toggle="tab" href="#amensaje">Paso 2 - selecciona tipo de usuario y escribo mensaje </a></li>


          <li><a data-toggle="tab" href="#gnotificacion">Paso 3 - Enviar Mensaje</a></li>


        </ul>





            {!!Form::open(['route'=>'notificaciones.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'notificaciones'])!!}


              @include('notificaciones.form.campos')





            {!!Form::close()!!}


          @include('mensajes.carga')


      </div>


    </div>


  </div>


@endsection


