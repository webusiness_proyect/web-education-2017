<!DOCTYPE html>

<html>

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    @yield('titulo')



    <!-- Arhivos css para estilos-->

    @include('plantillas.estilos')

    <!-- /#fin del los archivos css -->



  </head>

  <body>

    <div class="wraper">

      <!-- Contenido del menu -->

      @include('plantillas.menu')

      <!--  Fin del contenido del menu -->



    <!-- Aquí se carga el contenido de la página -->

    @yield('cuerpo')

    <!-- Fin del contenido de la página -->



  </div>

  <!-- /#wrapper -->



  <!-- seccion para los archivos javascript -->

  @include('plantillas.footer')



  <!-- /#fin de los archivos javascript -->

  </body>

</html>

