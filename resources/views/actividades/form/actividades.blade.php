

      <div class="form-group">

        {!!Form::label('actividad', 'Tipo de actividad: *', ['class'=>'col-sm-2 control-label'])!!}

        <div class="col-sm-10">

         {!!Form::select('id_puesto', $puestos, null, ['class'=>'form-control', 'placeholder'=>'Seleccione puesto...'])!!}

          {!!Form::select('id_tipo_actividad', ['TAREA EN CASA'=>'TAREA EN CASA', 'TAREA EN CLASE'=>'TAREA EN CLASE', 'LABORATORIOS'=>'LABORATORIOS', 'RESUMEN'=>'RESUMEN', 'ANALISIS'=>'ANALISIS', 'DEBATES'=>'DEBATES', 'TRABAJOS PRACTICOS'=>'TRABAJOS PRACTICOS', 'INVESTIGACIONES'=>'INVESTIGACIONES', 'TRABAJOS ESCRITOS'=>'TRABAJOS ESCRITOS', 'DRAMATIZACION'=>'DRAMATIZACION', 'EXPOSICION'=>'EXPOSICION', 'OTROS'=>'OTROS',], null, ['class'=>'form-control', 'placeholder'=>'Tipo de Actividad'])!!}

        </div>

      </div>



       

      <div class="form-group">

        {!!Form::label('nombre actividad', 'Actividad*', ['class'=>'col-sm-2 control-label'])!!}

        <div class="col-sm-10">

            {!!Form::text('nombre_actividad', null, ['class'=>'form-control', 'placeholder'=>'Nombre de la actividad...'])!!}

        </div>

      </div>

        

      <div class="form-group">

        {!!Form::label('descripcion', 'Descripcion de la actividad', ['class'=>'col-sm-2 control-label'])!!}

        <div class="col-sm-10">

            {!!Form::text('descripcion_actividad', null, ['class'=>'form-control', 'placeholder'=>'Descripcion de la actividad...'])!!}

        </div>

      </div>

       

      <div class="form-group">

        {!!Form::label('fecha', 'Fecha de entrega', ['class'=>'col-sm-2 control-label'])!!}

        <div class="col-sm-10">

            <div class="input-group calendario">

                {!!Form::text('fecha_entrega', null, ['class'=>'form-control', 'placeholder'=>'Fecha de entrega tarea...'])!!}

                <span class="input-group-addon">

                  <i class="fa fa-calendar"></i>

                </span>

            </div>

        </div>

      </div>

     

      <div class="form-group">

        {!!Form::label('nota total', 'Nota total de tarea*', ['class'=>'col-sm-2 control-label'])!!}

        <div class="col-sm-10">

          {!!Form::number('nota_total', null, ['class'=>'form-control', 'placeholder'=>'Valor de la tarea'])!!}

        </div>

      </div>

       

      <div class="form-group">

        <div class="col-sm-offset-2 col-sm-10">

          <button type="submit" name="guardar" class="btn btn-success"><span class="fa fa-save"></span> Registrar</button>

        </div>

      </div>



