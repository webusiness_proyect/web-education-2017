@extends('principal')





@section('titulo')


  <title>Nuevo Rol</title>


@endsection





@section('cuerpo')


  <div id="page-wrapper">


    <div class="row">


      <div class="col-sm-12">

      <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-plus" aria-hidden="true"></div> <div class="fa fa-sitemap" aria-hidden="true"></div>Nuevo Rol</h1>

         <p>


          Nota: Todos los campos con (*) son olbigatorios.


        </p>


        @include('mensajes.errores')
        </div>



       

       


      </div>


       <div class="col-sm-12 panel panel-default">

          <br/>
           <center>
            {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}
          </center>
          <br/>

        {!!Form::open(['route'=>'roles.store', 'method'=>'post', 'class'=>'form-horizontal', 'id'=>'nrol'])!!}


          @include('roles.form.campos')


        {!!Form::close()!!}


      </div>


    </div>


  </div>


@endsection


