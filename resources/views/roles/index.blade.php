@extends('principal')





@section('titulo')


  <title> Roles</title>


@endsection





@section('cuerpo')


  <div id="page-wrapper">





      <div class="row">


          <div class="col-lg-12">

          <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-sitemap" aria-hidden="true"></div>Roles disponibles</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra roles, en los cuales puedes configurar o personalizar el acceso a los modulos de los usuarios
            </h4>

          
           
          </center>
        </div>
        <div class="alert alert-warning alert-dismissable">
          <center>

            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-info btn btn-default" aria-hidden="true"></i><STRONG>TEN CUIDADO AL PERSONALIZAR ESTA OPCION YA QUE PUEDES DAR ACCESO A USUARIOS NO AUTORIZADOS 
            </STRONG>
           

            
          </CENTER>
    
        </div>

        @include('mensajes.msg')

        </div>
        <div class="col-sm-12 panel panel-default">

          <br/>
           <center>
        {!! link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']) !!}


            @permission('crear-rol')


            {!!link_to_route('roles.create', $title = "Nuevo Rol", $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus'])!!}


            <br>


            <br>


            @endpermission


            @if(count($roles) == 0)


              <p class="text-info">


                No se han registrado roles aun.


              </p>
              
          </center>


            @else


                <div class="table-responsive">


                  <table class="table table-hover table-bordered">


                    <thead>


                      <tr>


                        <th>


                          NO


                        </th>


                        <th>


                          NOMBRE ROL


                        </th>


                        @permission('editar-rol')


                        <th>


                          ACTUALIZAR


                        </th>


                        @endpermission


                        @permission('estado-rol')


                        <th>


                          ESTADO


                        </th>


                        @endpermission


                      </tr>


                    </thead>


                    <tbody id="datosRoles">


                      @foreach($roles as $key => $rol)


                        <tr>


                          <td>


                            {{ $key+1 }}


                          </td>


                          <td>


                            {{ mb_strtoupper($rol->name) }}


                          </td>


                          @permission('editar-rol')


                          <td>


                            {!!link_to_route('roles.edit', $title = 'Editar', $parameters = $rol->id, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o'])!!}


                          </td>


                          @endpermission


                          @permission('estado-rol')


                          <td>


                            @if($rol->state_rol == TRUE)


                              <input type="checkbox" name="estado" checked value="{{ $rol->id }}" class="toggleEstado">


                            @else


                              <input type="checkbox" name="estado" value="{{ $rol->id }}" class="toggleEstado">


                            @endif


                          </td>


                          @endpermission


                        </tr>


                      @endforeach


                    </tbody>


                  </table>


                  {!!Form::hidden('_token', csrf_token(), ['id'=>'token'])!!}


                </div>


            @endif





          </div>


          <!-- /.col-lg-12 -->


      </div>





  </div>


@endsection


