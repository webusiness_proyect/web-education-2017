@extends('principal')

@section('titulo')
  <title>Editar Rol</title>
@endsection

@section('cuerpo')
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header">Editar Rol</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        @include('mensajes.errores')
      </div>
      <div class="col-sm-12">
      @permission('crear-rol')
        <button type="button" name="permisos" class="btn btn-primary" data-toggle="modal" data-target="#formModal"><span class="fa fa-plus-circle"></span> Agregar Permiso</button>
        @endpermission
        <br>
        <br>
        <h3>Rol: <small>{{ mb_strtoupper($rol->name) }}</small></h3>
      {{--  {!!Form::model($rol, ['route'=>['roles.update', $rol->id_rol], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'nrol'])!!}
        <div class="form-group">
          {!!Form::label('nombre', 'Nombre*', ['class'=>'col-sm-2 control-label'])!!}
          <div class="col-sm-10">
            {!!Form::text('nombre_rol', null, ['class'=>'form-control', 'placeholder'=>'Nombre del nuevo rol...'])!!}
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="button" name="guardar" class="btn btn-success"><span class="fa fa-save"></span> Registrar</button>
          </div>
        </div>
        {!!Form::close()!!} --}}
        {!!Form::hidden('rol', $rol->id, ['id'=>'idRol'])!!}
        {!!Form::hidden('_token', csrf_token(), ['id'=>'token'])!!}
        <br>
        <br>
        <h2>Lista de Permisos Asignados</h2>
        <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>
                NO
              </th>
              <th>
                NOMBRE PERMISO
              </th>
              <th>
                ESTADO PERMISO
              </th>
            </tr>
          </thead>
          <tbody id="datosPermisosAsignados">
            @foreach($permisos as $key => $permiso)
              <tr>
                <td>
                  {{ $key+1 }}
                </td>
                <td>
                  {{ mb_strtoupper($permiso->name) }}
                </td>
                <td>
                @permission('crear-rol')
                  @if ($permiso->state_permission_role == true)
                    <input type="checkbox" name="estado" checked value="{{ $permiso->id_permission_role }}" class="toggleEstado">
                  @else
                    <input type="checkbox" name="estado" value="{{ $permiso->id_permission_role }}" class="toggleEstado">
                  @endif
                  @endpermission
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>

<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        <div class="form-horizontal">

        <div class="form-group">
          {!!Form::label('opciones', 'Opciones*', ['class'=>'col-sm-2 control-label'])!!}
          <div class="col-sm-10">
            {!!Form::select('opciones', $opciones, null, ['placeholder'=>'Seleccione una opción...', 'class'=>'form-control', 'id'=>'opcionesSistema'])!!}
          </div>
        </div>

        <div class="form-group">
          {!!Form::label('permisos', 'Lista Permisos*', ['class'=>'col-sm-2 control-label'])!!}
          <div class="col-sm-10">
            <div class="table-responsive">

            <table class="table table-hover">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    NOMBRE PERMISO
                  </th>
                  <th>
                    OPCION
                  </th>
                </tr>
              </thead>
              <tbody id="subOpciones">

              </tbody>
            </table>
          </div>
        </div>
        </div>

        <div class="form-group">
          {!!Form::label('permiso', 'Permisos Rol*', ['class'=>'col-sm-2 control-label'])!!}
          <div class="col-sm-10">
            <div id="permisosRoles">

            </div>
          </div>
        </div>

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        @permission('crear-rol')
        <button type="button" class="btn btn-success" id="registrarPermisos"><span class="fa fa-save"></span> Registrar</button>
        @endpermission
      </div>
    </div>
  </div>
</div>

      </div>
    </div>
  </div>
@endsection
