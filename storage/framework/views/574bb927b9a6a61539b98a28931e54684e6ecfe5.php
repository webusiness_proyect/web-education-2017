<?php $__env->startSection('titulo'); ?>


  <title>Notificaciones Enviadas</title>


<?php $__env->stopSection(); ?>





<?php $__env->startSection('cuerpo'); ?>


  <div id="page-wrapper">


    <div class="row">


      <div class="col-sm-12">


      <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-envelope-o" aria-hidden="true"></div>Mis Notificaciones</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> ENVIA </strong> notificaciones y mensajes a los usuarios del sistema.
            </h4>
           
          </center>
        </div>
        

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>


      </div>


 
       <div class="col-sm-12 panel panel-default">

          <br/>
           <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>



        <?php if (\Entrust::can('crear-notificacion')) : ?>


        <?php echo link_to_route('notificaciones.create', $title = 'Nueva Notificacion', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square']); ?>


        


        <br>


        <br>


        <?php endif; // Entrust::can ?>


        <?php if(count($notificaciones) == 0): ?>


          <p class="text-info">


            No se ha creado ninguna notificacion aun.


          </p>
          </center>


        <?php else: ?>


          <div class="col-sm-12 panel panel-default table-responsive">


            <table class="table table-hover">


              <thead>


                <tr>


                 <th>

                ESTADO

                 </th>


                  <th>


                    TITULO


                  </th>


                  <th>


                    DESCRIPCION


                  </th>


                  <th>


                    FECHA


                  </th>
                  
                  


                  <?php if (\Entrust::can('editar-notificacion')) : ?>


                  <th>


                    MODIFICAR


                  </th>


                  <?php endif; // Entrust::can ?>


                  <?php if (\Entrust::can('crear-notificacion')) : ?>


                  <th>


                    DETALLES


                  </th>

                  <th>


                    RESPUESTAS


                  </th>


                  <?php endif; // Entrust::can ?>

                  




                </tr>


              </thead>


              <tbody id="datosNotificacion">


                <?php foreach($notificaciones as $key => $p): ?>


                  <tr>


                    
                  <td>
                  <?php if($p->estado_mensaje==1): ?>
                      <i class="fa fa-check" aria-hidden="true"></i><i class="fa fa-envelope-o" aria-hidden="true"></i>
                  <?php else: ?>
                       <i class="fa fa-times" aria-hidden="true"></i><i class="fa fa-envelope-o" aria-hidden="true"></i>
                  <?php endif; ?>
                  </td>

                    <td>


                      <?php echo e(mb_strtoupper($p->titulo_notificacion)); ?>



                    </td>
                     <td>


                      <?php echo e(mb_strtoupper($p->informacion_notificacion)); ?>



                    </td>



                    <td>


                      <?php echo e(mb_strtoupper($p->fecha_create)); ?>



                    </td>


                   


                    <?php if (\Entrust::can('editar-notificacion')) : ?>


                    <td>


                      <?php echo link_to_route('notificaciones.edit', $title = 'Editar', $parameters = $p->id_notificacion, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>



                    </td>



                    <td>


                      <?php echo link_to_route('notificaciones.show', $title = 'Ver Detalle', $parameters = $p->id_notificacion, $attributes = ['class'=>'btn btn-success fa fa-eye']); ?>



                    </td>

                    <td>


                      <?php echo link_to_route('notificaciones.show', $title = 'Ver Respuestas', $parameters = $p->id_notificacion, $attributes = ['class'=>'btn btn-default fa fa-eye']); ?>



                    </td>


                    <?php endif; // Entrust::can ?>






                  </tr>


                <?php endforeach; ?>


              </tbody>


            </table>

            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">

              <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <div class="text-center">

            <?php echo $notificaciones->links(); ?>


          </div>


          </div>


        <?php endif; ?>


      </div>


    </div>


  </div>


<?php $__env->stopSection(); ?>



<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>