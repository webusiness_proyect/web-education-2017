<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo $__env->yieldContent('titulo'); ?>

    <!-- Arhivos css para estilos-->
    <?php echo $__env->make('plantillas.estilos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- /#fin del los archivos css -->

  </head>
  <body>
    <div class="wraper">
      <!-- Contenido del menu -->
      <?php echo $__env->make('plantillas.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <!--  Fin del contenido del menu -->

    <!-- Aquí se carga el contenido de la página -->
    <?php echo $__env->yieldContent('cuerpo'); ?>
    <!-- Fin del contenido de la página -->

  </div>
  <!-- /#wrapper -->

  <!-- seccion para los archivos javascript -->
  <?php echo $__env->make('plantillas.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <!-- /#fin de los archivos javascript -->
  </body>
</html>
