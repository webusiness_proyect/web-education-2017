<?php $__env->startSection('titulo'); ?>
  <title>Editar Nivel</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="col-sm-12">
      <h1 class="page-header text-center">Editar Nivel</h1>
      <p>
        Nota: Todos los campos con (*) son obligatorios.
      </p>
      <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
    <div class="col-sm-12">
      <?php echo Form::model($nivel, ['route'=>['niveles.update', $nivel->id_nivel], 'method'=>'PUT', 'class'=>'form-horizontal']); ?>
        <?php echo $__env->make('niveles.form.campos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo Form::close(); ?>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>