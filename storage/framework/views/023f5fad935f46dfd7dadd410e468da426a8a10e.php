<?php $__env->startSection('titulo'); ?>
  <title>Mi perfil</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center"> <div class="fa fa-user" aria-hidden="true"></div>Mi Perfil</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CONFIGURA  </strong> tu contraseña y tu imagen para mostrar
            </h4>
          </center>
        </div>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <center>
          USUARIO: <?php echo e(ucwords(Auth::user()->name)); ?> 
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

       
        <br/>
       
      </center>
     
        <br/>
      </div>
      <div class="col-sm-12  panel panel-default">
       
          <div class="table-responsive">
            <table class="table table-hover">

                <tr>                
                  <th>
                    IMAGEN
                  </th>
                  
                    <?php if((Auth::user()->url)==null): ?>
                    <td>
                      <div class="fa fa-camera-retro fa-4x">
                      </div>
                       <?php echo link_to_route('miperfil.edit', $title = 'Cambiar imagen', $parameters = Auth::user()->id, $attributes = ['class'=>'btn btn-warning fa fa-edit']); ?>

                    
                    </td>
                      <?php else: ?>
                    <td>
                    <center>
                    <img src="<?php echo e(ucwords(Auth::user()->url)); ?>" width="200" height="250">
                     <?php echo link_to_route('miperfil.edit', $title = ' Cambiar imagen', $parameters = Auth::user()->id, $attributes = ['class'=>'btn btn-warning fa fa-edit']); ?>

                    
                    </center>
                    </td>
                    <?php endif; ?>               
                </tr>    
                <tr>                
                  <th>
                    USUARO
                  </th>
                  <th>
                    
                     <?php echo e(ucwords(Auth::user()->name)); ?> 
                  </th>               
                </tr>
                <tr>                
                  <th>
                    CORREO DE ACCESO
                  </th>
                  <th>
                     <?php echo e(ucwords(Auth::user()->email)); ?> 
                  </th>               
                </tr>

                <tr>                
                  <th>
                    CONTRASEÑA 
                  </th>
                  <th>
                    ************ 
                  </th>               
                </tr>


                
                

            </table>
          </div>
        
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>