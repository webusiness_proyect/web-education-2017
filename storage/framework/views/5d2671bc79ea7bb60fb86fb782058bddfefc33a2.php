<?php $__env->startSection('titulo'); ?>

  <title>Editar Foro</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center">Editar Foro</h1>

        <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php foreach($notas as $key=>$maximanota): ?>

        


        <p>

          Nota: Todos los campos con (*) son obligatorios.
          <?php echo e($nota_total=mb_strtoupper($maximanota->nota_total)); ?>


        </p>
        <?php endforeach; ?>

      </div>

      <div class="col-sm-12">

        <?php echo Form::model($ractividad, ['route'=>['misrespuestasactividades.update',$ractividad->id_respuesta_actividad], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'calificarrespuestas']); ?>


          <input type="hidden" name="curso" value="0">

          <?php echo $__env->make('misrespuestasactividades.form.campos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <?php echo Form::close(); ?>


      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>