<?php $__env->startSection('titulo'); ?>

  <title>Editar Notificacion</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

<div id="page-wrapper">

  <div class="row">

    <div class="col-sm-12">

      <h1 class="page-header text-center"> <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>    Atras</h1>

      <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <p>

        Nota: Todos los campos con (*) son obligatorios.

      </p>

    </div>



    <div class="col-sm-12">

      <?php echo Form::model($dato,  ['route'=>['notificaciones.update', $dato->id_notificacion], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'modificacionnotificacion']); ?>




        <div class="tab-content panel panel-default">

        <div id="amensaje" class="tab-pane fade in active">

          <h3>Editar Mensaje</h3>

          <?php echo $__env->make('notificaciones.form.mensajeedit', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>

        

       

        <div id="menu4" class="tab-pane fade">

          <h3>Registrar Estudiante</h3>

          <button type="submit" name="registrar" class="btn btn-success btn-lg"><i class="fa fa-save"></i> Registrar</button>


        </div>
        <br/>
       

        


      <ul class="nav nav-pills">
           

        <li class="active"><?php echo link_to(URL::previous(), 'CANCELAR', ['class' => 'btn btn-info fa fa-times']); ?> <i class="fa"></i></li>

        <li class="active"><a data-toggle="tab" href="#amensaje"> Cambiar mensaje  <i class="fa fa-bookmark-o"></i></a></li>

    
        <li><a data-toggle="tab" href="#menu4">5. Registrar <i class="fa fa-save"></i></a></li>

        </ul>
        </center>
        </div>

        </div>



      <?php echo Form::close(); ?>


    </div>
       <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>

</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>