<?php $__env->startSection('titulo'); ?>



  <title>Responde Actividad</title>



<?php $__env->stopSection(); ?>







<?php $__env->startSection('cuerpo'); ?>



<div id="page-wrapper">



  <div class="row">



    <div class="col-sm-12">



      <h1 class="page-header text-center"><div class="fa fa-send"></div><div class="glyphicon glyphicon-share"></div>  Entrega tu tarea</h1>



      <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>



  

      <div class="col-sm-12">

        <div class="alert alert-warning">


         
          <center>
        

            <div class="fa fa-eye"> 

                 Nota: Todos los campos con (*) son obligatorios.

            </div>
            <br/>

            <div class="fa fa-exclamation-triangle"> 

                Si subes tu tarea esta no podra modificarse nuevamente, asegurate de enviar el documento deseado
                 
            </div>
              <br/>
              <div class="fa fa-exclamation-triangle"> </div><strong>¡LIMITE DE TAMAÑO!</strong> No puedes enviar archivos mayores a 1 Mb

             <br/>
              <div class="fa fa-exclamation-triangle"> </div><strong>¡SUBE LA TAREA!</strong> Puedes enviar imagenes .jpg, .png, archivos .pdf, .doc, .docx, .xsls, ppsx
                 





          

       

      </div>



    </div>



    <div class="col-sm-12 panel panel-default">
    <br/>



      <?php echo Form::open(['route'=>'respuestaactividades.store', 'method'=>'POST', 'files'=>true, 'class'=>'form-horizontal', 'id'=>'respuestaactividades']); ?>




        <input type="hidden" name="curso" value="<?php echo e($curso); ?>">



        <?php echo $__env->make('respuestaactividades.form.campos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>



      <?php echo Form::close(); ?>




    </div>



  </div>



</div>



<?php $__env->stopSection(); ?>




<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>