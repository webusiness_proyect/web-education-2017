<?php $__env->startSection('titulo'); ?>

  <title>Mis alumnos</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

      <di <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-user" aria-hidden="true"></div> Mis Estudiantes</h1>
         </h4>
          <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i>¿CUANTOS CURSOS PERDIO MI HIJO? </strong>  mantente informado de los avances de tus hijos en sus cursos
            </h4>
           
          </center>
        </div>
        <div class="alert alert-success alert-dismissable">
          <center>

            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-money btn btn-default" aria-hidden="true"></i><STRONG>REALIZA PAGOS DE COLEGIATURA DESDE LA COMODIDAD DE TU CASA</STRONG> Solo tienes que tener una cuenta en <a href="https://www.paypal.com/gt/webapps/mpp/account-selection"> PAY PAL</a> o una tarjeta de credito/debito <br/>
           
           

            
          </CENTER>
    
        </div>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>

       
        <div class="col-sm-12 panel panel-default">

          <br/>



        <div class="table-responsive">

          <table class="table table-hover table-bordered">

            <thead>

              <tr>

               <th>

                <h4><strong> OPCIONES </strong> </h4>

                </th>

                <th>

                 <h4><strong> DATOS DEL ALUMNO</strong></h4>

                </th>

               

               

                <th>

                  <h4><strong>PENSUM</strong></h4>

                </th>

               
                <th>

                  <h4><strong>CARRERA</strong></h4>

                </th>
                <th>

                  <h4><strong>SECCION</strong></h4>

                </th>                
                
                 <th>

                 <h4><strong> CUOTA DE INSCRIPCION</strong></h4>

                </th>

                <th>

                  <h4><strong>CUOTA MENSUAL</strong></h4>

                </th>

                 <th>

                  <h4><strong>OPCIONES DE PAGO</strong></h4>

                </th>

              </tr>

            </thead>

            <tbody>

              <?php foreach($alumnos as $key => $alumno): ?>

                <tr>

                  
                   <td>

                      <center>
                         <H4><a href="/misalumnos/show?a=<?php echo e($alumno->id); ?>"  class="btn btn-warning"><div class="fa fa-eye fa-2x"></div>VER NOTAS</a><br/><H4>
                       
                      </center>

                  </td>

                  <td>

                    <H4><?php echo e(mb_strtoupper($alumno->nombre_estudiante)); ?>   <?php echo e(mb_strtoupper($alumno->apellidos_estudiante)); ?></H4>

                  </td>

                 

                  <td>

                    <H4><?php echo e(mb_strtoupper($alumno->nombre_grado)); ?>, 

                 

                    <?php echo e(mb_strtoupper($alumno->nombre_nivel)); ?>,

                

                    <?php echo e(mb_strtoupper($alumno->nombre_jornada)); ?>,

                

                    <?php echo e(mb_strtoupper($alumno->nombre_plan)); ?> </H4>

                  </td>

                  <td>

                    <H4><?php echo e(mb_strtoupper($alumno->nombre_carrera)); ?></H4>

                  </td>

                  <td>

                    <H4><?php echo e(mb_strtoupper($alumno->nombre_seccion)); ?></H4>

                  </td>

                   <td>

                    <H4><?php echo e(mb_strtoupper($alumno->cuota_inscripcion)); ?></H4>

                  </td>

                  <td>

                    <H4><?php echo e(mb_strtoupper($alumno->cuota_mensualidad)); ?></H4>

                  </td>

                  <td>
                     <H4><a href="/misalumnos/pagos?a=<?php echo e($alumno->id); ?>"  class="btn btn-success"><div class="fa fa-money fa-2x" enabled></div>PAGOS</a><br/><H4>
                  </td>     
                 

                </tr>

              <?php endforeach; ?>

            </tbody>

          </table>

        </div>

      </div>

      <div class="col-sm-12 text-center">

     
      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>