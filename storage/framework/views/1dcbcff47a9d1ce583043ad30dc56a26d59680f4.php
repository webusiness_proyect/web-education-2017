<?php $__env->startSection('titulo'); ?>
  <title>Empleados</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
       <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-users" aria-hidden="true"></div><div class="fa fa-gear" aria-hidden="true"></div>Empleados de mi Institución</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra los empleados de tu institucion ejemplo:<strong>Juan Perez, *docente </strong>
            </h4>

          
           
          </center>
        </div>
        <div class="alert alert-warning alert-dismissable">
          <center>

            <button type="button" class="close" data-dismiss="alert">&times;</button>
           <i class="fa fa-info btn btn-default" aria-hidden="true"></i> LOS ESTUDIANTES SERAN CREADOS EN EL MODULO <a href="<?php echo e(url('/inscripcionestudiantes')); ?>">INSCRIPCION ESTUDIANTES</a><br/>
           <i class="fa fa-info btn btn-default" aria-hidden="true"></i> LOS EMPLEADOS NO PODRAN ACCEDER A LA PLATAFORMA A MENOS DE QUE CREES UN  <a href="<?php echo e(url('/usuarios')); ?>">USUARIO</a> DESPUES DE REGISTRAR UNA PERSONA

            
          </CENTER>
    
        </div>
          <h6>
         <CENTER>
          * DEBERAS DE CREAR UN  <a href="<?php echo e(url('/puestos')); ?>">PUESTO</a> PREVIAMENTE <BR/>
          

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>
      </div>
      <div class="col-sm-12 panel panel-default">

      <br/>
        <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

        <?php if (\Entrust::can('crear-empleado')) : ?>
        <?php echo link_to_route('personas.create', $title = 'Nuevo Empleado', $parameters = null, $attributes =['class'=>'btn btn-primary fa fa-plus']); ?>

        <br>
        <br>
        <?php endif; // Entrust::can ?>
        <?php echo $__env->make('personas.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php if(count($personas) == 0): ?>
          <p class="text-info">
            No se han registrado empleados aun.
          </p>
          </center>
        <?php else: ?>
          <div class="table-responsive">
            <table class="table table-hover table- table-bordered">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    APELLIDOS
                  </th>
                  <th>
                    NOMBRES
                  </th>
                  <th>
                    TELEFONO
                  </th>
                  <th>
                    DPI
                  </th>
                  <th>
                    CORREO
                  </th>
                  <th>
                    PUESTO
                  </th>
                  <?php if (\Entrust::can('editar-empleado')) : ?>
                  <th>
                    ACTUALIZAR
                  </th>
                  <?php endif; // Entrust::can ?>
                  <?php if (\Entrust::can('estado-empleado')) : ?>
                  <th>
                    ESTADO
                  </th>
                  <?php endif; // Entrust::can ?>
                  <?php if (\Entrust::can('ver-empleado')) : ?>
                  <th>
                    MAS DATOS
                  </th>
                  <?php endif; // Entrust::can ?>
                </tr>
              </thead>
              <tbody id="datosPersonas">
                <?php foreach($personas as $key => $persona): ?>
                  <tr>
                    <td>
                      <?php echo e($persona->id_persona); ?>

                    </td>
                    <td>
                      <?php echo e($persona->apellidos_persona); ?>

                    </td>
                    <td>
                      <?php echo e($persona->nombres_persona); ?>

                    </td>
                    <td>
                      <?php echo e($persona->telefono_persona); ?>

                    </td>
                    <td>
                      <?php echo e($persona->cui_persona); ?>

                    </td>
                     <td>
                      <?php echo e($persona->correo_persona); ?>

                    </td>
                     <td>
                      <?php echo e($persona->nombre_puesto); ?>

                    </td>
                    <?php if (\Entrust::can('editar-empleado')) : ?>
                    <td>
                      <?php echo link_to_route('personas.edit', $title = 'Editar', $parameters = $persona->id_persona, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>

                    </td>
                    <?php endif; // Entrust::can ?>
                    <?php if (\Entrust::can('estado-empleado')) : ?>
                    <td>
                      <?php if($persona->estado_persona == true): ?>
                        <input type="checkbox" name="estado" checked value="<?php echo e($persona->id_persona); ?>" class="toggleEstado">
                      <?php else: ?>
                        <input type="checkbox" name="estado" value="<?php echo e($persona->id_persona); ?>" class="toggleEstado">
                      <?php endif; ?>
                    </td>
                    <?php endif; // Entrust::can ?>
                    <?php if (\Entrust::can('ver-empleado')) : ?>
                    <td>
                      <?php echo link_to_route('personas.show', $title = 'Ver Más', $parameters = $persona->id_persona, $attributes = ['class'=>'btn btn-info']); ?>

                    </td>
                    <?php endif; // Entrust::can ?>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            <?php echo Form::hidden('_token', csrf_token(), ['id'=>'token']); ?>

            <div class="text-center">
            <?php echo $personas->links(); ?>

          </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>