<?php $__env->startSection('titulo'); ?>

  <title>Sistema Educativo - WB</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>



  <div id="page-wrapper">



      <div class="row">

          <div class="col-lg-12">

          

              <br/>
               <div class="panel panel-primary">
                <div class="panel-heading">
                   <H3> <center>BIENVENIDOS <?php echo e(ucwords(Auth::user()->name)); ?> </center></H3>
                </div>
                <div class="panel-footer">  <H4> <center>Panel de Control de Sistema Educativo WB </center></H4></div>
              </div>
              



          </div>

          <!-- /.col-lg-12 -->

      </div>

        <div class="row">

          <div class="col-lg-8">

              <h3 class="text-center">Bienvenidos al software que hara mas facil para usted y sus maestros el asignar notas y controlar todo lo que pasa en su institucion, Wb Education, lo mejor en educacion </h3>
              <br/>



                <ul class="nav nav-tabs nav-justified">
                  <li class="active"><a href="#"><i class="fa fa-pie-chart" aria-hidden="true"></i>Estadisticas</a></li>
                  <li><a href="<?php echo e(url('/usuarios')); ?>"><i class="fa fa-users" aria-hidden="true"></i>Usuarios</a></li>
                  <li><a href="<?php echo e(url('/carreras')); ?>"><i class="fa fa-graduation-cap" aria-hidden="true"></i>Carreras</a></li>
                  <li><a href="<?php echo e(url('/grados')); ?>"><i class="fa fa-bookmark-o" aria-hidden="true"></i>Grados</a></li>
                  
                  <li><a href="<?php echo e(url('/asignaciondocente')); ?>"><i class="fa fa-book" aria-hidden="true"></i>Docentes</a></li>
                  <li> <a href="<?php echo e(url('/inscripcionestudiantes')); ?>"><i class="fa fa-child" aria-hidden="true"></i><i class="fa fa-plus" aria-hidden="true"></i>Inscripciones</a></li>
                  <li> <a href="<?php echo e(url('/horarios')); ?>"><i class="fa fa-calendar" aria-hidden="true"></i>Calendarios</a></li>


                </ul>

               <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                  <script type="text/javascript">
                   google.charts.load('current', {'packages':['corechart']});
                   google.charts.setOnLoadCallback(drawChart);
                   function drawChart() {

                      var data = google.visualization.arrayToDataTable([
                       ['Task', 'Hours per Day'],
                       ['Primero',     11],
                       ['Segundo',      2],
                       ['Tercero',  2],
                       ['Cuarto', 2],
                      ['Quinto',    7],
                       ['Sexto',    10],
                       ['Primero Basico',    7],
                       ['Segundo Basico',    9],
                       ['Tercero Basico',    10],
                        ['Carrera 1',    7],
                        ['Carrera 2',    12],
                       ['carrera 3',    7]
                      ]);

                     var options = {
                        title: '    ESTADO ACTUAL DE INSCRIPCIONES'
                      };

                      var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                      chart.draw(data, options);
                    }
                  </script>


              <div id="piechart"></div>

             <br/>
              <div class="panel panel-warning">
                <div class="panel-heading">
                   <H3> <center> <div class="fa fa-exclamation-circle" aria-hidden="true"></div> AYUDA Y SOPORTE </center></H3>
                </div>
                <div class="panel-footer">  <H4> <center>correo: education@webusiness.co Tel. +50223278063

                Guatemala, Zona 9, 1era. avenida 8-00.</center></H4></div>
              </div>

                 



          </div>

          <div class="col-lg-4">
              <center>
              <img src="<?php echo e(url('/img/imagenes/3er vista.png')); ?>">
              </center>
          </div>

          

          <!-- /.col-lg-12 -->

      </div>



  </div>

  <!-- /#page-wrapper -->



<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>