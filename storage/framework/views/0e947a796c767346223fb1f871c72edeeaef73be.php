<!-- jQuery -->
<script src="<?php echo e(url('/')); ?>/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo e(url('/')); ?>/js/bootstrap.min.js"></script>

<!-- Bootstrat validation JS -->
<script src="<?php echo e(url('/')); ?>/js/formValidation.min.js"></script>

<!-- Bootstrat validation JS -->
<script src="<?php echo e(url('/')); ?>/js/bootstrap.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo e(url('/')); ?>/js/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<!--
<script src="<?php echo e(url('/')); ?>/js/raphael-min.js"></script>
<script src="<?php echo e(url('/')); ?>/js/morris.min.js"></script>
<script src="<?php echo e(url('/')); ?>/js/morris-data.js"></script>
-->

<!-- Custom Theme JavaScript -->
<script src="<?php echo e(url('/')); ?>/js/sb-admin-2.js"></script>

<!-- Reglas de validaciones para los formularios JS -->
<script src="<?php echo e(url('/')); ?>/js/validaciones-dist.js"></script>

<!-- script para el datepicker bootstrap -->
<script type="text/javascript" src="<?php echo e(url('/')); ?>/js/moment.min.js"></script>
<script type="text/javascript" src="<?php echo e(url('/')); ?>/js/bootstrap-datetimepicker.min.js"></script>

<!-- idioma paramostrar en el datepicker bootstrap -->
<script src="<?php echo e(url('/')); ?>/js/es.js"></script>

<!-- script para las funciones de autocompletado -->
<script src="<?php echo e(url('/')); ?>/js/jquery.autocomplete.min.js"></script>

<!-- script para bootstrap toggle -->
<script src="<?php echo e(url('/')); ?>/js/bootstrap-toggle.min.js"></script>
<!--<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>-->

<!-- script para jquery alert-->
<script src="<?php echo e(url('/')); ?>/js/jquery-confirm.min.js"></script>

<!-- script para las funciones que se necesitan agregar al sitio -->
<script src="<?php echo e(url('/')); ?>/js/script-dist.js"></script>

<!-- Scripts para el editor html5 -->
<script src="<?php echo e(url('/')); ?>/js/nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
bkLib.onDomLoaded(function() {
  new nicEditor({buttonList : ['fontSize','bold','italic','underline','ol','ul','link', 'unlink'], iconsPath : '<?php echo e(url('/')); ?>/img/nicEditorIcons.gif'}).panelInstance('descripcionForo');
});
</script>


<footer>
<CENTER>
<h3> POWERED BY WEB BUSINESS </h3>
</CENTER>
</footer>