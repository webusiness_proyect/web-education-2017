<?php $__env->startSection('titulo'); ?>

  <title>  Cursos</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">
        <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-pencil-square" aria-hidden="true"></div>Cursos</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong>y administra cursos disponibles en tu institucion ejemplo:<strong> CURSO:MATEMATICAS</strong>
            </h4>
           
          </center>
        </div>
    

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>
        <div class="col-sm-12 panel panel-default">

          <br/>
           <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>


         <?php if (\Entrust::can('crear-curso')) : ?>

        <?php echo link_to_route('areas.create', $title = 'Nuevo Curso', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square']); ?>



         <?php echo $__env->make('areas.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <?php endif; // Entrust::can ?>

        <?php if(count($areas) == 0): ?>


          <p class="text-info">

            No se han registrado cursos aun.

          </p>
        </center>



        <?php else: ?>

        
           

        <br/>

  
    
          <table class="table table-hover table-hover table- table-bordered">

            <thead>

              <tr>

                <th>

                  NO

                </th>

                <th>

                  NOMBRE

                </th>

                <?php if (\Entrust::can('editar-curso')) : ?>

                <th>

                  ACTUALIZAR

                </th>

                <?php endif; // Entrust::can ?>

                <?php if (\Entrust::can('estado-curso')) : ?>

                <th>

                  ESTADO

                </th>

                <?php endif; // Entrust::can ?>

              </tr>

            </thead>

            <tbody id="datosAreas">

              <?php foreach($areas as $key => $area): ?>

                <tr>

                  <td>

                    <?php echo e($area->id_area); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($area->nombre_area)); ?>


                  </td>

                  <?php if (\Entrust::can('editar-curso')) : ?>

                  <td>

                    <?php echo link_to_route('areas.edit', $title = 'Editar', $parameters = $area->id_area, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>


                  </td>

                  <?php endif; // Entrust::can ?>

                  <?php if (\Entrust::can('estado-curso')) : ?>

                  <td>

                  
                    <?php if($area->estado_area == TRUE): ?>
                      <input type="checkbox" name="estado" checked value="<?php echo e($area->id_area); ?>" class="toggleEstado">
                  <?php else: ?>
                      <input type="checkbox" name="estado" value="<?php echo e($area->id_area); ?>" class="toggleEstado">
                  <?php endif; ?>


                  </td>

                  <?php endif; // Entrust::can ?>

                </tr>

              <?php endforeach; ?>

            </tbody>

          </table>

          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">

          <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          <div class="text-center">

            <?php echo $areas->links(); ?>


          </div>

        <?php endif; ?>

      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>