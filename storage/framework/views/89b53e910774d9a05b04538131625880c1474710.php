<?php echo Form::open(array('url'=>'tutores','method'=>'GET','autocomplete'=>'on','role'=>'search')); ?>

<div class="form-group">
<br/>
  <div class="input-group">
    
    <input type="text" class="form-control" name="searchText" placeholder="Buscar tutores por nombre o apellido..." value="<?php echo e($searchText); ?>">

    <span class="input-group-btn"> <div class="" aria-hidden="true"></div>

    <button type="submit" class="btn btn-info"><div class="fa fa-search"></div>Buscar</button>
    </span>
  </div>
</div>

<?php echo e(Form::close()); ?>