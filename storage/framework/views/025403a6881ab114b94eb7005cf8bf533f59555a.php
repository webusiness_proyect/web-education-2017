<?php $__env->startSection('titulo'); ?>

  <title>Actividades</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center"> <div class="fa fa-pencil"></div>  MIS ACTIVIDADES</h1>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      </div>
       <p>

          

        </p>

        <?php if(count($actividades) == 0): ?>

          <p class="text-info">

            No se han encontrado actividades para este curso...

          </p>

        <?php else: ?>


        <div class="col-sm-12 panel panel-danger">

      <div class="btn-danger">  
      <center>
      <br/>
      <H3> <DIV class="fa fa-exclamation-triangle"></DIV> TAREAS SIN ENTREGAR 
      
      </H3>
      <br/>
      </center>
      </div>
        <div class="panel-body">

<div class="container">
 
  <center><a href="#demo" class="btn btn-default" data-toggle="collapse"><div class="fa fa-plus"></div>... MAS NOTAS DE UNIDADES ANTERIORES ...</a></center>
  <div id="demo" class="collapse">
   
        
        

         <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>

                  <th>

                    ESTADO

                  </th>

                  <th>

                     ACTIVIDAD

                  </th>

                  <th>

                    DESCRIPCION ACTIVIDAD

                  </th>



                  <th>

                    FECHA DE ASIGNACION 

                  </th>

                  <th>

                    FECHA LIMITE DE ENTREGA 

                  </th>

                  <th>

                    NOTA ASIGNADA

                  </th>

                 

                  <th>

                    SUBIR TAREA

                  </th>

                </tr>

              </thead>

              <tbody>

               

                <?php foreach($ractividades as $key=>$ractividad): ?>

                

                 

                  <tr>

                   <td class="btn-danger">
                      <li class="fa fa-exclamation-triangle">SIN ENTREGAR</li>
                    

                  </td>
                 

                    

                    <td>

                    <?php echo e(mb_strtoupper($ractividad->nombre_actividad)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($ractividad->descripcion_actividad)); ?>


                  </td>

                  

                  <td>

                    <?php echo e(mb_strtoupper($ractividad->fecha_inicio)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($ractividad->fecha_entrega)); ?>


                  </td>


                  <td class="btn-danger">
                  TAREA NO ENTREGADA
                  </td>

                    

                    <td>

                      <a class="btn btn-info glyphicon glyphicon-share" href="/respuestaactividades/create?actividad=<?php echo e($ractividad->id_actividad); ?>"> ENTREGAR</a>

                    </td>

                  </tr>
                
                <?php endforeach; ?>

              </tbody>

            </table>
          </div>
          </div>
        </div>
      </div>

      

  


       
       <div class="btn-success">
       <br/>
       <center>
        <h3><DIV class="fa fa-check-circle"></DIV>TAREAS DE ESTA UNIDAD <strong>ENTREGADAS</strong></H3>
        <br/>
        </center>
        </h3>

        </div>

          <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>

                  <th>

                    ESTADO

                  </th>

                  <th>

                     ACTIVIDAD

                  </th>

                  <th>

                    DESCRIPCION ACTIVIDAD

                  </th>



                  <th>

                    FECHA DE ASIGNACION 

                  </th>

                  <th>

                    FECHA LIMITE DE ENTREGA 

                  </th>

                  <th>

                    NOTA ASIGNADA

                  </th>

                 

                  <th>

                    SUBIR TAREA

                  </th>

                </tr>

              </thead>

              <tbody>

               

                <?php foreach($actividades as $key=>$actividad): ?>

                

                 

                  <tr>

                    <?php if($actividad->estado_entrega_actividad=="ENTREGADO"): ?>

                  <td class="btn-warning">
                     
                    <li class="fa fa-info-circle"><?php echo e(mb_strtoupper($actividad->estado_entrega_actividad)); ?> pendiente de revision </li>

                  </td >
                    <?php elseif($actividad->estado_entrega_actividad=="CALIFICADO"): ?>

                    <td class="btn-success">
                     
                    <li class="fa fa-check-circle"><?php echo e(mb_strtoupper($actividad->estado_entrega_actividad)); ?></li>

                  </td>


                    <?php else: ?>
                   <td class="btn-danger">
                      <li class="fa fa-exclamation-triangle">SIN ENTREGAR</li>
                    

                  </td>
                  <?php endif; ?>

                    

                    <td>

                    <?php echo e(mb_strtoupper($actividad->nombre_actividad)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($actividad->descripcion_actividad)); ?>


                  </td>

                  

                  <td>

                    <?php echo e(mb_strtoupper($actividad->fecha_inicio)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($actividad->fecha_entrega)); ?>


                  </td>


                  <?php if($actividad->calificacion==NULL): ?>

                  <td class="btn-danger">
                  TAREA NO ENTREGADA
                  </td>

                  <?php elseif($actividad->calificacion==0): ?>
                     <td class="btn-warning">
                  TAREA PENDIENTE DE REVISION
                  </td>

                  <?php else: ?>


                  <td class="btn-success">
                      <center>


                    <?php echo e(mb_strtoupper($actividad->calificacion)); ?>

                    </center>

                  </td>
                    <?php endif; ?>
                    

                    <td>

                      <a disabled="disabled" class="btn btn-success fa fa-check-circle" href=#> TAREA ENTREGADA</a>

                      <!--<?php echo link_to_route('respuestaactividades.create', $title = 'Entregar tarea', $parameters = ['a'=>$actividad->id_actividad,'b'=>$actividad->id_actividad], $attributes = ['class'=>'btn btn-info ']); ?>

-->
                    </td>

                  </tr>
                
                <?php endforeach; ?>

              </tbody>

            </table>

            

          </div>
          

             <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
             <?php endif; ?>
        </div>

         

        


<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>