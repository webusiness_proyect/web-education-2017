



<div class="panel panel-primary hide" id="optionTemplate">

  <!-- Default panel contents -->

  <div class="panel-heading">Datos Tutores</div>

  <div class="panel-body">



<div class="form-group">

  <?php echo Form::label('nombre','Nombre*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('nombre_tutor', null, ['class'=>'form-control', 'placeholder'=>'Nombre del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('apellidos','Apellidos*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('apellidos_tutor', null, ['class'=>'form-control', 'placeholder'=>'Apellidos del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('direccion','Dirección*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('direccion_tutor', null, ['class'=>'form-control', 'placeholder'=>'Dirección de residencia del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('telefono','Telefono*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('telefono_primario_tutor', null, ['class'=>'form-control', 'placeholder'=>'Número de telefono del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('empresa telefonica','Empresa telefonica*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::select('empresa_telefono_tutor', $telefonicas, null, ['class'=>'form-control', 'placeholder'=>'Seleccione la empresa telefonica...']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('correo','Correo*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('correo_electronico_tutor', null, ['class'=>'form-control', 'placeholder'=>'Dirección de correo electronico del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('cui','CUI*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('cui_tutor', null, ['class'=>'form-control', 'placeholder'=>'Número de DPI del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('lugar trabajo','Lugar trabajo*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('lugar_trabajo_tutor', null, ['class'=>'form-control', 'placeholder'=>'Lugar donde trabaja el tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('direccion trabajo','Dirección trabajo*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('direccion_trabajo_tutor', null, ['class'=>'form-control', 'placeholder'=>'Dirección del lugar de trabajo del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('telefono','Telefono trabajo*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('telefono_trabajo_tutor', null, ['class'=>'form-control', 'placeholder'=>'Número de telefono de lugar de trabajo del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('empresa','Empresa telefonica*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::select('empresa_telefono_trabajo', $telefonicas, null, ['class'=>'form-control', 'placeholder'=>'Seleccione empresa telefonica...']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('cargo','Cargo*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('cargo_tutor', null, ['class'=>'form-control', 'placeholder'=>'Cargo que ocupa el tutor en el lugar de trabajo']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('parentesto','Parentesco*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('parentesco_tutor', null, ['class'=>'form-control', 'placeholder'=>'Parentesco del tutor con el estudiante']); ?>


  </div>

</div>



</div>

</div>



<div class="panel panel-primary">

  <!-- Default panel contents -->

   <div class="panel-heading">Datos Tutores</div>

  <div class="panel-body">



<div class="form-group">

  <?php echo Form::label('nombre','Nombre*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('nombre_tutor', null, ['class'=>'form-control', 'placeholder'=>'Nombre del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('apellidos','Apellidos*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('apellidos_tutor', null, ['class'=>'form-control', 'placeholder'=>'Apellidos del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('direccion','Dirección*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('direccion_tutor', null, ['class'=>'form-control', 'placeholder'=>'Dirección de residencia del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('telefono','Telefono*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('telefono_primario_tutor', null, ['class'=>'form-control', 'placeholder'=>'Número de telefono del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('empresa telefonica','Empresa telefonica*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::select('empresa_telefono_tutor', $telefonicas, null, ['class'=>'form-control', 'placeholder'=>'Seleccione la empresa telefonica...']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('correo','Correo*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('correo_electronico_tutor', null, ['class'=>'form-control', 'placeholder'=>'Dirección de correo electronico del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('cui','CUI*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('cui_tutor', null, ['class'=>'form-control', 'placeholder'=>'Número de DPI del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('lugar trabajo','Lugar trabajo*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('lugar_trabajo_tutor', null, ['class'=>'form-control', 'placeholder'=>'Lugar donde trabaja el tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('direccion trabajo','Dirección trabajo*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('direccion_trabajo_tutor', null, ['class'=>'form-control', 'placeholder'=>'Dirección del lugar de trabajo del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('telefono','Telefono trabajo*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('telefono_trabajo_tutor', null, ['class'=>'form-control', 'placeholder'=>'Número de telefono de lugar de trabajo del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('empresa','Empresa telefonica*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::select('empresa_telefono_trabajo', $telefonicas, null, ['class'=>'form-control', 'placeholder'=>'Seleccione empresa telefonica...']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('cargo','Cargo*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('cargo_tutor', null, ['class'=>'form-control', 'placeholder'=>'Cargo que ocupa el tutor en el lugar de trabajo']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('parentesto','Parentesco*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('parentesco_tutor', null, ['class'=>'form-control', 'placeholder'=>'Parentesco del tutor con el estudiante']); ?>


  </div>

</div>



</div>

</div>



<?php /*

<div class="panel panel-primary hide" id="optionTemplate">

  <!-- Default panel contents -->

   <div class="panel-heading">Datos Tutores</div>

  <div class="panel-body">



<div class="form-group">

  <?php echo Form::label('nombre','Nombre*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('nombre_tutor', null, ['class'=>'form-control', 'placeholder'=>'Nombre del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('apellidos','Apellidos*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('apellidos_tutor', null, ['class'=>'form-control', 'placeholder'=>'Apellidos del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('direccion','Dirección*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('direccion_tutor', null, ['class'=>'form-control', 'placeholder'=>'Dirección de residencia del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('telefono','Telefono*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('telefono_primario_tutor', null, ['class'=>'form-control', 'placeholder'=>'Número de telefono del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('empresa telefonica','Empresa telefonica*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::select('empresa_telefono_tutor', $telefonicas, null, ['class'=>'form-control', 'placeholder'=>'Seleccione la empresa telefonica...']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('correo','Correo*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('correo_electronico_tutor', null, ['class'=>'form-control', 'placeholder'=>'Dirección de correo electronico del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('cui','CUI*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('cui_tutor', null, ['class'=>'form-control', 'placeholder'=>'Número de DPI del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('lugar trabajo','Lugar trabajo*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('lugar_trabajo_tutor', null, ['class'=>'form-control', 'placeholder'=>'Lugar donde trabaja el tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('direccion trabajo','Dirección trabajo*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('direccion_trabajo_tutor', null, ['class'=>'form-control', 'placeholder'=>'Dirección del lugar de trabajo del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('telefono','Telefono trabajo*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('telefono_trabajo_tutor', null, ['class'=>'form-control', 'placeholder'=>'Número de telefono de lugar de trabajo del tutor']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('empresa','Empresa telefonica*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::select('empresa_telefono_trabajo', $telefonicas, null, ['class'=>'form-control', 'placeholder'=>'Seleccione empresa telefonica...']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('cargo','Cargo*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('cargo_tutor', null, ['class'=>'form-control', 'placeholder'=>'Cargo que ocupa el tutor en el lugar de trabajo']); ?>


  </div>

</div>

<div class="form-group">

  <?php echo Form::label('parentesto','Parentesco*', ['class'=>'col-sm-2 control-label']); ?>


  <div class="col-sm-10">

    <?php echo Form::text('parentesco_tutor', null, ['class'=>'form-control', 'placeholder'=>'Parentesco del tutor con el estudiante']); ?>


  </div>

</div>



</div>

</div>

*/ ?>

