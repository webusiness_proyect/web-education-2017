<?php $__env->startSection('titulo'); ?>
  <title>Puestos</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Puestos</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <?php if (\Entrust::can('crear-puesto')) : ?>
        <?php echo link_to_route('puestos.create', $title = 'Nuevo Puesto', $parameters = null, $attributes = ['class'=>'btn btn-primary']); ?>

        <br>
        <br>
        <?php endif; // Entrust::can ?>
        <?php if(count($puestos) == 0): ?>
          <p class="text-info">
            No se han registrado puestos aun.
          </p>
        <?php else: ?>
          <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NOMBRE
                </th>
                <?php if (\Entrust::can('editar-puesto')) : ?>
                <th>
                  ACTUALIZAR
                </th>
                <?php endif; // Entrust::can ?>
                <?php if (\Entrust::can('estado-puesto')) : ?>
                <th>
                  ESTADO
                </th>
                <?php endif; // Entrust::can ?>
              </tr>
            </thead>
            <tbody id="datosPuestos">
              <?php foreach($puestos as $key => $puesto): ?>
                <tr>
                  <td>
                    <?php echo e($key+1); ?>

                  </td>
                  <th>
                    <?php echo e(mb_strtoupper($puesto->nombre_puesto)); ?>

                  </th>
                  <?php if (\Entrust::can('editar-puesto')) : ?>
                  <th>
                    <?php echo link_to_route('puestos.edit', $title = 'Editar', $parameters = $puesto->id_puesto, $attributes = ['class'=>'btn btn-primary']); ?>

                  </th>
                  <?php endif; // Entrust::can ?>
                  <?php if (\Entrust::can('estado-puesto')) : ?>
                  <th>
                    <?php if($puesto->estado_puesto == true): ?>
                      <?php echo link_to_route('puestos.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarPuesto', 'data-id'=>$puesto->id_puesto, 'data-estado'=>$puesto->estado_puesto]); ?>

                    <?php else: ?>
                      <?php echo link_to_route('puestos.destroy', $title = 'Deshabilitado', $parameters = null, $attributes = ['class'=>'btn btn-danger eliminarPuesto', 'data-id'=>$puesto->id_puesto, 'data-estado'=>$puesto->estado_puesto]); ?>

                    <?php endif; ?>
                  </th>
                  <?php endif; // Entrust::can ?>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
        <?php endif; ?>
      </div>
      <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>