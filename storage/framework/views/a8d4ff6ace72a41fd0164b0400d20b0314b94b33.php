<?php $__env->startSection('titulo'); ?>

  <title>Sistema Educativo - WB</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>



  <div id="page-wrapper">



      <div class="row">

          <div class="col-lg-12">

          

            <div>
                   <H3> <center> <div class="fa fa-user" aria-hidden="true"></div>TUTOR BIENVENIDOS <?php echo e(ucwords(Auth::user()->name)); ?> </center></H3>
                </div>
               <div class="panel panel-primary">
               
                <div class="panel-footer">  
                  <H4> 
                    <center><strong> SITIO WEB EDUCATIVO </strong>
                  <strong>Por que sabemos lo importante que es para ti mantenerte informado sobre el 
                  desenvolvimiento y desempeño de tu hijo en su institucion hemos creado esta plataforma que permitira llevar en tiempo real cada una de las actividades, tareas, 
                  notas y la participacion de tu hijo en multiples actividades como foros o conversaciones</center></H4>


                  </div>
               </div>

                <div >
                   <H3> <center>ACCESO RAPIDO DEL TUTOR</center></H3>
                </div>


               
                   <div  class="table-responsive">
                    <table class="">

                      <thead>

                        <tr>
                          <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                          <link rel="stylesheet" href="/resources/demos/style.css">
                          <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                          <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                          <script>
                            $( function() {
                              $( document ).tooltip();
                            } );
                            </script>
                         <th>
                            <a href="#" class="btn btn-success btn-lg selected" title="Graficas y estadisticas en tiempo real"><i class="fa fa-pie-chart  fa-2x" aria-hidden="true"></i><br/>Actividades Pendientes de entregar</a>
 

 
                          </th>

                           <th>   

                            <a href="<?php echo e(url('/misalumnos')); ?>" class="btn btn-default btn-lg" title="Ve todas las tareas y actividades realizadas o sin realizar por tus hijos, aqui tambien podras ver los punteos de tus hijos"><i class="fa fa-graduation-cap fa-2x" aria-hidden="true"></i><br/>MIS ALUMNO</a>

                          </th>

                          <th>   

                            <a href="<?php echo e(url('/misdocnetes')); ?>" class="btn btn-default btn-lg" title="Conozca a los catedraticos que estan al cargo de la enseñanza de sus hijos, y ponte en contacto con ellos"><i class="fa fa-book fa-2x" aria-hidden="true"></i><br/>MIS DOCENTES</a>

                          </th>
                          <th>   

                            <a href="<?php echo e(url('/misdocnetes')); ?>" class="btn btn-default btn-lg" title="Mantente informado de los horarios de las clases, cursos y actividades  de tus hijos"><i class="fa fa-calendar fa-2x" aria-hidden="true"></i><br/>Mi horarios</a>

                          </th>
                         

                            
                        </thead>

                      </table>
                    </div>
              



          </div>

          <!-- /.col-lg-12 -->

      </div>
      <br/>

        <div class="row ">

          <div class="col-lg-8 panel panel-default">

           
                  


               <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                  <script type="text/javascript">
                   google.charts.load('current', {'packages':['corechart']});
                   google.charts.setOnLoadCallback(drawChart);
                   function drawChart() {

                      var data = google.visualization.arrayToDataTable([
                       ['Task', 'Hours per Day'],
                       ['ENTREGADOS',     11],
                      
                       ['PENDIENTES DE ENTREGAR',    7]
                      ]);

                     var options = {
                        title: 'TAREAS PENDIENTES DE ENTREGAR POR TU ALUMNO'
                      };

                      var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                      chart.draw(data, options);
                    }
                  </script>


              <div id="piechart"></div>

             <br/>
              <div class="panel panel-warning">
                <div class="panel-heading">
                   <H4> <center> <div class="fa fa-exclamation-circle" aria-hidden="true"></div> AYUDA Y SOPORTE </center></H4>
                </div>
                <div class="panel-footer">  <H4> <center>
                  <i class="fa fa-globe"></i><a href="<?php echo e(url('http://webusiness.co/')); ?>">http://webusiness.co/ </a><br/>
                  <i class="fa fa-at"></i>correo: education@webusiness.co <br/>
                  <a href="<?php echo e(url('/tel:+5022327806')); ?>"><i class="fa fa-phone"></i></a>Tel. +50223278063<br/>
                  <i class="fa fa-map-marker"></i>Centro America, Guatemala, Zona 9, 1era. avenida 8-00.<br/>
                 <a href="<?php echo e(url('/enviomensajes')); ?>"><i class="fa fa-envelope-o"></i>envianos un mensaje</a>
              </center></H4>
                 <center>
                   
                    
                    
                    
                  
                  </center>
              </div>
              </div>

                 



          </div>

          <div class="col-lg-4">
              <center>
              <img src="<?php echo e(url('/img/padre.png')); ?>"  width="300" height="450">
              </center>
          </div>

          

          <!-- /.col-lg-12 -->

      </div>



  </div>

  <!-- /#page-wrapper -->



<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>