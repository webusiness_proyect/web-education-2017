<?php $__env->startSection('titulo'); ?>



  <title>editar</title>



<?php $__env->stopSection(); ?>







<?php $__env->startSection('cuerpo'); ?>



<div id="page-wrapper">



  <div class="row">



    <div class="col-sm-12">



      <h1 class="page-header text-center"><div class="fa fa-edit"></div><div class="fa fa-user"></div>  Modifica la informacion de tu perfil </h1>



      <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>



  

      <div class="col-sm-12">

        <div class="alert alert-warning">


         
          <center>
        

            <div class="fa fa-edit"> 

                Modifica la informacion de tu perfil, *tu usuario de acceso no puede ser modificado

            </div>
            <br/>

          


          

       

      </div>



    </div>



    <div class="col-sm-12 panel panel-default">
    <br/>



      <?php echo Form::model($usuario, ['route'=>['miperfil.update', $usuario->id], 'method'=>'PUT',  'class'=>'form-horizontal', 'id'=>'nperfil']); ?>



 

        <?php echo $__env->make('miperfil.form.campos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>



      <?php echo Form::close(); ?>




    </div>



  </div>



</div>



<?php $__env->stopSection(); ?>




<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>