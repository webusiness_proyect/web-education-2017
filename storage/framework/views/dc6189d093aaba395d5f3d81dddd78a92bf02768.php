<?php $__env->startSection('titulo'); ?>

  <title>Sistema Educativo - WB</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>



  <div id="page-wrapper">



      <div class="row">

          <div class="col-lg-12">

          

              <div>
                   <H3> <center> <div class="fa fa-user" aria-hidden="true"></div> Panel de Control de Sistema Educativo WB </center></H3>
              </div>
              <div class="panel panel-primary">
               
              <div class="panel-footer">  
                  <H4> 
                    <center><strong> </strong>
                 Bienvenidos BIENVENIDOS <?php echo e(ucwords(Auth::user()->name)); ?> al software que hara mas facil para usted administrar su colegio o institución, <strong>Wb Education</strong> lo mejor en educación
                    </center>
                    </H4>


              </div>
              </div>
 
                <div >
                   <H3> <center>ACCESO RAPIDO DEL ADMINISTRADOR</center></H3>
                </div>


                    
      <a href="#" class="btn btn-success btn-lg selected " title="Graficas y estadisticas en tiempo real"><i class="fa fa-pie-chart  fa-2x col-sm-3 col-md-6 col-lg-2" aria-hidden="true"></i><br/><br/>  Estadisticas  </a>
 
      <a href="<?php echo e(url('/usuarios')); ?>" class="btn btn-default btn-lg" title="cree nuevos usuarios para ingresar para ingresar a su plataforma, el sistema generara un usuario y una contraseña las cuales llegaran al correo registrado"><i class="fa fa-users fa-2x col-sm-3 col-md-6 col-lg-2" aria-hidden="true"></i><br/><br/>Usuarios</a>
 
      <a href="<?php echo e(url('/carreras')); ?>" class="btn btn-default btn-lg" title="cree y administre nuevas carreras o diplomados para su institucion, ejemplo:Bachiller en Computación, Perito en mercadotecnia y cualquier otro curso vigente en tu pais"><i class="fa fa-graduation-cap fa-2x col-sm-3 col-md-6 col-lg-2" aria-hidden="true"></i><br/><br/>Carreras</a>

      <a href="<?php echo e(url('/grados')); ?>" class="btn btn-default btn-lg" title="cree y administre nuevos grados en su institucion ejemplo: primero basico "><i class="fa fa-bookmark-o fa-2x col-sm-3 col-md-6 col-lg-2" aria-hidden="true"></i><br/><br/>Grados</a>

      <a href="<?php echo e(url('/grados')); ?>" class="btn btn-default btn-lg" title="Cree su propio mensum de manera facil y muy logica"><i class="fa fa-bookmark fa-2x col-sm-3 col-md-6 col-lg-2" aria-hidden="true"></i><br/><br/>Mi Pensum</a>

      <a href="<?php echo e(url('/asignaciondocente')); ?>" class="btn btn-default btn-lg"><i class="fa fa-book fa-2x col-sm-3 col-md-6 col-lg-2" aria-hidden="true" title="asigne a personas roles y responsabilidades de docentes"></i><br/><br/>Docentes</a>

      <a href="<?php echo e(url('/inscripcionestudiantes')); ?>" class="btn btn-default btn-lg"><i class="fa fa-child fa-2x col-sm-3 col-md-6 col-lg-2" aria-hidden="true"></i><i class="fa fa-plus fa-2x col-sm-3 col-md-6 col-lg-2" aria-hidden="true" title="inscriba alumnos y seleccione una carrera disponible o un curso disponbile en el pensum de su institucion"></i><br/><br/>Inscripciones</a>

      <a href="<?php echo e(url('/horarios')); ?>" class="btn btn-default btn-lg"><i class="fa fa-calendar fa-2x col-sm-3 col-md-6 col-lg-2" aria-hidden="true" title="cree calendarios para optimizar el tiempo y recursos de su colegio, estos seran compartidos de manera inmediata con los integrantes del curso"></i><br/><br/>Calendarios</a>
 


                    
                          <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                          <link rel="stylesheet" href="/resources/demos/style.css">
                          <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                          <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                          <script>
                            $( function() {
                              $( document ).tooltip();
                            } );
                            </script>
                         
                        
              



          </div>

          <!-- /.col-lg-12 -->

      </div>
      <br/>


        <div class="row ">
          <div class="col-lg-8 panel panel-default">
               <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                  <script type="text/javascript">
                   google.charts.load('current', {'packages':['corechart']});
                   google.charts.setOnLoadCallback(drawChart);
                   function drawChart() {
                      var data = google.visualization.arrayToDataTable([
                       ['Task', 'Hours per Day'],
                       ['Primero',     11],
                       ['Segundo',      2],
                       ['Tercero',  2],
                       ['Cuarto', 2],
                      ['Quinto',    7],
                       ['Sexto',    10],
                       ['Primero Basico',    7],
                       ['Segundo Basico',    9],
                       ['Tercero Basico',    10],
                        ['Carrera 1',    7],
                        ['Carrera 2',    12],
                       ['carrera 3',    7]
                      ]);

                     var options = {
                        title: '    ESTADO ACTUAL DE INSCRIPCIONES'
                      };

                      var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                      chart.draw(data, options);
                    }
                  </script>


               <div id="piechart"></div>

             <br/>
              <div class="panel panel-warning">
                <div class="panel-heading">
                   <H3> <center> <div class="fa fa-exclamation-circle" aria-hidden="true"></div> AYUDA Y SOPORTE </center></H3>
                </div>
              
              </div>

                 



          </div>

          <div class="col-lg-4">
              <center>
              <img src="<?php echo e(url('/img/admin.png')); ?>" width="250" height="350">
              </center>
          </div>

          

          <!-- /.col-lg-12 -->

      </div>



  </div>

  <!-- /#page-wrapper -->



<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>