<?php $__env->startSection('titulo'); ?>
  <title>Foros</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center"><div class="fa fa-comments"></div>Foros de este curso</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12 panel panel-default">
        <p>

        <center>   
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

           <?php if (\Entrust::can('crear-foro')) : ?>


          <?php echo link_to_route('foros.create', $title = ' Nuevo Foro', $parameters = ['a'=>$curso], $attributes = ['class'=>'btn btn-primary fa fa-plus']); ?>


          <?php endif; // Entrust::can ?>
          </center>
        </p>
        <?php if(count($foros) == 0): ?>
          <p class="text-info">
            No se han creado foros en este curso...
          </p>
        <?php else: ?>
          <div class="table-responsive">
            <table class="table table-hover table-bordered">
              <thead>
                <tr>
                 
                  <th>
                    MIS FOROS
                  </th>
                 

                </tr>
              </thead>
              <tbody>
                <?php foreach($foros as $key=>$foro): ?>
                  <tr>

                    <td>
                    <h2>
                     
                      <?php echo link_to_route('respuestasforos.show', $title = $foro->titulo_foro, $parameters = $foro->id_foro, $attributes= null); ?></h2><?php echo link_to_route('respuestasforos.show', $title = $foro->mensaje_foro, $parameters = $foro->id_foro, $attributes= null); ?>

                      <?php echo link_to_route('respuestasforos.create', $title = ' Responder', $parameters = ['foro'=>$foro->id_foro], $attributes = ['class'=>'btn btn-success fa fa-comments']); ?>

                      <h5>CREADO POR:<?php echo link_to_route('respuestasforos.show', $title = $foro->nombre, $parameters = $foro->id_foro, $attributes= null); ?>

                      
                      <?php echo link_to_route('respuestasforos.show', $title = $foro->rol, $parameters = $foro->id_foro, $attributes= null); ?>

                      <?php echo link_to_route('respuestasforos.show', $title = $foro->fecha_foro, $parameters = $foro->id_foro, $attributes= null); ?>


                       <?php if (\Entrust::can('editar-foros')) : ?>
                  
                      
                        <?php echo link_to_route('foros.edit', $title = ' Editar', $parameters = $foro->id_foro, $attributes = ['class'=>'btn btn-warning fa fa-edit']); ?>

                      
                   
                      <?php endif; // Entrust::can ?>
                      </h5>
                    </td>

                    
                    

                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>