<?php $__env->startSection('titulo'); ?>

  <title>Usuarios</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center">Usuarios</h1>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      </div>

      <div class="col-sm-12">

        <?php if (\Entrust::can('crear-usuario')) : ?>

        <?php echo link_to_route('usuarios.create', $title = 'Nuevo Usuario', $parameters = null, $attributes = ['class'=>'btn btn-primary']); ?>


        <br>

        <br>

        <?php endif; // Entrust::can ?>

        <?php if(count($usuarios) == 0): ?>

          <p class="text-info">

            No se han registrado usuarios aun.

          </p>

        <?php else: ?>

          <div class="table-responsive">

            <table class="table table-hover">

              <thead>

                <tr>

                  <th>

                    NO

                  </th>

                  <th>

                    NOMBRE

                  </th>

                </tr>

              </thead>

              <tbody>

                <?php foreach($usuarios as $key => $usuario): ?>

                  <tr>

                    <td>

                      <?php echo e($key+1); ?>


                    </td>

                    <td>

                      <?php echo e(mb_strtoupper($usuario->name)); ?>


                    </td>

                    <td>

                      <?php echo e($usuario->email); ?>


                    </td>

                  </tr>

                <?php endforeach; ?>

              </tbody>

            </table>

            <center>

            <?php echo $usuarios->links(); ?>


            </center>

          </div>

        <?php endif; ?>

      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>