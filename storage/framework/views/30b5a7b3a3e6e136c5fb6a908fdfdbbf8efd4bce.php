<?php $__env->startSection('titulo'); ?>
  <title>Asignación Docentes</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Asignación Docentes</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <?php if (\Entrust::can('crear-asignacion')) : ?>
          <?php echo link_to_route('asignaciondocente.create', $title = 'Nueva Asignación', $parameters = null, $attributes = ['class'=>'btn btn-primary']); ?>
          <br>
          <br>
        <?php endif; // Entrust::can ?>
        <?php if(count($docentes) == 0): ?>
          <p class="text-info">
            No se han registrado asignaciónes de docentes aun.
          </p>
        <?php else: ?>
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    DOCENTE
                  </th>
                  <th>
                    ACTUALIZAR
                  </th>
                  <th>
                    VER ASIGNACIÓN
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($docentes as $key => $docente): ?>
                  <tr>
                    <td>
                      <?php echo e($key+1); ?>
                    </td>
                    <td>
                      <?php echo e(mb_strtoupper($docente->nombres_persona.' '.$docente->apellidos_persona)); ?>
                    </td>
                    <td>
                      <?php echo link_to_route('asignaciondocente.edit', $title = 'Editar Asignación', $parameters = $docente->id_persona, $attributes = ['class'=>'btn btn-primary']); ?>
                    </td>
                    <td>
                      <?php echo link_to_route('asignaciondocente.show', $title = 'Ver Cursos', $parameters = $docente->id_persona, $attributes = ['class'=>'btn btn-warning']); ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>