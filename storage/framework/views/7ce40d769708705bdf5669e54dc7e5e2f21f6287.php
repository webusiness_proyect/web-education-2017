<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sistema Educativo - WB</title>
    <!-- Arhivos css para estilos-->
    <?php echo $__env->make('plantillas.estilos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- /#fin del los archivos css -->


 
    
    <script>
    $( function() {
      $( document ).tooltip();
       $(".draggable").draggable();
     
    } );
    </script>
  </head>

<?php echo $__env->make('bienvenida.menututor', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         

  <body>



  <div id="page-wrapper">



      <div class="row">

          <div class="col-lg-12">

          

            <div>
                   <H3> <center> <div class="fa fa-user" aria-hidden="true"></div>TUTOR BIENVENIDOS <?php echo e(ucwords(Auth::user()->name)); ?> </center></H3>
                </div>
               <div class="panel panel-primary">
               
                <div class="panel-footer">  
                  <H4> 
                    <center>
                    <strong> SITIO WEB EDUCATIVO </strong>
                  <strong>Por que sabemos lo importante que es para ti mantenerte informado sobre el 
                  desenvolvimiento y desempeño de tu hijo en su institución hemos creado esta plataforma hacer muchas cosas
                    <a href="#demo2"  data-toggle="collapse">
                  <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i><strong>MAS INFORMACION</strong></a>
                  <center>
                  <div id="demo2" class="collapse">
                   <strong>¿QUE PUEDO HACER?</strong>
                   <li>Ver actividades actividades pendientes de entregar por mi hijo</li>

                  <li> Ver los horarios asignados a tus hijos cursos en tiempo real </li>
                  <li> Recibe notificaciones de tu institución como reuniones, información y mucho más </li>
                  <li>Conoce a los docentes de tus hijos y realiza consultas en cualquier momento</li>
                  </H4></center></H4>


                  </div>
                 
               </div>

                <div >
                   <H3> <center>ACCESO RAPIDO DEL TUTOR</center></H3>
                </div>


               
                   <div  class="table-responsive">
                    <table class="">

                      <thead>

                        <tr>
                          <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                          <link rel="stylesheet" href="/resources/demos/style.css">
                          <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                          <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                          <script>
                            $( function() {
                              $( document ).tooltip();
                            } );
                            </script>
                         <th>
                            <a href="#" class="btn btn-success btn-lg selected" title="Graficas y estadisticas en tiempo real"><i class="fa fa-pie-chart  fa-2x" aria-hidden="true"></i><br/>Actividades Pendientes de entregar</a>
 

 
                          </th>

                           <th>   

                            <a href="<?php echo e(url('/misalumnos')); ?>" class="btn btn-default btn-lg" title="Ve todas las tareas y actividades realizadas o sin realizar por tus hijos, aqui tambien podras ver los punteos de tus hijos"><i class="fa fa-graduation-cap fa-2x" aria-hidden="true"></i><br/>MIS ALUMNO</a>

                          </th>

                          <th>   

                            <a href="<?php echo e(url('/misdocnetes')); ?>" class="btn btn-default btn-lg" title="Conozca a los catedraticos que estan al cargo de la enseñanza de sus hijos, y ponte en contacto con ellos"><i class="fa fa-book fa-2x" aria-hidden="true"></i><br/>MIS DOCENTES</a>

                          </th>
                          <th>   

                            <a href="<?php echo e(url('/misdocnetes')); ?>" class="btn btn-default btn-lg" title="Mantente informado de los horarios de las clases, cursos y actividades  de tus hijos"><i class="fa fa-calendar fa-2x" aria-hidden="true"></i><br/>Mi horarios</a>

                          </th>
                         

                            
                        </thead>

                      </table>
                    </div>
              



          </div>

          <!-- /.col-lg-12 -->

      </div>
      <br/>

        <div class="row ">

          
            <div class="col-sm-2 panel panel-default" >
          <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                <center>
                  <a href="<?php echo e(url('/mishorarios')); ?>" title="Ver todos los horarios"><div class="fa fa-calendar fa-2x"></div><?php echo e(ucwords($today)); ?> 
                  </a>
                  </center>
                </th>
              </tr>
            </thead>
            <tbody>
             
              <?php foreach($horarios as $key => $horario): ?>
                <tr>
                
                  
                   

                  <td>
                      <h6>
                     <b>INICIA:</b><?php echo e(mb_strtoupper($horario->inicio)); ?>

                     <b>FIN:</b><?php echo e(mb_strtoupper($horario->fin)); ?><br/>
                     <b>CURSO:<?php echo e(mb_strtoupper($horario->nombre_area)); ?></b>
                     
                  
                     
                    </h6>
                    
                     


                  </td>
                </tr>
                <?php endforeach; ?>


             
            
            </tbody>
          </table>
        </div>
           
                  
              <div class="col-lg-6 panel panel-default">

               <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                  <script type="text/javascript">
                   google.charts.load('current', {'packages':['corechart']});
                   google.charts.setOnLoadCallback(drawChart);
                   function drawChart() {

                      var data = google.visualization.arrayToDataTable([
                       ['Task', 'Hours per Day'],
                       ['ENTREGADOS',     11],
                      
                       ['PENDIENTES DE ENTREGAR',    7]
                      ]);

                     var options = {
                        title: 'TAREAS PENDIENTES DE ENTREGAR POR TU ALUMNO'
                      };

                      var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                      chart.draw(data, options);
                    }
                  </script>


              <div id="piechart"></div>

             <br/>
          

                 



          </div>

          <div class="col-lg-4">
              <center>
              <img src="<?php echo e(url('/img/padre.png')); ?>"  width="300" height="450">
              </center>
          </div>

          

          <!-- /.col-lg-12 -->

      </div>



  </div>

  <!-- /#page-wrapper -->


<?php echo $__env->make('plantillas.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>



  <!-- /#fin de los archivos javascript -->

  </body>

</html>

