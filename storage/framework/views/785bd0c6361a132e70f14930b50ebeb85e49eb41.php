<?php $__env->startSection('titulo'); ?>
  <title>Empleados</title>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
       <div class="panel-body ">
        <h2 class="page-header text-center"> <div class="fa fa-users" aria-hidden="true"></div><div class="fa fa-gear" aria-hidden="true"></div>Notas de <?php echo e(ucwords($students[0]->nombre_estudiante.' '.$students[0]->apellidos_estudiante)); ?></h2>
        <h4 class="text-center">Año: <?php echo e($year); ?></h4>
        <br/>


          <center>
                          <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>  
          </center>
   

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
      </div>
      <div class="col-sm-12 panel panel-default">
      <br/>

        <?php if(count($students) == 0): ?>
          <p class="text-info">
            No hay alumnos registrados a este curso y este año.
          </p>
          </center>
        <?php else: ?>
          <div class="table-responsive">
            <table class="table table-hover table- table-bordered">
              <thead>
                <tr>
                  <th>
                    <H3><STRONG>CURSOS</STRONG></H3>
                  </th>
                  <th>
                    <H3><STRONG> SELECCIONE UNIDAD </STRONG></H3>
                  </th>
                 
                  
                
                 
                </tr>
              </thead>
              <tbody>
                <?php foreach($students as $key => $persona): ?>
                  <tr>

                   <td>
                    <H4><STRONG><?php echo e($persona->nombre_area); ?></STRONG></H4>
                   </td>
                   
                    <td>
                    <?php foreach($unidades as $key=>$unidad): ?>
                        

                         <a href="/resultadosbuscarnotasbimestre/?unidad=<?php echo e($unidad->id_unidad); ?>&area=<?php echo e($persona->id_area); ?>&year=<?php echo e($year); ?>&user=<?php echo e($persona->id); ?>" title="Click aqui para ver mis notas y puntos asignados para esta unidad" class="btn btn-default"><div class="fa fa-trophy" ></div> <?php echo e($unidad->nombre_unidad); ?></a>
                         
                    <?php endforeach; ?>

                      
                      
                    </td>
                   
                   
                   
                   
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>

           <?php echo Form::hidden('_token', csrf_token(), ['id'=>'token']); ?>


          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>