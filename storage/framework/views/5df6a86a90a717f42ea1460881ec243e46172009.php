<?php $__env->startSection('title'); ?>
  <title>Respuesta del Foro</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center"> <div class="fa fa-comments"></div>Foros del curso de <?php echo $foro->nombre_area; ?></h1>
      </div>
      <div class="col-sm-12">
      <center>   <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?></center>
      <br/>
        <div class="panel panel-primary">
          <div class="panel-heading">
            <h2 class="panel-title"><H1> <div class="fa fa-comment-o"></div> TEMA PRINCIPAL: <?php echo e($foro->titulo_foro); ?></H1>  <?php echo $foro->nombre; ?> - <?php echo $foro->rol; ?> </h2> 
          </div>
          <div class="panel-body">
            <?php echo $foro->mensaje_foro; ?>

            <?php echo link_to_route('respuestasforos.create', $title = ' Responder', $parameters = ['foro'=>$foro->id_foro], $attributes = ['class'=>'btn btn-info fa fa-comments']); ?>

          </div>
          <div class="panel-body">
            Fecha de creacion: <?php echo $foro->fecha_foro; ?>


          </div>
        </div>
      </div>
      <?php foreach($respuestas as $respuesta): ?>

      <?php if($respuesta['rol']=='Profesor'): ?>

        <div class="panel panel-primary">
          <div class="panel-heading">
            <h2 class="panel-title"><div class="fa fa-comment-o"></div><?php echo e(ucwords($respuesta['nombre'].' - '.$respuesta['rol'])); ?> </h2> 
          </div>
          <div class="panel-body">
            <?php echo $respuesta['respuesta']; ?>

          </div>
          <div class="panel-body">
            Fecha de respuesta: <?php echo e($respuesta['fecha']); ?>


          </div>
        </div>

        <?php else: ?>




      <div class="col-sm-offset-2 col-sm-10">
        <div class="panel panel-success">
          <div class="panel-heading">
            <h3 class="panel-title"><div class="fa fa-comments-o"></div><?php echo e(ucwords($respuesta['nombre'].' - '.$respuesta['rol'])); ?></h3>
          </div>
          <div class="panel-body">
            <?php echo $respuesta['respuesta']; ?>

          </div>
          <div class="panel-footer text-right">
             <?php echo link_to_route('respuestasforos.create', $title = ' Responder', $parameters = ['foro'=>$foro->id_foro], $attributes = ['class'=>'btn btn-info fa fa-comments']); ?> Fecha de respuesta: <?php echo e($respuesta['fecha']); ?>

          </div>
        </div>


      </div>

      <?php endif; ?>
    <?php endforeach; ?>



    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>