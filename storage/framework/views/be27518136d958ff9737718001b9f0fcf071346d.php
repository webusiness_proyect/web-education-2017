

<?php $__env->startSection('titulo'); ?>
  <title>Nuevo Usuarios</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center"><div class="fa fa-plus" aria-hidden="true"></div><div class="fa fa-user" aria-hidden="true"></div>Nuevo Usuario</h1>
        <p>
          <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

          Nota: Todos los campos con (*) son obligatorios.
        </p>
        <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         
      </div>
      <div class="col-sm-12">


        <?php echo Form::open(['route'=>'usuarios.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'nusuario']); ?>

          <?php echo $__env->make('usuarios.form.campos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo Form::close(); ?>

      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>