<?php $__env->startSection('titulo'); ?>
  <title>Grados</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel-body ">
        <h1 class="page-header text-center"> <div class="fa fa-bookmark-o" aria-hidden="true"></div>Grados</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra grados los cuales estaran disponibles en tu pensum ejemplo:<strong>SEGUNDO </strong>
            </h4>
          </center>
        </div>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      </div>
     <div class="col-sm-12 panel panel-default">
        <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

        <?php if (\Entrust::can('crear-grado')) : ?>
        <?php echo link_to_route('grados.create', $title = 'Nuevo Grado', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square']); ?>

        <br>
        <br>
        <?php endif; // Entrust::can ?>
         <?php echo $__env->make('grados.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
        <?php if(count($grados) == 0): ?>
          <p class="text-info">
            No se han registrado grados aun.
          </p>
          </center>
        <?php else: ?>
          <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NOMBRE GRADO
                </th>
                <th>
                  FECHA CREACION
                </th>
                <?php if (\Entrust::can('editar-grado')) : ?>
                <th>
                  ACTUALIZAR
                </th>
                <?php endif; // Entrust::can ?>
                <?php if (\Entrust::can('estado-grado')) : ?>
                <th>
                  ESTADO
                </th>
                <?php endif; // Entrust::can ?>
              </tr>
            </thead>
            <tbody id="datosGrados">
              <?php foreach($grados as $key => $grado): ?>
                <tr>
                  <td>
                    <?php echo e($key+1); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($grado->nombre_grado)); ?>

                  </td>
                   <td>
                    <?php echo e(mb_strtoupper($grado->fecha_registro_grado)); ?>

                  </td>
                  <?php if (\Entrust::can('editar-grado')) : ?>
                  <td>
                    <?php echo link_to_route('grados.edit', $title = 'Editar', $parameters = $grado->id_grado, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>

                  </td>
                  <?php endif; // Entrust::can ?>
                  <?php if (\Entrust::can('estado-grado')) : ?>
                  <td>
                 

                    <?php if($grado->estado_grado == TRUE): ?>
                              <input type="checkbox" name="estado" checked value="<?php echo e($grado->id_grado); ?>" class="toggleEstado">
                            <?php else: ?>
                              <input type="checkbox" name="estado" value="<?php echo e($grado->id_grado); ?>" class="toggleEstado">
                            <?php endif; ?>
                  </td>
                  <?php endif; // Entrust::can ?>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
          <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>