<?php $__env->startSection('titulo'); ?>

  <title>Notificaciones</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">
  <br/>
  <br/>
  <br/>

    <div class="row panel panel-default">

      <div class="col-sm-12 panel-body">


        <h1 class="page-header text-center"><div class="fa fa-comments"></div><?php echo e(ucwords($notificaciones[0]->titulo_notificacion)); ?></h1>

        <h3 class="text-center"><?php echo e(ucwords($notificaciones[0]->informacion_notificacion)); ?></h3>
        <h4 class="text-center">
        CREADO POR:<?php echo e(ucwords($notificaciones[0]->name.''.$notificaciones[0]->fecha_create)); ?> 
        </h4>

      </div>

      <div class="col-sm-12">

        

        </div>

        <br/>
        <center>
        <button type="submit" name="guardar" class="btn btn-success">
        <span class="fa fa-paper-plane-o"></span> RESPONDER</button>
        </center>
        <br/>
      </div>
      <br/>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>