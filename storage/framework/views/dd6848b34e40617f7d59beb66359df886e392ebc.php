<div class="form-group">
  <button type="button" class="btn btn-primary btn-block" id="agregarReferencia">Agregar Referencia <i class="fa fa-plus"></i></button>
</div>



   <div class="panel panel-primary hide"  id="referenciaTemplate">
     <!-- Default panel contents -->
     <div class="panel-heading">Datos Referencias</div>
     <div class="panel-body">
   <div class="form-group">
     <?php echo Form::label('nombre','Nombre*', ['class'=>'col-sm-12']); ?>
     <div class="col-sm-12">
       <?php echo Form::text('nombre', null, ['class'=>'form-control','placeholder'=>'Nombre completo de la referencia personal']); ?>
     </div>
   </div>
   <div class="form-group">
     <?php echo Form::label('parentesco','Parentesco*', ['class'=>'col-sm-12']); ?>
     <div class="col-sm-12">
       <?php echo Form::text('parentesco', null, ['class'=>'form-control','placeholder'=>'Parentescto de la referencia']); ?>
     </div>
   </div>
   <div class="form-group">
     <?php echo Form::label('telefono','Telefono*', ['class'=>'col-sm-12']); ?>
     <div class="col-sm-12">
       <?php echo Form::text('telefono', null, ['class'=>'form-control','placeholder'=>'Número de telefono de la referencia personal']); ?>
     </div>
   </div>
   </div>
   </div>

<?php /*<div class="add-employee">
        <label>Employee Name</label>
        <input type="text" name="employee[1][name]">
        <label>Employee Title</label>
        <input type="text" name="employee[1][title]">
    </div>*/ ?>

<div class="panel panel-primary" >
  <!-- Default panel contents -->
  <div class="panel-heading">Datos Referencias</div>
  <div class="panel-body">
<div class="form-group">
  <?php echo Form::label('nombre','Nombre*', ['class'=>'col-sm-12']); ?>
  <div class="col-sm-12">
    <?php echo Form::text('referencia[1][nombre]', null, ['class'=>'form-control','placeholder'=>'Nombre completo de la referencia personal']); ?>
  </div>
</div>
<div class="form-group">
  <?php echo Form::label('parentesco','Parentesco*', ['class'=>'col-sm-12']); ?>
  <div class="col-sm-12">
    <?php echo Form::text('referencia[1][parentesco]', null, ['class'=>'form-control','placeholder'=>'Parentescto de la referencia']); ?>
  </div>
</div>
<div class="form-group">
  <?php echo Form::label('telefono','Telefono*', ['class'=>'col-sm-12']); ?>
  <div class="col-sm-12">
    <?php echo Form::text('referencia[1][telefono]', null, ['class'=>'form-control','placeholder'=>'Número de telefono de la referencia personal']); ?>
  </div>
</div>
</div>
</div>



<?php /*
<div class="panel panel-primary hide" id="referenciaTemplate">
  <!-- Default panel contents -->
  <div class="panel-heading">Datos Tutores</div>
  <div class="panel-body">

<div class="form-group">
  <?php echo Form::label('nombre','Nombre*', ['class'=>'col-sm-12']); ?>
  <div class="col-sm-12">
    <?php echo Form::text('nombre_referencia[]', null, ['class'=>'form-control','placeholder'=>'Nombre de la referencia personal']); ?>
  </div>
</div>
<div class="form-group">
  <?php echo Form::label('parentesco','Parentesco*', ['class'=>'col-sm-12']); ?>
  <div class="col-sm-12">
    <?php echo Form::text('parentesco_referencia[]', null, ['class'=>'form-control','placeholder'=>'Parentescto de la referencia']); ?>
  </div>
</div>
<div class="form-group">
  <?php echo Form::label('telefono','Telefono*', ['class'=>'col-sm-12']); ?>
  <div class="col-sm-12">
    <?php echo Form::text('telefono_referencia[]', null, ['class'=>'form-control','placeholder'=>'Número de telefono de la referencia personal']); ?>
  </div>
</div>

</div>
</div>

*/ ?>
