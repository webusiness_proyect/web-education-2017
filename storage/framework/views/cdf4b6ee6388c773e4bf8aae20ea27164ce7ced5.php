<?php $__env->startSection('titulo'); ?>
  <title>Constancia de Inscripción</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Constancia de Inscripción</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>;
      </div>
        <div id="visualizador-pdf" style="height: 28em; padding:0; border: 10px solid rgba(0,0,0,.2);" class="col-sm-12">
          <!-- visualizador de pdf incrustado en la pagina -->

        </div>
      <script src="<?php echo e(url('/')); ?>/js/pdfobject.min.js"></script>
      <script>PDFObject.embed("<?php echo e(url('/')); ?>/constancia_inscripcion/constancia.pdf", "#visualizador-pdf");</script>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>