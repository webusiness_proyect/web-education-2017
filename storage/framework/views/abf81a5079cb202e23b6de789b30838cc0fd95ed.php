<?php $__env->startSection('titulo'); ?>

  <title>Planes</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center">Planes</h1>

          <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      </div>

      <div class="col-sm-12">

        <?php if (\Entrust::can('crear-plan')) : ?>

        <?php echo link_to_route('planes.create', $title ='Nuevo Plan', $parameters = null, $attributes = ['class'=>'btn btn-primary']); ?>


        <br>

        <br>

        <?php endif; // Entrust::can ?>

        <?php if(count($planes) == 0): ?>

          <p class="text-info">

            No se han registrado planes aun...

          </p>

        <?php else: ?>

          <table class="table table-hover">

            <thead>

              <tr>

                <th>

                  NO

                </th>

                <th>

                  NOMBRE PLAN

                </th>

                <?php if (\Entrust::can('editar-plan')) : ?>

                <th>

                  ACTUALIZAR

                </th>

                <?php endif; // Entrust::can ?>

                <?php if (\Entrust::can('estado-plan')) : ?>

                <th>

                  ESTADO

                </th>

                <?php endif; // Entrust::can ?>

              </tr>

            </thead>

            <tbody id="datosPlanes">

              <?php foreach($planes as $key => $plan): ?>

                <tr>

                  <td>

                    <?php echo e($key+1); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($plan->nombre_plan)); ?>


                  </td>

                  <?php if (\Entrust::can('editar-plan')) : ?>

                  <td>

                    <?php echo link_to_route('planes.edit', $title = 'Editar', $parameters = $plan->id_plan, $attributes = ['class'=>'btn btn-primary']); ?>


                  </td>

                  <?php endif; // Entrust::can ?>

                  <?php if (\Entrust::can('estado-plan')) : ?>

                  <td>

                    <?php if($plan->estado_plan == true): ?>

                      <?php echo link_to_route('planes.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarPlan', 'data-id'=>$plan->id_plan, 'data-estado'=>$plan->estado_plan]); ?>


                    <?php else: ?>

                      <?php echo link_to_route('planes.destroy', $title = 'Deshabilitado', $parameters = null, $attributes = ['class'=>'btn btn-danger eliminarPlan', 'data-id'=>$plan->id_plan, 'data-estado'=>$plan->estado_plan]); ?>


                    <?php endif; ?>

                  </td>

                  <?php endif; // Entrust::can ?>

                </tr>

              <?php endforeach; ?>

            </tbody>

          </table>

          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">

        <?php endif; ?>

      </div>

    </div>



    <!-- Mensaje de carga-->

    <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>



  </div>







<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>