    <div class="tab-content">      
    <div id="bgrados" class="tab-pane fade in active">        
    <h3>GRADOS</h3>        
    <div class="form-group">          
    <?php echo Form::label('plan', 'Plan*', ['class'=>'col-sm-2 control-label']); ?>          
    <div class="col-sm-10">            
    <?php echo Form::select('id_plan', $planes, null, ['class'=>'form-control', 'placeholder'=>'Selecione un plan...', 'id'=>'pnpensum']); ?>          
    </div>        
    </div>        
    <div class="form-group">
              <?php echo Form::label('jornada', 'Jornada*', ['class'=>'col-sm-2 control-label']); ?>          
              <div class="col-sm-10">            <?php echo Form::select('id_jornada', $jornadas, null, ['class'=>'form-control', 'placeholder'=>'Selecione una jornada...', 'id'=>'jnpensum']); ?>          
              </div>        
    </div>        
    <div class="form-group">          
    <div class="table-responsive">            
    <table class="table table-hover">              
    <thead>                
    <tr>                  
    <th>                    NO                  </th>                  
    <th>                    GRADO                  </th>                  
    <th>                    NIVEL                  </th>                  
    <th>                    CARRERA                  </th>                  
    <th>                    SECCION                  </th>                  
    <th>                    SELECCIONE                  </th>                </tr>              
    </thead>              
    <tbody id="grados" class="panel panel-default">              
    </tbody>            
    </table>          
    </div>        
    </div>      
    </div>      
    <div id="amensaje" class="tab-pane fade">        
    <h3>NOTIFICACION</h3>        
    
    <div class="form-group">          
    <?php echo Form::label('buscar', 'Seleccione Tipo de Usuarios*', ['class'=>'col-sm-2 control-label']); ?>          
    <div class="col-sm-10">            
    <div class="input-group">              
    <?php echo Form::select('id_rol', $roles, null, ['class'=>'form-control', 'placeholder'=>'Selecione un rol...']); ?> 
                
    <span class="input-group-btn">                
    <button class="btn btn-default" type="button" id="borrarBuscar"><span class="glyphicon glyphicon-remove"></span></button>                
    <button class="btn btn-default" type="button" id="agregarrol"><span class="fa fa-check"></span> Agregar</button>              
    </span>            
    </div>
    </div> 

    <script>
    var URLdomain = window.location.host;//para obtener el host del sitio
    var protocolo = window.location.protocol;//para obtenerl el protocolo del sitio
    

    $('#agregarrol').click(function(event) {
      event.preventDefault();
      var rol = $('#id_rol option[value='+ valor +']').attr('selected',true);
      var id = $('#id_rol').data('id');
      $('#id_rol').data('id', '');
      if (id == '') {
        alert('Curso no valido, no esta registrado en la base de datos!!!');
      } else {
        
          var b = false;
          //recorremos todos los inputs existentes con .each
          $('input[name="roles[]"]').each(function() {
            //$(this).val() es el valor del input correspondiente
            console.log($(this).val());
            if ($(this).val() == id) {
              b = true;
            }
          });
          if (b == false) {
            var html = '<div class="alert alert-success alert-dismissible" role="alert">';
            html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
            html += '<strong>'+rol+'</strong><input type="hidden" name="roles[]" value="'+id+'">;
            html += '</div>';
            $('#usuariosroles').append(html);
          
          $('#id_rol').val('');
        }//fin del if else
      }//fin del if else

    });
    </script>
   


              
                   
    <!-- Lugar donde se mostraran los cursos que se van a asignar al pensum del grado -->        
    <div class="col-sm-6" id="usuariosroles">        
    </div> 
    </div>

    <div class="form-group"> 
      <div class="col-sm-2">         
    <?php echo Form::label('titulo del mensaje', 'Titulo del mensaje*', ['class'=>'control-label']); ?>

    </div>
    
    <div class="col-sm-10">           
    <?php echo e(Form::text('titulo', null,['class'=>'form-control', 'placeholder'=>'titulo de la notificacion...'])); ?>          
    </div>
    </div> 
      <div class="form-group"> 
      <div class="col-sm-2">         
    <?php echo Form::label('descripcion', 'Cuerpo del mensaje*', ['class'=>'control-label']); ?>

    </div>
    
    <div class="col-sm-10">           
    <?php echo e(Form::textarea('descripcion', null,['class'=>'form-control', 'placeholder'=>'Nombre texto de la notificacion...'])); ?>          
    </div>
    </div>  
    </div>    
     

    <div id="gnotificacion" class="tab-pane fade">        
    <h3>Enviar Mensaje</h3>        
    <div class="col-sm-12"> 
    <center>         
    <button type="submit" name="guardar" class="btn btn-success">
    <span class="fa fa-paper-plane-o"></span> enviar</button>  
    </center>      
    </div>      
    </div>    
    	