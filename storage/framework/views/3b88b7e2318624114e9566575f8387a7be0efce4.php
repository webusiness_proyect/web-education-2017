

      <div class="form-group">

        <?php echo Form::label('actividad', 'Tipo de actividad: *', ['class'=>'col-sm-2 control-label']); ?>


        <div class="col-sm-10">

          <?php echo Form::select('id_tipo_actividad', $tipo_actividad, null, ['class'=>'form-control', 'placeholder'=>'Seleccione actividad...']); ?>


        </div>

      </div>



       

      <div class="form-group">

        <?php echo Form::label('nombre actividad', 'Actividad*', ['class'=>'col-sm-2 control-label']); ?>


        <div class="col-sm-10">

            <?php echo Form::text('nombre_actividad', null, ['class'=>'form-control', 'placeholder'=>'Nombre de la actividad...']); ?>


        </div>

      </div>

        

      <div class="form-group">

        <?php echo Form::label('descripcion', 'Descripcion de la actividad', ['class'=>'col-sm-2 control-label']); ?>


        <div class="col-sm-10">

            <?php echo Form::text('descripcion_actividad', null, ['class'=>'form-control', 'placeholder'=>'Descripcion de la actividad...']); ?>


        </div>

      </div>

       

      <div class="form-group">

        <?php echo Form::label('fecha', 'Fecha de entrega', ['class'=>'col-sm-2 control-label']); ?>


        <div class="col-sm-10">

            <div class="input-group calendario">

                <?php echo Form::text('fecha_entrega', null, ['class'=>'form-control', 'placeholder'=>'Fecha de entrega tarea...']); ?>


                <span class="input-group-addon">

                  <i class="fa fa-calendar"></i>

                </span>

            </div>

        </div>

      </div>

     

      <div class="form-group">

        <?php echo Form::label('nota total', 'Nota total de tarea*', ['class'=>'col-sm-2 control-label']); ?>


        <div class="col-sm-10">

          <?php echo Form::number('nota_total', null, ['class'=>'form-control', 'placeholder'=>'Valor de la tarea']); ?>


        </div>

      </div>

       

      <div class="form-group">

        <div class="col-sm-offset-2 col-sm-10">

          <button type="submit" name="guardar" class="btn btn-success"><span class="fa fa-save"></span> Registrar</button>

        </div>

      </div>



