<?php $__env->startSection('titulo'); ?>

  <title>Nueva Inscripción</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">



      

        <h1 class="page-header text-center"> <div class="fa fa-plus" aria-hidden="true"> </div><div class="fa fa-users" aria-hidden="true"></div>INSCRIPCION DE ESTUDIANTES</h1>

        

      </div>

        

        <p>

          NOTA: Todos los campos con (*) son requeridos.

        </p>

        <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      </div>

      <div class="col-sm-12">

        <?php echo Form::open(['route'=>'inscripcionestudiantes.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'inscripcionestudiante']); ?>


        <center>

       

        </center>



        <div class="tab-content panel panel-default">

        <div id="home" class="tab-pane fade in active">

          <h3>Grado Inscripción</h3>

          <?php echo $__env->make('inscripciones.campos.grado', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>

        <div id="menu1" class="tab-pane fade">

          <h3>Datos Estudiante</h3>

          <?php echo $__env->make('inscripciones.campos.estudiantes', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>

        <div id="menu2" class="tab-pane fade">

          <h3>Datos Tutores</h3>

          <?php echo $__env->make('inscripciones.campos.tutores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 

        </div>

        <div id="menu3" class="tab-pane fade">

          <h3>Datos Referencias</h3>

          <?php echo $__env->make('inscripciones.campos.referencias', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>

        <div id="menu4" class="tab-pane fade">

          <h3>Registrar Estudiante</h3>

          <button type="submit" name="registrar" class="btn btn-success btn-lg"><i class="fa fa-save"></i> Registrar</button>


        </div>
        <br/>
       

        


           <ul class="nav nav-pills">
           

        <li class="active"><?php echo link_to(URL::previous(), 'CANCELAR', ['class' => 'btn btn-info fa fa-times']); ?> <i class="fa"></i></li>

        <li class="active"><a data-toggle="tab" href="#home">1. Grado <i class="fa fa-bookmark-o"></i></a></li>

        <li><a data-toggle="tab" href="#menu1">2. Estudiante <i class="fa fa-child"></i></a></li>

        <li><a data-toggle="tab" href="#menu2">3. Tutores <i class="fa fa-eye"></i></a></li>

        <li><a data-toggle="tab" href="#menu3">4. Referencias <i class="fa fa-male"></i></a></li>

        <li><a data-toggle="tab" href="#menu4">5. Registrar <i class="fa fa-save"></i></a></li>

        </ul>
        </center>
        </div>

        </div>






        <?php echo Form::close(); ?>


      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>