<?php $__env->startSection('titulo'); ?>
  <title>Planes Niveles</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
         <div class="panel-body ">
        <h1 class="page-header text-center"> <div class="fa fa-pencil-square" aria-hidden="true"></div>Planes Niveles</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> ASIGNA </strong> a un nivel un plan y una jornada ejemplo:<strong>*NIVEL: PRIMARIA, PLAN: DIARIO, JORNADA: MATUTINA</strong>
            </h4>
            
          </center>
        </div>
        <h6>
         <CENTER>
            * ESTOS DEBERAN DE EXISTIR PARA PODER SER ADMINISTRADOS
          </CENTER>
          </h6>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
         <div class="col-sm-12 panel panel-default">
          <br/>
           <center>
            <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

            <?php if (\Entrust::can('crear-plannivel')) : ?>
            <?php echo link_to_route('asignarniveles.create', $title = 'Nuevo Plan - Nivel', $parameters = null, $attributes =['class'=>'btn btn-primary fa fa-plus-square']); ?>

          <br>
          <br>
        <?php endif; // Entrust::can ?>
        <?php echo $__env->make('nivelesplanesjornadas.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php if(count($datos) == 0): ?>
          <p class="text-info">
            No se han registrado asignacioónes de de niveles a planes y jornadas.
          </p>
          </center>
          </div>
        <?php else: ?>
          <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NIVEL
                </th>
                <th>
                  PLAN
                </th>
                <th>
                  JORNADA
                </th>
                <?php if (\Entrust::can('editar-plannivel')) : ?>
                <th>
                  ACTUALIZAR
                </th>
                <?php endif; // Entrust::can ?>
                <?php if (\Entrust::can('editar-plannivel')) : ?>
                <th>
                  ESTADO 
                </th>
                <?php endif; // Entrust::can ?>
              </tr>
            </thead>
            <tbody id="datosAsignacion">
              <?php foreach($datos as $key => $row): ?>
                <tr>
                  <td>
                    <?php echo e($key+1); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($row->nombre_nivel)); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($row->nombre_plan)); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($row->nombre_jornada)); ?>

                  </td>
                  <?php if (\Entrust::can('editar-plannivel')) : ?>
                  <td>
                    <?php echo link_to_route('asignarniveles.edit', $title = 'Editar', $parameters = $row->id_nivel_plan_jornada, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>

                  </td>
                  <td>
                   <?php if($row->estado_niveles_planes_jornadas == TRUE): ?>
                              <input type="checkbox" name="estado" checked value="<?php echo e($row->id_nivel_plan_jornada); ?>" class="toggleEstado">
                            <?php else: ?>
                              <input type="checkbox" name="estado" value="<?php echo e($row->id_nivel_plan_jornada); ?>" class="toggleEstado">
                            <?php endif; ?>
                  <?php endif; // Entrust::can ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>

          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">

          <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          <div class="text-center">

            <?php echo $datos->links(); ?>


          </div>

        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>