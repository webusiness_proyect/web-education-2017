<?php $__env->startSection('titulo'); ?>
  <title>Cursos</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Cursos</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <?php if (\Entrust::can('crear-curso')) : ?>
        <?php echo link_to_route('areas.create', $title = 'Nuevo Curso', $parameters = null, $attributes = ['class'=>'btn btn-primary']); ?>
        <br>
        <br>
        <?php endif; // Entrust::can ?>
        <?php if(count($areas) == 0): ?>
          <p class="text-info">
            No se han registrado cursos aun.
          </p>
        <?php else: ?>
          <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NOMBRE
                </th>
                <?php if (\Entrust::can('editar-curso')) : ?>
                <th>
                  ACTUALIZAR
                </th>
                <?php endif; // Entrust::can ?>
                <?php if (\Entrust::can('estado-curso')) : ?>
                <th>
                  ESTADO
                </th>
                <?php endif; // Entrust::can ?>
              </tr>
            </thead>
            <tbody id="datosAreas">
              <?php foreach($areas as $key => $area): ?>
                <tr>
                  <td>
                    <?php echo e($key+1); ?>
                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($area->nombre_area)); ?>
                  </td>
                  <?php if (\Entrust::can('editar-curso')) : ?>
                  <td>
                    <?php echo link_to_route('areas.edit', $title = 'Editar', $parameters = $area->id_area, $attributes = ['class'=>'btn btn-primary']); ?>
                  </td>
                  <?php endif; // Entrust::can ?>
                  <?php if (\Entrust::can('estado-curso')) : ?>
                  <td>
                    <?php if($area->estado_area == true): ?>
                      <?php echo link_to_route('areas.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarArea', 'data-id'=>$area->id_area, 'data-estado'=>$area->estado_area]); ?>
                    <?php else: ?>
                      <?php echo link_to_route('areas.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarArea', 'data-id'=>$area->id_area, 'data-estado'=>$area->estado_area]); ?>
                    <?php endif; ?>
                  </td>
                  <?php endif; // Entrust::can ?>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
          <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <div class="text-center">
            <?php echo $areas->links(); ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>