<?php $__env->startSection('titulo'); ?>

  <title>Mis alumnos</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

      <di <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-user" aria-hidden="true"></div> Alumnos a mi cargo </h1>
        <h4 class="text-center"><div class="glyphicon glyphicon-cog"></div>   </h4>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>

       
        <div class="col-sm-12 panel panel-default">

          <br/>



        <div class="table-responsive">

          <table class="table table-hover table-bordered">

            <thead>

              <tr>

               <th>

                 <h4> CODIGO ALUMNO</h4>

                </th>

                <th>

                 <h4> NOMBRE ALUMNO </h4>

                </th>

                <th>

                  <h4>APELLIDO ALUMNO</h4>

                </th>

               

                <th>

                  <h4>GRADO</h4>

                </th>

                <th>

                 <h4> NIVEL</h4>

                </th>

                <th>

                  <h4>JORNADA</h4>

                </th>

                <th>

                  <h4>PLAN</h4>

                </th>
                <th>

                  <h4>CARRERA</h4>

                </th>
                <th>

                  <h4>SECCION</h4>

                </th>                
                
                 <th>

                 <h4> CUOTA DE INSCRIPCION</h4>

                </th>

                <th>

                  <h4>CUOTA MENSUAL</h4>

                </th>

              </tr>

            </thead>

            <tbody>

              <?php foreach($alumnos as $key => $alumno): ?>

                <tr>

                  
                   <td>

                    <b><?php echo e(mb_strtoupper($alumno->id_estudiante)); ?></b>

                  </td>

                  <td>

                    <b><?php echo e(mb_strtoupper($alumno->nombre_estudiante)); ?></b>

                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($alumno->apellidos_estudiante)); ?>


                  </td>

                 

                  <td>

                    <?php echo e(mb_strtoupper($alumno->nombre_grado)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($alumno->nombre_nivel)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($alumno->nombre_jornada)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($alumno->nombre_plan)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($alumno->nombre_carrera)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($alumno->nombre_seccion)); ?>


                  </td>

                   <td>

                    <?php echo e(mb_strtoupper($alumno->cuota_inscripcion)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($alumno->cuota_mensualidad)); ?>


                  </td>

                 

                </tr>

              <?php endforeach; ?>

            </tbody>

          </table>

        </div>

      </div>

      <div class="col-sm-12 text-center">

     
      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>