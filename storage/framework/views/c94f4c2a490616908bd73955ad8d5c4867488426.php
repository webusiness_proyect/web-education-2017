<?php $__env->startSection('titulo'); ?>
  <title>Editar Unidad</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Editar Unidad</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
      </div>
      <div class="col-sm-12">
        <?php echo Form::model($unidad,['route'=>['unidades.update', $unidad->id_unidad], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'unidades']); ?>
          <?php echo $__env->make('unidades.form.campos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo Form::close(); ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>