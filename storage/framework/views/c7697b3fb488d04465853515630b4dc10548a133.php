<?php $__env->startSection('titulo'); ?>

  <title>Actividades</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center"><div class="fa fa-eye" aria-hidden="true"></div><div class="fa fa-pencil-square" aria-hidden="true"></div>CALIFICA TUS TAREAS </h1>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      </div>

      <div class="col-sm-12 panel panel-default">
        <br/>
         <center>   
           <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>


        <p>

          

        </p>

        <?php if(count($ractividades) == 0): ?>

          <p class="text-info">
        


            No se han encontrado actividades para este curso...
            </center>

          </p>

        <?php else: ?>



      
<div class="container">
 
  <center><a href="#demo2" class="btn btn-success" data-toggle="collapse"><div class="fa fa-plus"></div> <div class="fa fa-check-circle"></div>TAREAS DE ESTA UNIDAD ENTREGADAS</a></center>
  <div id="demo2" class="collapse in">

    <div class="row">

      <div class="col-sm-10">

               



          <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>
                  <th>

                    <H5><STRONG>ESTADO </STRONG></H5>

                  </th>

                  <th>

                   <H5><STRONG>ALUMNO </STRONG></H5>

                  </th>

                  

                  <th id="nota">

                   <H5><STRONG> CALIFICACION </STRONG></H5>

                  </th>

                 




                  <th>

                   <H5><STRONG> FECHA ENTREGA </STRONG></H5>

                  </th>

                  <th>

                   <H5><STRONG> COMENTARIOS </STRONG></H5>

                  </th>

                   <th>

                    <H5><STRONG>ARCHIVO </STRONG></H5>

                  </th>
                   <th >

                    <H5><STRONG> OPCIONES </STRONG></H5>

                  </th>
                 

                 

                </tr>

              </thead>

              <tbody>

                <?php foreach($ractividades as $key=>$actividad): ?>

                <?php foreach($notas as $key=>$maximanota): ?>

                  <tr>

                    

                        
                      <?php if($actividad->estado_entrega_actividad=="CALIFICADO"): ?>
                      <td class="btn-success">  <li class="fa fa-check-circle"> CALIFICADO </li></td>
                      <?php else: ?>
                      <td class="btn-danger">  <li class="fa fa-info-circle"> NO CALIFICADA </li></td>
                      <?php endif; ?>

                    

                  <td>
                    <H5> <?php echo e(mb_strtoupper($actividad->apellidos_estudiante)); ?>

                    <?php echo e(mb_strtoupper($actividad->nombre_estudiante)); ?> </H5>

                  </td>

                  


                   


                  <td>

                    <H5><?php echo e($nota=mb_strtoupper($actividad->calificacion)); ?></H5>


                  

                  







                  <td>

                     <H5><?php echo e(mb_strtoupper($actividad->fecha_entrega)); ?></H5>

                  </td>

                  <td>

                     <H5><?php echo e(mb_strtoupper($actividad->comentarios)); ?></H5>

                  </td>

                  <td>

                    
                     <H5><?php echo Html::link($actividad->url, 'DESCARGAR ', $attributes = ['class'=>'btn btn-success  fa fa-download']); ?></H5>
                  </td>
                  

                    <td>



                      <H5> <?php echo link_to_route('misrespuestasactividades.edit', $title = 'CALIFICAR',  $parameters = ['b'=>$actividad->id_respuesta_actividad, 'a'=>$curso], $attributes = ['class'=>'btn btn-default  fa fa-check-square']); ?></H5>

                    </td>

                    <?php echo e(Form::hidden('post_id', $nota_total=($maximanota->nota_total))); ?>

                    

                  </td>

                  <?php if($nota<=$nota_total): ?>

                   <?php else: ?>
                  <td class="btn-warning">

                    NOTA MAXIMA<li class="fa fa-exclamation-triangle"><?php echo e($nota_total=mb_strtoupper($maximanota->nota_total)); ?></li>

                  </td>
                  <?php endif; ?>

                    

                        <?php if($nota>$nota_total): ?>


                        <td class="btn-danger"> <li class=" fa fa-remove">NOTA MALA</li> </td>

                        <?php else: ?>

                        

                        <?php endif; ?>



                   
                  </tr>

                <?php endforeach; ?>

                <?php endforeach; ?>


              </tbody>

            </table>

               

           

          </div>

    
      
      </div>

    </div>

  </div>

      
<div class="container">
 
  <center><a href="#demo" class="btn btn-danger" data-toggle="collapse"><div class="fa fa-minus"></div>  <div class="fa fa-exclamation-triangle"> </div>TAREAS NO ENTREGADAS</a></center>
  <div id="demo" class="collapse">

    <div class="row">

      <div class="col-sm-10">

          <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>
                  <th>

                    <H5><STRONG>ESTADO </STRONG></H5>

                  </th>

                  <th>

                   <H5><STRONG>ALUMNO </STRONG></H5>

                  </th>

                  

                   <th>

                   <H5><STRONG>OPCIONES</STRONG></H5>

                  </th>
                  
                 

                 

                </tr>

              </thead>

              <tbody>

                <?php foreach($sinentrega as $key=>$actividad): ?>



                  <tr>

                    

                      <center>
                      <td class="btn-danger"> <H5><strong>  <div class="fa fa-exclamation-triangle"> </div>NO ENTREGADA </strong></H5></td>
                      </center>

                    

                  <td>
                    <H5>

                       <?php echo e($nota=mb_strtoupper($actividad->apellidos_estudiante)); ?>


                      <?php echo e(mb_strtoupper($actividad->nombre_estudiante)); ?>


                 

                   </H5>


                  

                  




                  

                    <td>



                      <H5><?php echo link_to_route('misrespuestasactividades.edit', $title = 'NOTIFICAR',  $parameters = ['a'=>$actividad->id], $attributes = ['class'=>'btn btn-danger  fa fa-info-circle']); ?> <?php echo link_to_route('misrespuestasactividades.create', $title = 'AGREGAR NOTA',  $parameters = ['a'=>$actividad->id], $attributes = ['class'=>'btn btn-default  fa fa-check-square']); ?></H5>

                    </td>

               </tr>

                <?php endforeach; ?>



              </tbody>

            </table>
          </div>
        </div>
        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
       </div>
       
        </div>

    <br/>


<div class="container">
 
  <center><a href="#demo2" class="btn btn-default" data-toggle="collapse"><div class="fa fa-plus"></div> <div class="fa fa-check-circle"></div>... TAREAS DE UNIDADES ANTERIORES ...</a></center>
  <div id="demo2" class="collapse">

    <div class="row">

      <div class="col-sm-10">

               



          <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>
                  <th>

                    <H5><STRONG>ESTADO </STRONG></H5>

                  </th>

                  <th>

                   <H5><STRONG>ALUMNO </STRONG></H5>

                  </th>

                  

                  <th id="nota">

                   <H5><STRONG> CALIFICACION </STRONG></H5>

                  </th>

                 




                  <th>

                   <H5><STRONG> FECHA ENTREGA </STRONG></H5>

                  </th>

                  <th>

                   <H5><STRONG> COMENTARIOS </STRONG></H5>

                  </th>

                   <th>

                    <H5><STRONG>ARCHIVO </STRONG></H5>

                  </th>
                   <th >

                    <H5><STRONG> OPCIONES </STRONG></H5>

                  </th>
                 

                 

                </tr>

              </thead>

              <tbody>

                <?php foreach($ractividades as $key=>$actividad): ?>

                <?php foreach($notas as $key=>$maximanota): ?>

                  <tr>

                    

                        
                      <?php if($actividad->estado_entrega_actividad=="CALIFICADO"): ?>
                      <td class="btn-success">  <li class="fa fa-check-circle"> CALIFICADO </li></td>
                      <?php else: ?>
                      <td class="btn-danger">  <li class="fa fa-info-circle"> NO CALIFICADA </li></td>
                      <?php endif; ?>

                    

                  <td>
                    <H5> <?php echo e(mb_strtoupper($actividad->apellidos_estudiante)); ?>

                    <?php echo e(mb_strtoupper($actividad->nombre_estudiante)); ?> </H5>

                  </td>

                  


                   


                  <td>

                    <H5><?php echo e($nota=mb_strtoupper($actividad->calificacion)); ?></H5>


                  

                  







                  <td>

                     <H5><?php echo e(mb_strtoupper($actividad->fecha_entrega)); ?></H5>

                  </td>

                  <td>

                     <H5><?php echo e(mb_strtoupper($actividad->comentarios)); ?></H5>

                  </td>

                  <td>

                    
                     <H5><?php echo Html::link($actividad->url, 'DESCARGAR ', $attributes = ['class'=>'btn btn-success  fa fa-download']); ?></H5>
                  </td>
                  

                    <td>



                      <H5> <?php echo link_to_route('misrespuestasactividades.edit', $title = 'CALIFICAR',  $parameters = ['b'=>$actividad->id_respuesta_actividad, 'a'=>$curso], $attributes = ['class'=>'btn btn-default  fa fa-check-square']); ?></H5>

                    </td>

                    <?php echo e(Form::hidden('post_id', $nota_total=($maximanota->nota_total))); ?>

                    

                  </td>

                  <?php if($nota<=$nota_total): ?>

                   <?php else: ?>
                  <td class="btn-warning">

                    NOTA MAXIMA<li class="fa fa-exclamation-triangle"><?php echo e($nota_total=mb_strtoupper($maximanota->nota_total)); ?></li>

                  </td>
                  <?php endif; ?>

                    

                        <?php if($nota>$nota_total): ?>


                        <td class="btn-danger"> <li class=" fa fa-remove">NOTA MALA</li> </td>

                        <?php else: ?>

                        

                        <?php endif; ?>



                   
                  </tr>

                <?php endforeach; ?>

                <?php endforeach; ?>


              </tbody>

            </table>

               

           

          </div>

    
      
      </div>
      <?php endif; ?> 

    </div>

  </div>

   </div>
</br>

<?php $__env->stopSection(); ?>



<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>