
      

       
      
        
      <div class="form-group">
        <?php echo Form::label('descripcion', 'Comentarios sobre la tarea:', ['class'=>'col-sm-2 control-label']); ?>

        <div class="col-sm-10">
            <?php echo Form::text('comentarios', null, ['class'=>'form-control', 'placeholder'=>'Descripcion de la actividad...']); ?>

        </div>
      </div>
       
     
     
      <div class="form-group">
        <?php echo Form::label('Selecciona tu tarea', 'Selecciona tu entrega', ['class'=>'col-sm-2 control-label']); ?>

        <div class="col-sm-10">
          <?php echo Form::file('ruta'); ?>

        </div>
      </div>
       
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
          <button type="submit" name="guardar" class="btn btn-success"><span class="fa fa-save"></span> Agrega Entrega</button>
        </div>
      </div>
      <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

