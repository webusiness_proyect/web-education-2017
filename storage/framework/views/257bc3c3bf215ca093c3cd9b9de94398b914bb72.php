<?php $__env->startSection('titulo'); ?>
  <title>tutores</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">



<div class="panel-body ">
        <h1 class="page-header text-center"> <div class="fa fa-users" aria-hidden="true"></div>TUTORES O PADRES DE FAMILIA</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> ADMINISTRA </strong> y modifica datos de padres de estudiantes en su institucion
            </h4>

          
           
          </center>
        </div>
        
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

         <?php if (\Entrust::can('crear-alumno')) : ?>

        <?php echo link_to_route('inscripcionestudiantes.create', $title = 'Nueva Inscripción', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus']); ?>

        <?php echo $__env->make('tutores.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php endif; // Entrust::can ?>



         <?php if(count($tutores) == 0): ?>
          <p class="text-info">
            No se han registrado tutores aun...
          </p>

        </center>




        
      <?php else: ?>
      <div class="table-responsive panel panel-default">

        <table class="table table-hover">
            <thead>
              <tr>
             
                <th>
                  PADRE
                </th>
                <th>
                  CORREO 
                </th>
                <th>
                  TELEFONO 
                </th>
                <th>
                  ENCARGADO DE 
                 
                </th>

                <?php if (\Entrust::can('editar-tutores')) : ?>
                <th>
                  OPCIONES
                </th>
              
                <th>
                  ESTADO
                </th>
                <?php endif; // Entrust::can ?>
              </tr>
            </thead>
            <tbody id="datosEstudiantes">
              <?php foreach($tutores as $key => $tutor): ?>
                <tr>
                 
                  <td>

                  <?php echo e(mb_strtoupper($tutor->apellidos_tutor)); ?>


                  <?php echo e(mb_strtoupper($tutor->nombre_tutor)); ?>


                    
                  </td>
                   <td>
                    <?php echo e(mb_strtoupper($tutor->correo_electronico_tutor)); ?>

                  </td>
                   <td>
                    <?php echo e(mb_strtoupper($tutor->telefono_primario_tutor)); ?>

                  </td>
                   <td>
                    <?php echo e(mb_strtoupper($tutor->nombre_estudiante)); ?>


                    <?php echo e(mb_strtoupper($tutor->apellidos_estudiante)); ?>

                  </td>
                  
                  <?php if (\Entrust::can('editar-tutores')) : ?>
                  <td>
                    <?php echo link_to_route('tutores.edit', $title = 'Editar ', $parameters = $tutor->id_tutor, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>

                    
                  </td>
                 
                  <td>
  
                     <?php if($tutor->estado_tutor == TRUE): ?>
                              <input type="checkbox" name="estado" checked value="<?php echo e($tutor->id_tutor); ?>" class="toggleEstado">
                            <?php else: ?>
                              <input type="checkbox" name="estado" value="<?php echo e($tutor->id_tutor); ?>" class="toggleEstado">
                            <?php endif; ?>
                  </td>
                  <?php endif; // Entrust::can ?>
                </tr>
              <?php endforeach; ?>
            </tbody>


          </table>
          </div>
           <div class="text-center">
            <?php echo $tutores->links(); ?>

          </div>
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
          <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <?php endif; ?>
      
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>