<?php $__env->startSection('titulo'); ?>

  <title>  Ayuda</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">
        <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-pencil-square" aria-hidden="true"></div>HELP DESK</h1>
           <div class="panel panel-default">
            <div class="row">
            <div class="col-sm-10">
           
            <center><a href="#demo"  data-toggle="collapse"><div class="fa fa-question-circle fa-2x "></div><H4><STRONG>AYUDA</STRONG></H4></a></center>
            <div id="demo" class="collapse">
             
          <center>
            <h4>
              <strong> ¿QUE ES UN TIKET? </strong> Un ticket se crea cuando tienes un problema con la plataforma, por ejemplo no puedes visualizar algun contenido, no tienes permiso para acceder a algo, o simplemente deseas comunicarte con el administrador, si tardamos mucho en responder a tu correo electronico llamanos   <a href="<?php echo e(url('/tel:+5022327806')); ?>"><i class="fa fa-phone"></i>Tel. +50223278063</a> con gusto responderemos cualquier duda
            </h4>
          </center>
        </div>
        </div>
        </div>
        </div>
    

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>
        <div class="col-sm-12 panel panel-default">

          <br/>
           <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>


         

        <?php echo link_to_route('helpdesk.create', $title = 'Nuevo Ticket', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square']); ?>



        <?php if(count($mensajes) == 0): ?>


          <p class="text-info">

            No hay solicitudes de soporte actualmente.

          </p>
        </center>



        <?php else: ?>

        
           

        <br/>

  
    
          <table class="table table-hover table-hover table- table-bordered">

            <thead>

              <tr>

                <th>

                  No. Solicitud

                </th>

                <th>

                  ASUNTO

                </th>

                

                <th>

                  MENSAJE

                </th>

                


                <th>

                  ESTADO

                </th>


                <th>

                  TIPO MENSAJE

                </th>
               
                <th>

                  PARA

                </th>

                <th>

                  FECHA 

                </th>

              </tr>

            </thead>

            <tbody id="datoshelpdesk">

              <?php foreach($mensajes as $key => $area): ?>

                <tr>

                 

                  <td>

                    <?php echo e(mb_strtoupper($area->id_mensaje)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($area->asunto)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($area->mensaje)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($area->estado)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($area->tipo_mensaje)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($area->name)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($area->crated_at)); ?>


                  </td>

                </tr>

              <?php endforeach; ?>

            </tbody>

          </table>

          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">

          <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          <div class="text-center">

            <?php echo $mensajes->links(); ?>


          </div>

        <?php endif; ?>

      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>