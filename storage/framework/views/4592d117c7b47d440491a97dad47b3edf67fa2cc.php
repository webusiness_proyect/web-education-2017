<?php $__env->startSection('titulo'); ?>

  <title>Secciones</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center">Secciones</h1>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      </div>

      <div class="col-sm-12">

        <?php if (\Entrust::can('crear-seccion')) : ?>

        <?php echo link_to_route('secciones.create', $title = 'Nueva sección', $parameters = null, $attributes = ['class'=>'btn btn-primary']); ?>


        <br>

        <br>

        <?php endif; // Entrust::can ?>

        <?php if(count($secciones) == 0): ?>

          <p class="text-info">

            No se han registrado secciones aun.

          </p>

        <?php else: ?>

          <table class="table table-hover">

            <thead>

              <tr>

                <th>

                  NO

                </th>

                <th>

                  NOMBRE SECCION

                </th>

                <?php if (\Entrust::can('editar-seccion')) : ?>

                <th>

                  ACTUALIZAR

                </th>

                <?php endif; // Entrust::can ?>

              </tr>

            </thead>

            <tbody>

              <?php foreach($secciones as $key => $seccion): ?>

                <tr>

                  <td>

                    <?php echo e($key+1); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($seccion->nombre_seccion)); ?>


                  </td>

                  <?php if (\Entrust::can('editar-seccion')) : ?>

                  <td>

                    <?php echo link_to_route('secciones.edit', $title ='Editar', $parameters = $seccion->id_seccion, $attributes = ['class'=>'btn btn-primary']); ?>


                  </td>

                  <?php endif; // Entrust::can ?>

                </tr>

              <?php endforeach; ?>

            </tbody>

          </table>

        <?php endif; ?>

      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>