<?php $__env->startSection('titulo'); ?>
  <title>Horarios</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel-body ">
        <h1 class="page-header text-center"> <div class="fa fa-bookmark-o" aria-hidden="true"></div> - MIS HORARIOS</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <br/>
        <div class="form-group">

       

        </div>
      </div>
      </div>
     <div class="col-sm-12 panel panel-default">
        <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>


        <br/>
          
        <?php if(count($horarios) == 0): ?>
          <p class="text-info">
            No se han registrado horarios aun.
          </p>

          </center>
        <?php else: ?>

        <div class="col-sm-2 panel panel-default" >
          <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                

                <th>
                  LUNES
                </th>
              
              </tr>
            </thead>
            <tbody id="datosGrados">
             
              <?php foreach($horarios as $key => $horario): ?>
                <tr>
                
                  
                   

                  <td>
                      <h6>
                     <b>INICIA:</b><?php echo e(mb_strtoupper($horario->inicio)); ?>

                     <b>FIN:</b><?php echo e(mb_strtoupper($horario->fin)); ?><br/>
                     <b>CURSO:<?php echo e(mb_strtoupper($horario->nombre_area)); ?></b>
                     
                     
                     <b>GRADO:</b><?php echo e(mb_strtoupper($horario->nombre_grado)); ?>

                     <?php echo e(mb_strtoupper($horario->nombre_carrera)); ?>

                   
                       <?php echo e(mb_strtoupper($horario->nombre_jornada)); ?>

                     <?php echo e(mb_strtoupper($horario->nombre_plan)); ?>

                     <?php echo e(mb_strtoupper($horario->nombre_nivel)); ?>

                     
                    </h6>
                    
                     
                     <?php if (\Entrust::can('crear-horario')) : ?>
                     <?php echo link_to_route('horarios.edit', $title = 'Editar', $parameters = $horario->idcalendario, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>

                     <?php endif; // Entrust::can ?>

                  </td>
                </tr>
                <?php endforeach; ?>
             
            
            </tbody>
          </table>
        </div>
        <div class="col-sm-2 panel panel-default" >

          <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                  MARTES
                </th>
              </tr>
            </thead>
            <tbody id="datosGrados">
               <?php foreach($martes as $key => $martes): ?>
                <tr>
                  <td>
                    <h6>
                     <b>INICIA:</b><?php echo e(mb_strtoupper($martes->inicio)); ?>

                     <b>FIN:</b><?php echo e(mb_strtoupper($martes->fin)); ?><br/>
                     <b>CURSO:<?php echo e(mb_strtoupper($martes->nombre_area)); ?></b>
                     
                     
                     <b>GRADO:</b><?php echo e(mb_strtoupper($martes->nombre_grado)); ?>

                     <?php echo e(mb_strtoupper($martes->nombre_carrera)); ?>

                   
                       <?php echo e(mb_strtoupper($martes->nombre_jornada)); ?>

                     <?php echo e(mb_strtoupper($martes->nombre_plan)); ?>

                     <?php echo e(mb_strtoupper($martes->nombre_nivel)); ?>

                     
                    </h6>
                    
                     
                     <?php if (\Entrust::can('crear-horario')) : ?>
                     <?php echo link_to_route('horarios.edit', $title = 'Editar', $parameters = $martes->idcalendario, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>

                     <?php endif; // Entrust::can ?>

                  </td>
                 
                </tr>
                <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        

        <div class="col-sm-2 panel panel-default">

        <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                  MIERCOLES
                </th>
              </tr>
            </thead>
            <tbody id="datosGrados">
               <?php foreach($miercoles as $key => $martes): ?>
                <tr>
                  <td>
                      <h6>
                     <b>INICIA:</b><?php echo e(mb_strtoupper($martes->inicio)); ?>

                     <b>FIN:</b><?php echo e(mb_strtoupper($martes->fin)); ?><br/>
                     <b>CURSO:<?php echo e(mb_strtoupper($martes->nombre_area)); ?></b>
                     
                     
                     <b>GRADO:</b><?php echo e(mb_strtoupper($martes->nombre_grado)); ?>

                     <?php echo e(mb_strtoupper($martes->nombre_carrera)); ?>

                   
                       <?php echo e(mb_strtoupper($martes->nombre_jornada)); ?>

                     <?php echo e(mb_strtoupper($martes->nombre_plan)); ?>

                     <?php echo e(mb_strtoupper($martes->nombre_nivel)); ?>

                     
                    </h6>
                    
                     
                     <?php if (\Entrust::can('crear-horario')) : ?>
                     <?php echo link_to_route('horarios.edit', $title = 'Editar', $parameters = $martes->idcalendario, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>

                     <?php endif; // Entrust::can ?>

                  </td>
              </tr>
                   <?php endforeach; ?>

            </tbody>
          </table>
        </div>

        <div class="col-sm-2 panel panel-default" >

                    <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                  JUEVES
                </th>
              </tr>
            </thead>
            <tbody id="datosGrados">
             
               <?php foreach($jueves as $key => $martes): ?>
               <tr>
                  <td>
                      <h6>
                     <b>INICIA:</b><?php echo e(mb_strtoupper($martes->inicio)); ?>

                     <b>FIN:</b><?php echo e(mb_strtoupper($martes->fin)); ?><br/>
                     <b>CURSO:<?php echo e(mb_strtoupper($martes->nombre_area)); ?></b>
                     
                     
                     <b>GRADO:</b><?php echo e(mb_strtoupper($martes->nombre_grado)); ?>

                     <?php echo e(mb_strtoupper($martes->nombre_carrera)); ?>

                   
                       <?php echo e(mb_strtoupper($martes->nombre_jornada)); ?>

                     <?php echo e(mb_strtoupper($martes->nombre_plan)); ?>

                     <?php echo e(mb_strtoupper($martes->nombre_nivel)); ?>

                     
                    </h6>
                    
                     
                     <?php if (\Entrust::can('crear-horario')) : ?>
                     <?php echo link_to_route('horarios.edit', $title = 'Editar', $parameters = $martes->idcalendario, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>

                     <?php endif; // Entrust::can ?>

                  </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
          </table>
        </div>

        <div class="col-sm-2 panel panel-default">

        <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                  VIERNES
                </th>
              </tr>
            </thead>
            <tbody id="datosGrados">
               <?php foreach($viernes as $key => $martes): ?>
               <tr>
                  <td>
                      <h6>
                     <b>INICIA:</b><?php echo e(mb_strtoupper($martes->inicio)); ?>

                     <b>FIN:</b><?php echo e(mb_strtoupper($martes->fin)); ?><br/>
                     <b>CURSO:<?php echo e(mb_strtoupper($martes->nombre_area)); ?></b>
                     <b>GRADO:</b><?php echo e(mb_strtoupper($martes->nombre_grado)); ?>

                     <?php echo e(mb_strtoupper($martes->nombre_carrera)); ?>

                   
                       <?php echo e(mb_strtoupper($martes->nombre_jornada)); ?>

                     <?php echo e(mb_strtoupper($martes->nombre_plan)); ?>

                     <?php echo e(mb_strtoupper($martes->nombre_nivel)); ?>

                    </h6>
                     <?php if (\Entrust::can('crear-horario')) : ?>
                     <?php echo link_to_route('horarios.edit', $title = 'Editar', $parameters = $martes->idcalendario, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>

                     <?php endif; // Entrust::can ?>
                  </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
          </table>
        </div>
         <div class="col-sm-2 panel panel-default">

          <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                  SABADO
                </th>
              </tr>
            </thead>
            <tbody id="datosGrados">
               <?php foreach($sabado as $key => $martes): ?>
               <tr>
                  <td>
                      <h6>
                     <b>INICIA:</b><?php echo e(mb_strtoupper($martes->inicio)); ?>

                     <b>FIN:</b><?php echo e(mb_strtoupper($martes->fin)); ?><br/>
                     <b>CURSO:<?php echo e(mb_strtoupper($martes->nombre_area)); ?></b>
                     
                     
                     <b>GRADO:</b><?php echo e(mb_strtoupper($martes->nombre_grado)); ?>

                     <?php echo e(mb_strtoupper($martes->nombre_carrera)); ?>

                   
                       <?php echo e(mb_strtoupper($martes->nombre_jornada)); ?>

                     <?php echo e(mb_strtoupper($martes->nombre_plan)); ?>

                     <?php echo e(mb_strtoupper($martes->nombre_nivel)); ?> 
                    </h6>
                     <?php if (\Entrust::can('crear-horario')) : ?>
                     <?php echo link_to_route('horarios.edit', $title = 'Editar', $parameters = $martes->idcalendario, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>

                     <?php endif; // Entrust::can ?>
                  </td>
              </tr>
              <?php endforeach; ?>
                
            </tbody>
          </table>
        </div>
        <div class="col-sm-2 panel panel-default">
          <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                  DOMINGO
                </th>
              </tr>
            </thead>
            <tbody id="datosGrados">
               <?php foreach($domingo as $key => $martes): ?>
               <tr>
                  <td>
                      <h6>
                     <b>INICIA:</b><?php echo e(mb_strtoupper($martes->inicio)); ?>

                     <b>FIN:</b><?php echo e(mb_strtoupper($martes->fin)); ?><br/>
                     <b>CURSO:<?php echo e(mb_strtoupper($martes->nombre_area)); ?></b>
                     <b>GRADO:</b><?php echo e(mb_strtoupper($martes->nombre_grado)); ?>

                     <?php echo e(mb_strtoupper($martes->nombre_carrera)); ?>

                    <?php echo e(mb_strtoupper($martes->nombre_jornada)); ?>

                     <?php echo e(mb_strtoupper($martes->nombre_plan)); ?>

                     <?php echo e(mb_strtoupper($martes->nombre_nivel)); ?>

                    </h6>
                     <?php if (\Entrust::can('crear-horario')) : ?>
                     <?php echo link_to_route('horarios.edit', $title = 'Editar', $parameters = $martes->idcalendario, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>

                     <?php endif; // Entrust::can ?>
                  </td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>

          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
          <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>