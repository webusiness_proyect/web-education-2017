<?php $__env->startSection('titulo'); ?>
  <title>Personas con rol</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Personas</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>

      <div class="col-sm-12">
        <?php echo Form::open(['route'=>'verusuario.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'index']); ?>

          <?php echo $__env->make('verusuario.form.campos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo Form::close(); ?>

        </br>
      </div>
      <div class="col-sm-12">
       
        <?php if(count($personas) == 0): ?>
          <p class="text-info">
            No se han registrado personas aun.
          </p>
        <?php else: ?>
          <div class="table-responsive">
            <table class="table table-hover table-hover table- table-bordered">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    NOMBRE
                  </th>
                  <th>
                    APELLIDO
                  </th>
                  <th>
                    TELEFONO
                  </th>
                  <th>
                    CORREO
                  </th>
                  <th>
                    PUESTO
                  </th>                  
                </tr>
              </thead>
              <tbody>
                <?php foreach($personas as $key => $usuario): ?>
                  <tr>
                    <td>
                      <?php echo e($total=$key+1); ?>

                    </td>
                    <td>
                      <?php echo e(mb_strtoupper($usuario->nombres_persona)); ?>

                    </td>
                     <td>
                      <?php echo e(mb_strtoupper($usuario->apellidos_persona)); ?>

                    </td>
                     <td>
                      <?php echo e(mb_strtoupper($usuario->telefono_persona)); ?>

                    </td>
                     <td>
                      <?php echo e(mb_strtoupper($usuario->correo_persona)); ?>

                    </td>
                    <td>
                      <?php echo e($usuario->nombre_puesto); ?>

                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            <h4 class="page-header text-center">TOTAL DE PERSONAS <?php echo e($total); ?></h4>
            
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>