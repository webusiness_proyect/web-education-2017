<?php $__env->startSection('titulo'); ?>


  <title>Nuevo Pensum</title>


<?php $__env->stopSection(); ?>





<?php $__env->startSection('cuerpo'); ?>


  <div id="page-wrapper">


    <div class="row">


      <div class="col-sm-12">

      <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-plus" aria-hidden="true"></div><div class="fa fa-bookmark" aria-hidden="true"></div> - Nuevo Pensum de grados</h1>

         <p>


          Nota: Todos los campos con (*) son obligatorios.


        </p>


        <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>


      </div>


      
       


      </div>


      <div class="col-sm-12">


        <ul class="nav nav-tabs">


          <li class="active"><a data-toggle="tab" href="#bgrados">Paso 1 - Agregar grados a jornada</a></li>


          <li><a data-toggle="tab" href="#acurso">Paso 2 - Agregar salon a cursos </a></li>


          <li><a data-toggle="tab" href="#gcambios">Paso 3 - Registrar Pensum</a></li>


        </ul>





            <?php echo Form::open(['route'=>'pensum.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'pensum']); ?>



              <?php echo $__env->make('asignacionareas.form.campos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>





            <?php echo Form::close(); ?>



          <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


      </div>


    </div>


  </div>


<?php $__env->stopSection(); ?>



<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>