

<?php $__env->startSection('titulo'); ?>
  <title>Editar Foro</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Editar Foro</h1>
        <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
      </div>
      <div class="col-sm-12">
        <?php echo Form::model($foro, ['route'=>['foros.update',$foro->id_foro], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'foros']); ?>
          <input type="hidden" name="curso" value="0">
          <?php echo $__env->make('foros.form.campos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo Form::close(); ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>