<?php $__env->startSection('titulo'); ?>

  <title>Actividades</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center">TAREAS RESPONDIDAS</h1>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      </div>

      <div class="col-sm-12">

        <p>

          

        </p>

        <?php if(count($ractividades) == 0): ?>

          <p class="text-info">

            No se han encontrado actividades para este curso...

          </p>

        <?php else: ?>

          <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>

                  <th>

                    NO

                  </th>

                  <th>

                    <div class="glyphicon glyphicon-user"></div>NOMBRE

                  </th>

                  <th>

                    ESTADO DE ENTREGA

                  </th>

                  <th id="nota">

                    CALIFICACION

                  </th>

                  <th  id="nota_max">

                    NOTA MAXIMA RECOMENDADA

                  </th>




                  <th>

                    FECHA ENTREGA

                  </th>

                  <th>

                    COMENTARIOS

                  </th>

                   <th>

                    ARCHIVO

                  </th>
                   <th >

                    NOTA

                  </th>
                 

                  <th >

                    ESTADO NOTA

                  </th>

                </tr>

              </thead>

              <tbody>

                <?php foreach($ractividades as $key=>$actividad): ?>

                <?php foreach($notas as $key=>$maximanota): ?>

                  <tr>

                    <td>

                        <H4><?php echo e($key+1); ?></H4>

                    </td>

                  <td>
                    <H4><?php echo e(mb_strtoupper($actividad->name)); ?></H4>

                  </td>

                  


                   
                      <?php if($actividad->estado_entrega_actividad=="CALIFICADO"): ?>
                      <td class="btn-success"> <h4> <li class="fa fa-check-circle"> CALIFICADO </h4></li></td>
                      <?php else: ?>
                      <td class="btn-danger"> <h4> <li class="fa fa-info-circle"> NO CALIFICADA </h4></li></td>
                      <?php endif; ?>


                  <td>

                    <H4><?php echo e($nota=mb_strtoupper($actividad->calificacion)); ?></H4>

                  </td>


                  <td class="btn-warning">

                    <H4><li class="fa fa-exclamation-triangle"><?php echo e($nota_total=mb_strtoupper($maximanota->nota_total)); ?></li></H4>

                  </td>







                  <td>

                    <H4><?php echo e(mb_strtoupper($actividad->fecha_entrega)); ?></H4>

                  </td>

                  <td>

                    <H4><?php echo e(mb_strtoupper($actividad->comentarios)); ?></H4>

                  </td>

                  <td>

                    <?php echo link_to_route('misrespuestasactividades.edit', $title = 'DESCARGAR',  $parameters = ['a'=>$actividad->id_respuesta_actividad], $attributes = ['class'=>'btn btn-success ']); ?>

                  </td>
                  

                    <td>

                      <?php echo link_to_route('misrespuestasactividades.edit', $title = 'CALIFICAR',  $parameters = ['a'=>$actividad->id_respuesta_actividad], $attributes = ['class'=>'btn btn-info ']); ?>


                    </td>
                    

                        <?php if($nota>$nota_total): ?>


                        <td class="btn-danger"> <li class=" fa fa-remove">NOTA MALA</li> </td>

                        <?php else: ?>

                        <td class="btn-success"> <li class=" fa fa-check">NOTA OK</li> </td>

                        <?php endif; ?>

                   
                  </tr>

                <?php endforeach; ?>

                <?php endforeach; ?>


              </tbody>

            </table>

               

             <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">

          </div>

        <?php endif; ?>
      
      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>