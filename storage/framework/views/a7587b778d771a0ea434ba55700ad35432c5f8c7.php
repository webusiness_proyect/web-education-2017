<div class="form-group">
  <?php echo Form::label('nombre', 'Nombre*', ['class'=>'col-sm-2 control-label']); ?>
  <div class="col-sm-10">
    <?php echo Form::text('nombre_unidad', null, ['class'=>'form-control', 'placeholder'=>'Nombre de la unidad..']); ?>
  </div>
</div>
<div class="form-group">
  <?php echo Form::label('fecha_inicio', 'Fecha Inicio*', ['class'=>'col-sm-2 control-label']); ?>
  <div class="col-sm-10">
    <div class="input-group calendario">
      <span class="input-group-addon">
          <i class="fa fa-calendar"></i>
      </span>
      <?php echo Form::text('fecha_inicio', null, ['class'=>'form-control', 'placeholder'=>'Fecha de inicio de la unidad']); ?>
    </div>
  </div>
</div>
<div class="form-group">
  <?php echo Form::label('fecha_final', 'Fecha Final*', ['class'=>'col-sm-2 control-label']); ?>
  <div class="col-sm-10">
    <div class="input-group calendario">
      <span class="input-group-addon">
          <i class="fa fa-calendar"></i>
      </span>
      <?php echo Form::text('fecha_final', null, ['class'=>'form-control', 'placeholder'=>'Fecha final de la unidad']); ?>
    </div>
  </div>
</div>
<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" name="button" class="btn btn-success"><span class="fa fa-save"></span> Guardar</button>
  </div>
</div>
