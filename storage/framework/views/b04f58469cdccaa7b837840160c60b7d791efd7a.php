<?php $__env->startSection('titulo'); ?>

  <title>Mis alumnos</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

      <di <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-user" aria-hidden="true"></div> Docentes de mis hijos </h1>
        <h4 class="text-center"><div class="glyphicon glyphicon-cog"></div>   </h4>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>

       
        <div class="col-sm-12 panel panel-default">

          <br/>



        <div class="table-responsive">

          <table class="table table-hover table-bordered">

            <thead>

              <tr>

               <th>

                 <h4> CODIGO DOCENTE</h4>

                </th>

                <th>

                 <h4> NOMBRE DOCENTE </h4>

                </th>

                <th>

                  <h4>APELLIDO DOCENTE</h4>

                </th>

               

              </tr>

            </thead>

            <tbody>

              <?php foreach($docentes as $key => $docente): ?>

                <tr>

                  
                   <td>

                    <b><?php echo e(mb_strtoupper($docente->id_persona)); ?></b>

                  </td>

                  <td>

                    <b><?php echo e(mb_strtoupper($docente->nombres_persona)); ?></b>

                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($docente->apellidos_persona)); ?>


                  </td>

                 
                 

                </tr>

              <?php endforeach; ?>

            </tbody>

          </table>

        </div>

      </div>

      <div class="col-sm-12 text-center">

     
      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>