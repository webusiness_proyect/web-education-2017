<?php $__env->startSection('titulo'); ?>

  <title>Actividades</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center">TAREAS RESPONDIDAS</h1>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      </div>

      <div class="col-sm-12">

        <p>

          

        </p>

        <?php if(count($ractividades) == 0): ?>

          <p class="text-info">

            No se han encontrado actividades para este curso...

          </p>

        <?php else: ?>

          <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>

                  <th>

                    NO

                  </th>

                  <th>

                    NOMBRE

                  </th>

                  <th>

                    ESTADO DE ENTREGA

                  </th>

                  <th>

                    CALIFICACION

                  </th>



                  <th>

                    FECHA ENTREGA

                  </th>

                  <th>

                    COMENTARIOS

                  </th>

                   <th>

                    ARCHIVO

                  </th>

                 

                  <th>

                    CALIFICAR TAREA

                  </th>

                </tr>

              </thead>

              <tbody>

                <?php foreach($ractividades as $key=>$actividad): ?>

                  <tr>

                    <td>

                        <?php echo e($key+1); ?>


                    </td>

                  <td>

                    <?php echo e(mb_strtoupper($actividad->name)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($actividad->estado_entrega_actividad)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($actividad->calificacion)); ?>


                  </td>



                  <td>

                    <?php echo e(mb_strtoupper($actividad->fecha_entrega)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($actividad->comentarios)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($actividad->ruta_actividad)); ?>


                  </td>

                    

                    <td>

                      <?php echo link_to_route('misrespuestasactividades.edit', $title = 'CALIFICAR',  $parameters = $actividad->id_respuesta_actividad, $attributes = ['class'=>'btn btn-info ']); ?>


                    </td>

                  </tr>

                <?php endforeach; ?>

              </tbody>

            </table>

             <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">

          </div>

        <?php endif; ?>

      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>