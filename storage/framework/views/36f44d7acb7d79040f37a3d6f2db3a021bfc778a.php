

<?php $__env->startSection('titulo'); ?>
  <title>Pensum del Grado</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Pensum del Grado</h1>
        <h3 class="text-center"><?php echo e(ucwords($pensum[0]->nombre_grado.' '.$pensum[0]->nombre_nivel.' '.$pensum[0]->nombre_carrera)); ?></h3>
      </div>
      <div class="col-sm-12">
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <th>
                NO
              </th>
              <th>
                CURSO
              </th>
              <th>
                SALON
              </th>
            </thead>
            <tbody>
              <?php foreach($pensum as $key => $p): ?>
                <tr>
                  <td>
                    <?php echo e($key+1); ?>
                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($p->nombre_area)); ?>
                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($p->nombre_salon)); ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>