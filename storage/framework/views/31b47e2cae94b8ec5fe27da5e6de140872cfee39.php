<?php $__env->startSection('titulo'); ?>


  <title>Nuevo Puesto</title>


<?php $__env->stopSection(); ?>





<?php $__env->startSection('cuerpo'); ?>


  <div id="page-wrapper">


    <div class="row">


      <div class="col-sm-12">
      <div class="panel-body ">

        <h1 class="page-header text-center"> 
        <div class="fa fa-plus" aria-hidden="true"></div> 
        
        <div class="fa fa-gears" aria-hidden="true"></div>Nuevo Puesto</h1>

        <p>


          Nota: Todos los campos con (*) son obligatorios.


        </p>


        <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>



        </div>


     



      </div>


      <div class="col-sm-12 panel panel-default">
      <center>
      <br/>

        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>


      </center>
      <br/>
        <?php echo Form::open(['route'=>'puestos.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'npuesto']); ?>



          <?php echo $__env->make('puestos.form.campos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


        <?php echo Form::close(); ?>



      </div>


    </div>


  </div>


<?php $__env->stopSection(); ?>



<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>