<?php $__env->startSection('titulo'); ?>



  <title>FILTRO</title>



<?php $__env->stopSection(); ?>







<?php $__env->startSection('cuerpo'); ?>



  <div id="page-wrapper">



    <div class="row">



      <div class="col-sm-12">

      <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-search" aria-hidden="true"></div> BUSQUEDA DE NOTAS </h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> Filtra </strong> y administra todas las notas disponibles en tu institucion
            </h4>
           
          </center>
        </div>
    
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
      </div>



      </div>



      <div class="panel panel-primary">

  <!-- Default panel contents -->

  <div class="panel-heading">Busqueda de grados</div>

  <div class="panel-body">

  <?php echo Form::open(array('url'=>'buscarnotas','method'=>'GET','autocomplete'=>'off','role'=>'search')); ?>




    <div class="form-group">

      <?php echo Form::label('plan', 'Plan*', ['class'=>'col-sm-2 control-label']); ?>


      <div class="col-sm-10">

        <?php echo Form::select('id_plan', $planes, null, ['class'=>'form-control', 'placeholder'=>'Seleccione plan...', 'id'=>'plan'] ); ?>

      <br/>
      </div>

    </div>


    <div class="form-group">

      <?php echo Form::label('jornada', 'Jornada*', ['class'=>'col-sm-2 control-label']); ?>


        <div class="col-sm-10">

        <?php echo Form::select('id_jornada', $jornadas, null, ['class'=>'form-control', 'placeholder'=>'Seleccione jornada...', 'id'=>'jornada']); ?>

        <br/>
        </div>

    </div>

    <div class="form-group">

      <?php echo Form::label('nivel', 'Nivel*', ['class'=>'col-sm-2 control-label']); ?>


        <div class="col-sm-10">

        <?php echo Form::select('id_nivel', $niveles, null, ['class'=>'form-control', 'placeholder'=>'Seleccione nivel...', 'id'=>'nIEstudiante']); ?>

        <br/>
        </div>

    </div>

    <div class="form-group">

      <?php echo Form::label('grado', 'Grado*', ['class'=>'col-sm-2 control-label']); ?>


        <div class="col-sm-10">

        <?php echo Form::select('id_grado', array(), null, ['class'=>'form-control', 'placeholder'=>'Seleccione grado...', 'id'=>'grado']); ?>

        <br/>
        </div>

    </div>
    
           
 <div class="form-group">

        <?php echo Form::label('inicia', 'Año Escolar*', ['class'=>'col-sm-2 control-label']); ?>


        <div class="input-group year col-sm-10">

                <?php echo Form::text('año', null, ['class'=>'form-control', 'placeholder'=>'Año o ciclo escolar...', 'id'=>'año']); ?>


                <span class="input-group-addon">

                  <i class="glyphicon glyphicon-time"></i>

                </span>

        </div>

      </div>

      <center>

      <span class="input-group-btn"> <div class="" aria-hidden="true"></div>

          <button type="submit" class="btn btn-info btn-lg"><div class="fa fa-search"></div>Buscar</button>
      </span>
      </center>

      <?php echo e(Form::close()); ?>








    <!-- Mensaje de carga-->



    <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>







  </div>















<?php $__env->stopSection(); ?>




<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>