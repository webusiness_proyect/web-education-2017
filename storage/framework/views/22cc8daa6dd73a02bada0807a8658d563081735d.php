<?php $__env->startSection('titulo'); ?>

  <title>Grados Niveles</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

       <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-pencil-square" aria-hidden="true"></div>Grados con Niveles</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> ASIGNA </strong> a un plan una jornada, un nivel, carrera, grado, seccion, costos de inscripcion y mensualidad ejemplo:<strong>*PLAN: DIARIO, JORNADA:MATUTINA, NIVEL:PRIMARIO, CARRERA: BACHILLERATO EN COMPUTACION, SECCION:A, INSCRIPCION: $.500.00, MENSUALIDAD: $.400.00</strong>
            </h4>
           
          </center>
        </div>
          <h6>
         <CENTER>
            * ESTOS DEBERAN DE EXISTIR PARA PODER SER ADMINISTRADOS
          </CENTER>
          </h6>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>
        <div class="col-sm-12 panel panel-default">

          <br/>
           <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>


        <?php if (\Entrust::can('crear-gradonivel')) : ?>

        <?php echo link_to_route('gradoniveles.create', $title = 'Nuevo Grado-Nivel', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square']); ?>


        <br>

        <br>

        <?php endif; // Entrust::can ?>

        <?php echo $__env->make('gradosniveles.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <?php if(count($datos) == 0): ?>

          <p class="text-info">

            No se han registrado grados en ningun nivel aun.

          </p>
        </center>
        <?php else: ?>

          <div class="table-responsive">

            <table class="table table-hover table-hover table- table-bordered">

              <thead>

                <tr>

                  <th>

                    NO

                  </th>

                  <th>

                    SECCION

                  </th>

                  <th>

                    GRADO

                  </th>

                  <th>

                    NIVEL

                  </th>

                  <th>

                    CARRERA

                  </th>

                  <?php if (\Entrust::can('editar-gradonivel')) : ?>

                  <th>

                    ACTUALIZAR

                  </th>

                  <?php endif; // Entrust::can ?>

                  <?php if (\Entrust::can('estado-gradonivel')) : ?>

                  <th>

                    ESTADO

                  </th>

                  <?php endif; // Entrust::can ?>

                </tr>

              </thead>

              <tbody id="datosGradosNiveles">

                <?php foreach($datos as $key => $dato): ?>

                  <tr>

                    <td>

                      <?php echo e($key+1); ?>


                    </td>

                    <td>

                      <?php echo e(mb_strtoupper($dato->nombre_seccion)); ?>


                    </td>

                    <td>

                      <?php echo e(mb_strtoupper($dato->nombre_grado)); ?>


                    </td>

                    <td>

                      <?php echo e(mb_strtoupper($dato->nombre_nivel)); ?>


                    </td>

                    <td>

                      <?php echo e(mb_strtoupper($dato->nombre_carrera)); ?>


                    </td>

                    <?php if (\Entrust::can('editar-gradonivel')) : ?>

                    <td>

                      <?php echo link_to_route('gradoniveles.edit', $title = 'Editar', $parameters = $dato->id_nivel_grado, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>


                    </td>

                    <?php endif; // Entrust::can ?>

                    <?php if (\Entrust::can('estado-gradonivel')) : ?>

                    <td>


                      <?php if($dato->estado_nivel_grado == TRUE): ?>
                              <input type="checkbox" name="estado" checked value="<?php echo e($dato->id_nivel_grado); ?>" class="toggleEstado">
                            <?php else: ?>
                              <input type="checkbox" name="estado" value="<?php echo e($dato->id_nivel_grado); ?>" class="toggleEstado">
                            <?php endif; ?>
                      

                    </td>

                    <?php endif; // Entrust::can ?>

                  </tr>

                <?php endforeach; ?>

              </tbody>

            </table>

            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">

            <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          </div>

        <?php endif; ?>

      </div>

      <div class="col-sm-12 text-center">

        <?php echo e($datos->setPath('gradoniveles')->links()); ?>


      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>