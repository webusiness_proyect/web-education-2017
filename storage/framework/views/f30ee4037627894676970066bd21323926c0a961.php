<?php $__env->startSection('titulo'); ?>
  <title>Asignación Docente</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
      <div class="panel-body ">
        <h1 class="page-header text-center"> <div class="fa fa-flag" aria-hidden="true"></div>Asignación Docente</h1>
        </div>
      </div>
      <div class="col-sm-12">

        <h2>Docente: <small><?php echo e(mb_strtoupper($docente->nombres_persona.' '.$docente->apellidos_persona)); ?></small></h2>
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  CURSO
                </th>
                <th>
                  GRADO
                </th>
                <th>
                  CARRERA
                </th>
                <th>
                  NIVEL
                </th>
                <th>
                  JORNADA
                </th>
                <th>
                  PLAN
                </th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($asignaciones as $key => $asignacion): ?>
                <tr>
                  <td>
                    <?php echo e($key+1); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($asignacion->nombre_area)); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($asignacion->nombre_grado)); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($asignacion->nombre_carrera)); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($asignacion->nombre_nivel)); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($asignacion->nombre_jornada)); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($asignacion->nombre_plan)); ?>

                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>