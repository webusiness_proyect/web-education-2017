<?php $__env->startSection('titulo'); ?>

  <title>Sistema Educativo - WB</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>



  <div id="page-wrapper">



      <div class="row">

          <div class="col-lg-12">

          <h1 class="text-center"> BIENVENIDOS</h1>

              <h1 class="page-header text-center">  Sistema Educativo WB</h1>

              



          </div>

          <!-- /.col-lg-12 -->

      </div>

        <div class="row">

          <div class="col-lg-6">

              <h3 class="text-center">Bienvenidos al software que hara mas facil para usted y sus maestros el asignar notas y controlar todo lo que pasa en su institucion, Wb Education, lo mejor en educacion </h3>
              <br/>
               <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                  <script type="text/javascript">
                   google.charts.load('current', {'packages':['corechart']});
                   google.charts.setOnLoadCallback(drawChart);
                   function drawChart() {

                      var data = google.visualization.arrayToDataTable([
                       ['Task', 'Hours per Day'],
                       ['Primero',     11],
                       ['Segundo',      2],
                       ['Tercero',  2],
                       ['Cuarto', 2],
                      ['Quinto',    7],
                       ['Sexto',    10],
                       ['Primero Basico',    7],
                       ['Segundo Basico',    9],
                       ['Tercero Basico',    10],
                        ['Carrera 1',    7],
                        ['Carrera 2',    12],
                       ['carrera 3',    7]
                      ]);

                     var options = {
                        title: '    ESTADO ACTUAL DE INSCRIPCIONES'
                      };

                      var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                      chart.draw(data, options);
                    }
                  </script>


              <div id="piechart"></div>

              <script type='text/javascript' src="https://www.gstatic.com/charts/loader.js"></script>
               <div id="regions_div" style="width: 503px; height: 300px;"></div>

               <script type="text/javascript">
               google.charts.load('upcoming', {'packages':['geochart']});
                  google.charts.setOnLoadCallback(drawRegionsMap);

                 function drawRegionsMap() {

                   var data = google.visualization.arrayToDataTable([
                      ['Country', 'Popularity'],
                      ['Germany', 200],
                     ['United States', 600],
                      ['Brazil', 400],
                      ['Canada', 500],
                      ['France', 600],
                      ['RU', 700],
                      ['Guatemala', 900],
                      ['Mexico', 800],
                      ['El Salvador', 850],
                      ['Costa Rica', 700],
                      ['Nicaragua', 500]
                    ]);

                    var options = {};

                   var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

                   chart.draw(data, options);
                  }

               </script>
               <div id="regions_div"></div>



          </div>

          <div class="col-lg-6">

              <img src="<?php echo e(url('/img/imagenes/3er vista.png')); ?>">

          </div>

          

          <!-- /.col-lg-12 -->

      </div>



  </div>

  <!-- /#page-wrapper -->



<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>