<?php $__env->startSection('titulo'); ?>
  <title>Empleados</title>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
       <div class="panel-body ">
        <h2 class="page-header text-center"> <div class="fa fa-users" aria-hidden="true"></div><div class="fa fa-gear" aria-hidden="true"></div>Notas de <?php echo e(ucwords($students[0]->nombre_estudiante.' '.$students[0]->apellidos_estudiante)); ?></h2>
        <h4 class="text-center"><?php echo e(ucwords($unidad[0]->nombre_unidad)); ?> del <?php echo e(ucwords($unidad[0]->fecha_inicio)); ?> al <?php echo e(ucwords($unidad[0]->fecha_final)); ?> Año: <?php echo e($year); ?></h4>
        <br/>


          <center>
                          <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>  
          </center>
   

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
      </div>
      <div class="col-sm-12 panel panel-default">
      <br/>

        <?php if(count($actividades) == 0): ?>
          <p class="text-info">
            No hay actividades entregadas por este alumno.
          </p>
          </center>
        <?php else: ?>
          <div class="table-responsive">
            <table class="table table-hover table- table-bordered">
              <thead>
                <tr>
                  <th>
                    <H3><STRONG>NOMBRE</STRONG></H3>
                  </th>
                  <th>
                    <H3><STRONG> DESCRIPCION </STRONG></H3>
                  </th>
                  <th>
                    <H3><STRONG> PUNTEO </STRONG></H3>
                  </th>
                  <th>
                    <H3><STRONG> MODIFICAR NOTA </STRONG></H3>
                  </th>
                 
                  
                
                 
                </tr>
              </thead>
              <tbody>
                <?php foreach($actividades as $key => $act): ?>
                  <tr>

                   
                   
                    <td>
                    <H4><STRONG><?php echo e(ucwords($act->nombre_actividad)); ?> </STRONG></H4>
                   </td>
                    <td>
                    <H4><STRONG><?php echo e(ucwords($act->descripcion_actividad)); ?> </STRONG></H4>
                    </td>
                   <td>
                    <H4><STRONG><?php echo e(ucwords($act->calificacion)); ?> </STRONG></H4>
                   </td>
                   <td>
                       

                    <H5> <?php echo link_to_route('resultadosbuscarnotasbimestre.edit', $title = 'CALIFICAR',  $parameters = [$act->id], $attributes = ['class'=>'btn btn-default  fa fa-check-square']); ?></H5>

                  
                  </td>
                   
                   
                   
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>

          

          </div>
        <?php endif; ?>


      </div>
      <div class="col-sm-12 panel panel-default">
      <br/>

        <?php if(count($sinnota) == 0): ?>
          <p class="text-info">
            No hay notas sin entregar.
          </p>
          </center>
        <?php else: ?>
          <div class="table-responsive">
            <table class="table table-hover table- table-bordered">
              <thead>
                <tr>
                  <th>
                    <H3><STRONG>NOMBRE</STRONG></H3>
                  </th>
                  <th>
                    <H3><STRONG> DESCRIPCION </STRONG></H3>
                  </th>
                  <th>
                    <H3><STRONG> PUNTEO </STRONG></H3>
                  </th>
                  <th>
                    <H3><STRONG> CREAR NOTA </STRONG></H3>
                  </th>
                 
                  
                
                 
                </tr>
              </thead>
              <tbody>
                <?php foreach($sinnota as $key => $act): ?>
                  <tr>

                   
                   
                    <td>
                    <H4><STRONG><?php echo e(ucwords($act->nombre_actividad)); ?> </STRONG></H4>
                   </td>

                   <td>
                    <H4><STRONG><?php echo e(ucwords($act->descripcion_actividad)); ?> </STRONG></H4>
                   </td>
                   <td class="alert-danger">
                     <center><strong> 0</strong></center>
                   </td>
                   <td>
                       

                    <?php echo link_to_route('areas.edit', $title = 'Crear Nota', $parameters = $act->id_actividad, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>


                  
                  </td>
                   
                   
                   
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>

           <?php echo Form::hidden('_token', csrf_token(), ['id'=>'token']); ?>


          </div>
        <?php endif; ?>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>