 <ul class="<?php echo e($class); ?>" id="side-menu">
   <li>
       <a href="<?php echo e(url('/')); ?>"><i class="fa fa-home fa-fw"></i> Página de Inicio</a>
   </li>
    <?php foreach($items as $item): ?>
        <li <?php if($item['class']): ?> class="<?php echo e($item['class']); ?>" <?php endif; ?> id="menu_<?php echo e($item['id']); ?>">
            <?php if(empty($item['submenu'])): ?>
                <a href="#">
                    <?php echo e($item['title']); ?>
                </a>
            <?php else: ?>
              <?php /*   <a href="<?php echo e($item['url']); ?>" class="dropdown-toggle" data-toggle="dropdown"> */ ?>
              <a href="<?php echo e(url('/').$item['url']); ?>">
                    <?php echo e($item['title']); ?>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <?php foreach($item['submenu'] as $subitem): ?>
                        <li>
                            <a href="<?php echo e($subitem['url']); ?>"><?php echo e($subitem['title']); ?></a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>
</ul>
