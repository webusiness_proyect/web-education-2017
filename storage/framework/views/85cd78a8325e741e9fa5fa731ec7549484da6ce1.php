<?php $__env->startSection('titulo'); ?>
  <title>Actividades</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">ACTIVIDADES</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <p>
          <?php echo link_to_route('actividades.create', $title = ' Nueva actividad', $parameters = ['a'=>$curso,'b'=>$curso], $attributes = ['class'=>'btn btn-primary fa fa-plus']); ?>

        </p>
        <?php if(count($actividades) == 0): ?>
          <p class="text-info">
            No se han creado actividades en este curso...
          </p>
        <?php else: ?>
          <div class="table-responsive">
            <table class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    NOMBRE ACTIVIDAD
                  </th>
                  <th>
                    DESCRIPCION ACTIVIDAD
                  </th>
                  <th>
                    FECHA DE INICIO
                  </th>
                  <th>
                    FECHA ENTREGA
                  </th>
                  <th>
                    NOTA TOTAL
                  </th>
                  <th>
                    EDITAR
                  </th>
                  <th>
                    VER ENTREGADOS
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php foreach($actividades as $key=>$actividad): ?>
                  <tr>
                    <td>
                        <?php echo e($key+1); ?>

                    </td>
                    
                    <td>
                    <?php echo e(mb_strtoupper($actividad->nombre_actividad)); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($actividad->descripcion_actividad)); ?>

                  </td>
                  <td>
                   <?php echo e(mb_strtoupper($actividad->fecha_inicio)); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($actividad->fecha_entrega)); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($actividad->nota_total)); ?>

                  </td>
                    <td>
                      <?php if (app('Illuminate\Contracts\Auth\Access\Gate')->check('owner', $actividad)): ?>
                      <?php echo link_to_route('actividades.edit', $title = ' Editar', $parameters = $actividad->id_actividad, $attributes = ['class'=>'btn btn-primary fa fa-edit']); ?>

                      <?php endif; ?>
                    </td>
                    <td>
                      <?php echo link_to_route('respuestasforos.create', $title = 'Ver Entregas', $parameters = ['actividad'=>$actividad->id_actividad], $attributes = ['class'=>'btn btn-info ']); ?>

                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
             <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>