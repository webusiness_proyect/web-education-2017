<ul class="<?php echo e($class); ?>" id="side-menu">
   <li>
          <a href="<?php echo e(url('/')); ?>"><i class="fa fa-home fa-fw"></i> Página de Inicio</a>
   </li>    
   <?php foreach($items as $item): ?>        
   	<li <?php if($item['class']): ?> class="<?php echo e($item['class']); ?>" <?php endif; ?> id="menu_<?php echo e($item['id']); ?>">
   	            <?php if(empty($item['submenu'])): ?>
   	                            <a href="#">                    
   	                            <?php echo e($item['title']); ?>                
   	                            </a>            
   	                        <?php else: ?>              
   	                        	<?php /*   <a href="<?php echo e($item['url']); ?>" class="dropdown-toggle" data-toggle="dropdown"> */ ?>              
   	                        	<a href="<?php echo e(url('/').$item['url']); ?>">                     
   	                        	<a href="<?php echo e(url('/').$item['url']); ?>">                                    
   	                        	<?php echo e($item['title']);                     
   	                        	if (e($item['title'])=='Persona')                    
   	                        		{?>                        
   	                        		<i class="fa fa-user"></i>   

   	                        	   <?php                 
   	                        	    }                    
   	                        	if(e($item['title'])=='Usuario')
   	                        	    {?>                        
   	                        	    <i class="fa fa-users"></i>  

   	                        	    <?php      
   	                        	    }                    
   	                        	if(e($item['title'])=='Institucional')
   	                        	 	{?>                        
   	                        	    <i class="fa fa-university"></i>                     
   	                        	    <?php                    
   	                        	    }                    
   	                        	    if(e($item['title'])=='Docente')                    
   	                        	                                 	
   	                        	    {?>                        
   	                        	    <i class="fa fa-book"></i>                        
   	                        	     <?php                    
   	                        	     }                     
   	                        	     if(e($item['title'])=='Estudiante')                    
   	                        	     	{?>                        
   	                        	    
   	                        	    <i class="fa fa-graduation-cap"></i> 

   	                        	        <?php                    
   	                        	        }                     
   	                        	    if(e($item['title'])=='Tutores')                    
   	                        	    	{?>                        
   	                        	                                 
   	                        	     <i class="fa fa-eye"></i>                        <?php                    
   	                        	     } 
   	                        	      if(e($item['title'])=='Directores')

   	                        	     ?> 
   	                        	     <i class="fa fa-eye"></i>                                          
   	                        	     <span class="fa arrow"></span></a>                
   	                        	     


   	                    <ul class="nav nav-second-level"> 
   	       

   	                     <?php foreach($item['submenu'] as $subitem): ?>
                         <li>                            
                         <a href="<?php echo e($subitem['url']); ?>"><?php echo e($subitem['title']); 
                         	if (e($subitem['title'])=='Persona')
		                    {?>
		                        <i class="fa fa-user"></i>
		                        <?php
		                    }
		                    if(e($subitem['title'])=='Unidades')
		                    {?>
		                        <i class="fa fa-pencil-square"></i>
		                        <?php
		                    }
		                    if(e($subitem['title'])=='Carreras')
		                    {?>
		                        <i class="fa fa-graduation-cap"></i>
		                        <?php
		                    }
		                     if(e($subitem['title'])=='Niveles')
		                    {?>
		                        <i class="fa fa-sliders"></i>
		                        <?php
		                    }
		                     if(e($subitem['title'])=='Grados')
		                    {?>
		                        <i class="fa fa-bookmark-o"></i>
		                        <?php
		                    }


		                    if(e($subitem['title'])=='Planes niveles')
		                    {?>
		                        <i class="fa fa-pencil-square"></i>
		                        <?php
		                    }
		                    if(e($subitem['title'])=='Grados niveles')
		                    {?>
		                        <i class="fa fa-graduation-cap"></i>
		                        <?php
		                    }
		                     if(e($subitem['title'])=='Secciones')
		                    {?>
		                        <i class="fa fa-sort-alpha-asc"></i>
		                        <?php
		                    }
		                     if(e($subitem['title'])=='Jornadas')
		                    {?>
		                        <i class="fa fa-sun-o"></i>
		                        <?php
		                    }

		                    if(e($subitem['title'])=='Salones')
		                    {?>
		                        <i class="fa fa-home"></i>
		                        <?php
		                    }
		                    if(e($subitem['title'])=='Planes')
		                    {?>
		                        <i class="glyphicon glyphicon-time"></i>
		                        <?php
		                    }
		                     if(e($subitem['title'])=='Cursos')
		                    {?>
		                        <i class="fa fa-pencil-square"></i>
		                        <?php
		                    }
		                     if(e($subitem['title'])=='Pensum grados')
		                    {?>
		                        <i class="fa fa-bookmark-o"></i>
		                        <?php
		                    }

		                    if(e($subitem['title'])=='Empleados')
		                    {?>
		                        <i class="fa fa-gear"></i>
		                        <?php
		                    }
		                     if(e($subitem['title'])=='Puestos')
		                    {?>
		                        <i class="fa fa-gears"></i>
		                        <?php
		                    }

		                    if(e($subitem['title'])=='Usuario')
		                    {?>
		                        <i class="fa fa-user"></i><i class="fa fa-plus"></i>
		                        <?php
		                    }

		                     if(e($subitem['title'])=='Roles')
		                    {?>
		                        <i class="fa fa-sitemap"></i>
		                        <?php
		                    }


		                    if(e($subitem['title'])=='Asignacion docente')
		                    {?>
		                        <i class="fa fa-eye"></i><i class="fa fa-plus"></i>
		                        <?php
		                    }

		                     

		                     if(e($subitem['title'])=='Inscripcion estudiantes')
		                    {?>
		                        <i class="fa fa-child"></i><i class="fa fa-plus"></i>
		                        <?php
		                    }
				                    if(e($subitem['title'])=='Mis  cursos')
		                    {?>
		                        <i class="fa fa-pencil"></i>
		                        <?php
		                    }
		                    if(e($subitem['title'])=='Mis  notas')
		                    {?>
		                        <i class="fa fa-book"></i>
		                        <?php
		                    }

		                    if(e($subitem['title'])=='F o r o s')
		                    {?>
		                        <i class="fa fa-comments"></i>
		                        <?php
		                    }

		                     if(e($subitem['title'])=='Calendario')
		                    {?>
		                        <i class="fa fa-calendar"></i>
		                        <?php
		                    }

		                    if(e($subitem['title'])=='Mis  foros')
		                    {?>
		                        <i class="fa fa-comments-o"></i>
		                        <?php
		                    }

		                    if(e($subitem['title'])=='Mis  actividades')
		                    {?>
		                        <i class="fa fa-pencil-square"></i>
		                        <?php
		                    }


		                    if(e($subitem['title'])=='Mis horarios')
		                    {?>
		                        <i class="fa fa-calendar"></i>
		                        <?php
		                    }

		                     if(e($subitem['title'])=='Mis foros docente')
		                    {?>
		                        <i class="fa fa-comments-o"></i>
		                        <?php
		                    }


		                     if(e($subitem['title'])=='Mis actividades docente')
		                    {?>
		                        <i class="fa fa-pencil-square"></i>
		                        <?php
		                    }

		                     if(e($subitem['title'])=='Mis horarios docente')
		                    {?>
		                        <i class="fa fa-calendar"></i>
		                        <?php
		                    }

		                     if(e($subitem['title'])=='Mis alumnos')
		                    {?>
		                        <i class="fa fa-child"></i>
		                        <?php
		                    }

		                     if(e($subitem['title'])=='Mis docentes')
		                    {?>
		                        <i class="fa fa-book"></i>
		                        <?php
		                    }
		                    
		                    
		                    



                         ?></a>

                         </li>                    
                     <?php endforeach; ?>                
                     </ul>            
                 <?php endif; ?>        
                 </li>    
             <?php endforeach; ?>
</ul>