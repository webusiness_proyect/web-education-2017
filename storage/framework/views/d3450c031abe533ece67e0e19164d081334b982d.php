<?php $__env->startSection('title'); ?>
  <title>Repsuesta del Foro</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Respuestas del Foro</h1>
      </div>
      <div class="col-sm-12">

        <div class="panel panel-info">
          <div class="panel-heading">
            <h3 class="panel-title"><?php echo e($foro->titulo_foro); ?></h3>
          </div>
          <div class="panel-body">
            <?php echo $foro->mensaje_foro; ?>
          </div>
          <div class="panel-body">
            <?php echo $foro->mensaje_foro; ?>
          </div>
        </div>
      </div>
      <?php foreach($respuestas as $respuesta): ?>

      <div class="col-sm-offset-2 col-sm-10">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><?php echo e(ucwords($respuesta['nombre'].' '.$respuesta['apellido'])); ?></h3>
          </div>
          <div class="panel-body">
            <?php echo $respuesta['respuesta']; ?>
          </div>
          <div class="panel-footer text-right">
            <?php echo e($respuesta['fecha']); ?>
          </div>
        </div>
      </div>
    <?php endforeach; ?>

    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>