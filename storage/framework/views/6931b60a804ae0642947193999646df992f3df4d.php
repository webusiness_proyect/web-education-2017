<?php $__env->startSection('titulo'); ?>

  <title>Mis alumnos</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

      <di <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-bookmark" aria-hidden="true"></div> Docentes de mis hijos </h1>
      
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        

           <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

       
      </center>

        </div>

       
        <div class="col-sm-12 panel panel-default">

          <br/>



        <div class="table-responsive">

          <table class="table table-hover table-bordered">

            <thead>

              <tr>

               <th>
                <CENTER>
                  <h4><strong>ENVIA MENSAJE</strong></h4>
                </CENTER>
                </th>


                <th>
                  <CENTER>
                 <h4><strong> DOCENTE </strong></h4>
               </CENTER>
                </th>

                <th>
                  <CENTER>
                 <h4><strong> OPCIONES </strong></h4>
               </CENTER>
                </th>


               

              </tr>

            </thead>

            <tbody>

              <?php foreach($docentes as $key => $docente): ?>

                <tr>

                  
                   <td>

                    <center>
                    <a href="<?php echo e(url('/enviomensajes')); ?>"><i class="fa fa-envelope-o fa-2x"></i></a>
                  </center>

                  </td>

                  
                  <td>
                  <center>
                    <h4><?php echo e(mb_strtoupper($docente->nombres_persona)); ?>    <?php echo e(mb_strtoupper($docente->apellidos_persona)); ?></h4>
                  </center>
                  </td>

                  
                    <td>

                    <center>
                       <H4><a href="/miscursosprofesores/show?a=<?php echo e($docente->id_persona); ?>"  class="btn btn-success"><div class="fa fa-eye fa-2x" enabled></div> CURSOS</a><br/><H4>
                       
                      </center>

                  </td>

                 
                 

                </tr>

              <?php endforeach; ?>

            </tbody>

          </table>

        </div>

      </div>

      <div class="col-sm-12 text-center">

     
      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>