<?php $__env->startSection('titulo'); ?>
  <title>Nuevo Grado-Nivel</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nuevo Grado - Nivel</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <?php echo Form::open(['route'=>'gradoniveles.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'ngradonivel']); ?>
          <?php echo $__env->make('gradosniveles.form.campos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo Form::close(); ?>
      </div>
      <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>