<?php $__env->startSection('titulo'); ?>
  <title>Editar Rol</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header">Editar Rol</h1>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
        <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
      <?php if (\Entrust::can('crear-rol')) : ?>
        <button type="button" name="permisos" class="btn btn-primary" data-toggle="modal" data-target="#formModal"><span class="fa fa-plus-circle"></span> Agregar Permiso</button>
        <?php endif; // Entrust::can ?>
        <br>
        <br>
        <h3>Rol: <small><?php echo e(mb_strtoupper($rol->name)); ?></small></h3>
      <?php /*  <?php echo Form::model($rol, ['route'=>['roles.update', $rol->id_rol], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'nrol']); ?>

        <div class="form-group">
          <?php echo Form::label('nombre', 'Nombre*', ['class'=>'col-sm-2 control-label']); ?>

          <div class="col-sm-10">
            <?php echo Form::text('nombre_rol', null, ['class'=>'form-control', 'placeholder'=>'Nombre del nuevo rol...']); ?>

          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="button" name="guardar" class="btn btn-success"><span class="fa fa-save"></span> Registrar</button>
          </div>
        </div>
        <?php echo Form::close(); ?> */ ?>
        <?php echo Form::hidden('rol', $rol->id, ['id'=>'idRol']); ?>

        <?php echo Form::hidden('_token', csrf_token(), ['id'=>'token']); ?>

        <br>
        <br>
        <h2>Lista de Permisos Asignados</h2>
        <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>
                NO
              </th>
              <th>
                NOMBRE PERMISO
              </th>
              <th>
                ESTADO PERMISO
              </th>
            </tr>
          </thead>
          <tbody id="datosPermisosAsignados">
            <?php foreach($permisos as $key => $permiso): ?>
              <tr>
                <td>
                  <?php echo e($key+1); ?>

                </td>
                <td>
                  <?php echo e(mb_strtoupper($permiso->name)); ?>

                </td>
                <td>
                <?php if (\Entrust::can('crear-rol')) : ?>
                  <?php if($permiso->state_permission_role == true): ?>
                    <input type="checkbox" name="estado" checked value="<?php echo e($permiso->id_permission_role); ?>" class="toggleEstado">
                  <?php else: ?>
                    <input type="checkbox" name="estado" value="<?php echo e($permiso->id_permission_role); ?>" class="toggleEstado">
                  <?php endif; ?>
                  <?php endif; // Entrust::can ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>

<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        <div class="form-horizontal">

        <div class="form-group">
          <?php echo Form::label('opciones', 'Opciones*', ['class'=>'col-sm-2 control-label']); ?>

          <div class="col-sm-10">
            <?php echo Form::select('opciones', $opciones, null, ['placeholder'=>'Seleccione una opción...', 'class'=>'form-control', 'id'=>'opcionesSistema']); ?>

          </div>
        </div>

        <div class="form-group">
          <?php echo Form::label('permisos', 'Lista Permisos*', ['class'=>'col-sm-2 control-label']); ?>

          <div class="col-sm-10">
            <div class="table-responsive">

            <table class="table table-hover">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    NOMBRE PERMISO
                  </th>
                  <th>
                    OPCION
                  </th>
                </tr>
              </thead>
              <tbody id="subOpciones">

              </tbody>
            </table>
          </div>
        </div>
        </div>

        <div class="form-group">
          <?php echo Form::label('permiso', 'Permisos Rol*', ['class'=>'col-sm-2 control-label']); ?>

          <div class="col-sm-10">
            <div id="permisosRoles">

            </div>
          </div>
        </div>

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <?php if (\Entrust::can('crear-rol')) : ?>
        <button type="button" class="btn btn-success" id="registrarPermisos"><span class="fa fa-save"></span> Registrar</button>
        <?php endif; // Entrust::can ?>
      </div>
    </div>
  </div>
</div>

      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>