<?php $__env->startSection('titulo'); ?>
  <title>Carreras</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center "> <div class="fa fa-graduation-cap"></div>Carreras</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra las carreras impartidas en tu institucion, ejemplo: <STRONG>BACHILLERATO EN COMPUTACION</STRONG>
            </h4>
          </center>
        </div>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12 panel panel-default">
        <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

        <?php if (\Entrust::can('crear-carrera')) : ?>
          <?php echo link_to_route('carreras.create', $title = 'Nueva Carrera', $parameters = null, $attributes = ['class'=>'btn btn-primary  fa fa-plus']); ?>

        <?php endif; // Entrust::can ?>

        <?php echo $__env->make('carreras.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  

        <?php if(count($carreras) == 0): ?>
          <p class="text-info">
            No se han registrado carreras aun.
          </p>
          </div>

        </center>

        <div class="col-sm-12 panel panel-default">
        <?php else: ?>
          <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  CARRERA
                </th>
                <?php if (\Entrust::can('editar-carrera')) : ?>
                <th>
                  ACTUALIZAR
                </th>
                <?php endif; // Entrust::can ?>
                <th>
                  ESTADO
                </th>
              </tr>
            </thead>
            <tbody id="datosCarreras">
              <?php foreach($carreras as $key => $carrera): ?>
                <tr>
                  <td>
                    <?php echo e($key+1); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($carrera->nombre_carrera)); ?>

                  </td>
                  <?php if (\Entrust::can('editar-carrera')) : ?>
                  <td>
                    <?php echo link_to_route('carreras.edit', $title = 'Editar', $parameters = $carrera->id_carrera, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>

                  </td>
                  <?php endif; // Entrust::can ?>
                  <td>
                    
                     <?php if($carrera->estado_carrera == TRUE): ?>
                              <input type="checkbox" name="estado" checked value="<?php echo e($carrera->id_carrera); ?>" class="toggleEstado">
                            <?php else: ?>
                              <input type="checkbox" name="estado" value="<?php echo e($carrera->id_carrera); ?>" class="toggleEstado">
                            <?php endif; ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
          <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>
      </div>

      <div class="col-sm-12 text-center">


        <?php echo e($carreras->setPath('carreras')->links()); ?>



      </div>

    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>