<div class="form-group">
  <?php echo Form::label('nombre', 'Nombres*', ['class'=>'col-sm-2 control-label']); ?>

  <div class="col-sm-10">
    <?php echo Form::text('nombres_persona', null, ['class'=>'form-control', 'placeholder'=>'Nombre empleado...']); ?>

  </div>
</div>
<div class="form-group">
  <?php echo Form::label('apellidos', 'Apellidos*', ['class'=>'col-sm-2 control-label']); ?>

  <div class="col-sm-10">
    <?php echo Form::text('apellidos_persona', null, ['class'=>'form-control', 'placeholder'=>'Apellidos empleado...']); ?>

  </div>
</div>
<div class="form-group">
  <?php echo Form::label('cui', 'CUI*', ['class'=>'col-sm-2 control-label']); ?>

  <div class="col-sm-10">
    <?php echo Form::text('cui_persona', null, ['class'=>'form-control', 'placeholder'=>'No DPI empleado...']); ?>

  </div>
</div>
<div class="form-group">
  <?php echo Form::label('direccion', 'Dirección*', ['class'=>'col-sm-2 control-label']); ?>

  <div class="col-sm-10">
    <?php echo Form::text('direccion_persona', null, ['class'=>'form-control', 'placeholder'=>'Direción empleado...']); ?>

  </div>
</div>
<div class="form-group">
  <?php echo Form::label('correo', 'Correo*', ['class'=>'col-sm-2 control-label']); ?>

  <div class="col-sm-10">
    <?php echo Form::text('correo_persona', null, ['class'=>'form-control', 'placeholder'=>'Direción de correo electronico personal...']); ?>

  </div>
</div>
<div class="form-group">
  <?php echo Form::label('nacimiento', 'Fecha Nacimiento*', ['class'=>'col-sm-2 control-label']); ?>

  <div class="col-sm-10">
    <div class=" input-group date calendario">
    <?php echo Form::text('fecha_nacimiento_persona', null, ['class'=>'form-control', 'placeholder'=>'Fecha nacimiento empleado...']); ?>

    <span class="input-group-addon">
        <span class="glyphicon glyphicon-calendar"></span>
    </span>
  </div>
  </div>
</div>
<div class="form-group">
  <?php echo Form::label('telefono', 'Telefono*', ['class'=>'col-sm-2 control-label']); ?>

  <div class="col-sm-10">
    <?php echo Form::text('telefono_persona', null, ['class'=>'form-control', 'placeholder'=>'No telefono empleado...']); ?>

  </div>
</div>
<div class="form-group">
  <?php echo Form::label('telefono2', 'Telefono Emergencias', ['class'=>'col-sm-2 control-label']); ?>

  <div class="col-sm-10">
    <?php echo Form::text('telefono_auxiliar_persona', null, ['class'=>'form-control', 'placeholder'=>'No telefono para emergencias empleado...']); ?>

  </div>
</div>
<div class="form-group">
  <?php echo Form::label('puesto', 'Puesto*', ['class'=>'col-sm-2 control-label']); ?>

  <div class="col-sm-10">
    <?php echo Form::select('id_puesto', $puestos, null, ['class'=>'form-control', 'placeholder'=>'Seleccione puesto...']); ?>

  </div>
</div>
<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" name="guardar" class="btn btn-success"><span class="fa fa-save"></span> Registrar</button>
  </div>
</div>
