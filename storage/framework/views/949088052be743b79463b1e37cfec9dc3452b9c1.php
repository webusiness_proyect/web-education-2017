<?php $__env->startSection('titulo'); ?>
  <title>Secciones</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">

        <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-sort-alpha-asc" aria-hidden="true"></div> Secciones disponibles</h1>
          <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra secciones disponibles en tu institucion ejemplo:<strong>SECCION: A</strong>
            </h4>
            
          </center>
        </div>
        
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>

      </div>
      <div class="col-sm-12 panel panel-default">

        <br/>
        <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

        <?php if (\Entrust::can('crear-seccion')) : ?>
        <?php echo link_to_route('secciones.create', $title = 'Nueva sección', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square']); ?>

        <br>
        <br>
        <?php endif; // Entrust::can ?>
        <?php if(count($secciones) == 0): ?>
          <p class="text-info">
            No se han registrado secciones aun.
          </p>
        </center>
        <?php else: ?>
          <table class="table table-hover table-bordered">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NOMBRE SECCION
                </th>
                <?php if (\Entrust::can('editar-seccion')) : ?>
                <th>
                  ACTUALIZAR
                </th>
                <th>
                  ESTADO
                </th>
                <?php endif; // Entrust::can ?>
              </tr>
            </thead>
            <tbody id="datosSecciones">
              <?php foreach($secciones as $key => $seccion): ?>
                <tr>
                  <td>
                    <?php echo e($key+1); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($seccion->nombre_seccion)); ?>

                  </td>
                  <?php if (\Entrust::can('editar-seccion')) : ?>
                  <td>
                    <?php echo link_to_route('secciones.edit', $title ='Editar', $parameters = $seccion->id_seccion, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>

                  </td>
                  <td>
                   <?php if($seccion->estado_secciones == TRUE): ?>
                      <input type="checkbox" name="estado" checked value="<?php echo e($seccion->id_seccion); ?>" class="toggleEstado">
                  <?php else: ?>
                      <input type="checkbox" name="estado" value="<?php echo e($seccion->id_seccion); ?>" class="toggleEstado">
                  <?php endif; ?>

                  </td>
                  <?php endif; // Entrust::can ?>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>