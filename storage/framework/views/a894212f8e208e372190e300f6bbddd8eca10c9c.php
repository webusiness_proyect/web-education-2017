<?php $__env->startSection('titulo'); ?>

  <title>Jornadas</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center">Jornadas</h1>

          <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      </div>

      <div class="col-sm-12">

        <?php if (\Entrust::can('crear-jornada')) : ?>

        <?php echo link_to_route('jornadas.create', $title = 'Nueva Jornada', $parameters = null, $attributes = ['class'=>'btn btn-primary']); ?>


        <br>

        <br>

        <?php endif; // Entrust::can ?>

        <?php if(count($jornadas) == 0): ?>

          <p class="text-info">

            No se han registrado jornadas aun.

          </p>

        <?php else: ?>

          <div class="table-responsive">

            <table class="table table-hover">

              <thead>

                <tr>

                  <th>

                    NO

                  </th>

                  <th>

                    JORNADA

                  </th>

                  <?php if (\Entrust::can('editar-jornada')) : ?>

                  <th>

                    ACTUALIZAR

                  </th>

                  <?php endif; // Entrust::can ?>

                  <?php if (\Entrust::can('estado_jornada')) : ?>

                  <th>

                    ESTADO

                  </th>

                  <?php endif; // Entrust::can ?>

                </tr>

              </thead>

              <tbody id="datosJornadas">

                <?php foreach($jornadas as $key => $jornada): ?>

                  <tr>

                    <td>

                      <?php echo e($key+1); ?>


                    </td>

                    <td>

                      <?php echo e(mb_strtoupper($jornada->nombre_jornada)); ?>


                    </td>

                    <?php if (\Entrust::can('editar-jornada')) : ?>

                    <td>

                      <?php echo link_to_route('jornadas.edit', $title = 'Editar', $parameters = $jornada->id_jornada, $attributes = ['class'=>'btn btn-primary']); ?>


                    </td>

                    <?php endif; // Entrust::can ?>

                    <?php if (\Entrust::can('estado_jornada')) : ?>

                    <td>

                      <?php if($jornada->estado_jornada == true): ?>

                          <?php echo link_to_route('jornadas.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarJornada', 'data-id'=>$jornada->id_jornada, 'data-estado'=>$jornada->estado_jornada]); ?>


                      <?php else: ?>

                          <?php echo link_to_route('jornadas.destroy', $title = 'Deshabilitado', $parameters = null, $attributes = ['class'=>'btn btn-danger eliminarJornada', 'data-id'=>$jornada->id_jornada, 'data-estado'=>$jornada->estado_jornada]); ?>


                      <?php endif; ?>

                    </td>

                    <?php endif; // Entrust::can ?>

                  </tr>

                <?php endforeach; ?>

              </tbody>

            </table>

          </div>

          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">

        <?php endif; ?>

      </div>

    </div>

    <!-- Mensaje de carga-->

    <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>