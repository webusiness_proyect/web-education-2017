<?php $__env->startSection('titulo'); ?>

  <title>Mis foros Asignados</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

      <di <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-bookmark" aria-hidden="true"></div> - - Mis foros de cursos </h1>
        <h4 class="text-center"><div class="glyphicon glyphicon-cog"></div>   <?php echo e(mb_strtoupper($unidad->nombre_unidad)); ?></h4>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>

         <?php echo $__env->make('docentes.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="col-sm-12 panel panel-default">

          <br/>



        <div class="table-responsive">

          <table class="table table-hover table-bordered">

            <thead>

              <tr>

               <th>

                 <h4> MIS FOROS</h4>

                </th>

                <th>

                 <h4> CURSO</h4>

                </th>

                <th>

                  <h4>NIVEL</h4>

                </th>

                <th>

                 <h4> GRADO</h4>

                </th>

                <th>

                  <h4>CARRERA</h4>

                </th>

                <th>

                  <h4>SECCION</h4>

                </th>

                <th>

                 <h4> PLAN</h4>

                </th>

                <th>

                  <h4>JORNADA</h4>

                </th>

                

              </tr>

            </thead>

            <tbody>

              <?php foreach($cursos as $key => $curso): ?>

                <tr>

                   <td>

                 


                    <!-- Single button -->

                    <center>



                    <a href="/foros?a=<?php echo e($curso->id_asignacion_area); ?>&b=<?php echo e($curso->id_asignacion_docente); ?>" class="btn btn-default"><div class="fa fa-comments"></div>Foros de <?php echo e($curso->nombre_area); ?></a>

  
                    
                    </center>

            

             


                    <?php /* <?php echo link_to_route('docente.create', $title = ' Zona', $parameters = ['a'=>$curso->id_asignacion_area,'b'=>$curso->id_asignacion_docente], $attributes = ['class'=>'btn btn-primary fa fa-edit']); ?>


                    */ ?>

                  </td>

                  <td>

                    <b><?php echo e(mb_strtoupper($curso->nombre_area)); ?></b>

                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($curso->nombre_nivel)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($curso->nombre_grado)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($curso->nombre_carrera)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($curso->nombre_seccion)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($curso->nombre_plan)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($curso->nombre_jornada)); ?>


                  </td>

                 

                </tr>

              <?php endforeach; ?>

            </tbody>

          </table>

        </div>

      </div>

      <div class="col-sm-12 text-center">

        <?php echo e($cursos->setPath('docente')->links()); ?>


      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>