<?php $__env->startSection('content'); ?>

<style>
body {
  /* Location of the image */
  background-image: url(/img/INICIO.jpg);
  
  /* Background image is centered vertically and horizontally at all times */
  background-position: center center;
  
  /* Background image doesn't tile */
  background-repeat: no-repeat;
  
  /* Background image is fixed in the viewport so that it doesn't move when 
     the content's height is greater than the image's height */
  background-attachment: fixed;
  
  /* This is what makes the background image rescale based
     on the container's size */
  background-size: cover;
  
  /* Set a background color that will be displayed
     while the background image is loading */
  background-color: #464646;
}
</style>
<div class="container" >
    <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel">
               	<div class="panel-heading">
               	<h2>Ingreso</h2>
               	</div>
               	<div class="panel-body">
                      	<form class="form-horizontal" role="form" method="POST" action="<?php echo e(url('/login')); ?>" id="login">
                      	  <?php echo e(csrf_field()); ?>                      
                      	  <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">                        <!--  <label for="email" class="col-md-4 control-label">E-Mail Address</label>-->
                      	  <div class="col-md-12">      
                      	  <div class="input-group"> 
                      	         <span class="input-group-addon"><i class="fa fa-user"></i></span>                            <?php echo Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'Correo institucional...']); ?>                          <!--  <input id="email" type="email" class="form-control" name="email" value="<?php echo e(old('email')); ?>">-->                          </div>                              <?php if($errors->has('email')): ?>                                  <span class="help-block">                                      <strong><?php echo e($errors->first('email')); ?></strong>                                  </span>                              <?php endif; ?>                          </div>                      </div>                      <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">                          <div class="col-md-12">                            <div class="input-group">                              <span class="input-group-addon"><i class="fa fa-key"></i></span>                              <?php echo Form::password('password', ['class'=>'form-control', 'placeholder'=>'Contraseña...']); ?>                              <!--<input id="password" type="password" class="form-control" name="password">-->                            </div>                              <?php if($errors->has('password')): ?>                                  <span class="help-block">                                      <strong><?php echo e($errors->first('password')); ?></strong>                                  </span>                              <?php endif; ?>                          </div>                      </div>                                                 <div class="col-md-6">                              <div class="checkbox">                                  <label>                                      <input type="checkbox" name="remember"> Recordarme                                  </label>                              </div>                                                          </div>                              <div class="col-md-6">                                  <a href="">Olvido contraseña o usuario</a>                              </div>                                                                             <button type="submit" class="btn btn-lg btn-default btn-block">                                  <i class="fa fa-btn fa-sign-in"></i> Ingresar                              </button>                  </form>                
                      	         </div>            
                      	         </div>        
                      	         </div>    
                      	         </div>
                      	         </div><?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>