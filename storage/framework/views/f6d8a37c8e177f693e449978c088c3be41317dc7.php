

      <div class="form-group">

        <?php echo Form::label('carrera', 'Seleccione Carrera: *', ['class'=>'col-sm-2 control-label']); ?>


        <div class="col-sm-10">

          <?php echo Form::select('id_nivel_grado', $carreras, null, ['class'=>'form-control', 'placeholder'=>'Seleccione Carrera...', 'id'=>'jncarrera']); ?>


        </div>

      </div>

      <div class="form-group">

        <?php echo Form::label('actividad', 'Seleccione curso: *', ['class'=>'col-sm-2 control-label']); ?>


        <div class="col-sm-10">

          <?php echo Form::select('id_area', $areas, null, ['class'=>'form-control', 'placeholder'=>'Selecciones un curso...', 'id'=>'nnareanivel']); ?>




        </div>

      </div>



       

      <div class="form-group">

        <?php echo Form::label('dia', 'Seleccione Dia*', ['class'=>'col-sm-2 control-label']); ?>


         <div class="col-sm-10">

          <?php echo Form::select('id_dia', $DIAS, null, ['class'=>'form-control', 'placeholder'=>'Seleccione dia...']); ?>


        </div>

      </div>

        

      <div class="form-group">

        <?php echo Form::label('inicia', 'Hora de inicio de actividad', ['class'=>'col-sm-2 control-label']); ?>


        <div class="input-group hora">

                <?php echo Form::text('hora_inicia', null, ['class'=>'form-control', 'placeholder'=>'Hora de inicio...']); ?>


                <span class="input-group-addon">

                  <i class="glyphicon glyphicon-time"></i>

                </span>

            </div>

      </div>



       
 <div class="form-group">

        <?php echo Form::label('inicia', 'Hora de fin de actividad', ['class'=>'col-sm-2 control-label']); ?>


        <div class="input-group hora">

                <?php echo Form::text('hora_fin', null, ['class'=>'form-control', 'placeholder'=>'Hora de fin...']); ?>


                <span class="input-group-addon">

                  <i class="glyphicon glyphicon-time"></i>

                </span>

            </div>

      </div>

     

       

      <div class="form-group">

        <div class="col-sm-offset-2 col-sm-10">

          <button type="submit" name="guardar" class="btn btn-success"><span class="fa fa-save"></span> Registrar</button>

        </div>

      </div>



