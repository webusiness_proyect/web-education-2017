<?php $__env->startSection('titulo'); ?>

  <title>Mis foros Asignados</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

      <di <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-comments" aria-hidden="true"></div> - - Mis foros de cursos </h1>
        <h4 class="text-center"><div class="glyphicon glyphicon-cog"></div>   <?php echo e(mb_strtoupper($unidad->nombre_unidad)); ?></h4>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y participa en  <strong>FOROS </strong> o conversaciones sobre temas de interes de tus cursos con tus alumnos 
            </h4>

          
           
          </center>

          <div class="alert alert-warning alert-dismissable">
          <center>

            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-info btn btn-default" aria-hidden="true"></i><STRONG> REALIZA CONVERSACIONES EN LAS CUALES TUS ALUMNOS PODRAN DAR SU OPINION Y COMENTAR DUDAS RESPECTO A UN TEMA EN ESPECIAL</a> </STRONG><br/>
           

            
          </CENTER>
    
        </div>
        </div>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>

        

           <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

       
      </center>

         <?php echo $__env->make('docentes.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="col-sm-12 panel panel-default">

          <br/>



        <div class="table-responsive">

          <table class="table table-hover table-bordered">

            <thead>

              <tr>

               <th>

                 <h4><STRONG><CENTER> OPCIONES</CENTER></h4></STRONG>

                </th>

                <th>

                 <h4><STRONG><CENTER> CURSO</CENTER></h4></STRONG>

                </th>

                <th>

                 <h4><STRONG><CENTER> PENSUM </CENTER></h4></STRONG>

                </th>

                

                <th>

                  <h4><STRONG><CENTER>CARRERA</CENTER></h4></STRONG>

                </th>

                <th>

                  <h4><STRONG><CENTER>SECCION</CENTER></h4></STRONG>

                </th>


                

              </tr>

            </thead>

            <tbody>

              <?php foreach($cursos as $key => $curso): ?>

                <tr>

                   <td>

                 


                    <!-- Single button -->

                    <center>



                    <a href="/foros?a=<?php echo e($curso->id_asignacion_area); ?>&b=<?php echo e($curso->id_asignacion_docente); ?>" class="btn btn-default"><div class="fa fa-comments"></div>Foros y conversaciones</a>

  
                    
                    </center>

            

             


                    <?php /* <?php echo link_to_route('docente.create', $title = ' Zona', $parameters = ['a'=>$curso->id_asignacion_area,'b'=>$curso->id_asignacion_docente], $attributes = ['class'=>'btn btn-primary fa fa-edit']); ?>


                    */ ?>

                  </td>

                   <td>

                    <b><?php echo e(mb_strtoupper($curso->nombre_area)); ?></b>

                  </td>

                  <td>

                      <?php echo e(mb_strtoupper($curso->nombre_grado)); ?>   
                      <?php echo e(mb_strtoupper($curso->nombre_nivel)); ?>    
                      <?php echo e(mb_strtoupper($curso->nombre_plan)); ?>

                      <?php echo e(mb_strtoupper($curso->nombre_jornada)); ?>


                  </td>
                  <td>
                  <?php echo e(mb_strtoupper($curso->nombre_carrera)); ?>

                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($curso->nombre_seccion)); ?>


                  </td>

                 

                </tr>

              <?php endforeach; ?>

            </tbody>

          </table>

        </div>

      </div>

      <div class="col-sm-12 text-center">

        <?php echo e($cursos->setPath('misforosdocente')->links()); ?>


      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principaldocente', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>