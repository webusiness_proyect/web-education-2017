<?php $__env->startSection('titulo'); ?>

  <title>Usuarios</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">
          <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-user" aria-hidden="true"></div>Usuarios</h1>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>
        <div class="col-sm-12 panel panel-default">

          <br/>
           <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

        <?php if (\Entrust::can('crear-usuario')) : ?>

        <?php echo link_to_route('usuarios.create', $title = 'Nuevo Usuario', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus']); ?>


        <br>

        <br>

        <?php endif; // Entrust::can ?>
        <?php echo $__env->make('usuarios.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <?php if(count($usuarios) == 0): ?>

          <p class="text-info">

            No se han registrado usuarios aun.

          </p>



        </center>



        <?php else: ?>

          <div class="table-responsive">

            <table class="table table-hover">

              <thead>

                <tr>

                  <th>

                    CODIGO

                  </th>

                  <th>

                    NOMBRE

                  </th>

                   <th>

                    CORREO

                  </th>

                  <th>

                    TIPO DE USUARIO

                  </th>

                  <th>

                    EDITAR

                  </th>

                </tr>

              </thead>

              <tbody>

                <?php foreach($usuarios as $key => $usuario): ?>

                  <tr>

                    <td>

                      <?php echo e(mb_strtoupper($usuario->id)); ?>


                    </td>

                    <td>

                      <?php echo e(mb_strtoupper($usuario->nombre)); ?>


                    </td>

                    <td>

                      <?php echo e($usuario->correo); ?>


                    </td>

                     <td>

                      <?php echo e($usuario->rol); ?>


                    </td>

                    <td>

                    <?php echo link_to_route('usuarios.edit', $title = 'Editar', $parameters = $usuario->id, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>


                  </td>

                  </tr>

                <?php endforeach; ?>

              </tbody>

            </table>

            <div class="text-center">

            <?php echo $usuarios->links(); ?>


          </div>

          </div>

        <?php endif; ?>

      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>