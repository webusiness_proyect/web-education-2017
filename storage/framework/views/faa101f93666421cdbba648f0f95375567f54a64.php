<?php $__env->startSection('titulo'); ?>

  <title>Mis Cursos Asignados</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center">Mis Cursos Asignados</h1>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      </div>

      <div class="col-sm-12">

        <div class="table-responsive">

          <table class="table table-hover table-bordered">

            <thead>

              <tr>

                <th>

                  NO

                </th>

                <th>

                  CURSO

                </th>

                <th>

                  NIVEL

                </th>

                <th>

                  GRADO

                </th>

                <th>

                  CARRERA

                </th>

                <th>

                  SECCION

                </th>

                <th>

                  PLAN

                </th>

                <th>

                  JORNADA

                </th>

                <th>

                  ACCIONES

                </th>

              </tr>

            </thead>

            <tbody>

              <?php foreach($cursos as $key => $curso): ?>

                <tr>

                  <td>

                    <?php echo e($key+1); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($curso->nombre_area)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($curso->nombre_nivel)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($curso->nombre_grado)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($curso->nombre_carrera)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($curso->nombre_seccion)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($curso->nombre_plan)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($curso->nombre_jornada)); ?>


                  </td>

                  <td>

                  <?php /*   <select class="form-control" onchange="window.location.href=this.value">

                      <option value="">Seleccione...</option>

                      <option value="/docente/create?a=<?php echo e($curso->id_asignacion_area); ?>&b=<?php echo e($curso->id_asignacion_docente); ?>">Zona</option>

                      <option value="/foros?a=<?php echo e($curso->id_asignacion_area); ?>&b=<?php echo e($curso->id_asignacion_docente); ?>">Foros</option>

                      <option value="/foros?a=<?php echo e($curso->id_area); ?>&b=<?php echo e($curso->id_asignacion_docente); ?>&c=<?php echo e($curso->id_asignacion_area); ?>">Actividades</option>

                    </select>

                    */ ?>



                    <!-- Single button -->

                <div class="btn-group">

                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                    Seleccione... <span class="caret"></span>

                  </button>

                  <ul class="dropdown-menu">

                    <li><a href="/docente/create?a=<?php echo e($curso->id_asignacion_area); ?>&b=<?php echo e($curso->id_asignacion_docente); ?>">Notas</a></li>

                    <li><a href="/foros?a=<?php echo e($curso->id_asignacion_area); ?>&b=<?php echo e($curso->id_asignacion_docente); ?>">Foros</a></li>

                    <li><a href="/Actividad?a=<?php echo e($curso->id_area); ?>&b=<?php echo e($curso->id_asignacion_docente); ?>&c=<?php echo e($curso->id_asignacion_area); ?>">Actividades y tareas</a></li>

                    <li role="separator" class="divider"></li>

                    <li><a href="#">Reportes</a></li>

                  </ul>

                </div>



                    <?php /* <?php echo link_to_route('docente.create', $title = ' Zona', $parameters = ['a'=>$curso->id_asignacion_area,'b'=>$curso->id_asignacion_docente], $attributes = ['class'=>'btn btn-primary fa fa-edit']); ?>


                    */ ?>

                  </td>

                </tr>

              <?php endforeach; ?>

            </tbody>

          </table>

        </div>

      </div>

      <div class="col-sm-12 text-center">

        <?php echo e($cursos->setPath('docente')->links()); ?>


      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>