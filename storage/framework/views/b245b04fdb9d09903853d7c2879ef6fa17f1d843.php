<?php $__env->startSection('titulo'); ?>



  <title> Actividades y Tareas </title>



<?php $__env->stopSection(); ?>







<?php $__env->startSection('cuerpo'); ?>



  <div id="page-wrapper">



    <div class="row">



      <div class="col-sm-12">



        <h1 class="page-header text-center"> <div class="fa fa-pencil-square" aria-hidden="true"></div>ACTIVIDADES Y TAREAS DE ESTE CURSO </h1>
        <h3 class="text-center"><div class="glyphicon glyphicon-cog"></div>   <?php echo e(mb_strtoupper($unidad->nombre_unidad)); ?></h3>




        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>



      </div>







      <div class="col-sm-12 panel panel-default">







        <p>

         <center>   
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>




       







          <?php echo link_to_route('Actividad.create', $title = ' Nueva actividad', $parameters = ['a'=>$curso], $attributes = ['class'=>'btn btn-primary fa fa-plus']); ?>




        </p>



        <?php if(count($actividades) == 0): ?>



          



        <?php else: ?>
        </center>



          <div class="table-responsive">



            <table class="table table-hover table-bordered">



              <thead>



                <tr>



                  <th>



                    NO



                  </th>



                  <th>



                    NOMBRE ACTIVIDAD



                  </th>



                  <th>



                    DESCRIPCION ACTIVIDAD



                  </th>



                  <th>



                    FECHA DE INICIO



                  </th>



                  <th>



                    FECHA ENTREGA



                  </th>



                  <th>



                    NOTA MAXIMA TOTAL



                  </th>



                  <th>



                    EDITAR



                  </th>



                  <th>



                    VER ENTREGADOS



                  </th>



                </tr>



              </thead>



              <tbody>



                <?php foreach($actividades as $key=>$actividad): ?>



                  <tr>



                    <td>



                        <?php echo e($key+1); ?>




                    </td>



                    



                    <td>



                    <?php echo e(mb_strtoupper($actividad->nombre_actividad)); ?>




                  </td>



                  <td>



                    <?php echo e(mb_strtoupper($actividad->descripcion_actividad)); ?>




                  </td>



                  <td>



                   <?php echo e(mb_strtoupper($actividad->fecha_inicio)); ?>




                  </td>



                  <td>



                    <?php echo e(mb_strtoupper($actividad->fecha_entrega)); ?>




                  </td>



                  <td>



                    <?php echo e(mb_strtoupper($actividad->nota_total)); ?>




                  </td>



                    <td>



                       <?php echo link_to_route('Actividad.edit', $title = 'Editar Actividad', $parameters = ['actividad'=>$actividad->id_actividad], $attributes = ['class'=>'btn btn-warning fa fa-edit']); ?>




                    </td>



                    <td>



                      <a class="btn btn-success fa fa-eye" href="misrespuestasactividades/?a=<?php echo e($actividad->id_actividad); ?>">Ver tareas entregadas</a>

                      

                      <!--

                      <?php echo link_to_route('misrespuestasactividades.index', $title = 'Ver Entregas', $parameters = ['a'=>$actividad->id_actividad], $attributes = ['class'=>'btn btn-info ']); ?>


                      -->

                    </td>



                  </tr>



                <?php endforeach; ?>



              </tbody>



            </table>





             <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">



          </div>





        <?php endif; ?>



      </div>
       <?php foreach($total as $key=>$total1): ?>

        <?php if($total=mb_strtoupper($total1->total)> 100): ?>

                  <h1 class="text-danger">
                  <center>

                   NOTA TOTAL: <?php echo e($total=mb_strtoupper($total1->total)); ?> </br>
                     </h1>
                  </center>
                     <?php else: ?>

                 <h1 class="text-success">
                  <center>
                   NOTA TOTAL: <?php echo e($total=mb_strtoupper($total1->total)); ?> </br>
                   </h1>
                  </center>

                 
                 <?php endif; ?>
                   



            <?php endforeach; ?>



    </div>



  </div>



<?php $__env->stopSection(); ?>




<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>