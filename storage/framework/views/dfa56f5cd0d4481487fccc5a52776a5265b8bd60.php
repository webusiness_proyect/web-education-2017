<?php $__env->startSection('titulo'); ?>


  <title> Roles</title>


<?php $__env->stopSection(); ?>





<?php $__env->startSection('cuerpo'); ?>


  <div id="page-wrapper">





      <div class="row">


          <div class="col-lg-12">

          <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-sitemap" aria-hidden="true"></div>Roles disponibles</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra roles, en los cuales puedes configurar o personalizar el acceso a los modulos de los usuarios
            </h4>

          
           
          </center>
        </div>
        <div class="alert alert-warning alert-dismissable">
          <center>

            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-info btn btn-default" aria-hidden="true"></i><STRONG>TEN CUIDADO AL PERSONALIZAR ESTA OPCION YA QUE PUEDES DAR ACCESO A USUARIOS NO AUTORIZADOS 
            </STRONG>
           

            
          </CENTER>
    
        </div>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>
        <div class="col-sm-12 panel panel-default">

          <br/>
           <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>



            <?php if (\Entrust::can('crear-rol')) : ?>


            <?php echo link_to_route('roles.create', $title = "Nuevo Rol", $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus']); ?>



            <br>


            <br>


            <?php endif; // Entrust::can ?>


            <?php if(count($roles) == 0): ?>


              <p class="text-info">


                No se han registrado roles aun.


              </p>
              
          </center>


            <?php else: ?>


                <div class="table-responsive">


                  <table class="table table-hover table-bordered">


                    <thead>


                      <tr>


                        <th>


                          NO


                        </th>


                        <th>


                          NOMBRE ROL


                        </th>


                        <?php if (\Entrust::can('editar-rol')) : ?>


                        <th>


                          ACTUALIZAR


                        </th>


                        <?php endif; // Entrust::can ?>


                        <?php if (\Entrust::can('estado-rol')) : ?>


                        <th>


                          ESTADO


                        </th>


                        <?php endif; // Entrust::can ?>


                      </tr>


                    </thead>


                    <tbody id="datosRoles">


                      <?php foreach($roles as $key => $rol): ?>


                        <tr>


                          <td>


                            <?php echo e($key+1); ?>



                          </td>


                          <td>


                            <?php echo e(mb_strtoupper($rol->name)); ?>



                          </td>


                          <?php if (\Entrust::can('editar-rol')) : ?>


                          <td>


                            <?php echo link_to_route('roles.edit', $title = 'Editar', $parameters = $rol->id, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>



                          </td>


                          <?php endif; // Entrust::can ?>


                          <?php if (\Entrust::can('estado-rol')) : ?>


                          <td>


                            <?php if($rol->state_rol == TRUE): ?>


                              <input type="checkbox" name="estado" checked value="<?php echo e($rol->id); ?>" class="toggleEstado">


                            <?php else: ?>


                              <input type="checkbox" name="estado" value="<?php echo e($rol->id); ?>" class="toggleEstado">


                            <?php endif; ?>


                          </td>


                          <?php endif; // Entrust::can ?>


                        </tr>


                      <?php endforeach; ?>


                    </tbody>


                  </table>


                  <?php echo Form::hidden('_token', csrf_token(), ['id'=>'token']); ?>



                </div>


            <?php endif; ?>





          </div>


          <!-- /.col-lg-12 -->


      </div>





  </div>


<?php $__env->stopSection(); ?>



<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>