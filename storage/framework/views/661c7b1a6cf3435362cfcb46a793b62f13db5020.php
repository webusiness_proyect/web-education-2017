<?php $__env->startSection('titulo'); ?>
  <title>Estudiantes</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Estudiantes Inscritos</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        
        <?php if(count($estudiantes) == 0): ?>
          <p class="text-info">
            No se han registrado empleados aun.
          </p>
        <?php else: ?>

          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    APELLIDOS
                  </th>
                  <th>
                    NOMBRES
                  </th>
                  <th>
                    FECHA DE NACIMIENTO
                  </th>
                  <th>
                    GENERO
                  </th>
                  <th>
                    DIRECCION
                  </th>
                  <th>
                    TELEFONO
                  </th>
                  <!--
                  <?php if (\Entrust::can('editar-empleado')) : ?>
                  <th>
                    ACTUALIZAR
                  </th>
                  <?php endif; // Entrust::can ?>
                  <?php if (\Entrust::can('estado-empleado')) : ?>
                  <th>
                    ESTADO
                  </th>
                  <?php endif; // Entrust::can ?>
                  <?php if (\Entrust::can('ver-empleado')) : ?>
                  <th>
                    MAS DATOS
                  </th>
                  <?php endif; // Entrust::can ?>-->
                </tr>
              </thead>
              <tbody id="datosEstudiante">
                <?php foreach($estudiantes as $key => $persona): ?>
                  <tr>
                    <td>
                      <?php echo e($total=$key+1); ?>

                    </td>
                    <td>
                      <?php echo e($persona->apellidos_estudiante); ?>

                    </td>
                    <td>
                      <?php echo e($persona->nombre_estudiante); ?>

                    </td>
                    <td>
                      <?php echo e($persona->fecha_nacimiento_estudiante); ?>

                    </td>
                    <td>
                      <?php echo e($persona->genero_estudiante); ?>

                    </td>
                    <td>
                      <?php echo e($persona->direccion_estudiante); ?>

                    </td>
                    <td>
                      <?php echo e($persona->telefono_casa); ?>

                    </td>
                    <!--<?php if (\Entrust::can('editar-empleado')) : ?>
                    <td>
                      <?php echo link_to_route('personas.edit', $title = 'Editar', $parameters = $persona->id_persona, $attributes = ['class'=>'btn btn-primary']); ?>

                    </td>
                    <?php endif; // Entrust::can ?>
                    <?php if (\Entrust::can('estado-empleado')) : ?>
                    <td>
                      <?php if($persona->estado_persona == true): ?>
                        <input type="checkbox" name="estado" checked value="<?php echo e($persona->id_persona); ?>" class="toggleEstado">
                      <?php else: ?>
                        <input type="checkbox" name="estado" value="<?php echo e($persona->id_persona); ?>" class="toggleEstado">
                      <?php endif; ?>
                    </td>
                    <?php endif; // Entrust::can ?>
                    <?php if (\Entrust::can('ver-empleado')) : ?>
                    <td>
                      <?php echo link_to_route('personas.show', $title = 'Ver Más', $parameters = $persona->id_persona, $attributes = ['class'=>'btn btn-info']); ?>

                    </td>
                    <?php endif; // Entrust::can ?>
                    -->
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            <h3 class="page-header text-center"> total de estudiantes <?php echo e($total); ?> </h3>
            <?php echo Form::hidden('_token', csrf_token(), ['id'=>'token']); ?>

          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>