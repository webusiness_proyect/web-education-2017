<?php $__env->startSection('titulo'); ?>

  <title>Mis Cursos Asignados</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

      <di <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-book" aria-hidden="true"></div> - - Mis Actividades y tareas </h1>
        <h4 class="text-center"><div class="glyphicon glyphicon-cog"></div>   <?php echo e(mb_strtoupper($unidad->nombre_unidad)); ?></h4>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA TAREAS</strong> y <STRONG>CALIFICA TAREAS </STRONG> en tus cursos asignados, para que tus alumnos puedan realizarlas en horario normal de clases o en sus casas
            </h4>

          
           
          </center>

          <div class="alert alert-warning alert-dismissable">
          <center>

            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-info btn btn-default" aria-hidden="true"></i><STRONG> ESTAS TAREAS PUEDEN SER ENTREGADAS EN UN ARCHIVO DIGITAL EN DOCUMENTO DE WORD, PDF, HOJA DE EXCEL, DIAPOSITIVA DE POWER POINT O IMAGEN PNG O JPG</a> </STRONG><br/>
           

            
          </CENTER>
    
        </div>
        </div>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>
        

           <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

       
      </center>
         <?php echo $__env->make('docentes.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="col-sm-12 panel panel-default">

          <br/>



        <div class="table-responsive">

          <table class="table table-hover table-bordered">

            <thead>

              <tr>

               
               <th>

                 <h4><STRONG><CENTER> OPCIONES</CENTER></h4></STRONG>

                </th>

                <th>

                 <h4><STRONG><CENTER> CURSO</CENTER></h4></STRONG>

                </th>

                <th>

                 <h4><STRONG><CENTER> PENSUM </CENTER></h4></STRONG>

                </th>

                

                <th>

                  <h4><STRONG><CENTER>CARRERA</CENTER></h4></STRONG>

                </th>

                <th>

                  <h4><STRONG><CENTER>SECCION</CENTER></h4></STRONG>

                </th>
                

              </tr>

            </thead>

            <tbody>

              <?php foreach($cursos as $key => $curso): ?>

                <tr>

                   <td>

                 


                    <!-- Single button -->

                    <center>


        

                    <a href="/Actividad?a=<?php echo e($curso->id_area); ?>&b=<?php echo e($curso->id_asignacion_docente); ?>&c=<?php echo e($curso->id_asignacion_area); ?>" class="btn btn-warning"><div class="fa fa-pencil-square"></div>Tareas y actividades</a>

                    
                    </center>

            

             


                    <?php /* <?php echo link_to_route('docente.create', $title = ' Zona', $parameters = ['a'=>$curso->id_asignacion_area,'b'=>$curso->id_asignacion_docente,'c'=>$curso->id_unidad], $attributes = ['class'=>'btn btn-primary fa fa-edit']); ?>


                    */ ?>

                  </td>

                  <td>

                    <b><?php echo e(mb_strtoupper($curso->nombre_area)); ?></b>

                  </td>

                  <td>

                      <?php echo e(mb_strtoupper($curso->nombre_grado)); ?>   
                      <?php echo e(mb_strtoupper($curso->nombre_nivel)); ?>    
                      <?php echo e(mb_strtoupper($curso->nombre_plan)); ?>

                      <?php echo e(mb_strtoupper($curso->nombre_jornada)); ?>


                  </td>
                  <td>
                  <?php echo e(mb_strtoupper($curso->nombre_carrera)); ?>

                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($curso->nombre_seccion)); ?>


                  </td>

                 

                </tr>

              <?php endforeach; ?>

            </tbody>

          </table>

        </div>

      </div>

      <div class="col-sm-12 text-center">

        <?php echo e($cursos->setPath('misactividadesdocente')->links()); ?>


      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>