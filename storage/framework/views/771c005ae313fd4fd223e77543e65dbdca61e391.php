<?php $__env->startSection('titulo'); ?>



  <title>Mis Cursos</title>



<?php $__env->stopSection(); ?>







<?php $__env->startSection('cuerpo'); ?>



  <div id="page-wrapper">



    <div class="row">



      <div class="col-sm-12">



        <h1 class="page-header text-center"><div class="glyphicon glyphicon-th"></div>  Mis Cursos</h1>



        <h3 class="text-center"><div class="glyphicon glyphicon-cog"></div>   <?php echo e(mb_strtoupper($unidad->nombre_unidad)); ?></h3>



        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <?php echo $__env->make('estudiantes.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>




      </div>



      <div class="col-sm-12 panel panel-default">



        <div class="table-responsive">



          <table class="table table-hover table-bordered">



            <thead>



              <tr>



                <th>



                  NO



                </th>



                <th>



                  CURSO



                </th>

                  <th>



                  GRADO



                </th>


                <th>



                  NIVEL



                </th>



                <th>



                  ACCIONES



                </th>



              </tr>



            </thead>



            <tbody>



              <?php foreach($cursos as $key => $curso): ?>



                <tr>



                  <td>



                    <?php echo e($curso->id_area); ?>




                  </td>



                  <td>



                    <?php echo e(mb_strtoupper($curso->nombre_area)); ?>




                  </td>


                  <td>



                    <?php echo e(mb_strtoupper($curso->nombre_grado)); ?>




                  </td>

                    <td>



                    <?php echo e(mb_strtoupper($curso->nombre_nivel)); ?>




                  </td>



                  <td>



                    <!-- Single button -->



                    <div class="btn-group">



                      <button type="button" class="btn btn-success dropdown-toggle fa fa-bullhorn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">



                        Seleccione... <span class="caret"></span>



                      </button>



                      <ul class="dropdown-menu">

                        <li><a href="/MisNotas?a=<?php echo e($curso->id_area); ?>&b=<?php echo e($curso->id); ?>"><div class="fa fa-trophy"></div>notas de este curso</a></li>

                        <li><a href="/foros?a=<?php echo e($curso->id_asignacion_area); ?>"><div class="fa fa-comments"></div> Foros de este curso</a></li>

                        <li><a href="/misactividades?a=<?php echo e($curso->id_area); ?>"><div class="fa fa-pencil-square"></div> Tareas de este curso</a></li>



                        



                        



                      



                    </div>



                  </td>



                </tr>
                



              <?php endforeach; ?>



            </tbody>



          </table>

           <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">

          <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          <div class="text-center">

            <?php echo $cursos->links(); ?>


          </div>



        </div>



      </div>



    </div>



  </div>



<?php $__env->stopSection(); ?>




<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>