<div class="form-group">
  <?php echo Form::label('nivel', 'Nivel*', ['class'=>'col-sm-2 control-label']); ?>
  <div class="col-sm-10">
    <?php echo Form::select('id_nivel', $niveles, null, ['class'=>'form-control', 'placeholder'=>'Seleccione nivel...']); ?>
  </div>
</div>
<div class="form-group">
  <?php echo Form::label('plan', 'Plan*', ['class'=>'col-sm-2 control-label']); ?>
  <div class="col-sm-10">
    <?php echo Form::select('id_plan', $planes, null, ['class'=>'form-control', 'placeholder'=>'Seleccione plan...']); ?>
  </div>
</div>
<div class="form-group">
  <?php echo Form::label('jornada', 'Jornada*', ['class'=>'col-sm-2 control-label']); ?>
  <div class="col-sm-10">
    <?php echo Form::select('id_jornada', $jornadas, null, ['class'=>'form-control', 'placeholder'=>'Seleccione jornada...']); ?>
  </div>
</div>
<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" name="guardar" class="btn btn-success"><span class="fa fa-save"></span> Registrar</button>
  </div>
</div>
