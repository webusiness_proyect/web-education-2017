<?php $__env->startSection('titulo'); ?>
  <title>Niveles</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel-body ">
        <h1 class="page-header text-center"> <div class="fa fa-sliders" aria-hidden="true"></div>Niveles</h1>
        <div class="panel panel-default">
           <div class="row">
            <div class="col-sm-10">
           
            <center><a href="#demo"  data-toggle="collapse"><div class="fa fa-question-circle fa-2x "></div><H4><STRONG>INFORMACION</STRONG></H4></a></center>
            <div id="demo" class="collapse">
             
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra niveles educativos para tu institución disponibles en tu region ejemplo:<strong>PRIMARIA</strong>
            </h4>
          </center>
        </div>
      </div>
      </div>
      </div>
      </div>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12 panel panel-default">
        <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

        <?php if (\Entrust::can('crear-nivel')) : ?>
        <?php echo link_to_route('niveles.create', $title = 'Nuevo Nivel', $parameters =  null, $attributes =['class'=>'btn btn-primary fa fa-plus-square']); ?>

        <br>
        <br>
        <?php endif; // Entrust::can ?>
        <?php if(count($niveles) == 0): ?>
          <p class="text-info">
            No se han registrado niveles aun...
          </p>
        </center>
        <?php else: ?>
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  NOMBRE NIVEL
                </th>
                <?php if (\Entrust::can('editar-nivel')) : ?>
                <th>
                  ACTUALIZAR
                </th>
                <?php endif; // Entrust::can ?>
                <?php if (\Entrust::can('estado-nivel')) : ?>
                <th>
                  ESTADO
                </th>
                <?php endif; // Entrust::can ?>
              </tr>
            </thead>
            <tbody id="datosNiveles">
              <?php foreach($niveles as $key => $nivel): ?>
                <tr>
                  <td>
                    <?php echo e($key+1); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($nivel->nombre_nivel)); ?>

                  </td>
                  <?php if (\Entrust::can('editar-nivel')) : ?>
                  <td>
                    <?php echo link_to_route('niveles.edit', $title = 'Editar', $parameters = $nivel->id_nivel, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>

                  </td>
                  <?php endif; // Entrust::can ?>
                  <?php if (\Entrust::can('estado-nivel')) : ?>
                  <td>



                 

                     <?php if($nivel->estado_nivel == TRUE): ?>
                              <input type="checkbox" name="estado" checked value="<?php echo e($nivel->id_nivel); ?>" class="toggleEstado">
                            <?php else: ?>
                              <input type="checkbox" name="estado" value="<?php echo e($nivel->id_nivel); ?>" class="toggleEstado">
                            <?php endif; ?>
                  </td>
                  <?php endif; // Entrust::can ?>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
        <?php endif; ?>
      </div>
    </div>
    <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>