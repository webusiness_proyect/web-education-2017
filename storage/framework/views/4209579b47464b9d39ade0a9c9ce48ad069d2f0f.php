

<?php $__env->startSection('titulo'); ?>
  <title>Editar Plan</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Editar Plan</h1>
        <!-- Errores del formulario del lado del servidor -->
        <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <?php echo Form::model($plan, ['route'=>['planes.update', $plan->id_plan], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'nplan']); ?>
        <!-- Campos del formuario-->
        <?php echo $__env->make('planes.form.campos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo Form::close(); ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>