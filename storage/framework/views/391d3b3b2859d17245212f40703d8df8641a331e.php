<?php $__env->startSection('titulo'); ?>

  <title>Actividades</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center"><div class="fa fa-eye" aria-hidden="true"></div><div class="fa fa-pencil-square" aria-hidden="true"></div>TAREAS RESPONDIDAS</h1>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      </div>

      <div class="col-sm-12 panel panel-default">
         <center>   
           <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>


        <p>

          

        </p>

        <?php if(count($ractividades) == 0): ?>

          <p class="text-info">
        


            No se han encontrado actividades para este curso...
            </center>

          </p>

        <?php else: ?>

          <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>
                  <th>

                    ESTADO 

                  </th>

                  <th>

                   NOMBRE

                  </th>

                  

                  <th id="nota">

                    CALIFICACION

                  </th>

                 




                  <th>

                    FECHA ENTREGA

                  </th>

                  <th>

                    COMENTARIOS

                  </th>

                   <th>

                    ARCHIVO

                  </th>
                   <th >

                    NOTA

                  </th>
                 

                 

                </tr>

              </thead>

              <tbody>

                <?php foreach($ractividades as $key=>$actividad): ?>

                <?php foreach($notas as $key=>$maximanota): ?>

                  <tr>

                    

                        
                      <?php if($actividad->estado_entrega_actividad=="CALIFICADO"): ?>
                      <td class="btn-success">  <li class="fa fa-check-circle"> CALIFICADO </li></td>
                      <?php else: ?>
                      <td class="btn-danger">  <li class="fa fa-info-circle"> NO CALIFICADA </li></td>
                      <?php endif; ?>

                    

                  <td>
                    <?php echo e(mb_strtoupper($actividad->name)); ?>


                  </td>

                  


                   


                  <td>

                    <H4><?php echo e($nota=mb_strtoupper($actividad->calificacion)); ?></H4>


                  

                  







                  <td>

                    <?php echo e(mb_strtoupper($actividad->fecha_entrega)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($actividad->comentarios)); ?>


                  </td>

                  <td>

                    
                    <?php echo Html::link($actividad->url, 'DESCARGAR ', $attributes = ['class'=>'btn btn-danger  fa fa-download']); ?>

                  </td>
                  

                    <td>



                      <?php echo link_to_route('misrespuestasactividades.edit', $title = 'CALIFICAR',  $parameters = ['a'=>$actividad->id_respuesta_actividad], $attributes = ['class'=>'btn btn-info  fa fa-check-square']); ?>


                    </td>

                    <?php echo e(Form::hidden('post_id', $nota_total=($maximanota->nota_total))); ?>

                    

                  </td>

                  <?php if($nota<=$nota_total): ?>

                   <?php else: ?>
                  <td class="btn-warning">

                    NOTA MAXIMA<li class="fa fa-exclamation-triangle"><?php echo e($nota_total=mb_strtoupper($maximanota->nota_total)); ?></li>

                  </td>
                  <?php endif; ?>

                    

                        <?php if($nota>$nota_total): ?>


                        <td class="btn-danger"> <li class=" fa fa-remove">NOTA MALA</li> </td>

                        <?php else: ?>

                        

                        <?php endif; ?>



                   
                  </tr>

                <?php endforeach; ?>

                <?php endforeach; ?>


              </tbody>

            </table>

               

             <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">

          </div>

        <?php endif; ?>
      
      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>