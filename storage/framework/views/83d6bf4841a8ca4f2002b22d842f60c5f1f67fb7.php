<?php $__env->startSection('titulo'); ?>
  <title>Salones</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
         <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-home " aria-hidden="true"></div> Salones de clases disponibles </h1>
         <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong>y administra salones de clases disponibles en tu institucion ejemplo:<strong> SALON:1</strong>
            </h4>
           
          </center>
        </div>
    
         

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
      </div>
      <div class="col-sm-12 panel panel-default">

        <center>
        <br/>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

        <?php if (\Entrust::can('crear-salon')) : ?>
        <?php echo link_to_route('salones.create', $title = 'Nuevo Salón', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square']); ?>



        <?php endif; // Entrust::can ?>
        <?php echo $__env->make('salones.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php if(count($salones) == 0): ?>
          <p class="text-info">
            No se han registrado salones aun.
          </p>
        </center>
        <?php else: ?>
        <div class="col-sm-12 panel panel-default">
          <table class="table table-hover table-bordered">
            <thead>
              <th>
                NO
              </th>
              <th>
                NOMBRE
              </th>
              <?php if (\Entrust::can('editar-salon')) : ?>
              <th>
                ACTUALIZAR
              </th>
              <?php endif; // Entrust::can ?>
              <?php if (\Entrust::can('estado-salon')) : ?>
              <th>
                ESTADO
              </th>
              <?php endif; // Entrust::can ?>
            </thead>
            <tbody id="datosSalones">
              <?php foreach($salones as $key => $salon): ?>
                <tr>
                  <td>
                    <?php echo e($key+1); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($salon->nombre_salon)); ?>

                  </td>
                  <?php if (\Entrust::can('editar-salon')) : ?>
                  <td>
                    <?php echo link_to_route('salones.edit', $title = 'Editar', $parameters = $salon->id_salon, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>

                  </td>
                  <?php endif; // Entrust::can ?>
                  <?php if (\Entrust::can('estado-salon')) : ?>
                  <td>

                  <?php if($salon->estado_salon == TRUE): ?>
                              <input type="checkbox" name="estado" checked value="<?php echo e($salon->id_salon); ?>" class="toggleEstado">
                            <?php else: ?>
                              <input type="checkbox" name="estado" value="<?php echo e($salon->id_salon); ?>" class="toggleEstado">
                            <?php endif; ?>



                  </td>
                  <?php endif; // Entrust::can ?>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
          <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
           <div class="text-center">

            <?php echo $salones->links(); ?>


          </div>
        <?php endif; ?>

      </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>