 <ul class="<?php echo e($class); ?>" id="side-menu">
   <li>
       <a href="<?php echo e(url('/')); ?>"><i class="fa fa-home fa-fw"></i> MENU</a>
   </li>
    <?php foreach($items as $item): ?>
        <li <?php if($item['class']): ?> class="<?php echo e($item['class']); ?>" <?php endif; ?> id="menu_<?php echo e($item['id']); ?>">
            <?php if(empty($item['submenu'])): ?>
                <a href="#">
                    <?php echo e($item['title']); ?>

                </a>
            <?php else: ?>
              <?php /*   <a href="<?php echo e($item['url']); ?>" class="dropdown-toggle" data-toggle="dropdown"> */ ?>
              <a href="<?php echo e(url('/').$item['url']); ?>">
                    <?php echo e($item['title']); 
                    if (e($item['title'])=='Persona')
                    {?>
                        <i class="fa fa-user"></i>
                        <?php
                    }
                    if(e($item['title'])=='Usuario')
                    {?>
                        <i class="fa fa-users"></i>
                        <?php
                    }
                    if(e($item['title'])=='Institucional')
                    {?>
                        <i class="fa fa-university"></i>
                        <?php
                    }
                     if(e($item['title'])=='Docente')
                    {?>
                        <i class="fa fa-book"></i>
                        <?php
                    }
                     if(e($item['title'])=='Estudiante')
                    {?>
                        <i class="fa fa-graduation-cap"></i>
                        <?php
                    }
                    ?>


                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <?php foreach($item['submenu'] as $subitem): ?>
                        <li>
                            <a href="<?php echo e($subitem['url']); ?>"><?php echo e($subitem['title']); ?>
                            <?php if (e($subitem['title'])=='Carreras')
                                {?>
                                    <i class="fa fa-book"></i>
                                    <?php
                                }
                                if (e($subitem['title'])=='Niveles')
                                {?>
                                    <i class="fa fa-sitemap"></i>
                                    <?php
                                }
                            ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>
</ul>
