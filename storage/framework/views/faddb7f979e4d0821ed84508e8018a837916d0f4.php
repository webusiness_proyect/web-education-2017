<?php $__env->startSection('titulo'); ?>

  <title>Nueva Horario</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

<div id="page-wrapper">

  <div class="row">

    <div class="col-sm-12">

      <h1 class="page-header text-center"> <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>    Nuevo horario</h1>

      <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <p>

        Nota: Todos los campos con (*) son obligatorios.

      </p>

    </div>



    <div class="col-sm-12">

      <?php echo Form::model($dato, ['route'=>['horarios.update', $dato->idcalendario], 'method'=>'PUT', 'class'=>'form-horizontal', 'id'=>'nhorario']); ?>


        

        <?php echo $__env->make('horarios.form.horarios', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      <?php echo Form::close(); ?>


    </div>
       <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>

</div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>