<?php $__env->startSection('titulo'); ?>



  <title>Mis Actividades</title>



<?php $__env->stopSection(); ?>







<?php $__env->startSection('cuerpo'); ?>



  <div id="page-wrapper">



    <div class="row">



      <div class="col-sm-12">



        <h1 class="page-header text-center"><div class="glyphicon glyphicon-th"></div>  Mis Actividades y tareas</h1>



        <h3 class="text-center"><div class="glyphicon glyphicon-cog"></div>   <?php echo e(mb_strtoupper($unidad->nombre_unidad)); ?></h3>



        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <?php echo $__env->make('estudiantes.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>




      </div>



      <div class="col-sm-12 panel panel-default">



        <div class="table-responsive">



          <table class="table table-hover table-bordered">



            <thead>



              <tr>




               <th>

                 <h4> MIS ACTIVIDADES</h4>

                </th>


                <th>



                  CURSO



                </th>

                  <th>



                  GRADO



                </th>


                <th>



                  NIVEL



                </th>






              </tr>



            </thead>



            <tbody>



              <?php foreach($cursos as $key => $curso): ?>



                <tr>



                  <td>

                  <center>


                   

                      
                        <a href="/misactividades?a=<?php echo e($curso->id_area); ?>"  class="btn btn-warning"><div class="fa fa-pencil-square"></div> Tareas de <?php echo e(mb_strtoupper($curso->nombre_area)); ?></a>



                        



                  </center>

                  </td>



                  <td>



                    <?php echo e(mb_strtoupper($curso->nombre_area)); ?>




                  </td>


                  <td>



                    <?php echo e(mb_strtoupper($curso->nombre_grado)); ?>




                  </td>

                    <td>



                    <?php echo e(mb_strtoupper($curso->nombre_nivel)); ?>




                  </td>



              



                </tr>
                



              <?php endforeach; ?>



            </tbody>



          </table>

           <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">

          <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          <div class="text-center">

            <?php echo $cursos->links(); ?>


          </div>



        </div>



      </div>



    </div>



  </div>



<?php $__env->stopSection(); ?>




<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>