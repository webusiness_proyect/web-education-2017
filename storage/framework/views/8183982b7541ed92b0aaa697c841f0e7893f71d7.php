<?php $__env->startSection('titulo'); ?>


  <title>Pensum Grados</title>


<?php $__env->stopSection(); ?>





<?php $__env->startSection('cuerpo'); ?>


  <div id="page-wrapper">


    <div class="row">


      <div class="col-sm-12">


      <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-bookmark" aria-hidden="true"></div>Pensum de grados</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CONFIGURA </strong> tu pensum, selecciona un grado, asignales cursos y asignales un salon ejemplo:<strong>*PRIMERO PRIMARIA, CURSO DE MATEMATICAS, INGLES, ETC EN EL SALON A</strong>
            </h4>
           
          </center>
        </div>
          <h6>
         <CENTER>
          * DEBERAS DE CONFIGURAR EL <a href="<?php echo e(url('/asignarniveles')); ?>">PLAN NIVELES</a> PREVIAMENTE

            
          </CENTER>
          </h6>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>


      </div>


 
       <div class="col-sm-12 panel panel-default">

          <br/>
           <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>



        <?php if (\Entrust::can('crear-pensum')) : ?>


        <?php echo link_to_route('pensum.create', $title = 'Nuevo Pensum', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square']); ?>


        <?php echo $__env->make('asignacionareas.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


        <br>


        <br>


        <?php endif; // Entrust::can ?>


        <?php if(count($pensum) == 0): ?>


          <p class="text-info">


            No se ha registrado ningun pensum aun.


          </p>
          </center>


        <?php else: ?>


          <div class="col-sm-12 panel panel-default table-responsive">


            <table class="table table-hover">


              <thead>


                <tr>


                  <th>


                    NO


                  </th>


                  <th>


                    GRADO


                  </th>


                  <th>


                    NIVEL


                  </th>


                  <th>


                    CARRERA


                  </th>


                  <?php if (\Entrust::can('editar-pensum')) : ?>


                  <th>


                    ACTUALIZAR


                  </th>


                  <?php endif; // Entrust::can ?>


                  <?php if (\Entrust::can('ver-pensum')) : ?>


                  <th>


                    VER


                  </th>


                  <?php endif; // Entrust::can ?>

                  




                </tr>


              </thead>


              <tbody id="datosPensum">


                <?php foreach($pensum as $key => $p): ?>


                  <tr>


                    <td>


                      <?php echo e($key+1); ?>



                    </td>


                    <td>


                      <?php echo e(mb_strtoupper($p->nombre_grado)); ?>



                    </td>


                    <td>


                      <?php echo e(mb_strtoupper($p->nombre_nivel)); ?>



                    </td>


                    <td>


                      <?php echo e(mb_strtoupper($p->nombre_carrera)); ?>



                    </td>


                    <?php if (\Entrust::can('editar-pensum')) : ?>


                    <td>


                      <?php echo link_to_route('pensum.edit', $title = 'Editar', $parameters = $p->id_nivel_grado, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>



                    </td>


                    <?php endif; // Entrust::can ?>


                    <?php if (\Entrust::can('ver-pensum')) : ?>


                    <td>


                      <?php echo link_to_route('pensum.show', $title = 'Ver Pensum', $parameters = $p->id_nivel_grado, $attributes = ['class'=>'btn btn-default fa fa-eye']); ?>



                    </td>


                    <?php endif; // Entrust::can ?>






                  </tr>


                <?php endforeach; ?>


              </tbody>


            </table>

            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">

              <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <div class="text-center">

            <?php echo $pensum->links(); ?>


          </div>


          </div>


        <?php endif; ?>


      </div>


    </div>


  </div>


<?php $__env->stopSection(); ?>



<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>