<div class="form-group">
  <?php echo Form::label('nombre', 'Nombre*', ['class'=>'col-sm-2 control-label']); ?>
  <div class="col-sm-10">
    <?php echo Form::text('nombre_area', null, ['class'=>'form-control', 'placeholder'=>'Nombre del curso...']); ?>
  </div>
</div>
<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" name="guardar" class="btn btn-success"><span class="fa fa-save"></span> Registrar</button>
  </div>
</div>
