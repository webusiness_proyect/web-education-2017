<?php $__env->startSection('titulo'); ?>

  <title>Asignación Docentes</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">
 <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-flag" aria-hidden="true"></div>Asignación de Cursos a  Docentes</h1>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          </div>
        <div class="col-sm-12 panel panel-default">

          <br/>
           <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>


        <?php if (\Entrust::can('crear-asignacion')) : ?>


          <?php echo link_to_route('asignaciondocente.create', $title = 'Nueva Asignación', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus']); ?>



          <br>


          <br>


        <?php endif; // Entrust::can ?>


         <?php echo $__env->make('asignaciondocente.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


        <?php if(count($docentes) == 0): ?>


          <p class="text-info">


            No se han registrado asignaciónes de docentes aun.


          </p>

          </center>
        <?php else: ?>

          <div class="table-responsive">

            <table class="table table-hover">

              <thead>

                <tr>

                  <th>

                    NO

                  </th>

                  <th>

                    DOCENTE

                  </th>

                  <th>

                    ACTUALIZAR

                  </th>

                  <th>

                    VER ASIGNACIÓN

                  </th>



                </tr>

              </thead>

              <tbody>

                <?php foreach($docentes as $key => $docente): ?>

                  <tr>

                    <td>

                      <?php echo e($key+1); ?>


                    </td>

                    <td>

                      <?php echo e(mb_strtoupper($docente->nombres_persona.' '.$docente->apellidos_persona)); ?>


                    </td>

                    <td>

                      <?php echo link_to_route('asignaciondocente.edit', $title = 'Editar Asignación', $parameters = $docente->id_persona, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>


                    </td>

                    <td>

                      <?php echo link_to_route('asignaciondocente.show', $title = 'Ver Cursos', $parameters = $docente->id_persona, $attributes = ['class'=>'btn btn-default fa fa-eye']); ?>


                    </td>

            

                  </tr>

                <?php endforeach; ?>

              </tbody>

            </table>
             <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">

          <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          <div class="text-center">

            <?php echo $docentes->links(); ?>


          </div>


          </div>

        <?php endif; ?>

      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>