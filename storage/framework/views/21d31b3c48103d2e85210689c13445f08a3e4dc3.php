<?php $__env->startSection('titulo'); ?>

  <title>Notificaciones</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center">NOTIFICACIONES ENVIADAS</h1>

        <h3 class="text-center"><?php echo e(ucwords($notificaciones[0]->titulo_notificacion.' '.$notificaciones[0]->informacion_notificacion.' '.$notificaciones[0]->fecha_create)); ?></h3>

      </div>

      <div class="col-sm-12">

        <div class="table-responsive">

          <table class="table table-hover">

            <thead>

              <th>

                NO

              </th>

              <th>

                GRADO

              </th>

              <th>

                CARRERA

              </th>

              <th>

                NIVEL

              </th>
              <th>

                JORNADA

              </th>  

              <th>

                PLAN

              </th> 
              <th>

                TIPO DE USUARIO

              </th> 
            </thead>

            <tbody>

              <?php foreach($notificaciones as $key => $p): ?>

                <tr>

                  <td>

                    <?php echo e($key+1); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($p->grado)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($p->carrera)); ?>


                  </td>
                  <td>

                    <?php echo e(mb_strtoupper($p->nivel)); ?>


                  </td>
                  <td>

                    <?php echo e(mb_strtoupper($p->jornada)); ?>


                  </td>
                  <td>

                    <?php echo e(mb_strtoupper($p->plan)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($p->name)); ?>


                  </td>                                                                        

                </tr>

              <?php endforeach; ?>

            </tbody>

          </table>

        </div>

      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>