<?php $__env->startSection('titulo'); ?>

  <title>Sistema Educativo - WB</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>



  <div id="page-wrapper">



      <div class="row">

          <div class="col-lg-12">

          

            <div>
                   <H3> <center> <div class="fa fa-user" aria-hidden="true"></div> BIENVENIDOS <?php echo e(ucwords(Auth::user()->name)); ?> </center></H3>
                </div>
               <div class="panel panel-primary">
               
                <div class="panel-footer">  
                  <H4> 
                    <center><strong>A TU SITIO WEB EDUCATIVO </strong>
                  <strong>En esta plataforma tu podras CREAR, ADMINISTRAR todos los cursos asignados por tu isntitucion, aqui puedes crear actividades como Conversaciones o foros, dejar tareas para que las resuelvan y las adjunten en un documento de word, pdf, imagen y pueda ser calificada por ti, asi como asigar punteos y muchas cosas mas</center></H4>


                  </div>
               </div>

                <div >
                   <H3> <center>ACCESO RAPIDO DEL DOCENTE</center></H3>
                </div>


               
                   <div  class="table-responsive">
                    <table class="">

                      <thead>

                        <tr>
                          <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                          <link rel="stylesheet" href="/resources/demos/style.css">
                          <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                          <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                          <script>
                            $( function() {
                              $( document ).tooltip();
                            } );
                            </script>
                         <th>
                            <a href="#" class="btn btn-success btn-lg selected" title="Graficas y estadisticas en tiempo real"><i class="fa fa-pie-chart  fa-2x" aria-hidden="true"></i><br/>Actividades Pendientes de entregar</a>
 

 
                          </th>

                           <th>   

                            <a href="<?php echo e(url('/misactividadesdocente')); ?>" class="btn btn-default btn-lg" title="Crea actividades o tareas para que tus alumnos puedan resolverlas en clase o en casa y despues adjuntar la tarea en un documento digital"><i class="fa fa-check fa-2x" aria-hidden="true"></i><br/>Actividades y tareas</a>

                          </th>

                          <th>   

                            <a href="<?php echo e(url('/mishorariosdocente')); ?>" class="btn btn-default btn-lg" title="Administre su tiempo de manera efectiva sabiendo que curso esta en este horario"><i class="fa fa-calendar fa-2x" aria-hidden="true"></i><br/>Mi horarios</a>

                          </th>

                           <th>   

                            <a href="<?php echo e(url('/docente')); ?>" class="btn btn-default btn-lg" title="Ve todos tus cursos y notas asignadas a tus cursos"><i class="fa fa-pencil fa-2x" aria-hidden="true"></i><i class="fa fa-user fa-2x" aria-hidden="true"></i><br/>Mis cursos y alumnos </a>

                          </th>

                             <th>   

                            <a href="<?php echo e(url('/misforosdocente')); ?>" class="btn btn-default btn-lg" title="cree foros y conversaciones en los que participes tu y tus alumnos sobre un tema en especial de tu curso "><i class="fa fa-comments fa-2x" aria-hidden="true"></i><br/>Mis foros y conversaciones</a>

                          </th>

                         

                            
                        </thead>

                      </table>
                    </div>
              



          </div>

          <!-- /.col-lg-12 -->

      </div>
      <br/>

        <div class="row ">

          <div class="col-lg-8 panel panel-default">

           
                  


               <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                  <script type="text/javascript">
                   google.charts.load('current', {'packages':['corechart']});
                   google.charts.setOnLoadCallback(drawChart);
                   function drawChart() {

                      var data = google.visualization.arrayToDataTable([
                       ['Task', 'Hours per Day'],
                       ['CALIFICADAS',     11],
                      
                       ['PENDIENTES DE CALIFICAR',    7]
                      ]);

                     var options = {
                        title: 'TAREAS PENDIENTE DE CALIFICAR'
                      };

                      var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                      chart.draw(data, options);
                    }
                  </script>


              <div id="piechart"></div>

             <br/>
              <div class="panel panel-warning">
                <div class="panel-heading">
                   <H4> <center> <div class="fa fa-exclamation-circle" aria-hidden="true"></div> AYUDA Y SOPORTE </center></H4>
                </div>
                <div class="panel-footer">  <H4> <center>
                  <i class="fa fa-globe"></i><a href="<?php echo e(url('http://webusiness.co/')); ?>">http://webusiness.co/ </a><br/>
                  <i class="fa fa-at"></i>correo: education@webusiness.co <br/>
                  <a href="<?php echo e(url('/tel:+5022327806')); ?>"><i class="fa fa-phone"></i></a>Tel. +50223278063<br/>
                  <i class="fa fa-map-marker"></i>Centro America, Guatemala, Zona 9, 1era. avenida 8-00.<br/>
                 <a href="<?php echo e(url('/enviomensajes')); ?>"><i class="fa fa-envelope-o"></i>envianos un mensaje</a>
              </center></H4>
                 <center>
                   
                    
                    
                    
                  
                  </center>
              </div>
              </div>

                 



          </div>

          <div class="col-lg-4">
              <center>
              <img src="<?php echo e(url('/img/docente.png')); ?>"  width="250" height="450">
              </center>
          </div>

          

          <!-- /.col-lg-12 -->

      </div>



  </div>

  <!-- /#page-wrapper -->



<?php $__env->stopSection(); ?>

<?php echo $__env->make('principaldocente', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>