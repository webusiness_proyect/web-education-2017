<?php $__env->startSection('titulo'); ?>


  <title>Puestos</title>


<?php $__env->stopSection(); ?>





<?php $__env->startSection('cuerpo'); ?>


  <div id="page-wrapper">


    <div class="row">


      <div class="col-sm-12">

      
        <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-users" aria-hidden="true"></div> <div class="fa fa-gears" aria-hidden="true"></div>Puestos de mi empresa</h1>

        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra, los puestos de empleados de tu empresa, para asignarlos a personas ejemplo:<strong>*DOCENTE</strong>
            </h4>
           
          </center>
        </div>
          <h6>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>


      </div>


      <div class="col-sm-12 panel panel-default">
      <br/>
      <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>


        <?php if (\Entrust::can('crear-puesto')) : ?>


        <?php echo link_to_route('puestos.create', $title = 'Nuevo Puesto', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus']); ?>



        <br>


        <br>


        <?php endif; // Entrust::can ?>


        <?php if(count($puestos) == 0): ?>


          <p class="text-info">


            No se han registrado puestos aun.


          </p>

        </center>


        <?php else: ?>


          <table class="table table-hover table-bordered">


            <thead>


              <tr>


                <th>


                  NO


                </th>


                <th>


                  NOMBRE


                </th>


                <?php if (\Entrust::can('editar-puesto')) : ?>


                <th>


                  ACTUALIZAR


                </th>


                <?php endif; // Entrust::can ?>


                <?php if (\Entrust::can('estado-puesto')) : ?>


                <th>


                  ESTADO


                </th>


                <?php endif; // Entrust::can ?>


              </tr>


            </thead>


            <tbody id="datosPuestos">


              <?php foreach($puestos as $key => $puesto): ?>


                <tr>


                  <td>


                    <?php echo e($key+1); ?>



                  </td>


                  <th>


                    <?php echo e(mb_strtoupper($puesto->nombre_puesto)); ?>



                  </th>


                  <?php if (\Entrust::can('editar-puesto')) : ?>


                  <th>


                    <?php echo link_to_route('puestos.edit', $title = 'Editar', $parameters = $puesto->id_puesto, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>



                  </th>


                  <?php endif; // Entrust::can ?>


                  <?php if (\Entrust::can('estado-puesto')) : ?>


                  <th>


                    <?php if($puesto->estado_puesto == true): ?>


                      <?php echo link_to_route('puestos.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success fa fa-check eliminarPuesto', 'data-id'=>$puesto->id_puesto, 'data-estado'=>$puesto->estado_puesto]); ?>



                    <?php else: ?>


                      <?php echo link_to_route('puestos.destroy', $title = 'Deshabilitado', $parameters = null, $attributes = ['class'=>'btn btn-danger fa fa-times eliminarPuesto', 'data-id'=>$puesto->id_puesto, 'data-estado'=>$puesto->estado_puesto]); ?>



                    <?php endif; ?>


                  </th>


                  <?php endif; // Entrust::can ?>


                </tr>


              <?php endforeach; ?>


            </tbody>


          </table>


          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">


        <?php endif; ?>


      </div>


      <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>


    </div>


  </div>


<?php $__env->stopSection(); ?>



<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>