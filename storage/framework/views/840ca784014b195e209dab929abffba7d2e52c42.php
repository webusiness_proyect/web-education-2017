<?php $__env->startSection('titulo'); ?>
  <title>Nueva Respuesta</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Nueva Respuesta</h1>
        <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <p>
          Nota: Todos los campos con (*) son obligatorios.
        </p>
      </div>
      <div class="col-sm-offset-2 col-sm-10">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><?php echo e($foro->titulo_foro); ?></h3>
          </div>
          <div class="panel-body">
            <?php echo $foro->mensaje_foro; ?>
          </div>
        </div>
      </div>

      <div class="col-sm-12">

        <?php echo Form::open(['route'=>'respuestasforos.store', 'method'=>'POST', 'class'=>'form-horizontal','id'=>'respuestasforos']); ?>
          <?php echo Form::hidden('foro', $foro->id_foro); ?>


          <div class="form-group">
            <?php echo Form::label('mensaje', 'Mensaje*', ['class'=>'col-sm-2 control-label']); ?>
            <div class="col-sm-10">
              <?php echo Form::textarea('mensaje_respuesta', null, ['class'=>'form-control', 'placeholder'=>'Mensaje de respuesta...', 'id'=>'descripcionForo']); ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" name="button" class="btn btn-success"><span class="fa fa-save"></span> Guardar</button>
            </div>
          </div>
        <?php echo Form::close(); ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>