<?php $__env->startSection('titulo'); ?>
  <title>Pensum Grados</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Pensum Grados</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <?php if (\Entrust::can('crear-pensum')) : ?>
        <?php echo link_to_route('pensum.create', $title = 'Nuevo Pensum', $parameters = null, $attributes = ['class'=>'btn btn-primary']); ?>
        <br>
        <br>
        <?php endif; // Entrust::can ?>
        <?php if(count($pensum) == 0): ?>
          <p class="text-info">
            No se ha registrado ningun pensum aun.
          </p>
        <?php else: ?>
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    GRADO
                  </th>
                  <th>
                    NIVEL
                  </th>
                  <th>
                    CARRERA
                  </th>
                  <?php if (\Entrust::can('editar-pensum')) : ?>
                  <th>
                    ACTUALIZAR
                  </th>
                  <?php endif; // Entrust::can ?>
                  <?php if (\Entrust::can('ver-pensum')) : ?>
                  <th>
                    VER
                  </th>
                  <?php endif; // Entrust::can ?>
                </tr>
              </thead>
              <tbody id="datosPensum">
                <?php foreach($pensum as $key => $p): ?>
                  <tr>
                    <td>
                      <?php echo e($key+1); ?>
                    </td>
                    <td>
                      <?php echo e(mb_strtoupper($p->nombre_grado)); ?>
                    </td>
                    <td>
                      <?php echo e(mb_strtoupper($p->nombre_nivel)); ?>
                    </td>
                    <td>
                      <?php echo e(mb_strtoupper($p->nombre_carrera)); ?>
                    </td>
                    <?php if (\Entrust::can('editar-pensum')) : ?>
                    <td>
                      <?php echo link_to_route('pensum.edit', $title = 'Editar', $parameters = $p->id_nivel_grado, $attributes = ['class'=>'btn btn-primary']); ?>
                    </td>
                    <?php endif; // Entrust::can ?>
                    <?php if (\Entrust::can('ver-pensum')) : ?>
                    <td>
                      <?php echo link_to_route('pensum.show', $title = 'Ver Pensum', $parameters = $p->id_nivel_grado, $attributes = ['class'=>'btn btn-warning']); ?>
                    </td>
                    <?php endif; // Entrust::can ?>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>