<?php $__env->startSection('titulo'); ?>
  <title>Editar Asignación</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Editar Asignación</h1>
      </div>
      <div class="col-sm-12">
        <h2>Docente: <small><?php echo e(mb_strtoupper($docente->nombres_persona.' '.$docente->apellidos_persona)); ?></small></h2>
        <button type="button" name="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"><span class="fa fa-plus-circle"></span> Asignar Curso</button>
        <br>
        <br>
        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>
                  NO
                </th>
                <th>
                  CURSO
                </th>
                <th>
                  GRADO
                </th>
                <th>
                  CARRERA
                </th>
                <th>
                  NIVEL
                </th>
                <th>
                  JORNADA
                </th>
                <th>
                  PLAN
                </th>
                <th>
                  ESTADO
                </th>
              </tr>
            </thead>
            <tbody id="asignacionDocenteCursos">
              <?php foreach($asignaciones as $key => $asignacion): ?>
                <tr>
                  <td>
                    <?php echo e($key+1); ?>
                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($asignacion->nombre_area)); ?>
                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($asignacion->nombre_grado)); ?>
                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($asignacion->nombre_carrera)); ?>
                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($asignacion->nombre_nivel)); ?>
                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($asignacion->nombre_jornada)); ?>
                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($asignacion->nombre_plan)); ?>
                  </td>
                  <td>
                    <?php if($asignacion->estado_asignacion_docente == true): ?>
                      <?php echo Form::checkbox('estado_asignacion', $asignacion->id_asignacion_docente, true, ['class'=>'toggleEstado']); ?>
                    <?php else: ?>
                      <?php echo Form::checkbox('estado_asignacion', $asignacion->id_asignacion_docente, false, ['class'=>'toggleEstado']); ?>
                    <?php endif; ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <?php echo Form::hidden('_token', csrf_token(), ['id'=>'token']); ?>
        </div>
      </div>
    </div>
  </div>


  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content modal-lg">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Asignar cursos</h4>
        </div>
        <div class="modal-body form-horizontal">
          <!-- ################################## -->
          <?php echo Form::hidden('id_persona', $docente->id_persona, ['id'=>'idPersona']); ?>
          <div class="form-group">
            <?php echo Form::label('plan', 'Plan*', ['class'=>'col-sm-2 control-label']); ?>
            <div class="col-sm-10">
              <?php echo Form::select('id_plan', $planes, null, ['class'=>'form-control', 'placeholder'=>'Seleccione un plan...', 'id'=>'pAsignacionDocente']); ?>
            </div>
          </div>
          <div class="form-group">
            <?php echo Form::label('jornada', 'Jornada*', ['class'=>'col-sm-2 control-label']); ?>
            <div class="col-sm-10">
              <?php echo Form::select('id_jornada', $jornadas, null, ['class'=>'form-control', 'placeholder'=>'Seleccione una jornada...', 'id'=>'jAsignacionDocente']); ?>
            </div>
          </div>
          <div class="form-group">
            <?php echo Form::label('nivel', 'Nivel*', ['class'=>'col-sm-2 control-label']); ?>
            <div class="col-sm-10">
              <?php echo Form::select('id_nivel', $niveles, null, ['class'=>'form-control', 'placeholder'=>'Seleccione un nivel...', 'id'=>'nAsignacionDocente']); ?>
            </div>
          </div>
          <div class="form-group">
            <?php echo Form::label('grado', 'Grado*', ['class'=>'col-sm-2 control-label']); ?>
            <div class="col-sm-10">
              <?php /*<?php echo Form::select('animal', array('Cats' => array('leopard' => 'Leopard'), 'Dogs' => array('spaniel' => 'Spaniel'),), null, ['class'=>'form-control', 'placeholder'=>'Seleccione un grado...']); ?>*/ ?>
              <?php echo Form::select('id_grado', [], null, ['class'=>'form-control', 'placeholder'=>'Seleccione un grado...', 'id'=>'gAsignacionDocente']); ?>
            </div>
          </div>
          <div class="form-group" >
            <?php echo Form::label('pensum', 'Pensum', ['class'=>'col-sm-2 control-label']); ?>
            <div class="col-sm-10" id="pensumGrado">

            </div>
          </div>
          <div class="form-group" >
            <?php echo Form::label('asignacion', 'Asignación*', ['class'=>'col-sm-2 control-label']); ?>
            <div class="col-sm-10">

            <div class="panel-group">
              <div class="panel panel-default">
               <div class="panel-heading">
                 <h4 class="panel-title">
                   <a data-toggle="collapse" href=".collapse1">Cursos Asignados <span class="badge" id="countCursos">0</span></a>
                 </h4>
               </div>
               <div id="asignacionDocente" class="panel-collapse collapse collapse1">

               </div>
              </div>
            </div>

            </div>
          </div>
          <!-- ################################## -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button type="button" class="btn btn-primary" id="rNADocente"><span class="fa fa-save"></span> Guardar</button>
        </div>
      </div>
    </div>
  </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>