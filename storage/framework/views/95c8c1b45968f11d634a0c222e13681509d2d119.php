<?php $__env->startSection('titulo'); ?>
  <title>Asignar Notas a Estudiantes</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Asignar Notas</h1>
        <h3 class="text-center text-info"><?php echo e(mb_strtoupper($unidad->nombre_unidad)); ?></h3>
      </div>
      <div class="col-sm-12">
        <ul>
          <?php foreach($datos as $key => $row): ?>
            <li><?php echo e($row); ?></li>
          <?php endforeach; ?>
        </ul>
      </div>
      <div class="col-sm-12">
        <div class="table-responsive">
          <table class="table table-hover table-bordered">
            <thead>
              
              <tr>
                <th>
                  NO
                </th>
                <th>
                  ESTUDIANTE
                </th>
                <th>
                  ZONA
                </th>
                <th>
                  EXAMEN
                </th>
                <th>
                  TOTAL
                </th>
                 <th>
                  OPCIONES
                </th>

              </tr>
            </thead>
            <tbody>
              <?php foreach($nomina as $key => $estudiante): ?>
                <tr>
                  <td>
                    <?php echo e($key+1); ?>

                  </td>
                  <td>
                    <?php echo e(ucwords($estudiante->apellidos_estudiante.' '.$estudiante->nombre_estudiante)); ?>

                  </td>
                  <td>   
                  </td>
                  <td>        
                  </td>
                  <td>
                  </td>
                  <td>
                     <?php echo link_to_route('docente.edit', $title = 'Editar', $parameters = $estudiante->id_estudiante, $attributes = ['class'=>'btn btn-primary']); ?>

              
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>