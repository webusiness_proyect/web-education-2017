<!-- Navigation -->

<nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo e(url('/')); ?>"></a><img src="<?php echo e(url('/img/LOGO EDUCATIVO-20.png')); ?>">
    </div>
    <!-- /.navbar-header -->
   
    <ul class="nav navbar-top-links navbar-right">
     <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
    $( function() {
      $( document ).tooltip();
       $(".draggable").draggable();
     
    } );
    </script>

    <H5> 
      
        <?php if(($pendientesentrega->newpendientes)==0): ?>
        <a href="<?php echo e(url('misactividadeshijos')); ?>"><div class="fa fa-check fa-2x" id="notificaciones" title="Su hijo no tiene tareas pendientes de entregar"></div> <span class="label label-danger  pull-center" title="No hay tareas pendientes de entregar"></a></span>
        <?php else: ?>

          <a href="<?php echo e(url('misactividadeshijos')); ?>"><div class="fa fa-check fa-2x" id="notificaciones" title="Mis Tareas y actividades pendientes"></div> <span class="label label-danger  pull-center" title="Nuevo Actividades pendientes de entregar por su hijo">+<?php echo e(ucwords($pendientesentrega->newpendientes)); ?></a></span>
        <?php endif; ?>
         


        <?php foreach($notificacionesnuevas as $key => $not): ?>
        <?php if($not->newnot==0): ?>
        <a href="<?php echo e(url('notificacionespadre')); ?>"><div class="fa fa-envelope-o fa-2x" id="notificaciones" title="No tiene ninguna notificacion nueva, click para ver todas"></div> <span class="label label-danger  pull-center" title="No tiene ninguna notificacion nueva"></a></span>
        <?php else: ?>

          <a href="<?php echo e(url('notificacionespadre')); ?>"><div class="fa fa-envelope-o fa-2x" id="notificaciones" title="Mis Notificaciones y Mensajes"></div> <span class="label label-danger  pull-center" title="Nuevos Mensajes y notificaciones">+<?php echo e(mb_strtoupper($not->newnot)); ?></a></span>
        <?php endif; ?>
         <?php endforeach; ?>
         
   
        <li class="dropdown">   
            <a class="dropdown-toggle" id="nombre" data-toggle="dropdown" href="#"> 
                 <?php if((Auth::user()->url)==null): ?>
                    <div class="fa fa-user fa-2x"></div>
                    <?php else: ?>
                    <img src="<?php echo e(ucwords(Auth::user()->url)); ?>" width="40" height="30">
                    <?php endif; ?>  
                     <?php echo e(ucwords(Auth::user()->name)); ?>

                  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="<?php echo e(url('miperfil')); ?>"><i class="fa fa-user fa-fw"></i> Perfil de Usuario</a>
                </li>
                <li><a href="<?php echo e(url('misconfiguraciones')); ?>"><i class="fa fa-gear fa-fw"></i> Configuraciones</a>
                </li>
                <li><a href="<?php echo e(url('helpdesk')); ?>"><i class="fa fa-exclamation-circle"></i> Ayuda y Soporte</a>
                </li>  
                <li class="divider"></li>
                <li><a href="<?php echo e(url('/logout')); ?>"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    </H5>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse" id="menus">
          <!-- Crea el html para el menu lateral izquierdo -->
          <?php echo Menu::make($items, 'nav'); ?>

          <!-- Fin de la creación del menu lateral izquierdo -->

        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>


<!-- /#Navigation -->
