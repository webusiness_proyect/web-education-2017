<?php $__env->startSection('titulo'); ?>
  <title>Grados Niveles</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Grados Niveles</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <?php if (\Entrust::can('crear-gradonivel')) : ?>
        <?php echo link_to_route('gradoniveles.create', $title = 'Nuevo Grado-Nivel', $parameters = null, $attributes = ['class'=>'btn btn-primary']); ?>
        <br>
        <br>
        <?php endif; // Entrust::can ?>
        <?php if(count($datos) == 0): ?>
          <p class="text-info">
            No se han registrado grados en ningun nivel aun.
          </p>
        <?php else: ?>
          <div class="table-responsive">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    SECCION
                  </th>
                  <th>
                    GRADO
                  </th>
                  <th>
                    NIVEL
                  </th>
                  <th>
                    CARRERA
                  </th>
                  <?php if (\Entrust::can('editar-gradonivel')) : ?>
                  <th>
                    ACTUALIZAR
                  </th>
                  <?php endif; // Entrust::can ?>
                  <?php if (\Entrust::can('estado-gradonivel')) : ?>
                  <th>
                    ESTADO
                  </th>
                  <?php endif; // Entrust::can ?>
                </tr>
              </thead>
              <tbody id="datosGradosNiveles">
                <?php foreach($datos as $key => $dato): ?>
                  <tr>
                    <td>
                      <?php echo e($key+1); ?>
                    </td>
                    <td>
                      <?php echo e(mb_strtoupper($dato->nombre_seccion)); ?>
                    </td>
                    <td>
                      <?php echo e(mb_strtoupper($dato->nombre_grado)); ?>
                    </td>
                    <td>
                      <?php echo e(mb_strtoupper($dato->nombre_nivel)); ?>
                    </td>
                    <td>
                      <?php echo e(mb_strtoupper($dato->nombre_carrera)); ?>
                    </td>
                    <?php if (\Entrust::can('editar-gradonivel')) : ?>
                    <td>
                      <?php echo link_to_route('gradoniveles.edit', $title = 'Editar', $parameters = $dato->id_nivel_grado, $attributes = ['class'=>'btn btn-primary']); ?>
                    </td>
                    <?php endif; // Entrust::can ?>
                    <?php if (\Entrust::can('estado-gradonivel')) : ?>
                    <td>
                      <?php if($dato->estado_nivel_grado == true): ?>
                        <?php echo link_to_route('gradoniveles.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarGradoNivel', 'data-id'=>$dato->id_nivel_grado, 'data-estado'=>$dato->estado_nivel_grado]); ?>
                      <?php else: ?>
                        <?php echo link_to_route('gradoniveles.destroy', $title = 'Deshabilitado', $parameters = null, $attributes = ['class'=>'btn btn-danger eliminarGradoNivel', 'data-id'=>$dato->id_nivel_grado, 'data-estado'=>$dato->estado_nivel_grado]); ?>
                      <?php endif; ?>
                    </td>
                    <?php endif; // Entrust::can ?>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
            <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>