<!DOCTYPE html>

<html>

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Sistema Educativo - WB</title>



    <!-- Arhivos css para estilos-->

    <?php echo $__env->make('plantillas.estilos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <!-- /#fin del los archivos css -->



  </head>



<nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo e(url('/')); ?>"></a><img src="<?php echo e(url('/img/LOGO EDUCATIVO-20.png')); ?>">

    </div>
    <!-- /.navbar-header -->


    <ul class="nav navbar-top-links navbar-right">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
    $( function() {
      $( document ).tooltip();
       $(".draggable").draggable();
     
    } );
    </script>

    

   
        <H5>
         
        <div class="fa fa-check fa-2x" id="notificaciones" title="Mis Actividades pendientes de calificar"></div> <span class="label label-danger  pull-center" title="Nuevas tareas o actividades"> 12
</span>

 <?php foreach($notificacionesnuevas as $key => $not): ?>

  



        
        <a href="<?php echo e(url('notificacionesalumno')); ?>"><div class="fa fa-envelope-o fa-2x" id="notificaciones" title="Mis Notificaciones y Mensajes"></div> <span class="label label-danger  pull-center" title="Nuevos Mensajes y notificaciones"><?php echo e(mb_strtoupper($not->newnot)); ?></a></span>

         <?php endforeach; ?>
         
   
        <li class="dropdown">
                   
            <a class="dropdown-toggle" id="nombre" data-toggle="dropdown" href="#">
                
                 <?php if((Auth::user()->url)==null): ?>
                    <div class="fa fa-user fa-2x"></div>
                    <?php else: ?>
                    <img src="<?php echo e(ucwords(Auth::user()->url)); ?>" width="40" height="30">
                    <?php endif; ?>  
                     <?php echo e(ucwords(Auth::user()->name)); ?>

                  <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li><a href="<?php echo e(url('miperfil')); ?>"><i class="fa fa-user fa-fw"></i> Perfil de Usuario</a>
                </li>
                <li><a href="<?php echo e(url('misconfiguraciones')); ?>"><i class="fa fa-gear fa-fw"></i> Configuraciones</a>
                </li>
                <li><a href="<?php echo e(url('helpdesk')); ?>"><i class="fa fa-exclamation-circle"></i> Ayuda y Soporte</a>
                 
                </li>



                   
                <li class="divider"></li>
                <li><a href="<?php echo e(url('/logout')); ?>"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    </H5>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse" id="menus">
          <!-- Crea el html para el menu lateral izquierdo -->
          <?php echo Menu::make($items, 'nav'); ?>

          <!-- Fin de la creación del menu lateral izquierdo -->

        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
         


  <body>

    <div class="wraper">




  <div id="page-wrapper">



      <div class="row">

          <div class="col-lg-12">

          

            <div>
                   <H3> <center> <div class="fa fa-user" aria-hidden="true"></div> BIENVENIDOS <?php echo e(ucwords(Auth::user()->name)); ?> </center></H3>
                   <center>
                   <?php echo e(ucwords($date)); ?>

                   </center>
                </div>
               <div class="panel panel-primary">
               
                <div class="panel-footer">  
                  <H4> 
                    <center><strong>A TU SITIO WEB EDUCATIVO </strong>
                  <strong>En esta plataforma tu podras ver los cursos asignados por tu establecimiento, ver y responder las tareas asignadas por tu profesor de todos tus cursos, podras ver participar en foros o discuciones, ver las <strong> notas </strong> de tus cursos, <strong> esto desde estes. </strong>
                    </center></H4>


                  </div>
               </div>

                <div >
                   <H3> <center>ACCESO RAPIDO DEL ALUMNO</center></H3>
                </div>


               
                   <div  class="table-responsive">
                    <table class="">

                      <thead>

                        <tr>
                          <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                          <link rel="stylesheet" href="/resources/demos/style.css">
                          <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                          <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                          <script>
                            $( function() {
                              $( document ).tooltip();
                             
                            } );
                            </script>


                         <th>

                            <a href="#" class="btn btn-success btn-lg selected" title="Graficas y estadisticas en tiempo real"><i class="fa fa-pie-chart  fa-2x" aria-hidden="true"></i><br/>Tareas Entregadas</a>
 

 
                          </th>

                            <th>   

                            <a href="<?php echo e(url('/todasmisactividades')); ?>" class="btn btn-default btn-lg" title="cree y administre nuevas carreras o diplomados para su institucion, ejemplo:Bachiller en Computación, Perito en mercadotecnia y cualquier otro curso vigente en tu pais"><i class="fa fa-graduation-cap fa-2x" aria-hidden="true"></i><br/>Mis Actividades y tareas</a>

                          </th>

                          <th>   

                            <a href="<?php echo e(url('/mishorarios')); ?>" class="btn btn-default btn-lg" title="cree y administre nuevos grados en su institucion ejemplo: primero basico "><i class="fa fa-calendar fa-2x" aria-hidden="true"></i><br/>Mis Horarios</a>

                          </th>

                           <th>   

                            <a href="<?php echo e(url('/estudiantes')); ?>" class="btn btn-default btn-lg" title="Ve todas las actividades asignadas por tu profesor en tus cursos"><i class="fa fa-pencil fa-2x" aria-hidden="true"></i><i class="fa fa-user fa-2x" aria-hidden="true"></i><br/>Mis cursos </a>

                          </th>

                             <th>   

                            <a href="<?php echo e(url('/MisNotasPorUnidad')); ?>" class="btn btn-default btn-lg" title="cree y administre nuevas carreras o diplomados para su institucion, ejemplo:Bachiller en Computación, Perito en mercadotecnia y cualquier otro curso vigente en tu pais"><i class="fa fa-graduation-cap fa-2x" aria-hidden="true"></i><br/>Mis notas de esta unidad</a>

                          </th>

                          <th>   

                            <a href="<?php echo e(url('/misforos')); ?>" class="btn btn-default btn-lg" title="Participa en foros y chats sobre temas importantes de tus cursos"><i class="fa fa-book fa-2x" aria-hidden="true"></i><br/>Mis Foros y conversaciones</a>

                          </th>

                        

                            
                        </thead>

                      </table>
                    </div>
              



          </div>

          <!-- /.col-lg-12 -->

      </div>
      <br/>
      
        <div class="row ">
        <div class="col-sm-2 panel panel-default" >
          <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                

                <th>
                  <?php echo e(ucwords($today)); ?>

                </th>
              
              </tr>
            </thead>
            <tbody>
             
              <?php foreach($horarios as $key => $horario): ?>
                <tr>
                
                  
                   

                  <td>
                      <h6>
                     <b>INICIA:</b><?php echo e(mb_strtoupper($horario->inicio)); ?>

                     <b>FIN:</b><?php echo e(mb_strtoupper($horario->fin)); ?><br/>
                     <b>CURSO:<?php echo e(mb_strtoupper($horario->nombre_area)); ?></b>
                     
                     
                     <b>GRADO:</b><?php echo e(mb_strtoupper($horario->nombre_grado)); ?>

                     <?php echo e(mb_strtoupper($horario->nombre_carrera)); ?>

                   
                       <?php echo e(mb_strtoupper($horario->nombre_jornada)); ?>

                     <?php echo e(mb_strtoupper($horario->nombre_plan)); ?>

                     <?php echo e(mb_strtoupper($horario->nombre_nivel)); ?>

                     
                    </h6>
                    
                     


                  </td>
                </tr>
                <?php endforeach; ?>
             
            
            </tbody>
          </table>
        </div>

          <div class="col-lg-6 panel panel-default">

           
                  


               <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                  <script type="text/javascript">
                   google.charts.load('current', {'packages':['corechart']});
                   google.charts.setOnLoadCallback(drawChart);
                   function drawChart() {

                      var data = google.visualization.arrayToDataTable([
                       ['Task', 'Hours per Day'],
                       ['ENTREGADAS',     11],
                      
                       ['NO ENTREGADAS',    7]
                      ]);

                     var options = {
                        title: 'TAREAS ENTREGADAS'
                      };

                      var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                      chart.draw(data, options);
                    }
                  </script>


              <div id="piechart"></div>

             <br/>
            
             <br/>
              <div class="panel panel-warning">
                <div class="panel-heading">
                   <H2> <center> <div class="fa fa-exclamation-circle fa-3x" aria-hidden="true"></div> AYUDA Y SOPORTE </center></H2>
                </div>
              
              </div>


                 



          </div>

          <div class="col-lg-4">
              <center>
              <img src="<?php echo e(url('/img/imagenes/3er vista.png')); ?>"  width="250" height="450">
              </center>
          </div>

          

          <!-- /.col-lg-12 -->

      </div>



  </div>

  <!-- /#page-wrapper -->


