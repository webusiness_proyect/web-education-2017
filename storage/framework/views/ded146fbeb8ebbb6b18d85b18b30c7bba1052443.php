<?php $__env->startSection('titulo'); ?>



  <title>Mis Actividades</title>



<?php $__env->stopSection(); ?>







<?php $__env->startSection('cuerpo'); ?>



  <div id="page-wrapper">



    <div class="row">



      <div class="col-sm-12">



        <h1 class="page-header text-center"><div class="fa fa-pencil-square"></div>  Mis Actividades y tareas -  <?php echo e(mb_strtoupper($unidad->nombre_unidad)); ?></h1>


           <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> ENTREGA </strong> tus <strong>ACTIVIDADES</strong> y <strong>TAREAS</strong> asignadas por tus profesores de todos tus cursos
            </h4>
           
          </center>
        </div>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        

           <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

       
      </center>

        <?php echo $__env->make('estudiantes.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>




      </div>

    



      <div class="col-sm-12 panel panel-default">



        <div class="table-responsive">



          <table class="table table-hover table-bordered">



            <thead>



              <tr>




               <th>

                 <CENTER> MIS ACTIVIDADES DE CURSOS</CENTER>

                </th>


                

                  <th>



                  GRADO



                </th>


                <th>



                  PENSUM



                </th>

                <th>



                  SALON



                </th>






              </tr>



            </thead>



            <tbody>



              <?php foreach($cursos as $key => $curso): ?>



                <tr>



                  <td>

                  <center>


                   

                      
                        <a href="/misactividades?a=<?php echo e($curso->id_area); ?>"  class="btn btn-warning"><div class="fa fa-pencil-square"></div> Tareas y actividades de  <?php echo e(mb_strtoupper($curso->nombre_area)); ?></a>



                        



                  </center>

                  </td>



                 


                  <td>



                    <?php echo e(mb_strtoupper($curso->nombre_grado)); ?>




                  </td>

                    <td>



                    <?php echo e(mb_strtoupper($curso->nombre_nivel)); ?>

                     <?php echo e(mb_strtoupper($curso->nombre_plan)); ?>

                      <?php echo e(mb_strtoupper($curso->nombre_jornada)); ?>

                      





                  </td>
                  <td>


                    <?php echo e(mb_strtoupper($curso->nombre_salon)); ?>



                  </td>


              



                </tr>
                



              <?php endforeach; ?>



            </tbody>



          </table>

           <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">

          <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

          <div class="text-center">

            <?php echo $cursos->links(); ?>


          </div>



        </div>



      </div>



    </div>



  </div>



<?php $__env->stopSection(); ?>




<?php echo $__env->make('principalalumno', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>