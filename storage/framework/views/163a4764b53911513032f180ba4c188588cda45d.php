<?php $__env->startSection('titulo'); ?>
  <title>Inscripciones Estudiantes</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">



<div class="panel-body ">
        <h1 class="page-header text-center"> <div class="fa fa-users" aria-hidden="true"></div>INSCRIPCION DE ESTUDIANTES</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra estudiantes, asigna un tutor y una referencia, el sistema creara un usuario y una contraseña para cada usuario los cuales llegaran a los correos registrados
            </h4>

          
           
          </center>
        </div>
        <div class="alert alert-warning alert-dismissable">
          <center>

            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-info btn btn-default" aria-hidden="true"></i><STRONG>LLENA TODOS LOS CAMPOS EN EL ORDEN INDICADO  TODOS LOS * NO DEBEN DEJARSE EN BLANCO</a> </STRONG><br/>
           

            
          </CENTER>
    
        </div>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

         <?php if (\Entrust::can('crear-alumno')) : ?>

        <?php echo link_to_route('inscripcionestudiantes.create', $title = 'Nueva Inscripción', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus']); ?>

        <?php echo $__env->make('inscripciones.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php endif; // Entrust::can ?>



         <?php if(count($cursos) == 0): ?>
          <p class="text-info">
            No se han registrado estudiantes aun...
          </p>

        </center>




        
      <?php else: ?>
      <div class="table-responsive panel panel-default">

        <table class="table table-hover">
            <thead>
              <tr>
             
                <th>
                  ALUMNO
                </th>
                <th>
                  TELEFONO 
                </th>
                <th>
                  CORREO 
                </th>
                <th>
                  GRADO 
                </th>
                <th>
                  CARRERA 
                </th>
                <th>
                  NIVEL 
                </th>
                <?php if (\Entrust::can('editar-alumno')) : ?>
                <th>
                  OPCIONES
                </th>
                <?php endif; // Entrust::can ?>
                <?php if (\Entrust::can('estado-alumno')) : ?>
                <th>
                  ESTADO
                </th>
                <?php endif; // Entrust::can ?>
              </tr>
            </thead>
            <tbody id="datosEstudiantes">
              <?php foreach($cursos as $key => $grado): ?>
                <tr>
                 
                  <td>
                    <?php echo e(mb_strtoupper($grado->apellidos_estudiante)); ?>


                    <?php echo e(mb_strtoupper($grado->nombre_estudiante)); ?>

                  </td>
                   <td>
                    <?php echo e(mb_strtoupper($grado->telefono_casa)); ?>

                  </td>
                   <td>
                    <?php echo e(mb_strtoupper($grado->correo_estudiante)); ?>

                  </td>
                   <td>
                    <?php echo e(mb_strtoupper($grado->nombre_grado)); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($grado->nombre_carrera)); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($grado->nombre_nivel)); ?>

                  </td>
                  <?php if (\Entrust::can('editar-alumno')) : ?>
                  <td>
                  <center>
                    <?php echo link_to_route('inscripcionestudiantes.edit', $title = 'Editar', $parameters = $grado->id_estudiante, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>

                    <a href="/tutoreshijos/show?a=<?php echo e($grado->id_estudiante); ?>" class="btn btn-default"><div class="fa fa-eye"></div> Padres </a>
                  </center>

                   
                  </td>
                 
                  <td>
  
                     <?php if($grado->estado_estudiante == TRUE): ?>
                              <input type="checkbox" name="estado" checked value="<?php echo e($grado->id); ?>" class="toggleEstado">
                            <?php else: ?>
                              <input type="checkbox" name="estado" value="<?php echo e($grado->id); ?>" class="toggleEstado">
                            <?php endif; ?>
                  </td>
                  <?php endif; // Entrust::can ?>
                </tr>
              <?php endforeach; ?>
            </tbody>


          </table>
          </div>
           <div class="text-center">
            <?php echo $cursos->links(); ?>

          </div>
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
          <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <?php endif; ?>
      
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>