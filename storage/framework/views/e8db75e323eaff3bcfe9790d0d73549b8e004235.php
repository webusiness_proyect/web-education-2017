<?php if($errors->has()): ?>
  <?php foreach($errors->all() as $error): ?>
     <div class="alert alert-danger alert-dismissable">
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo e($error); ?>
     </div>
   <?php endforeach; ?>
<?php endif; ?>
