

<?php $__env->startSection('titulo'); ?>
  <title>Nueva Actividad</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
<div id="page-wrapper">
  <div class="row">
    <div class="col-sm-12">
      <h1 class="page-header text-center">Nueva Actividad</h1>
      <?php echo $__env->make('mensajes.errores', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <p>
        Nota: Todos los campos con (*) son obligatorios.
      </p>
    </div>
    <div class="col-sm-12">
      <?php echo Form::open(['route'=>'Actividad.store', 'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'actividades']); ?>
        <input type="hidden" name="curso" value="<?php echo e($curso); ?>">
        <?php echo $__env->make('Actividad.form.actividades', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo Form::close(); ?>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>