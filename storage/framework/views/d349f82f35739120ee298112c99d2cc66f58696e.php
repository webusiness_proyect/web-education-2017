<?php $__env->startSection('titulo'); ?>
  <title>Empleados</title>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
       <div class="panel-body">
        <h1 class="page-header text-center"> <div class="fa fa-users" aria-hidden="true"></div><div class="fa fa-search" aria-hidden="true"></div>Alumnos con este pensum</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-search fa-2x btn btn-default" aria-hidden="true"></i> Resultados de busqueda </strong> 
            </h4>
          </center>
        </div>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
      </div>
      <div class="col-sm-12 panel panel-default">
      <br/>
        <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

        <?php if(count($students) == 0): ?>
          <p class="text-info">
            No hay alumnos registrados a este curso y este año.
          </p>
          </center>
        <?php else: ?>
        <br/>
          <div class="table-responsive">
            <table class="table table-hover table- table-bordered">
              <thead>
                <tr>
                  <th>
                    OPCIONES
                  </th>
                  <th>
                    DATOS
                  </th>
                 </tr>
              </thead>
              <tbody id="datosPersonas">
                <?php foreach($students as $key => $persona): ?>
                  <tr>
                    <td>              
                       <a class="btn btn-default fa fa-eye" href="buscarnotasbimestre/?a=<?php echo e($persona->id); ?>&b=<?php echo e($year); ?>">Ver notas de cursos</a>
                    </td>
                    <td>
                      <?php echo e($persona->apellidos_estudiante); ?>

                      <?php echo e($persona->nombre_estudiante); ?>


                    </td>
                   
                   
                   
                   
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            <?php echo Form::hidden('_token', csrf_token(), ['id'=>'token']); ?>

            <div class="text-center">
            <?php echo $students->links(); ?>

          </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>