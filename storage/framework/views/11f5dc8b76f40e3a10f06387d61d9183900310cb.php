<?php $__env->startSection('titulo'); ?>

  <title>Usuarios</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">
          <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-user" aria-hidden="true"></div>Usuarios</h1>
        <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong> y administra usuarios de tu institucion, el sistema creara un usuario y una conseña los cuales llegara a el correo del usuario registrado
            </h4>

          
           
          </center>
        </div>
        <div class="alert alert-warning alert-dismissable">
          <center>

            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <i class="fa fa-info btn btn-default" aria-hidden="true"></i>ANTES DE CREAR UN USUARIO DEBERAS DE CREAR UNA <a href="<?php echo e(url('/personas')); ?>">PERSONA </a> <br/>
           <i class="fa fa-info btn btn-default" aria-hidden="true"></i> LOS ESTUDIANTES SERAN CREADOS EN EL MODULO <a href="<?php echo e(url('/inscripcionestudiantes')); ?>">INSCRIPCION ESTUDIANTES</a><br/>
           

            
          </CENTER>
    
        </div>
          


        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>
        <div class="col-sm-12 panel panel-default">

          <br/>
           <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

        <?php if (\Entrust::can('crear-usuario')) : ?>

        <?php echo link_to_route('usuarios.create', $title = 'Nuevo Usuario', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus']); ?>


        <br>

        <br>

        <?php endif; // Entrust::can ?>
        <?php echo $__env->make('usuarios.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <?php if(count($usuarios) == 0): ?>

          <p class="text-info">

            No se han registrado usuarios aun.

          </p>



        </center>



        <?php else: ?>

          <div class="table-responsive">

            <table class="table table-hover">

              <thead>

                <tr>

                  <th>

                    CODIGO

                  </th>

                  <th>

                    NOMBRE

                  </th>

                   <th>

                    CORREO

                  </th>

                  <th>

                    TIPO DE USUARIO

                  </th>
                  <?php if (\Entrust::can('crear-usuario')) : ?>

                  <th>

                    EDITAR

                  </th>

                  <th>

                    ESTADO

                  </th>
                  <?php endif; // Entrust::can ?>                  


                </tr>

              </thead>

              <tbody id="datosUsuarios">

                <?php foreach($usuarios as $key => $usuario): ?>

                  <tr>

                    <td>

                      <?php echo e(mb_strtoupper($usuario->id)); ?>


                    </td>

                    <td>

                      <?php echo e(mb_strtoupper($usuario->nombre)); ?>


                    </td>

                    <td>

                      <?php echo e($usuario->correo); ?>


                    </td>

                     <td>

                      <?php echo e($usuario->rol); ?>


                    </td>
                    <?php if (\Entrust::can('editar-usuario')) : ?>

                    <td>

                    <?php echo link_to_route('usuarios.edit', $title = 'Editar', $parameters = $usuario->id, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>


                  </td>

                    <td>

                  <?php if($usuario->estado_usuario == TRUE): ?>
                      <input type="checkbox" name="estado" checked value="<?php echo e($usuario->id); ?>" class="toggleEstado">
                  <?php else: ?>
                      <input type="checkbox" name="estado" value="<?php echo e($usuario->id); ?>" class="toggleEstado">
                  <?php endif; ?>

                  </td>
                  <?php endif; // Entrust::can ?>

                  </tr>

                <?php endforeach; ?>

              </tbody>

            </table>

            <div class="text-center">

            <?php echo $usuarios->links(); ?>


          </div>

          </div>

        <?php endif; ?>

      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>