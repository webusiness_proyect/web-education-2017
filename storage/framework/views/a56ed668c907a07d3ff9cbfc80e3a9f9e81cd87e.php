<?php $__env->startSection('titulo'); ?>

  <title>Mis alumnos</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

      <di <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-user" aria-hidden="true"></div> Mis Estudiantes</h1>
         </h4>
          <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-check fa-2x btn btn-default" aria-hidden="true"></i>ACTIVIDADES PENDIENTES DE ENTREGAR </strong>  mantente informado sobre las actividades pendientes para entregar
            </h4>
           
          </center>
        </div>
        
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>



           <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

       
      </center>

        </div>

       
        <div class="col-sm-12 panel panel-default">

          <br/>



        <div class="table-responsive">

          <table class="table table-hover table-bordered">

            <thead>

              <tr>

               <th>

                <h4><strong> OPCIONES </strong> </h4>

                </th>

                <th>

                 <h4><strong> DATOS DEL ALUMNO</strong></h4>

                </th>

                <th>

                  <h4><strong>PENSUM</strong></h4>

                </th>

               
                <th>

                  <h4><strong>CARRERA</strong></h4>

                </th>
                <th>

                  <h4><strong>SECCION</strong></h4>

                </th>                
                
                

              </tr>

            </thead>

            <tbody>

              <?php foreach($alumnos as $key => $alumno): ?>

                <tr>

                  
                   <td>

                      <center>
                         <H4><a href="/misactividadeshijos/show?a=<?php echo e($alumno->id); ?>"  class="btn btn-warning"><div class="fa fa-eye fa-2x"></div>VER ACTIVIDADES</a><br/><H4>
                       
                      </center>

                  </td>

                  <td>

                    <H4><?php echo e(mb_strtoupper($alumno->nombre_estudiante)); ?>   <?php echo e(mb_strtoupper($alumno->apellidos_estudiante)); ?></H4>

                  </td>

                 

                  <td>

                    <H4><?php echo e(mb_strtoupper($alumno->nombre_grado)); ?>, 

                 

                    <?php echo e(mb_strtoupper($alumno->nombre_nivel)); ?>,

                

                    <?php echo e(mb_strtoupper($alumno->nombre_jornada)); ?>,

                

                    <?php echo e(mb_strtoupper($alumno->nombre_plan)); ?> </H4>

                  </td>

                  <td>

                    <H4><?php echo e(mb_strtoupper($alumno->nombre_carrera)); ?></H4>

                  </td>

                  <td>

                    <H4><?php echo e(mb_strtoupper($alumno->nombre_seccion)); ?></H4>

                  </td>

                   
                 

                </tr>

              <?php endforeach; ?>

            </tbody>

          </table>

        </div>

      </div>

      <div class="col-sm-12 text-center">

     
      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>