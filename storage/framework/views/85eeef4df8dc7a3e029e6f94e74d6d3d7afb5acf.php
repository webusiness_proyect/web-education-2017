<?php $__env->startSection('titulo'); ?>
  <title>Jornadas</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-sun-o" aria-hidden="true"></div> Jornadas disponibles <div class="fa fa-moon-o" aria-hidden="true"></div></h1>
          <div class="panel panel-default">
          <center>
            <h4>
              <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i> CREA </strong>y administra las jornadas disponibles en tu institucion ejemplo:<strong> JORNADA:MATUTINA</strong>
            </h4>
           
          </center>
        </div>
         

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        </div>
      </div>
      <div class="col-sm-12 panel panel-default">

        <br/>
        <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

        <?php if (\Entrust::can('crear-jornada')) : ?>
        <?php echo link_to_route('jornadas.create', $title = 'Nueva Jornada', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square']); ?>

        <br>
        <br>
        <?php endif; // Entrust::can ?>
        <?php if(count($jornadas) == 0): ?>
          <p class="text-info">
            No se han registrado jornadas aun.
          </p>
          </center>
        <?php else: ?>
          <div class="table-responsive">
            <table class="table table-hover table-bordered">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    JORNADA
                  </th>
                  <?php if (\Entrust::can('editar-jornada')) : ?>
                  <th>
                    ACTUALIZAR
                  </th>
                  <?php endif; // Entrust::can ?>
                  <?php if (\Entrust::can('estado-jornada')) : ?>
                  <th>
                    ESTADO
                  </th>
                  <?php endif; // Entrust::can ?>
                </tr>
              </thead>
              <tbody id="datosJornadas">
                <?php foreach($jornadas as $key => $jornada): ?>
                  <tr>
                    <td>
                      <?php echo e($key+1); ?>

                    </td>
                    <td>
                      <?php echo e(mb_strtoupper($jornada->nombre_jornada)); ?>

                    </td>
                    <?php if (\Entrust::can('editar-jornada')) : ?>
                    <td>
                      <?php echo link_to_route('jornadas.edit', $title = 'Editar', $parameters = $jornada->id_jornada, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>

                    </td>
                    <?php endif; // Entrust::can ?>
                    <?php if (\Entrust::can('estado-jornada')) : ?>
                    <td>
    

                      <?php if($jornada->estado_jornada == TRUE): ?>
                      <input type="checkbox" name="estado" checked value="<?php echo e($jornada->id_jornada); ?>" class="toggleEstado">
                    <?php else: ?>
                        <input type="checkbox" name="estado" value="<?php echo e($jornada->id_jornada); ?>" class="toggleEstado">
                    <?php endif; ?>

                    </td>
                    <?php endif; // Entrust::can ?>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
        <?php endif; ?>
      </div>
    </div>
    <!-- Mensaje de carga-->
    <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>