<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sistema Educativo - WB</title>
    <!-- Arhivos css para estilos-->
    <?php echo $__env->make('plantillas.estilos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- /#fin del los archivos css -->


 
    
    <script>
    $( function() {
      $( document ).tooltip();
       $(".draggable").draggable();
     
    } );
    </script>
  </head>

<?php echo $__env->make('bienvenida.menualumno', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         

  <body>
    <div class="wraper">
      <div id="page-wrapper">
      <div class="row">
          <div class="col-lg-12">
            <div>
                   <H3> <center> <div class="fa fa-user" aria-hidden="true"></div> BIENVENIDOS <?php echo e(ucwords(Auth::user()->name)); ?> </center>
                   </H3>
                   <center>
                   <?php echo e(ucwords($date)); ?>

                   </center>
            </div>
            <div class="panel panel-primary">
                <div class="panel-footer">  
                  <H4> 
                    <center>
                    <a href="#demo2"  data-toggle="collapse">
                    <strong> <i class="fa fa-question-circle fa-2x btn btn-default" aria-hidden="true"></i><strong>INFORMACION</strong></a></strong>
                    </center>
                    <center>
                    <div id="demo2" class="collapse">
                    
                      <strong>En esta plataforma tu podras ver los cursos asignados por tu establecimiento, ver y responder las tareas asignadas por tu profesor de todos tus cursos, podras ver participar en foros o discuciones, ver las <strong> notas </strong> de tus cursos, <strong> esto desde estes. </strong>
                    </center></H4>
                    </div>
                </div>
           
            <div >
                <H3> <center>ACCESO RAPIDO DEL ALUMNO</center></H3>
            </div>
                   <div  class="table-responsive">
                    <table class="">
                      <thead>
                        <tr>
                          
                         <th>
                            <a href="#" class="btn btn-success btn-lg selected" title="Graficas y estadisticas en tiempo real"><i class="fa fa-pie-chart  fa-2x" aria-hidden="true"></i><br/>Tareas Entregadas</a>
                          </th>
                          <th>   
                            <a href="<?php echo e(url('/todasmisactividades')); ?>" class="btn btn-default btn-lg" title="cree y administre nuevas carreras o diplomados para su institucion, ejemplo:Bachiller en Computación, Perito en mercadotecnia y cualquier otro curso vigente en tu pais"><i class="fa fa-pencil-square fa-2x" aria-hidden="true"></i><br/>

                             <strong>Tareas Nuevas (+<?php echo e(ucwords($actividades->newact)); ?>)</strong></a>
                          </th>
                          <th>   
                            <a href="<?php echo e(url('/mishorarios')); ?>" class="btn btn-default btn-lg" title="cree y administre nuevos grados en su institucion ejemplo: primero basico "><i class="fa fa-calendar fa-2x" aria-hidden="true"></i><br/>Mis Horarios</a>
                          </th>
                          <th>   
                            <a href="<?php echo e(url('/estudiantes')); ?>" class="btn btn-default btn-lg" title="Ve todas las actividades asignadas por tu profesor en tus cursos"><i class="fa fa-pencil fa-2x" aria-hidden="true"></i><i class="fa fa-user fa-2x" aria-hidden="true"></i><br/>Mis cursos </a>
                          </th>
                          <th>   
                            <a href="<?php echo e(url('/MisNotasPorUnidad')); ?>" class="btn btn-default btn-lg" title="cree y administre nuevas carreras o diplomados para su institucion, ejemplo:Bachiller en Computación, Perito en mercadotecnia y cualquier otro curso vigente en tu pais"><i class="fa fa-graduation-cap fa-2x" aria-hidden="true"></i><br/>Mis notas de esta unidad</a>
                          </th>
                          <th>   
                            <a href="<?php echo e(url('/misforos')); ?>" class="btn btn-default btn-lg" title="Participa en foros y chats sobre temas importantes de tus cursos"><i class="fa fa-book fa-2x" aria-hidden="true"></i><br/>Mis Foros y conversaciones</a>
                          </th>
                        </thead>
                      </table>
                    </div>
          </div>

          <!-- /.col-lg-12 -->

      </div>
      <br/>
        <div class="row ">
        <div class="col-sm-2 panel panel-default" >
          <table class="table table-hover table-hover table- table-bordered">
            <thead>
              <tr>
                <th>
                <center>
                  <a href="<?php echo e(url('/mishorarios')); ?>" title="Ver todos los horarios"><div class="fa fa-calendar fa-2x"></div><?php echo e(ucwords($today)); ?> 
                  </a>
                  </center>
                </th>
              </tr>
            </thead>
            <tbody>
             
              <?php foreach($horarios as $key => $horario): ?>
                <tr>
                
                  
                   

                  <td>
                      <h6>
                     <b>INICIA:</b><?php echo e(mb_strtoupper($horario->inicio)); ?>

                     <b>FIN:</b><?php echo e(mb_strtoupper($horario->fin)); ?><br/>
                     <b>CURSO:<?php echo e(mb_strtoupper($horario->nombre_area)); ?></b>
                     
                  
                     
                    </h6>
                    
                     


                  </td>
                </tr>
                <?php endforeach; ?>


             
            
            </tbody>
          </table>
        </div>

          <div class="col-lg-6 panel panel-default">

           
                  


               <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                  <script type="text/javascript">
                   google.charts.load('current', {'packages':['corechart']});
                   google.charts.setOnLoadCallback(drawChart);
                   function drawChart() {

                      var data = google.visualization.arrayToDataTable([
                       ['Task', 'Hours per Day'],
                       ['ENTREGADAS',     11],
                      
                       ['NO ENTREGADAS',    7]
                      ]);

                     var options = {
                        title: 'TAREAS ENTREGADAS'
                      };

                      var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                      chart.draw(data, options);
                    }
                  </script>


              <div id="piechart"></div>

             <br/>
            
             <br/>
              


                 



          </div>

          <div class="col-lg-4">
              <center>
              <img src="<?php echo e(url('/img/imagenes/3er vista.png')); ?>"  width="250" height="450">
              </center>
          </div>

          

          <!-- /.col-lg-12 -->

      </div>



  </div>

  <!-- /#page-wrapper -->
 <?php echo $__env->make('plantillas.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>



  <!-- /#fin de los archivos javascript -->

  </body>

</html>

