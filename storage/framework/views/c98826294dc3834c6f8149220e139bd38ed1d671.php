<?php $__env->startSection('titulo'); ?>

  <title>Actividades</title>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('cuerpo'); ?>

  <div id="page-wrapper">

    <div class="row">

      <div class="col-sm-12">

        <h1 class="page-header text-center "> <div class="glyphicon glyphicon-book"></div> NOTAS </h1>

        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

      </div>
            <div class="col-sm-12">

        <div class="alert alert-info">


         
          <center>
          <?php foreach($total as $key=>$actividad): ?>

            <div class="fa fa-check-square"> Aqui apareceran todas las notas de las actividades entregadas por el alumno <?php echo e($nota=mb_strtoupper($actividad->nombre_estudiante).' ' .mb_strtoupper($actividad->apellidos_estudiante).' del curso de:  ' .mb_strtoupper($actividad->nombre_area)); ?></div>
          <?php endforeach; ?>
          </center>

          

        </div>

      </div>



      <div class="col-sm-12">



        <p>

       



         
        </p>

        <?php if(count($notas) == 0): ?>

          <p class="text-info">

            No se han encontrado notas para este curso...

          </p>

        <?php else: ?>


          <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>

                  <th>

                    NO

                  </th>

                  <th>

                    NOMBRE ACTIVIDAD

                  </th>

                  <th>

                    FECHA ENTREGA

                  </th>


                  <th>

                    ESTADO DE ENTREGA

                  </th>


                  <th>

                    PUNTEO

                  </th>
                  <th>

                    UNIDAD

                  </th>

                </tr>

              </thead>

              <tbody>

                <?php foreach($notas as $key=>$actividad): ?>
                

                  <tr>

                    <td >

                       <h5 class=" glyphicon glyphicon-edit"> <?php echo e($key+1); ?></h5>

                    </td>

                    

                    <td>

                    <?php echo e(mb_strtoupper($actividad->actividad)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($actividad->fecha_entrega)); ?>


                  </td>

                  <td>

                   <?php echo e(mb_strtoupper($actividad->estado_entrega_actividad)); ?>


                  </td>

                   <td>

                   <?php echo e($nota=mb_strtoupper($actividad->calificacion)); ?>


                  </td>
                  <td>

                   <?php echo e($nota=mb_strtoupper($actividad->nombre_unidad)); ?>


                  </td>

                 
                  
                  

                  </tr>

                  

                <?php endforeach; ?>



              </tbody>

            </table>
            <?php foreach($total as $key=>$total1): ?>
                
                <?php if($total=mb_strtoupper($total1->calificacion)< 60): ?>

                  <h1 class="text-danger blockquote-reverse">

                   NOTA TOTAL: <?php echo e($total=mb_strtoupper($total1->calificacion)); ?> </br>
                   </h1>
                   <H4 class="text-danger blockquote-reverse">NOTA REPROBADA</H4>

                  

                <?php else: ?>

                 <h1 class="text-success blockquote-reverse">

                   NOTA TOTAL: <?php echo e($total=mb_strtoupper($total1->calificacion)); ?> </br>
                   </h1>
                   <H4 class="text-success blockquote-reverse">NOTA APROBADA</H4>

                 
                 <?php endif; ?>
                <?php endforeach; ?>


<div class="container">
 
  <center><a href="#demo" class="btn btn-default" data-toggle="collapse"><div class="fa fa-plus"></div>... MAS NOTAS DE UNIDADES ANTERIORES ...</a></center>
  <div id="demo" class="collapse">
   
        

          <div class="table-responsive">


          <div class="table-responsive">

            <table class="table table-hover table-bordered">

              <thead>

                <tr>

                  <th>

                    NO

                  </th>

                  <th>

                    NOMBRE ACTIVIDAD

                  </th>

                  <th>

                    DESCRIPCION ACTIVIDAD

                  </th>


                  <th>

                    ESTADO DE ENTREGA

                  </th>


                  <th>

                    PUNTEO

                  </th>
                  <th>

                    UNIDAD

                  </th>

                </tr>

              </thead>

              <tbody>

                <?php foreach($anteriores as $key=>$actividad): ?>
                

                  <tr>

                    <td >

                       <h5 class=" glyphicon glyphicon-edit"> <?php echo e($key+1); ?></h5>

                    </td>

                    

                    <td>

                    <?php echo e(mb_strtoupper($actividad->actividad)); ?>


                  </td>

                  <td>

                    <?php echo e(mb_strtoupper($actividad->fecha_entrega)); ?>


                  </td>

                  <td>

                   <?php echo e(mb_strtoupper($actividad->estado_entrega_actividad)); ?>


                  </td>

                   <td>

                   <?php echo e($nota=mb_strtoupper($actividad->calificacion)); ?>


                  </td>
                  <td>

                   <?php echo e($nota=mb_strtoupper($actividad->nombre_unidad)); ?>


                  </td>

                 
                  
                  

                  </tr>

                  

                <?php endforeach; ?>



              </tbody>

            </table>

          </div>
        </div>

             <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">

          </div>

        <?php endif; ?>

      </div>

    </div>

  </div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>