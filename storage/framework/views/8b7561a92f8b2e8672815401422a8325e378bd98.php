<?php $__env->startSection('titulo'); ?>
  <title>Empleados</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">
        <h1 class="page-header text-center">Empleados</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <?php if (\Entrust::can('crear-empleado')) : ?>
        <?php echo link_to_route('personas.create', $title = 'Nuevo Empleado', $parameters = null, $attributes =['class'=>'btn btn-primary']); ?>

        <br>
        <br>
        <?php endif; // Entrust::can ?>
        <?php if(count($personas) == 0): ?>
          <p class="text-info">
            No se han registrado empleados aun.
          </p>
        <?php else: ?>
          <div class="table-responsive">
            <table class="table table-hover table- table-bordered">
              <thead>
                <tr>
                  <th>
                    NO
                  </th>
                  <th>
                    APELLIDOS
                  </th>
                  <th>
                    NOMBRES
                  </th>
                  <th>
                    TELEFONO
                  </th>
                  <th>
                    DPI
                  </th>
                  <?php if (\Entrust::can('editar-empleado')) : ?>
                  <th>
                    ACTUALIZAR
                  </th>
                  <?php endif; // Entrust::can ?>
                  <?php if (\Entrust::can('estado-empleado')) : ?>
                  <th>
                    ESTADO
                  </th>
                  <?php endif; // Entrust::can ?>
                  <?php if (\Entrust::can('ver-empleado')) : ?>
                  <th>
                    MAS DATOS
                  </th>
                  <?php endif; // Entrust::can ?>
                </tr>
              </thead>
              <tbody id="datosPersonas">
                <?php foreach($personas as $key => $persona): ?>
                  <tr>
                    <td>
                      <?php echo e($key+1); ?>

                    </td>
                    <td>
                      <?php echo e($persona->apellidos_persona); ?>

                    </td>
                    <td>
                      <?php echo e($persona->nombres_persona); ?>

                    </td>
                    <td>
                      <?php echo e($persona->telefono_persona); ?>

                    </td>
                    <td>
                      <?php echo e($persona->cui_persona); ?>

                    </td>
                    <?php if (\Entrust::can('editar-empleado')) : ?>
                    <td>
                      <?php echo link_to_route('personas.edit', $title = 'Editar', $parameters = $persona->id_persona, $attributes = ['class'=>'btn btn-primary']); ?>

                    </td>
                    <?php endif; // Entrust::can ?>
                    <?php if (\Entrust::can('estado-empleado')) : ?>
                    <td>
                      <?php if($persona->estado_persona == true): ?>
                        <input type="checkbox" name="estado" checked value="<?php echo e($persona->id_persona); ?>" class="toggleEstado">
                      <?php else: ?>
                        <input type="checkbox" name="estado" value="<?php echo e($persona->id_persona); ?>" class="toggleEstado">
                      <?php endif; ?>
                    </td>
                    <?php endif; // Entrust::can ?>
                    <?php if (\Entrust::can('ver-empleado')) : ?>
                    <td>
                      <?php echo link_to_route('personas.show', $title = 'Ver Más', $parameters = $persona->id_persona, $attributes = ['class'=>'btn btn-info']); ?>

                    </td>
                    <?php endif; // Entrust::can ?>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            <?php echo Form::hidden('_token', csrf_token(), ['id'=>'token']); ?>

            <div class="text-center">
            <?php echo $personas->links(); ?>

          </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>