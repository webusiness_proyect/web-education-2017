<?php $__env->startSection('titulo'); ?>
  <title>Inscripciones Estudiantes</title>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('cuerpo'); ?>
  <div id="page-wrapper">
    <div class="row">
      <div class="col-sm-12">



<div class="panel-body ">
        <h1 class="page-header text-center"> <div class="fa fa-users" aria-hidden="true"></div>INSCRIPCION DE ESTUDIANTES</h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </div>
      <div class="col-sm-12">
        <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>

         <?php if (\Entrust::can('crear-usuario')) : ?>
        <?php echo link_to_route('inscripcionestudiantes.create', $title = 'Nueva Inscripción', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus']); ?>

        <?php echo $__env->make('inscripciones.search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php endif; // Entrust::can ?>



         <?php if(count($cursos) == 0): ?>
          <p class="text-info">
            No se han registrado estudiantes aun...
          </p>

        </center>




        
      <?php else: ?>
      <div class="table-responsive panel panel-default">

        <table class="table table-hover">
            <thead>
              <tr>
              <th>
                  NO
                </th>
                <th>
                  APELLIDO
                </th>
                <th>
                  NOMBRE 
                </th>
                <th>
                  TELEFONO 
                </th>
                <th>
                  CORREO 
                </th>
                <th>
                  GRADO 
                </th>
                <th>
                  CARRERA 
                </th>
                <th>
                  NIVEL 
                </th>
                <?php if (\Entrust::can('editar-grado')) : ?>
                <th>
                  ACTUALIZAR
                </th>
                <?php endif; // Entrust::can ?>
                <?php if (\Entrust::can('estado-grado')) : ?>
                <th>
                  ESTADO
                </th>
                <?php endif; // Entrust::can ?>
              </tr>
            </thead>
            <tbody >
              <?php foreach($cursos as $key => $grado): ?>
                <tr>
                  <td>
                    <?php echo e($grado->id_estudiante); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($grado->apellidos_estudiante)); ?>

                  </td>
                   <td>
                    <?php echo e(mb_strtoupper($grado->nombre_estudiante)); ?>

                  </td>
                   <td>
                    <?php echo e(mb_strtoupper($grado->telefono_casa)); ?>

                  </td>
                   <td>
                    <?php echo e(mb_strtoupper($grado->email)); ?>

                  </td>
                   <td>
                    <?php echo e(mb_strtoupper($grado->nombre_grado)); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($grado->nombre_carrera)); ?>

                  </td>
                  <td>
                    <?php echo e(mb_strtoupper($grado->nombre_nivel)); ?>

                  </td>
                  <?php if (\Entrust::can('editar-grado')) : ?>
                  <td>
                    <?php echo link_to_route('estudiantes.edit', $title = 'Editar', $parameters = $grado->id_estudiante, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>

                  </td>
                  <?php endif; // Entrust::can ?>
                  <?php if (\Entrust::can('estado-grado')) : ?>
                  <td>
                    <?php if($grado->estado_grado == true): ?>
                      <?php echo link_to_route('grados.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarGrado', 'data-id'=>$grado->id_grado, 'data-estado'=>$grado->estado_grado]); ?>

                    <?php else: ?>
                      <?php echo link_to_route('grados.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success eliminarGrado', 'data-id'=>$grado->id_grado, 'data-estado'=>$grado->estado_grado]); ?>

                    <?php endif; ?>
                  </td>
                  <?php endif; // Entrust::can ?>
                </tr>
              <?php endforeach; ?>
            </tbody>


          </table>
          </div>
           <div class="text-center">
            <?php echo $cursos->links(); ?>

          </div>
          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">
          <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <?php endif; ?>
      
      </div>
    </div>
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>