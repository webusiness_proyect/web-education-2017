<?php $__env->startSection('titulo'); ?>



  <title>Planes</title>



<?php $__env->stopSection(); ?>







<?php $__env->startSection('cuerpo'); ?>



  <div id="page-wrapper">



    <div class="row">



      <div class="col-sm-12">

      <div class="panel-body ">

        <h1 class="page-header text-center"> <div class="fa fa-calendar-o" aria-hidden="true"></div> Planes de estudios disponibles </h1>
        <?php echo $__env->make('mensajes.msg', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
      </div>



      </div>



      <div class="col-sm-12 panel panel-default">

        <center>
        <?php echo link_to(URL::previous(), 'Atras', ['class' => 'btn btn-info fa fa-reply']); ?>



        <?php if (\Entrust::can('crear-plan')) : ?>



        <?php echo link_to_route('planes.create', $title ='Nuevo Plan', $parameters = null, $attributes = ['class'=>'btn btn-primary fa fa-plus-square']); ?>




        <br>



        <br>



        <?php endif; // Entrust::can ?>



        <?php if(count($planes) == 0): ?>



          <p class="text-info">



            No se han registrado planes aun...



          </p>

          </center>



        <?php else: ?>



          <table class="table table-hover table-bordered">



            <thead>



              <tr>



                <th>



                  NO



                </th>



                <th>



                  NOMBRE PLAN



                </th>



                <?php if (\Entrust::can('editar-plan')) : ?>



                <th>



                  ACTUALIZAR



                </th>



                <?php endif; // Entrust::can ?>



                <?php if (\Entrust::can('estado-plan')) : ?>



                <th>



                  ESTADO



                </th>



                <?php endif; // Entrust::can ?>



              </tr>



            </thead>



            <tbody id="datosPlanes">



              <?php foreach($planes as $key => $plan): ?>



                <tr>



                  <td>



                    <?php echo e($key+1); ?>




                  </td>



                  <td>



                    <?php echo e(mb_strtoupper($plan->nombre_plan)); ?>




                  </td>



                  <?php if (\Entrust::can('editar-plan')) : ?>



                  <td>



                    <?php echo link_to_route('planes.edit', $title = 'Editar', $parameters = $plan->id_plan, $attributes = ['class'=>'btn btn-warning fa fa-pencil-square-o']); ?>




                  </td>



                  <?php endif; // Entrust::can ?>



                  <?php if (\Entrust::can('estado-plan')) : ?>



                  <td>



                    <?php if($plan->estado_plan == 1): ?>



                      <?php echo link_to_route('planes.destroy', $title = 'Habilitado', $parameters = null, $attributes = ['class'=>'btn btn-success fa fa-check eliminarPlan', 'data-id'=>$plan->id_plan, 'data-estado'=>$plan->estado_plan]); ?>




                    <?php else: ?>



                      <?php echo link_to_route('planes.destroy', $title = 'Deshabilitado', $parameters = null, $attributes = ['class'=>'btn btn-danger fa fa-times eliminarPlan', 'data-id'=>$plan->id_plan, 'data-estado'=>$plan->estado_plan]); ?>




                    <?php endif; ?>



                  </td>



                  <?php endif; // Entrust::can ?>



                </tr>



              <?php endforeach; ?>



            </tbody>



          </table>



          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" id="token">



        <?php endif; ?>



      </div>



    </div>







    <!-- Mensaje de carga-->



    <?php echo $__env->make('mensajes.carga', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>







  </div>















<?php $__env->stopSection(); ?>




<?php echo $__env->make('principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>